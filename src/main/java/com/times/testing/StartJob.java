package com.times.testing;

// import Apache HTTP Client v 4.3
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;

import java.io.ByteArrayInputStream;
import java.util.List;

// import JSON
// import from JDK

public class StartJob {

    static String EPAPER_MASTER_COLLECTION = "epaper_master";
    static String EPAPER_FILES_COLLECTION = "epaperfileTest";


    public static void main(String[] args) throws Exception {
        String mongoFileId = "56c594b3a82660708cad679e";


        BasicDBObject statusQuery = new BasicDBObject();
        statusQuery.put("status", 1);

        BasicDBObject fileIdQuery = new BasicDBObject();
        statusQuery.put("_id", mongoFileId);

        BasicDBList queryParts = new BasicDBList();
        queryParts.add(statusQuery);
        queryParts.add(fileIdQuery);

        BasicDBObject finalQuery = new BasicDBObject();
        finalQuery.put("$and", queryParts);

        List<DBObject> fileList = MongoRestAPIService.getList(EPAPER_FILES_COLLECTION, finalQuery);

        // Right now it is setting fileId as constant, it should be get from 'fileList'
        fileIdQuery = new BasicDBObject();
        fileIdQuery.put("_id", mongoFileId);
        byte[] fileContent = MongoRestAPIService.getFile(EPAPER_FILES_COLLECTION, fileIdQuery);

        ZamzarService zs = new ZamzarService();

        String fileName = "sample-pdf.pdf";
        String jobId = zs.submitJob(new ByteArrayInputStream(fileContent), fileName);

        // Update mongo here with jobId and status = 2



    }


}