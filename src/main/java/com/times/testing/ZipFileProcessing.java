package com.times.testing;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by arun.sharma2 on 17/02/16.
 */
public class ZipFileProcessing {
    public static void main( String[] args )
    {
        byte[] buffer = new byte[1024];

        try{

            FileOutputStream fos = new FileOutputStream("/Users/arun.sharma2/MyFile.zip");
            ZipOutputStream zos = new ZipOutputStream(fos);
            ZipEntry ze= new ZipEntry("test.txt");
            zos.putNextEntry(ze);
            FileInputStream in = new FileInputStream("/Users/arun.sharma2/test.txt");

            int len;
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }

            in.close();
            zos.closeEntry();

            //remember close it
            zos.close();

            System.out.println("Done");

        }catch(IOException ex){
            ex.printStackTrace();
        }
    }
}
