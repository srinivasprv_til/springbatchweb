package com.times.testing;

import org.apache.http.HttpEntity;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.*;

/**
 * Created by arun.sharma2 on 18/02/16.
 */
public class ZamzarService {
    public static final String TARGET_FORMAT = "html5";
    public static final String TARGET_FORMAT_IDENTIFIER = "target_format";
    private static String API_KEY = "ff382b6575957646d4dc57b8a3a9b3043b5fb77a";
    String ZAMZAR_SUBMIT_ENDPOINT = "https://sandbox.zamzar.com/v1/jobs";
    String ZAMZAR_STATUS_ENDPOINT = "https://sandbox.zamzar.com/v1/jobs/"; // + jobId;

    private String getDownloadEndpoint(String fileId) {
        return "https://sandbox.zamzar.com/v1/files/" + fileId + "/content";
    }


    public String submitJob(InputStream is, String fileName) throws IOException {

        CloseableHttpClient httpClient = getHttpClient(API_KEY);
        HttpEntity requestContent = MultipartEntityBuilder.create()
                .addPart(fileName, new InputStreamBody(is, ContentType.APPLICATION_OCTET_STREAM))
                .addPart(TARGET_FORMAT_IDENTIFIER, new StringBody(TARGET_FORMAT, ContentType.TEXT_PLAIN))
                .build();

        HttpPost request = new HttpPost(ZAMZAR_SUBMIT_ENDPOINT);

        request.setEntity(requestContent);


        // Make request
        CloseableHttpResponse response = null;
        String result= null;
        try {
            response = httpClient.execute(request);

            // Extract body from response
            HttpEntity responseContent = response.getEntity();

            result = EntityUtils.toString(responseContent, "UTF-8");
        } catch (IOException e) {
            throw e;
        }

        // Parse result as JSON
        JSONObject json = new JSONObject(result);
        return (String) json.get("id");
    }


    public void downloadFile(String jobId) throws IOException {
        JSONObject json = getStatusJsonObject(jobId);
        String fileId = json.getJSONArray("target_files").getJSONObject(0).getString("id");

        String endpoint = getDownloadEndpoint(fileId);
        CloseableHttpClient httpClient = getHttpClient(API_KEY);
        HttpGet request = new HttpGet(endpoint);

        // Make request
        CloseableHttpResponse response = httpClient.execute(request);

        // Extract body from response
        HttpEntity responseContent = response.getEntity();

        // Save response content to file on local disk
        BufferedInputStream bis = new BufferedInputStream(responseContent.getContent());
        File localFilename = new File("/Users/arun.sharma2/Desktop/pdf-sample.zip");
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(localFilename));

        int inByte;
        while((inByte = bis.read()) != -1) {
            bos.write(inByte);
        }

        // Print success message
        System.out.println("File downloaded");

        // Finalise response, client and streams
        response.close();
        httpClient.close();
        bos.close();
        bis.close();
    }

    public Boolean isCompleted(String jobid) throws IOException {
        JSONObject json = getStatusJsonObject(jobid);

        String status = (String) json.get("status");
        if ("successful".equals(status)) return true;
        else return false;

        //return (String) ((List<JSONObject>) json.get("target_files")).get(0).get("id");
    }

    private JSONObject getStatusJsonObject(String jobid) throws IOException {
        CloseableHttpClient httpClient = getHttpClient(API_KEY);
        HttpGet request = new HttpGet(getZamzarStatusURLForJob(jobid));

        // Make request
        CloseableHttpResponse response = httpClient.execute(request);

        // Extract body from response
        HttpEntity responseContent = response.getEntity();
        String result = EntityUtils.toString(responseContent, "UTF-8");

        // Parse result as JSON
        JSONObject json = new JSONObject(result);

        // Print result
        System.out.println(json);

        // Finalise response and client
        response.close();
        httpClient.close();
        return json;
    }

    private String getZamzarStatusURLForJob(String jobid) {
        return ZAMZAR_STATUS_ENDPOINT + jobid;
    }

    // Creates a HTTP client object that always makes requests
    // that are signed with the specified API key via Basic Auth
    private static CloseableHttpClient getHttpClient(String apiKey) {
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(apiKey, ""));

        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setDefaultCredentialsProvider(credentialsProvider)
                .build();

        return httpClient;
    }
}
