package com.times.testing;

import com.mongodb.BasicDBObject;
import com.mongodb.Mongo;
import com.times.common.dao.MongoRestAPIService;

import java.io.File;

/**
 * Created by arun.sharma2 on 18/02/16.
 */
public class UploadFile {
    public static void main(String[] args) {
        File file  = new File("/Users/arun.sharma2/Desktop/pdf-sample.pdf");
        MongoRestAPIService.addFile("epaperfilesTest", file);
    }
}
