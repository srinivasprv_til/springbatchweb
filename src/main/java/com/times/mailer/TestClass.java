package com.times.mailer;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * This class is used to invoke the manual batch.
 * 
 * @author Manish.Chandra
 *
 */
public class TestClass {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = null;
    	try { 
    		ctx = new ClassPathXmlApplicationContext(new String[]{"launch-context.xml"});
        	Job job =(Job) ctx.getBean("NewsMailerJob");
        	JobLauncher jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");
    		JobParameter jbp = new JobParameter(new Date());
    		Map<String, JobParameter> parameters = new HashMap<String, JobParameter>();
    		parameters.put("date", jbp);
    		jobLauncher.run(job, new JobParameters(parameters));
		} catch (JobExecutionAlreadyRunningException e) {
			e.printStackTrace();
		} catch (JobRestartException e) {
			e.printStackTrace();
		} catch (JobInstanceAlreadyCompleteException e) {
			e.printStackTrace();
		} catch (JobParametersInvalidException e) {
			e.printStackTrace();
		}
	}

}
