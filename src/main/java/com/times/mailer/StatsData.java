package com.times.mailer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

import com.times.common.mail.EmailProcessor;
import com.times.common.util.Constant;
import com.times.mailer.dao.MailerDao;

/**
 * This class is used as intercepter to intercept when .
 * 
 * @author Manish.Chandra
 * @author Ranjeet.Jha
 *
 */
public class StatsData implements StepExecutionListener {

	private static final Log log = LogFactory.getLog(StatsData.class);
	
	private MailerDao dao;
	private EmailProcessor emailProcessor;

	public void beforeStep(StepExecution stepExecution) {
		
		try {
			log.debug("Inside the afterStep : and going to send the mail ");
			//emailProcessor.processStart(null);
			//ExecutionContext ec = stepExecution.getExecutionContext();
		} catch (Exception e) {
			log.error("Exception caught,  msg : " + e.getMessage());
			e.printStackTrace();
		}
	
	}

	public ExitStatus afterStep(StepExecution stepExecution) {
		String newsLetterId = null;
		boolean isDaily = false;
		try {
			JobParameters jobParams = stepExecution.getJobParameters(); 
			newsLetterId = jobParams.getString(Constant.JOB_PARAM_NEWSLETTER_ID);
			int dailyOrWeekly = (int)jobParams.getLong(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY);
			if (Constant.JOB_PARAM_NL_DAILY == dailyOrWeekly){
				isDaily = true;
			} else {
				isDaily = false;
			}
			//this.dao.updateNewsLetterDailyTracker(code, skey, ireadCount, iCount);
			/*if (jobParams != null) {
				for (Map.Entry<String, JobParameter> entry : jobParams.getParameters().entrySet()) {
					logger.debug(entry.getKey() + " : " + entry.getValue().getValue());
				}
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//TODO: Log Stats
		/*
		try {
			log.debug("Inside the afterStep : and going to send the mail ");
			emailProcessor.processCompleted(null);
			ExecutionContext ctx = stepExecution.getExecutionContext();
			Mailer mailer =(Mailer)ctx.get("mailer");
			int itype = mailer.isDaily() ? 1: 2;
			String newsid = mailer.getId();
			//add data to mongo db for total successful write...
			String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + itype + Constant.KEY_BUILDER_SEPERATOR + mailer.getScheduleTime();
			int iCount = stepExecution.getWriteCount();
			int ireadCount = stepExecution.getReadCount();
			
			//getDao().addMailerStats(skey, ireadCount, iCount);
			String code = null;
			if (InMemoryData.keyCodeMap.containsKey(skey)) {
				code = InMemoryData.keyCodeMap.get(skey);
			}
			getDao().updateNewsLetterDailyTracker(code, skey, ireadCount, iCount);
			//System.out.println(skey +" --reads--"+ ireadCount +" -- write--"+ iCount);
		} catch (Exception e) {
			log.error("Exception caught in afterStep metod after process completed,  msg : " + e.getMessage());
		}*/
		
		return ExitStatus.COMPLETED;
	}

	/**
	 * @param emailProcessor the emailProcessor to set
	 */
	public void setEmailProcessor(EmailProcessor emailProcessor) {
		this.emailProcessor = emailProcessor;
	}

	public MailerDao getDao() {
		return dao;
	}

	public void setDao(MailerDao dao) {
		this.dao = dao;
	}

	
}



