package com.times.mailer.constant;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.times.common.constant.AmazonBrowseNodes;

public interface GadgetConstant {
	
	//Mongo Collection Names
	public static final String BRAND_DETAIL = "gadget_brand_detail";
	public static final String PRODUCT_DETAIL = "gadget_product_detail";
	public static final String PRODUCT_DETAIL_OLD = "product_detail";
	public static final String SECONDARY_PRODUCT_DETAIL = "products_secondryData";
	public static final String AMAZON_PRODUCT_DETAIL="amazon_product_detail";
	
	
	//Source
	public static final String PRIMARY_SOURCE = "91mobiles";
	public static final String EDITOR_SOURCE = "EDITOR";
	
	
	//HOST ID
	public static final int GN_HOSTID = 307;
	public static final String GN_HOST_NAME="gadgetsnow.com";
	
	public static final String CATEGORY="category";
	public static final String SHOPPING_ID="shoppingId";
	public static final String SHOPPING_DATA="shopping_data";
	public static final String INSERTED_AT="insertedAt";
	
	
	/*//Mongo Collection Names
	public static final String BRAND_DETAIL = "brand_detail";
	public static final String PRODUCT_DETAIL = "product_detail";
	public static final String PRODUCT_DETAIL_OLD = "product_detail";
	public static final String SECONDARY_PRODUCT_DETAIL = "products_secondryData";
	
	
	//Source
	public static final String PRIMARY_SOURCE = "BUYT";
	public static final String EDITOR_SOURCE = "EDITOR";
	
	//HOST ID
	public static final int TOI_HOSTID = 83;*/
	
	
	//Product Status
	public static final int INACTIVE = 0;
	public static final int PENDING = 1;
	public static final int ACTIVE = 2;
	public static final String SHOPPING_ITEM = "shopping_items";	
	
	
	//FieldNames
	class ShoppingFields{
	
	 public static final String PRODUCT_ID="PRODUCT_ID";
     public static final String PRODUCT_NAME= "PRODUCT_NAME";
     public static final String MRP="MRP";
     public static final String SELLING_PRICE= "SELLING_PRICE";
     public static final String SHIPPING_PRICE="SHIPPING_PRICE";
     public static final String DISCOUNT="DISCOUNT";
     public static final String COLOR="COLOR";
     public static final String AVAILABILITY="AVAILABILITY";
     public static final String COLOR_HEXCODE="COLOR_HEXCODE";
     
	}
	
	//Category
//	public static final String MOBILES = "mobile";
//	public static final String TABLETS = "tablet";
//	public static final String LAPTOPS = "laptop";
//	public static final String CAMERAS = "camera";
//	public static final String TV = "tv";
//	public static final String AC = "ac";
//	public static final String REFRIGERATORS = "refrigerator";
//	public static final String WASHING_MACHINES = "washingmachine";
//	public static final String POWER_BANKS = "powerbank";
//	public static final String SMART_WATCH = "smartwatch";
//	public static final String FITNESS_BAND = "fitnessband";
	
	class Fields{
		
		public static final String LCUNAME="lcuname";
		public static final String UNAME="uname";
		public static final String INSERTED_AT="insertedAt";
		public static final String BRAND_NAME="brand_name";
		public static final String NAME="name";
		
	}
	
	
	//Category Map
	class Category {
		
		public static final String MOBILES = "mobile";
		public static final String TABLETS = "tablet";
		public static final String LAPTOPS = "laptop";
		public static final String CAMERAS = "camera";
		public static final String TV = "tv";
		public static final String AC = "ac";
		public static final String REFRIGERATORS = "refrigerator";
		public static final String WASHING_MACHINES = "washingmachine";
		public static final String POWER_BANKS = "powerbank";
		public static final String SMART_WATCH = "smartwatch";
		public static final String FITNESS_BAND = "fitnessband";
		
		public static final String [] SELECTED_CATEGORIES_FOR_AMAZON_JOB={
				MOBILES,
				TABLETS,
				LAPTOPS,
				AC,
				REFRIGERATORS,
				WASHING_MACHINES,
				TV		
		};
		
		public static final Map<String, String> categoryMap = new LinkedHashMap<String, String>();
		static {
			categoryMap.put("553", MOBILES);
			categoryMap.put("579", TABLETS);
			categoryMap.put("24", LAPTOPS);
			categoryMap.put("555", CAMERAS);
	
			categoryMap.put("110", TV);
			categoryMap.put("142", AC);
			
			categoryMap.put("236", REFRIGERATORS);
			categoryMap.put("137", WASHING_MACHINES);
			categoryMap.put("585", POWER_BANKS);
			
			categoryMap.put("583", SMART_WATCH);
			categoryMap.put("584", FITNESS_BAND);
		}
		
		public static Map<String,String> CATEGORY_TO_BROWSE_NODE_MAP=Collections.unmodifiableMap(
			    new HashMap<String, String>() {{
			    	 put(MOBILES,AmazonBrowseNodes.SMART_AND_BASIC_PHONES_ID);
			    	 put(TABLETS,AmazonBrowseNodes.TABLETS_ID);
			    	 put(LAPTOPS,AmazonBrowseNodes.LAPTOPS_ID);
			    	 put(CAMERAS,AmazonBrowseNodes.CAMERAS_AND_PHOTOGRAPHY);
			    	 put(TV,AmazonBrowseNodes.TELEVISIONS);
			    	 put(POWER_BANKS,AmazonBrowseNodes.POWERBANKS);
			    	 put(SMART_WATCH,AmazonBrowseNodes.SMART_WATCH_AND_ACCESSORIES);
			    	 put(FITNESS_BAND,AmazonBrowseNodes.ACTIVITY_TRACKERS);
			    	 put(WASHING_MACHINES,AmazonBrowseNodes.WASHING_MACHINE);
			    	 put(AC,AmazonBrowseNodes.AIR_CONDITIONER);
			    	 put(REFRIGERATORS,AmazonBrowseNodes.REFRIDGERATORS);
			    	 
			    }});
		
		public static Map<String,String> BROWSE_NODE_TO_CATEGORY_MAP=CATEGORY_TO_BROWSE_NODE_MAP.
																entrySet().stream().
															collect( Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey, (a, b) -> a));
		
		
	}
	
}
