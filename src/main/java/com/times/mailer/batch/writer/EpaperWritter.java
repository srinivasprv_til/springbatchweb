package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.Constant;
import com.times.mailer.model.EpaperFileDetails;
import com.times.mailer.model.EpaperInfo;


public class EpaperWritter implements ItemWriter<EpaperInfo> {
	
	private static final Log log = LogFactory.getLog(EpaperWritter.class);

	public void write(List<? extends EpaperInfo> epaperInfoList) throws Exception {
		log.debug("inside the write ");
		if (epaperInfoList != null && epaperInfoList.size() >  0) {
			EpaperInfo epaperInfo = epaperInfoList.get(0);
			if (epaperInfo != null && epaperInfo.getFileDetails() != null && epaperInfo.getFileDetails().size() > 0) {
				for (EpaperFileDetails efd : epaperInfo.getFileDetails()) {
					
					DBObject mrDBObject = new BasicDBObject();
					mrDBObject.put("ukey", epaperInfo.getUnique_key());
					mrDBObject.put("gfsid", efd.getGsfid());
					mrDBObject.put("date", efd.getDate());
					mrDBObject.put("c_at", efd.getCreationDate());		
					mrDBObject.put("fileName", efd.getFileName());
					mrDBObject.put("status", 1);
					MongoRestAPIService.insert(Constant.EPAPER_RAW_DATA_COLLECTION, mrDBObject);
					
				}
			}
		}
	}

}