package com.times.mailer.batch.writer;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.constant.AmazonItemFields;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.model.AmazonItem;
import com.times.common.util.GadgetUtil;
import com.times.mailer.constant.GadgetConstant;

public class AmazonProductDetailWriter implements ItemWriter<List<AmazonItem>>{


	private static final Logger logger = LoggerFactory.getLogger(AmazonProductDetailWriter.class);


	public void write(List<? extends List<AmazonItem>> items) throws Exception {

		if (items!=null && items.size()>0 && items.get(0)!=null) {
			logger.info("Writer received a list of size "+items.get(0).size());
			List<AmazonItem> amazonItems = items.get(0);
			for (AmazonItem amazonItem : amazonItems) {

				insertAmazonProductInMongo(amazonItem);
			} 
		}
		
		
	}

	public void insertAmazonProductInMongo(AmazonItem amazonItem){

		try {

			DBObject apiObject=getDBObjectFromAmazonItem(amazonItem);
			
			DBObject insertDateObject = new BasicDBObject();
			insertDateObject.put("insertedAt", new Date());
			DBObject setObject=new BasicDBObject().append("$set", apiObject).append("$setOnInsert", insertDateObject);

			if (apiObject!=null) {
				MongoRestAPIService.updateWithPost(GadgetConstant.AMAZON_PRODUCT_DETAIL, new BasicDBObject().append(AmazonItemFields.DB_ASIN, 
						apiObject.get(AmazonItemFields.DB_ASIN)),setObject,true);
			}

		}
		catch(Exception e){
			logger.error("Insertion failed for ASIN : "+amazonItem.getASIN());
		}


	}



	private DBObject getDBObjectFromAmazonItem(AmazonItem amazonItem ){

		try {
			DBObject apiObject = new BasicDBObject();
			if(amazonItem.getTitle()!=null){
				apiObject.put(AmazonItemFields.DB_NAME, amazonItem.getTitle());
				apiObject.put(AmazonItemFields.DB_LCNAME, amazonItem.getTitle().toLowerCase());
				apiObject.put(AmazonItemFields.DB_LCUNAME, GadgetUtil.getUName(amazonItem.getTitle().toLowerCase()));

			}
			
			if( amazonItem.getASIN()!=null){
				apiObject.put(AmazonItemFields.DB_ASIN, amazonItem.getASIN());
			}

			if(amazonItem.getBrand()!=null){
				apiObject.put(AmazonItemFields.DB_BRAND, amazonItem.getBrand());
			}
			
			if(amazonItem.getBrowseNode()!=null){
				String browseNode=amazonItem.getBrowseNode();
				apiObject.put(AmazonItemFields.DB_BROWSE_NODE, browseNode);
				
				if(GadgetConstant.Category.BROWSE_NODE_TO_CATEGORY_MAP.get(browseNode)!=null){
					apiObject.put(GadgetConstant.CATEGORY, GadgetConstant.Category.BROWSE_NODE_TO_CATEGORY_MAP.get(browseNode));
				}	
			}

			if(amazonItem.getManufacturer()!=null){
				apiObject.put(AmazonItemFields.DB_MANUFACTURER, amazonItem.getManufacturer());
			}

			DBObject lowestPriceDBObject=amazonItem.getLowestNewPriceDBObject();
			if(lowestPriceDBObject!=null){
				apiObject.put(AmazonItemFields.DB_LOWEST_NEW_PRICE,lowestPriceDBObject);
			}
			
			DBObject listPriceDBObject=amazonItem.getListPriceDBObject();
			if(listPriceDBObject!=null){
				apiObject.put(AmazonItemFields.DB_LIST_PRICE,listPriceDBObject);
			}
			
			DBObject smallImage= amazonItem.getSmallImageDBObject();
			if(smallImage!=null){
				apiObject.put(AmazonItemFields.DB_SMALL_IMAGE,smallImage);
			}
			
			DBObject lowestUsedPriceDBObject=amazonItem.getLowestUsedPriceDBObject();
			if(lowestUsedPriceDBObject!=null){
				apiObject.put(AmazonItemFields.DB_LOWEST_USED_PRICE,lowestUsedPriceDBObject);
			}

			if(amazonItem.getDetailPageURL()!=null){
				apiObject.put(AmazonItemFields.DB_DETAIL_PAGE_URL, amazonItem.getDetailPageURL());
			}

			if(amazonItem.getEditorialReviews()!=null){
				apiObject.put(AmazonItemFields.EDITORIAL_REVIEW, amazonItem.getEditorialReviews());
			}
			
			if( amazonItem.getColor()!=null){
				apiObject.put(AmazonItemFields.DB_COLOR, amazonItem.getColor());
			}
			
			if( amazonItem.getProductTypeName()!=null){
				apiObject.put(AmazonItemFields.PRODUCT_TYPE_NAME, amazonItem.getProductTypeName());
			}

			if(amazonItem.getSalesRank()!=null){
				apiObject.put(AmazonItemFields.DB_SALES_RANK, amazonItem.getSalesRank());
			}
			
			apiObject.put("updatedAt", new Date());

			return apiObject;
		} catch (Exception e) {
			logger.error("Transformation into DBObject failed for ASIN : "+amazonItem.getASIN());
			return null;

		}
	}

	

	public ExitStatus afterStep(StepExecution stepExecution) {

		logger.debug("After Step method..");
		return null;
	}



}
