package com.times.mailer.batch.writer;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.model.IBeatEntity;
import com.times.mailer.constant.GadgetConstant;

public class GadgetsPopularityWritter implements ItemWriter<IBeatEntity> {
	
	private static final Logger logger = LoggerFactory.getLogger(GadgetsPopularityWritter.class); 

	@Override
	public void write(List<? extends IBeatEntity> ibeatEntities) throws Exception {
		
		if(ibeatEntities!=null && ibeatEntities.size()>0){
			
			logger.info("Writter received a chunk of size={} ",ibeatEntities.size());
			
			for(IBeatEntity ibeatEntity: ibeatEntities ){
				
				updateGadgetPopularityData(ibeatEntity);
			}
		}else{
			logger.error("Empty/Null chunk received to the writter");
		}
		
	}

	
	/**
	 * Updates gadget corresponding to msid in the ibeatEntity, If msid is not a gadget then no update/upsert is done
	 * @param ibeatEntity
	 * @see com.times.common.model.IBeatEntity
	 */
	private void updateGadgetPopularityData(IBeatEntity ibeatEntity) {
		
		if(ibeatEntity!=null && ibeatEntity.getMsid()>0){
			
			logger.info("Updating ibeatEntity={}",String.valueOf(ibeatEntity));
			
			int msid=ibeatEntity.getMsid();
			long pageViews=ibeatEntity.getViews();
			long comments=ibeatEntity.getComments();
			long social=ibeatEntity.getSocial();
			
			DBObject updateObject= new BasicDBObject();
			
			if(pageViews>0){
				updateObject.put("page_views", pageViews);
			}
			
			if(comments>0){
				updateObject.put("comments", comments);
			}
			
			if(social>0){
				updateObject.put("social", social);
			}
			
			DBObject setObject= new BasicDBObject().append("$set",updateObject);
			logger.info("set object={}",setObject);
			
			DBObject queryObject= new BasicDBObject().append("reviewid.msid", ibeatEntity.getMsid());
			logger.info("query object={}",queryObject);

			
			if(MongoRestAPIService.getCount(GadgetConstant.PRODUCT_DETAIL, queryObject)>0){
				logger.info("MSID is a gadget, Updating ........\n");
				MongoRestAPIService.updateWithPost(GadgetConstant.PRODUCT_DETAIL, queryObject, setObject,false);
			}else{
				logger.info("MSID is not a gadget");
			}
			
		}
		
	}

}
