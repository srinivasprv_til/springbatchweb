package com.times.mailer.batch.writer;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.function.Function;

import org.apache.http.HttpException;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.util.StringUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.ExternalHttpUtil;
import com.times.common.util.GadgetUtil;
import com.times.common.util.InMemoryData;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.dao.TechGadgetProductDAO;
import com.times.mailer.model.TechGadgetProductDetail;

import static com.times.mailer.constant.GadgetConstant.*;
import static com.times.mailer.constant.GadgetConstant.Category.*;

/**
 * This class receives the Object of 'TechGadgetProductDetail' from reader, and finds the complete detail of the product.
 * This detail is then saved to Mongo in 2 collections.
 * Mongo Collection:
 * 	1. brand_detail: Contains the detail specific to a brand.
 * 		DT field contains the date last updated.
 * 	
 * 	2. product_detail: Contains the detail specific to a product.
 * 					This also contains some of the brand detail for which the product belongs to.
 * 		DT field contains the date last updated.
 * 
 * @author Anurag.Singhal
 *
 */
public class TechGadgetProductWritter implements ItemWriter<TechGadgetProductDetail>{

	
	private static final Logger logger = LoggerFactory.getLogger(TechGadgetProductWritter.class);
	
//	private String ssoToken;
	private TechGadgetProductDAO gadgetDAO;
	
	public List<String> processedBrandList = new ArrayList<String>();
	
	//OldGadgetsMap
    public Map<String, Integer> oldGadgetsMap = new HashMap<String, Integer>();
    public Map<String, String> oldGadgetSEOMap = new HashMap<String, String>();
    
    public Map<String, Integer> manuallyMatchedBuyTGadgets = new HashMap<String, Integer>();
    
//    public Map<Integer, Map<String, DBObject>> categoryProductListMap = new HashMap<Integer, Map<String, DBObject>>();
    public Map<String, DBObject> categoryBrandListMap = new HashMap<String, DBObject>();
    
    //This Map contains the all category Data, with List of Objects for a given UniqueId in innerMAP.
    public Map<String, Map<String, List<DBObject>>> categoryProductList4UniqueIDMap = new HashMap<String, Map<String, List<DBObject>>>();
    
    
    //HardCoded Sections in which New Blank articles will be created.
    /*private int MOBILE_BRAND_LIST_MSID = 50453005;
    private int TABLET_BRAND_LIST_MSID = 50453017;
    private int LAPTOP_BRAND_LIST_MSID = 50453023;
    private int CAMERA_BRAND_LIST_MSID = 50453059;
    private int MISSING_BRAND_LIST_MSID = 50465808;
    
    
    private int MISSING_MOBILE_BRAND_MSID = 50478948;
    private int MISSING_LAPTOP_BRAND_MSID = 50478955;
    private int MISSING_CAMERA_BRAND_MSID = 50478961;
    
	private Map<String, Integer> mobBrandMsidMap = new HashMap<String, Integer>();
	private Map<String, Integer> laptopBrandMsidMap = new HashMap<String, Integer>();
	private Map<String, Integer> cameraBrandMsidMap = new HashMap<String, Integer>();*/

	public void write(List<? extends TechGadgetProductDetail> items) throws Exception {
		
		String ftpLocation = null;
		
		try {
			
			if(categoryProductList4UniqueIDMap.isEmpty())
				init();
			
			if (items != null && items.size() > 0) {
				
				for (TechGadgetProductDetail productDetail : items) {
					//Add the Brand Detail in Mongo
					
					if(productDetail.getSpecs() != null) {
						insertBrandInMongo(productDetail);
						insertProductInMongo(productDetail);
					} else {
						logger.error("No Specs found for product:{}", productDetail);
					}
				}
			}
		} catch (Exception e) {
			
			logger.error("Exception while inserting TechGadget productDetail in Mongo DB: "+ e.getMessage());
			new CMSCallExceptions("Exception caught while Inserting data to DB: " + ftpLocation + " msg: " + e.getMessage(),
									CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		}
	}
	
	// End public methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start private methods
	
	
	private void insertBrandInMongo(TechGadgetProductDetail productDetail) {
		try {
			String uniqueID = getUniqueID(productDetail.getBrandName());
			
			//For Secondary Source, Do not update Brand data, if already exists.
			//Also, Change Brand Name and Image, as present in Primary Source.
			if(categoryBrandListMap.containsKey(uniqueID)) {
				productDetail.setBrandName(String.valueOf(categoryBrandListMap.get(uniqueID).get("name")));
				productDetail.setBrandImage(String.valueOf(categoryBrandListMap.get(uniqueID).get("image")));
			}
			
			if(!processedBrandList.contains(productDetail.getCategory() + "##" + uniqueID)) {
				
				DBObject queryObject = new BasicDBObject();
//				queryObject.put("category", productDetail.getCategory());
				queryObject.put("uniqueid", uniqueID);
				
				//Return if category is also present.
				if(categoryBrandListMap.containsKey(uniqueID)) {
					
					if(((List<String>)categoryBrandListMap.get(uniqueID).get("category")).contains(productDetail.getCategory())) {
						processedBrandList.add(productDetail.getCategory() + "##" + uniqueID);
						return;
					}
					else {
						MongoRestAPIService.updateWithPost(BRAND_DETAIL, queryObject, new BasicDBObject().append("$addToSet", new BasicDBObject("category", productDetail.getCategory())), false);
						((List<String>)categoryBrandListMap.get(uniqueID).get("category")).add(productDetail.getCategory());
						
						processedBrandList.add(productDetail.getCategory() + "##" + uniqueID);
						return;
					}
				}
				
				
				DBObject dbObject = new BasicDBObject();
				dbObject.put("name", productDetail.getBrandName());
//				String uniqueID = getUniqueID(productDetail.getBrandName());
				dbObject.put("uniqueid", uniqueID);//UNIQUE ID, on which brand will be identified as Unique
				
				dbObject.put("uname", getUName(productDetail.getBrandName()));
				dbObject.put("image", productDetail.getBrandImage());
				
				List<String> categoryLst = new ArrayList<String>(1);
				categoryLst.add(productDetail.getCategory());
				dbObject.put("category", categoryLst);
				
				dbObject.put("msid", 0);
				dbObject.put("updatedAt", new Date());
				
				if(PRIMARY_SOURCE.equals(productDetail.getSource()))
					dbObject.put("status", ACTIVE);
				else{
					//Also Send Alert here.
					dbObject.put("status", PENDING);
					InMemoryData.brandAlertList.add(dbObject);
				}
				
				
				DBObject insertDateObject = new BasicDBObject();
				insertDateObject.put("insertedAt", new Date());
				
				DBObject finalObject = new BasicDBObject().append("$setOnInsert", insertDateObject).append("$set", dbObject);
				MongoRestAPIService.updateWithPost(BRAND_DETAIL, queryObject, finalObject, true);
				
				categoryBrandListMap.put(uniqueID, dbObject);
				processedBrandList.add(productDetail.getCategory() + "##" + uniqueID);
			}
			
		} catch (MongoException e) {
			
			logger.error("MongoException caught while inserting TechGadget productDetail , msg : " + e.getMessage(), e);
			
		} catch (Exception e) {
			
			logger.error("Generic Exception caught while inserting TechGadget productDetail ", e);
			
		} finally {
		}
	}
	
	
	private void insertProductInMongo(TechGadgetProductDetail productDetail) {
		try {
			
			DBObject apiObject = new BasicDBObject();
			apiObject.put("pid", productDetail.getProductId());//ID from the Source
			
			String uniqueID = getUniqueID(productDetail.getProductName());
			apiObject.put("uniqueid", uniqueID);//UNIQUE ID, on which product will be identified as Unique
			
			if(!StringUtils.hasText(productDetail.getSource()))
					return;
			
			boolean isNewRecord = false;
			
			if(categoryProductList4UniqueIDMap.get(productDetail.getCategory()) != null) {
				
				List<DBObject> productList4UniqueID = categoryProductList4UniqueIDMap.get(productDetail.getCategory()).get(uniqueID);
				int status;
				int msid = 0;
				DBObject existingDBObject = null;
				
				if(productList4UniqueID != null && !productList4UniqueID.isEmpty()) {
					//uniqueId+Source:
					List<DBObject> sameSourceList = productList4UniqueID.stream()
							.filter(dbObject -> productDetail.getSource().equals(String.valueOf(dbObject.get("source")))).collect(Collectors.toList());
					
					if(PRIMARY_SOURCE.equals(productDetail.getSource())) {
						if(sameSourceList != null && !sameSourceList.isEmpty()) {
							existingDBObject = sameSourceList.get(0);
							status = Integer.parseInt(String.valueOf(existingDBObject.get("status")));
						}
						else {
							isNewRecord = true;
							List<DBObject> activeLst = productList4UniqueID.stream()
									.filter(dbObject -> Integer.valueOf(String.valueOf(dbObject.get("status"))) == ACTIVE).collect(Collectors.toList());
							
							if(activeLst != null && !activeLst.isEmpty()) {
								status = PENDING;
								existingDBObject = activeLst.get(0);
								//Set the Name of this primary gadget, as that of Active data present from other source.
								//This is done, so that Uname can be same as being used earlier.
								productDetail.setProductName(String.valueOf(existingDBObject.get("name")));
							}
							else {
								status = ACTIVE;
								existingDBObject = productList4UniqueID.get(0);
							}
						}
					} 
					
					
					else if(EDITOR_SOURCE.equals(productDetail.getSource())) {
						if(sameSourceList != null && !sameSourceList.isEmpty()) {
							existingDBObject = sameSourceList.get(0);
							status = productDetail.getStatus();//Status selected by Editor
							
							//Update the status in existing in-memory data.
							existingDBObject.put("status", status);
						} else {
							isNewRecord = true;
							
							if(productDetail.getStatus() == INACTIVE || productDetail.getStatus() == PENDING) {
								//WARNING MAY be given to editor, that it is inactive.
								status = productDetail.getStatus();//Status selected by Editor
								existingDBObject = productList4UniqueID.get(0);
							}
							else {
								List<DBObject> activeLst = productList4UniqueID.stream()
										.filter(dbObject -> Integer.valueOf(String.valueOf(dbObject.get("status"))) == ACTIVE).collect(Collectors.toList());
								
								if(activeLst != null && !activeLst.isEmpty()) {
									//PROVIDE ERROR MSG, that Product already exists in active mode, with different source.
									existingDBObject = activeLst.get(0);
									return;
								}
								else {
									status = ACTIVE;
									existingDBObject = productList4UniqueID.get(0);
								}
							}
						}
					} else {
						//To Update price in Primary
						if(productDetail.getPrice() > 0) {
							//Update Only the price in primary and return;
							//TODO
						}
						return;
					}
					
					apiObject.put("status", status);
					
					if(existingDBObject != null) {
//						msid = Integer.parseInt(String.valueOf(existingDBObject.get("msid")));
//						apiObject.put("msid", msid);
						
						if(existingDBObject.get("reviewid") != null && existingDBObject.get("reviewid") instanceof List) {
							msid = getParameterFromReviewList((List<DBObject>)existingDBObject.get("reviewid"), "msid");
						}
						
						apiObject.put("reviewid", existingDBObject.get("reviewid"));
						
						if(existingDBObject.get("seourl") != null)
							apiObject.put("seourl", existingDBObject.get("seourl"));
						if(existingDBObject.get("price") != null)
							apiObject.put("price", existingDBObject.get("price"));
					}
					
				} 
				
				//If none such exists:
				//Make this active, and create Article.(Send alert if this is not the primary source)
				else {
					if(PRIMARY_SOURCE.equals(productDetail.getSource()) || EDITOR_SOURCE.equals(productDetail.getSource())) {
						status = ACTIVE;
					} else {
						status = PENDING;
					}
					msid = 0;
					
					List<DBObject> reviewList = new ArrayList<DBObject>();
					reviewList.add(new BasicDBObject("msid", msid).append("hostid", GN_HOSTID));
					
					apiObject.put("status", status);
					apiObject.put("reviewid", reviewList);
					if(productDetail.getPrice() > 0)
						apiObject.put("price", productDetail.getPrice());
					
					
					isNewRecord = true;
					
				}
				
				if(status == ACTIVE && msid == 0) {
					//Create Article, and put msid
					DBObject articleObject = addArticleIfNewProduct(uniqueID, productDetail.getBrandName(), productDetail.getProductName(), 
								productDetail.getCategory(), productDetail.getProductId());
					apiObject.put("reviewid", articleObject.get("reviewid"));
					if(articleObject.get("seourl") != null)
						apiObject.put("seourl", articleObject.get("seourl"));
					
					//Update MSID in Existing In-memory Object, so that if further same uniqueID data comes, it uses the same msid.
					if(existingDBObject != null) {
						existingDBObject.put("reviewid", articleObject.get("reviewid"));
					}
					
				}
				
			}
			
			
			//New Category data is coming here, which will not be added currently.
			else {
				return;
			}
			
			apiObject.put("name", productDetail.getProductName());
			apiObject.put("lcname", productDetail.getProductName().toLowerCase()); //lower case name

			//Updated Name
			apiObject.put("uname", getUName(productDetail.getProductName()));
			apiObject.put("brand_uname", getUName(productDetail.getBrandName()));
			
			apiObject.put("image", productDetail.getProductImage());
			
			if(productDetail.getAdded() != null)
				apiObject.put("added", productDetail.getAdded());
			if(productDetail.getDeleted() != null)
				apiObject.put("deleted", productDetail.getDeleted());
			if(productDetail.getAnnouncedDate() != null)
				apiObject.put("announced", productDetail.getAnnouncedDate());
			if(productDetail.getModelName() != null)
				apiObject.put("model_name", productDetail.getModelName());
//			if(productDetail.getPrice() > 0)
//				apiObject.put("price", productDetail.getPrice());
			
			apiObject.put("category", productDetail.getCategory());
			apiObject.put("Key_features", productDetail.getKey_features());
			apiObject.put("specs", productDetail.getSpecs());
			
//			dbObject.put("Key_features", apiObject.get("Key_features"));
//			dbObject.put("specs", apiObject.get("specs"));
			
			apiObject.put("brand_name", productDetail.getBrandName());
			apiObject.put("brand_image", productDetail.getBrandImage());
			
			apiObject.put("source", productDetail.getSource());
//			apiObject.put("status", ACTIVE);
			
			//Product Last Updated
			apiObject.put("updatedAt", new Date());
			
			DBObject queryObject = new BasicDBObject();
//			queryObject.put("pid", productDetail.getProductId());
			//Commented below line temporarily, so that uniqueId can be created first time.
			queryObject.put("category", productDetail.getCategory());
			queryObject.put("uniqueid", uniqueID);
			queryObject.put("source", productDetail.getSource());
			
			/*DBObject articleMsid = addArticleIfNewProduct(uniqueID, productDetail.getBrandName(), productDetail.getProductName(), 
					String.valueOf(apiObject.get("uname")), productDetail.getCategory(), productDetail.getProductId());
			
			apiObject.put("msid", articleMsid.get("msid"));
			apiObject.put("status", articleMsid.get("status"));
			if(articleMsid.get("seourl") != null)
				apiObject.put("seourl", articleMsid.get("seourl"));
			if(articleMsid.get("price") != null)
				apiObject.put("price", articleMsid.get("price"));*/
			
			
			DBObject insertDateObject = new BasicDBObject();
			insertDateObject.put("insertedAt", new Date());
			
			MongoRestAPIService.updateWithPost(PRODUCT_DETAIL, queryObject, new BasicDBObject().append("$setOnInsert", insertDateObject).append("$set", apiObject), true);
			
			//Update the Map created for future incoming data..
			if(isNewRecord) {
				List<DBObject> list = new ArrayList<DBObject>();
				DBObject afterAddObj = MongoRestAPIService.get(PRODUCT_DETAIL, queryObject);
				if(afterAddObj != null) {
					list.add(new BasicDBObject().append("uniqueid", afterAddObj.get("uniqueid"))
							.append("source", afterAddObj.get("source")).append("status", afterAddObj.get("status"))
							.append("reviewid", afterAddObj.get("reviewid")).append("price", afterAddObj.get("price")));
					categoryProductList4UniqueIDMap.get(productDetail.getCategory()).put(uniqueID, list);
					
					
					//Create List for Sending Alert.
					if(productDetail.getCategory().equals(MOBILES))
						InMemoryData.alertList.add(afterAddObj);
				} else {
					logger.error("Some MisBehaving occurred here... {}", apiObject);
				}
			}
			
		} catch (MongoException e) {
			
			logger.error("MongoException caught while inserting TechGadget productDetail , msg : " + e.getMessage(), e);
			
		} catch (Exception e) {
			
			logger.error("Generic Exception caught while inserting TechGadget productDetail ", e);
			
		} finally {
		}
	}
	
	private int getParameterFromReviewList(List<DBObject> reviewLst, String parameter) {
		for(DBObject reviewObj : reviewLst) {
			if(GN_HOSTID == getIntValue(reviewObj.get("hostid"))) {
				return getIntValue(reviewObj.get(parameter));
			}
		}
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * This method checks if there is new product being inserted, an CMSNext API for creating article is fired for that product.
	 * @param productDetail
	 * @param queryObject
	 */
	private DBObject addArticleIfNewProduct(String uniqueID, String brandName, String productName, String category, String pid) {
		
		/*//Returns if it already exists in active MODE, and source is also Same(BuyT).(Just update Specs)
		if(categoryProductListMap.get(category) != null 
				&& categoryProductListMap.get(category).containsKey(uniqueID + "###" + ACTIVE)
				&& BUYT_SOURCE.equals(categoryProductListMap.get(category).get(uniqueID + "###" + ACTIVE).get("source"))) {
			return categoryProductListMap.get(category).get(uniqueID + "###" + ACTIVE);
		}
		
		//if it is already present in INACTIVE MODE, and with same Source.(Just update Specs)
		if(categoryProductListMap.get(category) != null 
				&& categoryProductListMap.get(category).containsKey(uniqueID + "###" + INACTIVE)
				&& BUYT_SOURCE.equals(categoryProductListMap.get(category).get(uniqueID + "###" + INACTIVE).get("source"))) {
			return categoryProductListMap.get(category).get(uniqueID + "###" + INACTIVE);
		}
		
		
		//Donot update if it is already present in ACTIVE MODE, but with different Source.
		//Create New Specs here with INACTIVE MODE, and send notification as it is available now from primary source
		if(categoryProductListMap.get(category) != null 
				&& categoryProductListMap.get(category).containsKey(uniqueID + "###" + ACTIVE)
				&& !BUYT_SOURCE.equals(categoryProductListMap.get(category).get(uniqueID + "###" + ACTIVE).get("source"))) {
		
			return new BasicDBObject("status", INACTIVE).append("msid", categoryProductListMap.get(category).get(uniqueID + "###" + ACTIVE).get("msid"));
		}
		
		//Send Notification, if it is already present in INACTIVE MODE, and with different Source.
		//(Create Specification with Active Mode here, and donot create Article in this case).
		if(categoryProductListMap.get(category) != null 
				&& categoryProductListMap.get(category).containsKey(uniqueID + "###" + INACTIVE)
				&& !BUYT_SOURCE.equals(categoryProductListMap.get(category).get(uniqueID + "###" + INACTIVE).get("source"))) {
		
			return new BasicDBObject("status", ACTIVE).append("msid", categoryProductListMap.get(category).get(uniqueID + "###" + INACTIVE).get("msid"));
		}
		*/
		
		List<DBObject> reviewList = new ArrayList<DBObject>();
		
		//Temporary Check, until we move to completely on New Gadgets System.
		if(category.equals(MOBILES) && oldGadgetsMap.containsKey(uniqueID)) {
			reviewList.add(new BasicDBObject("msid", oldGadgetsMap.get(uniqueID)).append("parentid", 0).append("hostid", GN_HOSTID));
			return new BasicDBObject("status", ACTIVE).append("reviewid", reviewList).append("seourl", oldGadgetSEOMap.get(uniqueID));
		}
		
		//Temporary Check, to manually match BuyT gadgets with Old Gadgets.
		if(category.equals(MOBILES) && manuallyMatchedBuyTGadgets.containsKey(pid)) {
			reviewList.add(new BasicDBObject("msid", manuallyMatchedBuyTGadgets.get(pid)).append("parentid", 0).append("hostid", GN_HOSTID));
			return new BasicDBObject("status", ACTIVE).append("reviewid", reviewList);
		}
		
		//Product Doesn't exists
		logger.info("Going to Add article for Category:{}, Brand: {}, Product:{} with ID:{}", category, brandName, productName, uniqueID);

		int msid;
		//Temporary Check, until we move to laptops/Cameras on New Gadgets System.
		if(category.equals(MOBILES) && MongoRestAPIService.MONGO_REST_API_URL.contains("mra.indiatimes.com"))
			msid = addArticle(brandName, productName, category);
		else
			msid = 0;
		
		reviewList.add(new BasicDBObject("msid", msid).append("parentid", 0).append("hostid", GN_HOSTID));
		return new BasicDBObject("status", ACTIVE).append("reviewid", reviewList);
		
	}
	
	
	/**
	 * This method return SEO Compatible name for the product.
	 * This should also uniquely identifies the product, but for more accuracy, we have  {@link #getUniqueID getUniqueID} method.
	 * 
	 * Replaces '(', ')' and whitespaces to HYPHEN.
	 * Then replaces everything else apart from Digits, Lower/Upper case alphabets, +, and Hyphen(-) to BLANK.
	 * Then replace Multiple Hyphen to Single Hyphen.
	 * Then replace '+' to 'plus' string.
	 * Then delete '-' from first and last character, if present.
	 * 
	 * @param name
	 * @return
	 */
	private static String getUName(String name) {
		//Updated Name
//		StringBuilder uname = new StringBuilder(name.replaceAll("\\("," ").replaceAll("\\)", " ").replaceAll("\\s+","-").replaceAll("-+","-").replaceAll("\\+","plus"));
		StringBuilder uname = new StringBuilder(name.replaceAll("[\\(\\)\\s]", "-").replaceAll("[^\\dA-Za-z+-]", "").replaceAll("-+","-").replaceAll("\\+","plus"));
		if('-' == uname.charAt(0)) {
			uname.deleteCharAt(0);	
		} 
		if('-' == uname.charAt(uname.length()-1)) {
			uname.deleteCharAt(uname.length()-1);
		}

		return uname.toString();
	}
	
	
	/**
	 * This method identifies a gadget with a unique String.
	 * We keep only Digits and Alphabets as valid character to Uniquely identify a gadget.
	 * '+' is also considered as there may be two different gadgets with 'plus' appended in its name.
	 * e.g. 'HTC One X' and 'HTC One X+'
	 * 
	 * As check in current Data with BuyT feeds, Below are the special characters present in gadget name:
	 * .@/':,&;?*#_\"
	 * 
	 * @param name
	 * @return
	 */
	private static String getUniqueID(String name) {
		//Unique ID to identify a gadget
		return name.replaceAll("[^\\dA-Za-z+]", "").replaceAll("\\+","plus").toLowerCase();
//		StringBuilder uname = new StringBuilder(name.replaceAll("\\("," ").replaceAll("\\)", " ").replaceAll("\\s+","-").replaceAll("-+","-").replaceAll("\\+","plus"));
//		return uname.toString().replace("-", "").toLowerCase();
	}
	
	
	private int addArticle(String brandName, String productName, String category) {
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("brand", brandName));
		urlParameters.add(new BasicNameValuePair("product", productName));
		urlParameters.add(new BasicNameValuePair("category", category));
		
		try {
			String extData = ExternalHttpUtil.getExtData_PostCall("http://toidev.indiatimes.com/tech-gadget/addreview/", urlParameters);
			logger.info("MSID Generated:{}", extData);
			return Integer.parseInt(extData);
		} catch (HttpException | IOException e) {
			logger.error("Error occured while adding article for Brand:{}, Product:{}", brandName, productName, e);
		}
		
		return 0;
		
	/*	try {
//			String strURL = "http://api.cmslitedev.indiatimes.com:8080/cmslite/api/workfolder/publish";
			String strURL = "http://denmark.timesinternet.in/cmslite/api/workfolder/publish";
			JSONObject json = new JSONObject();
			json.put("type", "2-0");
			json.put("ssoToken", getSSOToken());
			json.put("name", productName);
			json.put("story", productName);
			json.put("parents", new JSONArray().put(new JSONObject().put("parentId", getParentId(category, brandName)).put("hostId", 83)));
			json.put("activationStatus", "ACTIVE");
			json.put("entityType", "ARTICLE");
			json.put("persistableLiteMetadataMap", new JSONObject().put("436", new JSONObject().put("metatype", 436).put("value", "0").put("dataType", "CHARACTER"))
																.put("434", new JSONObject().put("metatype", 434).put("value", uName).put("dataType", "STRING")));
		
			String extData = ExternalHttpUtil.getExtData_PostCall(strURL, json);
			if(extData != null) {
				JSONObject extJson = new JSONObject(extData);
				return (Integer) extJson.get("msId");
			} else {
				logger.error("No data Found while adding article for Brand:{}, Product:{}", brandName, productName);
			}
		} catch (Exception e) {
			logger.error("Error occured while adding article for Brand:{}, Product:{}", brandName, productName, e);
		}
		
		return 0;*/
	}
	
	/*private int getParentId(String category, String brandName) {
		if(category.equals(MOBILES)) {
			if(mobBrandMsidMap.containsKey(brandName))
				return mobBrandMsidMap.get(brandName);
			
			return MISSING_MOBILE_BRAND_MSID; //'Missing Brands' in Mobiles
		}
		
		else if(category.equals(LAPTOPS)) {
			if(laptopBrandMsidMap.containsKey(brandName))
				return laptopBrandMsidMap.get(brandName);
			
			return MISSING_LAPTOP_BRAND_MSID; //'Missing Brands' in Laptops
		}
		
		else if(category.equals(CAMERAS)) {
			if(cameraBrandMsidMap.containsKey(brandName))
				return cameraBrandMsidMap.get(brandName);
			
			return MISSING_CAMERA_BRAND_MSID; //'Missing Brands' in Cameras
		}
		
		return MISSING_BRAND_LIST_MSID; //'Missing Brands' Overall for New category
	}

	private String getSSOToken() throws HttpException, IOException {
		if(ssoToken != null)
			return ssoToken;
		
		String user = "techgadgetuser@gmail.com";
		String pass = "$TechGadget4$";
		String ssoAPIURL = "http://jsso.indiatimes.com/sso/crossdomain/genericLogin?responsetype=json&siteId=32c05a0c7818edb2451260f7c7fac46c&login=" + user + "&passwd=" + pass;
		String ssoData = ExternalHttpUtil.getExtDataForSolar(ssoAPIURL);
		if(ssoData != null) {
			JSONObject ssoJson = new JSONObject(ssoData);
			ssoToken = (String) ssoJson.get("ticketId");
			return ssoToken;
		}
		
		return null;
	}*/
	
	
	public TechGadgetProductDAO getGadgetDAO() {
		return gadgetDAO;
	}

	public void setGadgetDAO(TechGadgetProductDAO gadgetDAO) {
		this.gadgetDAO = gadgetDAO;
	}

//	public static void main(String[] args) {
//		TechGadgetProductWritter writer = new TechGadgetProductWritter();
//		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
//		urlParameters.add(new BasicNameValuePair("brand", "XOLO"));
//		urlParameters.add(new BasicNameValuePair("product", "XOLO Note 3"));
//		urlParameters.add(new BasicNameValuePair("category", "1"));
//		
//		try {
//			String extData = ExternalHttpUtil.getExtData_PostCall("http://toistg.indiatimes.com/tech-gadget/addreview", urlParameters);
//			System.out.println(extData);
//		} catch (HttpException | IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println(writer.addArticle(null, "XOLO", "XOLO Note 3", "XOLO-Note-3", MOBILES));
//	}
	
	
	private int getIntValue(Object obj) {
		try {
			if(obj != null && !"".equals(obj) && !"null".equalsIgnoreCase(String.valueOf(obj)))
				return Integer.parseInt(String.valueOf(obj));
		} catch (Exception e) {
			logger.error("Error Reading integer value:{}", obj);
		}
		return 0;
	}
	
	public void init() {
		logger.info("Going to initialize Old Gadget List..");
		List<Map<String, Object>> oldGadgetLst = gadgetDAO.getOldGadgetsList();
//		Map<String, String> tempMap = new HashMap<String, String>();
		if(oldGadgetLst != null && !oldGadgetLst.isEmpty()) {
			for(Map<String, Object> gadget : oldGadgetLst) {
				oldGadgetsMap.put(getUniqueID(gadget.get("brand") + " " + gadget.get("label")), Integer.parseInt(String.valueOf(gadget.get("msid"))));
				//Need to remove below line if we are finding extra old gadgets not present in primary source.
//				oldGadgetsMap.put(getUniqueID(String.valueOf(gadget.get("label"))), Integer.parseInt(String.valueOf(gadget.get("msid"))));
				
				oldGadgetSEOMap.put(getUniqueID(gadget.get("brand") + " " + gadget.get("label")), String.valueOf(gadget.get("model")));
//				oldGadgetSEOMap.put(getUniqueID(String.valueOf(gadget.get("label"))), String.valueOf(gadget.get("model")));
				
//				tempMap.put(getUniqueID(gadget.get("brand") + " " + gadget.get("label")), gadget.get("brand") + " " + gadget.get("label"));
			}
		}
		
		/*logger.info("Going to initialize Brand Sections List..");
		List<Map<String, Object>> categoryBrandLst = gadgetDAO.getBrandFoldersList(MOBILE_BRAND_LIST_MSID);
		if(categoryBrandLst != null && !categoryBrandLst.isEmpty()) {
			for(Map<String, Object> gadget : categoryBrandLst) {
				mobBrandMsidMap.put(String.valueOf(gadget.get("msname")), Integer.parseInt(String.valueOf(gadget.get("msid"))));
			}
		}
		
		categoryBrandLst = gadgetDAO.getBrandFoldersList(LAPTOP_BRAND_LIST_MSID);
		if(categoryBrandLst != null && !categoryBrandLst.isEmpty()) {
			for(Map<String, Object> gadget : categoryBrandLst) {
				laptopBrandMsidMap.put(String.valueOf(gadget.get("msname")), Integer.parseInt(String.valueOf(gadget.get("msid"))));
			}
		}
		
		categoryBrandLst = gadgetDAO.getBrandFoldersList(CAMERA_BRAND_LIST_MSID);
		if(categoryBrandLst != null && !categoryBrandLst.isEmpty()) {
			for(Map<String, Object> gadget : categoryBrandLst) {
				cameraBrandMsidMap.put(String.valueOf(gadget.get("msname")), Integer.parseInt(String.valueOf(gadget.get("msid"))));
			}
		}*/
		
		
		//Initialize the Complete BuyT Data for Each Category
		//http://mra.indiatimes.com/mra/get/product_detail?query={category:1, source:"BUYT"}&key={uniqueid:1,_id:0}
		logger.info("Going to initialize All Existing Mongo Products Data..");
		
		Map<String, Object> keyMap = new HashMap<String, Object>();
		keyMap.put("key", "{name:1,uniqueid:1,reviewid:1,status:1,source:1,seourl:1,price:1,_id:0}");
		
		List<DBObject> productList =  MongoRestAPIService.getList(PRODUCT_DETAIL, 
				new BasicDBObject("category", MOBILES), keyMap);
		if(productList != null) {
			
//			Map<String, DBObject> productIdMap = productList.stream().collect(Collectors.toMap(
//					dbObject -> String.valueOf(dbObject.get("uniqueid")) + "###" + String.valueOf(dbObject.get("source")), Function.identity(), (a, b) -> a));
			
//			categoryProductListMap.put(MOBILES, productIdMap);
			
			categoryProductList4UniqueIDMap.put(MOBILES, productList.stream().collect(
					Collectors.groupingBy(dbObject -> String.valueOf(dbObject.get("uniqueid")))));
			
		}
		
		
		productList =  MongoRestAPIService.getList(PRODUCT_DETAIL, 
				new BasicDBObject("category", TABLETS), keyMap);
		if(productList != null) {
//			Map<String, DBObject> productIdMap = productList.stream().collect(Collectors.toMap(
//					dbObject -> String.valueOf(dbObject.get("uniqueid")) + "###" + String.valueOf(dbObject.get("source")), Function.identity(), (a, b) -> a));
//			
//			categoryProductListMap.put(TABLETS, productIdMap);
			
			categoryProductList4UniqueIDMap.put(TABLETS, productList.stream().collect(
					Collectors.groupingBy(dbObject -> String.valueOf(dbObject.get("uniqueid")))));
		}
		
		
		productList =  MongoRestAPIService.getList(PRODUCT_DETAIL, 
				new BasicDBObject("category", LAPTOPS), keyMap);
		if(productList != null) {
//			Map<String, DBObject> productIdMap = productList.stream().collect(Collectors.toMap(
//					dbObject -> String.valueOf(dbObject.get("uniqueid")) + "###" + String.valueOf(dbObject.get("source")), Function.identity(), (a, b) -> a));
//			
//			categoryProductListMap.put(LAPTOPS, productIdMap);
			
			categoryProductList4UniqueIDMap.put(LAPTOPS, productList.stream().collect(
					Collectors.groupingBy(dbObject -> String.valueOf(dbObject.get("uniqueid")))));
		}
		
		
		productList =  MongoRestAPIService.getList(PRODUCT_DETAIL, 
				new BasicDBObject("category", CAMERAS), keyMap);
		if(productList != null) {
//			Map<String, DBObject> productIdMap = productList.stream().collect(Collectors.toMap(
//					dbObject -> String.valueOf(dbObject.get("uniqueid")) + "###" + String.valueOf(dbObject.get("source")), Function.identity(), (a, b) -> a));
//			
//			categoryProductListMap.put(CAMERAS, productIdMap);
			
			categoryProductList4UniqueIDMap.put(CAMERAS, productList.stream().collect(
					Collectors.groupingBy(dbObject -> String.valueOf(dbObject.get("uniqueid")))));
		}
		
		//Initialize Old Collection to get the MSID From there..
//		keyMap = new HashMap<String, Object>();
//		keyMap.put("key", "{uniqueid:1,msid:1,_id:0}");
//		productList =  MongoRestAPIService.getList(PRODUCT_DETAIL_OLD, 
//				new BasicDBObject("category", MOBILES).append("msid", new BasicDBObject("$gt", 0)), keyMap);
//		
//		oldGadgetsMap.putAll(
//				productList.stream().collect(Collectors.toMap(
//						dbObject -> String.valueOf(dbObject.get("uniqueid")), dbObject -> Integer.parseInt(String.valueOf(dbObject.get("msid"))), (a, b) -> a)));
		
		
		
		List<DBObject> brandList =  MongoRestAPIService.getList(BRAND_DETAIL, new BasicDBObject());
		if(brandList != null) {
			categoryBrandListMap = brandList.stream().collect(
					Collectors.toMap(dbObject -> String.valueOf(dbObject.get("uniqueid")), Function.identity(), (a, b) -> a));

			
//			Map<String, DBObject> brandIdMap = new HashMap<String, DBObject>();
//			for(DBObject dbObject : brandList) {
//				if(categoryBrandListMap.containsKey(Integer.parseInt(String.valueOf(dbObject.get("category"))))) {
//					categoryBrandListMap.get(Integer.parseInt(String.valueOf(dbObject.get("category")))).put(String.valueOf(dbObject.get("uniqueid")), dbObject);
//				}
//				else {
//					brandIdMap = new HashMap<String, DBObject>();
//					brandIdMap.put(String.valueOf(dbObject.get("uniqueid")), dbObject);
//					categoryBrandListMap.put(Integer.parseInt(String.valueOf(dbObject.get("category"))), brandIdMap);
//				}
//			}
		}
		
		
		logger.info("Going to initialize ManuallyMatchedBuyTGadgetsMap..");
		initializeManuallyMatchedBuyTGadgetsMap();
		

		//Temp Code to print Extra Gadgets, Delete it later
		/*for(String uniqueID : oldGadgetsMap.keySet()) {
			if(categoryProductListMap.get(MOBILES).containsKey(uniqueID + "###2")) {
//				System.out.println("Matched");
			}
			else if(manuallyMatchedBuyTGadgets.values().contains(oldGadgetsMap.get(uniqueID))) {
//				System.out.println("Matched");
			}
			else {
				System.out.println("UnMatched - " + uniqueID + "\tBrand: " + tempMap.get(uniqueID) +  "\tMSID: " + oldGadgetsMap.get(uniqueID));
			}
		}*/
		
	}
	

	private void initializeManuallyMatchedBuyTGadgetsMap() {
		manuallyMatchedBuyTGadgets.put("buyt3012761" , 48252923);
		manuallyMatchedBuyTGadgets.put("buyt750838" , 46367445);
		manuallyMatchedBuyTGadgets.put("buyt1477447" , 49691082);
		manuallyMatchedBuyTGadgets.put("buyt4122340" , 49163643);
		manuallyMatchedBuyTGadgets.put("buyt4845714" , 49892754);
		manuallyMatchedBuyTGadgets.put("buyt8578499" , 37825350);
		manuallyMatchedBuyTGadgets.put("buyt7419319" , 46713726);
		manuallyMatchedBuyTGadgets.put("buyt1228442" , 47452086);
		manuallyMatchedBuyTGadgets.put("buyt1027068" , 31028593);
		manuallyMatchedBuyTGadgets.put("buyt9479338" , 48003209);
		manuallyMatchedBuyTGadgets.put("buyt2962214" , 48472385);
		manuallyMatchedBuyTGadgets.put("buyt1871361" , 47721989);
		manuallyMatchedBuyTGadgets.put("buyt4375944" , 48342383);
		manuallyMatchedBuyTGadgets.put("buyt878298454" , 48746588);
		manuallyMatchedBuyTGadgets.put("buyt2468588" , 46624051);
		manuallyMatchedBuyTGadgets.put("buyt8012652" , 46264146);
		manuallyMatchedBuyTGadgets.put("buyt7853238" , 48330616);
		manuallyMatchedBuyTGadgets.put("buyt9962383" , 48743766);
		manuallyMatchedBuyTGadgets.put("buyt8597137" , 46130840);
		manuallyMatchedBuyTGadgets.put("buyt2311304" , 47369955);
		manuallyMatchedBuyTGadgets.put("buyt8602738" , 47747041);
		manuallyMatchedBuyTGadgets.put("buyt4474327" , 48362385);
		manuallyMatchedBuyTGadgets.put("buyt7527033" , 16411370);
		manuallyMatchedBuyTGadgets.put("buyt7644246" , 47385339);
		manuallyMatchedBuyTGadgets.put("buyt5807979" , 42876696);
		manuallyMatchedBuyTGadgets.put("buyt4092361" , 49921519);
		manuallyMatchedBuyTGadgets.put("buyt5114182" , 48122785);
		manuallyMatchedBuyTGadgets.put("buyt2781501" , 49563447);
		manuallyMatchedBuyTGadgets.put("buyt7294154" , 50528797);
		manuallyMatchedBuyTGadgets.put("buyt6227556" , 49078106);
		manuallyMatchedBuyTGadgets.put("buyt4035849" , 14119539);
		manuallyMatchedBuyTGadgets.put("buyt8318894" , 50561187);
		manuallyMatchedBuyTGadgets.put("buyt1641101" , 49631066);
		manuallyMatchedBuyTGadgets.put("buyt5003354" , 50151490);
		manuallyMatchedBuyTGadgets.put("buyt2584084" , 31045930);
		manuallyMatchedBuyTGadgets.put("buyt7375619" , 50109863);
		manuallyMatchedBuyTGadgets.put("buyt698050452" , 47865437);
		manuallyMatchedBuyTGadgets.put("buyt4084609" , 47785781);
		manuallyMatchedBuyTGadgets.put("buyt6896450" , 48359235);
		manuallyMatchedBuyTGadgets.put("buyt870039145" , 47875730);
		manuallyMatchedBuyTGadgets.put("buyt1158510" , 47470909);
		manuallyMatchedBuyTGadgets.put("buyt735308" , 48698261);
		manuallyMatchedBuyTGadgets.put("buyt9502025" , 47516452);
		manuallyMatchedBuyTGadgets.put("buyt1927016" , 48100744);
		manuallyMatchedBuyTGadgets.put("buyt8545025" , 47675615);
		manuallyMatchedBuyTGadgets.put("buyt5214750" , 46630800);
		manuallyMatchedBuyTGadgets.put("buyt7179535" , 47014201);
		manuallyMatchedBuyTGadgets.put("buyt5485379" , 50038904);
		manuallyMatchedBuyTGadgets.put("buyt2800077" , 47085090);
		manuallyMatchedBuyTGadgets.put("buyt1446875" , 49644562);
		manuallyMatchedBuyTGadgets.put("buyt9629118" , 46053662);
		manuallyMatchedBuyTGadgets.put("buyt3526352" , 48544179);
		manuallyMatchedBuyTGadgets.put("buyt7898357" , 47372296);
		manuallyMatchedBuyTGadgets.put("buyt3813820" , 46262759);
		manuallyMatchedBuyTGadgets.put("buyt4583452" , 49583498);
		manuallyMatchedBuyTGadgets.put("buyt9548244" , 49816350);
		manuallyMatchedBuyTGadgets.put("buyt9884147" , 49104421);
		manuallyMatchedBuyTGadgets.put("buyt3423304" , 46865539);
		manuallyMatchedBuyTGadgets.put("buyt500000" , 47910642);
		manuallyMatchedBuyTGadgets.put("buyt4268511" , 49768039);
		manuallyMatchedBuyTGadgets.put("buyt3613107" , 46609982);
		manuallyMatchedBuyTGadgets.put("buyt5355712" , 49468783);
		manuallyMatchedBuyTGadgets.put("buyt4344650" , 49271521);
		manuallyMatchedBuyTGadgets.put("buyt5685340" , 48108787);
		manuallyMatchedBuyTGadgets.put("buyt3064623" , 46198850);
		manuallyMatchedBuyTGadgets.put("buyt6094769" , 47095943);
		manuallyMatchedBuyTGadgets.put("buyt401736" , 47368301);
		manuallyMatchedBuyTGadgets.put("buyt8142770" , 47151230);
		manuallyMatchedBuyTGadgets.put("buyt1182858" , 48168749);
		manuallyMatchedBuyTGadgets.put("buyt3762590" , 49214936);
		manuallyMatchedBuyTGadgets.put("buyt1228794" , 49469963);
		manuallyMatchedBuyTGadgets.put("buyt6632603" , 48264789);
		manuallyMatchedBuyTGadgets.put("buyt2053242" , 46921340);
		manuallyMatchedBuyTGadgets.put("buyt7331268" , 47429542);
		manuallyMatchedBuyTGadgets.put("buyt8585731" , 48901667);
		manuallyMatchedBuyTGadgets.put("buyt2864096" , 46621306);
		manuallyMatchedBuyTGadgets.put("buyt2497143" , 49072537);
		manuallyMatchedBuyTGadgets.put("buyt8698381" , 47354558);
		manuallyMatchedBuyTGadgets.put("buyt7485329" , 47247556);
		manuallyMatchedBuyTGadgets.put("buyt7450624" , 47639205);
		manuallyMatchedBuyTGadgets.put("buyt9244359" , 49056955);
		manuallyMatchedBuyTGadgets.put("buyt3499507" , 26561861);
		manuallyMatchedBuyTGadgets.put("buyt3102422" , 44781111);
		manuallyMatchedBuyTGadgets.put("buyt7643546" , 48777014);
		manuallyMatchedBuyTGadgets.put("buyt1347176" , 45501553);
		manuallyMatchedBuyTGadgets.put("buyt7486240" , 49229304);
		manuallyMatchedBuyTGadgets.put("buyt3533417" , 46422033);
		manuallyMatchedBuyTGadgets.put("buyt4781640" , 50558862);
		manuallyMatchedBuyTGadgets.put("buyt6612037" , 47815444);
		manuallyMatchedBuyTGadgets.put("buyt1850908" , 26345872);
		manuallyMatchedBuyTGadgets.put("buyt4039380" , 20745832);
		manuallyMatchedBuyTGadgets.put("buyt9580477" , 20140081);
		manuallyMatchedBuyTGadgets.put("buyt558724862" , 48254976);
		manuallyMatchedBuyTGadgets.put("buyt1155574" , 48369510);
		manuallyMatchedBuyTGadgets.put("buyt3831460" , 49090476);
		manuallyMatchedBuyTGadgets.put("buyt3739020" , 50168562);
		manuallyMatchedBuyTGadgets.put("buyt9520258" , 46999053);
		manuallyMatchedBuyTGadgets.put("buyt7415772" , 49847739);
		manuallyMatchedBuyTGadgets.put("buyt1905884" , 47840961);
		manuallyMatchedBuyTGadgets.put("buyt5604585" , 48543042);
		manuallyMatchedBuyTGadgets.put("buyt2194452" , 48698245);
		manuallyMatchedBuyTGadgets.put("buyt8869540" , 49982161);
		manuallyMatchedBuyTGadgets.put("buyt7391835" , 24668626);
		manuallyMatchedBuyTGadgets.put("buyt3489803" , 46444412);
		manuallyMatchedBuyTGadgets.put("buyt6364752" , 49255825);
		manuallyMatchedBuyTGadgets.put("buyt7553252" , 46838738);
		manuallyMatchedBuyTGadgets.put("buyt5589237" , 50437761);
		manuallyMatchedBuyTGadgets.put("buyt5129926" , 47910129);
		manuallyMatchedBuyTGadgets.put("buyt7592754" , 47830860);
		manuallyMatchedBuyTGadgets.put("buyt3564501" , 50450141);
		manuallyMatchedBuyTGadgets.put("buyt1956150" , 50079193);
		manuallyMatchedBuyTGadgets.put("buyt760647564" , 48357712);
		manuallyMatchedBuyTGadgets.put("buyt8814804" , 49246570);
		manuallyMatchedBuyTGadgets.put("buyt4196037" , 48375092);
		manuallyMatchedBuyTGadgets.put("buyt9943164" , 49072825);
		manuallyMatchedBuyTGadgets.put("buyt8785760" , 49815537);
	}

	public static void main(String[] args) throws ParseException {
		
		categoryMap.put(1, MOBILES);
		categoryMap.put(2, TABLETS);
		categoryMap.put(3, LAPTOPS);
		categoryMap.put(4, CAMERAS);
		
		Map<String, Object> keyMap = new HashMap<String, Object>();
		/*keyMap.put("key", "{DT:1,category:1,insertDT:1}");
		
		
		List<DBObject> brandList = MongoRestAPIService.getList(BRAND_DETAIL, new BasicDBObject(), keyMap);
		if(brandList != null & brandList.size() > 0) {
			DBObject temp;
			for(DBObject obj : brandList) {
				temp = new BasicDBObject();
				temp.put("_id", obj.get("_id"));
				MongoRestAPIService.update(BRAND_DETAIL, temp, new BasicDBObject().append("$set", new BasicDBObject("updatedAt", obj.get("DT"))
				.append("insertedAt", obj.get("insertDT"))
				.append("category", convertCategory((ArrayList<Integer>)obj.get("category")))
				));
				
				
				MongoRestAPIService.update(BRAND_DETAIL, temp, new BasicDBObject().append("$unset", new BasicDBObject("DT", "").append("insertDT", "")));
			}
		}*/
		
		
		
		
		keyMap.put("key", "{announced:1}");
		Map<Object, Object> map = new HashMap<Object, Object>();
//		List<DBObject> list = MongoRestAPIService.getList(PRODUCT_DETAIL, new BasicDBObject(), keyMap);
//		List<DBObject> list = MongoRestAPIService.getList(PRODUCT_DETAIL, new BasicDBObject("source", "BATCH"), keyMap);
		List<DBObject> list = MongoRestAPIService.getList(PRODUCT_DETAIL, new BasicDBObject().append("announced", new BasicDBObject("$type", 2)), keyMap);
		if(list != null && list.size()>0) {
			DBObject temp;
			for(DBObject obj :list) {
				
				temp = new BasicDBObject();
				temp.put("_id", obj.get("_id"));
				
//				DecimalFormat df = new DecimalFormat("0.###E0");
//				Number a = df.parse(String.valueOf(obj.get("msid")));
//				System.out.println(a);
				
//				List<DBObject> reviewList = new ArrayList<DBObject>();
//				reviewList.add(new BasicDBObject("msid", obj.get("msid")).append("hostid", TOI_HOSTID));
//				
//				
//				MongoRestAPIService.update(PRODUCT_DETAIL, temp, new BasicDBObject().append("$set", new BasicDBObject("updatedAt", obj.get("DT"))
//				.append("insertedAt", obj.get("insertDT"))
//				.append("category", categoryMap.get(Integer.valueOf(obj.get("category")+"")))
//				.append("image", new String[]{obj.get("image")+""})
//				.append("reviewid", reviewList)));
				
//				MongoRestAPIService.delete(PRODUCT_DETAIL, temp);
				MongoRestAPIService.update(PRODUCT_DETAIL, temp, new BasicDBObject().append("$set", new BasicDBObject("announced", GadgetUtil.getProperDateFormat(obj.get("announced")))));
//				MongoRestAPIService.update(PRODUCT_DETAIL, temp, new BasicDBObject().append("$set", new BasicDBObject("msid", "NumberInt("+a.intValue()+")")));
//			}
//				new BasicDBObject().append("$addToSet", new BasicDBObject("categorynew", Integer.parseInt(String.valueOf(obj.get("category")))))
			}
		}
		
		
//		insertBrand("General Mobile");
//		insertBrand("LeEco");
//		insertBrand("Obi Worldphone");
//		insertBrand("Polaroid");
//		insertBrand("SilentCircle");
//		insertBrand("Vernee");
//		insertBrand("Viaan");
//		insertBrand("Viao");
		
//		String[] strArry = new String[]{"Aqua Mobiles", "BQ", "Elephone", "Google", "Kestrel", "Mitashi", "Phicomm", "UMI", "Zopo"};
		/*String[] strArry = new String[]{"Phicomm"};
		List<DBObject> newBrandLst = MongoRestAPIService.getList(BRAND_DETAIL, new BasicDBObject("name", new BasicDBObject("$in", strArry)));
		
		MongoRestAPIService.MONGO_REST_API_URL = "http://mra.indiatimes.com/mra/";
		
		for(DBObject dbj : newBrandLst)
			MongoRestAPIService.insertWithPost(BRAND_DETAIL, dbj);*/
		
		
	}
	
	private static List<String> convertCategory(ArrayList<Integer> arrayList) {
		List<String> lst = new ArrayList<String>();
		
		if(arrayList != null && arrayList.size() > 0) {
			for(Integer a : arrayList) {
				lst.add(categoryMap.get(a));
			}
		}
		
		return lst;
	}
	
	private static void insertBrand(String brandName) {
		DBObject dbObject = new BasicDBObject();
		dbObject.put("name", brandName);
		String uniqueID = getUniqueID(brandName);
		dbObject.put("uniqueid", uniqueID);//UNIQUE ID, on which brand will be identified as Unique
		
		dbObject.put("uname", getUName(brandName));
		dbObject.put("image", "");
		
		List<String> categoryLst = new ArrayList<String>(1);
		categoryLst.add("mobile");
		dbObject.put("category", categoryLst);
		
		dbObject.put("msid", 0);
		dbObject.put("updatedAt", new Date());
		
//		if(PRIMARY_SOURCE.equals(productDetail.getSource()))
			dbObject.put("status", ACTIVE);
//		else{
//			//Also Send Alert here.
//			dbObject.put("status", PENDING);
//			InMemoryData.brandAlertList.add(dbObject);
//		}
		
		
		DBObject insertDateObject = new BasicDBObject();
		insertDateObject.put("insertedAt", new Date());
		
		DBObject queryObject = new BasicDBObject();
		queryObject.put("uniqueid", uniqueID);
		
		DBObject finalObject = new BasicDBObject().append("$setOnInsert", insertDateObject).append("$set", dbObject);
		MongoRestAPIService.updateWithPost(BRAND_DETAIL, queryObject, finalObject, true);
	}

	private static Map<Integer, String> categoryMap = new HashMap<Integer, String>();
	
}
