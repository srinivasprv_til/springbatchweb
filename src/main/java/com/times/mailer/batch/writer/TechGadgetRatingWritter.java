package com.times.mailer.batch.writer;

import java.util.ArrayList;
import java.util.List;

import org.bson.BasicBSONObject;
import org.bson.types.BasicBSONList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;


//import com.indiatimes.cms.data.service.MongoRestAPIService;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.Constant;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;

public class TechGadgetRatingWritter implements ItemWriter<DBObject>{
	
private static final Logger logger = LoggerFactory.getLogger(TechGadgetRatingWritter.class);
	
	@Override
	public void write(List<? extends DBObject> items) throws Exception {
		try {
			if (items != null && items.size() > 0) {
				for (DBObject object : items) {
					
					Number msid = 0;
					Number parentId = 0;
					BasicDBList reviewList = (BasicDBList) object.get(Constant.PRODUCT_DETAIL_REVIEW_ID);
					
					if(reviewList != null && reviewList.size() > 0){
						parentId = (Number) ((BasicBSONObject) reviewList.get(0)).get(Constant.PRODUCT_DETAIL_PARENT_ID);
						msid = (Number) ((BasicBSONObject) reviewList.get(0)).get(Constant.PRODUCT_DETAIL_MSID);
					}
					
					List<DBObject> ratingList = getUserRating(msid);
					if(parentId != null && (ratingList == null || ratingList.size() ==0)){
						// Get rating from parent
						ratingList = getUserRating(parentId);
					}
					updateUserRating(object, ratingList);
				}
			}
		} catch (Exception e) {
			logger.error("Exception while inserting rating for TechGadget productDetail in Mongo DB: "+ e);
			new CMSCallExceptions("Exception caught while Inserting rating data to DB: "  + " msg: " + e.getMessage(),
									CMSExceptionConstants.CMS_MongoImage_Exception, e);
		}
	}
	
	private void updateUserRating(DBObject object, List<DBObject> ratingList) {

		try{
			
			if(ratingList == null || ratingList.size() == 0){
				return;
			}
			DBObject[] array = new DBObject[ratingList.size()];

			double allRating = 0;
			double allCount = 0;
			double average = 0;
			double criticRating = 0;
			
			int i = 0;
			for(DBObject ob: ratingList){

				double rating = (double) ((DBObject)ob.get("_id")).get("rating");
				int count = (int) ob.get("count");

				DBObject json = new BasicDBObject();
				json.put("rating", rating);
				json.put("count", count);

				allRating = allRating + (rating*count);
				allCount = allCount + count;

				array[i++] = json;
				
				BasicDBList list = (BasicDBList) ob.get("criticRating");
				criticRating = Double.parseDouble((String) list.get(0));
			}
			average = allRating/allCount;

			DBObject setObject = new BasicDBObject();
			setObject.put("ratings", array);
			setObject.put("averageRating", average);
			setObject.put("criticRating", criticRating);

			String uname = (String) object.get("uname");
			DBObject queryJson = new BasicDBObject("uname", uname).append("status", 2);

			BasicDBObject updateObject = new BasicDBObject();
			updateObject.append("$set", setObject);
			
			MongoRestAPIService.update(Constant.PRODUCT_DETAIL_COLLECTION, queryJson, updateObject, false);
			
			logger.warn("Updated rating for uname: "+uname);
		}catch(Exception e){
			logger.error("Error while updating rating on product detail tech gadget: "+ e);
		}
	}

	private List<DBObject> getUserRating(Number msid) throws Exception{
		
		DBObject matchQuery = new BasicDBObject();		
		matchQuery.put("$match", new BasicDBObject("msid", msid));
		
		DBObject filterObject = new BasicDBObject();
		filterObject.put("msid", "$msid");
		filterObject.put("rating", "$U_R");
		
		DBObject groupQuery = new BasicDBObject("$group", new BasicDBObject("_id", filterObject).
												append("count", new BasicDBObject("$sum", 1)).
												append("criticRating",new BasicDBObject("$addToSet", "$C_R")));
		
		DBObject filterObject1 = new BasicDBObject("$sort", new BasicDBObject("count", 1));
		
		List<DBObject> arr1 = new ArrayList<DBObject>();
		arr1.add(matchQuery);arr1.add(groupQuery);arr1.add(filterObject1);
		
		logger.warn("Got rating for msid: "+ msid);
		
		return MongoRestAPIService.aggregateList(Constant.MOVIE_RATING_COLLECTION, arr1);
	}

}
