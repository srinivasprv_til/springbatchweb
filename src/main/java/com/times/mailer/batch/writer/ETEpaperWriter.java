package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.model.ETEpaperArticle;
import com.times.common.util.InMemoryData;

/**
 * 
 * @author praveen.bokka
 *
 */
public class ETEpaperWriter implements ItemWriter<List<DBObject>> {
	
	private static final Log log = LogFactory.getLog(ETEpaperWriter.class);
	
//	@Autowired
//	private String solrLocation;
//	
//	public String getSolrLocation() {
//		return solrLocation;
//	}
//
//	public void setSolrLocation(String solrLocation) {
//		this.solrLocation = solrLocation;
//	}

	public void write(List<? extends List<DBObject>> item) throws Exception {
		log.debug("etEpaper write method start..");
		
		List<DBObject> dbList = null;
		if(item != null) {
			dbList = item.get(0);
		}
		
		if(dbList != null){
			//try inserting into mongo
			//if fails do not update status
			try {
				for(DBObject document : dbList){
					DBObject query = new BasicDBObject();
					query.put("msid", document.get("msid"));
					query.put("epaperId", document.get("epaperId"));
					query.put("pubid", document.get("pubid"));
					log.debug("Inserting msid: "+document.get("msid"));
	//				DBObject existingDoc = MongoRestAPIService.get("ETEpaper", query);
	//				if(existingDoc != null) {
	//					List<DBObject> epaperArray =(List<DBObject>) existingDoc.get("epaperArticles");
	//					List<DBObject> epaperArray2 =(List<DBObject>) document.get("epaperArticles");
	//					for(DBObject epaperArticle: epaperArray) {
	//						if(!epaperArray2.contains(epaperArticle)) {
	//							epaperArray2.add(epaperArticle);
	//						}
	//					}
	//					//epaperArray.addAll(epaperArray2);
	//					document.put("epaperArticles", epaperArray2);
	//				}
					BasicDBObject setObject = new BasicDBObject();
					setObject.append("$set", document);
					MongoRestAPIService.updateWithPost("ETEpaperBackup",query, setObject);
					MongoRestAPIService.updateWithPost("ETEpaper",query, setObject);
					//though status is true every time, if one op fails catch will update status to false
					//atleast one object need to be inserted for status to be true
					InMemoryData.etEpaperSuccess = true;
				}
			} catch (Exception e) {
				InMemoryData.etEpaperSuccess = false;
			}
			//InMemoryData.success_status_ibeat = true;
		}
		
//		HttpSolrClient server = new HttpSolrClient(solrLocation);
//		//HttpSolrClient server = new HttpSolrClient("http://localhost:8983/solr/epaper");
//		
//		server.addBeans(articleList);
//		server.commit();
//		server.close();
		log.debug("etEpaper write method end..");
	}

}
