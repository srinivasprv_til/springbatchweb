package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;

/**
 * 
 * @author praveen.bokka
 *
 */
public class ETEpaperCMSDataWriter implements ItemWriter<List<DBObject>> {
	
	private static final Log log = LogFactory.getLog(ETEpaperCMSDataWriter.class);
	
	public void write(List<? extends List<DBObject>> item) throws Exception {
		log.debug("etEpaperCMSData write method start..");
		List<DBObject> dbList = item.get(0);
		if(dbList != null){
			for(DBObject document : dbList){
				DBObject query = new BasicDBObject();
				query.put("msid", document.get("msid"));
				query.put("epaperId", document.get("epaperId"));
				log.debug("Inserting msid: "+document.get("msid"));
//				DBObject existingDoc = MongoRestAPIService.get("ETEpaper", query);
//				if(existingDoc != null) {
//					List<DBObject> epaperArray =(List<DBObject>) existingDoc.get("epaperArticles");
//					List<DBObject> epaperArray2 =(List<DBObject>) document.get("epaperArticles");
//					for(DBObject epaperArticle: epaperArray) {
//						if(!epaperArray2.contains(epaperArticle)) {
//							epaperArray2.add(epaperArticle);
//						}
//					}
//					//epaperArray.addAll(epaperArray2);
//					document.put("epaperArticles", epaperArray2);
//				}
				BasicDBObject setObject = new BasicDBObject();
				setObject.append("$set", document);
				MongoRestAPIService.updateWithPost("ETEpaper",query, setObject);
				MongoRestAPIService.updateWithPost("ETEpaperBackup",query, setObject);
			}
			//InMemoryData.success_status_ibeat = true;
		}
		
		log.debug("etEpaperCMSData write method end..");
	}
	
//	public static void main(String[] args) {
//		DBObject rowJson= new BasicDBObject();
//		rowJson.put("type", "test");
//		rowJson.put("lastDate", new Date());
//		
//		BasicDBObject newDocument = new BasicDBObject();
//        newDocument.append("$set", rowJson);
//       // MongoRestAPIService.updateWithPost(NPRSS_COLLECTION, finalQuery, newDocument);
//		
//		
//		
//		DBObject queryJson = new BasicDBObject();
//		queryJson.put("type", "test");
//		MongoRestAPIService.updateWithPost("ETEpaperStatus", queryJson, newDocument);
//	}

}
