package com.times.mailer.batch.writer;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.codehaus.jackson.node.ArrayNode;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.jdom.Document;
import org.jdom.Element;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemWriter;

import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.EmailVO;
import com.times.common.util.InMemoryData;
import com.times.common.util.NamedThreadFactory;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.MsidObject;
import com.times.mailer.model.NetstorageInfo;
import com.times.common.model.articleKeyWordDetails;
import com.times.common.service.FileContentServices;

/**
 * 
 * @author Dipali.Patidar
 *
 *
 */
public class ChartbeatTrendingStoriesWriter implements ItemWriter<List<Integer>>  {

	private static final Logger logger = LoggerFactory.getLogger(ChartbeatTrendingStoriesWriter.class);
	
	private final static String cmsentityXml="http://economictimes.indiatimes.com/xml/trendingstories1.cms?upcache=2&";
	
	private NetstorageFileInfoDao netstorageFileInfoDao;
	private FileContentServices fileContentServices;
	private NamedThreadFactory threadfactory;
	ExecutorService service = null;

	public void write(List<? extends List<Integer>> documents) throws Exception {

		logger.debug("Initiating writer");

		Element xmlElement = new Element("trendingstories", "");
		
		if (documents != null && documents.get(0) != null) {
			
			List<Integer> articlesMsidList = documents.get(0);
			
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTimeZone(TimeZone.getTimeZone("IST"));
			cal.setTime(date);
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			int minute = cal.get(Calendar.MINUTE);

			SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			String dateStr = sdf.format(date);
			

			SimpleDateFormat numberFormat = new SimpleDateFormat("yyyyMMdd");
			numberFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			String idateStr = numberFormat.format(date);
			
			int iDate = Integer.valueOf(idateStr);

			DBObject query = new BasicDBObject();
			query.put("DT", dateStr);
			query.put("HR", hour);
			query.put("TP", 100);
			query.put("hostId", 153);
			query.put("campaignId","ETHome");
			
            DBObject setObj = new BasicDBObject();
    		
    		setObj.put("iDT", iDate);
    		
    		setObj.put("intervals."+String.valueOf(minute/10),articlesMsidList);

			DBObject update = new BasicDBObject();
			
			update.put("$set", setObj);
			
			
			//query.put("hostId", InMemoryData.instance.getHostId());
			MongoRestAPIService.update("Timeline", query, update);
			
			
			String msidString = "";
			if(articlesMsidList!=null || !articlesMsidList.isEmpty())
			{    for(int msid:articlesMsidList){
				msidString = msidString + String.valueOf(msid) + ",";
				
			   }
			msidString = msidString.substring(0, msidString.length() - 1);
			
			String finalurl = cmsentityXml+"msid="+msidString;
			
			
			NetstorageInfo storageInfo = new NetstorageInfo();
			storageInfo.setFileName("ChartbeatTrendingStories.htm");
			storageInfo.setFolderLocation("/93656/configspace");
			storageInfo.setServerName("et.upload.akamai.com");
			storageInfo.setUserid("cmsetdeploy");
			storageInfo.setPassword("pE0ple");
			
			FileLocationInfo fileUploadInfo =new FileLocationInfo();
			fileUploadInfo.setSourceUrl(finalurl);
			fileUploadInfo.setNetstorageLocation(storageInfo);
			

			logger.info("Processing url: "+ fileUploadInfo.getSourceUrl());	
			String fileContent = fileContentServices.getContentByHTTP(finalurl);
            if (fileContent != null) {
                 
                  
                  fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
              
                  logger.debug("uploaded size: " + fileContent.length() + " source url : " +finalurl );
                 
                 
            }

	
			}
			
		}

		logger.debug("end of writer");

	}

	public FileContentServices getFileContentServices() {
		return fileContentServices;
	}
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}
	
}

