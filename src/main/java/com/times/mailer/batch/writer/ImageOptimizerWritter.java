package com.times.mailer.batch.writer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.service.FileContentServices;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.dao.ImageOptimizerDao;
import com.times.mailer.model.ImageInfo;

public class ImageOptimizerWritter implements ItemWriter<ImageInfo> {
	
	// -----------------------------------------------------------------------------------------------
	// Start Private members
		
	private static final Logger logger = LoggerFactory.getLogger(ImageOptimizerWritter.class);
	
	private FileContentServices fileContentServices;
	
	private ImageOptimizerDao imageOptimizerDao;
	
	// End Private members
	// -----------------------------------------------------------------------------------------------
	
	
	// -----------------------------------------------------------------------------------------------
	// Start public methods

	@Override
	public void write(List<? extends ImageInfo> items) throws Exception {

		try {

			if (items != null && items.size() > 0) {

				for (ImageInfo imageInfo : items) {

					logger.warn("Writting: "+ items.size());

					if(imageInfo.getMimeType() ==null){

						if(imageInfo.getName() != null && imageInfo.getName().lastIndexOf(".") != -1){

							String type = imageInfo.getName().substring(imageInfo.getName().lastIndexOf(".")+1);

							imageInfo.setMimeType("image/"+type);
						}else{

							/// Default set jpg
							imageInfo.setMimeType("image/jpg");
						}
					}

					this.fileContentServices.getOptimizedImage(imageInfo);

					updateContentInDB(imageInfo);
				}

			}
		} catch (Exception e) {

			logger.error("Exception while updating image optimized data in DB: "+ e.getMessage());
			new CMSCallExceptions("Exception caught while INserting data to DB: " +  " msg: " + e.getMessage(),
					CMSExceptionConstants.CMS_MediaController_Exception, e);
		}
	}
	
	// End public methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start private methods
	
	private void updateContentInDB(ImageInfo imageInfo) {
		
		this.imageOptimizerDao.updateImageData(imageInfo);
	}
	
	// End private methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start getters/setters
	
	public void setFileContentServices(FileContentServices fileContentServices) {
		
		this.fileContentServices = fileContentServices;
	}

	public ImageOptimizerDao getImageOptimizerDao() {
		return imageOptimizerDao;
	}

	public void setImageOptimizerDao(ImageOptimizerDao imageOptimizerDao) {
		this.imageOptimizerDao = imageOptimizerDao;
	}
	
	// End getters/setters
	// -----------------------------------------------------------------------------------------------
}
