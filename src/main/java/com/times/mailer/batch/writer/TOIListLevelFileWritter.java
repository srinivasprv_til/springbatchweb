/**
 * 
 */
package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.service.FileContentServices;
import com.times.common.util.MailerUtil;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.FileLocationInfo;

/**
 * This Writer of the batch is used to update the content to the Akamai Netstorage specified location.
 * 
 * @author Ranjeet.Jha
 *
 */
public class TOIListLevelFileWritter  implements ItemWriter<FileLocationInfo> {
	
	private static final Log log = LogFactory.getLog(TOIListLevelFileWritter.class);
	
	private NetstorageFileInfoDao netstorageFileInfoDao;
	//private EmailProcessor smtpMailer;
	private FileContentServices fileContentServices;
	
	/**
	 * @param netstorageFileInfoDao the netstorageFileInfoDao to set
	 */
	public void setNetstorageFileInfoDao(NetstorageFileInfoDao netstorageFileInfoDao) {
		this.netstorageFileInfoDao = netstorageFileInfoDao;
	}

	@Override
	public void write(List<? extends FileLocationInfo> items) throws Exception {
		log.debug("inside the write method of writter");
		String ftpLocation = null;
		try {
			if (items != null && items.size() > 0) {
				
				FileLocationInfo fileUploadInfo = items.get(0);
				ftpLocation = fileUploadInfo.getNetstorageLocation().getServerName() + fileUploadInfo.getNetstorageLocation().getFolderLocation();
				String sourceUrl = fileUploadInfo.getSourceUrl().trim();
				String fileContent = MailerUtil.getData(sourceUrl);
				//InputStream inputStream = NetstorageUtil.getInputStreamByUrl(sourceUrl);
				fileContentServices.getContentByHTTP(sourceUrl);
				if (fileContent != null) {
					//NetstorageUtil.uploadFileContent(fileUploadInfo.getNetstorageLocation(), fileContent);
					fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
					log.debug("source URL : " + sourceUrl + " target URL: " + ftpLocation);
					
					// update last_updated_at column for record 
					netstorageFileInfoDao.updateFileLocationInfo(fileUploadInfo);
					
				}
			}
		} catch (Exception e) {
			new CMSCallExceptions("Exception caught while ftp update. ftp url: " + ftpLocation + " msg: " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			log.error("exception caught while ftp update. ftp url: " + ftpLocation);
		}
	}

	/**
	 * @return the fileContentServices
	 */
	public FileContentServices getFileContentServices() {
		return fileContentServices;
	}

	/**
	 * @param fileContentServices the fileContentServices to set
	 */
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}

	/**
	 * @param smtpMailer
	 */
	/*public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}*/
	
	/*public static void main(String... args ) {
		List<FileLocationInfo> list = populateFileLocationInfo();
		try {
			for (FileLocationInfo fileLocationInfo : list) {
				
				String fileContent = NetstorageUtil.getData(fileLocationInfo.getSourceUrl());
				if (fileContent != null) {
					NetstorageUtil.uploadFileContent(fileLocationInfo.getNetstorageLocation(), fileContent);
					System.out.println("hi");
					//NetstorageUtil.uploadFileContent(fileLocationInfo.getNetstorageLocation(), fileContent);
					System.out.println("folder : " + fileLocationInfo.getNetstorageLocation().getFolderLocation());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	// TODO : for testing purpose used this method.
		/*private static List<FileLocationInfo> populateFileLocationInfo() {
			List<FileLocationInfo> list = new ArrayList<FileLocationInfo>();
			
			NetstorageInfo akamaiInfo = new NetstorageInfo();
			FileLocationInfo fileLocationInfo = new FileLocationInfo();
			akamaiInfo.setFileName("myindex.html");
			akamaiInfo.setServerName("timescricket.upload.akamai.com");
			akamaiInfo.setFolderLocation("/92522/TOI/city");
			akamaiInfo.setUserid("toiupload");
			akamaiInfo.setPassword("user4upl0ad");
			
			fileLocationInfo.setNetstorageLocation(akamaiInfo);
			try {
				fileLocationInfo.setSourceUrl(new URL("http://timesofindia.indiatimes.com/city").toString());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			fileLocationInfo.setStatus(true);
			list.add(fileLocationInfo);
			//list.add(new FileLocationInfo(2,"http://timesofindia.indiatimes.com/india", 2, akamaiInfo));
			
			return list;
		}*/

}
