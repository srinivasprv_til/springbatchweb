/**
 * 
 */
package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.service.FileContentServices;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.model.FileContentInfo;

/**
 * This Writer of the batch is used to update the content to the Akamai Netstorage specified location.
 * 
 * @author Ranjeet.Jha
 *
 */
public class CVoterContentWritter  implements ItemWriter<FileContentInfo> {
	
	private static final Log log = LogFactory.getLog(CVoterContentWritter.class);
	
	private FileContentServices fileContentServices;
	
	//private EmailProcessor smtpMailer;

	@Override
	public void write(List<? extends FileContentInfo> items) throws Exception {
		//log.debug("inside the write method of writter");
		String ftpLocation = null;
		try {
			if (items != null && items.size() > 0) {
				for (FileContentInfo fileUploadInfo : items) {
					//FileLocationInfo fileUploadInfo = items.get(0);
					ftpLocation = fileUploadInfo.getNetstorageLocation().getServerName() + fileUploadInfo.getNetstorageLocation().getFolderLocation() + "/" + fileUploadInfo.getNetstorageLocation().getFileName();
					String sourceUrl = fileUploadInfo.getSourceUrl().trim();
					//String fileContent = MailerUtil.getData(sourceUrl);
					String fileContent = fileUploadInfo.getContent();
					//InputStream inputStream = NetstorageUtil.getInputStreamByUrl(sourceUrl);
					if (fileContent != null) {
						
						//System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
						fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
						//fileContentServices.writeFileInLocalFileSystem(fileUploadInfo.getNetstorageLocation(), fileContent);
						log.debug("uploaded size: " + fileContent.length() + " source url : " + sourceUrl + " target url: " + ftpLocation);
						
						// upload content to Amazon S3
						fileContentServices.uploadFileAmazonS3(fileUploadInfo.getAmazonS3(), fileContent, null);
						
						// update last_updated_at column for record 
						//netstorageFileInfoDao.updateFileLocationInfo(fileUploadInfo);
						
					}
				}
			}
		} catch (Exception e) {
			new CMSCallExceptions("Exception caught while ftp update. ftp url: " + ftpLocation + " msg: " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			log.error("exception caught while ftp update. ftp url: " + ftpLocation);
		}
	}

	/**
	 * @param fileContentServices the fileContentServices to set
	 */
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}

	/**
	 * @param smtpMailer
	 */
	/*public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}*/
	
	/*public static void main(String... args ) {
		List<FileLocationInfo> list = populateFileLocationInfo();
		try {
			for (FileLocationInfo fileLocationInfo : list) {
				
				String fileContent = NetstorageUtil.getData(fileLocationInfo.getSourceUrl());
				if (fileContent != null) {
					NetstorageUtil.uploadFileContent(fileLocationInfo.getNetstorageLocation(), fileContent);
					System.out.println("hi");
					//NetstorageUtil.uploadFileContent(fileLocationInfo.getNetstorageLocation(), fileContent);
					System.out.println("folder : " + fileLocationInfo.getNetstorageLocation().getFolderLocation());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	

}
