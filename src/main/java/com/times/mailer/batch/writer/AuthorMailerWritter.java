package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.mail.MailContent;
import com.times.common.util.ExternalHttpUtil;

/**
 * This batch writer is used to send the mail to the author.
 * This mailer call http://cmsgenericmailer.indiatimes.com/MailServiceWeb/messages/uploadMessage api with mail type 8.
 *
 * @author Rajeev Khatri
 *
 */
public class AuthorMailerWritter implements ItemWriter<MailContent> {	
	private static final Log log = LogFactory.getLog(AuthorMailerWritter.class);


	public void write(List<? extends MailContent> mail) throws Exception {
		log.debug("inside the write ");
		if (mail != null && mail.get(0) != null ) {

			String result = ExternalHttpUtil.postRequestWithBody(com.times.common.util.Constant.uploadMailerServiceUrl, mail.get(0).toString());
			System.out.println(result);
		}		
	}

}