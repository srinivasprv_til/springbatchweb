package com.times.mailer.batch.writer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
import com.times.common.util.StringUtils;
import com.times.mailer.batch.reader.WelcomeMailerReader;

public class HTWelcomeMailerWriter implements ItemWriter<JSONArray> {

	private final static Logger logger = LoggerFactory.getLogger(WelcomeMailerReader.class);

	private EmailProcessor emailProcessor;

	public EmailProcessor getEmailProcessor() {
		return emailProcessor;
	}

	public void setEmailProcessor(EmailProcessor emailProcessor) {
		this.emailProcessor = emailProcessor;
	}

	private static String getMsgData(String templateURL) throws IOException {
		
		if (templateURL != null) {
			URL url = new URL(templateURL);
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			String encoding = con.getContentEncoding();
			encoding = encoding == null ? "UTF-8" : encoding;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buf = new byte[8192];
			int len = 0;
			while ((len = in.read(buf)) != -1) {
				baos.write(buf, 0, len);
			}
			String body = new String(baos.toByteArray(), encoding);
			body = StringEscapeUtils.escapeSql(body);
			return body;
		} else {
			return null;
		}

	}


	@Override
	public void write(List<? extends JSONArray> item) throws Exception {
		JSONArray listOFJson = item.get(0);
		for (int i = 0; i < listOFJson.length(); i++) {
			EmailVO emailVO = new EmailVO();
			JSONObject jsonObject = listOFJson.getJSONObject(i);
			String email = jsonObject.getString("email");
			String channel = jsonObject.getString("channel");
			String user_name = jsonObject.getString("user_name");
			String guidename = jsonObject.getString("guidename");

			if (!org.apache.commons.lang3.StringUtils.isEmpty(user_name)
					&& !org.apache.commons.lang3.StringUtils.isEmpty(email)
					&& !org.apache.commons.lang3.StringUtils.isEmpty(channel)
					&& !org.apache.commons.lang3.StringUtils.isEmpty(guidename)) {
				if (channel.equals("happytrips")) {
					emailVO.setSender("HappyTrips <mailerservice@timesofindia.com>");

					emailVO.setSubject("Welcome to " + guidename + " travel guide app");
					String templateUrl = "http://toidev.indiatimes.com:8365/cgi_welcome_mailer.cms?" + "guidename=" 
					+ guidename + "&user_name=" + user_name + "&email=" + email;

					emailVO.setBody(getMsgData(templateUrl));
				}

				emailVO.setRecipients(email);

				try {
					logger.debug("email:{},subject:{},msg:{}", emailVO.getRecipients(), emailVO.getSubject(),
							emailVO.getBody().length());

					if (emailVO.getRecipients() != null && emailVO.getSubject() != null && emailVO.getBody() != null) {
						emailProcessor.sendEMail(emailVO);
					}
				} catch (NullPointerException nullPointer) {
					logger.error("null pointer exception {}", nullPointer);
				}

			}

		}

	}

}
