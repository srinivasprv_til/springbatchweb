package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.Constant;
import com.times.mailer.model.EpaperFileDetails;


public class EpaperZipWritter implements ItemWriter<EpaperFileDetails> {

	private static final Log log = LogFactory.getLog(EpaperZipWritter.class);


	public void write(List<? extends EpaperFileDetails> epaperInfoList) throws Exception {

		if (epaperInfoList != null && epaperInfoList.get(0) != null) {
			EpaperFileDetails item = epaperInfoList.get(0);
			ObjectId id= new ObjectId(item.getId());
			DBObject query = new BasicDBObject();

			query.put("_id", id);

			DBObject storedDBObject = MongoRestAPIService.get(Constant.EPAPER_RAW_DATA_COLLECTION, query);
			if (!storedDBObject.get("status").toString().equalsIgnoreCase("0")) {
				storedDBObject.put("status", 2);
			}
			storedDBObject.put("gfsid", item.getGsfid());

			MongoRestAPIService.findAndUpdate(Constant.EPAPER_RAW_DATA_COLLECTION, query, storedDBObject);
		}

	}

	
}