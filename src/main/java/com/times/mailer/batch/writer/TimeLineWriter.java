package com.times.mailer.batch.writer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.EmailVO;
import com.times.common.util.InMemoryData;
import com.times.mailer.model.MsidObject;

/**
 * 
 * @author Rajeev Khatri
 *
 */
public class TimeLineWriter implements ItemWriter<Document> {

	private static final Log log = LogFactory.getLog(TimeLineWriter.class);

	public void write(List<? extends Document> documents) throws Exception {
		if(documents != null && documents.get(0) != null) {
			Date date = new Date();
			Element root = documents.get(0).getRootElement();
			List<Element> children = root.getChild("prioritycontent").getChild("list").getChildren();

			List<String> priorityList = new ArrayList<String>();

			for(Element storyElem : children) { 
				String msid =  storyElem.getChild("msid").getTextTrim();
//				String msTitle = storyElem.getChild("title").getTextTrim();
//				String overridelink = storyElem.getChild("overridelink").getTextTrim();
//
//				MsidObject msidObject = new MsidObject();
//				msidObject.setMsid(msid);
//				msidObject.setMsTitle(msTitle);
//				if(overridelink != null) {
//					msidObject.setOverridelink(overridelink);
//				}

				priorityList.add(msid);
			}
			Calendar cal = Calendar.getInstance();
			cal.setTimeZone(TimeZone.getTimeZone("IST"));
			cal.setTime(date);
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			int minute = cal.get(Calendar.MINUTE);

			SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			String dateStr = sdf.format(date);
			

			SimpleDateFormat numberFormat = new SimpleDateFormat("yyyyMMdd");
			numberFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			String idateStr = numberFormat.format(date);
			
			int iDate = Integer.valueOf(idateStr);

			DBObject query = new BasicDBObject();
			query.put("DT", dateStr);
			query.put("HR", hour);

			DBObject update = new BasicDBObject();

			//store msid's every 5minutes to minute field
			//	        	update.put("$set", new BasicDBObject("intervals."+String.valueOf(minute/5),priorityList));

			//store msid's in positions
			DBObject positionsObj = new BasicDBObject();
			for(int i=0; i < priorityList.size(); i++) {
				positionsObj.put("POS."+i,Integer.parseInt(priorityList.get(i)));
			}
			DBObject setObjects = new BasicDBObject();
			setObjects.put("iDT",iDate);
			setObjects.put("hostId",InMemoryData.instance.getHostId());
			setObjects.put("campaignId",InMemoryData.instance.getCampaignId());
			update.put("$set", setObjects);
			update.put("$push", positionsObj);

			query.put("TP", 0);
			query.put("hostId",InMemoryData.instance.getHostId());
            query.put("campaignId",InMemoryData.instance.getCampaignId());

			//	        	query.put("TP", 1);

			DBObject existingDoc = MongoRestAPIService.get("Timeline", query);


			Set<Integer> existingSet = new HashSet<Integer>();
			for(int i = 0; i < 10; i++) {
				if(existingDoc != null && existingDoc.get("POS") != null && ((DBObject)existingDoc.get("POS")).get(String.valueOf(i)) != null) {
					List<Integer> tempList = (List<Integer>) ((DBObject)existingDoc.get("POS")).get(String.valueOf(i));
					for(Integer msid: tempList) {
						existingSet.add(msid);
					}
				}
			}

			//handle day break- 0 hours
			if(hour == 0) {
				//	        		hour = 24;
				Date tempDate = addDays(date, -1);
				sdf = new SimpleDateFormat("dd_MM_yyyy");
				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
				query.put("DT", sdf.format(tempDate));
				query.put("HR", 23);
			} else {
				query.put("HR", hour-1);
			}
			existingDoc = MongoRestAPIService.get("Timeline", query);

			for(int i = 0; i < 10; i++) {
				if(existingDoc != null && existingDoc.get("POS") != null && ((DBObject)existingDoc.get("POS")).get(String.valueOf(i)) != null) {
					List<Integer> tempList = (List<Integer>) ((DBObject)existingDoc.get("POS")).get(String.valueOf(i));
					for(Integer msid: tempList) {
						existingSet.add(msid);
					}
				}
			}

			Set<Integer> diffList = new HashSet<Integer>();

			for(String msid: priorityList) {
				if(!existingSet.contains(Integer.valueOf(msid))) {
					diffList.add(Integer.valueOf(msid));
				}
			}

			//tp 0 update
			DBObject updateQuery = new BasicDBObject();
			updateQuery.put("HR", hour);
			updateQuery.put("DT", dateStr);
			updateQuery.put("TP", 0);
			updateQuery.put("hostId", InMemoryData.instance.getHostId());
			MongoRestAPIService.update("Timeline", updateQuery, update);
			update.removeField("$push");
			//	        	if(existingDoc == null) {
			//	        		update.put("$set", new BasicDBObject("intervals."+String.valueOf(minute/5),priorityList));
			//	        	} else {
			DBObject setObj = new BasicDBObject();
    		
    		setObj.put("iDT", iDate);
    		setObj.put("hostId",InMemoryData.instance.getHostId());
    		setObj.put("campaignId",InMemoryData.instance.getCampaignId());
    		setObj.put("intervals."+String.valueOf(minute/5),diffList);
    		
    		
    		update.put("$set", setObj);
			//	        	}
			updateQuery.put("TP", 1);
			updateQuery.put("hostId", InMemoryData.instance.getHostId());
			MongoRestAPIService.update("Timeline", updateQuery, update);

			System.out.println(priorityList);
		}
	}

	

	private static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);

		return cal.getTime();
	}

}
