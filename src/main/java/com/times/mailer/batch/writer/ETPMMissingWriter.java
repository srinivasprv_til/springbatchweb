package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.model.ETEpaperArticle;
import com.times.common.util.InMemoryData;

/**
 * 
 * @author praveen.bokka
 *
 */
public class ETPMMissingWriter implements ItemWriter<List<DBObject>> {
	
	private static final Log log = LogFactory.getLog(ETPMMissingWriter.class);
	
	public static final String PM_MISSING_COLLECTION = "ETPMMissing";

	public void write(List<? extends List<DBObject>> item) throws Exception {
		log.debug("etPMMissing write method start..");
		
		List<DBObject> dbList = null;
		if(item != null) {
			dbList = item.get(0);
		}
		
		if(dbList != null){
			try {
				for(DBObject document : dbList){
					MongoRestAPIService.updateWithPost(PM_MISSING_COLLECTION, document,document);
				}
			} catch (Exception e) {
				log.error("Error while inserting to mongo");
			}
		}
		
		log.debug("etPMMissing write method end..");
	}

}
