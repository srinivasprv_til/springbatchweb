package com.times.mailer.batch.writer;

import java.util.List;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.model.UserFeed;
import com.times.common.service.EhcacheBatchService;
import com.times.common.service.FileContentServices;
import com.times.common.service.OfflineFeedService;
import com.times.mailer.batch.reader.OfflineFeedMergeReader;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.NetstorageInfo;

public class EhcacheBatchWriter implements ItemWriter<Integer> {
	private final static Logger logger = LoggerFactory.getLogger(EhcacheBatchWriter.class);
	EhcacheBatchService ehcacheBatchService;
	private FileContentServices fileContentServices;
	
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}
	public EhcacheBatchService getEhcacheBatchService() {
		return ehcacheBatchService;
	}
	public void setEhcacheBatchService(EhcacheBatchService ehcacheBatchService) {
		this.ehcacheBatchService = ehcacheBatchService;
	}


	public void write(List<? extends Integer> msidList) throws Exception {
		int perpage = 200;
		int perpage_poi = 50;
		if (msidList != null && msidList.size() >  0) {
			String msid = String.valueOf(msidList.get(0));
			if (msid != null) {
				try{
					Map<String, Map<String, Element>> dataMap = ehcacheBatchService.getDataXML(msid);
					if(dataMap.get("articles") != null && dataMap.get("articles").size() > 0){
						if(dataMap.get("articles").size() <= perpage){
							Document doc=new Document();
							doc.setRootElement(ehcacheBatchService.getXML(dataMap.get("articles")));
							String fileContent = ehcacheBatchService.getFileContent(doc);
							if(fileContent.contains("Document")){
								FileLocationInfo fileUploadInfo = new FileLocationInfo();
								String fileName = "happytrips_allarticle_feeds_"+msid+",curpg-1.xml";
								fileUploadInfo.setNetstorageLocation(getNetStorageInfo(fileName));
								fileUploadInfo.setStatus(true);
								System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
								fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
								logger.info("  target url: " + fileName);
							}else{
								System.out.println("  Old content retained - article: 1" + fileContent);
							}
						}else{
							Document doc=new Document();
							int maxpage;
							maxpage = (int) Math.ceil(dataMap.get("articles").size()/perpage);
							if(dataMap.get("articles").size()%perpage > 0){
								maxpage++;
							}
							List<Element> lst =  ehcacheBatchService.getElementList(dataMap.get("articles"));
							for(int pageno=1; pageno<=maxpage; pageno++){
								doc.setRootElement(ehcacheBatchService.getXML(lst,pageno, perpage));
								String fileContent = ehcacheBatchService.getFileContent(doc);
								if(fileContent.contains("Document")){
									FileLocationInfo fileUploadInfo = new FileLocationInfo();
									String fileName = "happytrips_allarticle_feeds_"+msid+",curpg-"+pageno+".xml";
									fileUploadInfo.setNetstorageLocation(getNetStorageInfo(fileName));
									fileUploadInfo.setStatus(true);
									System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
									fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
									logger.info("  target url: " + fileName);
								}else{
									System.out.println("  Old content retained - article: 2" + fileContent);
								}
							}
						}
					}else{
						System.out.println("  Old content retained - article: 3" + dataMap.get("articles"));
						logger.info("articles" + dataMap.get("articles"));
					}
					
					if(dataMap.get("guide") != null && dataMap.get("guide").size() > 0){
						Document doc=new Document();
						doc.setRootElement(ehcacheBatchService.getXML(dataMap.get("guide")));
						String fileContent = ehcacheBatchService.getFileContent(doc);
						if(fileContent.contains("Document")){
							FileLocationInfo fileUploadInfo = new FileLocationInfo();
							String fileName = "happytrips_allguide_feeds_"+msid+",curpg-1.xml";
							fileUploadInfo.setNetstorageLocation(getNetStorageInfo(fileName));
							fileUploadInfo.setStatus(true);
							System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
							fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
							logger.info("  target url: " + fileName);
						}else{
							System.out.println("  Old content retained - Guide: " + fileContent);
						}
					}else{
						System.out.println("  Old content retained - Guide: " + dataMap.get("guide"));
						logger.info("guide" + dataMap.get("guide"));
					}
					if(dataMap.get("poi") != null &&  dataMap.get("poi").size() > 0){
						if(dataMap.get("poi").size() <= perpage_poi){
							Document doc=new Document();
							doc.setRootElement(ehcacheBatchService.getXMLPOI(dataMap.get("poi")));
							String fileContent = ehcacheBatchService.getFileContent(doc);
							if(fileContent.contains("group")){
								FileLocationInfo fileUploadInfo = new FileLocationInfo();
								String fileName = "happytrips_allpoi_feeds_"+msid+",curpg-1.xml";
								fileUploadInfo.setNetstorageLocation(getNetStorageInfo(fileName));
								fileUploadInfo.setStatus(true);
								System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
								fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
								System.out.println("  target url: " + fileName);
							}else{
								System.out.println("  Old content retained - POI " + fileContent);
							}
						}else{
							Document doc=new Document();
							int maxpage;
							maxpage = (int) Math.ceil(dataMap.get("poi").size()/perpage_poi);
							if(dataMap.get("poi").size()%perpage_poi > 0){
								maxpage++;
							}
							List<Element> lst =  ehcacheBatchService.getElementList(dataMap.get("poi"));
							for(int pageno=1; pageno<=maxpage; pageno++){
								doc.setRootElement(ehcacheBatchService.getXML(lst,pageno, perpage_poi));
								String fileContent = ehcacheBatchService.getFileContent(doc);
								if(fileContent.contains("group")){
									FileLocationInfo fileUploadInfo = new FileLocationInfo();
									String fileName = "happytrips_allpoi_feeds_"+msid+",curpg-"+pageno+".xml";
									fileUploadInfo.setNetstorageLocation(getNetStorageInfo(fileName));
									fileUploadInfo.setStatus(true);
									System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
									fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
									logger.info("  target url: " + fileName);
								}else{
									System.out.println("  Old content retained - poi: " + fileContent);
								}
							}
						}
						
						
					}else{
						System.out.println("  Old content retained - POI " + dataMap.get("poi"));
						logger.info("poi" + dataMap.get("poi"));
					}
				}catch(Exception e){
					//logger.error("Error in filecontent : "+e.getMessage() +" | guide msid : " + msid);
					System.out.println("Error in filecontent : "+e.getMessage() +" | guide msid : " + msid);
					e.printStackTrace();
				}
			}
		}
	}

	private NetstorageInfo getNetStorageInfo(String fileName) {
		NetstorageInfo netstorageInfo = new NetstorageInfo();
		netstorageInfo.setFileName(fileName);
		netstorageInfo.setServerName("happytrips.upload.akamai.com");
		netstorageInfo.setFolderLocation("/505970/offlinefeeds");
		netstorageInfo.setUserid("happyftp");
		netstorageInfo.setPassword("hAPpy#$56fTP");
		
		return netstorageInfo;
	}

	public void writewithoutpagination(List<? extends Integer> msidList) throws Exception {

		if (msidList != null && msidList.size() >  0) {
			String msid = String.valueOf(msidList.get(0));
			if (msid != null) {
				try{
					/*Map<String, List<Map<String,String>>> allMap = ehcacheBatchService.getDataJson(msid);
					if(allMap != null){ 
						String articleContent = ehcacheBatchService.getJson(msid, allMap);
						FileLocationInfo fileUploadInfo = new FileLocationInfo();
						String fileName = "happytrips_all_feeds_"+msid+".htm";
						fileUploadInfo.setNetstorageLocation(getNetStorageInfo(fileName));
						fileUploadInfo.setStatus(true);
						System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
						fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), articleContent);
						logger.info("  target url: " + fileName);
					}else{
						logger.info("articles" + allMap);
					}*/
					Map<String, Map<String, Element>> dataMap = ehcacheBatchService.getDataXML(msid);
					if(dataMap.get("articles") != null && dataMap.get("articles").size() > 0){ 
						Document doc=new Document();
						doc.setRootElement(ehcacheBatchService.getXML(dataMap.get("articles")));
						String fileContent = ehcacheBatchService.getFileContent(doc);
						if(fileContent.contains("Document")){
							FileLocationInfo fileUploadInfo = new FileLocationInfo();
							String fileName = "happytrips_allarticle_feeds_"+msid+".xml";
							fileUploadInfo.setNetstorageLocation(getNetStorageInfo(fileName));
							fileUploadInfo.setStatus(true);
							System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
							fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
							logger.info("  target url: " + fileName);
						}else{
							System.out.println("  Old content retained - article: 4" + fileContent);
						}
					}else{
						System.out.println("  Old content retained - article: 5" + dataMap.get("articles"));
						logger.info("articles" + dataMap.get("articles"));
					}
					
					if(dataMap.get("guide") != null && dataMap.get("guide").size() > 0){
						Document doc=new Document();
						doc.setRootElement(ehcacheBatchService.getXML(dataMap.get("guide")));
						String fileContent = ehcacheBatchService.getFileContent(doc);
						if(fileContent.contains("Document")){
							FileLocationInfo fileUploadInfo = new FileLocationInfo();
							String fileName = "happytrips_allguide_feeds_"+msid+".xml";
							fileUploadInfo.setNetstorageLocation(getNetStorageInfo(fileName));
							fileUploadInfo.setStatus(true);
							System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
							fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
							logger.info("  target url: " + fileName);
						}else{
							System.out.println("  Old content retained - Guide: " + fileContent);
						}
					}else{
						System.out.println("  Old content retained - Guide: " + dataMap.get("guide"));
						logger.info("guide" + dataMap.get("guide"));
					}
					if(dataMap.get("poi") != null &&  dataMap.get("poi").size() > 0){
						Document doc=new Document();
						doc.setRootElement(ehcacheBatchService.getXMLPOI(dataMap.get("poi")));
						String fileContent = ehcacheBatchService.getFileContent(doc);
						if(fileContent.contains("group")){
							FileLocationInfo fileUploadInfo = new FileLocationInfo();
							String fileName = "happytrips_allpoi_feeds_"+msid+".xml";
							fileUploadInfo.setNetstorageLocation(getNetStorageInfo(fileName));
							fileUploadInfo.setStatus(true);
							System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
							fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
							System.out.println("  target url: " + fileName);
						}else{
							System.out.println("  Old content retained - POI " + fileContent);
						}
					}else{
						System.out.println("  Old content retained - POI " + dataMap.get("poi"));
						logger.info("poi" + dataMap.get("poi"));
					}
					/*if(dataMap.get("slideshow") != null && !"{}".equals(dataMap.get("slideshow"))){
						String articleContent = ehcacheBatchService.getJson(dataMap.get("slideshow"));
						FileLocationInfo fileUploadInfo = new FileLocationInfo();
						String fileName = "happytrips_allslideshow_feeds_"+msid+".htm";
						fileUploadInfo.setNetstorageLocation(getNetStorageInfo(fileName));
						fileUploadInfo.setStatus(true);
						System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
						fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), articleContent);
						logger.info("  target url: " + fileName);
					}else{
						logger.info("slideshow" + dataMap.get("slideshow"));
					}*/
					
						
						
				}catch(Exception e){
					//logger.error("Error in filecontent : "+e.getMessage() +" | guide msid : " + msid);
					System.out.println("Error in filecontent : "+e.getMessage() +" | guide msid : " + msid);
					e.printStackTrace();
				}
			}
		}
	}


 
}