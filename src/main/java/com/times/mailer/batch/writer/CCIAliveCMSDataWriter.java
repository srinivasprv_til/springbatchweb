package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;

/**
 * 
 * @author praveen.bokka
 *
 */
public class CCIAliveCMSDataWriter implements ItemWriter<List<DBObject>> {
	
	private static final Log log = LogFactory.getLog(CCIAliveCMSDataWriter.class);
	
	public void write(List<? extends List<DBObject>> item) throws Exception {
		log.debug("CCIAliveCMSData write method start..");
		
		//right now wrtier does nothing. as list will be always empty.
		//keeping the code if needed in future.
		List<DBObject> dbList = item.get(0);
		if(dbList != null){
			for(DBObject document : dbList){
				DBObject query = new BasicDBObject();
				query.put("msid", document.get("msid"));
				query.put("CCIId", document.get("CCIId"));
				log.debug("Inserting msid: "+document.get("msid"));
				BasicDBObject setObject = new BasicDBObject();
				setObject.append("$set", document);
			}
		}
		
		log.debug("CCIAliveCMSData write method end..");
	}
	
}
