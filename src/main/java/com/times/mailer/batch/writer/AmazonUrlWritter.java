package com.times.mailer.batch.writer;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.service.FileContentServices;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.model.FileLocationInfo;

public class AmazonUrlWritter implements ItemWriter<FileLocationInfo> {
	
	// -----------------------------------------------------------------------------------------------
	// Start Private members
		
	private static final Logger logger = LoggerFactory.getLogger(AmazonUrlWritter.class);
	
	private FileContentServices fileContentServices;
	
	private static final String AMAZON_PRODUCT_URL_DATA = "amazonProductUrlData";
	
	// End Private members
	// -----------------------------------------------------------------------------------------------
	
	
	// -----------------------------------------------------------------------------------------------
	// Start public methods

	@Override
	public void write(List<? extends FileLocationInfo> items) throws Exception {
		
		String ftpLocation = null;
		
		try {
			
			if (items != null && items.size() > 0) {
				
				for (FileLocationInfo fileLocationInfo : items) {
					
					String sourceUrl = fileLocationInfo.getSourceUrl().trim();
					
					String fileContent = "";
					
					// This variable is used to hit the url n times for a given product id.
					// try n times to get the data from amazon, if even after n times data is null then continue;
					// currently n =1;
					int COUNT = 0;
					
					while(COUNT < 1){
						
						logger.debug("Sleeping for 1 seconds");
						
						Thread.sleep(1000);
						
						logger.debug(fileLocationInfo.getStateId()+ " Resuming after sleeping for 1 secs");
						
						logger.warn("Url:  "+ sourceUrl);
						
						fileContent = this.fileContentServices.getExtData(sourceUrl);
						
						if(fileContent != null && !"".equals(fileContent)){
							
							logger.info("Saving in Mongo DB: "+fileLocationInfo.getStateId());
							
							insertInMongo(fileContent, fileLocationInfo.getStateId());
							
							break;
						}
						
						COUNT++;
					}
				}
			}
		} catch (Exception e) {
			
			logger.error("Exception while inserting amazon daat in Mongo DB: "+ e.getMessage());
			new CMSCallExceptions("Exception caught while INserting data to DB: " + ftpLocation + " msg: " + e.getMessage(),
									CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		}
	}
	
	// End public methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start private methods
	
	public void insertInMongo(String urlContent, String productId) {
		try {
			DBObject dbObject = new BasicDBObject();
			dbObject.put("_id", productId);
			dbObject.put("DT", new Date());
			dbObject.put("xmlData", urlContent);
			
			MongoRestAPIService.insertWithPost(AMAZON_PRODUCT_URL_DATA, dbObject);
			
		} catch (MongoException e) {
			
			logger.error("MongoException caught while inserting amazon Product Url data , msg : " + e.getMessage(), e);
			
		} catch (Exception e) {
			
			logger.error("Generic Exception caught while inserting amazon Product Url data ", e);
			
		} finally {
		}
	}
	
	// End private methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start getters/setters
	
	public void setFileContentServices(FileContentServices fileContentServices) {
		
		this.fileContentServices = fileContentServices;
	}
	
	// End getters/setters
	// -----------------------------------------------------------------------------------------------
}
