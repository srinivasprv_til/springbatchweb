package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.dao.PerfectMarketingDAO;
import com.times.common.model.PerfectMarketingObject;

public class PerfectMarketNewUrlsWriter implements ItemWriter<List<PerfectMarketingObject>> {
	
	private static final Log log = LogFactory.getLog(PerfectMarketNewUrlsWriter.class);

	public static final String PM_RESULT_COLLECTION = "perfectm_result";
	public static final String PM_UNSUCESSFUL_COLLECTION = "perfectm_unsuccessful";
	public static final String PM_FAILED_COLLECTION = "perfectm_failed";

	private PerfectMarketingDAO dao;

	
	public void write(List<? extends List<PerfectMarketingObject>> item) throws Exception {
		log.debug("pm write method start..");
		try {
			List<PerfectMarketingObject> pmList = item.get(0);
	
			for (PerfectMarketingObject pmObj : pmList) {
				String msid = String.valueOf(pmObj.getMsid());
				String pmURL = pmObj.getRedirecturl();
				if (dao.getPMDBObject(msid, PM_RESULT_COLLECTION, pmURL) == null) {
					int serialNo = dao
							.getSerialNo(PM_RESULT_COLLECTION);
					pmObj.setSerialnumber(serialNo + 1);
					pmObj.setNoattempt(1);
					if (dao.addPMObject(pmObj, PM_RESULT_COLLECTION)) {
						dao.updateSerialNo(PM_RESULT_COLLECTION,
								serialNo + 1);
					}
				} /*else {
					pmObj = dao.getPMObject(msid, PM_RESULT_COLLECTION);
					PerfectMarketingObject pmUpdateObj = pmObj.clone();
					dao.updatePMObject(pmObj, pmUpdateObj,
							PM_RESULT_COLLECTION);
				}*/
						
			}
	
//			try {
//				Thread.sleep(100);
//			} catch (InterruptedException e) {
//				log.error("Exception occurred in sleep method with error = "
//						+ e.getMessage());
//			}
				
				
			log.debug("pm write method end..");
		} catch (Exception e) {
			log.error("Exception occurred in writter method with error = "
					+ e.getMessage());
		}
	}

	public PerfectMarketingDAO getDao() {
		return dao;
	}


	public void setDao(PerfectMarketingDAO dao) {
		this.dao = dao;
	}

}
