/**
 * 
 */
package com.times.mailer.batch.writer;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemWriter;

import com.times.common.service.FileContentServices;
import com.times.common.util.NamedThreadFactory;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.FileLocationInfo;


public class NetStorageContentWriter implements ItemWriter<FileLocationInfo> , StepExecutionListener  {

	private final static Logger logger = LoggerFactory.getLogger(NetStorageContentWriter.class);
	private NetstorageFileInfoDao netstorageFileInfoDao;
	private FileContentServices fileContentServices;
	private NamedThreadFactory threadfactory;
	ExecutorService service =null;

	public void write(List<? extends FileLocationInfo> items) throws Exception {	

		for(FileLocationInfo fileUploadInfo:items ){
			logger.info("Processing url: "+ fileUploadInfo.getSourceUrl());				
			threadfactory.setWorkType(fileUploadInfo.getWorkType());

			NetStorageWriterTask writerJob = new NetStorageWriterTask(netstorageFileInfoDao, fileContentServices, fileUploadInfo);
			service.submit(writerJob);
		}	

	}

	public NetstorageFileInfoDao getNetstorageFileInfoDao() {
		return netstorageFileInfoDao;
	}

	public void setNetstorageFileInfoDao(NetstorageFileInfoDao netstorageFileInfoDao) {
		this.netstorageFileInfoDao = netstorageFileInfoDao;
	}

	public FileContentServices getFileContentServices() {
		return fileContentServices;
	}

	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}

	public NamedThreadFactory getThreadfactory() {
		return threadfactory;
	}

	public void setThreadfactory(NamedThreadFactory threadfactory) {
		this.threadfactory = threadfactory;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		
		NamedThreadFactory threadFactory = getThreadfactory();
		
		service = Executors.newFixedThreadPool(50, threadFactory);
		
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		service.shutdown();
		try {
			service.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			logger.error("Failed executor termination" , e);;
		}
		return null;
	}
	
	

}
