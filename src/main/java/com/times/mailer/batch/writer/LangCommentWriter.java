package com.times.mailer.batch.writer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.item.ItemWriter;

import com.times.common.mail.MailSender;
import com.times.mailer.dao.LangCommentDAO;
import com.times.mailer.dao.MailerDao;
import com.times.mailer.dao.SQLMailerDAO;
import com.times.nbt.model.Newsletter;

public class LangCommentWriter implements ItemWriter<List<Newsletter>>{

	SQLMailerDAO  mailerDao;
	LangCommentDAO CommentDao;
	private static Map<Integer, MailSender> mailerCollection = new HashMap<Integer, MailSender>();

	public LangCommentDAO getCommentDao() {
		return CommentDao;
	}



	public void setCommentDao(LangCommentDAO commentDao) {
		CommentDao = commentDao;
	}


	

	@Override
	public void write(List<? extends List<Newsletter>> data) throws Exception {
		// TODO Auto-generated method stub
		int eroid =-1;
		if (data!=null && data.size()>0){ 
		
			for (int ictr=0; ictr< data.get(0).size();ictr++){
			
				
				
				Newsletter newsletter = (Newsletter) data.get(0).get(ictr);
				if (ictr==0){
					eroid = newsletter.getCmtid();
				}
				if (newsletter!=null ){
					//System.out.println("newsletter body"+ newsletter.getMailContent());
					//System.out.println("newsletter subject"+ newsletter.getMailSubject());
					//System.out.println("newsletter to"+ newsletter.getMsgTo());
					MailSender sender;
					if (mailerCollection.size()>0 && mailerCollection.containsKey(newsletter.getHostid())){
						sender =  mailerCollection.get(newsletter.getHostid());
					}else{
						
						//sender = new MailSender("10.157.211.113" ,newsletter.getMsgSenderEmail(),MailSender.RETURN_PATH, newsletter.getMsgSenderEmail());
						sender = new MailSender("nmailer.indiatimes.com" ,newsletter.getMsgSenderEmail(),MailSender.RETURN_PATH, newsletter.getMsgSenderEmail());
						mailerCollection.put(newsletter.getHostid(), sender);
					}
					if (sender!=null)
					{
						sender.sendMessage(newsletter.getMsgTo(), newsletter.getMailSubject() , newsletter.getMailContent());
					}
					//getMailerDao().SendMail(newsletter);
				}
			}
			
			if (eroid!=-1){
				getCommentDao().UpdateMailerStats(eroid);
			}
		}
	}



	public SQLMailerDAO getMailerDao() {
		return mailerDao;
	}



	public void setMailerDao(SQLMailerDAO mailerDao) {
		this.mailerDao = mailerDao;
	}

	

}
