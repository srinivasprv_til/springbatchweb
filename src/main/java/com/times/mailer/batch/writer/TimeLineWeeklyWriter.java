package com.times.mailer.batch.writer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.EmailVO;
import com.times.common.util.InMemoryData;
import com.times.mailer.model.MsidObject;

/**
 * 
 * @author Rajeev Khatri
 *
 */
public class TimeLineWeeklyWriter implements ItemWriter<List<DBObject>> {

	private static final Log log = LogFactory.getLog(TimeLineWeeklyWriter.class);

	public void write(List<? extends List<DBObject>> documents) throws Exception {
		
		 log.debug("Initiating writer");
		if (documents != null && documents.get(0) != null) {
			List<DBObject> dayDocList = documents.get(0);
			Map<Integer, Integer> unsortWeightageMap = new HashMap<Integer, Integer>();
			List<Integer> priorityMsidList = new ArrayList<Integer>();

			// Adding weight to msids and sorting them in descending order

			for (int j = 0; j < dayDocList.size(); j++) {

				DBObject existingDoc = dayDocList.get(j);
				int hour = (int) existingDoc.get("HR");
				int top = 0;
				int weightage = 1;
				if (hour >= 0 && hour < 9) {
					weightage = 1;
				} else if (hour >= 9 && hour < 17) {
					weightage = 5;
				} else if (hour >= 17 && hour < 22) {
					weightage = 4;
				} else {
					weightage = 2;
				}

				if (existingDoc != null && existingDoc.get("POS") != null
						&& ((DBObject) existingDoc.get("POS")).get(String.valueOf(top)) != null) {
					List<Integer> tempList = (List<Integer>) ((DBObject) existingDoc.get("POS"))
							.get(String.valueOf(top));
					for (Integer msid : tempList) {
						int value = weightage;
						if (unsortWeightageMap.containsKey(msid)) {
							value += unsortWeightageMap.get(msid);
						}
						unsortWeightageMap.put(msid, value);
					}

				}
			}

			List<Map.Entry<Integer, Integer>> list = new LinkedList<Map.Entry<Integer, Integer>>(
					unsortWeightageMap.entrySet());

			Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
				public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
					return (o1.getValue()).compareTo(o2.getValue());
				}
			});

			Map<Integer, Integer> sortedWeightageMap = new LinkedHashMap<Integer, Integer>();
			for (Map.Entry<Integer, Integer> entry : list) {
				sortedWeightageMap.put(entry.getKey(), entry.getValue());
			}

			List<Integer> sortedMsidList = new ArrayList<Integer>(sortedWeightageMap.keySet());
			Collections.reverse(sortedMsidList);

			// tp 10 update
			
			Calendar cal = Calendar.getInstance();
			cal.setTimeZone(TimeZone.getTimeZone("IST"));
			cal.setTime(InMemoryData.etTimeLineDate);
			int day = cal.get(Calendar.DAY_OF_WEEK);
			int weekNumber = cal.get(Calendar.WEEK_OF_YEAR);

			
			
			cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
			cal.set(Calendar.WEEK_OF_YEAR, weekNumber);
			Date date = cal.getTime();
			
			log.debug("Writing data for date"+ InMemoryData.etTimeLineDate);
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			String dateStr = sdf.format(date);

			SimpleDateFormat numberFormat = new SimpleDateFormat("yyyyMMdd");
			numberFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			String idateStr = numberFormat.format(date);

			int iDate = Integer.valueOf(idateStr);

			DBObject query = new BasicDBObject();

			query.put("DT", dateStr);

			query.put("WOY", weekNumber);
			
			query.put("hostId", InMemoryData.instance.getHostId());
			
			query.put("campaignId", InMemoryData.instance.getCampaignId());

			DBObject update = new BasicDBObject();

			// store msid's for a day
			DBObject daySortedObj = new BasicDBObject();

			daySortedObj.put("iDT", iDate);
			daySortedObj.put("DAY." + day, sortedMsidList);

			update.put("$set", daySortedObj);

			query.put("TP", 10);
			MongoRestAPIService.update("Timeline", query, update);
			
			log.debug("end of writer");

		}
	}

}
