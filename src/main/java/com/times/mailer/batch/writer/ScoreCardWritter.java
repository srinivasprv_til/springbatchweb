package com.times.mailer.batch.writer;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.springframework.batch.item.ItemWriter;
import org.springframework.web.util.UriUtils;

import com.times.common.service.FileContentServices;
import com.times.common.service.upload.FileUploadServiceFactory;
import com.times.common.util.NetstorageUtil;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.FileLocationInfo.LocationType;

/**
 * @author Ranjeet.Jha
 *
 */
public class ScoreCardWritter implements ItemWriter<FileLocationInfo> {
	
	private static final Log log = LogFactory.getLog(ScoreCardWritter.class);
	
	private NetstorageFileInfoDao netstorageFileInfoDao;
	private FileContentServices fileContentServices;
	
	//private EmailProcessor smtpMailer;
	
	/**
	 * @param netstorageFileInfoDao the netstorageFileInfoDao to set
	 */
	public void setNetstorageFileInfoDao(NetstorageFileInfoDao netstorageFileInfoDao) {
		this.netstorageFileInfoDao = netstorageFileInfoDao;
	}

	@Override
	public void write(List<? extends FileLocationInfo> items) throws Exception {
		//log.debug("inside the write method of writter");
		String location = null;
		try {
			if (items != null && items.size() > 0) {
				for (FileLocationInfo fileUploadInfo : items) {
					if (fileUploadInfo.getLocationType() == LocationType.NET_STORAGE) {
						location = fileUploadInfo.getNetstorageLocation().getServerName()
								+ fileUploadInfo.getNetstorageLocation().getFolderLocation() + "/"
								+ fileUploadInfo.getNetstorageLocation().getFileName();

					} else if (fileUploadInfo.getLocationType() == LocationType.AWS) {
						location = fileUploadInfo.getAmazonS3().getBucketName() + "/"
								+ fileUploadInfo.getAmazonS3().getFileName();
					}					
					
					String sourceUrl = fileUploadInfo.getSourceUrl().trim();
					//String fileContent = fileContentServices.getContentByHTTP(sourceUrl);
					String fileContent = getContentByHTTP(sourceUrl);
					if (fileContent != null) {
						
						//System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName() + "u id " + fileUploadInfo.getNetstorageLocation());
						FileUploadServiceFactory.getUploadServiceByLocation(fileUploadInfo.getLocationType())
						.upload(fileUploadInfo, fileContent);
						//fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
						//fileContentServices.writeFileInLocalFileSystem(fileUploadInfo.getNetstorageLocation(), fileContent);
						log.debug("uploaded size: " + fileContent.length() + " source url : " + sourceUrl + " target url: " + location);
						
						// update last_updated_at column for record 
						netstorageFileInfoDao.updateFileLocationInfo(fileUploadInfo);
					}
				}
			}
		} catch (Exception e) {
			new CMSCallExceptions("Exception caught while ftp update. ftp url: " + location + " msg: " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			log.error("exception caught while ftp update. ftp url: " + location);
		}
	}

	/**
	 * @param fileContentServices the fileContentServices to set
	 */
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}

	public String getContentByHTTP(String url) throws Exception {
		long startTime = System.currentTimeMillis();

		String outData = null;
		
		HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
		RequestConfig config = RequestConfig.custom().setMaxRedirects(5)
				.setConnectTimeout(5000).setSocketTimeout(5000).build();

		//Request.setHeader(CoreProtocolPNames.HTTP_CONTENT_CHARSET, Consts.UTF_8)
		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try {
			url = UriUtils.encodeQuery(url, "UTF-8");
			gMethod = new HttpGet(url);
			gMethod.setConfig(config);
			//gMethod.addHeader("Content-Type", "text/html; charset=UTF-8");
			hResponse = hClient.execute(gMethod);
			//hResponse.setCharacterEncoding("utf-8");
			int status = hResponse.getStatusLine().getStatusCode();
			//hResponse.addHeader(CoreProtocolPNames.HTTP_CONTENT_CHARSET, Consts.UTF_8);
			
			if (status == HttpStatus.SC_OK) {
				
				// this is not working need to figureout
				//hResponse.setHeader("Content-Type", "text/html; charset=UTF-8");
				//hResponse.getHeaders(name)("Content-Type");
				//String extData = new BasicResponseHandler().handleResponse(hResponse);
				HttpEntity entity = hResponse.getEntity();
				Header[] hs = hResponse.getHeaders("Content-Type");
				/*for (int i = 0 ; i < hs.length; i++) {
					System.out.println("content-Type" + hs[i].getName() + " : " + hs[i].getValue());
				}*/
				//Charset charset = Charset.forName("UTF-8");
				InputStream is = entity.getContent();
				String extData = NetstorageUtil.getContentByInputStream(is);
				//String extData = EntityUtils.toString(entity, charset);
				/*File f = new File("d:/test.htm");
				FileOutputStream fio = new FileOutputStream(f);
				fio.write(extData.getBytes());
				fio.close();*/
				//extData = new String(extData.getBytes(), charset);
				return extData;
			}
		} catch (IOException e) {
			log.error("IOException caught during reading of URL: " + url + " msg : "+ e.getMessage());
			log.debug("pulling time in ms : " + (System.currentTimeMillis() - startTime) + " of URL : " + url);
			new CMSCallExceptions(url + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
			//outData = getContentByHTTP(url);
		} catch (Exception e) {
			log.error("Exception caught during reading of URL: " + url + " msg : "+ e.getMessage());
			new CMSCallExceptions(url + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
		} finally {
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return outData;
	}

	
	/*public static void main(String... args ) {
		List<FileLocationInfo> list = populateFileLocationInfo();
		try {
			for (FileLocationInfo fileLocationInfo : list) {
				
				String fileContent = NetstorageUtil.getData(fileLocationInfo.getSourceUrl());
				if (fileContent != null) {
					NetstorageUtil.uploadFileContent(fileLocationInfo.getNetstorageLocation(), fileContent);
					System.out.println("hi");
					//NetstorageUtil.uploadFileContent(fileLocationInfo.getNetstorageLocation(), fileContent);
					System.out.println("folder : " + fileLocationInfo.getNetstorageLocation().getFolderLocation());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	

}
