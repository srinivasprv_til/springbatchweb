package com.times.mailer.batch.writer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.model.UserFeed;
import com.times.common.service.OfflineFeedService;
import com.times.mailer.batch.reader.OfflineFeedMergeReader;

public class OfflineFeedWriter implements ItemWriter<UserFeed> {
	private final static Logger logger = LoggerFactory.getLogger(OfflineFeedWriter.class);
	OfflineFeedService offlineFeedService;
	public OfflineFeedService getOfflineFeedService() {
		return offlineFeedService;
	}
	public void setOfflineFeedService(OfflineFeedService offlineFeedService) {
		this.offlineFeedService = offlineFeedService;
	}


	public void write(List<? extends UserFeed> userFeedList) throws Exception {

		if (userFeedList != null && userFeedList.size() >  0) {
			UserFeed uf = userFeedList.get(0);
			if (uf != null) {
				try{
					offlineFeedService.postUserActivity(uf);
				}catch(Exception e){
					logger.error("Error in postUserActivity : "+e.getMessage() +" | USER id : " + uf.getUserId());
				}
			}
		}
	}


 
}