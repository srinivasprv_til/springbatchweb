package com.times.mailer.batch.writer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.function.Function;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.HttpException;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.util.StringUtils;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.service.SimilarGadgetMatcherService;
import com.times.common.util.ExternalHttpUtil;
import com.times.common.util.GadgetUtil;
import com.times.common.util.InMemoryData;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.dao.TechGadgetProductDAO;
import com.times.mailer.model.TechGadgetProductDetail;

import static com.times.mailer.constant.GadgetConstant.*;
import static com.times.mailer.constant.GadgetConstant.Category.*;

/**
 * This class receives the Object of 'TechGadgetProductDetail' from reader, and finds the complete detail of the product.
 * This detail is then saved to Mongo in 2 collections.
 * Mongo Collection:
 * 	1. brand_detail: Contains the detail specific to a brand.
 * 		DT field contains the date last updated.
 * 	
 * 	2. product_detail: Contains the detail specific to a product.
 * 					This also contains some of the brand detail for which the product belongs to.
 * 		DT field contains the date last updated.
 * 
 * @author Anurag.Singhal
 *
 */
public class TechGadgetProductWritterNew implements ItemWriter<TechGadgetProductDetail>{

	
	private static final Logger logger = LoggerFactory.getLogger(TechGadgetProductWritterNew.class);
	
//	private String ssoToken;
	private TechGadgetProductDAO gadgetDAO;
	
	public List<String> processedBrandList = new ArrayList<String>();
	
	private SimilarGadgetMatcherService similarGadgetSearchService;
	
	
	//OldGadgetsMap
    public Map<String, Integer> oldGadgetsMap = new HashMap<String, Integer>();
    public Map<String, String> oldGadgetSEOMap = new HashMap<String, String>();
    
    public Map<String, Integer> manuallyMatchedBuyTGadgets = new HashMap<String, Integer>();
    
//    public Map<Integer, Map<String, DBObject>> categoryProductListMap = new HashMap<Integer, Map<String, DBObject>>();
    public Map<String, DBObject> categoryBrandListMap = new HashMap<String, DBObject>();
    
    //This Map contains the all category Data, with List of Objects for a given UniqueId in innerMAP.
    public Map<String, Map<String, List<DBObject>>> categoryProductList4UniqueIDMap = new HashMap<String, Map<String, List<DBObject>>>();
    
    private static String CORRECT_MONGO_URL="mra.indiatimes.com";
    private static String OLD_NAME="old_uname";
    
    //HardCoded Sections in which New Blank articles will be created.
   /* private int MOBILE_BRAND_LIST_MSID = 50453005;
    private int TABLET_BRAND_LIST_MSID = 50453017;
    private int LAPTOP_BRAND_LIST_MSID = 50453023;
    private int CAMERA_BRAND_LIST_MSID = 50453059;
    private int MISSING_BRAND_LIST_MSID = 50465808;
    
    
    private int MISSING_MOBILE_BRAND_MSID = 50478948;
    private int MISSING_LAPTOP_BRAND_MSID = 50478955;
    private int MISSING_CAMERA_BRAND_MSID = 50478961;
    
	private Map<String, Integer> mobBrandMsidMap = new HashMap<String, Integer>();
	private Map<String, Integer> laptopBrandMsidMap = new HashMap<String, Integer>();
	private Map<String, Integer> cameraBrandMsidMap = new HashMap<String, Integer>();*/

	public void write(List<? extends TechGadgetProductDetail> items) throws Exception {
		
		String ftpLocation = null;
		
		try {
			
			if(categoryProductList4UniqueIDMap.isEmpty())
				init();
			
			if (items != null && items.size() > 0) {
				
				for (TechGadgetProductDetail productDetail : items) {
					
					//TODO: Check temporarily removed as 91mobiles are not giving specs due to DEAL PENDING.
					if(productDetail.getSpecs() != null) {
						//Add the Brand Detail in Mongo
					    insertBrandInMongo(productDetail);
						
						insertProductInMongo(productDetail);
					} else {
						logger.error("No Specs found for product:{}", productDetail);
					}
				}
			}
		} catch (Exception e) {
			
			logger.error("Exception while inserting TechGadget productDetail in Mongo DB: "+ e.getMessage());
			new CMSCallExceptions("Exception caught while Inserting data to DB: " + ftpLocation + " msg: " + e.getMessage(),
									CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		}
	}
	
	// End public methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start private methods
	
	
	private void insertBrandInMongo(TechGadgetProductDetail productDetail) {
		try {
			String uniqueID = GadgetUtil.getUniqueID(productDetail.getBrandName());
			
			
			//For Secondary Source, Do not update Brand data, if already exists.
			//Also, Change Brand Name and Image, as present in Primary Source.
			if(categoryBrandListMap.containsKey(uniqueID)) {
				productDetail.setBrandName(String.valueOf(categoryBrandListMap.get(uniqueID).get("name")));
				productDetail.setBrandImage(String.valueOf(categoryBrandListMap.get(uniqueID).get("image")));
			}
			
			if(!processedBrandList.contains(productDetail.getCategory() + "##" + uniqueID)) {
				
				DBObject queryObject = new BasicDBObject();
//				queryObject.put("category", productDetail.getCategory());
				queryObject.put("uniqueid", uniqueID);
				
				//Return if category is also present.
				if(categoryBrandListMap.containsKey(uniqueID)) {
					
					if(isCategoryExists((List<DBObject>)categoryBrandListMap.get(uniqueID).get("category"), productDetail.getCategory())) {
						
						processedBrandList.add(productDetail.getCategory() + "##" + uniqueID);
						return;
					}
					else {
						int brandSectionID = getOrCreateBrandSectionId(productDetail.getDiscontinued(), productDetail.getCategory(), productDetail.getBrandName());
						
						List<DBObject> msidList = new ArrayList<DBObject>(1);
						msidList.add(new BasicDBObject("msid", brandSectionID).append("hostid", GN_HOSTID));
						DBObject categoryObj = new BasicDBObject(productDetail.getCategory(), msidList);
						
						MongoRestAPIService.updateWithPost(BRAND_DETAIL, queryObject, new BasicDBObject().append("$addToSet", new BasicDBObject("category", categoryObj)), false);
						((List<DBObject>)categoryBrandListMap.get(uniqueID).get("category")).add(categoryObj);
						
						processedBrandList.add(productDetail.getCategory() + "##" + uniqueID);
						
						//Send Alert
						InMemoryData.brandAlertList.add(new BasicDBObject("category", productDetail.getCategory())
								.append("name", categoryBrandListMap.get(uniqueID).get("name"))
								.append("uname", categoryBrandListMap.get(uniqueID).get("uname"))
								.append("status", categoryBrandListMap.get(uniqueID).get("status")));
						return;
					}
				}
				
				
				DBObject dbObject = new BasicDBObject();
				dbObject.put("name", productDetail.getBrandName());
//				String uniqueID = getUniqueID(productDetail.getBrandName());
				dbObject.put("uniqueid", uniqueID);//UNIQUE ID, on which brand will be identified as Unique
				dbObject.put("uniqueidint", uniqueID.hashCode());//UNIQUE HASHCODE, which will be used by IBEAT
				
				/*
				 * PID artificially generated to match product_detail
				 * As SOLR indexes both collections together, And an id  which doesn't change(for both collections) is required
				 */
				dbObject.put("pid", uniqueID.hashCode()); 
				
				// Create image msid in parent folder for image
				int imageMsid = getImageMsid(null, productDetail.getBrandImage(), 54046151, productDetail.getBrandName());
				dbObject.put("imageMsid", imageMsid);
				
				dbObject.put("uname", GadgetUtil.getUName(productDetail.getBrandName()));
				dbObject.put("lcuname", dbObject.get("uname").toString().toLowerCase());
				dbObject.put("image", productDetail.getBrandImage());
				
//				List<String> categoryLst = new ArrayList<String>(1);
//				categoryLst.add(productDetail.getCategory());
//				dbObject.put("category", categoryLst);
				
				int brandSectionID = getOrCreateBrandSectionId(productDetail.getDiscontinued(), productDetail.getCategory(), productDetail.getBrandName());
				
				List<DBObject> msidList = new ArrayList<DBObject>(1);
				msidList.add(new BasicDBObject("msid", brandSectionID).append("hostid", GN_HOSTID));
				List<DBObject> newCategoryLst = new ArrayList<DBObject>(1);
				newCategoryLst.add(new BasicDBObject(productDetail.getCategory(), msidList));
				dbObject.put("category", newCategoryLst);
				
				
				dbObject.put("updatedAt", new Date());
				
				if(PRIMARY_SOURCE.equals(productDetail.getSource()))
					dbObject.put("status", ACTIVE);
				else{
					dbObject.put("status", PENDING);
				}
				
				//Also Send Alert here, if new brand added.
				InMemoryData.brandAlertList.add(new BasicDBObject("category", productDetail.getCategory())
						.append("name", dbObject.get("name"))
						.append("uname", dbObject.get("uname"))
						.append("status", dbObject.get("status")));
				
				
				DBObject insertDateObject = new BasicDBObject();
				insertDateObject.put("insertedAt", new Date());
				
				DBObject finalObject = new BasicDBObject().append("$setOnInsert", insertDateObject).append("$set", dbObject);
				MongoRestAPIService.updateWithPost(BRAND_DETAIL, queryObject, finalObject, true);
				
				categoryBrandListMap.put(uniqueID, dbObject);
				processedBrandList.add(productDetail.getCategory() + "##" + uniqueID);
			}
			
		} catch (MongoException e) {
			
			logger.error("MongoException caught while inserting TechGadget productDetail , msg : " + e.getMessage(), e);
			
		} catch (Exception e) {
			
			logger.error("Generic Exception caught while inserting TechGadget productDetail ", e);
			
		} finally {
		}
	}
	
	
	private boolean isCategoryExists(List<DBObject> categoryLst, String category) {
		if(categoryLst != null && !categoryLst.isEmpty()) {
			for(DBObject obj : categoryLst) {
				if(obj.get(category) != null)
					return true;
			}
		}
		
		return false;
	}
	
	
	private void insertProductInMongo(TechGadgetProductDetail productDetail) {
		try {
			
			//To track whether product's name has changed with respect to productId
			boolean hasNameChanged=false;
			
			DBObject apiObject = new BasicDBObject();
			apiObject.put("pid", productDetail.getProductId());//ID from the Source
			
			//#CHANGE
			String uniqueID = GadgetUtil.getUniqueID(productDetail.getProductName());
			apiObject.put("uniqueid", uniqueID);
			
			String existingProductUniqueID=null;
			String pId = productDetail.getProductId();
			
			if(!StringUtils.hasText(productDetail.getSource()))
					return;
			
			boolean isNewRecord = false;
			
			if(categoryProductList4UniqueIDMap.get(productDetail.getCategory()) != null) {
				
				//GET LIST OF PRODUCTS WITH SAME PID
				List<DBObject> productList4UniqueID = categoryProductList4UniqueIDMap.get(productDetail.getCategory()).get(pId);
				int status;
				int msid = 0;
				DBObject existingDBObject = null;
				
				if(productList4UniqueID != null && !productList4UniqueID.isEmpty()) {
					//pid+Source:
					List<DBObject> sameSourceList = productList4UniqueID.stream()
							.filter(dbObject -> productDetail.getSource().equals(String.valueOf(dbObject.get("source")))).collect(Collectors.toList());
					
					if(PRIMARY_SOURCE.equals(productDetail.getSource())) {
						if(sameSourceList != null && !sameSourceList.isEmpty()) {
							
							//existingDBObject = sameSourceList.get(0);
							
							//#CHANGE
							//If there are multiple gadgets with same pid, then existing one is selected on the basis of insertdate
							existingDBObject=getLatestBasedOnInsertData(sameSourceList);
							existingProductUniqueID=(String)existingDBObject.get("uniqueid");
							
							//discontinueOtherExceptLatest(existingDBObject,sameSourceList);
							
							if(!GadgetUtil.getUniqueID(productDetail.getProductName()).equals(GadgetUtil.getUniqueID((String) existingDBObject.get("name")))){
								hasNameChanged=true;
							}
							
							status = Integer.parseInt(String.valueOf(existingDBObject.get("status")));
						}
						else {
							isNewRecord = true;
							List<DBObject> activeLst = productList4UniqueID.stream()
									.filter(dbObject -> Integer.valueOf(String.valueOf(dbObject.get("status"))) == ACTIVE).collect(Collectors.toList());
							
							if(activeLst != null && !activeLst.isEmpty()) {
								status = PENDING;
								existingDBObject = activeLst.get(0);
								//Set the Name of this primary gadget, as that of Active data present from other source.
								//This is done, so that Uname can be same as being used earlier.
								productDetail.setProductName(String.valueOf(existingDBObject.get("name")));
							}
							else {
								status = ACTIVE;
								existingDBObject = productList4UniqueID.get(0);
							}
						}
					} 
					
					
					else if(EDITOR_SOURCE.equals(productDetail.getSource())) {
						if(sameSourceList != null && !sameSourceList.isEmpty()) {
							existingDBObject = sameSourceList.get(0);
							status = productDetail.getStatus();//Status selected by Editor
							
							//Update the status in existing in-memory data.
							existingDBObject.put("status", status);
						} else {
							isNewRecord = true;
							
							if(productDetail.getStatus() == INACTIVE || productDetail.getStatus() == PENDING) {
								//WARNING MAY be given to editor, that it is inactive.
								status = productDetail.getStatus();//Status selected by Editor
								existingDBObject = productList4UniqueID.get(0);
							}
							else {
								List<DBObject> activeLst = productList4UniqueID.stream()
										.filter(dbObject -> Integer.valueOf(String.valueOf(dbObject.get("status"))) == ACTIVE).collect(Collectors.toList());
								
								if(activeLst != null && !activeLst.isEmpty()) {
									//PROVIDE ERROR MSG, that Product already exists in active mode, with different source.
									existingDBObject = activeLst.get(0);
									return;
								}
								else {
									status = ACTIVE;
									existingDBObject = productList4UniqueID.get(0);
								}
							}
						}
					} else {
						//To Update price in Primary
						if(productDetail.getPrice() > 0) {
							//Update Only the price in primary and return;
							//TODO
						}
						return;
					}
					
					apiObject.put("status", status);
					
					if(existingDBObject != null) {
						
						if(existingDBObject.get("reviewid") != null && existingDBObject.get("reviewid") instanceof List) {
							msid = getParameterFromReviewList((List<DBObject>)existingDBObject.get("reviewid"), "msid");
						}
						
						if(msid!=0 && hasNameChanged){
							logger.warn("Product name Changed from "+existingDBObject.get("uname")+" to "+productDetail.getProductName()+" MSID:"+msid +"pid "+pId);
							
							boolean isPrimary=false;
							int parentId = getParameterFromReviewList((List<DBObject>)existingDBObject.get("reviewid"), "parentid");
							
							try{
								int primary= (int) existingDBObject.get("primary");
								isPrimary=(primary==1);
							}catch(Exception e){
								
						}
							
							//If name has changed for a particular pid, then update metadata for the existing article
							//If gadget is primary then update parent article too
							updateArticleMetadata(productDetail.getBrandName(), productDetail.getProductName(), 
								productDetail.getCategory(), productDetail.getDiscontinued(),msid,isPrimary,parentId);
							
							//OLD NAME HANDLING
							List <String> oldNames=(List<String>) existingDBObject.get(OLD_NAME);
							if(oldNames==null || oldNames.isEmpty()){
								oldNames=new ArrayList();	
							}
							
							oldNames.add((String)existingDBObject.get("uname"));
							apiObject.put(OLD_NAME, oldNames);
							
						}
						
						apiObject.put("reviewid", existingDBObject.get("reviewid"));
						apiObject.put("primary", existingDBObject.get("primary") != null ? existingDBObject.get("primary") : 0);
						
						if(existingDBObject.get("seourl") != null)
							apiObject.put("seourl", existingDBObject.get("seourl"));
						if(existingDBObject.get("price") != null)
							apiObject.put("price", existingDBObject.get("price"));
					}
					
				} 
				
				//If none such exists:
				//Make this active, and create Article.(Send alert if this is not the primary source)
				else {
					if(PRIMARY_SOURCE.equals(productDetail.getSource()) || EDITOR_SOURCE.equals(productDetail.getSource())) {
						status = ACTIVE;
					} else {
						status = PENDING;
					}
					msid = 0;
					
					List<DBObject> reviewList = new ArrayList<DBObject>();
					reviewList.add(new BasicDBObject("msid", msid).append("hostid", GN_HOSTID));
					
					apiObject.put("status", status);
					apiObject.put("reviewid", reviewList);
					if(productDetail.getPrice() > 0)
						apiObject.put("price", productDetail.getPrice());
					
					
					isNewRecord = true;
					
				}
				
				if(status == ACTIVE && msid == 0) {
					//Create Article, and put msid
					DBObject articleObject = addArticleIfNewProduct(uniqueID, productDetail.getBrandName(), productDetail.getProductName(), 
								productDetail.getCategory(), productDetail.getProductId(), productDetail.getCommonName(), productDetail.getDiscontinued());
					apiObject.put("reviewid", articleObject.get("reviewid"));
					if(articleObject.get("seourl") != null)
						apiObject.put("seourl", articleObject.get("seourl"));
					
					apiObject.put("primary", articleObject.get("primary") != null ? articleObject.get("primary") : 0);
					
					//Update MSID in Existing In-memory Object, so that if further same uniqueID data comes, it uses the same msid.
					if(existingDBObject != null) {
						existingDBObject.put("reviewid", articleObject.get("reviewid"));
					}
					
				}
				
				// Set slide show and image msid
				int slideShowMsid = getSlideShowMsid(existingDBObject, productDetail);
				productDetail.setSlideShowId(slideShowMsid);
				productDetail.setImageMsid(getImageMsidArray(existingDBObject, productDetail, slideShowMsid));
			}
			//New Category data is coming here, which will not be added currently.
			else {
				return;
			}
			
			apiObject.put("slideShowId", productDetail.getSlideShowId());
			apiObject.put("imageMsid", productDetail.getImageMsid());
			
			apiObject.put("name", productDetail.getProductName());
			apiObject.put("lcname", productDetail.getProductName().toLowerCase()); //lower case name

			//Updated Name
			apiObject.put("uname", GadgetUtil.getUName(productDetail.getProductName()));
			apiObject.put("lcuname", apiObject.get("uname").toString().toLowerCase());
			
			apiObject.put("brand_uname", GadgetUtil.getUName(productDetail.getBrandName()));
			apiObject.put("brand_lcuname", apiObject.get("brand_uname").toString().toLowerCase());
			
			apiObject.put("image", productDetail.getProductImage());
			
			if(productDetail.getAnnouncedDate() != null)
				apiObject.put("announced", productDetail.getAnnouncedDate());
			if(productDetail.getModelName() != null){
				apiObject.put("model_name", productDetail.getModelName());
				apiObject.put("model_lcuname", GadgetUtil.getUName(productDetail.getModelName()).toLowerCase());
			}
			if(productDetail.getCommonName() != null) {
				apiObject.put("common_name", productDetail.getCommonName());
				apiObject.put("common_uname", GadgetUtil.getUName(productDetail.getCommonName()));
				apiObject.put("common_lcuname", apiObject.get("common_uname").toString().toLowerCase());
				
				String brand_lcuname=apiObject.get("brand_lcuname").toString();
				String commonModelName=apiObject.get("common_lcuname").toString().replaceFirst(brand_lcuname+"-","");
				
				apiObject.put("c_model_lcuname",commonModelName );
				
			}
			
			
			
			apiObject.put("price", productDetail.getPrice());
			
			apiObject.put("category", productDetail.getCategory());
			apiObject.put("keyFeatures", productDetail.getKey_features());
			apiObject.put("specs", productDetail.getSpecs());
			
			apiObject.put("variants", productDetail.getVariants());
			apiObject.put("colors", productDetail.getColors());
			
			apiObject.put("discontinue", productDetail.getDiscontinued());
			
			apiObject.put("upcoming", productDetail.getUpcoming());
			
			apiObject.put("brand_name", productDetail.getBrandName());
			apiObject.put("brand_image", productDetail.getBrandImage());
			
			apiObject.put("source", productDetail.getSource());
//			apiObject.put("status", ACTIVE);
			
			//Product Last Updated
			apiObject.put("updatedAt", new Date());
			
			DBObject queryObject = new BasicDBObject();
//			queryObject.put("pid", productDetail.getProductId());
			//Commented below line temporarily, so that uniqueId can be created first time.
			//#Change
			queryObject.put("category", productDetail.getCategory());
			queryObject.put("pid", pId);
			if(existingProductUniqueID!=null && !existingProductUniqueID.isEmpty() ){
				queryObject.put("uniqueid", existingProductUniqueID);
			}else{
				queryObject.put("uniqueid", uniqueID);
			}
			queryObject.put("source", productDetail.getSource());
			
			
			
			/*DBObject articleMsid = addArticleIfNewProduct(uniqueID, productDetail.getBrandName(), productDetail.getProductName(), 
					String.valueOf(apiObject.get("uname")), productDetail.getCategory(), productDetail.getProductId());
			
			apiObject.put("msid", articleMsid.get("msid"));
			apiObject.put("status", articleMsid.get("status"));
			if(articleMsid.get("seourl") != null)
				apiObject.put("seourl", articleMsid.get("seourl"));
			if(articleMsid.get("price") != null)
				apiObject.put("price", articleMsid.get("price"));*/
			
			
			DBObject insertDateObject = new BasicDBObject();
			insertDateObject.put("insertedAt", new Date());
			
			//Temporary Change
			/*
			apiObject.put("debug_info", "same");
			if(hasNameChanged){
				apiObject.put("debug_info", "nameChanged");
			}
			if(isNewRecord){
				apiObject.put("debug_info", "newRecord");
			}
			*/
			
			if (isNewRecord) {
				try {
					//GET SIMILAR AMAZON PRODUCTS DBOBJECT
					DBObject similarProductsDetails = similarGadgetSearchService.getAccumulatedSimilarProducts(
							String.valueOf(apiObject.get("name")), String.valueOf(apiObject.get("brand_name")),
							String.valueOf(apiObject.get("category")));
					//Put all into API Object
					if (similarProductsDetails != null)
						apiObject.putAll(similarProductsDetails);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			
			
			MongoRestAPIService.updateWithPost(PRODUCT_DETAIL, queryObject, new BasicDBObject().append("$setOnInsert", insertDateObject).append("$set", apiObject), true);
			
			//Update the Map created for future incoming data..
			if(isNewRecord) {
				
				apiObject.removeField("keyFeatures");
				apiObject.removeField("specs");
//				apiObject.removeField("image");
				apiObject.removeField("colors");
				
				List<DBObject> list = new ArrayList<DBObject>();
				list.add(apiObject);
				categoryProductList4UniqueIDMap.get(productDetail.getCategory()).put(pId, list);
				InMemoryData.alertList.add(apiObject);
				
				/*DBObject afterAddObj = MongoRestAPIService.get(PRODUCT_DETAIL, queryObject);
				if(afterAddObj != null) {
					list.add(new BasicDBObject().append("uniqueid", afterAddObj.get("uniqueid"))
							.append("source", afterAddObj.get("source")).append("status", afterAddObj.get("status"))
							.append("reviewid", afterAddObj.get("reviewid")).append("price", afterAddObj.get("price")));
					categoryProductList4UniqueIDMap.get(productDetail.getCategory()).put(uniqueID, list);
					
					
					//Create List for Sending Alert.
//					if(productDetail.getCategory().equals(MOBILES))
						InMemoryData.alertList.add(afterAddObj);
				} else {
					logger.error("Some MisBehaving occurred here... {}", apiObject);
				}*/
			}
			
		} catch (MongoException e) {
			
			logger.error("MongoException caught while inserting TechGadget productDetail , msg : " + e.getMessage(), e);
			
		} catch (Exception e) {
			
			logger.error("Generic Exception caught while inserting TechGadget productDetail ", e);
			
		} finally {
		}
	}
	
	private void updateSlideAndImage() {
		
		for(Map.Entry<String, Map<String, List<DBObject>>> mapEntry: categoryProductList4UniqueIDMap.entrySet()){
			
			logger.warn("Entering for categhory: "+ mapEntry.getKey());
			
			for(Map.Entry<String, List<DBObject>> mapEntry1: mapEntry.getValue().entrySet()){
				
				DBObject existingDBObject = mapEntry1.getValue().get(0);
				
				TechGadgetProductDetail productDetail = new TechGadgetProductDetail();
				
				productDetail.setProductName((String) existingDBObject.get("uname"));
				productDetail.setProductImage(getStringArray(existingDBObject.get("image")));
				
				try{
					if(existingDBObject != null && existingDBObject.get("slideShowId") != null && (int)existingDBObject.get("slideShowId") != 0){
//						continue;
					}
				}catch(Exception e){
					logger.error("Slide show not found in db object: "+ e);
				}
				
				// Set slide show and image msid
				int slideShowMsid = getSlideShowMsid(existingDBObject, productDetail);
				productDetail.setSlideShowId(slideShowMsid);
				productDetail.setImageMsid(getImageMsidArray(existingDBObject, productDetail, slideShowMsid));
				
				DBObject insertDateObject = new BasicDBObject();
				insertDateObject.put("insertedAt", new Date());
				
				// ------Start of live update code mongo
				// To update image msid in live mongo from test mongo
				
//				DBObject selectQuery = new BasicDBObject();
//				selectQuery.put("uname", productDetail.getProductName());
//				
//				MongoRestAPIService.MONGO_REST_API_URL = "http://mra.indiatimes.com/mra/";
//				List<DBObject> list = MongoRestAPIService.getList(PRODUCT_DETAIL, selectQuery);
//				
//				DBObject objectFromLive = new BasicDBObject();
//				if(list != null && list.size() > 0){
//					objectFromLive = list.get(0);
//				}
//				objectFromLive.put("slideShowId", productDetail.getSlideShowId());
//				objectFromLive.put("imageMsid", productDetail.getImageMsid());
//				try{
//					MongoRestAPIService.updateWithPost(PRODUCT_DETAIL, selectQuery, 
//													new BasicDBObject().append("$setOnInsert", insertDateObject).append("$set", objectFromLive), true);
//				}catch(Exception e){
//					logger.error("Exception: "+ e);
//				}
				
				// ------end of live update code mongo
				
				existingDBObject.put("slideShowId", productDetail.getSlideShowId());
				existingDBObject.put("imageMsid", productDetail.getImageMsid());
				
				String uniqueID = GadgetUtil.getUniqueID(productDetail.getProductName());
				existingDBObject.put("uniqueid", uniqueID);
				
				DBObject queryObject = new BasicDBObject();
				queryObject.put("category", mapEntry.getKey());
				queryObject.put("uniqueid", uniqueID);
				
				try{
					MongoRestAPIService.updateWithPost(PRODUCT_DETAIL, queryObject, 
													new BasicDBObject().append("$setOnInsert", insertDateObject).append("$set", existingDBObject), true);
				}catch(Exception e){
					logger.error("Exception: "+ e);
				}
				
			}
		}
		
	}
	
	private void updateBrandImage() {

		DBObject selectQuery = new BasicDBObject();

		MongoRestAPIService.MONGO_REST_API_URL = "http://mra.indiatimes.com/mra/";
		List<DBObject> list = MongoRestAPIService.getList(BRAND_DETAIL, selectQuery);

		for(DBObject existingDBObject: list){

			String uname = (String) existingDBObject.get("uname");
			String imageUrl = (String) existingDBObject.get("image");

			// Set slide show and image msid
			int slideShowMsid = 54046151;//getSlideShowMsid(existingDBObject, productDetail);
			int imageMsid = 0;
			if(uname.equals("Blackberry")){
				imageMsid = 54046407;
			}else{
				try {
					imageMsid = getImageMsid(existingDBObject, imageUrl, slideShowMsid, uname);
				} catch (Exception e) {
					logger.error("Error while getting imageMSID"+uname);
				}
			}

			existingDBObject.put("imageMsid", imageMsid);

			DBObject insertDateObject = new BasicDBObject();
			insertDateObject.put("insertedAt", new Date());

			DBObject queryObject = new BasicDBObject();
			queryObject.put("uname", uname);

			try{
				MongoRestAPIService.updateWithPost(BRAND_DETAIL, queryObject, 
						new BasicDBObject().append("$setOnInsert", insertDateObject).append("$set", existingDBObject), true);
			}catch(Exception e){
				logger.error("Exception: "+ e);
			}
		}
	}
	
	
	private void updateArticleMetadata(String brandName, String productName, String category,int discontinued,int msid, boolean isPrimary,int parentId){
		
		if(discontinued==1){
			return;
		}
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("brand", brandName));
		urlParameters.add(new BasicNameValuePair("product", productName));
		urlParameters.add(new BasicNameValuePair("category", category));
		urlParameters.add(new BasicNameValuePair("msid", String.valueOf(msid)));
		
		//Update Article metadata
		try {
			String extData = ExternalHttpUtil.getExtData_PostCall("http://gadget.indiatimes.com/tech-gadget/addreview/updateMetadata/", urlParameters);
			logger.info("Metadata for the article with MSID ="+msid+" updated");
		} catch (HttpException | IOException e) {
			logger.error("HttpException Error occured while updating article for Brand:{}, Product:{}", brandName, productName, e);
		} catch(Exception e) {
			logger.error("Other Exception updating while adding article for Brand:{}, Product:{}", brandName, productName, e);
		}
		
		//If product is primary
		//Update OverRideLink metadata also
		if(isPrimary && parentId!=0){
			updateArticleMetadata(brandName, productName, category, discontinued, parentId, false, 0);
		}
		
		
	}
	private String[] getStringArray(Object obj) {
		if(obj != null && !"".equals(String.valueOf(obj)) && !"null".equalsIgnoreCase(String.valueOf(obj))
				&& obj instanceof BasicDBList) {
			
			BasicDBList list = (BasicDBList) obj;
			if(list != null && !list.isEmpty()) {
				Object[] arry = list.toArray();
				String[] newArry = new String[arry.length];
				for(int i=0; i < arry.length; i++) {
					newArry[i] = getStringValue(arry[i]);
				}
				
				return newArry;
			}
		}
		return new String[] {};
	}
	
	private String getStringValue(Object obj) {
		if(obj != null && !"".equals(String.valueOf(obj)) && !"null".equalsIgnoreCase(String.valueOf(obj)))
			return String.valueOf(obj).trim();
		return "";
	}

	private int getSlideShowMsid(DBObject existingDBObject, TechGadgetProductDetail productDetail) {
		
		// Check if slide show already created or not
		try{
			if(existingDBObject != null && existingDBObject.get("slideShowId") != null){
				int slideShowId =  (int) existingDBObject.get("slideShowId");
				if(slideShowId != 0){
					return slideShowId;
				}
			}
		}catch(Exception e){
			logger.error("Slide show not found in db object: "+ e);
		}
		
		
		// Create slide show and get id
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("product", GadgetUtil.getUName(productDetail.getProductName())));
		
		int slideShowMsid = 0;
		try {
			// First create slide show and get msid of slide show
			String extData = ExternalHttpUtil.getExtData_PostCall("http://gadget.indiatimes.com/tech-gadget/addreview/slideshow", urlParameters);
			logger.info("Slide show MSID Generated:{}", extData);
			slideShowMsid = Integer.parseInt(extData);
			
		} catch(Exception e) {
			logger.error("Other Exception occured while adding article for Product:{}", productDetail.getProductName(), e);
		}
		return slideShowMsid;
	}
	
	private int[] getImageMsidArray(DBObject existingDBObject, TechGadgetProductDetail productDetail, int slideShowMsid){
		
		String[] imageUrls = Arrays.copyOf(productDetail.getProductImage(), productDetail.getProductImage().length);
		int[] existingMsids = null;
		int diff = 0;
		if(slideShowMsid == 0){
			return null;
		}
		// check if some images are already created in slide show or not
		try{
			if(existingDBObject != null && existingDBObject.get("imageMsid") != null){
				
				BasicDBList imageList = (BasicDBList) existingDBObject.get("imageMsid");
				
				if(imageList != null && imageList.size() > 0){
					
					existingMsids = new int[imageList.size()];
					for(int i =0;i < imageList.size(); i++){
						existingMsids[i] = (int) imageList.get(i);
					}
				}				
				String[] imageUrlFromSource = productDetail.getProductImage();
				
				if(imageUrlFromSource == null || (existingMsids.length == imageUrlFromSource.length)){
					
					boolean msidZeroExists = false;
					for(int i =0;i<existingMsids.length ;i++){
						// Check if zero msid exist
						if(existingMsids[i] != 0){
							imageUrls[i] = null;
						}else{
							msidZeroExists = true;
						}
					}
					// Return from here is all msid exists.
					if(!msidZeroExists){
						return existingMsids;
					}
				}else{
					
					// Add the new images in the image url array and send these images urls to create image msid
					diff = imageUrlFromSource.length - existingMsids.length;
					if(diff > 0){
						imageUrls = new String[diff];
						int imageUrlLength = productDetail.getProductImage().length;
						for(int i =0; i < diff; i++){
							imageUrls[i] = productDetail.getProductImage()[imageUrlLength-diff+i];
						}
					}else{
						return removeZero(existingMsids);
//						return existingMsids;
					}
				}
 			}else{
 				
 				if(existingDBObject != null && existingDBObject.get("imageMsid") == null && slideShowMsid !=0){
 					int[] slideShowChildrenFromDB = gadgetDAO.getSlideShowChildren(slideShowMsid);
 					if(slideShowChildrenFromDB != null && slideShowChildrenFromDB.length > 0){
 						return slideShowChildrenFromDB;
 					}
 				}
 			}
		}catch(Exception e){
			logger.error("No Images created in slideshow: "+ slideShowMsid);
		}
		
		int[] imageMsidArray = null;
		
		if(imageUrls != null && imageUrls[0].contains(",")){
			imageMsidArray = getImageMsidArrayFromSingleApiUrl(productDetail.getProductName(), imageUrls, slideShowMsid);
		}else{
			
			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("product", GadgetUtil.getUName(productDetail.getProductName())));
			int length = Arrays.toString(imageUrls).length();
			urlParameters.add(new BasicNameValuePair("imageurls", Arrays.toString(imageUrls).substring(1, length-1)));
			urlParameters.add(new BasicNameValuePair("slideShowMsid", String.valueOf(slideShowMsid)));
			
			
			String extData1 = "";
			try {
				// Hit Api and create images in slide show and get image msid array
				extData1 = ExternalHttpUtil.getExtData_PostCall("http://gadget.indiatimes.com/tech-gadget/addreview/addImagesInSlideShow", urlParameters);
				if(extData1 != null){
					imageMsidArray = Arrays.asList(extData1.replace("[", "").replace("]", "").split(","))
								            .stream()
								            .map(String::trim)
								            .mapToInt(Integer::parseInt).toArray();
				}
			} catch (Exception e) {
				logger.error("Other Exception occured while adding article for Product:{}", productDetail.getProductName(), e);
			}
		}
		
		if(diff == 0){
			return mergeArraySameSize(existingMsids, imageMsidArray);
		}else{
			return mergeArray(existingMsids, imageMsidArray);
		}
		
	}
	
	private int[] getImageMsidArrayFromSingleApiUrl(String productName,String[] imageUrls, int slideShowMsid) {
		
		int count =0;
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("product", GadgetUtil.getUName(productName)));
		urlParameters.add(new BasicNameValuePair("slideShowMsid", String.valueOf(slideShowMsid)));
		
		if(imageUrls == null || imageUrls.length == 0){
			return null;
		}
		
		int[] imageMsidArray = new int[imageUrls.length];
		
		for(String s: imageUrls){
			
			urlParameters.add(new BasicNameValuePair("imageurl", s));
			
			String extData1 = "";
			try {
				// Hit Api and create images in slide show and get image msid array
				extData1 = ExternalHttpUtil.getExtData_PostCall("http://gadget.indiatimes.com/tech-gadget/addreview/uploadNewImageInSlideshow", urlParameters);
				if(extData1 != null){
					imageMsidArray[count++] = Integer.parseInt(extData1);
				}
			} catch (Exception e) {
				logger.error("Other Exception occured while adding article for Product:{}", productName, e);
			}
			urlParameters.remove(2);
		}
		
		return imageMsidArray;
	}

	private int[] removeZero(int[] existingMsids) {
		
		List<Integer> list = new ArrayList<Integer>();
		if(existingMsids != null && existingMsids.length > 0){
			
			for(int i =0;i<existingMsids.length;i++){
				if(existingMsids[i] != 0){
					list.add(existingMsids[i]);
				}
			}
		}
		return ArrayUtils.toPrimitive(list.toArray(new Integer[list.size()]));
	}

	private int getImageMsid(DBObject existingDBObject, String imageUrl, int parentMsid, String name){
		
		if(parentMsid == 0){
			return 0;
		}
		// check if some images are already created in slide show or not
		try{
			if(existingDBObject != null && existingDBObject.get("imageMsid") != null && (int)existingDBObject.get("imageMsid") != 0) {
				
				return  (int)existingDBObject.get("imageMsid");
				}
		}catch(Exception e){
			logger.error("No Images created in slideshow: "+ parentMsid);
		}
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("product", name));
		urlParameters.add(new BasicNameValuePair("imageurls", imageUrl));
		urlParameters.add(new BasicNameValuePair("slideShowMsid", String.valueOf(parentMsid)));
		
		
		String extData1 = "";
		int[] imageMsidArray = null;
		try {
			// Hit Api and create images in slide show and get image msid array
			extData1 = ExternalHttpUtil.getExtData_PostCall("http://gadget.indiatimes.com/tech-gadget/addreview/addImagesInSlideShow", urlParameters);
			if(extData1 != null){
				imageMsidArray = Arrays.asList(extData1.replace("[", "").replace("]", "").split(","))
							            .stream()
							            .map(String::trim)
							            .mapToInt(Integer::parseInt).toArray();
			}
		} catch (Exception e) {
			logger.error("Other Exception occured while adding article for Product:{}", name, e);
		}
		
		if(imageMsidArray != null && imageMsidArray.length > 0){
			return imageMsidArray[0];
		}else{
			return 0;
		}
		
	}	
	
	// Merge two arrays
	private int[] mergeArray(int[] array1, int[] array2) {
		
		if(array1 == null && array2 == null){
			return null;
		}
		if(array1 == null && array2 != null){
			return array2;
		}
		if(array1 != null && array2 == null){
			return array1;
		}
		
		final int alen = array1.length;
	    final int blen = array2.length;
	    final int[] result = (int[]) java.lang.reflect.Array.newInstance(array1.getClass().getComponentType(), alen + blen);
	    System.arraycopy(array1, 0, result, 0, alen);
	    System.arraycopy(array2, 0, result, alen, blen);
	    
	    return result;
	}
	
	// Merge two arrays
	private int[] mergeArraySameSize(int[] array1, int[] array2) {
		
		if(array1 == null && array2 == null){
			return null;
		}
		if(array1 == null && array2 != null){
			return array2;
		}
		if(array1 != null && array2 == null){
			return array1;
		}
		
	    final int[] result = (int[]) java.lang.reflect.Array.newInstance(array1.getClass().getComponentType(), array1.length);
	    
	    for(int i=0; i< array1.length ;i++){
	    	if(array1[i] != 0){
	    		result[i] = array1[i];
	    	}else{
	    		result[i] = array2[i];
	    	}
	    }
	    return result;
	}

	private int getParameterFromReviewList(List<DBObject> reviewLst, String parameter) {
		for(DBObject reviewObj : reviewLst) {
			if(GN_HOSTID == getIntValue(reviewObj.get("hostid"))) {
				return getIntValue(reviewObj.get(parameter));
			}
		}
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * This method checks if there is new product being inserted, an CMSNext API for creating article is fired for that product.
	 * @param productDetail
	 * @param queryObject
	 */
	private DBObject addArticleIfNewProduct(String uniqueID, String brandName, String productName, String category, String pid, String commonName, int discontinued) {
		
		/*//Returns if it already exists in active MODE, and source is also Same(BuyT).(Just update Specs)
		if(categoryProductListMap.get(category) != null 
				&& categoryProductListMap.get(category).containsKey(uniqueID + "###" + ACTIVE)
				&& BUYT_SOURCE.equals(categoryProductListMap.get(category).get(uniqueID + "###" + ACTIVE).get("source"))) {
			return categoryProductListMap.get(category).get(uniqueID + "###" + ACTIVE);
		}
		
		//if it is already present in INACTIVE MODE, and with same Source.(Just update Specs)
		if(categoryProductListMap.get(category) != null 
				&& categoryProductListMap.get(category).containsKey(uniqueID + "###" + INACTIVE)
				&& BUYT_SOURCE.equals(categoryProductListMap.get(category).get(uniqueID + "###" + INACTIVE).get("source"))) {
			return categoryProductListMap.get(category).get(uniqueID + "###" + INACTIVE);
		}
		
		
		//Donot update if it is already present in ACTIVE MODE, but with different Source.
		//Create New Specs here with INACTIVE MODE, and send notification as it is available now from primary source
		if(categoryProductListMap.get(category) != null 
				&& categoryProductListMap.get(category).containsKey(uniqueID + "###" + ACTIVE)
				&& !BUYT_SOURCE.equals(categoryProductListMap.get(category).get(uniqueID + "###" + ACTIVE).get("source"))) {
		
			return new BasicDBObject("status", INACTIVE).append("msid", categoryProductListMap.get(category).get(uniqueID + "###" + ACTIVE).get("msid"));
		}
		
		//Send Notification, if it is already present in INACTIVE MODE, and with different Source.
		//(Create Specification with Active Mode here, and donot create Article in this case).
		if(categoryProductListMap.get(category) != null 
				&& categoryProductListMap.get(category).containsKey(uniqueID + "###" + INACTIVE)
				&& !BUYT_SOURCE.equals(categoryProductListMap.get(category).get(uniqueID + "###" + INACTIVE).get("source"))) {
		
			return new BasicDBObject("status", ACTIVE).append("msid", categoryProductListMap.get(category).get(uniqueID + "###" + INACTIVE).get("msid"));
		}
		*/
		
		int msid = 0;
		int parentId = 0;
		boolean primaryGadget = true;
		List<DBObject> reviewList = new ArrayList<DBObject>();
		
		String commonNameUniqueID = "";
		if(StringUtils.hasText(commonName))
			commonNameUniqueID = GadgetUtil.getUniqueID(commonName);
		//Temporary Check, until we move to completely on New Gadgets System.
		if(category.equals(MOBILES) && (oldGadgetsMap.containsKey(uniqueID) || oldGadgetsMap.containsKey(commonNameUniqueID))) {
			
			//CHANGE CONATINS TO mra.indiatimes.com
			if(StringUtils.hasText(commonName) &&
					MongoRestAPIService.MONGO_REST_API_URL.contains(CORRECT_MONGO_URL)) {
				DBObject obj = MongoRestAPIService.get(PRODUCT_DETAIL, new BasicDBObject("category", category).append("common_name", commonName).append("reviewid.hostid", GN_HOSTID));
				if(obj != null && obj.get("reviewid") != null && (parentId = getParameterFromReviewList((List<DBObject>) obj.get("reviewid"), "parentid")) > 0) {
					msid = addOverideLinkArticle(parentId, brandName, productName, category); //Multipublished this Article from this msid.
					primaryGadget = false;
				} else {
					parentId = (oldGadgetsMap.containsKey(uniqueID) && oldGadgetsMap.get(uniqueID) > 0) ? oldGadgetsMap.get(uniqueID) : (oldGadgetsMap.get(commonNameUniqueID) != null ? oldGadgetsMap.get(commonNameUniqueID) : 0);
					msid = addOverideLinkArticle(parentId, brandName, productName, category); //Multipublished this Article from this msid.
					primaryGadget = true;
				}
			} else {
				msid = (oldGadgetsMap.containsKey(uniqueID) && oldGadgetsMap.get(uniqueID) > 0) ? oldGadgetsMap.get(uniqueID) : (oldGadgetsMap.get(commonNameUniqueID) != null ? oldGadgetsMap.get(commonNameUniqueID) : 0);
				parentId = 0;
				primaryGadget = true;
			}
			
			reviewList.add(new BasicDBObject("msid", msid).append("parentid", parentId).append("hostid", GN_HOSTID));
			return new BasicDBObject("status", ACTIVE).append("reviewid", reviewList).append("seourl", oldGadgetSEOMap.get(uniqueID)).append("primary", primaryGadget ? 1 : 0);
		}
		
		//Temporary Check, to manually match BuyT gadgets with Old Gadgets.
		/*if(category.equals(MOBILES) && manuallyMatchedBuyTGadgets.containsKey(pid)) {
			reviewList.add(new BasicDBObject("msid", manuallyMatchedBuyTGadgets.get(pid)).append("hostid", TOI_HOSTID));
			return new BasicDBObject("status", ACTIVE).append("reviewid", reviewList);
		}*/
		
		//Product Doesn't exists
		logger.info("Going to Add article for Category:{}, Brand: {}, Product:{} with ID:{}", category, brandName, productName, uniqueID);

		//Temporary Check, until we move to laptops/Cameras on New Gadgets System.
		//CHANGE CONATINS TO mra.indiatimes.com
		if(/*category.equals(MOBILES) && */MongoRestAPIService.MONGO_REST_API_URL.contains(CORRECT_MONGO_URL)) {
			
			if(StringUtils.hasText(commonName)) {
				DBObject obj = MongoRestAPIService.get(PRODUCT_DETAIL, new BasicDBObject("category", category).append("common_name", commonName).append("reviewid.hostid", GN_HOSTID));
				//If Exists with CommonName
				if(obj != null && obj.get("reviewid") != null && (parentId = getParameterFromReviewList((List<DBObject>) obj.get("reviewid"), "parentid")) > 0) {
					msid = addOverideLinkArticle(parentId, brandName, productName, category); //Multipublished this Article from this msid.
					primaryGadget = false;
				}
				//Create Article only if the gadget(mobile/tablet) is not discontinued.
				else if(discontinued != 1) {
					int[] msidArray = addArticleWithOverideLinkArticle(brandName, productName, commonName, category); //Add Completely new Article with Multipublished Article also.
					msid = msidArray[1]; //MultipublishedId
					parentId = msidArray[0]; //Parent ArticleId from which 1st was multipublished.
					primaryGadget = true;
				}
				
				//Create Article only if the gadget(mobile/tablet) is not discontinued.				
			} else if(discontinued != 1) {
				msid = addArticle(brandName, productName, category);
				primaryGadget = true;
			}
		}
		
		reviewList.add(new BasicDBObject("msid", msid).append("parentid", parentId).append("hostid", GN_HOSTID));
		return new BasicDBObject("status", ACTIVE).append("reviewid", reviewList).append("primary", primaryGadget ? 1 : 0);
		
	}
	
	
	private int addArticle(String brandName, String productName, String category) {

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("brand", brandName));
		urlParameters.add(new BasicNameValuePair("product", productName));
		urlParameters.add(new BasicNameValuePair("category", category));
		
		try {
			String extData = ExternalHttpUtil.getExtData_PostCall("http://gadget.indiatimes.com/tech-gadget/addreview/", urlParameters);
			logger.info("MSID Generated:{}", extData);
			return Integer.parseInt(extData);
		} catch (HttpException | IOException e) {
			logger.error("Error occured while adding article for Brand:{}, Product:{}", brandName, productName, e);
		} catch(Exception e) {
			logger.error("Other Exception occured while adding article for Brand:{}, Product:{}", brandName, productName, e);
		}
		
		return 0;
		
	/*	try {
//			String strURL = "http://api.cmslitedev.indiatimes.com:8080/cmslite/api/workfolder/publish";
			String strURL = "http://denmark.timesinternet.in/cmslite/api/workfolder/publish";
			JSONObject json = new JSONObject();
			json.put("type", "2-0");
			json.put("ssoToken", getSSOToken());
			json.put("name", productName);
			json.put("story", productName);
			json.put("parents", new JSONArray().put(new JSONObject().put("parentId", getParentId(category, brandName)).put("hostId", 83)));
			json.put("activationStatus", "ACTIVE");
			json.put("entityType", "ARTICLE");
			json.put("persistableLiteMetadataMap", new JSONObject().put("436", new JSONObject().put("metatype", 436).put("value", "0").put("dataType", "CHARACTER"))
																.put("434", new JSONObject().put("metatype", 434).put("value", uName).put("dataType", "STRING")));
		
			String extData = ExternalHttpUtil.getExtData_PostCall(strURL, json);
			if(extData != null) {
				JSONObject extJson = new JSONObject(extData);
				return (Integer) extJson.get("msId");
			} else {
				logger.error("No data Found while adding article for Brand:{}, Product:{}", brandName, productName);
			}
		} catch (Exception e) {
			logger.error("Error occured while adding article for Brand:{}, Product:{}", brandName, productName, e);
		}
		
		return 0;*/
	}
	
	private int addOverideLinkArticle(int msid, String brandName, String productName, String category) {
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("msid", String.valueOf(msid)));
		urlParameters.add(new BasicNameValuePair("brand", brandName));
		urlParameters.add(new BasicNameValuePair("product", productName));
		urlParameters.add(new BasicNameValuePair("category", category));
		
		try {
			String extData = ExternalHttpUtil.getExtData_PostCall("http://gadget.indiatimes.com/tech-gadget/addreview/overidelinkarticle", urlParameters);
			logger.info("MSID Generated:{}", extData);
			return Integer.parseInt(extData);
		} catch (HttpException | IOException e) {
			logger.error("Error occured while adding article for Brand:{}, Product:{}", brandName, productName, e);
		} catch(Exception e) {
			logger.error("Other Exception occured while adding article for Brand:{}, Product:{}", brandName, productName, e);
		}
		
		return 0;
	}
	
	
	private int[] addArticleWithOverideLinkArticle(String brandName, String productName, String commonName, String category) {

		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("brand", brandName));
		urlParameters.add(new BasicNameValuePair("product", productName));
		urlParameters.add(new BasicNameValuePair("commonName", commonName));
		urlParameters.add(new BasicNameValuePair("category", category));
		
		try {
			String extData = ExternalHttpUtil.getExtData_PostCall("http://gadget.indiatimes.com/tech-gadget/addreview/articlewithoveridelink", urlParameters);
			logger.info("MSID Generated:{}", extData);
			
			return Arrays.asList(extData.replace("[", "").replace("]", "").split(","))
		            .stream()
		            .map(String::trim)
		            .mapToInt(Integer::parseInt).toArray();
			
		} catch (HttpException | IOException e) {
			logger.error("Error occured while adding article for Brand:{}, Product:{}", brandName, productName, e);
		} catch(Exception e) {
			logger.error("Other Exception occured while adding article for Brand:{}, Product:{}", brandName, productName, e);
		}
		
		return new int[]{0,0};
	}
	
	
	private int getOrCreateBrandSectionId(int discontinued, String category, String brandName) {
				
				
		if(discontinued == 1)
			return 0;
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("category", category));
		urlParameters.add(new BasicNameValuePair("brand", brandName));
		
		try {
			String extData = ExternalHttpUtil.getExtData_PostCall("http://gadget.indiatimes.com/tech-gadget/addreview/getorcreatesection", urlParameters);
			logger.info("MSID Generated:{}", extData);
			return Integer.parseInt(extData);
		} catch (HttpException | IOException e) {
			logger.error("Error occured while adding BrandSection in CMS for Category:{}, Brand:{}", category, brandName, e);
		} catch(Exception e) {
			logger.error("Other Exception occured while adding BrandSection in CMS for Category:{}, Brand:{}", category, brandName, e);
		}
		
		return 0;
	}
	
	
	private DBObject getLatestBasedOnInsertData(List <DBObject> productList){
		
		if(productList==null || productList.isEmpty()){
			return null;
		}
		
		if(productList.size()==1){
			return productList.get(0);
		}
		
		DBObject latestObject= productList.get(0);
		Date latestInsertedDate = (Date) productList.get(0).get("insertedAt");
		
		for(int i=1;i<productList.size();++i){
			Date insertDate = (Date) productList.get(i).get("insertedAt");
			
			if(insertDate.compareTo(latestInsertedDate)>0){
				latestInsertedDate=insertDate;
				latestObject=productList.get(i);
			}
		}
		
		return latestObject;
		
		
	}
	
	private void discontinueOtherExceptLatest(DBObject latestProduct,List<DBObject> sameSourceList){
		
		if(sameSourceList==null || sameSourceList.isEmpty()){
			return;
		}
		
		if(sameSourceList.size()==1){
			return;
		}
		
		for (DBObject product: sameSourceList){
			if(!product.equals(latestProduct)){
				
				//Already discontinued!
				try {
					if((int)product.get("discontinue")==1){
						continue;
					}
				} catch (Exception e) {
					
				}
				
				//discontinue
				DBObject query = new BasicDBObject();
				query.put("category", product.get("category"));
				query.put("pid", product.get("pid"));
				query.put("insertedAt", product.get("insertedAt"));
				query.put("uniqueid", product.get("uniqueid"));
				
				DBObject discontinue= new BasicDBObject().append("discontinue",1);
				//product.put("debug_info", "pId+discontinued");
				
				MongoRestAPIService.updateWithPost(PRODUCT_DETAIL, query, new BasicDBObject().append("$set", discontinue));
			}
		}
	}
	
	/*private int getParentId(String category, String brandName) {
		if(category.equals(MOBILES)) {
			if(mobBrandMsidMap.containsKey(brandName))
				return mobBrandMsidMap.get(brandName);
			
			return MISSING_MOBILE_BRAND_MSID; //'Missing Brands' in Mobiles
		}
		
		else if(category.equals(LAPTOPS)) {
			if(laptopBrandMsidMap.containsKey(brandName))
				return laptopBrandMsidMap.get(brandName);
			
			return MISSING_LAPTOP_BRAND_MSID; //'Missing Brands' in Laptops
		}
		
		else if(category.equals(CAMERAS)) {
			if(cameraBrandMsidMap.containsKey(brandName))
				return cameraBrandMsidMap.get(brandName);
			
			return MISSING_CAMERA_BRAND_MSID; //'Missing Brands' in Cameras
		}
		
		return MISSING_BRAND_LIST_MSID; //'Missing Brands' Overall for New category
	}

	private String getSSOToken() throws HttpException, IOException {
		if(ssoToken != null)
			return ssoToken;
		
		String user = "techgadgetuser@gmail.com";
		String pass = "$TechGadget4$";
		String ssoAPIURL = "http://jsso.indiatimes.com/sso/crossdomain/genericLogin?responsetype=json&siteId=32c05a0c7818edb2451260f7c7fac46c&login=" + user + "&passwd=" + pass;
		String ssoData = ExternalHttpUtil.getExtDataForSolar(ssoAPIURL);
		if(ssoData != null) {
			JSONObject ssoJson = new JSONObject(ssoData);
			ssoToken = (String) ssoJson.get("ticketId");
			return ssoToken;
		}
		
		return null;
	}*/
	
	
	public TechGadgetProductDAO getGadgetDAO() {
		return gadgetDAO;
	}

	public void setGadgetDAO(TechGadgetProductDAO gadgetDAO) {
		this.gadgetDAO = gadgetDAO;
	}

//	public static void main(String[] args) {
//		TechGadgetProductWritter writer = new TechGadgetProductWritter();
//		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
//		urlParameters.add(new BasicNameValuePair("brand", "XOLO"));
//		urlParameters.add(new BasicNameValuePair("product", "XOLO Note 3"));
//		urlParameters.add(new BasicNameValuePair("category", "1"));
//		
//		try {
//			String extData = ExternalHttpUtil.getExtData_PostCall("http://toistg.indiatimes.com/tech-gadget/addreview", urlParameters);
//			System.out.println(extData);
//		} catch (HttpException | IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println(writer.addArticle(null, "XOLO", "XOLO Note 3", "XOLO-Note-3", MOBILES));
//	}
	
	
	
	
	private int getIntValue(Object obj) {
		try {
			if(obj != null && !"".equals(obj) && !"null".equalsIgnoreCase(String.valueOf(obj)))
				return Integer.parseInt(String.valueOf(obj));
		} catch (Exception e) {
			logger.error("Error Reading integer value:{}", obj);
		}
		return 0;
	}
	
	
	public void init() {
		logger.info("Going to initialize Old Gadget List..");
		List<Map<String, Object>> oldGadgetLst = gadgetDAO.getOldGadgetsList();
//		Map<String, String> tempMap = new HashMap<String, String>();
		if(oldGadgetLst != null && !oldGadgetLst.isEmpty()) {
			for(Map<String, Object> gadget : oldGadgetLst) {
				oldGadgetsMap.put(GadgetUtil.getUniqueID(gadget.get("brand") + " " + gadget.get("label")), Integer.parseInt(String.valueOf(gadget.get("msid"))));
				//Need to remove below line if we are finding extra old gadgets not present in primary source.
				oldGadgetsMap.put(GadgetUtil.getUniqueID(String.valueOf(gadget.get("label"))), Integer.parseInt(String.valueOf(gadget.get("msid"))));
				
//				oldGadgetSEOMap.put(GadgetUtil.getUniqueID(gadget.get("brand") + " " + gadget.get("label")), gadget.get("brand") + "\t" + gadget.get("label") + "\t" + gadget.get("model"));
				oldGadgetSEOMap.put(GadgetUtil.getUniqueID(gadget.get("brand") + " " + gadget.get("label")), String.valueOf(gadget.get("model")));
				oldGadgetSEOMap.put(GadgetUtil.getUniqueID(String.valueOf(gadget.get("label"))), String.valueOf(gadget.get("model")));
				
//				tempMap.put(getUniqueID(gadget.get("brand") + " " + gadget.get("label")), gadget.get("brand") + " " + gadget.get("label"));
			}
		}
		
		/*logger.info("Going to initialize Brand Sections List..");
		List<Map<String, Object>> categoryBrandLst = gadgetDAO.getBrandFoldersList(MOBILE_BRAND_LIST_MSID);
		if(categoryBrandLst != null && !categoryBrandLst.isEmpty()) {
			for(Map<String, Object> gadget : categoryBrandLst) {
				mobBrandMsidMap.put(String.valueOf(gadget.get("msname")), Integer.parseInt(String.valueOf(gadget.get("msid"))));
			}
		}
		
		categoryBrandLst = gadgetDAO.getBrandFoldersList(LAPTOP_BRAND_LIST_MSID);
		if(categoryBrandLst != null && !categoryBrandLst.isEmpty()) {
			for(Map<String, Object> gadget : categoryBrandLst) {
				laptopBrandMsidMap.put(String.valueOf(gadget.get("msname")), Integer.parseInt(String.valueOf(gadget.get("msid"))));
			}
		}
		
		categoryBrandLst = gadgetDAO.getBrandFoldersList(CAMERA_BRAND_LIST_MSID);
		if(categoryBrandLst != null && !categoryBrandLst.isEmpty()) {
			for(Map<String, Object> gadget : categoryBrandLst) {
				cameraBrandMsidMap.put(String.valueOf(gadget.get("msname")), Integer.parseInt(String.valueOf(gadget.get("msid"))));
			}
		}*/
		
		
		//Initialize the Complete 91Mobiles Data for Each Category
		//http://mra.indiatimes.com/mra/get/product_detail?query={category:1, source:"BUYT"}&key={uniqueid:1,_id:0}
		logger.info("Going to initialize All Existing Mongo Products Data..");
		
		Map<String, Object> keyMap = new HashMap<String, Object>();
//		keyMap.put("key", "{name:1,uniqueid:1,reviewid:1,status:1,source:1,seourl:1,price:1,primary:1,_id:0}");
		keyMap.put("key", "{keyFeatures:0,specs:0,colors:0,_id:0}");
		
		//#CHANGE
		for(String category : categoryMap.values()) {
			List<DBObject> productList =  MongoRestAPIService.getList(PRODUCT_DETAIL, 
					new BasicDBObject("category", category), keyMap);
			if(productList != null) {
				categoryProductList4UniqueIDMap.put(category, productList.stream().collect(
						Collectors.groupingBy(dbObject -> String.valueOf(dbObject.get("pid")))));
			}
			//return;
		}
		
		
		//Initialize Old Collection to get the MSID From there..

//		keyMap = new HashMap<String, Object>();
//		keyMap.put("key", "{uniqueid:1,msid:1,_id:0}");
//		productList =  MongoRestAPIService.getList(PRODUCT_DETAIL_OLD, 
//				new BasicDBObject("category", MOBILES).append("msid", new BasicDBObject("$gt", 0)), keyMap);
//		
//		oldGadgetsMap.putAll(
//				productList.stream().collect(Collectors.toMap(
//						dbObject -> String.valueOf(dbObject.get("uniqueid")), dbObject -> Integer.parseInt(String.valueOf(dbObject.get("msid"))), (a, b) -> a)));
		
		
		
		
		List<DBObject> brandList =  MongoRestAPIService.getList(BRAND_DETAIL, new BasicDBObject());
		if(brandList != null) {
			categoryBrandListMap = brandList.stream().collect(
					Collectors.toMap(dbObject -> String.valueOf(dbObject.get("uniqueid")), Function.identity(), (a, b) -> a));

			
//			Map<String, DBObject> brandIdMap = new HashMap<String, DBObject>();
//			for(DBObject dbObject : brandList) {
//				if(categoryBrandListMap.containsKey(Integer.parseInt(String.valueOf(dbObject.get("category"))))) {
//					categoryBrandListMap.get(Integer.parseInt(String.valueOf(dbObject.get("category")))).put(String.valueOf(dbObject.get("uniqueid")), dbObject);
//				}
//				else {
//					brandIdMap = new HashMap<String, DBObject>();
//					brandIdMap.put(String.valueOf(dbObject.get("uniqueid")), dbObject);
//					categoryBrandListMap.put(Integer.parseInt(String.valueOf(dbObject.get("category"))), brandIdMap);
//				}
//			}
		}
		
		
		logger.info("Going to initialize ManuallyMatchedBuyTGadgetsMap..");
		initializeManuallyMatchedBuyTGadgetsMap();
		

		//Temp Code to print Extra Gadgets, Delete it later
		/*for(String uniqueID : oldGadgetsMap.keySet()) {
			if(categoryProductList4UniqueIDMap.get(MOBILES).containsKey(uniqueID)) {
//				System.out.println("Matched");
			}
			
			else {
				DBObject obj = MongoRestAPIService.get(PRODUCT_DETAIL, new BasicDBObject("reviewid.msid", oldGadgetsMap.get(uniqueID)));
				
				if(obj == null)
					obj = MongoRestAPIService.get(PRODUCT_DETAIL, new BasicDBObject("reviewid.parentid", oldGadgetsMap.get(uniqueID)));
				
				if(obj == null)
					System.out.println("UnMatched - UniqueID: " + uniqueID + "\t" +"MSID: " + oldGadgetsMap.get(uniqueID) + "\t" + oldGadgetSEOMap.get(uniqueID));
			}
		}*/
		
	}
	

	private void initializeManuallyMatchedBuyTGadgetsMap() {
		manuallyMatchedBuyTGadgets.put("buyt3012761" , 48252923);
		manuallyMatchedBuyTGadgets.put("buyt750838" , 46367445);
		manuallyMatchedBuyTGadgets.put("buyt1477447" , 49691082);
		manuallyMatchedBuyTGadgets.put("buyt4122340" , 49163643);
		manuallyMatchedBuyTGadgets.put("buyt4845714" , 49892754);
		manuallyMatchedBuyTGadgets.put("buyt8578499" , 37825350);
		manuallyMatchedBuyTGadgets.put("buyt7419319" , 46713726);
		manuallyMatchedBuyTGadgets.put("buyt1228442" , 47452086);
		manuallyMatchedBuyTGadgets.put("buyt1027068" , 31028593);
		manuallyMatchedBuyTGadgets.put("buyt9479338" , 48003209);
		manuallyMatchedBuyTGadgets.put("buyt2962214" , 48472385);
		manuallyMatchedBuyTGadgets.put("buyt1871361" , 47721989);
		manuallyMatchedBuyTGadgets.put("buyt4375944" , 48342383);
		manuallyMatchedBuyTGadgets.put("buyt878298454" , 48746588);
		manuallyMatchedBuyTGadgets.put("buyt2468588" , 46624051);
		manuallyMatchedBuyTGadgets.put("buyt8012652" , 46264146);
		manuallyMatchedBuyTGadgets.put("buyt7853238" , 48330616);
		manuallyMatchedBuyTGadgets.put("buyt9962383" , 48743766);
		manuallyMatchedBuyTGadgets.put("buyt8597137" , 46130840);
		manuallyMatchedBuyTGadgets.put("buyt2311304" , 47369955);
		manuallyMatchedBuyTGadgets.put("buyt8602738" , 47747041);
		manuallyMatchedBuyTGadgets.put("buyt4474327" , 48362385);
		manuallyMatchedBuyTGadgets.put("buyt7527033" , 16411370);
		manuallyMatchedBuyTGadgets.put("buyt7644246" , 47385339);
		manuallyMatchedBuyTGadgets.put("buyt5807979" , 42876696);
		manuallyMatchedBuyTGadgets.put("buyt4092361" , 49921519);
		manuallyMatchedBuyTGadgets.put("buyt5114182" , 48122785);
		manuallyMatchedBuyTGadgets.put("buyt2781501" , 49563447);
		manuallyMatchedBuyTGadgets.put("buyt7294154" , 50528797);
		manuallyMatchedBuyTGadgets.put("buyt6227556" , 49078106);
		manuallyMatchedBuyTGadgets.put("buyt4035849" , 14119539);
		manuallyMatchedBuyTGadgets.put("buyt8318894" , 50561187);
		manuallyMatchedBuyTGadgets.put("buyt1641101" , 49631066);
		manuallyMatchedBuyTGadgets.put("buyt5003354" , 50151490);
		manuallyMatchedBuyTGadgets.put("buyt2584084" , 31045930);
		manuallyMatchedBuyTGadgets.put("buyt7375619" , 50109863);
		manuallyMatchedBuyTGadgets.put("buyt698050452" , 47865437);
		manuallyMatchedBuyTGadgets.put("buyt4084609" , 47785781);
		manuallyMatchedBuyTGadgets.put("buyt6896450" , 48359235);
		manuallyMatchedBuyTGadgets.put("buyt870039145" , 47875730);
		manuallyMatchedBuyTGadgets.put("buyt1158510" , 47470909);
		manuallyMatchedBuyTGadgets.put("buyt735308" , 48698261);
		manuallyMatchedBuyTGadgets.put("buyt9502025" , 47516452);
		manuallyMatchedBuyTGadgets.put("buyt1927016" , 48100744);
		manuallyMatchedBuyTGadgets.put("buyt8545025" , 47675615);
		manuallyMatchedBuyTGadgets.put("buyt5214750" , 46630800);
		manuallyMatchedBuyTGadgets.put("buyt7179535" , 47014201);
		manuallyMatchedBuyTGadgets.put("buyt5485379" , 50038904);
		manuallyMatchedBuyTGadgets.put("buyt2800077" , 47085090);
		manuallyMatchedBuyTGadgets.put("buyt1446875" , 49644562);
		manuallyMatchedBuyTGadgets.put("buyt9629118" , 46053662);
		manuallyMatchedBuyTGadgets.put("buyt3526352" , 48544179);
		manuallyMatchedBuyTGadgets.put("buyt7898357" , 47372296);
		manuallyMatchedBuyTGadgets.put("buyt3813820" , 46262759);
		manuallyMatchedBuyTGadgets.put("buyt4583452" , 49583498);
		manuallyMatchedBuyTGadgets.put("buyt9548244" , 49816350);
		manuallyMatchedBuyTGadgets.put("buyt9884147" , 49104421);
		manuallyMatchedBuyTGadgets.put("buyt3423304" , 46865539);
		manuallyMatchedBuyTGadgets.put("buyt500000" , 47910642);
		manuallyMatchedBuyTGadgets.put("buyt4268511" , 49768039);
		manuallyMatchedBuyTGadgets.put("buyt3613107" , 46609982);
		manuallyMatchedBuyTGadgets.put("buyt5355712" , 49468783);
		manuallyMatchedBuyTGadgets.put("buyt4344650" , 49271521);
		manuallyMatchedBuyTGadgets.put("buyt5685340" , 48108787);
		manuallyMatchedBuyTGadgets.put("buyt3064623" , 46198850);
		manuallyMatchedBuyTGadgets.put("buyt6094769" , 47095943);
		manuallyMatchedBuyTGadgets.put("buyt401736" , 47368301);
		manuallyMatchedBuyTGadgets.put("buyt8142770" , 47151230);
		manuallyMatchedBuyTGadgets.put("buyt1182858" , 48168749);
		manuallyMatchedBuyTGadgets.put("buyt3762590" , 49214936);
		manuallyMatchedBuyTGadgets.put("buyt1228794" , 49469963);
		manuallyMatchedBuyTGadgets.put("buyt6632603" , 48264789);
		manuallyMatchedBuyTGadgets.put("buyt2053242" , 46921340);
		manuallyMatchedBuyTGadgets.put("buyt7331268" , 47429542);
		manuallyMatchedBuyTGadgets.put("buyt8585731" , 48901667);
		manuallyMatchedBuyTGadgets.put("buyt2864096" , 46621306);
		manuallyMatchedBuyTGadgets.put("buyt2497143" , 49072537);
		manuallyMatchedBuyTGadgets.put("buyt8698381" , 47354558);
		manuallyMatchedBuyTGadgets.put("buyt7485329" , 47247556);
		manuallyMatchedBuyTGadgets.put("buyt7450624" , 47639205);
		manuallyMatchedBuyTGadgets.put("buyt9244359" , 49056955);
		manuallyMatchedBuyTGadgets.put("buyt3499507" , 26561861);
		manuallyMatchedBuyTGadgets.put("buyt3102422" , 44781111);
		manuallyMatchedBuyTGadgets.put("buyt7643546" , 48777014);
		manuallyMatchedBuyTGadgets.put("buyt1347176" , 45501553);
		manuallyMatchedBuyTGadgets.put("buyt7486240" , 49229304);
		manuallyMatchedBuyTGadgets.put("buyt3533417" , 46422033);
		manuallyMatchedBuyTGadgets.put("buyt4781640" , 50558862);
		manuallyMatchedBuyTGadgets.put("buyt6612037" , 47815444);
		manuallyMatchedBuyTGadgets.put("buyt1850908" , 26345872);
		manuallyMatchedBuyTGadgets.put("buyt4039380" , 20745832);
		manuallyMatchedBuyTGadgets.put("buyt9580477" , 20140081);
		manuallyMatchedBuyTGadgets.put("buyt558724862" , 48254976);
		manuallyMatchedBuyTGadgets.put("buyt1155574" , 48369510);
		manuallyMatchedBuyTGadgets.put("buyt3831460" , 49090476);
		manuallyMatchedBuyTGadgets.put("buyt3739020" , 50168562);
		manuallyMatchedBuyTGadgets.put("buyt9520258" , 46999053);
		manuallyMatchedBuyTGadgets.put("buyt7415772" , 49847739);
		manuallyMatchedBuyTGadgets.put("buyt1905884" , 47840961);
		manuallyMatchedBuyTGadgets.put("buyt5604585" , 48543042);
		manuallyMatchedBuyTGadgets.put("buyt2194452" , 48698245);
		manuallyMatchedBuyTGadgets.put("buyt8869540" , 49982161);
		manuallyMatchedBuyTGadgets.put("buyt7391835" , 24668626);
		manuallyMatchedBuyTGadgets.put("buyt3489803" , 46444412);
		manuallyMatchedBuyTGadgets.put("buyt6364752" , 49255825);
		manuallyMatchedBuyTGadgets.put("buyt7553252" , 46838738);
		manuallyMatchedBuyTGadgets.put("buyt5589237" , 50437761);
		manuallyMatchedBuyTGadgets.put("buyt5129926" , 47910129);
		manuallyMatchedBuyTGadgets.put("buyt7592754" , 47830860);
		manuallyMatchedBuyTGadgets.put("buyt3564501" , 50450141);
		manuallyMatchedBuyTGadgets.put("buyt1956150" , 50079193);
		manuallyMatchedBuyTGadgets.put("buyt760647564" , 48357712);
		manuallyMatchedBuyTGadgets.put("buyt8814804" , 49246570);
		manuallyMatchedBuyTGadgets.put("buyt4196037" , 48375092);
		manuallyMatchedBuyTGadgets.put("buyt9943164" , 49072825);
		manuallyMatchedBuyTGadgets.put("buyt8785760" , 49815537);
	}

	/*public static void main(String[] args) throws ParseException {
		Map<String, Object> keyMap = new HashMap<String, Object>();
		keyMap.put("key", "{msid:1}");
//		keyMap.put("sort", "{insertDT:-1}");
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		List<DBObject> list = MongoRestAPIService.getList(PRODUCT_DETAIL, new BasicDBObject("msid", new BasicDBObject("$gt", 0)), keyMap);
		if(list != null && list.size()>0) {
			DBObject temp;
			DBObject finalObj = null;
			for(DBObject obj :list) {
				finalObj = null;
//				if(!map.containsKey(obj.get("uniqueid")))
//					map.put(obj.get("uniqueid"), obj.get("_id"));
				
				temp = new BasicDBObject();
				temp.put("_id", obj.get("_id"));
				
//				if(obj.get("status") == null) {
//					finalObj = new BasicDBObject().append("$set", new BasicDBObject("insertDT", new Date()));
//				}
				
//				if(finalObj != null) {
//					System.out.println(obj.get("name"));
				DecimalFormat df = new DecimalFormat("0.###E0");
				Number a = df.parse(String.valueOf(obj.get("msid")));
				System.out.println(a);
				MongoRestAPIService.update(PRODUCT_DETAIL, temp, new BasicDBObject().append("$set", new BasicDBObject("msid", 0)));
					MongoRestAPIService.update(PRODUCT_DETAIL, temp, new BasicDBObject().append("$set", new BasicDBObject("msid", Integer.valueOf(a.intValue()))));
//					MongoRestAPIService.update(PRODUCT_DETAIL, temp, new BasicDBObject().append("$set", new BasicDBObject("msid", "NumberInt("+a.intValue()+")")));
//				}
//				new BasicDBObject().append("$addToSet", new BasicDBObject("categorynew", Integer.parseInt(String.valueOf(obj.get("category")))))
			}
		}
	}*/
	
	public static void main(String[] args) {
		
//		List<DBObject> list = MongoRestAPIService.getList("gadget_product_detail_test", new BasicDBObject("reviewid.msid", new BasicDBObject("$in", Arrays.asList(51981450,51955749,51939719,51827920,51824940,51811823))));
		/*List<DBObject> list = MongoRestAPIService.getList("gadget_product_detail_test", new BasicDBObject("reviewid.msid", new BasicDBObject("$in", Arrays.asList(51908441,51928950))));
		list.addAll(MongoRestAPIService.getList("gadget_product_detail_test2", new BasicDBObject("reviewid.msid", new BasicDBObject("$in", Arrays.asList(51940655,51977179)))));
		if(list != null && list.size()>0) {
			DBObject temp;
			DBObject finalObj = null;
			MongoRestAPIService.MONGO_REST_API_URL = "http://mra.indiatimes.com/mra/";
			for(DBObject obj :list) {
				MongoRestAPIService.insert("product_detail", obj);
			}
		}*/
		
		Map<String, Object> keyMap = new HashMap<String, Object>();
		keyMap.put("key", "{common_name:1,uname:1}");
		
		List<DBObject> list = MongoRestAPIService.getList("gadget_product_detail", new BasicDBObject("lcuname", new BasicDBObject("$exists", false)), keyMap);
		if(list != null && list.size()>0) {
			DBObject temp;
			for(DBObject obj :list) {
				temp = new BasicDBObject();
				temp.put("_id", obj.get("_id"));
				MongoRestAPIService.update("gadget_product_detail", temp, new BasicDBObject().append("$set", 
						new BasicDBObject("lcuname", obj.get("uname").toString().toLowerCase())));
				
				
				if(obj.get("common_name") != null && !"".equals(obj.get("common_name")) && !"null".equalsIgnoreCase(String.valueOf(obj.get("common_name")))) {
					MongoRestAPIService.update("gadget_product_detail", temp, new BasicDBObject().append("$set", 
							new BasicDBObject()
							.append("common_uname", GadgetUtil.getUName(obj.get("common_name").toString()))
							.append("common_lcuname", GadgetUtil.getUName(obj.get("common_name").toString()).toLowerCase())));
				} else {
					MongoRestAPIService.update("gadget_product_detail", temp, new BasicDBObject().append("$unset", 
							new BasicDBObject("common_name", "")));
				}
				
			}
		}
	}

	public SimilarGadgetMatcherService getSimilarGadgetSearchService() {
		return similarGadgetSearchService;
	}

	public void setSimilarGadgetSearchService(SimilarGadgetMatcherService similarGadgetSearchService) {
		this.similarGadgetSearchService = similarGadgetSearchService;
	}
	
}
