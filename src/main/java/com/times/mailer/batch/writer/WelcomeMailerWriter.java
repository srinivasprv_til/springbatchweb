package com.times.mailer.batch.writer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
import com.times.common.util.StringUtils;
import com.times.mailer.batch.reader.WelcomeMailerReader;

public class WelcomeMailerWriter implements ItemWriter<JSONArray> {

	private final static Logger logger = LoggerFactory.getLogger(WelcomeMailerReader.class);

	private EmailProcessor emailProcessor;

	public EmailProcessor getEmailProcessor() {
		return emailProcessor;
	}

	public void setEmailProcessor(EmailProcessor emailProcessor) {
		this.emailProcessor = emailProcessor;
	}

	public static Connection sql_conn = null;

	private final static String SQL_INSERT_MsgQ = "insert into MsgQ "
			+ " (MsgSenderName, MsgSenderMailID, MsgSubject, MsgToList, Msg, MsgContentType, Msgcharset, SenderInfo, SenderIP, SenderUserAgent) "
			+ " values "
			+ " (:MsgSenderName,:MsgSenderMailID,:MsgSubject,:MsgToList,:Msg,:MsgContentType,:Msgcharset,:SenderInfo,:SenderIP,:SenderUserAgent)";

	private static String getMsgQ(String input, JSONObject newJsonObject) throws IOException {
		String channel = newJsonObject.getString("channel");
		if (channel.equals("News Point")) {
			input = input.replace(":MsgSenderName", "'News Point'");
			input = input.replace(":MsgSenderMailID", "'mailerservice@timesofindia.com'");
		} else {
			input = input.replace(":MsgSenderName", "'TOI'");
			input = input.replace(":MsgSenderMailID", "'mailerservice@timesofindia.com'");
			input = input.replace(":MsgSubject", "N'Make the most out of TIMES OF INDIA APP'");
		}

		// String email = newJsonObject.getString("email");
		String email = "ajit.kumar.1977@gmail.com";
		input = input.replace(":MsgToList", "'" + email.toString() + "'");
		input = input.replace(":MsgContentType", "'text/html'");
		input = input.replace(":Msgcharset", "'UTF-8'");
		// input = input.replace(":MsgCCList", "'NULL'");
		// input = input.replace(":MsgBCCList", "'NULL'");
		input = input.replace(":SenderInfo", "'Welcome Mailer'");
		InetAddress ip = null;
		try {

			ip = InetAddress.getLocalHost();

		} catch (UnknownHostException e) {

			e.printStackTrace();

		}
		input = input.replace(":SenderIP", "'" + ip.toString() + "'");
		input = input.replace(":SenderUserAgent", "'Java 8'");
		String languages = newJsonObject.getString("languages");

		if (languages.length() == 1) {
			if (languages.equals("1")) {
				input = input.replace(":MsgSubject", "N'Make the most out of News Point APP'");
			}
			if (languages.equals("2")) {
				input = input.replace(":MsgSubject", "N'News Point ऐप इस्तेमाल करने के 9 आसान टिप्स'");
			}
			if (languages.equals("3")) {
				input = input.replace(":MsgSubject", "N'न्यूजपॉइंट अॅपमध्ये वाचा सर्व बातम्या'");
			}
			if (languages.equals("4")) {
				input = input.replace(":MsgSubject", "N'নিউজপয়েন্ট অ্যাপ থেকে সেরাটা পান'");
			}
			if (languages.equals("5")) {
				input = input.replace(":MsgSubject", "N'ನ್ಯೂಸ್‌ಪಾಯಿಂಟ್ ಆ್ಯಪ್‌ನಿಂದ ಗರಿಷ್ಠ ಲಾಭ ಪಡೆದುಕೊಳ್ಳಿ'");
			}
			if (languages.equals("6")) {
				input = input.replace(":MsgSubject", "N'Make the most out of Newspoint App'");
			}
			if (languages.equals("7")) {
				input = input.replace(":MsgSubject", "N'ആസ്വദിക്കൂ ന്യൂസ് പോയിന്‍റ് ആപ്പ്'");
			}
			if (languages.equals("8")) {
				input = input.replace(":MsgSubject", "N'அதிகமாக நியூஸ்பாயின்ட் ஏப் பயன்படுத்தவும்'");
			}
			if (languages.equals("9")) {
				input = input.replace(":MsgSubject", "N'న్యూస్ పాయింట్ యాప్ నుంచి మరింత సమాచారం'");
			}
			input = input.replace(":Msg", "N\'" + getMsgData(languages, channel) + "\'");
		} else {
			input = input.replace(":MsgSubject", "N'Make the most out of Newspoint App'");
			input = input.replace(":Msg", "N\'" + getMsgData("1", channel) + "\'");
		}

		return input;
	}

	private static String getMsgSubject(String languages) {
		String input = null;
		if (languages.equals("1")) {
			input = "Make the most out of News Point APP";
		}
		if (languages.equals("2")) {
			input = "News Point ऐप इस्तेमाल करने के 9 आसान टिप्स";
		}
		if (languages.equals("3")) {
			input = "न्यूजपॉइंट अॅपमध्ये वाचा सर्व बातम्या";
		}
		if (languages.equals("4")) {
			input = "নিউজপয়েন্ট অ্যাপ থেকে সেরাটা পান";
		}
		if (languages.equals("5")) {
			input = "ನ್ಯೂಸ್‌ಪಾಯಿಂಟ್ ಆ್ಯಪ್‌ನಿಂದ ಗರಿಷ್ಠ ಲಾಭ ಪಡೆದುಕೊಳ್ಳಿ";
		}
		if (languages.equals("6")) {
			input = "Make the most out of Newspoint App";
		}
		if (languages.equals("7")) {
			input = "ആസ്വദിക്കൂ ന്യൂസ് പോയിന്‍റ് ആപ്പ്";
		}
		if (languages.equals("8")) {
			input = "அதிகமாக நியூஸ்பாயின்ட் ஏப் பயன்படுத்தவும்";
		}
		if (languages.equals("9")) {
			input = "న్యూస్ పాయింట్ యాప్ నుంచి మరింత సమాచారం";
		}
		return input;
	}

	private static String getMsgData(String language, String channel) throws IOException {
		String languageURL = null;
		if (language.equals("1") && channel.equals("News Point")) {
			languageURL = "http://navbharattimes.indiatimes.com/app_download_np.cms";
		}
		if (language.equals("1") && channel.equals("TOI")) {
			languageURL = "http://timesofindia.indiatimes.com/app_download_nl.cms";
		}
		if (language.equals("2")) {
			languageURL = "http://navbharattimes.indiatimes.com/app_download_nbt.cms?source=np";
		}
		if (language.equals("3")) {
			languageURL = "http://navbharattimes.indiatimes.com/app_download_mt.cms";
		}
		if (language.equals("4")) {
			languageURL = "http://navbharattimes.indiatimes.com/app_download_eis.cms";
		}
		if (language.equals("5")) {
			languageURL = "http://navbharattimes.indiatimes.com/app_download_vk.cms";
		}
		if (language.equals("6")) {
			languageURL = "http://navbharattimes.indiatimes.com/app_download_ngs.cms";
		}
		if (language.equals("7")) {
			languageURL = "http://navbharattimes.indiatimes.com/app_download_mal.cms";
		}
		if (language.equals("8")) {
			languageURL = "http://navbharattimes.indiatimes.com/app_download_tam.cms";
		}
		if (language.equals("9")) {
			languageURL = "http://navbharattimes.indiatimes.com/app_download_tl.cms";
		}
		if (languageURL != null) {
			URL url = new URL(languageURL);
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			String encoding = con.getContentEncoding();
			encoding = encoding == null ? "UTF-8" : encoding;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buf = new byte[8192];
			int len = 0;
			while ((len = in.read(buf)) != -1) {
				baos.write(buf, 0, len);
			}
			String body = new String(baos.toByteArray(), encoding);
			body = StringEscapeUtils.escapeSql(body);
			return body;
		} else {
			return null;
		}

	}

	private static Connection getConnection() {
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:sqlserver://192.168.22.85:1433;DatabaseName=mailer";

		// Database credentials
		final String USER = "cmsweb";
		final String PASS = "st@lk$";

		Connection conn = null;
		Statement stmt = null;

		try {

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

		} catch (Exception e) {
			logger.error("error while connecting to sql", e);
		}

		return conn;

	}

	private static void insertIntoSql(String sql) {
		if (sql_conn == null) {
			sql_conn = getConnection();
		}
		Statement stmt = null;

		try {
			stmt = sql_conn.createStatement();
			stmt.execute(sql);

			stmt.close();
		} catch (SQLException se) {
			logger.error("Error while inserting into sql db", se);
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			}
		}

		logger.debug("Inserted into sql in MsgQ table!");
	}

	@Override
	public void write(List<? extends JSONArray> item) throws Exception {
		JSONArray listOFJson = item.get(0);
		for (int i = 0; i < listOFJson.length(); i++) {
			EmailVO emailVO = new EmailVO();
			JSONObject jsonObject = listOFJson.getJSONObject(i);
			String email = jsonObject.getString("email");
			String channel = jsonObject.getString("channel");
			String languages = jsonObject.getString("languages");

			if (!org.apache.commons.lang3.StringUtils.isEmpty(languages)
					&& !org.apache.commons.lang3.StringUtils.isEmpty(email)
					&& !org.apache.commons.lang3.StringUtils.isEmpty(channel)) {
				if (channel.equals("News Point")) {
					emailVO.setSender("News Point <mailerservice@timesofindia.com>");

					if (languages.length() == 1) {
						emailVO.setSubject(getMsgSubject(languages));
						emailVO.setBody(getMsgData(languages, channel));
					} else {
						emailVO.setSubject("Make the most out of Newspoint App");
						emailVO.setBody(getMsgData("1", channel));
					}

				}
				if (channel.equals("TOI")) {
					emailVO.setSender("TOI <mailerservice@timesofindia.com>");
					emailVO.setSubject("Make the most out of TIMES OF INDIA APP");
					emailVO.setBody(getMsgData(languages, channel));
				}

				emailVO.setRecipients(email);

				try {
					logger.debug("email:{},subject:{},msg:{}", emailVO.getRecipients(), emailVO.getSubject(),
							emailVO.getBody().length());

					if (emailVO.getRecipients() != null && emailVO.getSubject() != null && emailVO.getBody() != null) {
						emailProcessor.sendEMail(emailVO);
					}
				} catch (NullPointerException nullPointer) {
					logger.error("null pointer exception {}", nullPointer);
				}

			}

			// String sql = SQL_INSERT_MsgQ;
			//
			// String executeSql = getMsgQ(sql, listOFJson.getJSONObject(i));
			// executeSql = executeSql.replace("##MsgNotBefore##",
			// "dateadd(minute,1,GETDATE ())");
			// insertIntoSql(executeSql);
		}

		if (sql_conn != null) {
			try {
				sql_conn.close();
				sql_conn = null;
			} catch (SQLException e) {
				logger.error("sql error while closing the sql connection", e);
			}
		}

	}

}
