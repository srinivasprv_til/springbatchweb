/**
 * 
 */
package com.times.mailer.batch.writer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.dao.CricketMYSQLDaoInterface;
import com.times.common.dao.EhcacheBatchDaoInterface;
import com.times.common.service.EhcacheBatchService;
import com.times.common.service.FileContentServices;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.model.FileContentInfo;
import com.times.mailer.model.FileLocationInfo;

/**
 * This class is used to insert / updata match id in MySql.
 * 
 * @author Geetanjali
 *
 */
public class CricketMySQLInsertUpdateWriter implements ItemWriter<Map<String,String>> {

	private static final Log log = LogFactory.getLog(CricketMySQLInsertUpdateWriter.class);
	
	CricketMYSQLDaoInterface cricketMYSQLDaoInterface;
	public CricketMYSQLDaoInterface getCricketMYSQLDaoInterface() {
		return cricketMYSQLDaoInterface;
	}
	public void setCricketMYSQLDaoInterface(CricketMYSQLDaoInterface cricketMYSQLDaoInterface) {
		this.cricketMYSQLDaoInterface = cricketMYSQLDaoInterface;
	}

	@Override
	public void write(List<? extends Map<String,String>> items) throws Exception {
		try{
			int ctr=0;
			if(items != null){
				while(items.iterator().hasNext() && ctr < items.size()){
					System.out.println(ctr++);
					
					Map<String,String> matchidmap = items.iterator().next();
					for (Entry<String, String> entry : matchidmap.entrySet()) {
						
						String matchid = entry.getKey();
						System.out.println("matchid-"+matchid);
						String matchstatus = entry.getValue();
						int dbmatchid = getCricketMYSQLDaoInterface().selectMatchID(matchid);
						if("2".equals(matchstatus)){
							if(!(Integer.parseInt(matchid)==dbmatchid)){
								int i = getCricketMYSQLDaoInterface().insertMatchID(matchid, matchstatus);
								log.info(matchid +" inserted - DB - " + i);
							}
						}else if("0".equals(matchstatus)){
							if((Integer.parseInt(matchid)==dbmatchid)){
								int i = getCricketMYSQLDaoInterface().updateMatchID(matchid, matchstatus);
								log.info(matchid +" updated - DB - " + i);
							}
						}
					}
				}
			}
		}catch(Exception e){
			log.error("nothing has done - "+e.getMessage());
			e.printStackTrace();
		}
		
	}
}