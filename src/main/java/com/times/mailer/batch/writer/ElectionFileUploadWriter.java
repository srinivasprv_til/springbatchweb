/**
 * 
 */
package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.service.FileContentServices;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.model.FileContentInfo;
import com.times.mailer.model.FileLocationInfo;

/**
 * This Writer class is used to write for election content with some special content check. like "NO DATA FOUND".
 * 
 * @author Ranjeet.Jha
 *
 */
public class ElectionFileUploadWriter implements ItemWriter<FileLocationInfo> {
	
	private static final Log log = LogFactory.getLog(ElectionFileUploadWriter.class);
	
	private FileContentServices fileContentServices;

	@Override
	public void write(List<? extends FileLocationInfo> items) throws Exception {
		//log.debug("inside the write method of writter");
		String ftpLocation = null;
		String sourceUrl = null;
		try {
			if (items != null && items.size() > 0) {
				for (FileLocationInfo fileUploadInfo : items) {
					ftpLocation = fileUploadInfo.getNetstorageLocation().getServerName() + fileUploadInfo.getNetstorageLocation().getFolderLocation() + "/" + fileUploadInfo.getNetstorageLocation().getFileName();
					sourceUrl = fileUploadInfo.getSourceUrl().trim();
					String fileContent = fileContentServices.getContentByHTTP(sourceUrl);
					
					if (fileContent != null && !fileContent.contains("NO DATA FOUND")) {
						System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName());
						fileContentServices.uploadContentByFTP(fileUploadInfo.getNetstorageLocation(), fileContent);
						
						// upload content to Amazon S3
						//fileContentServices.uploadFileAmazonS3(fileUploadInfo.getAmazonS3(), fileContent);
						log.debug("uploaded size: " + fileContent.length() + " source url : " + sourceUrl + " target url: " + ftpLocation);
					} else {
						log.debug("\"NO DATA FOUND\" found in content so not uploaded, url : " + sourceUrl); 
					}
				}
			}
		} catch (Exception e) {
			new CMSCallExceptions("Exception caught while ftp update. ftp url: " + ftpLocation + " msg: " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			log.error("exception caught while ftp update. ftp url: " + ftpLocation);
		}
	}

	/**
	 * @param fileContentServices the fileContentServices to set
	 */
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}

}