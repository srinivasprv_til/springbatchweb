/**
 * 
 */
package com.times.mailer.batch.writer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
/**
 * @author ranjeet.jha
 *
 */
public class SMSMailerWritter implements ItemWriter<List<EmailVO>> {
	
	private static final Log log = LogFactory.getLog(SMSMailerWritter.class);
	
	private EmailProcessor smtpMailer;
	
	

	
	public void write(List<? extends List<EmailVO>> emailVOList) throws Exception {
		log.debug("inside the write  ");
		for (List<EmailVO> voList : emailVOList) {
			
			String allMobiles = null;
			StringBuilder mobileNos = new StringBuilder();
			for (EmailVO emailVo : voList) {
				if (emailVo.getMobile() != null && !"null".equalsIgnoreCase(emailVo.getMobile())) {
					mobileNos.append(emailVo.getMobile()).append(",");
				}
			}
			if (mobileNos != null && mobileNos.length() > 0) {
				allMobiles = mobileNos.substring(0, mobileNos.length()-1);
				System.out.println(allMobiles);
			}
			if (allMobiles != null) {
				sendSMS(allMobiles, voList.get(0).getSmsmessage());
			}
			
			for (EmailVO emailVo : voList) {
				if (emailVo != null && emailVo.getBody() != null) {
					log.debug("email Sent to : " + emailVo.getRecipients());
					System.out.println("email Sent to : " + emailVo.getRecipients());
					smtpMailer.sendEMail(emailVo);
				}
			}
		}
			
	}
	
	
	
	/**
	 * 
	 * @param response
	 * @param mobilenumber
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ProtocolException
	 */
	private String sendSMS(String mobilenumbers, String message) throws UnsupportedEncodingException, MalformedURLException, IOException, ProtocolException {
		String retval = "";
		try {
			
			String postData = "";

			
			String User = "times_internet";  
			String passwd = "times@1234"; 
			
			String sid = "TIMES"; 
			String mtype = "N";
			String DR = "Y";

			postData += "User=" + URLEncoder.encode(User, "UTF-8") + "&passwd=" + passwd + "&mobilenumber="
					+ mobilenumbers + "&message=" + URLEncoder.encode(message, "UTF-8") + "&sid=" + sid + "&mtype="
					+ mtype + "&DR=" + DR;
			URL url = new URL("http://smscountry.com/SMSCwebservice.asp");
			HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();


			urlconnection.setRequestMethod("POST");
			urlconnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			urlconnection.setDoOutput(true);
			OutputStreamWriter out = new OutputStreamWriter(urlconnection.getOutputStream());
			out.write(postData);
			out.close();
			BufferedReader in = new BufferedReader(new InputStreamReader(urlconnection.getInputStream()));
			String decodedString;

			while ((decodedString = in.readLine()) != null) {
				retval += decodedString;
			}
			in.close();

		} catch (Exception e) {
			
		}
		return retval;
	}
	

	/**
	 * @return
	 */
	public EmailProcessor getSmtpMailer() {
		return smtpMailer;
	}

	/**
	 * @param smtpMailer
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}

}