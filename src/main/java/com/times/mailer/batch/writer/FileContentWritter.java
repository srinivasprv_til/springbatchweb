/**
 * 
 */
package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.service.FileContentServices;
import com.times.common.service.upload.FileUploadServiceFactory;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.FileLocationInfo.LocationType;

/**
 * This Writer of the batch is used to update the content to the Akamai Netstorage specified location.
 * 
 * @author Ranjeet.Jha
 *
 */
public class FileContentWritter  implements ItemWriter<FileLocationInfo> {

	private static final Log log = LogFactory.getLog(FileContentWritter.class);

	private NetstorageFileInfoDao netstorageFileInfoDao;
	private FileContentServices fileContentServices;

	//private EmailProcessor smtpMailer;

	/**
	 * @param netstorageFileInfoDao the netstorageFileInfoDao to set
	 */
	public void setNetstorageFileInfoDao(NetstorageFileInfoDao netstorageFileInfoDao) {
		this.netstorageFileInfoDao = netstorageFileInfoDao;
	}

	@Override
	public void write(List<? extends FileLocationInfo> items) throws Exception {
		//log.debug("inside the write method of writter");
		String location = null;
		try {
			if (items != null && items.size() > 0) {
				for (FileLocationInfo fileUploadInfo : items) {
					//FileLocationInfo fileUploadInfo = items.get(0);
					// TODO : get location according to new value
					if (fileUploadInfo.getLocationType() == LocationType.NET_STORAGE) {
						location = fileUploadInfo.getNetstorageLocation().getServerName()
								+ fileUploadInfo.getNetstorageLocation().getFolderLocation() + "/"
								+ fileUploadInfo.getNetstorageLocation().getFileName();

					} else if (fileUploadInfo.getLocationType() == LocationType.AWS) {
						location = fileUploadInfo.getAmazonS3().getBucketName() + "/"
								+ fileUploadInfo.getAmazonS3().getFileName();
					}
					String sourceUrl = fileUploadInfo.getSourceUrl().trim();
					String fileContent = fileContentServices.getContentByHTTP(sourceUrl);
					if (fileContent != null) {

						//System.out.println("uploading content : " + fileUploadInfo.getNetstorageLocation().getFileName() + " , fileName: " + fileUploadInfo.getNetstorageLocation().getFileName());
						if (isValidContent(fileUploadInfo, fileContent)) {
							// TODO : use factory to get bean
							FileUploadServiceFactory.getUploadServiceByLocation(fileUploadInfo.getLocationType())
									.upload(fileUploadInfo, fileContent);
							}
						log.debug("uploaded size: " + fileContent.length() + " source url : " + sourceUrl + " target url: " + location);

						// update last_updated_at column for record 
						netstorageFileInfoDao.updateFileLocationInfo(fileUploadInfo);

					}
				}
			}
		} catch (Exception e) {
			new CMSCallExceptions("Exception caught while ftp update. ftp url: " + location + " msg: " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			log.error("exception caught while ftp update. ftp url: " + location);
		}
	}

	/**
	 * @param fileContentServices the fileContentServices to set
	 */
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}

	private boolean isValidContent(FileLocationInfo fileUploadInfo, String content) {
		boolean isValid = false;

		// in case of live blog check the ma
		if (fileUploadInfo.getWorkType() == 4 && fileUploadInfo.getSourceUrl().contains("/liveblogcontent/json/?msid=")) {
			int length = content.length();
			if (length > 25) {
				isValid = true;
			}
		} else if(fileUploadInfo.getWorkType() == 7) {
			int length = content.length();
			if (length > 50) {
				isValid = true;
			}
		}else {
			isValid = true;
		}
		return isValid;
	}

	/*public static void main(String... args ) {
		List<FileLocationInfo> list = populateFileLocationInfo();
		try {
			for (FileLocationInfo fileLocationInfo : list) {

				String fileContent = NetstorageUtil.getData(fileLocationInfo.getSourceUrl());
				if (fileContent != null) {
					NetstorageUtil.uploadFileContent(fileLocationInfo.getNetstorageLocation(), fileContent);
					System.out.println("hi");
					//NetstorageUtil.uploadFileContent(fileLocationInfo.getNetstorageLocation(), fileContent);
					System.out.println("folder : " + fileLocationInfo.getNetstorageLocation().getFolderLocation());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

}
