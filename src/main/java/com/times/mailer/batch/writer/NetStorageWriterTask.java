package com.times.mailer.batch.writer;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.times.common.service.FileContentServices;
import com.times.common.service.upload.FileUploadServiceFactory;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.FileLocationInfo.LocationType;

public class NetStorageWriterTask implements Runnable{
	
	private NetstorageFileInfoDao netstorageFileInfoDao;
	private FileContentServices fileContentServices;	
	private FileLocationInfo fileUploadInfo;
	
	private final static Logger logger = LoggerFactory.getLogger(NetStorageWriterTask.class);
	
	public NetStorageWriterTask(NetstorageFileInfoDao netstorageFileInfoDao, FileContentServices fileContentServices, FileLocationInfo fileUploadInfo){
		this.netstorageFileInfoDao = netstorageFileInfoDao;
		this.fileContentServices = fileContentServices;
		this.fileUploadInfo = fileUploadInfo;
	}

	@Override
	public void run() {
		String location = null;
		try {
			
			if (fileUploadInfo.getLocationType() == LocationType.NET_STORAGE) {
				location = fileUploadInfo.getNetstorageLocation().getServerName()
						+ fileUploadInfo.getNetstorageLocation().getFolderLocation() + "/"
						+ fileUploadInfo.getNetstorageLocation().getFileName();

			} else if (fileUploadInfo.getLocationType() == LocationType.AWS) {
				location = fileUploadInfo.getAmazonS3().getBucketName() + "/"
						+ fileUploadInfo.getAmazonS3().getFileName();
			}			
			
			String sourceUrl = fileUploadInfo.getSourceUrl().trim();
			String fileContent = fileContentServices.getContentByHTTP(sourceUrl,5000, 15000);
			boolean ftpUploadStatus=false;
			if (fileContent != null) {

				if (isValidContent(fileUploadInfo, fileContent)) {
					if (fileUploadInfo.getSourceUrl().contains("pagination=true")) {
						netstorageUploadWithPagination(fileContent,ftpUploadStatus, sourceUrl, location);
					} else {
						FileUploadServiceFactory.getUploadServiceByLocation(fileUploadInfo.getLocationType())
						.upload(fileUploadInfo, fileContent);
						/*logger.debug("Uploaded size: {} for source url :{} and target url :{} and content:{}", 
								fileContent.length(), sourceUrl, location, fileContent);*/
					}
				}
				/*if(ftpUploadStatus){*/
				logger.debug("Uploaded size: {} for source url :{} and target url :{}", fileContent.length(), sourceUrl, location);
				// update last_updated_at column for record 
				netstorageFileInfoDao.updateFileLocationInfo(fileUploadInfo);
				/*}*/
			}
			
		} catch (Exception e) {
			logger.error("exception caught while ftp update. ftp url: {}", location, e);
		}
		
	}
	
	private void netstorageUploadWithPagination(String fileContent, boolean ftpUploadStatus, String sourceUrl, String location) {
		logger.debug("netstorageUploadWithPagination API call for source url :{} and target url :{}", fileContent.length(), sourceUrl, location);
		long startTime = System.currentTimeMillis();
		if (fileContent.indexOf("(") != -1 && fileContent.lastIndexOf(")") != -1) {
			String callback = fileContent.substring(0,fileContent.indexOf("("));
			fileContent = fileContent.substring(fileContent.indexOf("(")+1,fileContent.lastIndexOf(")"));
			int perPage = 60;
			int pageNo = 0;
			int loop;
			JSONArray jsonArray = new JSONArray(fileContent);		

			if (jsonArray != null && jsonArray.length() > 0) {
				int pagecount = (jsonArray.length()/perPage) + 1;
				loop = perPage;	
				String fileName = null;
				if (fileUploadInfo.getLocationType() == LocationType.NET_STORAGE) {
					fileName = fileUploadInfo.getNetstorageLocation().getFileName();
				} else if (fileUploadInfo.getLocationType() == LocationType.AWS) {
					fileName = fileUploadInfo.getAmazonS3().getFileName();
				}
				for (int idex = 1; idex <= pagecount; idex++) {
					JSONArray ja = new JSONArray();
								
				
					loop = perPage * idex;

					if (loop > jsonArray.length()) {
						loop = jsonArray.length();
					}
					for (; pageNo < loop; pageNo++) {					    		
						JSONObject explrObject = jsonArray.getJSONObject(pageNo);
						ja.put(explrObject);
					}
					try {
						if (fileUploadInfo.getLocationType() == LocationType.NET_STORAGE) {
							fileUploadInfo.getNetstorageLocation().setFileName(fileName.replace("#page", String.valueOf(idex)));
						} else if (fileUploadInfo.getLocationType() == LocationType.AWS) {
							fileUploadInfo.getAmazonS3().setFileName(fileName.replace("#page", String.valueOf(idex)));
						}
						
						ftpUploadStatus = FileUploadServiceFactory.getUploadServiceByLocation(fileUploadInfo.getLocationType())
								.upload(fileUploadInfo, callback + " (" + ja.toString() + ")");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					logger.debug("Uploaded size: {} for source url :{} and target url :{}", fileContent.length(), sourceUrl, location);
				}
			}
			logger.debug("upload time in pagination ms : " + (System.currentTimeMillis() - startTime) + " of fileContent ");
		}
	}
	
	private boolean isValidContent(FileLocationInfo fileUploadInfo, String content) {
		boolean isValid = false;

		// in case of live blog check the ma
		if (fileUploadInfo.getWorkType() == 4 && fileUploadInfo.getSourceUrl().contains("/liveblogcontent/json/?msid=")) {
			int length = content.length();
			if (length > 25) {
				isValid = true;
			}
		} else if(fileUploadInfo.getWorkType() == 7) {
			int length = content.length();
			if (length > 50) {
				isValid = true;
			}
		}else {
			isValid = true;
		}
		return isValid;
	}

}
