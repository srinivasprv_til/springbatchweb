package com.times.mailer.batch.writer;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
import com.times.common.service.FileContentServices;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.FileLocationInfo;

public class AmazonS3FileWritter implements ItemWriter<FileLocationInfo> {

	private static final Log log = LogFactory.getLog(FileContentWritter.class);

	private NetstorageFileInfoDao netstorageFileInfoDao;
	private FileContentServices fileContentServices;
	//headerPreserve variable if tuned from *jobContext.xml file if we have to fetch and store headers with content.
	private boolean headerPreserve = false;
	
	private EmailProcessor smtpMailer;

	

	/**
	 * @param netstorageFileInfoDao the netstorageFileInfoDao to set
	 */
	public void setNetstorageFileInfoDao(NetstorageFileInfoDao netstorageFileInfoDao) {
		this.netstorageFileInfoDao = netstorageFileInfoDao;
	}

	@Override
	public void write(List<? extends FileLocationInfo> items) throws Exception {
		String ftpLocation = null;

		try {
			if (items != null && items.size() > 0) {
				for (FileLocationInfo fileUploadInfo : items) {

					ftpLocation = fileUploadInfo.getNetstorageLocation().getServerName() + fileUploadInfo.getNetstorageLocation().getFolderLocation() + "/" + fileUploadInfo.getNetstorageLocation().getFileName();
					String sourceUrl = fileUploadInfo.getSourceUrl().trim();

					// if headerPreserve is enabled
					if (headerPreserve) {
						Map<Object, Object> resultMap = null;
						try {
							resultMap = fileContentServices.getContentAndHeaderByHTTP(sourceUrl);
						} catch (Exception e) {
							log.debug("Exception caught while fetch data from url: " + sourceUrl);
						}
						if (resultMap != null && resultMap.size() > 0) {
							String fileContent = (String) resultMap.get("Content");
							Map headerMap = (Map) resultMap.get("Header");
							if (fileContent != null) {
								fileContentServices.uploadFileAmazonS3(fileUploadInfo.getAmazonS3(), fileContent, headerMap);
								log.debug("uploaded size: " + fileContent.length() + " source url : " + sourceUrl + " target url: " + ftpLocation
										+ "Expires : " + headerMap.get("Expires") + " Last-Modified " + headerMap.get("Last-Modified"));						
								// update last_updated_at column for record 
								netstorageFileInfoDao.updateFileLocationInfo(fileUploadInfo);
							}
						} else {						
							sourceUrl = getAlternateSourceURL(sourceUrl);							
							
							Map<Object, Object> resultRetryMap = null;
							try {
								resultRetryMap = fileContentServices.getContentAndHeaderByHTTP(sourceUrl);
							} catch (Exception e) {
								log.debug("Exception caught while fetch data from url: " + sourceUrl);
							}
							if (resultRetryMap != null && resultRetryMap.size() > 0) {
								String fileContent = (String) resultRetryMap.get("Content");
								Map headerMap = (Map) resultRetryMap.get("Header");
								if (fileContent != null) {
									fileContentServices.uploadFileAmazonS3(fileUploadInfo.getAmazonS3(), fileContent, headerMap);
									
									log.debug("uploaded size: " + fileContent.length() + " source url : " + sourceUrl + " target url: " + ftpLocation
											+ "Expires : " + headerMap.get("Expires") + " Last-Modified " + headerMap.get("Last-Modified"));	
									// update last_updated_at column for record 
									netstorageFileInfoDao.updateFileLocationInfo(fileUploadInfo);
								}
							} else {
								sendFailMailer(sourceUrl);							
							}
						}

					} else {

						String fileContent = fileContentServices.getContentByHTTP(sourceUrl);
						if (fileContent != null) {					

							fileContentServices.uploadFileAmazonS3(fileUploadInfo.getAmazonS3(), fileContent, null);
							log.debug("uploaded size: " + fileContent.length() + " source url : " + sourceUrl + " target url: " + ftpLocation);						

						}
					}
				}
			}
		} catch (Exception e) {
			new CMSCallExceptions("Exception caught while update. url: " + ftpLocation + " msg: " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			log.error("exception caught while ftp update. ftp url: " + ftpLocation);
		}
	}
	
	
	private String getAlternateSourceURL(String sourceUrl) {
		
		sourceUrl = sourceUrl.replaceAll("http://s3.timesofindia", "http://www2.timesofindia")
				.replaceAll("http://s3.spm.timesofindia.indiatimes.com", "http://sp.m.timesofindia.com")
				.replaceAll("http://s3.m.timesofindia.indiatimes.com", "http://www1.m.timesofindia.com")
				.replaceAll("s3.navbharattimes", "www2.navbharattimes")
				.replaceAll("s3.maharashtratimes", "www2.maharashtratimes")
				.replaceAll("s3.eisamay", "www1.eisamay")
				.replaceAll("s3.navgujaratsamay", "www1.navgujaratsamay")
				.replaceAll("s3.vijaykarnataka", "www1.vijaykarnataka")
				.replaceAll("s3.samayam", "www1.samayam")
				.replaceAll("s3.tamil", "www1.tamil")
				.replaceAll("s3.telugu", "www1.telugu")
				.replaceAll("s3.malayalam", "www1.malayalam");

		return sourceUrl;

	}
	
	
	private void sendFailMailer(String sourceUrl) {
		if (sourceUrl.indexOf("failmailer") != -1) {
			String body = "Amazon " + sourceUrl + " Home Page Content Upload Service is down.";
			String subject = "Amazon " + sourceUrl + " Home Page Content Upload Service is down.";
			String recipients = "JCMS-DEV@timesinternet.in,Cmsonduty@timesinternet.in";
			reportMailer(body, subject, recipients);
		}
	}
	
	/**
	 * 
	 */
	private void reportMailer(String body, String subject, String recipients) {
		EmailVO vo = new EmailVO();
		vo.setBody(body);
		vo.setSubject(subject);
		vo.setRecipients(recipients);
		try {
			smtpMailer.sendEMail(vo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param fileContentServices the fileContentServices to set
	 */
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}

	public boolean isHeaderPreserve() {
		return headerPreserve;
	}

	public void setHeaderPreserve(boolean headerPreserve) {
		this.headerPreserve = headerPreserve;
	}

	public EmailProcessor getSmtpMailer() {
		return smtpMailer;
	}

	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}


}