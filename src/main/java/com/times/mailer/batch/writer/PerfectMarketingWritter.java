package com.times.mailer.batch.writer;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.dao.PerfectMarketingDAO;
import com.times.common.model.PerfectMarketingObject;

public class PerfectMarketingWritter implements ItemWriter<LinkedHashMap<String, Date>> {
	
	private static final Log log = LogFactory.getLog(PerfectMarketingWritter.class);

	public static final String PM_RESULT_COLLECTION = "perfectm_result";
	public static final String PM_UNSUCESSFUL_COLLECTION = "perfectm_unsuccessful";
	public static final String PM_FAILED_COLLECTION = "perfectm_failed";
	String extUrl = "http://economictimes-ws.indiatimes.perfectmarket.com/map.xml?url=economictimes.com-#msid#";

	private PerfectMarketingDAO dao;

	
	public void write(List<? extends LinkedHashMap<String, Date>> item) throws Exception {
		log.debug("pm write method start..");
		try {
		LinkedHashMap<String, Date> lhmap = item.get(0);
		Set<String> keys =  lhmap.keySet();
		ArrayList outList = new ArrayList<String>();

		for (String msid : keys) {
			Date date = (Date) lhmap.get(msid);
			String url = extUrl.replaceAll("#msid#", msid);
			String pmURL = null;
			pmURL = MongoRestAPIService.getExtData(url);

			PerfectMarketingObject pmObj = new PerfectMarketingObject();

			if (pmURL != null && !pmURL.equals("null")) {
				pmURL = pmURL.substring(pmURL.indexOf("http://"),
						pmURL.indexOf("</uri>"));
				log.debug("found URL ~~~~" + pmURL + " for msid -> " + msid );
					//if (dao.getPMDBObject(msid, PM_UNSUCESSFUL_COLLECTION) == null) {
						if (dao.getPMDBObject(msid, PM_RESULT_COLLECTION, pmURL) == null) {
							int serialNo = dao
									.getSerialNo(PM_RESULT_COLLECTION);
							pmObj.setSerialnumber(serialNo + 1);
							pmObj.setMsid(Integer.valueOf(msid));
							pmObj.setRedirecturl(pmURL);
							pmObj.setDateadded(date);
							pmObj.setNoattempt(1);
							if (dao.addPMObject(pmObj, PM_RESULT_COLLECTION)) {
								dao.updateSerialNo(PM_RESULT_COLLECTION,
										serialNo + 1);
							}
						} else {
							pmObj = dao.getPMObject(msid, PM_RESULT_COLLECTION);
							PerfectMarketingObject pmUpdateObj = pmObj.clone();
							pmUpdateObj.setDateadded(date);
							pmUpdateObj.setRedirecturl(pmURL);
							dao.updatePMObject(pmObj, pmUpdateObj,
									PM_RESULT_COLLECTION);
						}
					//}
					
					/*else {
						pmObj = dao
								.getPMObject(msid, PM_UNSUCESSFUL_COLLECTION);
						dao.deletePMObject(msid, PM_UNSUCESSFUL_COLLECTION);
						int serialNo = dao
								.getSerialNo(PM_UNSUCESSFUL_COLLECTION);
						if (serialNo > 0) {
							serialNo -= 1;
						}
						dao.updateSerialNo(PM_UNSUCESSFUL_COLLECTION, serialNo);

						PerfectMarketingObject pmUpdateObj = pmObj.clone();
						pmUpdateObj.setRedirecturl(pmURL);
						pmUpdateObj.setDateadded(date);
						pmUpdateObj.setNoattempt(pmObj.getNoattempt() + 1);
						serialNo = dao.getSerialNo(PM_RESULT_COLLECTION);
						pmObj.setSerialnumber(serialNo + 1);
						if (dao.addPMObject(pmObj, PM_RESULT_COLLECTION)) {
							dao.updateSerialNo(PM_RESULT_COLLECTION,
									serialNo + 1);
						}
					}*/
				//System.out.println(pmURL);
				log.debug(pmURL);
				outList.add(pmURL);
				
				DBObject document = new BasicDBObject();
				document.put("PM_URL", pmURL);
				
				MongoRestAPIService.updateWithPost("ETPMOld", document,document);
			}
			else {
				log.debug("********* NO URL found for MSID ----> " + msid);
			}
			/*else {
				try {
					if (dao.getPMDBObject(msid, PM_RESULT_COLLECTION) == null) {
						if (dao.getPMDBObject(msid, PM_FAILED_COLLECTION) == null) {
							if (dao.getPMDBObject(msid,
									PM_UNSUCESSFUL_COLLECTION) == null) {
								int serialNo = dao
										.getSerialNo(PM_UNSUCESSFUL_COLLECTION);
								pmObj.setSerialnumber(serialNo + 1);
								pmObj.setMsid(Integer.valueOf(msid));
								pmObj.setRedirecturl(pmURL);
								pmObj.setDateadded(date);
								pmObj.setNoattempt(1);
								if (dao.addPMObject(pmObj,
										PM_UNSUCESSFUL_COLLECTION)) {
									dao.updateSerialNo(
											PM_UNSUCESSFUL_COLLECTION,
											serialNo + 1);
								}
							} else {
								pmObj = dao.getPMObject(msid,
										PM_UNSUCESSFUL_COLLECTION);
								if (pmObj.getNoattempt() >= 2) {
									int serialNo = dao
											.getSerialNo(PM_UNSUCESSFUL_COLLECTION);

									if (dao.deletePMObject(msid,
											PM_UNSUCESSFUL_COLLECTION)) {
										dao.updateSerialNo(
												PM_UNSUCESSFUL_COLLECTION,
												serialNo - 1);
										PerfectMarketingObject pmUpdateObj = pmObj
												.clone();
										pmUpdateObj.setNoattempt(pmObj
												.getNoattempt() + 1);
										pmUpdateObj.setRedirecturl(pmURL);
										pmUpdateObj.setDateadded(date);
										if (dao.addPMObject(pmUpdateObj,
												PM_FAILED_COLLECTION)) {
											dao.updateSerialNo(
													PM_FAILED_COLLECTION,
													dao.getSerialNo(PM_FAILED_COLLECTION) + 1);
										}
									}
								} else {
									PerfectMarketingObject pmUpdateObj = pmObj
											.clone();
									pmUpdateObj.setNoattempt(pmObj
											.getNoattempt() + 1);
									pmUpdateObj.setRedirecturl(pmURL);
									dao.updatePMObjectByMsid(msid, pmUpdateObj,
											PM_UNSUCESSFUL_COLLECTION);
								}
							}
						}
					}
				} catch (Exception e) {
					log.error("Exception occurred in writter method with error = "
							+ e.getMessage());
				}
				// System.out.println(pmURL);
				log.debug(pmURL);
				outList.add(pmURL);
			}*/

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				log.error("Exception occurred in sleep method with error = "
						+ e.getMessage());
			}
			
			
		}
		log.debug("pm write method end..");
		} catch (Exception e) {
			log.error("Exception occurred in writter method with error = "
					+ e.getMessage());
		}
	}

	public PerfectMarketingDAO getDao() {
		return dao;
	}


	public void setDao(PerfectMarketingDAO dao) {
		this.dao = dao;
	}

}
