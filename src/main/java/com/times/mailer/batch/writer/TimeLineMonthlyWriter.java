package com.times.mailer.batch.writer;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.node.ArrayNode;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.jdom.Document;
import org.jdom.Element;
import org.springframework.batch.item.ItemWriter;

import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.EmailVO;
import com.times.common.util.InMemoryData;
import com.times.mailer.model.MsidObject;
import com.times.common.model.articleKeyWordDetails;

/**
 * 
 * @author Dipali Patidar
 *
 */
public class TimeLineMonthlyWriter implements ItemWriter<List<articleKeyWordDetails>> {

	private static final Log log = LogFactory.getLog(TimeLineMonthlyWriter.class);

	public void write(List<? extends List<articleKeyWordDetails>> documents) throws Exception {
		int order = 1;

		log.debug("Initiating writer");
		if (documents != null && documents.get(0) != null) {
			List<articleKeyWordDetails> articleKeyWordDetailsList = documents.get(0);

			for (int i = 0; i < articleKeyWordDetailsList.size(); i++) {

				articleKeyWordDetails keywordDetails = articleKeyWordDetailsList.get(i);
				String url = keywordDetails.getUrl();
				String Msid = url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf("."));

				DBObject query = new BasicDBObject();

				DBObject update = new BasicDBObject();

				DBObject dbObj = new BasicDBObject();

				dbObj.put("Quote", keywordDetails.getSentence());
				dbObj.put("Person", keywordDetails.getPerson());
				dbObj.put("Organization", keywordDetails.getOrganization());
				dbObj.put("Position", keywordDetails.getPosition());
				dbObj.put("Order", order);
				dbObj.put("Status", "Active");
				dbObj.put("Msid", Msid);

				//query.put("TP", 100);
				query.put("Msid", Msid);
				query.put("Order", order);
				update.put("$set", dbObj);

				MongoRestAPIService.updateWithPost("TopArticles_Keywords", query, update);

				order++;
			}
		}

		log.debug("end of writer");
	}
}
