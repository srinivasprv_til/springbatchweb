/**
 * 
 */
package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemWriter;

import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
import com.times.mailer.batch.MailerWritter;

/**
 * This batch writer is used to send the mail to the subscribed user.
 * 
 * @author Ranjeet.Jha
 *
 */
public class TravelNewsletterWriter implements ItemWriter<EmailVO>, StepExecutionListener {
	
	private static final Log log = LogFactory.getLog(MailerWritter.class);
	
	private EmailProcessor smtpMailer;

	/* (non-Javadoc)
	 * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
	 */
	public void write(List<? extends EmailVO> mail) throws Exception {
		log.debug("inside the write ");
		if (mail != null && mail.get(0) != null && mail.get(0).getBody() != null) {
			System.out.println("email Sent to : " + mail.get(0).getRecipients());
			//log.debug("email Sent to : " + mail.get(0).getRecipients());
			//smtpMailer.sendEMail(mail.get(0));
			smtpMailer.sendSendPalEMail(mail.get(0));
		}		
	}

	/**
	 * @return
	 */
	public EmailProcessor getSmtpMailer() {
		return smtpMailer;
	}

	/**
	 * @param smtpMailer
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		System.out.println("afterStep TravelNewsletterWriter");
		smtpMailer.getMailSender().destroy();
		return stepExecution.getExitStatus();
	}
}