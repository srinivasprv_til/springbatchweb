package com.times.mailer.batch.writer;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.service.AmazonS3StorageService;
import com.times.common.util.Constant;
import com.times.common.util.EpaperOptimizerJpdf;

public class TempJob {
	public static void main(String[] args) throws Exception {
		AmazonS3StorageService amazonS3StorageService = new AmazonS3StorageService();
		
		EpaperOptimizerJpdf epaperOptimizerJpdf = new EpaperOptimizerJpdf();
		
		DBObject query = new BasicDBObject();
		query.put("date", "031116");
		List<DBObject> files = MongoRestAPIService.getList(Constant.EPAPER_RAW_DATA_COLLECTION, query );
		for (DBObject fileDbo : files) {
			ObjectId id= new ObjectId((String) fileDbo.get("gfsid"));
			DBObject fileQuery = new BasicDBObject();
			fileQuery.put("_id", id);
			byte[] content = MongoRestAPIService.getFile(Constant.EPAPER_FILE_COLLECTION, fileQuery);
			Files.write(Paths.get("/Users/arun.sharma2/" + id + ".pdf"), content, StandardOpenOption.CREATE);
			File targetFile = Files.createTempFile("epaper_file_", ".zip").toFile();
			epaperOptimizerJpdf.optimize(content, targetFile);
			String gfsid = MongoRestAPIService.addFile(Constant.EPAPER_FILE_COLLECTION, targetFile);
			fileDbo.removeField("_id");
			fileDbo.put("gfsid", gfsid);
			fileDbo.put("status", 2);
			MongoRestAPIService.insertWithPost(Constant.EPAPER_PROCESSED_DATA_COLLECTION, fileDbo);
			amazonS3StorageService.upload(Files.readAllBytes(targetFile.toPath()), fileDbo.get("fileName").toString());
			System.out.println(fileDbo + " processed");
			
		}
	}

}
