package com.times.mailer.batch.writer;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.service.AmazonS3StorageService;
import com.times.common.util.Constant;
import com.times.mailer.model.EpaperFileDetails;

public class EpaperFileWritter implements ItemWriter<EpaperFileDetails> {

	private static final Log log = LogFactory.getLog(EpaperFileWritter.class);
	AmazonS3StorageService amazonS3StorageService;

	public void write(List<? extends EpaperFileDetails> epaperInfoList) throws Exception {
		log.debug("inside the write ");
		if (epaperInfoList != null && epaperInfoList.size() > 0) {
			EpaperFileDetails efd = epaperInfoList.get(0);
			if (efd != null) {
				Path zipFile = Files.createTempFile("epaper_zip_", ".zip");
				Files.write(zipFile, efd.getZipFileContent());
				String zipGfsid = MongoRestAPIService.addFile(Constant.EPAPER_FILE_COLLECTION, zipFile.toFile());
				ObjectId id = new ObjectId(efd.getId());
				DBObject query = new BasicDBObject();
				query.put("_id", id);
				DBObject storedDBObject = MongoRestAPIService.get(Constant.EPAPER_RAW_DATA_COLLECTION, query);
				storedDBObject.put("status", 2);
				storedDBObject.put("gfsid", zipGfsid);
				storedDBObject.put("fileName", storedDBObject.get("fileName").toString().replace(".pdf", ".zip"));
				MongoRestAPIService.insert(Constant.EPAPER_PROCESSED_DATA_COLLECTION, storedDBObject);
				
				// Upload to s3 with pdf file name
				amazonS3StorageService.upload(efd.getZipFileContent(), efd.getFileName().replace(".pdf", ".zip"));
				

				DBObject storedObject = MongoRestAPIService.get(Constant.EPAPER_RAW_DATA_COLLECTION, query);
				storedObject.put("status", 2);
				
				MongoRestAPIService.findAndUpdate(Constant.EPAPER_RAW_DATA_COLLECTION, query, storedObject);
			}
		}
	}

	public AmazonS3StorageService getAmazonS3StorageService() {
		return this.amazonS3StorageService;
	}

	public void setAmazonS3StorageService(AmazonS3StorageService amazonS3StorageService) {
		this.amazonS3StorageService = amazonS3StorageService;
	}

}