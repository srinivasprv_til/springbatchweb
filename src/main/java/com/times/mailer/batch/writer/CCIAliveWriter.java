package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.model.ETEpaperArticle;
import com.times.common.util.InMemoryData;

/**
 * 
 * @author praveen.bokka
 *
 */
public class CCIAliveWriter implements ItemWriter<List<DBObject>> {
	
	private static final Log log = LogFactory.getLog(CCIAliveWriter.class);
	
	public void write(List<? extends List<DBObject>> item) throws Exception {
		log.debug("etEpaper write method start..");
		
		List<DBObject> dbList = null;
		if(item != null) {
			dbList = item.get(0);
		}
		
		if(dbList != null){
			//try inserting into mongo
			//if fails do not update status
			try {
				for(DBObject document : dbList){
					DBObject query = new BasicDBObject();
					query.put("CCIId", document.get("CCIId"));
					log.debug("Inserting msid: "+document.get("msid") +" CCIId" + document.get("CCIId"));
					BasicDBObject setObject = new BasicDBObject();
					setObject.append("$set", document);
					MongoRestAPIService.updateWithPost("CCIAlive",query, setObject);
					//though status is true every time, if one op fails catch will update status to false
					//atleast one object need to be inserted for status to be true
					InMemoryData.etEpaperSuccess = true;
				}
			} catch (Exception e) {
				InMemoryData.etEpaperSuccess = false;
			}
		}
		
		log.debug("etEpaper write method end..");
	}

}
