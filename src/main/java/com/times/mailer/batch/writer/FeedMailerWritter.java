/**
 * 
 */
package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;

/**
 * @author Rajeev Khatri
 *
 */
public class FeedMailerWritter implements ItemWriter<List<EmailVO>> {

	private static final Log log = LogFactory.getLog(FeedMailerWritter.class);

	private EmailProcessor smtpMailer;

	/* (non-Javadoc)
	 * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
	 */
	public void write(List<? extends List<EmailVO>> emailVOList) throws Exception {
		log.debug("inside the write  ");
		for (List<EmailVO> voList : emailVOList) {
			for (EmailVO emailVo : voList) {
				if (emailVo != null && emailVo.getBody() != null) {
					log.debug("email Sent to : " + emailVo.getRecipients());
					log.debug("Subject : " + emailVo.getSubject());
					System.out.println("email Sent to : " + emailVo.getRecipients());
					smtpMailer.sendEMail(emailVo);
				}
			}
		}

	}

	/**
	 * @return
	 */
	public EmailProcessor getSmtpMailer() {
		return smtpMailer;
	}

	/**
	 * @param smtpMailer
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}
}