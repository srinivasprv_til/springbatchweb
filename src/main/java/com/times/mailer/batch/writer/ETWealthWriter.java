package com.times.mailer.batch.writer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.InMemoryData;

public class ETWealthWriter implements ItemWriter<List<DBObject>> {
	
	private static final Log log = LogFactory.getLog(ETWealthWriter.class);
	
	private static final String ET_WEALTH_COLLECTION = "ETWealth";

	
	public void write(List<? extends List<DBObject>> item) throws Exception {
		log.debug("etwealth write method start..");
		
		List<DBObject> dbList = item.get(0);
		if(dbList != null){
			for(DBObject document : dbList){
				MongoRestAPIService.insert(ET_WEALTH_COLLECTION, document);
			}
			InMemoryData.success_status_ibeat = true;
		}
	}

}
