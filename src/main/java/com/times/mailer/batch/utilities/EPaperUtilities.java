package com.times.mailer.batch.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.DBObject;
import com.times.common.util.Constant;
import com.times.mailer.model.EpaperInfo;

public class EPaperUtilities {
	private final static Logger logger = LoggerFactory.getLogger(EPaperUtilities.class);
	@SuppressWarnings("unchecked")
	public static EpaperInfo prepareEpaperInfoObject(DBObject dbObjectRe) {

		EpaperInfo epaperInfo = new EpaperInfo();
		epaperInfo.setEdition(String.valueOf(dbObjectRe.get(Constant.EDITION)));
		epaperInfo.setPublisher(String.valueOf(dbObjectRe.get(Constant.PUBLISHER)));
		epaperInfo.setLocation(String.valueOf(dbObjectRe.get(Constant.LOCATION)));
		epaperInfo.setUnique_key(String.valueOf(dbObjectRe.get(Constant.UNIQUE_KEY)));
		epaperInfo.setStatus(String.valueOf(dbObjectRe.get(Constant.STATUS)));
		DBObject directoryLocations = (DBObject) dbObjectRe.get(Constant.EPAPER_FTP_LOCATION);
		Set<String> keys = directoryLocations.keySet();
		for (String key : keys) {
			List<String> value = (List<String>) directoryLocations.get(key);
			logger.debug("key: {}, value: {}", key, value);
			epaperInfo.getDirectoryDetails().put(key, value);
		}

		return epaperInfo;
	}

	public static String getFormatedDate(Date date) {
		return new SimpleDateFormat("ddMMYY").format(date);
	}

}
