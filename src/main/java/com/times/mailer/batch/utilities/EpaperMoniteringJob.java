package com.times.mailer.batch.utilities;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.Constant;
import com.times.mailer.batch.reader.EpaperReader;
import com.times.mailer.model.EpaperInfo;
import com.times.mailer.model.FTPFileWithDirectory;

public class EpaperMoniteringJob {

	private final static Logger	logger				= LoggerFactory.getLogger(EpaperMoniteringJob.class);
	private static final String	EPAPER_RETRIVE_JOB	= "/opt/script/EPaperJob.sh";
	private static final String	EPAPER_PROCESS_JOB	= "/opt/script/EPaperProcessJob.sh";

	public static void main(String[] args) throws Exception {
		logger.info("Monitering job started");
		DBObject query = new BasicDBObject();
		query.put(Constant.STATUS, 1);

		List<DBObject> dbObjects = MongoRestAPIService.getList(Constant.EPAPER_COLLECTION, query);

		if (dbObjects != null) {
			List<EpaperInfo> epaperInfos = dbObjects.stream().map((dbo) -> EPaperUtilities.prepareEpaperInfoObject(dbo))
			        .collect(Collectors.toList());
			for (EpaperInfo epaperInfo : epaperInfos) {
				try {
					if (!verifyEdition(epaperInfo)) {
						rerunJobs();
						break;
					} else {
						logger.error("Epaper {} varified", epaperInfo);
						updateMasterCollection(epaperInfo);
					}
				} catch (Exception e) {
					logger.error("Error occured", e);
					rerunJobs();
					break;
				}
			}
		} else {
			logger.error("failed to retrieve from mra.");
		}

		logger.info("Shutting down EpaperMoniteringJob");
	}

	private static void updateMasterCollection(EpaperInfo epaperInfo) {
		DBObject query = new BasicDBObject();
		query.put(Constant.UNIQUE_KEY, epaperInfo.getUnique_key());
		DBObject dbObject = MongoRestAPIService.get(Constant.EPAPER_COLLECTION, query);
		String storedCurrentDate = (String) dbObject.get(Constant.CURRENT_DATE_KEY);
		String currentDate = EPaperUtilities.getFormatedDate(getDate());

		List<DBObject> listOfFileForEdition = getListOfFileForEdition(epaperInfo, getDate());

		if (listOfFileForEdition != null && listOfFileForEdition.size() > 0) {
			/*
			 * Number of processed file is > 0
			 */
			if (storedCurrentDate == null || !storedCurrentDate.equals(currentDate)) {
				/*
				 * if current date is not set or current date is not equal to
				 * today's date, update the master collection
				 */
				logger.debug("Updating master collection. currentDate {}, storedCurrentDate: {}", currentDate,
				        storedCurrentDate);
				dbObject.put(Constant.CURRENT_DATE_KEY, currentDate);
				dbObject.put(Constant.PREVIOUS_DATE_KEY, storedCurrentDate);
				DBObject queryJson = new BasicDBObject();
				queryJson.put(Constant.UNIQUE_KEY, epaperInfo.getUnique_key());
				boolean success = MongoRestAPIService.findAndUpdate(Constant.EPAPER_COLLECTION, queryJson, dbObject);
				if (success) {
					logger.debug("master collection updated");
				} else {
					logger.debug("error while updating master collection. mra returned false");
				}
			} else {
				logger.debug("storedCurrent date is equal to current date. storedCurrentDate: {}", storedCurrentDate);
			}
		} else {
			logger.debug("No new file present in mongo. files: {}, epaperInfo: {}", listOfFileForEdition, epaperInfo);
		}

	}

	private static void rerunJobs() throws Exception {
		logger.info("Restarting jobs");
		Runtime runtime = Runtime.getRuntime();
		Process retriveJobProcess = runtime.exec(EPAPER_RETRIVE_JOB);
		logger.info("Epaper retrive job started");
		retriveJobProcess.waitFor();
		logger.info("Epaper retrieve job finished");
		Process processJobProcess = runtime.exec(EPAPER_PROCESS_JOB);
		logger.info("Epaper process job started");
		processJobProcess.waitFor();
		logger.info("Epaper process job finished");
		logger.info("Rerun of job completed");

	}

	/**
	 * return true if current edition is processed properly false if job is
	 * needed to run again
	 * 
	 * @param epaperInfo
	 * @return
	 * @throws Exception
	 */
	private static boolean verifyEdition(EpaperInfo epaperInfo) throws Exception {
		Date date = getDate();
		FTPEpaperService ftpEpaperService = new FTPEpaperService(epaperInfo, date);
		List<FTPFileWithDirectory> files = ftpEpaperService.listFiles();

		files = EpaperReader.filterFiles(files);

		List<DBObject> savedInMongo = getListOfFileForEdition(epaperInfo, date);

		if (savedInMongo != null) {
			logger.info("retrieved pdf file list of {} elements", savedInMongo.size());
			Set<String> processedFiles = savedInMongo.stream().map((dbo) -> getPdfFileName(dbo))
			        .collect(Collectors.toSet());

			Set<String> filesListFromFTP = files.stream().map((ftpfile) -> ftpfile.getName())
			        .collect(Collectors.toSet());

			ftpEpaperService.completePendingCommands();
			return processedFiles.equals(filesListFromFTP);
		} else {
			logger.error("Error while retriving data saved in mongodb {}", epaperInfo);
			// Retrun false as files could not be varified
			return false;
		}

	}

	private static List<DBObject> getListOfFileForEdition(EpaperInfo epaperInfo, Date date) {
		DBObject query = new BasicDBObject();
		String parsedDate = EPaperUtilities.getFormatedDate(date);
		query.put("ukey", epaperInfo.getUnique_key());
		query.put("date", parsedDate);
		query.put("status", 2);

		logger.info("about to hit mra for list of processed pdf files");
		List<DBObject> savedInMongo = MongoRestAPIService.getList(Constant.EPAPER_PROCESSED_DATA_COLLECTION, query);
		return savedInMongo;
	}

	private static String getPdfFileName(DBObject dbo) {
		return ((String) dbo.get("fileName")).replace(".zip", ".pdf");

	}

	private static Date getDate() {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		return date;

	}

}
