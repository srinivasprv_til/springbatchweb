package com.times.mailer.batch.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.times.common.util.Constant;
import com.times.mailer.model.EpaperInfo;
import com.times.mailer.model.FTPFileWithDirectory;

public class FTPEpaperService {
	private final static String	FTP_IP		= "10.100.109.86";
	private final static String	USER_NAME	= "online";
	private final static String	PASSWORD	= "online@321";
	private final static String	DIRECTORY	= "PDF@epaper";
	FTPClient					ftpClient;
	EpaperInfo					ePaperInfo;
	Date						date;

	private FTPClient getFtpClient() {
		return this.ftpClient;
	}

	private EpaperInfo getePaperInfo() {
		return this.ePaperInfo;
	}

	private Date getDate() {
		return this.date;
	}

	private final static Logger logger = LoggerFactory.getLogger(FTPEpaperService.class);

	public FTPEpaperService(EpaperInfo ePaperInfo, Date date) throws Exception {
		this.ePaperInfo = ePaperInfo;
		this.date = date;

		ftpClient = new FTPClient();

		ftpClient.connect(FTP_IP);
		boolean constatus = ftpClient.login(USER_NAME, PASSWORD);
		if (!constatus) {
			logger.error("login in ftp server failed");
			throw new Exception("login in ftp server failed");
		}
		ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

	}

	public List<FTPFileWithDirectory> listFiles() throws IOException {
		List<String> directories = getDirectories();

		List<List<FTPFileWithDirectory>> fileTree = new ArrayList<>();

		/*
		 * Not using nested stream as to complicate code and due to checked
		 * exceptions
		 */
		for (String directory : directories) {
			FTPFile[] files = getFtpClient().listFiles(directory);
			List<FTPFileWithDirectory> listing = Arrays.stream(files)
			        .map(file -> new FTPFileWithDirectory(file, directory)).collect(Collectors.toList());
			fileTree.add(listing);
		}

		// flat map the tree file structure
		List<FTPFileWithDirectory> result = fileTree.stream().flatMap(List::stream).collect(Collectors.toList());

		return result;

	}

	public InputStream retrieveFileStream(FTPFileWithDirectory file) throws IOException {
		String remoteFileName = file.getName();
		String path = file.getPath();
		ftpClient.changeWorkingDirectory(path);
		InputStream is = ftpClient.retrieveFileStream(remoteFileName);
		return is;
	}

	public void completePendingCommands() {
		try {
			if (!ftpClient.completePendingCommand()) {
				logger.error("Error while comleting pending commands");
			}
		} catch (Exception e) {
			logger.error("Error while completing ftp connection:", e);
		}
	}

	private List<String> getDirectories() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(getDate());
		/*
		 * Day of week start from Sunday(1) and end on Saturday(7)
		 */
		int dayValue = cal.get(Calendar.DAY_OF_WEEK);
		List<String> directories = getePaperInfo().getDirectoriesForKey(String.valueOf(dayValue));
		if (directories == null || directories.isEmpty()) {
			directories = getePaperInfo().getDirectoriesForKey(Constant.DEFAULT_DIRECTORY_KEY);
		}

		directories = directories.stream().map(dir -> getPDFFolder(dir)).collect(Collectors.toList());

		return directories;

	}

	public void closeSession() throws IOException {
		getFtpClient().logout();
		getFtpClient().disconnect();
	}

	private String getPDFFolder(String dir) {
		return dir + "/" + getFormatedDate() + "/" + DIRECTORY;
	}

	private String getFormatedDate() {
		return new SimpleDateFormat("ddMMYY").format(getDate());
	}

}
