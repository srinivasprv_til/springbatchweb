package com.times.mailer.batch.utilities;

import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class TimeoutCallable<R> {
	
	public R timeout(Duration timeout, Callable<R> callable) throws InterruptedException, ExecutionException, TimeoutException {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		R response;
		final Future<R> handler = executor.submit(callable);

		try {
		    response = handler.get(timeout.toMillis(), TimeUnit.MILLISECONDS);
		} catch (TimeoutException e) {
		    handler.cancel(true);
		    throw e;
		} finally {
			executor.shutdownNow();
		}
		return response; 
	}

}
