package com.times.mailer.batch.processor;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.jdom.Document;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.model.ETEpaperArticle;

/**
 * 
 * @author praveen.bokka
 *
 */
public class ETPMMissingProcessor implements ItemProcessor<List<DBObject>, List<DBObject>> {

	private static final Logger log = LoggerFactory.getLogger(ETPMMissingProcessor.class);
	
	public static final String PM_RESULT_COLLECTION = "perfectm_result";
	
	public List<DBObject> process(List<DBObject> hitUrls) throws Exception {
		log.debug("etPMMissing process method start..");
		List<DBObject> missingUrls = new ArrayList<DBObject>();
		
		for(DBObject hitUrl : hitUrls) {
			DBObject query = new BasicDBObject();
			String pmUrl = (String) hitUrl.get("PM_URL");
			query.put("R_URL", pmUrl);
			DBObject result = MongoRestAPIService.get(PM_RESULT_COLLECTION, query);
			if(result == null) {
				DBObject missingUrl =  new BasicDBObject();
				missingUrl.put("PM_URL", pmUrl);
				missingUrls.add(missingUrl);
			}
		}
		
		log.debug("etPMMissing process method ends..");
		return missingUrls;
	}

}
