package com.times.mailer.batch.processor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.jdom.Document;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.mailer.model.CCIAliveDoc;
import com.times.common.util.CCIAliveUtils;
import com.times.common.util.InMemoryData;

/**
 * 
 * @author praveen.bokka
 *
 */
public class CCIAliveCMSDataProcessor implements ItemProcessor<Document, List<DBObject>> {

	private static final Logger log = LoggerFactory.getLogger(CCIAliveCMSDataProcessor.class);
	
	@Autowired
	private String solrLocation;
	
	public String getSolrLocation() {
		return solrLocation;
	}

	public void setSolrLocation(String solrLocation) {
		this.solrLocation = solrLocation;
	}
	
	public List<DBObject> process(Document document) throws Exception {
		log.debug("CCIAliveCMSData process method start..");
		
		Date lastDate;
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		List<DBObject> objectList = new ArrayList<DBObject>();
		
		Element rootElem = document.getRootElement();
		
		List<Element> docList = rootElem.getChild("Documents").getChildren("Document");
		
		HttpSolrClient server = new HttpSolrClient(solrLocation);//"http://etdev.indiatimes.com:8983/solr/epaper");
		
		System.out.println("solrclient created");
		int lastMsid = 0;
		for(Element doc : docList) {
			String title = doc.getChildText("htitle");
			String description = doc.getChildText("text");
			if(description == null || description.trim().equals("")) {
				continue;
			}
			String id = doc.getChildText("msid");
			lastMsid = Integer.parseInt(id);
			String synopsis = doc.getChildText("synopsis");
			String date = doc.getChildText("effectivedate");
			String updateDate = doc.getChildText("updatedAt");
			String seopath = doc.getChildText("seopath");
			updateDate = updateDate.substring(0, 19) +"Z";
			lastDate = f.parse(updateDate);
			String multipublished = doc.getChildText("multiPublishMaster");
			
			
			SolrQuery query = new SolrQuery();
			query.set("df", "id");
			query.setQuery(id);
			
			QueryResponse response = server.query(query);
			SolrDocumentList responseList = response.getResults();
			long number = responseList.getNumFound();
			CCIAliveDoc articleObj = new CCIAliveDoc();
			articleObj.setType("CMS");
			articleObj.setId(id);
			articleObj.setTitle(title);
			articleObj.setStory(description);
			articleObj.setSubject(synopsis);
			articleObj.setSeopath(seopath);
			server.addBean(articleObj);
			server.commit();
			InMemoryData.CCIAliveMsid = lastMsid;
			InMemoryData.CCIAliveDate = lastDate;
		}
		server.close();
		
		log.debug("lastMsid : "+lastMsid);
		log.debug("CCIAliveCMSData mehtod ends...");
		return objectList;
		
	}
	
}
