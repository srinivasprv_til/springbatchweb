package com.times.mailer.batch.processor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.jdom.Document;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.model.ETEpaperArticle;
import com.times.common.util.InMemoryData;

/**
 * 
 * @author praveen.bokka
 *
 */
public class ETEpaperCMSDataProcessor implements ItemProcessor<Document, List<DBObject>> {

	private static final Logger log = LoggerFactory.getLogger(ETEpaperCMSDataProcessor.class);
	
	@Autowired
	private String solrLocation;
	
	public String getSolrLocation() {
		return solrLocation;
	}

	public void setSolrLocation(String solrLocation) {
		this.solrLocation = solrLocation;
	}
	
	public List<DBObject> process(Document document) throws Exception {
		log.debug("etEpaperCMSData process method start..");
		
		Date lastDate;
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		List<DBObject> objectList = new ArrayList<DBObject>();
		
		Element rootElem = document.getRootElement();
		
		List<Element> docList = rootElem.getChild("Documents").getChildren("Document");
		
		HttpSolrClient server = new HttpSolrClient(solrLocation);//"http://etdev.indiatimes.com:8983/solr/epaper");
		
		System.out.println("solrclient created");
		int lastMsid = 0;
		for(Element doc : docList) {
			String title = doc.getChildText("title");
			String description = doc.getChildText("text");
			if(description == null || description.trim().equals("")) {
				continue;
			}
			String id = doc.getChildText("msid");
			lastMsid = Integer.parseInt(id);
			String synopsis = doc.getChildText("synopsis");
			String date = doc.getChildText("effectivedate");
			String updateDate = doc.getChildText("updateDate");
			updateDate = updateDate.substring(0, 19) +"Z";
			lastDate = f.parse(updateDate);
			String multipublished = doc.getChildText("multiPublishMaster");
			
			
			SolrQuery query = new SolrQuery();
			query.set("df", "id");
			query.setQuery(id);
			
			QueryResponse response = server.query(query);
			SolrDocumentList responseList = response.getResults();
			long number = responseList.getNumFound();
			//if(number == 0) {
				ETEpaperArticle articleObj = new ETEpaperArticle();
				articleObj.setType("cms");
				articleObj.setId(id);
				articleObj.setTitle(title);
				articleObj.setDescription(description);
				articleObj.setDescription200(description.substring(0, description.length() >200 ?200 : description.length() ));
				articleObj.setSynopsis(synopsis);
				articleObj.setEdDate(lastDate);
				if(multipublished.equals("0"))
					articleObj.setMultipublished(true);
				else 
					articleObj.setMultipublished(false);
				server.addBean(articleObj);
				server.commit();
			//}
			
				List<DBObject> matchingArticles = getSimilarDescriptionDocs(server,  description);
				if(matchingArticles != null && matchingArticles.size() > 0) {
					for(DBObject matchingArticle : matchingArticles) {
						if(!objectList.contains(matchingArticle)) {
							objectList.add(matchingArticle);
						}
					}
//					objectList.addAll(matchingArticles);
				} //else {
					matchingArticles = getSimilarDescription200Docs(server,  description);
					if(matchingArticles != null && matchingArticles.size() > 0) {
						for(DBObject matchingArticle : matchingArticles) {
							if(!objectList.contains(matchingArticle)) {
								objectList.add(matchingArticle);
							}
						}
//						objectList.addAll(matchingArticles);
					} //else {
						matchingArticles = getSimilarSynopsisDocs(server,  description);
						if(matchingArticles != null && matchingArticles.size() > 0) {
							for(DBObject matchingArticle : matchingArticles) {
								if(!objectList.contains(matchingArticle)) {
									objectList.add(matchingArticle);
								}
							}
//							objectList.addAll(matchingArticles);
						}
					//}
				//}
			InMemoryData.etEpaperMsid = lastMsid;
			InMemoryData.etEpaperDate = lastDate;
		}
		//}
		server.close();
		
		markMsidRepetitions(objectList);
		
		log.debug("lastMsid : "+lastMsid);
		log.debug("etEpaperCMSData mehtod ends...");
		return objectList;
		
	}
	
	/**
	 * Marks repetition to 1 if an msid is matched to more than one epaper article for a pubid
	 * @param objectList
	 */
	private void markMsidRepetitions(List<DBObject> objectList) {
		Set<String> uniqueMsids = new HashSet<>();
		
		for(int i = 0; i < objectList.size(); i++) {
			DBObject object = objectList.get(i);
		//for(DBObject object : objectList) {
			String msid = (String)object.get("msid");
			String pubid = (String)object.get("pubid");
			
			//msid+pubid defines the key for matching
			if(uniqueMsids.contains(msid+pubid)) {
				object.put("repetition", 1);
			} else {
				object.put("repetition", 0);
			}
			uniqueMsids.add(msid+pubid);
		}
//		System.out.println(objectList);
	}
	
	private List<DBObject> getSimilarSynopsisDocs(HttpSolrClient server, String synopsis) {
		SolrQuery query = new SolrQuery();
		List<DBObject> objectList = new ArrayList<DBObject>();
		QueryResponse response = null;
		if(synopsis.length() > 500) {
			return null;
		}
		//description = description.replaceAll("[:/\"\\)\\(-\\[\\]]", "");//.replace("/", "").replace("\"", "").replace(")", "");
		//synopsis = synopsis.substring(0, synopsis.length() >200 ?200 : synopsis.length());
		synopsis = synopsis.replaceAll("[\\W]", " ");//remove all special characters
		query.setQuery(synopsis);
		query.setParam("df", "synopsis");
		query.set("ns", "{!func}product(scale(query({!type=dismax v=$q}),0,1),100)");
		query.set("fq", "{!frange l=70}$ns");
		//query.set("fl", "*,score");
		query.setIncludeScore(true);
		
		List<ETEpaperArticle> list = null;
		
		try{
			response = server.query(query);
			list = response.getBeans(ETEpaperArticle.class);
		} catch (Exception e) {
			log.error("Exception from solrclient: "+e + " " + query);
		}
		
		extractMatchingArticles(list, objectList);
		return objectList;
	}

	private List<DBObject> getSimilarDescription200Docs(HttpSolrClient server, String description) {
		SolrQuery query = new SolrQuery();
		List<DBObject> objectList = new ArrayList<DBObject>();
		QueryResponse response = null;
		//description = description.replaceAll("[:/\"\\)\\(-\\[\\]]", "");//.replace("/", "").replace("\"", "").replace(")", "");
		description = description.substring(0, description.length() >200 ?200 : description.length());
		description = description.replaceAll("[\\W]", " ");//remove all special characters
		//query.setQuery(description.length() > 4000 ? description.substring(0, 4000) : description);
		query.setQuery(description);
		query.setParam("df", "description200");
		query.set("ns", "{!func}product(scale(query({!type=dismax v=$q}),0,1),100)");
		query.set("fq", "{!frange l=70}$ns");
		//query.set("fl", "*,score");
		query.setIncludeScore(true);
		
		List<ETEpaperArticle> list = null;
		
		try{
			response = server.query(query);
			list = response.getBeans(ETEpaperArticle.class);
		} catch (Exception e) {
			log.error("Exception from solrclient: "+e + " " + query);
		}
		extractMatchingArticles(list, objectList);
		
		return objectList;
	}
	
	/**
	 * creates list of matching cms and epaper articles from similar articles  
	 * @param list - list of all similar articles
	 * @param objectList - return object: list of mongodb objects
	 */
	private void extractMatchingArticles(List<ETEpaperArticle> list, List<DBObject> objectList) {
		if(list != null && list.size() > 1) {
			List<ETEpaperArticle> cmsList = new ArrayList<ETEpaperArticle>();
			List<ETEpaperArticle> epaperList = new ArrayList<ETEpaperArticle>();
			
			//identify the article type and add to corresponding list
			for(ETEpaperArticle solrArticle: list ){
				if(solrArticle.getType().equals("cms")){
					cmsList.add(solrArticle);
				} else if(solrArticle.getType().equals("epaper")){
					epaperList.add(solrArticle);
				}
			}
			if(cmsList.size() > 0) {
				
//				for(int i = 0; i < cmsList.size(); i++) {
//					ETEpaperArticle cmsArticle = cmsList.get(i);
//				
//					DBObject map = new BasicDBObject();
//					map.put("msid", cmsArticle.getId());
//					map.put("cmsTitle", cmsArticle.getTitle());
//					List<DBObject> epaperArray = new ArrayList<>();
//					log.debug("Found msid "+cmsArticle.getId());
//					for(ETEpaperArticle epaperArticle: epaperList) {
//						DBObject epaperObject = new BasicDBObject();
//						log.debug("Corresponding Epaper ID: "+epaperArticle.getId());
//						epaperObject.put("epaperId", epaperArticle.getId());
//						epaperObject.put("title", epaperArticle.getTitle());
//						epaperObject.put("pageId", epaperArticle.getPageId());
//						epaperObject.put("editionTitle", epaperArticle.getEditionTitle());
//						epaperObject.put("pageTitle", epaperArticle.getPageTitle());
//						epaperObject.put("editionName", epaperArticle.getEditionName());
//						epaperObject.put("edDate", epaperArticle.getEdDate());
//						epaperObject.put("image", epaperArticle.getImage());
//						epaperObject.put("pubid", epaperArticle.getPubid());
//						epaperObject.put("pageDate", getDatefromPageID(epaperArticle.getPageId()));
//						epaperObject.put("page", getPagefromPageID(epaperArticle.getPageId()));
//						epaperObject.put("magazine", getMagfromPageID(epaperArticle.getPageId()));
//						epaperObject.put("epaperUrl", formArticleUrl(epaperArticle.getTitle(),epaperArticle.getId(),epaperArticle.getPubid()));
//						epaperArray.add(epaperObject);
//					}
//					if(epaperArray.size() > 0) {
//						map.put("epaperArticles", epaperArray);
//						objectList.add(map);
//					}
//				}
				
				for(ETEpaperArticle cmsArticle: cmsList) {
//					List<String> epaperPubids = new ArrayList<>();
					for(ETEpaperArticle epaperArticle: epaperList) {
						DBObject map = new BasicDBObject();
						map.put("msid", cmsArticle.getId());
						map.put("cmsTitle", cmsArticle.getTitle());
						//exception can occur for older articles which do not have multipublish info
						//try catch can be removed later
						try {
							map.put("multipublished", cmsArticle.isMultipublished());
						} catch (Exception e) {
							log.error("multipublish status not found. msid: "+cmsArticle.getId());
						}
						map.put("epaperId", epaperArticle.getId());
						map.put("epaperArticleId", epaperArticle.getName());
						map.put("title", epaperArticle.getTitle());
						map.put("pageId", epaperArticle.getPageId());
						map.put("editionTitle", epaperArticle.getEditionTitle());
						map.put("pageTitle", epaperArticle.getPageTitle());
						map.put("editionName", epaperArticle.getEditionName());
						map.put("edDate", epaperArticle.getEdDate());
						map.put("image", epaperArticle.getImage());
						map.put("pubid", epaperArticle.getPubid());
						map.put("pageDate", getDatefromPageID(epaperArticle.getPageId()));
						map.put("page", getPagefromPageID(epaperArticle.getPageId()));
						map.put("magazine", getMagfromPageID(epaperArticle.getPageId()));
						map.put("epaperUrl", formArticleUrl(epaperArticle.getTitle(),epaperArticle.getName(),epaperArticle.getPubid()));
						
//						if(epaperPubids.contains((epaperArticle.getPubid()))) {
//							map.put("repetition", 1);
//						} else {
//							map.put("repetition", 0);
//						}
//						epaperPubids.add(epaperArticle.getPubid());
						objectList.add(map);
					}
				}
			}
		}
	}

	private List<DBObject> getSimilarDescriptionDocs(HttpSolrClient server,  String description) {
		SolrQuery query = new SolrQuery();
		List<DBObject> objectList = new ArrayList<DBObject>();
		QueryResponse response = null;
		//description = description.replaceAll("[:/\"\\)\\(-\\[\\]]", "");//.replace("/", "").replace("\"", "").replace(")", "");
		description = description.replaceAll("[\\W]", " ");//remove all special characters
		query.setQuery(description.length() > 4000 ? description.substring(0, 4000) : description);
		query.setParam("df", "description");
		query.set("ns", "{!func}product(scale(query({!type=dismax v=$q}),0,1),100)");
		query.set("fq", "{!frange l=70}$ns");
		//query.set("fl", "*,score");
		query.setIncludeScore(true);
		
		List<ETEpaperArticle> list = null;
		
		try{
			response = server.query(query);
			list = response.getBeans(ETEpaperArticle.class);
		} catch (Exception e) {
			log.error("Exception from solrclient: "+e + " " + query);
		}
		
		
		extractMatchingArticles(list, objectList);
		return objectList;
	}
	
	private static String formArticleUrl(String title, String id, String pubid) {
		title = formUrlTitle(title);
		String url = "http://epaperbeta.timesofindia.com/Article.aspx?eid="+pubid+"&articlexml=" + title +id;
		return url;
	}
	
	private static String formUrlTitle(String title) {
		title = title.replace('-', ' ');
		title = title.replaceAll("[^\\w\\d\\s]", "");//only alphanumerics
		title = title.trim();
		String [] words = title.split(" ");
		title = "";
		try {
			for(int i = 0, count =0; count < 8 ; i++){
				if(!words[i].trim().equals("")) {
					title = title + words[i] + "-"; 
					count++;//number of non empty words
				}
			}
		} catch(IndexOutOfBoundsException e) {
			
		}
		return title;
	}
	
	public static int getPagefromPageID(String pageID) {
		String str = pageID.substring(pageID.length()-2);
		return Integer.parseInt(str);
	}
	
	public static String getDatefromPageID(String pageID) {
		String str = pageID.substring(0,pageID.length()-4);
		
		return str;
	}
	
	public static int getMagfromPageID(String pageID) {
		char str = pageID.charAt(pageID.length()-3);
		return Character.getNumericValue(str);
	}
	
	public static String getPageIdfromDate(String dateStr, int page, int magazine) {
		if(page != -1) {
			Date date = getDatefromString(dateStr);
			SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");
			String pageId = sdf.format(date);
			pageId = pageId +"_"+Integer.toString(magazine)+ String.format("%02d", page);
			return pageId;
		}
		return null;
	}
	
	public static Date getDatefromString(String dateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

}
