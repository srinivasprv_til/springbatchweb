/**
 * 
 */
package com.times.mailer.batch.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.StringUtils;

import com.mongodb.DBObject;

import com.times.common.mail.EmailVO;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.common.util.MailerUtil;
import com.times.mailer.dao.TravelNewsletterDao;
import com.times.mailer.model.NewsLetter;
import com.times.mailer.model.TravelAlertUser;
import com.times.mailer.model.TravelPreference;

/**
 * This class is used as processor of Travel Alert Batch mailer in which building body content based on user selected.
 * spring batch forward this to MailerWriter class.
 * 
 * @author ranjeet.jha
 *
 */
public class TravelAlertProcessor implements ItemProcessor<TravelAlertUser, List<EmailVO>> {

	private static final Logger log = LoggerFactory.getLogger(TravelAlertProcessor.class);

	static String sDelimiter = "#$#";
	private TravelNewsletterDao travelNewsletterDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public List<EmailVO> process(TravelAlertUser user) throws Exception {
		boolean isContentPieceFound = false;

		List<EmailVO> emailVOList = new ArrayList<EmailVO>();

		NewsLetter news = null;

		// get newsletter object of trave; alert (1032)
		for (NewsLetter newsObj : user.getNewsletters()) {
			if (newsObj != null && Constant.TRAVEL_ALERT_NLID.equalsIgnoreCase(newsObj.getId())) {
				news = newsObj;
				break;
			}
		}

		Map<String, Set<String>> listTypePreferenceMap = new HashMap<String, Set<String>>();

		// get sent user and msid info to mongoDB
		DBObject dbObject = travelNewsletterDao.getAlertDBObjectEmailSent(user.getEmailId());

		// loop to make EmailVo object for every preference of user.
		if (user.getPreferemceMsidsMap() != null && user.getPreferemceMsidsMap().keySet() != null) {
			Iterator preferenceMsid = user.getPreferemceMsidsMap().keySet().iterator();
			
			while(preferenceMsid.hasNext()) {
				EmailVO vo = new EmailVO();

				String preference = (String) preferenceMsid.next();

				// get msid for particuler preference for which mail has not been sent yet.
				Set<Integer> notSentmsid = getNewMsidNotFoundInDB(user.getPreferemceMsidsMap().get(preference), dbObject);

				boolean isPreferenceVerified = false;
				TravelPreference tpVerifiedObject = null;

				// check whether users had confirmed preference?
				if (user.getTravelSubscrptionPreference() != null) {
					for (TravelPreference tp : user.getTravelSubscrptionPreference()) {
						if (tp.getPreferenceValue().equals(preference)) {
							isPreferenceVerified = true;
							tpVerifiedObject = tp;
							break;
						}
					}
				}


				
				if (notSentmsid.size() > 0 && isPreferenceVerified && tpVerifiedObject != null) {					

					vo.setRecipients(user.getEmailId());
					vo.setSender(user.getEmailSubject());

					// set email body.
					getEmailBody(user, isContentPieceFound, vo, news, notSentmsid, preference, tpVerifiedObject);

					// set msids which will be added or appended in travelAlertLog table.
					if (user.getMsid() != null) {
						user.getMsid().addAll(notSentmsid);
					} else {
						user.setMsid(notSentmsid);
					}


					// Set preference map to be stored in travelAlertLog table.
					String listType = tpVerifiedObject.getListType();
					if (listType == null || StringUtils.hasText(listType)) {
						// Default list type will be alertList.
						listType = Constant.listTypes[0];
					}

					if (listTypePreferenceMap.get(listType) == null ) {
						Set<String> temp = new HashSet<String>();
						temp.add(preference);
						listTypePreferenceMap.put(listType,temp);
					} else {
						listTypePreferenceMap.get(listType).add(preference);
					}

				} else {

					vo.setBody(null);
				}

				emailVOList.add(vo);
			}
		}

		try {

			if (user.getMsid() != null && user.getMsid().size() > 0) {
				if (dbObject != null) {

					Set<Integer> msidToBeUpdate = mergeDBObject(user, dbObject);
					if (msidToBeUpdate != null && msidToBeUpdate.size() > 0) {
						List<Integer> listToBeUpdate = new ArrayList<Integer>(msidToBeUpdate);
						dbObject.put(TravelAlertUser.MSID, listToBeUpdate);
						travelNewsletterDao.updateTravelAlertCollection(dbObject , listTypePreferenceMap);
					}
				} else if (dbObject == null) {
					// Add sent user and msid info to mongoDB.listTypePreferenceMap contains listType and correspond preferences to add in travelAlertLog table.
					travelNewsletterDao.addTravelAlertUser(user , listTypePreferenceMap);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}


		return emailVOList;
	}

	/**
	 * This API helps to find msids of a particular preference for which mail has not been sent,
	 * @param newMsids
	 * @param dbObject
	 * @return
	 */
	private Set<Integer> getNewMsidNotFoundInDB(Set<Integer> newMsids, DBObject dbObject) {
		Set<Integer> notSentmsid = new HashSet<Integer>();


		if (newMsids != null && dbObject != null) {
			List<Integer> dbMsids = (List) dbObject.get(TravelAlertUser.MSID);
			if (dbMsids != null && dbMsids.size() > 0) {
				Iterator<Integer> it = newMsids.iterator();
				while(it.hasNext()) {
					Integer i = it.next();	
					if (!dbMsids.contains(i)) {
						notSentmsid.add(i.intValue());
					}
				} 
			}else  {
				notSentmsid = newMsids;
			}
		} else if (newMsids != null && dbObject == null) {
			notSentmsid = newMsids;
		}

		return notSentmsid;
	}

	private Set<Integer> mergeDBObject(TravelAlertUser taUser, DBObject addedDBObject) {
		Set<Integer> toBeUpdateSet =null;

		if (taUser != null && addedDBObject != null) {
			if (taUser != null && taUser.getMsid() != null && addedDBObject != null) {
				toBeUpdateSet = new HashSet<Integer>(taUser.getMsid());

				List<Integer> dbMsidList = (List<Integer>)addedDBObject.get(TravelAlertUser.MSID);
				if (dbMsidList != null && dbMsidList.size() > 0) {
					for (Integer ms : dbMsidList) {
						toBeUpdateSet.add(ms);
					}
				}
			}
		}

		return toBeUpdateSet;
	}



	/**
	 * This API make emailBody of mail.
	 * @param user
	 * @param isContentPieceFound
	 * @param vo
	 * @param news
	 * @param notSentmsid
	 * @param preference
	 * @return
	 */
	private EmailVO getEmailBody(TravelAlertUser user, boolean isContentPieceFound, EmailVO vo, NewsLetter news, Set<Integer> notSentmsid, String preference , TravelPreference tpVerifiedObject) {
		Integer msid;
		String contentPieces;
		if (news != null) {
			int daily_weekly = news.isDaily() ? 1: 2;
			//int morning_evening = news.getScheduleTime();
			int morning_evening = 1;
			String newsid = news.getId();
			String displayformat = news.getDisplayFormate();

			log.debug("newsLetter Id : " + newsid + " , " + user.getEmailId() + " , format : " +  news.getDisplayFormate());
			String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + morning_evening;
			String sBody = InMemoryData.newsMailerUIMap.get(skey+Constant.KEY_BUILDER_SEPERATOR+displayformat);

			// code for non personalised newsletter check
			if (sBody != null && notSentmsid != null	&& notSentmsid.size() > 0) {
				StringBuilder mailBodySB = new StringBuilder();

				//Iterator<Integer> it = user.getMsid().iterator();
				for (Iterator<Integer> it = notSentmsid.iterator(); it.hasNext();) {
					msid = it.next();
					// get html body part for particular msid.
					String bodyPart = getTemplateDataInHTML(user.getTemplatePath(),msid);
					if (bodyPart != null) {
						mailBodySB.append(bodyPart);
					}
				}

				contentPieces = mailBodySB.toString();
				if (StringUtils.hasText(contentPieces)) {
					isContentPieceFound = true;
				
					if (sBody != null) {
						sBody = sBody.replaceAll("#MSIDNAME#", preference);
						sBody = sBody.replace("#MSIDLIST#", contentPieces);
						sBody = sBody.replace("#emailid#", user.getEmailId());
						sBody = sBody.replace("#nlid#", tpVerifiedObject.getId());
						sBody = sBody.replace("#hashcode#", tpVerifiedObject.getHashcode());
						sBody = sBody.replace("#listtype#", Constant.listTypes[0]);
					}
				}

				// if  contentPiece is not then do not send mail.
				if (isContentPieceFound) {
					//sBody = sBody.replace("#emailid#" , user.getEmailId());
					vo.setBody(sBody);
				} else {
					vo.setBody(null);
				}

				vo.setSubject(user.getEmailSubject() + " " + preference);
				vo.setRecipients(user.getEmailId());

			}
		}
		return vo;
	}


	/**
	 * @param newsletterId
	 * @return
	 */
	private DBObject getNewsletterMasterByNewsletterID(String newsletterId) {
		DBObject nlDBObject = null;

		List<DBObject> dbObjects = travelNewsletterDao.getNewsletterDBObjectById("1040");

		if (dbObjects != null && dbObjects.size() > 0) {
			nlDBObject = dbObjects.get(0);
		}

		return nlDBObject;
	}


	/**
	 * @param travelNewsletterDao the travelNewsletterDao to set
	 */
	public void setTravelNewsletterDao(TravelNewsletterDao travelNewsletterDao) {
		this.travelNewsletterDao = travelNewsletterDao;
	}


	/**
	 * This api help to get html body of given msid. If body is not already stored in contentMap, http call is executed and result stored in map for future use.
	 * @param url
	 * @param msid
	 * @return
	 */
	private String getTemplateDataInHTML(String url, Integer msid) {

		if(StringUtils.hasText(InMemoryData.contentMap.get(msid))) {
			return InMemoryData.contentMap.get(msid);
		} else {

			url = url.replace("#msid", msid.toString());
			String content = MailerUtil.getData(url);
			if (StringUtils.hasText(content)) {
				InMemoryData.contentMap.put(msid, content);
				return content;			
			} else {
				return null;
			}
		}

	}

}
