/**
 * 
 */
package com.times.mailer.batch.processor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.times.common.service.FileContentServices;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.dao.CMSDataDao;
import com.times.mailer.model.FileContentInfo;
import com.times.mailer.model.FileLocationInfo;

/**
 * This is common file uploader spring batch processor.
 * 
 * @author Ranjeet.Jha
 *
 */
public class CVoterContentProcessor  implements ItemProcessor<FileLocationInfo, FileContentInfo> {

	private static final Logger log = LoggerFactory.getLogger(CVoterContentProcessor.class);
	private FileContentServices fileContentServices;
	private CMSDataDao cmsDataDao;
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public FileContentInfo process(FileLocationInfo fileLocationInfo) throws Exception {
		String ftpLocation = null;
		FileContentInfo fileContentInfo = new FileContentInfo();
		try {
			FileLocationInfo fileUploadInfo = fileLocationInfo;
			if (fileUploadInfo != null) {
				//for (FileLocationInfo fileUploadInfo : items) {
					//FileLocationInfo fileUploadInfo = items.get(0);
					ftpLocation = fileUploadInfo.getNetstorageLocation().getServerName() + fileUploadInfo.getNetstorageLocation().getFolderLocation() + "/" + fileUploadInfo.getNetstorageLocation().getFileName();
					String sourceUrl = fileUploadInfo.getSourceUrl().trim();
					//String fileContent = MailerUtil.getData(sourceUrl);
					String fileContent = fileContentServices.getContentByHTTP(sourceUrl);
					//InputStream inputStream = NetstorageUtil.getInputStreamByUrl(sourceUrl);
					if (fileContent != null) {
						if(!fileContent.contains("<?xml version=")){
							fileContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + fileContent;
						}
						
						SAXBuilder builder = new SAXBuilder();
						InputStream in = new ByteArrayInputStream(fileContent.getBytes("UTF-8"));
						Document doc = builder.build(in);
						
						Element rootElement= doc.getRootElement();
						XPath xPath = XPath.newInstance("//./NewDataSet/Table");
						List<Element> resultNode = xPath.selectNodes(rootElement);
						if (resultNode != null) {
							for (Element node : resultNode) {
								Element partyName = new Element("partyname").addContent(node.getChildText("party"));
								Element partyAlias = new Element("alias").addContent(node.getChildText("party"));
								Element colorcode = new Element("colorcode").addContent("840000");
								node.addContent(partyName);
								node.addContent(partyAlias);
								node.addContent(colorcode);
								node.removeChild("party_name");
							}
						}
						XMLOutputter outputter = new XMLOutputter();
						fileContent = outputter.outputString(doc);
						fileContentInfo.setContent(fileContent);
						fileContentInfo.setFileName(ftpLocation);
						fileContentInfo.setSourceUrl(sourceUrl);
						fileContentInfo.setWorktype(9);
						fileContentInfo.setStatus(fileLocationInfo.isStatus());
						
						fileContentInfo.setNetstorageLocation(fileUploadInfo.getNetstorageLocation());
					}
					
				//}
			}
		} catch (Exception e) {
			new CMSCallExceptions("Exception caught while ftp update. ftp url: " + ftpLocation + " msg: " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			log.error("exception caught while ftp update. ftp url: " + ftpLocation);
		}
		return fileContentInfo;
	}
	
	/**
	 * @param fileContentServices the fileContentServices to set
	 */
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}
	
	public CMSDataDao getCmsDataDao() {
		return cmsDataDao;
	}


	public void setCmsDataDao(CMSDataDao cmsDataDao) {
		this.cmsDataDao = cmsDataDao;
	}
	
	
	
}