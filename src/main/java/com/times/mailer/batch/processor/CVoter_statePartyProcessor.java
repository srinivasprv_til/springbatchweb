package com.times.mailer.batch.processor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.google.gson.Gson;
import com.times.common.service.FileContentServices;
import com.times.mailer.model.FileContentInfo;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.PartyGroupDetails;
import com.times.mailer.model.PollingConstants;
import com.times.mailer.model.StateResults;

public class CVoter_statePartyProcessor implements
		ItemProcessor<FileLocationInfo, FileContentInfo> {

	private static final Logger log = LoggerFactory
			.getLogger(CVoterContentProcessor.class);
	private FileContentServices fileContentServices;

	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}

	@Override
	public FileContentInfo process(FileLocationInfo fileLocationInfo)
			throws Exception {

		if (fileLocationInfo != null) {
			String sourceUrl = fileLocationInfo.getSourceUrl().trim();
			
			String strData = fileContentServices.getContentByHTTP(sourceUrl, sourceUrl.replace("counting.teamcvoter.com", "countings.teamcvoter.com"));
			if (strData!=null){
			String str =getJsonData(strData, fileLocationInfo.getStateId(), fileLocationInfo.getCallbackName());
			if (str!=null){
				System.out.println(fileLocationInfo.getSourceUrl());
				fileContentServices.uploadContentByFTP(fileLocationInfo.getNetstorageLocation(),str);
				
				// upload content to Amazon S3
				//fileContentServices.uploadFileAmazonS3(fileLocationInfo.getAmazonS3(), str);
				
			}
			}
			// for all india standing based on party
			// sourceUrl="http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_party_all&stateid=all&type=data";
		
			 
			//for all india group standing
			 
			//sourceUrl="http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_overall&stateid=all&type=data";
			//getJSONData(fileContentServices.getContentByHTTP(sourceUrl), "all");

			 
			// for party standing with loss_gain at state level
			//sourceUrl = "http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_party_ally_tally_gain_loss&type=data&stateid=101";

			// for vote swing at state level
			// sourceUrl =
			// "http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_party_ally_vote_share&type=data&stateid=101";
			/*
			for (int i = 101; i < 136; i++) {
				//sourceUrl = "http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_party_ally_vote_share&type=data&stateid="+ i;
				sourceUrl = "http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_party_ally_tally_gain_loss&type=data&stateid="+i;
				String fileContent = fileContentServices
						.getContentByHTTP(sourceUrl);
				if (fileContent != null) {
					getJSONData(fileContent, String.valueOf(i));

				}
			}
*/
			return null;
		}

		return null;
	}

	/**
	 * @param fileContent
	 * @throws UnsupportedEncodingException
	 * @throws JDOMException
	 * @throws IOException
	 */
	private String getJsonData(String fileContent, String stateInfo, String callBack)
			throws UnsupportedEncodingException, JDOMException, IOException {
		
		if (!fileContent.contains("<?xml version=")) {
			fileContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
					+ fileContent;
		}
		SAXBuilder builder = new SAXBuilder();
		InputStream in = new ByteArrayInputStream(fileContent.getBytes("UTF-8"));
		Document doc = builder.build(in);
		XPath xPath = XPath.newInstance("//./NewDataSet/Table");
		ArrayList<PartyGroupDetails> groupsummary = new ArrayList<PartyGroupDetails>();
		Element rootElement = doc.getRootElement();
		List<Element> resultNode = xPath.selectNodes(rootElement);
		if (resultNode != null) {
			for (Element node : resultNode) {
				PartyGroupDetails parties = new PartyGroupDetails();
				if (node.getChild("PARTY") != null) {
					parties.setParty(node.getChildText("PARTY"));
					parties.setParty_fullname(node.getChildText(node
							.getChildText("PARTY")));
					parties.setColorcode("color");
				}
				else if (node.getChild("party") != null) {
					parties.setParty(node.getChildText("party"));
					parties.setParty_fullname(node.getChildText(node
							.getChildText("party")));
				parties.setColorcode("color");
				} else if (node.getChild("party_name") != null) {
					parties.setParty(node.getChildText("party_name"));
					parties.setParty_fullname(node.getChildText(node
							.getChildText("party_name")));
					parties.setColorcode("color");
				}
				
				if (node.getChild("leading") != null) {
					parties.setLeading(node.getChildText("leading"));
				}else if(node.getChild("LEADING") != null) {
					parties.setLeading(node.getChildText("LEADING"));
				}
				if (node.getChild("won") != null) {
					parties.setWon(node.getChildText("won")+"");
				}else if (node.getChild("WON") != null) {
					parties.setWon(node.getChildText("WON")+"");
				} 

				if (node.getChild("last_won") != null) {
					parties.setLastwon(node.getChildText("last_won"));

				}
				if (node.getChild("total") != null) {
					parties.setTotal(node.getChildText("total")+"");
				}else if (node.getChild("TOTAL") != null) {
					parties.setTotal(node.getChildText("TOTAL")+"");
				}
					
				if (node.getChild("group_id") != null) {
					parties.setGroupname(String.valueOf(PollingConstants
							.cvoter_groupIdtoGroupName((node
									.getChildText("group_id")))));
				}else if(node.getChild("GROUP_ID") != null) {
					parties.setGroupname(String.valueOf(PollingConstants
							.cvoter_groupIdtoGroupName((node
									.getChildText("GROUP_ID")))));
				} 

				if (node.getChild("gain_loss") != null) {
					parties.setGainloss(node.getChildText("gain_loss")+"");
				}

				if (node.getChild("votes") != null) {
					parties.setVotes(node.getChildText("votes"));
				}

				if (node.getChild("total_votes") != null) {
					parties.setTotalvotes(String.valueOf(node
							.getChildText("total_votes")));
				}

				if (node.getChild("vote_share") != null)
					parties.setVoteshare(String.valueOf(node
							.getChildText("vote_share")));
				if (node.getChild("lvotes") != null)
					parties.setLastvotes(String.valueOf(node
							.getChildText("lvotes")));
				if (node.getChild("ltotal_votes") != null)
					parties.setLasttotalvotes(String.valueOf(node
							.getChildText("ltotal_votes")));
				if (node.getChild("lvote_share") != null)
					parties.setLastvoteshare(String.valueOf(node
							.getChildText("lvote_share")));
				if (node.getChild("swing") != null)
					parties.setVoteswing(String.valueOf(node
							.getChildText("swing")));

				groupsummary.add(parties);
			}

			StateResults state = new StateResults();

			stateInfo = (stateInfo == null) ? "all" : stateInfo;
			
			if (!stateInfo.equals("all")){
			int pollid =PollingConstants
					.getStateMapping(Integer.parseInt(stateInfo));
			//System.out.println(stateInfo + " >>> "+ pollid);
			state.setState( PollingConstants.getStateNameforEpolling(pollid));
			}else
				state.setState("all");
					
			state.setParties(groupsummary);
			Gson gson = new Gson();
			String str = gson.toJson(state);
		//	System.out.println(str);
			if (str!=null)
				return  callBack +"("+ str +")";
			else
				return null;
			//PollingConstants.saveFile("statewise("+str+")", "e:/epoll/json/stateswise/state_"+ pollid+".htm");
			//	PollingConstants.saveFile("statewise("+str+")", "e:/epoll/json/liveupdate/statewise_"+ pollid+".htm");
			//	PollingConstants.saveFile("party("+ str + ")", "e:/epoll/json/party.htm");
			//	PollingConstants.saveFile("swing("+ str + ")", "e:/epoll/json/swing/statewise_swing"+ pollid+".htm");
			//	PollingConstants.saveFile("group("+ str + ")", "e:/epoll/json/group.htm");
		}else
			return  null;
	}
}