/**
 * 
 */
package com.times.mailer.batch.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.times.mailer.model.FileLocationInfo;

/**
 * This is common file uploader spring batch processor.
 * 
 * @author Ranjeet.Jha
 *
 */
public class FileContentProcessor  implements ItemProcessor<FileLocationInfo, FileLocationInfo> {

	private static final Logger log = LoggerFactory.getLogger(FileContentProcessor.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public FileLocationInfo process(FileLocationInfo fileLocationInfo) throws Exception {
		//log.debug("in process of processor...");
		return fileLocationInfo;
	}
	
}