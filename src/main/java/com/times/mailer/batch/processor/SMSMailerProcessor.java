/**
 * 
 */
package com.times.mailer.batch.processor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.StringUtils;

import com.times.common.mail.EmailVO;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.common.util.MailerUtil;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.SMSUser;


public class SMSMailerProcessor implements ItemProcessor<SMSUser, List<EmailVO>> {

	private static final Logger log = LoggerFactory.getLogger(SMSMailerProcessor.class);
	private String smsMessage;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public List<EmailVO> process(SMSUser smsUser) throws Exception {
		List<EmailVO> emailVOList = new ArrayList<EmailVO>();
		Iterator itr = smsUser.getMailUserSubscribers().iterator();
		while (itr.hasNext()) {

			EmailVO vo = new EmailVO();
			MailUser mu =  (MailUser) itr.next();


			try {
				vo.setRecipients(mu.getEmailId());
				if (smsUser.getSms() != null && smsUser.getSms().equalsIgnoreCase("1")) {
					vo.setMobile(mu.getMobile());
					vo.setSmsmessage(smsMessage.replace("#program", smsUser.getProgramName()).replace("#channel", smsUser.getChannelName()).replace("#time", smsUser.getDateTime()));
				}
				
				// set email body.
				if (smsUser.getEmail() != null && smsUser.getEmail().equalsIgnoreCase("1")) {
					getEmailBody(smsUser, mu,vo);
				}
				vo.setSubject(smsMessage.replace("#program", smsUser.getProgramName()).replace("#channel", smsUser.getChannelName()).replace("#time",  smsUser.getDateTime()));
				emailVOList.add(vo);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}


		return emailVOList;
	}







	/**
	 * This API make emailBody of mail.
	 * @param user
	 * @param isContentPieceFound
	 * @param vo
	 * @param news
	 * @param notSentmsid
	 * @param preference
	 * @return
	 */
	private EmailVO getEmailBody(SMSUser smsuser, MailUser user, EmailVO vo) {
		
		if (user.getNewsletter() != null) {
			int daily_weekly = user.getNewsletter().isDaily() ? 1: 2;
			
			int morning_evening = user.getNewsletter().getScheduleTime();
			String newsid = user.getNewsletter().getId();
			String displayformat = user.getNewsletter().getDisplayFormate();

			log.debug("newsLetter Id : " + newsid + " , " + user.getEmailId() + " , format : " +  user.getNewsletter().getDisplayFormate());
			String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + morning_evening;
			String sBody = InMemoryData.newsMailerUIMap.get(skey+Constant.KEY_BUILDER_SEPERATOR+displayformat);


			if (sBody != null) {
				sBody = sBody.replace("#user", user.getFirstName());
				sBody = sBody.replace("#programme", smsuser.getProgramName());
				sBody = sBody.replace("#channel", smsuser.getChannelName());
				sBody = sBody.replace("#time", String.valueOf(smsuser.getDateTime()));		
				sBody = sBody.replace("#before", String.valueOf(smsuser.getTimeBefore()));		
				vo.setBody(sBody);
			} else {
				vo.setBody(null);
			}

			vo.setSubject(user.getEmailSubject());
			vo.setRecipients(user.getEmailId());

		}

		return vo;
	}


	/**
	 * This api help to get html body of given msid. If body is not already stored in contentMap, http call is executed and result stored in map for future use.
	 * @param url
	 * @param msid
	 * @return
	 */
	private String getTemplateDataInHTML(String url, Integer msid) {

		if(StringUtils.hasText(InMemoryData.contentMap.get(msid))) {
			return InMemoryData.contentMap.get(msid);
		} else {

			url = url.replace("#msid", msid.toString());
			String content = MailerUtil.postData(url);
			if (StringUtils.hasText(content)) {
				InMemoryData.contentMap.put(msid, content);
				return content;			
			} else {
				return null;
			}
		}

	}







	public String getSmsMessage() {
		return smsMessage;
	}







	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}



}
