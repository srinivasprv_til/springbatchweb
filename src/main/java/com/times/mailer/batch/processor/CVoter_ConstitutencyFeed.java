package com.times.mailer.batch.processor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;
import org.springframework.batch.item.ItemProcessor;

import com.google.gson.Gson;
import com.times.common.service.FileContentServices;
import com.times.mailer.dao.CMSDataDao;
import com.times.mailer.dao.EpollDao;
import com.times.mailer.model.CandidateConstituency;
import com.times.mailer.model.FileContentInfo;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.PartyGroupDetails;
import com.times.mailer.model.PollingConstants;
import com.times.mailer.model.StateResults;

public class CVoter_ConstitutencyFeed implements ItemProcessor<FileLocationInfo, FileContentInfo>{

	private FileContentServices fileContentServices;
	private EpollDao epollDAO;
	
	


	/**
	 * @return the epollDAO
	 */
	public EpollDao getEpollDAO() {
		return epollDAO;
	}

	/**
	 * @param epollDAO the epollDAO to set
	 */
	public void setEpollDAO(EpollDao epollDAO) {
		this.epollDAO = epollDAO;
	}

	/**
	 * @param fileContentServices the fileContentServices to set
	 */
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}

	@Override
	public FileContentInfo process(FileLocationInfo fileLocationInfo) throws Exception {
		// TODO Auto-generated method stub
		if (fileLocationInfo !=null){
			String sourceUrl = fileLocationInfo.getSourceUrl().trim();
			String strData = fileContentServices.getContentByHTTP(sourceUrl, sourceUrl.replace("counting.teamcvoter.com", "countings.teamcvoter.com"));
			if (strData!=null){
				String str =getJsonData(strData, fileLocationInfo.getStateId(), fileLocationInfo.getCallbackName());
			if (str!=null){
				System.out.println(fileLocationInfo.getSourceUrl());
				fileContentServices.uploadContentByFTP(fileLocationInfo.getNetstorageLocation(),str);
				
				// upload content to Amazon S3
				//fileContentServices.uploadFileAmazonS3(fileLocationInfo.getAmazonS3(), str);
				
			}
			}
			/*
			fileContentInfo.setContent(fileContent);
			fileContentInfo.setFileName(ftpLocation);
			fileContentInfo.setSourceUrl(sourceUrl);
			fileContentInfo.setWorktype(9);
			fileContentInfo.setStatus(fileLocationInfo.isStatus());
			fileContentInfo.setNetstorageLocation(fileUploadInfo.getNetstorageLocation());
			
			*/
			
			//sourceUrl = "http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_all_vip_flashes&stateid=all";
			//getJsonData(fileContentServices.getContentByHTTP(sourceUrl),  "all");
			//sourceUrl ="http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_constituency_updates&type=data&stateid=";
			/*
			for (int i = 101; i < 136; i++) {
				sourceUrl ="http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_constituency_updates&type=data&stateid="+i;
				
				System.out.println(sourceUrl);
				String fileContent = fileContentServices.getContentByHTTP(sourceUrl);
				if (fileContent!=null){
					getJsonData(fileContent,  String.valueOf(i));
					
					
				}
			}*/
			
		
		}
		
		return null;
	}

	/**
	 * @param fileContent
	 * @throws UnsupportedEncodingException
	 * @throws JDOMException
	 * @throws IOException
	 */
	private String  getJsonData(String fileContent, String stateInfo, String callBack)
			throws UnsupportedEncodingException, JDOMException, IOException {
		if(!fileContent.contains("<?xml version=")){
			fileContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + fileContent;
		}
		SAXBuilder builder = new SAXBuilder();
		InputStream in = new ByteArrayInputStream(fileContent.getBytes("UTF-8"));
		Document doc = builder.build(in);
		XPath xPath = XPath.newInstance("//./NewDataSet/Table");
		ArrayList<CandidateConstituency> constituencies = new ArrayList<CandidateConstituency>();
		Element rootElement= doc.getRootElement();
		List<Element> resultNode = xPath.selectNodes(rootElement);
	//	System.out.println(stateInfo +" " + fileContent);
		if (resultNode != null) {
			for (Element node : resultNode) {
				CandidateConstituency constituencyupdates = new CandidateConstituency();
				if (node.getChild("candidate_name")!=null) constituencyupdates.setCandidatename( String.valueOf(node.getChildText("candidate_name")));
				//else if (node.getChild("candidate_id")!=null) constituencyupdates.setCandidatename(String.valueOf(node.getChildText("candidate_id"))); //TODO: mapping from static collection
				if (node.getChild("vip_name")!=null) constituencyupdates.setVipname( String.valueOf(node.getChildText("vip_name")));
				if (node.getChild("image_path")!=null) constituencyupdates.setImageInfo( String.valueOf(node.getChildText("image_path")));
				if (node.getChild("lsname")!=null) constituencyupdates.setConstituency(String.valueOf(node.getChildText("lsname")));
				else if(node.getChild("pcname")!=null)constituencyupdates.setConstituency(String.valueOf(node.getChildText("pcname")));
				if (node.getChild("party_name")!=null) constituencyupdates.setPartyName( String.valueOf(node.getChildText("party_name")));
				if (node.getChild("group_id")!=null) constituencyupdates.setGroupname(PollingConstants.cvoter_groupIdtoGroupName(String.valueOf(node.getChildText("group_id"))));
				if (node.getChild("status")!=null) constituencyupdates.setStatus( String.valueOf(node.getChildText("status")));
				if (node.getChild("Votes")!=null) constituencyupdates.setVotes( String.valueOf(node.getChildText("Votes")));
				if (node.getChild("state_name")!=null) constituencyupdates.setState( String.valueOf(node.getChildText("state_name")));
				if (node.getChild("pcno")!=null) constituencyupdates.setConstituencyid(Integer.parseInt(String.valueOf(node.getChildText("pcno"))));
				constituencies.add(constituencyupdates);
				if ("update_const".equals(callBack)){
					//update db....
					if (node.getChild("candidate_id")!=null){
						try {
							Integer epollid = PollingConstants.getCVoterEpollId(Integer.parseInt(node.getChildText("candidate_id")));
							if (epollid!=-1){
								int iStatus = "lead".equals(String.valueOf(node.getChildText("status")).toLowerCase().trim())?1:2;
								getEpollDAO().saveEpollData(epollid, iStatus);
								//System.out.println("exec sp_updateConsitutencyByEpollid "+ epollid +","+ iStatus);		
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
							
					}
					
				}
			}

		StateResults result = new StateResults();
		stateInfo = (stateInfo == null) ? "all" : stateInfo;
		
		int pollid=0;
		if (!"all".equals(stateInfo)){
			pollid =PollingConstants
				.getStateMapping(Integer.parseInt(stateInfo));
			result.setState(PollingConstants.getStateNameforEpolling(pollid));
				
		}else
			result.setState(stateInfo);
		
		
		
		result.setConstituencies(constituencies);
		
		Gson gson = new Gson();
		String str = gson.toJson(result);
		return  callBack +"("+ str +")";
		//PollingConstants.saveFile("update_const("+ str+")", "e:/epoll/json/const_updates/const_updates"+ pollid+".htm");
		//PollingConstants.saveFile(str, "e:/epoll/json/vip/vip_"+  pollid+".html");
		//System.out.println(str);
		}else
			return null;
	}

}
