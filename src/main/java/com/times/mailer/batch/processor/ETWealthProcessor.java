package com.times.mailer.batch.processor;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.web.util.UriUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.InMemoryData;

public class ETWealthProcessor implements ItemProcessor<Map<String, Date>, List<DBObject>> {

	private static final Logger log = LoggerFactory.getLogger(ETWealthProcessor.class);
	
	private static Date lastCallTime = null;
	
	private static String ET_WEALTH_ERROR_COLLECTION = "ETWealthError";

	public List<DBObject> process(Map<String, Date> item) throws Exception {
		log.debug("etwealth process method start..");
		
		String ibeatURL = "http://ibeatserv.indiatimes.com/iBeat/artclicks.html?interval=8&articleIdPubs=#MSID_LIST&host=economictimes.indiatimes.com";
		
		//create coma separated list of msid's and pub dates
		String msid_list = "";
		for(Map.Entry<String, Date> entry : item.entrySet()){
			msid_list += entry.getKey();
			msid_list += "=";
			Date date = entry.getValue();
			msid_list += date.getTime();
			msid_list += ",";
		}
		
		//remove last coma
		if(!msid_list.equals("") && msid_list.charAt(msid_list.length() - 1) == ','){
			msid_list = msid_list.substring(0, msid_list.length() - 1);
		}
		
		//requirement changed
		//wait for 15 minutes before next call to ibeat
//		while(lastCallTime != null && ((new Date()).getTime() - lastCallTime.getTime()) < 900000 ){
//			Thread.sleep(60000);
//		}
		
		List<DBObject> dbList = null;
		if(!msid_list.equals("")){
			ibeatURL = ibeatURL.replace("#MSID_LIST", msid_list);
			
			//get the data from ibeat - not a good strategy to use MRA
			//MRA's implementation can be changed in future - that should not effect our implementation
			//String response = MongoRestAPIService.getExtData(ibeatURL);
			
			HttpResponse hResponse = hitAndGetResponse(ibeatURL, null, null);
			
			String response = null;
			int status = hResponse.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				response = new BasicResponseHandler().handleResponse(hResponse);
			} else if(status == HttpStatus.SC_INTERNAL_SERVER_ERROR){
				//500 status means count is not present for any msids
				response = "500 Internal Error";
				log.error("500 Error for URL:"+ibeatURL);
			} else {
				response = null;
			}
			HttpClientUtils.closeQuietly(hResponse);
			InMemoryData.actual_status_ibeat = status;

			//update get time
			lastCallTime = new Date();
			
			JSONObject dataJson = null;
			try{
				if(!response.equals("500 Internal Error")){
					//format of json is {HOST:et,DATA:{msid:{count:1,cat:wealth}}}
					JSONObject responseJson = new JSONObject(response);
					dataJson = (JSONObject) responseJson.get("DATA");
				} else {
					//assuming internel error means count zero from API
					dataJson = new JSONObject("{}");
				}
			} catch (Exception e) {
				log.error("Invalid response from url: "+ibeatURL);
				return null;
			}
			
			dbList = new ArrayList<DBObject>();
			
			for(Map.Entry<String, Date> entry : item.entrySet()){
				String msid = entry.getKey();
				String count = "0";
				String category = "";
				try{
					JSONObject msid_details = (JSONObject) dataJson.get(msid);
					count = (String) msid_details.get("COUNT");
					category = (String) msid_details.get("CAT");
				} catch (Exception e) {
					BasicDBObject document = new BasicDBObject();
					document.put("M_ID", msid);
					document.put("P_DATE", item.get(msid));
					//store missing msid and publish date in error collection
					MongoRestAPIService.insert(ET_WEALTH_ERROR_COLLECTION, document);
					log.error("MS_ID does not have count: "+msid);
				}
				//if we don't get count from ibeat, just put count 0
				DBObject msid_object = new BasicDBObject();
				msid_object.put("M_ID", msid);
				msid_object.put("COUNT", count);
				msid_object.put("CAT", category);
				dbList.add(msid_object);
			}
		}
		
		return dbList;
	}
	
	private static HttpResponse hitAndGetResponse(String strURL, String param, String paramValue) {
		
		// Get http client
		HttpClient httpClient = HttpClients.custom().build();
		
		// Create request config parameters
		RequestConfig config = RequestConfig.custom().setConnectTimeout(30000).setSocketTimeout(30000).build();
		
		// Post object
		HttpGet httpGet = null;
		
		// Response object
		HttpResponse httpResponse = null;
		
		
		
		try {
			// Encode Url
			strURL = UriUtils.encodeQuery(strURL, "UTF-8");
			
			// Create post request
			httpGet = new HttpGet(strURL);
			URI uri = null;
			if(param == null){
				uri = new URIBuilder(httpGet.getURI()).build();
			}else{
				uri = new URIBuilder(httpGet.getURI()).addParameter(param, paramValue).build();
			}
			
			httpGet.setURI(uri);
			// Set configuration
			httpGet.setConfig(config);
			System.out.println("Url getting response:  "+ httpGet.getURI());
			// Get response
			httpResponse = httpClient.execute(httpGet);
			Calendar cal = Calendar.getInstance();
	        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	        System.out.println( sdf.format(cal.getTime()) );
			System.out.println("Got Response at "+ sdf.format(cal.getTime()) + " :"+httpResponse);
		} catch (Exception e) {
			//System.out.println("Exception while:  Response:  "+httpResponse!=null ?httpResponse.toString():null+ "     "+ e);
			System.out.println("Exception occurred while:  getting url:"+ strURL);
			if(httpResponse != null){
				System.out.println("Response:"+httpResponse.toString());
			} else {
				System.out.println("HTTP Request Timedout after 30 sec");
			}
			return null;
		} finally {
			//HttpClientUtils.closeQuietly(httpClient);
			//HttpClientUtils.closeQuietly(httpResponse);
		}
		return httpResponse;
	}

}
