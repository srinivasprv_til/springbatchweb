/**
 * 
 */
package com.times.mailer.batch.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.times.mailer.model.FileLocationInfo;

/**
 * @author Ranjeet.Jha
 *
 */
public class TOIListLevelFileProcessor implements ItemProcessor<FileLocationInfo, FileLocationInfo> {

	private static final Logger log = LoggerFactory.getLogger(TOIListLevelFileProcessor.class);

	//private TravelNewsletterDao travelNewsletterDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public FileLocationInfo process(FileLocationInfo fileLocationInfo) throws Exception {
		log.debug("in process of processor...");
		return fileLocationInfo;
	}

	
	
}