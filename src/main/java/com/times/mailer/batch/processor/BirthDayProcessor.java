/**
 * 
 */
package com.times.mailer.batch.processor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.StringUtils;

import com.times.common.mail.EmailVO;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NewsLetter;


public class BirthDayProcessor implements ItemProcessor<MailUser, List<EmailVO>> {

	private static final Logger log = LoggerFactory.getLogger(BirthDayProcessor.class);

	static String sDelimiter = "#$#";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public List<EmailVO> process(MailUser user) throws Exception {
		boolean isContentPieceFound = false;

		List<EmailVO> emailVOList = new ArrayList<EmailVO>();

		NewsLetter news = user.getNewsletter();


		EmailVO vo = new EmailVO();

		try {

			if (user.getEmailId() != null) {
				// set email body.
				getEmailBody(user, vo, news);
				emailVOList.add(vo);

			} 
		} catch (Exception e) {
			log.error(e.getMessage());
		}


		return emailVOList;
	}







	/**
	 * This API make emailBody of mail.
	 *
	 */
	private EmailVO getEmailBody(MailUser user, EmailVO vo, NewsLetter news) {

		if (news != null) {
			int daily_weekly = news.isDaily() ? 1: 2;
			//int morning_evening = news.getScheduleTime();
			int morning_evening = 1;
			String newsid = news.getId();
			String displayformat = news.getDisplayFormate();

			log.debug("newsLetter Id : " + newsid + " , " + user.getEmailId() + " , format : " +  news.getDisplayFormate());
			String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + morning_evening;
			String sBody = InMemoryData.newsMailerUIMap.get(skey+Constant.KEY_BUILDER_SEPERATOR+displayformat);

			// code for non personalised newsletter check
			if (sBody != null) {					
				sBody = sBody.replace("[#emailid]", user.getEmailId());
				sBody = sBody.replace("[#nlid]", user.getNewsletter().getId());				
			}

			if (sBody != null) {			
				vo.setBody(sBody);
			} else {
				vo.setBody(null);
			}

			vo.setSubject(user.getEmailSubject());
			vo.setRecipients(user.getEmailId());

		}

		return vo;
	}
}
