package com.times.mailer.batch.processor;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemProcessor;

import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.Constant;
import com.times.common.util.EpaperOptimizer;
import com.times.mailer.model.EpaperFileDetails;



public class EpaperZipProcesser implements ItemProcessor<EpaperFileDetails, EpaperFileDetails> {

	private static final Log log = LogFactory.getLog(EpaperZipProcesser.class);

	EpaperOptimizer epaperOptimizer;

	@Override
	public EpaperFileDetails process(EpaperFileDetails item) throws Exception {
		File optimizeZip = epaperOptimizer.optimize(item);
		String fileSaveID = MongoRestAPIService.addFile(Constant.EPAPER_FILE_COLLECTION, optimizeZip);
		item.setGsfid(fileSaveID);

		return item;
	}

	public EpaperOptimizer getEpaperOptimizer() {
		return epaperOptimizer;
	}

	public void setEpaperOptimizer(EpaperOptimizer epaperOptimizer) {
		this.epaperOptimizer = epaperOptimizer;
	}

	

}