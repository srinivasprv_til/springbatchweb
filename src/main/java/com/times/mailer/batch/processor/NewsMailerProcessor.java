package com.times.mailer.batch.processor;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.mail.internet.MimeUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.StringUtils;

import com.times.common.mail.EmailVO;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.mailer.dao.MailerDao;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NewsLetter;

/**
 * 
 * 
 * @author Rajeev Khatri
 * 
 */
public class NewsMailerProcessor implements ItemProcessor<MailUser, EmailVO> {


	private static final Logger log = LoggerFactory.getLogger(NewsMailerProcessor.class);

	static String sDelimiter = "#$#";
	private MailerDao dao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public EmailVO process(MailUser user) throws Exception {

		long startTime = System.currentTimeMillis();

		EmailVO vo = new EmailVO();
		NewsLetter news = user.getNewsletter();
		if (news != null) {
			int daily_weekly = news.isDaily() ? 1: 2;
			int morning_evening = news.getScheduleTime();
			String newsid = news.getId();
			String displayformat = news.getDisplayFormate();

			log.debug("newsLetter Id : " + newsid + " , " + user.getSsoid() + " , format : " +  news.getDisplayFormate());
			String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + morning_evening;
			String sBody = InMemoryData.newsMailerUIMap.get(skey+Constant.KEY_BUILDER_SEPERATOR+displayformat);

			String subjectTitle = InMemoryData.newsMailerUIMap.get(skey + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1");

			String sNav = "";
			boolean blnShowNav = false;
			// code for non personalised newsletter check
			if (sBody != null && news.getUserPref() != null	&& (news.getUserPref().trim().length() > 1)) {
				boolean isContentFounD = false;
				String ids = news.getUserPref();
				if (ids != null && !"null".equals(ids)) {

					String[] idArray = ids.split("[,]");
					String[] sNavArray = news.getSecName().split("[|]");

					if (sNavArray.length == idArray.length) {
						blnShowNav = true;
					}

					sBody = sBody.replace("##pattern-newsletter##", " ");					
				} 


				sBody = sBody.replaceAll("#emailid" , user.getEmailId());
				
				if (news != null && news.getHashcode() != null) {
					sBody = sBody.replaceAll("#hashcode" , news.getHashcode());
				}

				vo.setBody(sBody);


				if (StringUtils.hasText(subjectTitle)) {
					vo.setSubject(encodeText(user.getEmailSubject() + ": " + subjectTitle, user.getEmailSubject()));

				} else {
					vo.setSubject(encodeText(user.getEmailSubject(), user.getEmailSubject()));
				}

				vo.setRecipients(user.getEmailId());
			}
		}

		log.debug("Mailer Process : " + (System.currentTimeMillis() - startTime) + " ms");
		return vo;
	}

	/**
	 * Encode the subject line and if any {@link UnsupportedEncodingException} exception raised then return base subject line.
	 * by separating colon char i.e. ":" else throw UnsupportedEncodingException.
	 * 
	 * @param subject
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String encodeText(String subject, String baseSubject ) throws UnsupportedEncodingException {
		String encodedSubject = null;
		try {

			for (String key : InMemoryData.SPECIAL_SYMBOL.keySet()) {
				subject = subject.replaceAll(key, InMemoryData.SPECIAL_SYMBOL.get(key));
			}

			encodedSubject = MimeUtility.encodeText(subject);
			if (encodedSubject.indexOf("?Q?") != -1 || encodedSubject.indexOf("?B?")!= -1) {
				return URLDecoder.decode(baseSubject, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			return URLDecoder.decode(baseSubject, "UTF-8");
		}
		return URLDecoder.decode(encodedSubject, "UTF-8");
	}


	public MailerDao getDao() {
		return dao;
	}

	public void setDao(MailerDao dao) {
		this.dao = dao;
	}
}
