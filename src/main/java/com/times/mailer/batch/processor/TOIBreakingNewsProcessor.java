package com.times.mailer.batch.processor;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.mail.internet.MimeUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.StringUtils;




import com.times.common.mail.EmailVO;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NewsLetter;


public class TOIBreakingNewsProcessor implements ItemProcessor<MailUser, EmailVO> {


	private static final Logger log = LoggerFactory.getLogger(TOIBreakingNewsProcessor.class);

	static String sDelimiter = "#$#";


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public EmailVO process(MailUser user) throws Exception {

		long startTime = System.currentTimeMillis();

		EmailVO vo = new EmailVO();
		NewsLetter news = user.getNewsletter();
		if (news != null) {
			int daily_weekly = news.isDaily() ? 1: 2;
			int morning_evening = news.getScheduleTime();
			String newsid = news.getId();
			String displayformat = news.getDisplayFormate();

			log.debug("newsLetter Id : " + newsid + " , " + user.getSsoid() + " , format : " +  news.getDisplayFormate());
			String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + morning_evening;
			String sBody = InMemoryData.newsMailerUIMap.get(skey+Constant.KEY_BUILDER_SEPERATOR+displayformat);

			String subjectTitle = InMemoryData.newsMailerUIMap.get(newsid + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1");			


			if (user.getNewsletter() != null && user.getNewsletter().getId() != null && 
					InMemoryData.newsletterContentMap.get(user.getNewsletter().getId()) != null) {
				String breakingNewsContent = InMemoryData.newsletterContentMap.get(user.getNewsletter().getId());
				
				if (subjectTitle == null) {
					StringTokenizer st = new StringTokenizer(breakingNewsContent, " ");
					ArrayList<String> token = new ArrayList<String>();
					while (st.hasMoreElements()) {
						token.add((String) st.nextElement());					
					}

					subjectTitle =  token.get(0) != null ?  token.get(0) : "";
					subjectTitle = subjectTitle + " " + (token.get(1) != null ?  token.get(1) : "");
					subjectTitle =  subjectTitle + " " + (token.get(2) != null ?  token.get(2) : "");
					subjectTitle =  subjectTitle + " " + (token.get(3) != null ?  token.get(3) : "");
					subjectTitle =  subjectTitle + " " + (token.get(4) != null ?  token.get(4) : "");
					if (subjectTitle.indexOf("<Br/>") != -1) {
						subjectTitle = subjectTitle.substring(0,subjectTitle.indexOf("<Br/>"));
					}
				}
				
				vo.setRecipients(user.getEmailId());
				String defaultSubject = user.getEmailSubject();
				
				if (StringUtils.hasText(subjectTitle)) {
					// MimeUtility.encodeText() is used to parsed encoding in proper format to avoid junk char 
					vo.setSubject(encodeText(defaultSubject + " : " + subjectTitle, defaultSubject));
				} else {
					vo.setSubject(encodeText(defaultSubject, defaultSubject));
				}
				

				vo.setBody(sBody.replaceAll("##pattern-newsletter##", breakingNewsContent));

				
			} else {
				vo.setBody(null);
			}
		}

		log.debug("Mailer Process : " + (System.currentTimeMillis() - startTime) + " ms");
		return vo;
	}

	/**
	 * Encode the subject line and if any {@link UnsupportedEncodingException} exception raised then return base subject line.
	 * by separating colon char i.e. ":" else throw UnsupportedEncodingException.
	 * 
	 * @param subject
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String encodeText(String subject, String baseSubject ) throws UnsupportedEncodingException {
		String encodedSubject = null;
		try {

			for (String key : InMemoryData.SPECIAL_SYMBOL.keySet()) {
				subject = subject.replaceAll(key, InMemoryData.SPECIAL_SYMBOL.get(key));
			}

			encodedSubject = MimeUtility.encodeText(subject);
			if (encodedSubject.indexOf("?Q?") != -1 || encodedSubject.indexOf("?B?")!= -1) {
				return baseSubject;
			}
		} catch (UnsupportedEncodingException e) {
			return baseSubject;
		}
		return encodedSubject;
	}	


}
