package com.times.mailer.batch.processor;

import java.util.Date;
import java.util.LinkedHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class PerfectMarketingProcessor implements ItemProcessor<LinkedHashMap<String, Date>,LinkedHashMap<String, Date>> {

	private static final Logger log = LoggerFactory.getLogger(PerfectMarketingProcessor.class);

	@Override
	public LinkedHashMap<String, Date> process(LinkedHashMap<String, Date> item) throws Exception {
		log.debug("pm process method start..");
		return item;
	}

}
