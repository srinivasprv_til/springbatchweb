package com.times.mailer.batch.processor;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.springframework.batch.item.ItemProcessor;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.util.EpaperOptimizerJpdf;
import com.times.mailer.model.EpaperFileDetails;

public class EpaperFileProcesser implements ItemProcessor<EpaperFileDetails, EpaperFileDetails> {

	private static final Log log = LogFactory.getLog(EpaperFileProcesser.class);

	EpaperOptimizerJpdf epaperOptimizerJpdf;

	public EpaperOptimizerJpdf getEpaperOptimizerJpdf() {
		return this.epaperOptimizerJpdf;
	}

	public void setEpaperOptimizerJpdf(EpaperOptimizerJpdf epaperOptimizerJpdf) {
		this.epaperOptimizerJpdf = epaperOptimizerJpdf;
	}

	@Override
	public EpaperFileDetails process(EpaperFileDetails item) throws Exception {
		String id = item.getId();
		ObjectId objectId = new ObjectId(id);

		DBObject query = new BasicDBObject();
		query.put("_id", objectId);
		try {
			Path targetTempFile = Files.createTempFile(Paths.get("/home/tilsuper/epaper_workspace"), "epaper_zip_",
			        "zip");
			epaperOptimizerJpdf.optimize(item.getPdfFileContent(), targetTempFile.toFile());
			item.setZipFileContent(Files.readAllBytes(targetTempFile));
		} catch (Exception e) {
			log.error("Exception occured while processing item: " + item, e);
		}

		return item;
	}

}