package com.times.mailer.batch.processor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.times.common.model.PerfectMarketingObject;
import com.times.common.util.InMemoryData;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;

/**
 * 
 * @author praveen.bokka
 *
 */
public class PerfectMarketNewUrlsProcessor implements ItemProcessor<Document, List<PerfectMarketingObject>> {

	private static final Logger log = LoggerFactory.getLogger(PerfectMarketNewUrlsProcessor.class);
	
	public static String SeoCharacter = "-";
	
	public List<PerfectMarketingObject> process(Document document) throws Exception {
		log.debug("PerfectMarketNewUrls process method start..");
		
		Date lastDate;
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		List<PerfectMarketingObject> pmList = new ArrayList<>();
		
		Element rootElem = document.getRootElement();
		
		List<Element> docList = rootElem.getChild("Documents").getChildren("Document");
		
		System.out.println("solrclient created");
		int lastMsid = 0;
		for(Element doc : docList) {
			String title = doc.getChildText("title");
			String description = doc.getChildText("text");
			if(description == null || description.trim().equals("")) {
				continue;
			}
			String id = doc.getChildText("msid");
			lastMsid = Integer.parseInt(id);
//			String date = doc.getChildText("effectivedate");
			String updateDate = doc.getChildText("updateDate");
			updateDate = updateDate.substring(0, 19) +"Z";
			lastDate = f.parse(updateDate);
			String effectiveDate = doc.getChildText("effectivedate");
			effectiveDate = effectiveDate.substring(0, 19) +"Z";
			Date effectiveLastDate = f.parse(effectiveDate);
//			String multipublished = doc.getChildText("multiPublishMaster");
			
			String seo = getSimpleString(title, true);
			
			String pmUrl = getPMUrl(seo, effectiveLastDate, lastMsid);
			
			PerfectMarketingObject pmObj = new PerfectMarketingObject();
			pmObj.setMsid(lastMsid);
			pmObj.setDateadded(lastDate);
			pmObj.setRedirecturl(pmUrl);
			pmObj.setExtractedfrom("created");
			
			pmList.add(pmObj);
			InMemoryData.perfectMarketMsid = lastMsid;
			InMemoryData.perfectMarketDate = lastDate;
		}
		log.debug("lastMsid : "+lastMsid);
		log.debug("PerfectMarketNewsUrl mehtod ends...");
		return pmList;
	}
	
	public static Date getDatefromString(String dateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	
	public static String getPMUrl(String seo, Date d, int msid) {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");//yyyy-MM-dd HH:mm:ssZ");
		f.setTimeZone(TimeZone.getTimeZone("IST"));
		String st = f.format(d);
		String pmUrl = "http://articles.economictimes.indiatimes.com/"+st+"/news/"+msid+"_1_"+seo;
		return pmUrl;
	}
	
	public static String getSimpleString(String msName, boolean lower) {
		int i;
		String sHTML = "";
		try {
//			msName = CMSXMLUtility.getValidRTF(msName);
			msName = msName.trim();
			sHTML = stripHTMLTags(msName).replaceAll(" ", SeoCharacter);
			sHTML = sHTML.replaceAll("\\.", SeoCharacter);
			if (sHTML.length() <= 0) {
				return sHTML;
				// 45=-, 48-57= 0-9, 65-90 =A-Z, 95=_, 97-122 = a-z
			}

			for (i = 0; i < sHTML.length(); i++) {
				if ((sHTML.charAt(i) != 45) && (sHTML.charAt(i) != 47) && (sHTML.charAt(i) != 95)
						&& ((sHTML.charAt(i) < 48) || (sHTML.charAt(i) > 57))
						&& ((sHTML.charAt(i) < 97) || (sHTML.charAt(i) > 122))
						&& ((sHTML.charAt(i) < 65) || (sHTML.charAt(i) > 90))) {
					sHTML = sHTML.replace(sHTML.charAt(i), '~');
				}
			}
			sHTML = sHTML.replaceAll("~", "");
		} catch (Exception e) {
			log.error("Error Occured in getSimpleString for String : " + msName, e);
			CMSCallExceptions tmperr = new CMSCallExceptions("Error Occured in getSimpleString for String : " + msName,
					CMSExceptionConstants.CMS_StringUtils_Exception, e);
			tmperr = null;
		}
		sHTML = sHTML.replaceAll("([-]+)", "-");
		if (lower) {
			sHTML = sHTML.toLowerCase();
		}
		try {
			StringTokenizer strToken = new StringTokenizer(sHTML, "/");
			String strPrev = "";
			String stripedSHTML = "";
			while (strToken.hasMoreTokens()) {
				String temp = strToken.nextToken();
				if (!strPrev.equalsIgnoreCase(temp)) {
					stripedSHTML = stripedSHTML + "/" + temp;
					strPrev = temp;
				}
			}
			sHTML = stripedSHTML.length() > 1 ? stripedSHTML.substring(1) : sHTML;
			strToken = null;
			strPrev = null;
			stripedSHTML = null;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return sHTML;
	}
	
	public static String stripHTMLTags(String msName) {
        int i = 0, j, k, iCtr;
        String sTemp, sTempBuff;
        sTempBuff = msName;
        i = sTempBuff.indexOf("<");
        j = sTempBuff.indexOf(">");
        k = sTempBuff.length();
        iCtr = 0;
        try {
            while ((i > -1) && (iCtr < k) && i < j) {
                sTemp = sTempBuff.substring(i, j + 1);
                sTempBuff = sTempBuff.substring(j + 1, sTempBuff.length());
                if ((!sTemp.equals("<BR>")) && !(sTemp.equals("<br>"))) {
                    msName = msName.replace(sTemp, "");
                }
                i = sTempBuff.indexOf("<");
                j = sTempBuff.indexOf(">");
                iCtr++;
            }
            return msName;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return StringEscapeUtils.escapeHtml4(msName);
        }
    }

}
