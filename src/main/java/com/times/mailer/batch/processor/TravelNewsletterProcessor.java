/**
 * 
 */
package com.times.mailer.batch.processor;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.mail.internet.MimeUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.StringUtils;

import com.times.common.mail.EmailVO;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.mailer.batch.MailerProcessor;
import com.times.mailer.dao.TravelNewsletterDao;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NewsLetter;

/**
 * This class is used as processor of Batch mailer in which building body content based on user selected.
 * spring batch forward this to MailerWriter class.
 * 
 * @author Ranjeet.Jha
 *
 */
public class TravelNewsletterProcessor implements ItemProcessor<MailUser, EmailVO> {

	//private static final Logger log = Log4jLoggerFactory.getLog(MailerProcessor.class);
	private static final Logger log = LoggerFactory.getLogger(MailerProcessor.class);
	
	static String sDelimiter = "#$#";
	private TravelNewsletterDao dao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public EmailVO process(MailUser user) throws Exception {

		EmailVO vo = new EmailVO();
		NewsLetter news = user.getNewsletter();
		if (news != null) {
			int daily_weekly = news.isDaily() ? 1: 2;
			int morning_evening = news.getScheduleTime();
			String newsid = news.getId();
			String displayformat = news.getDisplayFormate();
			
			log.debug("newsLetter Id : " + newsid + " , " + user.getSsoid() + " , format : " +  news.getDisplayFormate());
			String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + morning_evening;
			String sBody = InMemoryData.newsMailerUIMap.get(skey+Constant.KEY_BUILDER_SEPERATOR+displayformat);
			
			String subjectTitle = InMemoryData.newsMailerUIMap.get(skey + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1");
			
			
			
			if (sBody != null) {
				
				sBody = sBody.replaceAll("#emailid#" , user.getEmailId());
				vo.setBody(sBody);
				
				String defaultSubject = user.getEmailSubject() + " from HappyTrips.com";
				//vo.setSubject(encodeText(user.getEmailSubject()));
				if (StringUtils.hasText(subjectTitle)) {
					// MimeUtility.encodeText() is used to parsed encoding in proper format to avoid junk char 
					vo.setSubject(encodeText(subjectTitle, defaultSubject));
				} else {
					vo.setSubject(encodeText(user.getEmailSubject() + " and more", defaultSubject));
				}
				vo.setRecipients(user.getEmailId());
				
			}
		}
		
		return vo;
	}
	
	
	
	/**
	 * Encode the subject line and if any {@link UnsupportedEncodingException} exception raised then return base subject line.
	 * by separating colon char i.e. ":" else throw UnsupportedEncodingException.
	 * 
	 * @param subject
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String encodeText(String subject, String baseSubject ) throws UnsupportedEncodingException {
		String encodedSubject = null;
		try {
			for (String key : InMemoryData.SPECIAL_SYMBOL.keySet()) {
				subject = subject.replaceAll(key, InMemoryData.SPECIAL_SYMBOL.get(key));
			}
			encodedSubject = MimeUtility.encodeText(subject);
			if (encodedSubject.indexOf("?Q?") != -1 || encodedSubject.indexOf("?B?")!= -1 || encodedSubject.contains("\n")) {
				return baseSubject;
			}
		} catch (UnsupportedEncodingException e) {
			return baseSubject;
		}
		return encodedSubject;
	}
	
	/**
	 * @param idArray
	 * @param sNavArray
	 * @return
	 */
	
	
	public TravelNewsletterDao getDao() {
		return dao;
	}

	public void setDao(TravelNewsletterDao dao) {
		this.dao = dao;
	}
	
}
