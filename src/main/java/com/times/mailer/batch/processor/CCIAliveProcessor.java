package com.times.mailer.batch.processor;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.jdom.Document;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.util.CCIAliveUtils;
import com.times.common.util.InMemoryData;
import com.times.mailer.model.CCIAliveDoc;

/**
 * 
 * @author praveen.bokka
 *
 */
public class CCIAliveProcessor implements ItemProcessor<Document, List<DBObject>> {

	private static final Logger log = LoggerFactory.getLogger(CCIAliveProcessor.class);
	
	@Autowired
	private String solrLocation;
	
	public String getSolrLocation() {
		return solrLocation;
	}

	public void setSolrLocation(String solrLocation) {
		this.solrLocation = solrLocation;
	}
	
	private static String pubidGlobal;
	
	public List<DBObject> process(Document document) throws Exception {
		log.debug("etEpaper process method start..");
		
		List<DBObject> objectList = new ArrayList<DBObject>();
		Element rootElem = null;
		try {
			rootElem = document.getRootElement();
		} catch (Exception e) {
			return objectList;
		}
		
		HttpSolrClient server = new HttpSolrClient(solrLocation);
		
		List<Element> docList = rootElem.getChildren("doc");
		for(Element doc : docList) {
			
			String id = doc.getChildText("id");
			String title = doc.getChildText("title");
			String story = doc.getChildText("story");
			String subject = doc.getChildText("subject");
			String ingDate = doc.getChildText("ingestiondate");
			List<Element>	metadataList =	doc.getChild("associatedmetadata").getChild("source").getChildren("metadata");
			String edition ="";
			for(Element meta: metadataList){
				if(meta.getAttributeValue("name").equals("edition")) {
					edition = meta.getTextTrim();
					break;
				}
			}
			
			CCIAliveDoc docObj = new CCIAliveDoc();
			docObj.setId(id);
			docObj.setTitle(title);
			docObj.setStory(story);
			docObj.setSubject(subject);
			docObj.setEdition(edition);
			docObj.setType("CCI");
					
			DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
			Date date = null;
			try {
				date = format.parse(ingDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(date != null) {
				InMemoryData.CCIDate = date;
			}
					
			server.addBean(docObj);
			server.commit();
			List<CCIAliveDoc> cmsDocList = getSimilarDescriptionCMSDoc(server, story);
			if(cmsDocList.size() > 0) {
				CCIAliveDoc cmsDoc = cmsDocList.get(0);
				
				DBObject map = new BasicDBObject();
				map.put("msid", cmsDoc.getId());
				map.put("cmsTitle", cmsDoc.getTitle());
				map.put("cmsUrl", CCIAliveUtils.createCMSWebUrl(cmsDoc.getSeopath(),"http://timesofindia.indiatimes.com/", cmsDoc.getId()));
				map.put("CCIId", docObj.getId());
				map.put("title", docObj.getTitle());
				objectList.add(map);	
			}
		}		
		server.close();
		
//		System.out.println(objectList);
		
		log.debug("CCIAlive process method ends..");
		return objectList;
	}
	
	private List<CCIAliveDoc> getSimilarDescriptionCMSDoc(HttpSolrClient server,  String description) {
		SolrQuery query = new SolrQuery();
		QueryResponse response = null;
		description = description.replaceAll("[\\W]", " ");//remove all special characters
		description = description.length() > 4000 ? description.substring(0, 4000) : description;
		String q = description;// + " AND type:CMS";
		query.setQuery(q);
		query.setParam("df", "story");
		query.set("ns", "{!func}product(scale(query({!type=dismax v=$q}),0,1),100)");
		query.set("fq", "({!frange l=70}$ns AND type:CMS)");
//		query.set("type", "CMS");
		query.set("rows", 1);
		//query.set("fl", "*,score");
		query.setIncludeScore(true);
		
		List<CCIAliveDoc> list = null;
		
		try{
			response = server.query(query);
			list = response.getBeans(CCIAliveDoc.class);
		} catch (Exception e) {
			log.error("Exception from solrclient: "+e + " " + query);
		}
		
		
		return list;
	}

}
