package com.times.mailer.batch.processor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.batch.item.ItemProcessor;

import com.times.mailer.batch.reader.LangCommentReader;
import com.times.mailer.dao.LangCommentDAO;
import com.times.mailer.model.LangUserMailer;
import com.times.nbt.model.Newsletter;

public class LangCommentProcessor implements ItemProcessor<LangUserMailer, List<Newsletter>>{

	public static String strReplyTemplate ="repliednewsletter.cms"; //"replycommentemail";
	
	LangCommentDAO CommentDao;
	
	public String gerneralBodyMessage(LangUserMailer commentsData, int hostid){
		String mailMessage;
		switch (hostid) {
		case 155:
			mailMessage ="Dear Reader,</br></br>"
				+" You just wrote a comment on Maharashtratimes.com. "
				+"	Please <a href='http://Maharashtratimes.indiatimes.com/usrmailcomment.cms?msid=[#article_id]&usrmail=[#fromaddress]&mailon_commented=1'> click here </a> "
				+" to view it. </br> Do keep writing in. Thank you! </br></br>Rgds, </br> Editor </br></br></br></br></br>";
		
			break;

		default:
			mailMessage="";
			break;
		}
		return mailMessage;
	}
	
	
	
	private String getViewData(String view, Map<String, String> user, boolean isParent){
		Iterator<String> itr = user.keySet().iterator(); 
		while(itr.hasNext()){
			String key = itr.next();
			String value = String.valueOf(user.get(key));
			if (isParent){
				key ="p_"+ key;
			}
			
			
			if (value == null){
				value =" ";
			}
			view = view.replaceAll("\\[#"+key+"]", value);
			
			
		}
		return view;
	}
	
	@Override
	public List<Newsletter> process(LangUserMailer item)
			throws Exception {
		
		List<Newsletter> processNewsMailer = new ArrayList<Newsletter>();
		
		if (item !=null){
			String metaData="";
			String title;
			String StrBody;
			String hostid;
			
			int msid = Integer.parseInt(String.valueOf(item.getMsid()));
			Map<String, Object> cmsData = getCommentDao().getCMSObject(msid);
			hostid =String.valueOf(item.getParamMap().get("hostid"));
			
			if (cmsData!=null){
				metaData = String.valueOf(cmsData.get("metadata"));
				title = String.valueOf(cmsData.get("msname"));
				item.getParamMap().put("cms_title", title);
			}
		
			//get view for mailer...
			
			
			if (LangCommentReader.lstConfig !=null   && "1".equals(LangCommentReader.lstConfig.get(hostid).getCustomization())){
				if (metaData.trim().length()==0){
					metaData = hostid;
				}
			
				if (LangCommentReader.customCommentView.containsKey(hostid+"_"+ metaData)){
					StrBody = LangCommentReader.customCommentView.get(hostid+"_"+ metaData);
				}else{
					StrBody = LangCommentReader.customCommentView.get(hostid+"_"+ hostid);
				}
				
					
			}else{
				StrBody =gerneralBodyMessage(item, Integer.parseInt(String.valueOf(item.getParamMap().get("hostid"))));
			}
			
			
			if (StrBody!=null && StrBody.trim().length()>10){
				StrBody  = getViewData(StrBody, item.getParamMap(), false);
				Newsletter userMail = new Newsletter();
				userMail.setCmtid(item.getEroid());
				userMail.setHostid(Integer.parseInt(hostid));
				userMail.setMailContent(StrBody);
				userMail.setMailSubject("Your comment is live");
				if (LangCommentReader.lstConfig!=null){
					userMail.setMsgSenderEmail(String.valueOf(LangCommentReader.lstConfig.get(hostid).getFrmName()));
							
				}
				userMail.setMsgContentType("text/html");
				userMail.setMsgSenderName(String.valueOf(LangCommentReader.lstConfig.get(hostid).getFrmName()));
				userMail.setMsgTo(String.valueOf(item.getParamMap().get("altemailid")));
				//userMail.setMsgTo("nitesh.kumar@timesinternet.in");
				processNewsMailer.add(userMail);
		
				if (! ("0".equals(LangCommentReader.lstConfig.get(hostid).getReply()))){
					if (item.getParamMap().containsKey("parentid")){
						int id = Integer.parseInt(String.valueOf(item.getParamMap().get("parentid")));
						List<LangUserMailer> langMailer  = getCommentDao().getParentComments(id);
						if (langMailer!=null && langMailer.size()>0){
							LangUserMailer parentData = langMailer.get(0);
							if (!LangCommentReader.customCommentView.containsKey(hostid+"_replycomment")){
								StrBody ="";
							}else{
								StrBody = String.valueOf(LangCommentReader.customCommentView.get(hostid+"_replycomment"));
								StrBody = getViewData(StrBody, item.getParamMap(), false );
								StrBody = getViewData(StrBody, parentData.getParamMap(), true);
							}
				
							if (StrBody !=null && StrBody.trim().length() >10){
								Newsletter userpMail = new Newsletter();
								userpMail.setCmtid(item.getEroid());
								userpMail.setMailContent(StrBody);
								
								userpMail.setMailSubject(String.valueOf(item.getParamMap().get("fromname")) +" has replied to your comment on "+ LangCommentReader.lstConfig.get(hostid).getChannelName());
								userpMail.setMsgSenderEmail(LangCommentReader.lstConfig.get(hostid).getFrmName());
								userpMail.setMsgContentType("text/html");
								userpMail.setMsgSenderName(LangCommentReader.lstConfig.get(hostid).getFrmName());
								userpMail.setMsgTo(String.valueOf(parentData.getParamMap().get("altemailid")));
								//userpMail.setMsgTo("nitesh.kumar@timesinternet.in");
								processNewsMailer.add(userpMail);	
							}
						}
					}
				}
			}
			
		}
		return processNewsMailer;
	}


	public LangCommentDAO getCommentDao() {
		return CommentDao;
	}


	public void setCommentDao(LangCommentDAO commentDao) {
		CommentDao = commentDao;
	}

	


}
