/**
 * 
 */
package com.times.mailer.batch.processor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.StringUtils;

import com.mongodb.DBObject;
import com.times.common.mail.EmailVO;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.common.util.MailerUtil;
import com.times.mailer.dao.NewsAlertDao;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NewsLetter;


public class NewsAlertProcessor implements ItemProcessor<MailUser, List<EmailVO>> {

	private static final Logger log = LoggerFactory.getLogger(NewsAlertProcessor.class);

	static String sDelimiter = "#$#";
	private NewsAlertDao newsAlertDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public List<EmailVO> process(MailUser user) throws Exception {
		boolean isContentPieceFound = false;

		List<EmailVO> emailVOList = new ArrayList<EmailVO>();

		NewsLetter news = user.getNewsletter();


		EmailVO vo = new EmailVO();

		Map<String, Set<String>> nlidMsidMap = new HashMap<String, Set<String>>();
		Map<Integer, String> msidTimestampMap = new HashMap<Integer, String>();

		// get sent user and msid info to mongoDB
		DBObject dbObject = newsAlertDao.getAlertDBObjectEmailSent(user.getEmailId());


		try {

			if (user.getNewsletter() != null && user.getNewsletter().getId() != null && InMemoryData.newsletterContentMap.get(user.getNewsletter().getId()) != null) {
				String msidToBeUpdate = InMemoryData.newsletterContentMap.get(user.getNewsletter().getId());
				
				Set<Integer> newMsids = new HashSet<Integer>();
				
				if (msidToBeUpdate != null && StringUtils.hasText(msidToBeUpdate)) {
					String msidArr[] = msidToBeUpdate.split(",");
					for (String msid : msidArr) {
						newMsids.add(Integer.parseInt(msid));
					}
				}
				
				Set<Integer> notSentmsid = getNewMsidNotFoundInDB(newMsids,dbObject);
				
				if (notSentmsid.size() > 0) {

					vo.setRecipients(user.getEmailId());
					vo.setSender(user.getEmailSubject());

					// set email body.
					getEmailBody(user, isContentPieceFound, vo, news, notSentmsid);

					emailVOList.add(vo);

					if (nlidMsidMap.get(user.getNewsletter().getId()) == null ) {
						Set<String> temp = new HashSet<String>();
						for (Integer i : notSentmsid) {
							temp.add(i.toString());
						}						
						nlidMsidMap.put(user.getNewsletter().getId(),temp);
					} else {
						
						for (Integer i : notSentmsid) {
							nlidMsidMap.get(user.getNewsletter().getId()).add(i.toString());
						}
						
					}

					for (Integer i : notSentmsid) {
						msidTimestampMap.put(i, new Date().toString());
					}
					List<Integer> listToBeUpdate = new ArrayList<Integer>(notSentmsid);
					
					if (dbObject != null) {
						//dbObject.put(Constant.MSID, listToBeUpdate);
						newsAlertDao.updateAlertLogCollection(dbObject , nlidMsidMap, msidTimestampMap);
						
					} else if (dbObject == null) {					
						newsAlertDao.addAlertUser(user ,nlidMsidMap, msidTimestampMap);
					}
				} else {
					vo.setBody(null);
				}
			} else {
				vo.setBody(null);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}


		return emailVOList;
	}







	/**
	 * This API make emailBody of mail.
	 * @param user
	 * @param isContentPieceFound
	 * @param vo
	 * @param news
	 * @param notSentmsid
	 * @param preference
	 * @return
	 */
	private EmailVO getEmailBody(MailUser user, boolean isContentPieceFound, EmailVO vo, NewsLetter news, Set<Integer> notSentmsid) {
		Integer msid;
		String contentPieces;
		if (news != null) {
			int daily_weekly = news.isDaily() ? 1: 2;
			//int morning_evening = news.getScheduleTime();
			int morning_evening = 1;
			String newsid = news.getId();
			String displayformat = news.getDisplayFormate();

			log.debug("newsLetter Id : " + newsid + " , " + user.getEmailId() + " , format : " +  news.getDisplayFormate());
			String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + morning_evening;
			String sBody = InMemoryData.newsMailerUIMap.get(skey+Constant.KEY_BUILDER_SEPERATOR+displayformat);

			// code for non personalised newsletter check
			if (sBody != null && notSentmsid != null && notSentmsid.size() > 0) {
				StringBuilder mailBodySB = new StringBuilder();



				for (Iterator<Integer> it = notSentmsid.iterator(); it.hasNext();) {
					msid = it.next();
					// get html body part for particular msid.
					String bodyPart = getTemplateDataInHTML(user.getTemplatePath(),msid);
					if (bodyPart != null) {
						mailBodySB.append(bodyPart);
					}
				}
				contentPieces = mailBodySB.toString();

				if (StringUtils.hasText(contentPieces)) {

					contentPieces = mailBodySB.toString();
					if (StringUtils.hasText(contentPieces)) {
						isContentPieceFound = true;

						if (sBody != null) {
							
							sBody = sBody.replace("[#articlebody]", contentPieces);
							sBody = sBody.replace("[#emailid]", user.getEmailId());
							sBody = sBody.replace("[#nlid]", user.getNewsletter().getId());
							sBody = sBody.replace("[#hashcode]", user.getNewsletter().getHashcode());

						}
					}

					// if  contentPiece is not then do not send mail.
					if (isContentPieceFound) {
						//sBody = sBody.replace("#emailid#" , user.getEmailId());
						vo.setBody(sBody);
					} else {
						vo.setBody(null);
					}

					vo.setSubject(user.getEmailSubject());
					vo.setRecipients(user.getEmailId());

				}
			}
		}
		return vo;
	}


	/**
	 * @param newsletterId
	 * @return
	 */
	private DBObject getNewsletterMasterByNewsletterID(String newsletterId) {
		DBObject nlDBObject = null;

		List<DBObject> dbObjects = newsAlertDao.getNewsletterDBObjectById("1040");

		if (dbObjects != null && dbObjects.size() > 0) {
			nlDBObject = dbObjects.get(0);
		}

		return nlDBObject;
	}


	/**
	 * @param travelNewsletterDao the travelNewsletterDao to set
	 */
	public void setNewsAlertDao(NewsAlertDao newsAlertDao) {
		this.newsAlertDao = newsAlertDao;
	}


	/**
	 * This api help to get html body of given msid. If body is not already stored in contentMap, http call is executed and result stored in map for future use.
	 * @param url
	 * @param msid
	 * @return
	 */
	private String getTemplateDataInHTML(String url, Integer msid) {

		if(StringUtils.hasText(InMemoryData.contentMap.get(msid))) {
			return InMemoryData.contentMap.get(msid);
		} else {

			url = url.replace("#msid", msid.toString());
			String content = MailerUtil.postData(url);
			if (StringUtils.hasText(content)) {
				InMemoryData.contentMap.put(msid, content);
				return content;			
			} else {
				return null;
			}
		}

	}


	private Set<Integer> getNewMsidNotFoundInDB(Set<Integer> newMsids, DBObject dbObject) {
		Set<Integer> notSentmsid = new HashSet<Integer>();

		if (newMsids != null && dbObject != null) {
			//List<Integer> dbMsids = (List) dbObject.get(Constant.MSID);
			List<Integer> dbMsids = null;
			Map<Integer, String> msidTimestampMap = (Map) dbObject.get(Constant.MSID);
			if (msidTimestampMap != null) {
				dbMsids = new ArrayList<Integer>(msidTimestampMap.keySet());
			}
			if (dbMsids != null && dbMsids.size() > 0) {
				Iterator<Integer> it = newMsids.iterator();
				while(it.hasNext()) {
					Integer i = it.next();	
					if (!dbMsids.contains(i.toString())) {
						notSentmsid.add(i.intValue());
					}
				} 
			}else  {
				notSentmsid = newMsids;
			}
		} else if (newMsids != null && dbObject == null) {
			notSentmsid = newMsids;
		}

		return notSentmsid;
	}

}
