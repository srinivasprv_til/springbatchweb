package com.times.mailer.batch.processor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;
import org.springframework.batch.item.ItemProcessor;

import com.google.gson.Gson;
import com.mongodb.util.Hash;
import com.times.common.service.FileContentServices;
import com.times.mailer.model.CandidateConstituency;
import com.times.mailer.model.EPollingConstant;
import com.times.mailer.model.FileContentInfo;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.PartyGroupDetails;
import com.times.mailer.model.PollingConstants;
import com.times.mailer.model.StateResults;

public class EPollingConstituencyData implements ItemProcessor<FileLocationInfo, FileContentInfo> {

	//private ;

	private FileContentServices fileContentServices;

	private HashMap<Integer, Integer> mapLeadingPartyStatus =new HashMap<Integer, Integer>();
	private HashMap<Integer, Integer> mapWinPartyStatus = new HashMap<Integer, Integer>();
	
	/**
	 * @return the fileContentServices
	 */
	public FileContentServices getFileContentServices() {
		return fileContentServices;
	}

	/**
	 * @param fileContentServices the fileContentServices to set
	 */
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}

	@Override
	public FileContentInfo process(FileLocationInfo fileLocationInfo) throws Exception {
	
		if (fileLocationInfo != null) {
			if ("party".equals( fileLocationInfo.getCallbackName())){
				String strData = fileContentServices.getContentByHTTP(fileLocationInfo.getSourceUrl());
				if (strData!=null ){
				String str = getAllIndia(strData,"all","party");
				if (str!=null){
					System.out.println(fileLocationInfo.getSourceUrl());
					fileContentServices.uploadContentByFTP(fileLocationInfo.getNetstorageLocation(),str);
					
					// upload content to Amazon S3
					fileContentServices.uploadFileAmazonS3(fileLocationInfo.getAmazonS3(), str, null);
					
				}
				}
			}else if ("statewise".equals( fileLocationInfo.getCallbackName())){
				String sourceUrl = fileLocationInfo.getSourceUrl();
				String strData = fileContentServices.getContentByHTTP(sourceUrl);
				if (strData!=null ){
					String str=  getPartyWiseStateInfo(strData, String.valueOf(fileLocationInfo.getStateId()), "statewise");
					if (str!=null){
						System.out.println(fileLocationInfo.getSourceUrl());
						System.out.println(fileLocationInfo.getNetstorageLocation().getFileName());
						fileContentServices.uploadContentByFTP(fileLocationInfo.getNetstorageLocation(),str);
						
						// upload content to Amazon S3
						fileContentServices.uploadFileAmazonS3(fileLocationInfo.getAmazonS3(), str, null);
						
					}
										
				}
			}else if ("update_const".equals( fileLocationInfo.getCallbackName())){
				String sourceUrl = fileLocationInfo.getSourceUrl();
				String strData = fileContentServices.getContentByHTTP(sourceUrl);
				if (strData !=null){
				String str = getJsonDataforConstituencyFeed(strData, String.valueOf(fileLocationInfo.getStateId()), "update_const");
				if (str!=null){
					System.out.println(fileLocationInfo.getSourceUrl());
					fileContentServices.uploadContentByFTP(fileLocationInfo.getNetstorageLocation(),str);
					
					// upload content to Amazon S3
					fileContentServices.uploadFileAmazonS3(fileLocationInfo.getAmazonS3(), str, null);
					
				}
				}
			}
			
			
			//statebased
		/*	for (int i=1;i<36;i++){
				String sourceUrl=" http://cmsall.indiatimes.com/epollingnew/readxml.aspx?lid=1&ail=0&stateid="+i+"&countryid=1&typeid=1&pid=3&dt=2009-05-01";
				String strData = fileContentServices.getContentByHTTP(sourceUrl);
				
				String str = getJsonDataforConstituencyFeed(strData, String.valueOf(i), "callback");
				System.out.println("constituencyUpdates");
				System.out.println(str);
				System.out.println("Statewise updates");
				str=  getPartyWiseStateInfo(strData, String.valueOf(i), "callback");
				System.out.println(str);
				
			}*/
			/*
			System.out.println("process completed for the data constituency updates");
			String sourceUrl ="http://10.157.210.17:2014/readxml.aspx?allindia=1&countryid=1&typeid=1&pid=3&dt=2014-05-01";
			String strData = fileContentServices.getContentByHTTP(sourceUrl);
			getAllIndia(strData,"all","callback");*/
		
		}
		
		
		return null;
	}
	
	private String groupName(String PartyAlias){
		if (!(PollingConstants.PartyGroupMap!=null && PollingConstants.PartyGroupMap.size()>0)){
			PollingConstants.partyGroupMapping();
		}
		if (PollingConstants.PartyGroupMap.containsKey(PartyAlias)){
			return PollingConstants.PartyGroupMap.get(PartyAlias);
		}else
			return null;
	}
	
	
	private String getAllIndia(String fileContent, String stateInfo, String callBack ) throws JDOMException, IOException{
		
		if(!fileContent.contains("<?xml version=")){
			fileContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + fileContent;
		}
		
		SAXBuilder builder = new SAXBuilder();
		InputStream in;
		try{
			in = new ByteArrayInputStream(fileContent.getBytes("UTF-8"));
			ArrayList<PartyGroupDetails> arrPartyDetails = new ArrayList<PartyGroupDetails>();
			Document doc = builder.build(in);
			XPath xPath = XPath.newInstance("//./electioninfo/state/result");
			Element rootElement = doc.getRootElement();
			List<Element> resultNode = xPath.selectNodes(rootElement);
			for (Element element : resultNode) {
				if (element.getChild("partyinfo")!=null){
					Element ele = element.getChild("partyinfo");
					int ilead =0, iwon=0;
					if (ele.getAttribute("lead")!=null && !"".equals(ele.getAttributeValue("lead").trim())){
						 ilead = Integer.parseInt(String.valueOf( ele.getAttributeValue("lead").trim()));
					}
					if (ele.getAttribute("won")!=null && !"".equals(ele.getAttributeValue("won").trim())){
						 iwon = Integer.parseInt(String.valueOf( ele.getAttributeValue("won").trim()));
					}
					int total = iwon+ ilead;
					PartyGroupDetails obj = new PartyGroupDetails();
					obj.setParty(ele.getAttributeValue("partyalias"));
					obj.setParty_fullname(ele.getAttributeValue("partyname"));
					obj.setColorcode(ele.getAttributeValue("color"));
					String str= groupName(ele.getAttributeValue("partyalias"));
					if (str!=null)obj.setGroupname(str);
					obj.setTotal(String.valueOf(total));
					obj.setLeading(String.valueOf(ilead));
					obj.setWon(String.valueOf(iwon));
					boolean isUpdated =false;
					for (int i=0; i<arrPartyDetails.size();i++){
						if (arrPartyDetails.get(i).getParty().equals(String.valueOf(ele.getAttributeValue("partyalias")))){
							isUpdated =true;
							PartyGroupDetails obj11 = arrPartyDetails.get(i);
							total = total+ Integer.parseInt( String.valueOf(obj11.getTotal()));
							ilead = ilead+  Integer.parseInt( String.valueOf(obj11.getLeading()));
							iwon = iwon+ Integer.parseInt( String.valueOf(obj11.getWon()));
							obj11.setTotal(String.valueOf(total));
							obj11.setLeading(String.valueOf(ilead));
							obj11.setWon(String.valueOf(iwon));
							arrPartyDetails.set(i, obj11);
						}
					}
					if (!isUpdated){
						arrPartyDetails.add(obj);
					}
					
				}
				
			}

			StateResults state = new StateResults();
			state.setState("all");
			state.setParties(arrPartyDetails);
			Gson gson = new Gson ();
			String str= gson.toJson(state);
			if (str!=null)
				return  callBack +"("+ str +")";
			else
				return null;
			
			
		}catch(Exception ex){
			
		}finally{
			if (builder !=null)
				builder=null;		
		}
		
		
		return null;
	
	}
	
		
	
	
	
	private String getPartyWiseStateInfo(String fileContent, String stateInfo, String callBack ) throws JDOMException, IOException{
		if(!fileContent.contains("<?xml version=")){
			fileContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + fileContent;
		}
		SAXBuilder builder = new SAXBuilder();
		InputStream in;
		try{
			in = new ByteArrayInputStream(fileContent.getBytes("UTF-8"));
			Document doc = builder.build(in);
			XPath xPath = XPath.newInstance("//./pollxml/partycontesting/party");
			Element rootElement = doc.getRootElement();
			Map<Integer,String> partyMappingInfo = PartyMapping(fileContent);
			List<Element> resultNode = xPath.selectNodes(rootElement);
			if (resultNode!=null){
				ArrayList<PartyGroupDetails> groupsummary = new ArrayList<PartyGroupDetails>();
				for (Element node : resultNode) {
					PartyGroupDetails parties = new PartyGroupDetails();
					if (node.getAttribute("color")!=null) parties.setColorcode(node.getAttributeValue("color"));
					if (node.getAttribute("aliase")!=null) parties.setParty(node.getAttributeValue("aliase"));
					
					if (node.getAttribute("name")!=null)parties.setParty_fullname(node.getAttributeValue("name"));
					int iWon = 0, ilead=0;
					if (mapLeadingPartyStatus!=null && mapLeadingPartyStatus.containsKey(Integer.parseInt(node.getAttributeValue("id")))){
						ilead = mapLeadingPartyStatus.get(Integer.parseInt(node.getAttributeValue("id")));
					}
					if(mapWinPartyStatus!=null & mapWinPartyStatus.containsKey(Integer.parseInt(node.getAttributeValue("id")))){
						iWon = mapWinPartyStatus.get(Integer.parseInt(node.getAttributeValue("id")));
					}
					parties.setLeading(String.valueOf(ilead));
					parties.setWon(String.valueOf(iWon));
					parties.setTotal(String.valueOf((ilead+iWon)));	
					String strGroupName= groupName(node.getAttributeValue("aliase"));
					if (strGroupName!=null) parties.setGroupname(strGroupName);
					groupsummary.add(parties);
				}
				
				StateResults state = new StateResults();
				state.setState(PollingConstants.getStateNameforEpolling(Integer.parseInt(stateInfo)));
				state.setParties(groupsummary);
				Gson gson = new Gson();
				String str = gson.toJson(state); 
				if (str!=null)
					return  callBack +"("+ str +")";
				else
					return null;
				
			}
		}catch(Exception ex){
			
		}finally{
			if (builder!=null)
				builder=null;
		}
		
		return null;
	}
	
	
	private Map<Integer,String> PartyMapping(String fileContent) throws JDOMException, IOException{
		if(!fileContent.contains("<?xml version=")){
			fileContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + fileContent;
		}
		SAXBuilder builder = new SAXBuilder();
		InputStream in;
		try {
			in = new ByteArrayInputStream(fileContent.getBytes("UTF-8"));
			Document doc = builder.build(in);
			XPath xPath = XPath.newInstance("//./pollxml/partycontesting/party");
			Element rootElement = doc.getRootElement();
			List<Element> resultNode = xPath.selectNodes(rootElement);
			if (resultNode!=null){
				HashMap< Integer, String> partymapping = new HashMap<Integer, String>();
				for (Element node : resultNode) {
					if (node.getAttribute("id")!=null && node.getAttribute("aliase")!=null){
						partymapping.put(Integer.parseInt(String.valueOf(node.getAttributeValue("id"))), String.valueOf(node.getAttributeValue("aliase")));
					}
				}
				return partymapping;
			}
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			builder =null;
		}
		
		return null;
	}
	
	private Map<Integer, String> getStateInfo(String fileContent) throws JDOMException, IOException{
		if(!fileContent.contains("<?xml version=")){
			fileContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + fileContent;
		}
		SAXBuilder builder = new SAXBuilder();
		InputStream in;
		try {
			in = new ByteArrayInputStream(fileContent.getBytes("UTF-8"));
			Document doc = builder.build(in);
			XPath xPath = XPath.newInstance("//./pollxml/statecon/cont");
			Element rootElement = doc.getRootElement();
			List<Element> resultNode = xPath.selectNodes(rootElement);
			if (resultNode!=null){
				HashMap< Integer, String> constMapping = new HashMap<Integer, String>();
				for (Element node : resultNode) {
					if (node.getAttribute("contid")!=null && node.getAttribute("name")!=null){
						constMapping.put(Integer.parseInt(String.valueOf( node.getAttributeValue("contid"))), node.getAttributeValue("name"));
						
					}
				}
				return constMapping;
			}
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			builder =null;
		}
		
		
		return null;
		
	}

	private void addWinCount(int iPartyid){
		int iwonSeats=1;
		if (mapWinPartyStatus!=null && mapWinPartyStatus.containsKey(iPartyid)){
				 iwonSeats = mapWinPartyStatus.get(iPartyid)+1;
				}			
		mapWinPartyStatus.put(iPartyid, iwonSeats);
	}
	
	private void addLeadCount(int iPartyid){
		int iwonSeats=1;
		if (mapLeadingPartyStatus.containsKey(iPartyid)){
			 iwonSeats = mapLeadingPartyStatus.get(iPartyid)+1;
		}			
		mapLeadingPartyStatus.put(iPartyid, iwonSeats);
	}	
	
	
	private String getJsonDataforConstituencyFeed(String fileContent, String stateInfo, String callBack)
			throws UnsupportedEncodingException, JDOMException, IOException {
	
		if(!fileContent.contains("<?xml version=")){
			fileContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + fileContent;
		}
		
		SAXBuilder builder = new SAXBuilder();
		InputStream in = new ByteArrayInputStream(fileContent.getBytes("UTF-8"));
		Document doc = builder.build(in);
		XPath xPath = XPath.newInstance("//./pollxml/result/info");
		ArrayList<CandidateConstituency> groupsummary = new ArrayList<CandidateConstituency>();
		Element rootElement = doc.getRootElement();
		List<Element> resultNode = xPath.selectNodes(rootElement);
		if (resultNode != null) {
			Map<Integer,String> constituencyInfo = getStateInfo(fileContent);
			Map<Integer,String> partyMappingInfo = PartyMapping(fileContent);
			for (Element node : resultNode) {
				CandidateConstituency constituencyupdates = new CandidateConstituency();
				if (node.getAttribute("winner")!=null && !( "".equals(node.getAttributeValue("winner").trim()))) {
					String winner =String.valueOf(node.getAttributeValue("winner"));
					addWinCount(Integer.parseInt(winner));
					constituencyupdates.setStatus("WIN");
					constituencyupdates.setPartyName(partyMappingInfo.get(Integer.parseInt(winner)));
					String strGroupName= groupName(partyMappingInfo.get(Integer.parseInt(winner)));
					if (strGroupName!=null) constituencyupdates.setGroupname(strGroupName);
					
				}
				if (node.getAttribute("leading")!=null && !( "".equals(node.getAttributeValue("leading").trim()))) {
					String leading =String.valueOf(node.getAttributeValue("leading"));
					addLeadCount(Integer.parseInt(leading));
					constituencyupdates.setStatus("LEAD");
					constituencyupdates.setPartyName(partyMappingInfo.get(Integer.parseInt(leading)));
					String strGroupName= groupName(partyMappingInfo.get(Integer.parseInt(leading)));
					if (strGroupName!=null) constituencyupdates.setGroupname(strGroupName);
				}
				if (node.getAttribute("id")!=null ) {
					String constituencyid =String.valueOf(node.getAttributeValue("id"));
					if (constituencyInfo!=null){
						constituencyupdates.setConstituencyid(Integer.parseInt(constituencyid));
						String constituencName = String.valueOf(constituencyInfo.get(Integer.parseInt(String.valueOf(node.getAttributeValue("id")))));	
						constituencyupdates.setConstituency(constituencName);
					}
					
				}
				if (node.getAttribute("wcontestant")!=null) {
					constituencyupdates.setCandidatename(String.valueOf(node.getAttributeValue("wcontestant")));
				}
				constituencyupdates.setState(PollingConstants.getStateNameforEpolling(Integer.parseInt(stateInfo)));
				groupsummary.add(constituencyupdates);
				
					
					
			}
		}
		StateResults result = new StateResults();
		result.setState(PollingConstants.getStateNameforEpolling(Integer.parseInt(stateInfo)));
		result.setConstituencies(groupsummary);
		
		Gson gson = new Gson();
		String str=  gson.toJson(result);
		if (str!=null)
			return  callBack +"("+ str +")";
		else
			return null;
		
	
	}
	

}
