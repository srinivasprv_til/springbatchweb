package com.times.mailer.batch;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;

/**
 * This batch writer is used to send the mail to the subscribed user.
 * 
 * @author manish.chandra
 * @author Ranjeet.Jha
 *
 */
public class MailerWritter implements ItemWriter<EmailVO> {
	
	private static final Log log = LogFactory.getLog(MailerWritter.class);
	
	private EmailProcessor smtpMailer;

	/* (non-Javadoc)
	 * @see org.springframework.batch.item.ItemWriter#write(java.util.List)
	 */
	public void write(List<? extends EmailVO> mail) throws Exception {
		log.debug("inside the write ");
		if (mail != null && mail.get(0) != null && mail.get(0).getBody() != null) {
			log.debug("email Sent to : " + mail.get(0).getRecipients());
			smtpMailer.sendEMail(mail.get(0));
		}		
	}

	/**
	 * @return
	 */
	public EmailProcessor getSmtpMailer() {
		return smtpMailer;
	}

	/**
	 * @param smtpMailer
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}
}