package com.times.mailer.batch;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;


/**
 * Store data in app_user_data collection. channel_id is unique for each user. So if user entry exist , only data updated.
 * @author Rajeev.Khatri
 *
 */
public class AppDataWritter implements ItemWriter<List<DBObject>> {

	private static final Log log = LogFactory.getLog(AppDataWritter.class);

	public void write(List<? extends List<DBObject>> items) throws Exception {
		log.debug("inside the write AppDataWritter");
		List<DBObject> appUserDetailList = items.get(0);
		if (appUserDetailList != null && appUserDetailList.size() >  0) {
			for (DBObject appUserDetails : appUserDetailList) {

				if (appUserDetails != null ) {
					DBObject queryJson = new BasicDBObject();
					queryJson.put("channel_id", appUserDetails.get("channel_id"));

					BasicDBObject newDocument = new BasicDBObject();
					newDocument.append("$set", appUserDetails);

					MongoRestAPIService.update("app_user_data", queryJson, newDocument, true);
				}
			}
		}
	}

}