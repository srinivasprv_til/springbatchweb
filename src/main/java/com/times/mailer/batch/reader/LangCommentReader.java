package com.times.mailer.batch.reader;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.util.MailerUtil;
import com.times.mailer.batch.processor.LangCommentProcessor;
import com.times.mailer.dao.LangCommentDAO;
import com.times.mailer.dao.MailerDao;
import com.times.mailer.model.LangConfiguration;
import com.times.mailer.model.LangUserMailer;

public class LangCommentReader implements ItemReader<LangUserMailer>{

	String poisonFile;
	int index=0;
	LangCommentDAO CommentDao;
	MailerDao ConfigDAO;
	
	List<LangUserMailer> lstCommentUser;
	public static Map<String, String> customCommentView ;
	public static Map<String,LangConfiguration> lstConfig;
	
	
	public LangCommentDAO getCommentDao() {
		return CommentDao;
	}


	public MailerDao getConfigDAO() {
		return ConfigDAO;
	}


	public void setConfigDAO(MailerDao configDAO) {
		ConfigDAO = configDAO;
	}


	public void setCommentDao(LangCommentDAO commentDao) {
		CommentDao = commentDao;
	}

	
	public boolean initializePersonalizeView(){
		try {
			if (customCommentView == null){
					customCommentView = new HashMap<String, String>();
	
			List<LangConfiguration> config  =  getConfigDAO().getCommentsConfig();
			
			if (config!=null && config.size()>0){
				lstConfig = new HashMap<String, LangConfiguration>(); 
				
				for (LangConfiguration langConfiguration : config) {
					lstConfig.put(langConfiguration.getHostid(), langConfiguration);
					if ("true".equals(langConfiguration.getReply())){
						String httpUrl = langConfiguration.getChannelName() +""+  LangCommentProcessor.strReplyTemplate;
						String shostid=String.valueOf(langConfiguration.getHostid());
						String outData = MailerUtil.getData(httpUrl);
						if ((outData ==null)||(outData.trim()=="")? false:true){
							customCommentView.put(shostid+ "_replycomment", outData);
							}
					}
					
					
					if (langConfiguration.getMapKeyMapping()!=null && langConfiguration.getMapKeyMapping().size()>0){
						Map<String, String> strMapConfig =langConfiguration.getMapKeyMapping();
						Iterator<String> itr = strMapConfig.keySet().iterator();
						while (itr.hasNext()){
							String skey = itr.next();
							String httpUrl = String.valueOf(strMapConfig.get(skey));
							String shostid=String.valueOf(langConfiguration.getHostid());
							String outData = MailerUtil.getData(httpUrl);
							if ((outData ==null)||(outData.trim()=="")? false:true){
								customCommentView.put(shostid+"_"+ skey, outData);
								}
							}	
						}
					} 
				}
			return true;
			}
			
			return false;
		} catch (Exception e) {
			
			return false;
		}
	}

	@Override
	public LangUserMailer read() throws Exception, UnexpectedInputException, ParseException,
			NonTransientResourceException {
		
		//poison file code to be added.
		
		if ((index==0 )||(lstCommentUser== null)||(lstCommentUser.size()< index)){
			lstCommentUser = getCommentDao().getUserComments();
		}
		
		if (lstCommentUser!=null && lstCommentUser.size()> index){
			if (customCommentView== null || customCommentView.size()==0 ){
				if (initializePersonalizeView()== false)
						return null;
			}
			index ++;
			return lstCommentUser.get(index-1);
			
		}else{
			return null;		
		}
		
	}


	public String getPoisonFile() {
		return poisonFile;
	}


	public void setPoisonFile(String poisonFile) {
		this.poisonFile = poisonFile;
	}

}
