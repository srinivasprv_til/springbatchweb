/**
 * 
 */
package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.joblauncher.LiveBlogFileUploadLauncher;
import com.times.common.util.Constant;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.AmazonS3Credits;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NetstorageInfo;
import com.times.mailer.model.FileLocationInfo.LocationType;

/**
 * This batch reader is used for LiveBlog file upload.
 * 
 * @author Ranjeet.Jha
 *
 */
public class LiveBlogFileUploadReader  implements ItemReader<FileLocationInfo>, StepExecutionListener , JobExecutionListener{ //, JobExecutionListener

	private final static Logger logger = LoggerFactory.getLogger(LiveBlogFileUploadReader.class);
	
	//private EmailProcessor smtpMailer;
	private NetstorageFileInfoDao netstorageFileInfoDao;
	
	//private Boolean isExisting = false;
	private int perPage = 100;
	private int pageNo = 0;
	private long startTime = System.currentTimeMillis();
	
	public List<FileLocationInfo> fileLocationInfoList = Collections.synchronizedList(new ArrayList<FileLocationInfo>());
	
	// hostid as key and value as msid arrays, set by launcher if any then run the batch else no need to run batch job.
	public static Map<Integer, List<Integer>> hostMsidMap = Collections.synchronizedMap(new HashMap<Integer, List<Integer>>()); 
	
	//code changes ... 
	boolean isInitialized = false;
	
	/**
	 * @param netstorageFileInfoDao the netstorageFileInfoDao to set
	 */
	public void setNetstorageFileInfoDao(NetstorageFileInfoDao netstorageFileInfoDao) {
		this.netstorageFileInfoDao = netstorageFileInfoDao;
	}
	
	/**
	 * This method is used to read the {@link FileLocationInfo} object.
	 * 
	 * @param args
	 */
	public FileLocationInfo read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		//logger.debug("TOIListLevelReader.read method start..");
		
		// if subscriber got then 
		if (fileLocationInfoList.size() > 0) {
			//counter update in memory
			FileLocationInfo fileUploadDtls = getFileLocationInfo();			
			return fileUploadDtls;
		} 
		
		return null;
	}

	/**
	 * Get the {@link FileLocationInfo} domain model object , if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private FileLocationInfo getFileLocationInfo() {
		FileLocationInfo fileLocationInfo = null;
		synchronized (this) {
			if (fileLocationInfoList != null && fileLocationInfoList.size() > 0) {
				fileLocationInfo = fileLocationInfoList.remove(0);
			}
		}
		return fileLocationInfo;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		startTime = System.currentTimeMillis();
		//System.out.println("++++++++++++++++++++++++++++ beforeStep ++++++++++++++++++++++++++++++++++");
		//logger.debug(" beforeStep method..++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		JobParameters jobParams = stepExecution.getJobParameters();
		int workType = Integer.parseInt(jobParams.getString(Constant.JOB_PARAM_JOBTYPE_ID));
		
		// page no is applicable job is going to restart so that it will run from next page where batch running.
		if (jobParams.getString(Constant.JOB_PARAM_NL_RESTART_KEY) != null) {
			int pageNo = (int)jobParams.getLong(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO);
			this.pageNo = ++pageNo;
		} 
		
		this.isInitialized = true;
		
		
		
		// add LiveBlogFileInfo in memory
		addLiveBlogFileInfoIntoList(workType);
		
		addLiveBlogFileInfoWithoutMsidIntoList(workType);
		
		/*for (FileLocationInfo f : fileLocationInfoList) {
			System.out.println(f.getSourceUrl() + " : "  + f.getNetstorageLocation().getFileName() );
		}*/
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		//System.out.println(" ++++++++++++++++++++++ afterStep +++++++++++++++++++++++");
		if (LiveBlogFileUploadLauncher.jobExecution != null ) {
			logger.debug("status : " + LiveBlogFileUploadLauncher.jobExecution.getStatus());
		}
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}
		//logger.debug("afterStep method..");
		/*JobParameters jobParams = stepExecution.getJobParameters(); 
		if (jobParams != null) {
			for (Map.Entry<String, JobParameter> entry : jobParams.getParameters().entrySet()) {
				logger.debug(entry.getKey() + " : " + entry.getValue().getValue());
			}
		}*/
		
		//this.removeInMemoryData();
		
		//logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		
		return stepExecution.getExitStatus();
	}

	/**
	 * @param workType
	 */
	private void addLiveBlogFileInfoIntoList(int workType) {
		try {
			if (workType == Constant.WORK_TYPE_LIVE_BLOG && hostMsidMap != null && hostMsidMap.size() > 0) {
				for (Map.Entry<Integer, List<Integer>> entry : hostMsidMap.entrySet()) {
					int hostid = entry.getKey();
					if (hostid != 0) {
						List<FileLocationInfo> fileInfoList = netstorageFileInfoDao.getFileLocationInfo(hostid, workType, pageNo, perPage);
						if (fileInfoList != null) {
						List<FileLocationInfo> list = this.getFileLocationInfoByMSIds(entry.getValue(), fileInfoList);
						this.fileLocationInfoList.addAll(list);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * @param workType
	 */
	private void addLiveBlogFileInfoWithoutMsidIntoList(int workType) {
		try {
			if (workType == Constant.WORK_TYPE_LIVE_BLOG) {
				List<FileLocationInfo> fileInfoList = netstorageFileInfoDao.getFileLocationInfoByWorkType(workType);
				List<FileLocationInfo> list = new ArrayList<FileLocationInfo>();
				if (fileInfoList != null) {
					for (FileLocationInfo file : fileInfoList) {
						if (!file.getSourceUrl().contains("#msid")) {
							synchronized (this) {
								this.fileLocationInfoList.add(file);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		

	/**
	 * This method is used to build the FileLocationInfo list if multiple live blog active for the one host
	 * 
	 * @param msidList
	 * @param fileInfoList
	 * @return
	 */
	private List<FileLocationInfo> getFileLocationInfoByMSIds(List<Integer> msidList, List<FileLocationInfo> fileInfoList) {
		try {
			if (msidList != null && msidList.size() > 0 && fileInfoList != null && fileInfoList.size() > 0) {
				List<FileLocationInfo> fileInfoByMsid = new ArrayList<FileLocationInfo>(fileInfoList.size() * msidList.size());
				
				for (Integer msid : msidList) {
					
					for (FileLocationInfo fInfo : fileInfoList) {
						
						FileLocationInfo info = new FileLocationInfo();
						info.setId(fInfo.getId());
						info.setSourceUrl(fInfo.getSourceUrl().replace("#msid", String.valueOf(msid)));
						info.setStatus(fInfo.isStatus());
						info.setWorkType(fInfo.getWorkType());
						info.setLocationType(fInfo.getLocationType());
						
						if (fInfo.getLocationType() == LocationType.NET_STORAGE) {
							NetstorageInfo ftpInfo = new NetstorageInfo();
							ftpInfo.setFileName( fInfo.getNetstorageLocation().getFileName().replace("#msid", String.valueOf(msid)));
							ftpInfo.setId(fInfo.getNetstorageLocation().getId());
							ftpInfo.setFolderLocation(fInfo.getNetstorageLocation().getFolderLocation());
							ftpInfo.setPassword(fInfo.getNetstorageLocation().getPassword());
							ftpInfo.setServerName(fInfo.getNetstorageLocation().getServerName());
							ftpInfo.setUserid(fInfo.getNetstorageLocation().getUserid());
							
							info.setNetstorageLocation(ftpInfo);
							
						} else if (fInfo.getLocationType() == LocationType.AWS) {

							info.setAmazonS3(new AmazonS3Credits(fInfo.getAmazonS3().getAccessKey(), fInfo.getAmazonS3().getSecretKey(), fInfo.getAmazonS3().getBucketName(), fInfo.getAmazonS3().getFileName().replace("#msid", String.valueOf(msid)), null));
						}
						
						fileInfoByMsid.add(info);
					}
				}
				
				return fileInfoByMsid;
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileInfoList;
	}

	@Override
	public void beforeJob(JobExecution jobExecution) {
		System.out.println("-----------------------------------------------Befor job");
		
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		System.out.println("After finishing of job =======================================================================================");
		
	}
}