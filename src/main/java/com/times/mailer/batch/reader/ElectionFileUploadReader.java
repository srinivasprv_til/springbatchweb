package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.util.Constant;
import com.times.mailer.model.AmazonS3Credits;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NetstorageInfo;

/**
 * This class is used to upload the Election related fileUpload.
 * 
 * @author Ranjeet.Jha
 *
 */
public class ElectionFileUploadReader implements ItemReader<FileLocationInfo>, StepExecutionListener { 

	private final static Logger logger = LoggerFactory.getLogger(ElectionFileUploadReader.class);
	
	//private EmailProcessor smtpMailer;
	//private NetstorageFileInfoDao netstorageFileInfoDao;
	
	private static final int workType = Constant.WORK_TYPE_CVOTER_JSON;  
	
	public List<FileLocationInfo> fileLocationInfoList = Collections.synchronizedList(new ArrayList<FileLocationInfo>());
	
	//code changes ... 
	boolean isInitialized = false;
	private long startTime = System.currentTimeMillis();
	
	/**
	 * This method is used to read the {@link FileLocationInfo} object.
	 * 
	 * @param args
	 */
	public FileLocationInfo read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		// if subscriber got then 
		if (fileLocationInfoList.size() > 0) {
			//counter update in memory
			FileLocationInfo fileUploadDtls = getFileLocationInfo();
			return fileUploadDtls;
		} 
		
		return null;
	}

	/**
	 * Get the Newsletter user/subscriber, if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private FileLocationInfo getFileLocationInfo() {
		FileLocationInfo fileLocationInfo = null;
		synchronized (this) {
			if (fileLocationInfoList != null && fileLocationInfoList.size() > 0) {
				fileLocationInfo = fileLocationInfoList.remove(0);
			}
		}
		return fileLocationInfo;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");
		startTime = System.currentTimeMillis();
		JobParameters jobParams = stepExecution.getJobParameters();
		//int workType = Integer.parseInt(jobParams.getString(Constant.JOB_PARAM_JOBTYPE_ID));
		
		this.isInitialized = true;
		// populate in list
		populateFileLocationInfo();
		 
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		
		this.isInitialized = false;
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}
		
		return stepExecution.getExitStatus();
	}

	/**
	 * @param netstorageFileInfoDao the netstorageFileInfoDao to set
	 */
	/*public void setNetstorageFileInfoDao(NetstorageFileInfoDao netstorageFileInfoDao) {
		this.netstorageFileInfoDao = netstorageFileInfoDao;
	}*/
	
	/**
	 * This method is used to add source url and details in list i.e. would be in memory.
	 * 
	 * @return
	 */
	public void populateFileLocationInfo() {
		
		FileLocationInfo fileLocationInfo = new FileLocationInfo();
		String fileName = "allstates2014.htm";
		fileLocationInfo.setNetstorageLocation(getNetStorageInfo(fileName));
		//fileLocationInfo.setCallbackName("getVipData");
		//fileLocationInfo.setStateId("all");
		fileLocationInfo.setSourceUrl("http://cmsall.indiatimes.com/epollingnew/readxml.aspx?lid=1&stateid=3,4,20,23&typeid=2&dt=2014-04-1&rt=1");
		fileLocationInfo.setStatus(true);
		fileLocationInfo.setAmazonS3(new AmazonS3Credits(fileName));
		fileLocationInfoList.add(fileLocationInfo);
		
		FileLocationInfo fileLocationInfo2 = new FileLocationInfo();
		String fileName1 = "allindiaparties2014.htm";
		fileLocationInfo2.setNetstorageLocation(getNetStorageInfo(fileName1));
		//fileLocationInfo.setCallbackName("getVipData");
		//fileLocationInfo.setStateId("all");
		fileLocationInfo2.setSourceUrl("http://cmsall.indiatimes.com/epollingnew/readxml.aspx?lid=1&stateid=37&typeid=6&dt=2014-05-1&rt=1");
		fileLocationInfo2.setStatus(true);
		fileLocationInfo2.setAmazonS3(new AmazonS3Credits(fileName1));
		
		fileLocationInfoList.add(fileLocationInfo2);
		
	}
	
	private NetstorageInfo getNetStorageInfo(String fileName) {
		NetstorageInfo netstorageInfo = new NetstorageInfo();
		netstorageInfo.setFileName(fileName);
		netstorageInfo.setServerName("timescricket.upload.akamai.com");
		netstorageInfo.setFolderLocation("/92522/breakingnews");
		netstorageInfo.setUserid("toiEditTeam");
		netstorageInfo.setPassword("bre@k1ng");
		
		return netstorageInfo;
	}
	
}