package com.times.mailer.batch.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.InMemoryData;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * 
 * @author praveen.bokka
 *
 */
public class ETEpaperReader implements ItemReader<Document>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(ETEpaperReader.class);
	
	private String serverURL;// = "223.165.28.213";
    private int port;// = 21;
    public String getServerURL() {
		return serverURL;
	}


	public void setServerURL(String serverURL) {
		this.serverURL = serverURL;
	}


	public int getPort() {
		return port;
	}


	public void setPort(int port) {
		this.port = port;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public String getPass() {
		return pass;
	}


	public void setPass(String pass) {
		this.pass = pass;
	}

	private String user;// = "cmsfeed";
    private String pass;// = "ftpuser@123";
    Date lastProcessedDate = null;
    
    private List<DBObject> statusObjects = null;
    private int index = 0;
	
	public Document read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		logger.debug("etEpaper read method start..");
		
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		if(index != 0) {
			if(InMemoryData.etEpaperSuccess) {
				statusObjects.get(index-1).put("lastDate", f.format(lastProcessedDate));
			}
		}
		
		InMemoryData.etEpaperSuccess = false;
		
		int numEdition = statusObjects.size();
		if(numEdition == index) {
			logger.info("All editions processed. Exiting...");
			return null;
		}
		
		FTPClient ftpClient = new FTPClient();
		
		Document document = new Document();
		
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
        SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        
        Date today = new Date();//dateFormat.parse("20160525");//
        
        String year = yearFormat.format(today);
        String month = monthFormat.format(today);
        String date = dateFormat.format(today);
        
        DBObject currentEdition = statusObjects.get(index);
        
        index++;
        
        
        //last processed date for edition
        String dateStr	=	(String) currentEdition.get("lastDate");
        
		try {
			lastProcessedDate = f.parse(dateStr);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		//String of the edition
        String editionName = (String) currentEdition.get("edition");
        if(editionName == null) {
        	logger.error("Skipping as format of status Object is not correct."+ currentEdition.get("_id").toString());
        	return document;
        }
        String ftpPathStr = (String) currentEdition.get("ftpPath");
        
        logger.info("Processing edition: "+ editionName);
		
        //run job only till today
        if(lastProcessedDate != null && date.equals(dateFormat.format(lastProcessedDate))) {
        	logger.info("Already processed for today...."+editionName);
        	return document;
        }
        
        //TODO: Generalize for all editions(Following is for Delhi edition)
        String remoteFile = "/"+ftpPathStr+"/" + year + "/" + month + "/TOC_" +ftpPathStr + date + ".xml";
		
		try {
			ftpClient.connect(serverURL, port);
			ftpClient.login(user, pass);
			ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            
            //String remoteFile = "/ETDC/2016/02/TOC_ETDC20160218.xml";
            InputStream inputStream = ftpClient.retrieveFileStream(remoteFile);
            
            SAXBuilder saxBuilder = new SAXBuilder();
            document = saxBuilder.build(inputStream);
            inputStream.close();
            lastProcessedDate = today;
			
            //String xml = new XMLOutputter(Format.getPrettyFormat()).outputString(document);
            //logger.debug(xml);
            //System.out.println(xml);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			logger.error("Socket exception while retrieving file. Will try after 1 hour....");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("File may not be existing in given location. Will try after 1 hour....");
			e.printStackTrace();
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			logger.error("Unable to parse file as XML");
			e.printStackTrace();
		} finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
		
		
		return document;
	}


	/*public void beforeStep(StepExecution stepExecution) {		
		logger.debug(" beforeStep method..");
		
		DBObject dbQuery = new BasicDBObject();
		dbQuery.put("type", "epaperJobStatus");
		DBObject result = MongoRestAPIService.get("ETEpaperStatus", dbQuery);
		if(result == null) {
			lastProcessedDate = null;
			return;
		}
		String dateStr = (String) result.get("lastDate");
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			lastProcessedDate = f.parse(dateStr);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//(Date) result.get("lastDate");
		//InMemoryData.etEpaperMsid = (int) result.get("lastMsid");
		
		logger.debug(" before step method exiting");
	}*/
	
	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");
		
		DBObject dbQuery = new BasicDBObject();
		dbQuery.put("type", "epaperJobStatus");
		
		statusObjects = MongoRestAPIService.getList("ETEpaperStatus", dbQuery);
		
		
		logger.debug(" before step method exiting");
	}

//	public ExitStatus afterStep(StepExecution stepExecution) {
//		logger.debug("afterStep method..");
//		
//		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
//		f.setTimeZone(TimeZone.getTimeZone("UTC"));
//		DBObject dbQuery = new BasicDBObject();
//		dbQuery.put("type", "epaperJobStatus");
//		DBObject insertDoc = new BasicDBObject();
//		insertDoc.put("type", "epaperJobStatus");
//		insertDoc.put("lastDate", f.format(lastProcessedDate));
//		//insertDoc.put("lastMsid", InMemoryData.etEpaperMsid);
//		MongoRestAPIService.update("ETEpaperStatus",dbQuery, insertDoc,true);
//		
//		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
//		return stepExecution.getExitStatus();
//	}
	
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		for(DBObject statusObject : statusObjects) {
			//checking for invalid objects which are before multiple editions are added
			if(statusObject.get("edition") != null) {
				DBObject dbQuery = new BasicDBObject();
				dbQuery.put("type", "epaperJobStatus");
				dbQuery.put("edition", statusObject.get("edition"));
				statusObject.removeField("_id");
				MongoRestAPIService.update("ETEpaperStatus",dbQuery, statusObject,true);
			}
		}
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		return stepExecution.getExitStatus();
	}

}
