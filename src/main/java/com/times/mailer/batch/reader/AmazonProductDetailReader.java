package com.times.mailer.batch.reader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.constant.AmazonItemFields;
import com.times.common.constant.AmazonRequestFields;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
import com.times.common.model.AmazonItem;
import com.times.common.model.AmazonSearchResponse;
import com.times.common.service.AmazonItemLookupService;
import com.times.common.service.AmazonProductsAPIService;
import com.times.common.service.SimilarGadgetMatcherService;
import com.times.common.util.GadgetUtil;
import com.times.mailer.constant.GadgetConstant;

public class AmazonProductDetailReader implements ItemReader < List<AmazonItem> >, StepExecutionListener{

	private static final Logger logger = LoggerFactory.getLogger(AmazonProductDetailReader.class);
	//private static Logger logger=null;
	
	//Set of categories on which reading has to be done from Amazon API
	private Set <String> SELECTED_CATEGORIES_SET= new HashSet <String> ();
	
	private static List <String> ALL_CATEGORIES_LIST=new ArrayList <String>();

	//Current category index to identify the category which is being read
	private int currentcategoryIndex=-1;

	//Equals Number of categories (11 as of now)
	private static int MAX_CATEGORY_INDEX;
	
	private static final int SIMILAR_PRODUCTS_PER_BATCH=100;

	private Set<String> currentCategoryAsinSet= new HashSet<String>();

	private AmazonProductsAPIService amazonItemSearchService;
	
	private AmazonItemLookupService amazonItemLookupService;
	
	private EmailProcessor smtpMailer;
	
	private SimilarGadgetMatcherService similarGadgetSearchService;

	private static Map<String,Integer> MAX_PRICE_MAP= new HashMap<String,Integer>();
	
	private static final int  DEFAULT_MAX_PRICE=100000;
	private static final int DEFAULT_MIN_PRICE=600;
	private static final int DEFAULT_GAP=5000;
	
	private static final int JOB_START_HOUR=LocalDateTime.now().getHour();;
	
	DBObject jobReport= new BasicDBObject();


	/*
	 * AMAZON PRODUCT READER
	 * 
	 * COMMIT INTERVAL=1, Reader reads data for a category in each step and passes it to the writer (catgeory by category)
	 * 
	 * Reading Process:
	 * -->For each Category
	 * 	-->Get all brands that we have in our collection from mongoDB
	 * 	-->For each brand
	 * 		-->Get products from amazon
	 * 			--> If count <=10 
	 * 					save as it is
	 * 			-->If 10<count<=100
	 *					get all pages by changing item page
	 *			-->If 100<count<=200
	 *					get 100 brands by sorting up	
	 *					get 100 brands by sorting down
	 *			-->If count>200
	 *					break into price windows 
	 * 
	 */


	public List<AmazonItem>  read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		refreshSystemVariable();
		
		if (currentcategoryIndex<MAX_CATEGORY_INDEX) {

			String category=ALL_CATEGORIES_LIST.get(currentcategoryIndex);
			//String category="washingmachine"; --> For Debugging
			logger.info("Process Started for Category "+category);
			DBObject categoryReport=new BasicDBObject();
			
			//get All Items from Amazon, If category is in selected category list, then products are fetched from amazon else empty list is returned
			List <AmazonItem>amazonItemList=getAllProductsForCategoryFromAmazon(category);

			if(amazonItemList!=null){
				
				logger.info("Number of items for category before removing duplicates from AmazonAPI"+category+" = "+amazonItemList.size());
				categoryReport.put("From AmazonAPI", amazonItemList.size());
				
				removeDuplicateASINS(amazonItemList);
				
				logger.info("Number of items for category after removing duplicates from AmazonAPI "+category+" = "+amazonItemList.size());
				categoryReport.put("From AmazonAPI After Deduplication", amazonItemList.size());

				
				//Adding ASINS present in gadget_product_detail but not in this collection
				Set<String> ASINSFromMainColl = getExistingASINSetForCategory(category);
				logger.info("Number of ASINS for category from gadget_detail "+category+" = "+ASINSFromMainColl.size());
				categoryReport.put("ASINS IN GADGET DETAIL", ASINSFromMainColl.size());

				
				Set<String> unupdatedASINS=getUnupdatedASINS(currentCategoryAsinSet, ASINSFromMainColl);
				logger.info("Number of ASINS left for updation "+category+" = "+unupdatedASINS.size());
				categoryReport.put("ASINS UNUPDATED", unupdatedASINS.size());

								
				if(unupdatedASINS!=null){
					List<AmazonItem> amazonItems= amazonItemLookupService.getProductsOnASINList(unupdatedASINS.stream().collect(Collectors.toList()));
					if(amazonItemList!=null){
						amazonItemList.addAll(amazonItems);
					}
				}
				
				logger.info("Number of items sent to writter altogether "+category+" = ||  "+amazonItemList.size());
				categoryReport.put("Total products updated", amazonItemList.size());
				jobReport.put(category, categoryReport);

				
				return  amazonItemList;
			}else{
				return new ArrayList<AmazonItem>();
			}

		} else {
			return null;
		}

	}



	/**
	 * Refresh system variable.
	 * increase category index by 1, and clear category ASIN Set
	 * 
	 */
	private void refreshSystemVariable(){
		++currentcategoryIndex;
		currentCategoryAsinSet=new HashSet<String>();

	}


	/**
	 * Things done in before Step
	 * -->Reset System Variables
	 * -->initialize maximum Price map
	 * 		--> For maximum price, sort by price desc and take max price on first page(max on all 10 items considering lowestNew and list price)
	 **/
	public void beforeStep(StepExecution stepExecution) {
		
		currentcategoryIndex=-1;
		
		//Initialze Selected Category Set
		SELECTED_CATEGORIES_SET=Arrays.stream(GadgetConstant.Category.SELECTED_CATEGORIES_FOR_AMAZON_JOB).
				collect(Collectors.toCollection(HashSet::new));
		
		initializeMaximumPriceMapForSelected();
		initializeAllCategoryList();
		
		
		
		MAX_CATEGORY_INDEX=ALL_CATEGORIES_LIST.size();
		
		logger.info("\n\n\n**********\n\n\n");
		logger.info("Job Reader Started At "+new Date());


	}



	/**
	 * @param category
	 * @return List of Amazon Items in that category from amazonAPI if category is in selected list for which data has to be fetched from amazon
	 * 		   else return empty list
	 */
	private List<AmazonItem> getAllProductsForCategoryFromAmazon(final String category){
		
		List<AmazonItem> amazonItemsForCategory= new ArrayList<AmazonItem>();
		if(!SELECTED_CATEGORIES_SET.contains(category)){
			logger.info("Category is not in the selected categories");
			return amazonItemsForCategory;
		}
		
		Set<String> brandsList= getBrandsSetForCatgeory(category);


		int maxPrice=DEFAULT_MAX_PRICE;

		if (MAX_PRICE_MAP.get(category)!=null && MAX_PRICE_MAP.get(category)!=0){
			maxPrice=MAX_PRICE_MAP.get(category);
		}

		if(brandsList!=null){

			if(GadgetConstant.Category.MOBILES.equals(category)){
				brandsList.add("Mi");
				brandsList.add("Motorola");
			}

			logger.info("Total Brands for Category "+category+ " COUNT="+brandsList.size());
			for(String brand: brandsList){	

				List <AmazonItem> amazonItems= getAmazonProductsForBrand(brand, category, DEFAULT_MIN_PRICE, maxPrice, DEFAULT_GAP);
				amazonItemsForCategory.addAll(amazonItems);
				logger.info(" Actual Number of items for category,brand= ##("+category+","+ brand + ")## = @@  "+amazonItems.size());
			}
		}

		return amazonItemsForCategory;

	}



	private List<AmazonItem> getAmazonProductsForBrand(String brandName, String category,final int LOWEST, final int HIGHEST,final int GAP){
		List<AmazonItem> amazonItems= new ArrayList<AmazonItem>();

		Map<String,String> params= new HashMap<String, String> ();
		params.put(AmazonRequestFields.MANUFACTURER, brandName);
		params.put(AmazonRequestFields.BROWSE_NODE, GadgetConstant.Category.CATEGORY_TO_BROWSE_NODE_MAP.get(category));
		params.put(AmazonRequestFields.SORT, AmazonRequestFields.SORT_PRICE_ASC);

		AmazonSearchResponse amazonSearchResponse = amazonItemSearchService.getProductsFromAmazon(params);
		int numItems=0;

		if(amazonSearchResponse!=null){
			numItems=amazonSearchResponse.getTotalResults();
			logger.info("Expected number of Items for category,brand : **("+category+","+brandName+")** =@@  "+numItems);
		}

		if(numItems==0){
			return amazonItems;
		}

		if(numItems<=10){
			return amazonSearchResponse.getItemList();
		}

		if(numItems<=200){
			return getProductsForAmazonQueryUpto200(params);

		}else{

			int startPrice=LOWEST;


			while(startPrice<HIGHEST){

				params.put(AmazonRequestFields.MINIMUM_PRICE, String.valueOf(startPrice*100));
				params.put(AmazonRequestFields.MAXIMUM_PRICE, String.valueOf((startPrice+GAP)*100));

				AmazonSearchResponse amazonSearchResponsePage= amazonItemSearchService.getProductsFromAmazon(params);

				if(amazonSearchResponsePage!=null){

					int numResults=amazonSearchResponsePage.getTotalResults();

					if(numResults==0){
						startPrice=startPrice+GAP;
						continue;
					}else if(numResults<=10){
						amazonItems.addAll(amazonSearchResponsePage.getItemList());
					}else if(numResults<=200){
						amazonItems.addAll(getProductsForAmazonQueryUpto200(params));
					}else{

						//First half
						params.put(AmazonRequestFields.MINIMUM_PRICE, String.valueOf((startPrice)*100));
						params.put(AmazonRequestFields.MAXIMUM_PRICE, String.valueOf((startPrice+GAP/2)*100));

						amazonItems.addAll(getProductsForAmazonQueryUpto200(params));


						//Second Half
						params.put(AmazonRequestFields.MINIMUM_PRICE, String.valueOf((startPrice+GAP/2)*100));
						params.put(AmazonRequestFields.MAXIMUM_PRICE, String.valueOf((startPrice+GAP)*100));

						amazonItems.addAll(getProductsForAmazonQueryUpto200(params));


					}

				}

				startPrice=startPrice+GAP;
			}	

			return amazonItems;
		}
	}



	/**
	 * Read description in read() documentation
	 * @param params
	 * @return upto 200 results for a query
	 */
	private List <AmazonItem> getProductsForAmazonQueryUpto200(Map<String,String> params){

		List<AmazonItem> amazonItems= new ArrayList<AmazonItem>();

		AmazonSearchResponse amazonSearchResponse = amazonItemSearchService.getProductsFromAmazon(params);

		int totalResults=0;
		if(amazonSearchResponse!=null){
			totalResults= amazonSearchResponse.getTotalResults(); 

			if(totalResults>0){
				amazonItems.addAll(amazonSearchResponse.getItemList());
			}

		}


		if(totalResults<=10){
			return amazonItems;
		}


		if(totalResults<=100){

			int numPages=amazonSearchResponse.getTotalPages();


			List <AmazonItem> amazonItemsAllPages=getAllPagesDataForQuery(params,2,numPages);

			amazonItems.addAll(amazonItemsAllPages);


		}else if(totalResults<=200){

			List <AmazonItem> amazonItemsAllPages=getAllPagesDataForQuery(params,2,10);
			amazonItems.addAll(amazonItemsAllPages);

			//Reverse sort Order
			params.put(AmazonRequestFields.SORT, AmazonRequestFields.SORT_PRICE_DESC);

			//Calculate Items left 
			//+10 as a sentinel value to avoid  missing any product because of imperfections in sort by price
			int itemsLeft= totalResults-100+10; 

			if(itemsLeft>100){
				itemsLeft=100;
			}

			int numPagesLeft=(int) Math.ceil(itemsLeft/10.0);

			List <AmazonItem> amazonItemsAllPagesReverse=getAllPagesDataForQuery(params,1,numPagesLeft);
			amazonItems.addAll(amazonItemsAllPagesReverse);
		}

		return amazonItems;

	}


	/**
	 * Gets data for all pages of corresponding to the params provided, lastPage<=10  {AmazonAPI constraint}
	 * @param params
	 * @param startPage
	 * @param lastPage
	 * @return
	 */
	private List<AmazonItem> getAllPagesDataForQuery(Map<String,String> params,int startPage,int lastPage){
		
		List<AmazonItem> amazonItems = new ArrayList<AmazonItem>();

		for(int pageNum=startPage;pageNum<=lastPage;++pageNum){

			params.put(AmazonRequestFields.ITEM_PAGE,String.valueOf(pageNum));

			AmazonSearchResponse amazonSearchResponsePage= amazonItemSearchService.getProductsFromAmazon(params);

			if(amazonSearchResponsePage!=null){
				amazonItems.addAll(amazonSearchResponsePage.getItemList());
			}

		}	

		return amazonItems;
	}


	/**
	 * Removes duplicates from AmazonItems List
	 * Amazon API sort by Price is not perfect and products repeat in some cases, specially in case where we get 200 products, by sorting up
	 * once and down next
	 * @param amazonItemList
	 */
	private void removeDuplicateASINS(List<AmazonItem> amazonItemList) {

		for (Iterator<AmazonItem> iterator = amazonItemList.iterator(); iterator.hasNext();) {
			String asin = iterator.next().getASIN();
			if (currentCategoryAsinSet.contains(asin)) {
				iterator.remove();
			}else{
				currentCategoryAsinSet.add(asin);
			}
		}
	}

	
	private void initializeMaximumPriceMapForSelected(){
		Map<String, String> params = new HashMap<String, String>();
		params.put(AmazonRequestFields.SORT,AmazonRequestFields.SORT_PRICE_DESC );
		
		for (String category : SELECTED_CATEGORIES_SET){
			params.put(AmazonRequestFields.BROWSE_NODE, GadgetConstant.Category.CATEGORY_TO_BROWSE_NODE_MAP.get(category));
			AmazonSearchResponse amazonSearchResponse= amazonItemSearchService.getProductsFromAmazon(params);
			int maxPrice=0;
			if(amazonSearchResponse!=null){
				List<AmazonItem > amazonItems = amazonSearchResponse.getItemList();
				for(AmazonItem amazonItem: amazonItems){
					int listPrice=0, lowestNewPrice=0;
					
					try{
						listPrice=(int)amazonItem.getListPriceDBObject().get(AmazonItemFields.AMOUNT);

					}catch(Exception e){
						//No need to catch
					}

					try{
						lowestNewPrice=(int)amazonItem.getLowestNewPriceDBObject().get(AmazonItemFields.AMOUNT);
					}catch(Exception e){
						//No need to catch
					}

					int localMax=Math.max(listPrice, lowestNewPrice);
					if(localMax>maxPrice){
						maxPrice=localMax;
					}
				}

				if(maxPrice!=0){
					MAX_PRICE_MAP.put(category, maxPrice/100);
				}else{
					MAX_PRICE_MAP.put(category, DEFAULT_MAX_PRICE);
				}

			}else{
				MAX_PRICE_MAP.put(category, DEFAULT_MAX_PRICE);
			}

		}
	}

	
	private Set<String> getExistingASINSetForCategory(String category){
		
		DBObject query= new BasicDBObject().append(GadgetConstant.CATEGORY, category);
		//FOR AMAZON_RELATED
		
		Set<String> relatedASINSet= MongoRestAPIService.getDistinct(GadgetConstant.PRODUCT_DETAIL, 
																	AmazonItemFields.AMAZON_SIMILAR_FROM_SEARCH+"."+AmazonItemFields.DB_ASIN,
																	query);
		
		
		Set<String> exactASINSet=  MongoRestAPIService.getDistinct(GadgetConstant.PRODUCT_DETAIL, 
									AmazonItemFields.AMAZON_EXACT_FROM_DUMP+"."+AmazonItemFields.DB_ASIN,
									query);
		
		if(relatedASINSet==null && exactASINSet==null) return null;
		
		if(relatedASINSet==null){
			return relatedASINSet;
		}else if(exactASINSet==null){
			return exactASINSet;
		}else{
			relatedASINSet.addAll(exactASINSet);
			return relatedASINSet;
		}
		
		
	}
	
	
	
	private Set <String> getUnupdatedASINS(Set<String> updatedByBatch, Set<String> presentInCollection){
		if(updatedByBatch==null){
			return presentInCollection;
		}
		
		if(presentInCollection==null){
			return presentInCollection;
		}
		
		presentInCollection.removeAll(updatedByBatch);
		
		return presentInCollection;
	}
	
	private Set<String> getBrandsSetForCatgeory(String category){
		DBObject query= new BasicDBObject();
		
		if(StringUtils.isNotBlank(category)){
			query.put(GadgetConstant.CATEGORY, category);
		}
		
		String fieldName="brand_name";
		
		Set<String> brandSet = MongoRestAPIService.getDistinct(GadgetConstant.PRODUCT_DETAIL,fieldName,query);
		
		return brandSet;
	}

	private void initializeAllCategoryList() {
		
		Set<String> catgeorySet= GadgetConstant.Category.CATEGORY_TO_BROWSE_NODE_MAP.keySet();
		
		if(catgeorySet!=null){
			ALL_CATEGORIES_LIST=catgeorySet.stream().collect(Collectors.toList());
		}
		
		
	}
	
	private EmailVO generateMailObject() {
		EmailVO vo = new EmailVO();
		vo.setSender("gadgetsnow@timesinternet.in");
		vo.setRecipients(
				"prateek.chouhan@timesinternet.in,"+
				  "daksh.verma@timesinternet.in");
		

		vo.setSubject("Amazon Product Detail Job Report");
		if(jobReport!=null){
			Gson gObj = new GsonBuilder().setPrettyPrinting().create();
			String body=gObj.toJson(jobReport);
			vo.setBody(body);
		}else{
			return null;
		}
		
		return vo;
	}


	public ExitStatus afterStep(StepExecution stepExecution) {
		
		try {
			updateSimilarAndExactGadgets(SIMILAR_PRODUCTS_PER_BATCH, getSkipValue(JOB_START_HOUR));
		} catch (Exception e) {
			logger.error("Update of similar gadgets failed",e);
		}

		//logger.debug("After Step method..");
		EmailVO vo = generateMailObject();
		try {
			if(vo.getBody() != null && !"".equals(vo.getBody()))
				smtpMailer.sendEMail(vo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Failed to send email");
		}
		
		return stepExecution.getExitStatus();
	}
	
	
	/**
	 * Updates last {@code limit} gadgets in the {@link GadgetConstant#PRODUCT_DETAIL} and skips {@code skip} gadgets sorted in the order of {@link GadgetConstant#INSERTED_AT}<br>
	 * Updates similar and exact mapping of 91Mobiles gadget<br>
	 * Skip is based on hourNumber, Since job repeats after 2 hours (odd hours) (n-1)*100/2<br>
	 * For hour=1, skip=0 ; hour=3, skip=100 ; hour=5, skip=200 ; hour=7, skip=300
	 * @param limit
	 * @param skip
	 */
	private void updateSimilarAndExactGadgets(int limit, int skip){
		
		DBObject similarReport= new BasicDBObject();
		similarReport.put("limit", limit);
		similarReport.put("skip", skip);
		int sucessCount=0;
		
		logger.info("Going to update similar gadgets for limit={} and skip={}",limit,skip);
		
		DBObject query= new BasicDBObject();
		
		Map<String, Object> optionMap = new HashMap<String, Object>();
		
		BasicDBObject sortObject = new BasicDBObject();
		sortObject.put(GadgetConstant.INSERTED_AT, -1);
		DBObject fields= new BasicDBObject().append(GadgetConstant.Fields.NAME, 1)
				                            .append(GadgetConstant.Fields.BRAND_NAME, 1)
											.append(GadgetConstant.CATEGORY, 1)
											.append(GadgetConstant.Fields.LCUNAME, 1);

		optionMap.put("sort", sortObject);
		optionMap.put("limit", limit);
		optionMap.put("skip", skip);
		optionMap.put("key", fields);
		
		List<DBObject> gadgetsList=MongoRestAPIService.getList(GadgetConstant.PRODUCT_DETAIL, query, optionMap);
		
		if(gadgetsList!=null && gadgetsList.size()>0){
			
			logger.info("Received a list of {} gadgets",gadgetsList.size());
			similarReport.put("Received", gadgetsList.size());
			
			for (DBObject gadget: gadgetsList){
				boolean status=updateProduct(gadget);
				
				if (status) sucessCount+=1; 
			}
		}
		
		similarReport.put("successCount", sucessCount);
		jobReport.put("SIMILAR_REPORT", similarReport);
			
	}
	
	private boolean updateProduct(DBObject gadget){
		
		boolean updateSuccess=false;
		
		if (gadget != null && gadget.get(GadgetConstant.Fields.NAME) != null
				&& gadget.get(GadgetConstant.CATEGORY) != null && gadget.get(GadgetConstant.Fields.BRAND_NAME) != null
				&& gadget.get(GadgetConstant.Fields.LCUNAME) != null){
			
			DBObject similarProductsDetails = similarGadgetSearchService.getAccumulatedSimilarProducts(
					String.valueOf(gadget.get(GadgetConstant.Fields.NAME)),
					String.valueOf(gadget.get(GadgetConstant.Fields.BRAND_NAME)),
					String.valueOf(gadget.get(GadgetConstant.CATEGORY)));
			
			DBObject exactMatch=getExactMapping(GadgetUtil.getUName(String.valueOf(gadget.get(GadgetConstant.Fields.NAME))));
			
			DBObject completeAmazonObject= new BasicDBObject();
			
			if (similarProductsDetails != null && !similarProductsDetails.keySet().isEmpty()){	
				completeAmazonObject.putAll(similarProductsDetails);	
			}else{
				logger.warn("No similar products found for gadget ={}", String.valueOf(gadget));
			}
			
			if (exactMatch != null && !exactMatch.keySet().isEmpty()){
				
				completeAmazonObject.putAll(exactMatch);
			}else{
				logger.warn("No  exact products found for gadget ={}", String.valueOf(gadget));

			}
				
			
			if (completeAmazonObject != null && !completeAmazonObject.keySet().isEmpty()){
				
				DBObject setObject = new BasicDBObject().append("$set", completeAmazonObject);
				
				DBObject query= new BasicDBObject();
				query.put(GadgetConstant.CATEGORY,String.valueOf(gadget.get(GadgetConstant.CATEGORY)));
				query.put(GadgetConstant.Fields.LCUNAME,String.valueOf(gadget.get(GadgetConstant.Fields.LCUNAME)));
				
				
				MongoRestAPIService.updateWithPost(GadgetConstant.PRODUCT_DETAIL, query, setObject);
				logger.info("Successfully updated similar/exact gadgets for gadget={}",String.valueOf(gadget) );
				updateSuccess=true;
				
			}else{
				logger.warn("No similar or exact products found for gadget ={}", String.valueOf(gadget));
				updateSuccess=false;
			}
		}
		
		return updateSuccess;
	}
	
	
   private  DBObject getExactMapping(String uname){
	   
	   DBObject exactMatch=null;
	   if(StringUtils.isNotBlank(uname)){
			DBObject query = new BasicDBObject();
			query.put(AmazonItemFields.DB_LCUNAME,uname.toLowerCase());
			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("key", new BasicDBObject("_id",0).append(AmazonItemFields.DB_NAME, 1)
														   .append(AmazonItemFields.DB_ASIN, 1)
														   .append(AmazonItemFields.DB_DETAIL_PAGE_URL, 1)
														   .append(AmazonItemFields.DB_LOWEST_NEW_PRICE, 1)
														   .append(AmazonItemFields.DB_LIST_PRICE, 1)
														   .append(AmazonItemFields.DB_IS_PRIME, 1));
														   
			List<DBObject> amazonItems = MongoRestAPIService.getList(GadgetConstant.AMAZON_PRODUCT_DETAIL, query,optionMap);
			
			if (amazonItems != null && !amazonItems.isEmpty()){
				exactMatch=new BasicDBObject().append(AmazonItemFields.AMAZON_EXACT_FROM_DUMP, amazonItems.get(0));
			}
			
	   }
	   
	   return exactMatch;
	   
   }
	
	private int getSkipValue(int hourNumber){
		int skip=0;
		
		if (hourNumber%2==1) {
			hourNumber = (hourNumber - 1) /2;
		}else{
			hourNumber=hourNumber/2;
		}
		
		skip=hourNumber*100;
		
		return skip;
	}

	
	/**
	 * GETTERS AND SETTERS START
	 */

	public AmazonProductsAPIService getAmazonProductsAPIService() {
		return amazonItemSearchService;
	}
	public void setAmazonProductsAPIService(AmazonProductsAPIService amazonProductsAPIService) {
		this.amazonItemSearchService = amazonProductsAPIService;
	}


	public AmazonItemLookupService getAmazonItemLookupService() {
		return amazonItemLookupService;
	}


	public void setAmazonItemLookupService(AmazonItemLookupService amazonItemLookupService) {
		this.amazonItemLookupService = amazonItemLookupService;
	}



	public SimilarGadgetMatcherService getSimilarGadgetSearchService() {
		return similarGadgetSearchService;
	}



	public void setSimilarGadgetSearchService(SimilarGadgetMatcherService similarGadgetSearchService) {
		this.similarGadgetSearchService = similarGadgetSearchService;
	}



	public EmailProcessor getSmtpMailer() {
		return smtpMailer;
	}



	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}


}
