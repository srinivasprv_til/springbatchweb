package com.times.mailer.batch.reader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.ConnectException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.times.common.dao.MongoRestAPIService;

public class HTWelcomeMailerReader implements ItemReader<JSONArray>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(HTWelcomeMailerReader.class);

	public static final String DB_COLLECTION = "welcomeMailerTracker";
	public static String MONGO_REST_API_URL = "http://mra.indiatimes.com/mra/";
	private static final String getURL = "get/#collection?query=#query";
	private static final String HT_USER_URL = "http://mra.indiatimes.com/mra/get/offlineuserfeedhappytrips?#query#&sort={\"inserted_date\":1}&limit=500";

	public static String returnInsertedData_np() throws IOException {

		String fileName = "/opt/mailer/logs_np.txt";
		String line = null;
		String insertDate = null;

		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while ((line = bufferedReader.readLine()) != null) {
				insertDate = line;
			}

			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			logger.error("Unable to open file '" + fileName + "'");
		}

		return insertDate;
	}

	public static String returnInsertedData_toi() throws IOException {

		String fileName = "/opt/mailer/logs_toi.txt";
		String line = null;
		String insertDate = null;

		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while ((line = bufferedReader.readLine()) != null) {
				insertDate = line;
			}

			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			logger.error("Unable to open file '" + fileName + "'");
		}

		return insertDate;
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonArrayText = readAll(rd);
			if(jsonArrayText != null && StringUtils.isNoneBlank(jsonArrayText)){
				JSONArray json = new JSONArray(jsonArrayText);
				return json;	
			}
			else{
				JSONArray json = new JSONArray();
				return json;
			}
		} finally {
			is.close();
		}
	}

	public static boolean exist(String collection, DBObject query) {
		String extData = null;
		String strURL = null;
		strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll("#collection",
				collection);
		strURL = MONGO_REST_API_URL + strURL;
		extData = MongoRestAPIService.getExtData(strURL); 
		if(extData != null && StringUtils.isNoneBlank(extData) && extData.length() > 2) {
			return true;
		}
		return false;
	}

	@Override
	public ExitStatus afterStep(StepExecution arg0) {
		return null;
	}

	@Override
	public void beforeStep(StepExecution arg0) {

	}

	@Override
	public JSONArray read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		logger.debug("Happy trip Welcome-mailer read method start..");
		
		try {
			TimeUnit.MILLISECONDS.sleep(500);
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}

		DBObject queryObject = new BasicDBObject();
		queryObject.put("feed_type", "raw");

		String insertDate = returnInsertedData_np();
		if (insertDate != null) {
			SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

			Date insertdate = simpleFormat.parse(insertDate);
			Calendar c = Calendar.getInstance();

			c.setTime(insertdate);
			c.add(Calendar.HOUR, 5);
			c.add(Calendar.MINUTE, 30);

			BasicDBObject date = new BasicDBObject();
			date.append("$gt", c.getTime());
			queryObject.put("inserted_date", date);
		}
		else{
			Calendar c = Calendar.getInstance();
			BasicDBObject date = new BasicDBObject();
			date.append("$gt", c.getTime());
			queryObject.put("inserted_date", date);
		}

		String queryString = HT_USER_URL.replace("#query#",
				"query=" + URLEncoder.encode(queryObject.toString(), "UTF-8"));
		
//		String queryString= "http://mra.indiatimes.com/mra/get/offlineuserfeedhappytrips?query={}";
		
		logger.debug("getting data for happytrips from mongo {}",queryString);
		JSONArray listOFJson = readJsonFromUrl(queryString);
	
		
		JSONArray newListOfJson = new JSONArray();
		String fileName_ht = "/opt/mailer/logs_ht.txt";
			try {
				FileWriter fileWriter = new FileWriter(fileName_ht, false);
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
				String insertDate1 = null;
				for (int i = 0; i < listOFJson.length(); i++) {
					JSONObject jsonObject = listOFJson.getJSONObject(i);
					JSONObject newJsonObject = new JSONObject();
					JSONObject raw_data = (JSONObject) jsonObject.get("raw_data");
					if (raw_data != null && raw_data.has("email") && raw_data.has("channel")
							&& raw_data.has("user_name") && raw_data.has("guidename")) {
						newJsonObject.put("email", raw_data.get("email"));
						newJsonObject.put("channel", raw_data.get("channel"));
						newJsonObject.put("guidename", raw_data.get("guidename"));
						newJsonObject.put("user_name", raw_data.get("user_name"));
						
						JSONObject newJsonObject1 = (JSONObject) jsonObject.get("inserted_date");
						insertDate1 = newJsonObject1.getString("$date");
						

						DBObject dbObject = (DBObject) JSON.parse(newJsonObject.toString());
						DBObject dbObject_verify_exist = dbObject;
						dbObject_verify_exist.removeField("user_name");
						if (!exist(DB_COLLECTION, dbObject_verify_exist)
								&& MongoRestAPIService.insert(DB_COLLECTION, dbObject)) {
							logger.debug("Inserted into in welcomeMailerTracker collection! {}",newJsonObject.toString());
							newListOfJson.put(newJsonObject);
						}
					}

				}
				
				if(insertDate1 != null){
					bufferedWriter.write(insertDate1);
				}
				else{
					Date cDate = Calendar.getInstance().getTime();
					Calendar c = Calendar.getInstance();
					c.setTime(cDate);
					c.add(Calendar.HOUR, -5);
					c.add(Calendar.MINUTE, -30);
					SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
					bufferedWriter.write(simpleFormat.format(c.getTime()));
				}

				bufferedWriter.close();
				fileWriter.close();

			} catch (IOException ex) {
				logger.error("Error writing to file '" + fileName_ht + "'");
			}
				return newListOfJson;


	}

}
