/**
 * 
 */
package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.util.StringUtils;

import com.mongodb.DBObject;
import com.times.common.mail.EmailProcessor;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.common.util.MailerUtil;
import com.times.mailer.dao.CustomParamDao;
import com.times.mailer.dao.MailerDao;
import com.times.mailer.dao.MetaDao;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetter;
import com.times.mailer.model.NewsLetterDailyTracker;
import com.times.mailer.model.SMSUser;


public class SMSMailerReader  implements ItemReader<SMSUser>, StepExecutionListener { //, JobExecutionListener


	private final static Logger logger = LoggerFactory.getLogger(SMSMailerReader.class);

	
	private EmailProcessor smtpMailer;
	private Mailer myMailer;
	private MailerDao dao;
	private static String alertSubject = null;

	private int perPage = 10;
	private int pageNo = 0;
	private String newsLetterId = null;


	private List<SMSUser> lstSubscribers = Collections.synchronizedList(new ArrayList<SMSUser>());
	

	//code changes ... 
	private boolean isInitialized = false;
	private String cacheUIKey = null;
	
	

	public static final String NL_FORMAT_TEXT = "text";
	public static final String NL_FORMAT_HTML = "html";
	public static final String NL_FORMAT_PARAM = "nlformat";
	public static final long TIME_INTERVAL = 10;
	
	public static String MYTIME_GETUSER = "http://myt.indiatimes.com/mytimes/getUsersInfo?ssoids=#ssoids&extraInfo=true";

	
	public SMSUser read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		logger.debug("iread method start..");


		if (myMailer != null && lstSubscribers != null && lstSubscribers.size() == 0) {

			this.pushSubscribersIntoList(myMailer, perPage, pageNo );

			logger.debug("Fetch User Done");

		}

		// if subscriber got then 
		if (lstSubscribers.size() > 0) {
			
			SMSUser smsUser = getSubscriber();
			
			return smsUser;
		} 

		return null;
	}


	/**
	 * Get the Newsletter user/subscriber, if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private SMSUser getSubscriber() {
		List<MailUser> mailUserSubscribersList = new ArrayList<MailUser>();
		SMSUser subscriberUser = null;
		synchronized (this) {
			if (lstSubscribers != null && lstSubscribers.size() > 0) {
				subscriberUser = lstSubscribers.remove(0);
				
				dao.updateSMSFLAGEntry(subscriberUser.getId());
				
				if (subscriberUser.getUsers() != null) {
					String smsUsers = subscriberUser.getUsers();
					String url = MYTIME_GETUSER.replace("#ssoids", smsUsers);
					String sData = MailerUtil.getData(url);
					if (sData != null) {

						JSONArray ja;
						try {
							ja = new JSONArray(sData);
							if (ja != null && ja.length() > 0) {
								for (int i = 0; i < ja.length(); i++) {
									
									JSONObject json1 = ja.getJSONObject(i);
									if (json1.has("EMAIL")) {
										MailUser mu = new MailUser();
										mu.setEmailId(json1.getString("EMAIL"));

										mu.setFirstName(json1.getString("F_N"));

										if (json1.has("M_N")) {
											mu.setMobile(json1.getString("M_N"));
										}
										NewsLetter nl = new NewsLetter();
										nl.setId(myMailer.getId());
										nl.setDisplayFormate(NL_FORMAT_HTML);
										int scheduledTime = myMailer.getScheduleTime();
										nl.setScheduleTime(scheduledTime);

										nl.setDaily(myMailer.isDaily());
										mu.setNewsletter(nl);
										mailUserSubscribersList.add(mu);
									}
								}
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					
					subscriberUser.setMailUserSubscribers(mailUserSubscribersList);
				}

			}
		}
		return subscriberUser;
	}

	/**
	 * @param subscriberUser
	 */
	private void updateCounterInMap(MailUser subscriberUser) {
		//logger.debug("updateCounterInMap method ..");
		try {
			int lastCounter = 1;
			if (subscriberUser != null && subscriberUser.getNewsletter() != null) {
				int dailyOrWeekly = subscriberUser.getNewsletter().isDaily() ? 1: 2;
				int morningOrEvening = subscriberUser.getNewsletter().getScheduleTime();
				String sKey = subscriberUser.getNewsletter().getId(); 
				sKey = sKey + Constant.KEY_BUILDER_SEPERATOR + dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + morningOrEvening;
				if ( InMemoryData.emailSentCounter.containsKey(sKey)) {
					lastCounter = InMemoryData.emailSentCounter.get(sKey);
					InMemoryData.emailSentCounter.put(sKey, ++lastCounter);
				} else {
					InMemoryData.emailSentCounter.put(sKey, new Integer(lastCounter));
				}
			}
		} catch (Exception e) {
			logger.error("in side the updateCounterInMap method, msg : " + e.getMessage());
		}
	}

	/**
	 * This method is used to update stats finally for one newsletter.
	 * 
	 * @param sKey
	 */
	private void updateEmailStats(String sKey) {
		int iCount = InMemoryData.emailSentCounter.get(sKey);
		int ireadCount = 0;
		this.getDao().addMailerStats(sKey, 0, iCount);
		String code = null;
		if (InMemoryData.keyCodeMap.containsKey(sKey)) {
			code = InMemoryData.keyCodeMap.get(sKey);
		}
		//travelNewsletterDao.updateNewsLetterDailyTracker(code, sKey, ireadCount, iCount);
	}

	//public void List<DBObject> get

	/**
	 * This method is used to push the subscriber into the List of {@link MailUser}.
	 *  
	 */
	private void pushSubscribersIntoList(Mailer myMailer, int perPage, int pageNo) {
		long dailyOrWeekly = 1;
		if (lstSubscribers == null || lstSubscribers.isEmpty()){
			this.pageNo = ++pageNo;

			try {
				
				//customParamDao.addOrupdatePageNo(myMailer.getId(), dailyOrWeekly, myMailer.getScheduleTime(), pageNo);

				logger.debug("NLID : " + myMailer.getId() + " , pageNo : " + pageNo);
				
				lstSubscribers = dao.getSMSSubscriberDetails(myMailer, perPage, pageNo);

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}


	/**
	 * This method is used to get the Mailer i.e. Newsletter master object for the mailer.
	 * 
	 * @param nlId
	 * @param isDaily
	 * @return
	 */
	private List<Mailer> getMailer(String nlId, boolean isDaily) {
		List<Mailer> mailerMaster = dao.getNewsletterMailer(nlId, isDaily);
		return mailerMaster;
	}

	/**
	 * This method is used to add the newsletter Body part in in-memory i.e. collection.
	 * 
	 * @param myMailer
	 */
	private void initializeNewsletter(Mailer myMailer) {
		try {

			String newsid = myMailer.getId();
			logger.debug("nl id :" + newsid + " going to pull data ...");
			int scheduledTime = myMailer.getScheduleTime();
			int daily_weekly = myMailer.isDaily() ? 1 : 2;
			String skey1 = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + scheduledTime ;
			//String uniqueCode = addNewsLetterDailyTrackerIfNotfound(myMailer, newsid, scheduledTime, daily_weekly, skey1);

			
			String urlNewsletter =  myMailer.getHtmlPath(); 
			
			alertSubject = myMailer.getMailSubject();			
			
			String scachekey = skey1 +Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_HTML;
			if (!InMemoryData.newsMailerUIMap.containsKey(scachekey)) {
			
				String url4Html = urlNewsletter.indexOf("?") == -1 ? urlNewsletter +"?" : urlNewsletter + "&";
				url4Html = url4Html  + NL_FORMAT_PARAM + "=" + NL_FORMAT_HTML;
				String sData = MailerUtil.getData(url4Html);
				if (!StringUtils.hasText(sData)) {
					sData = MailerUtil.getData(url4Html);
				}
				if (sData != null) {
					//sData = sData.replace("##code##", uniqueCode);
					InMemoryData.newsMailerUIMap.put(scachekey, sData);
				}

				
				logIfDataNotFound(newsid, url4Html, sData);
			}



			logger.debug("initializeNewsletter : ");

		} catch (Exception e) {
			logger.error("exception occured while retreiving content , msg: " + e.getMessage());
		}
	}

	/**
	 * This method is used to logged the data.
	 * 
	 * @param url4Html
	 * @param sData
	 */
	private void logIfDataNotFound(String nlId, String url4Html, String sData) {
		String msg = null;
		if (sData != null) {
			msg =  "url : " + url4Html + " , len : " + sData.length() + " nlid : " + nlId;
			logger.debug(" url : " + url4Html + " , len : " + sData.length() + " nlid : " + nlId);
		} else {
			logger.debug("ALERT ALERT NOT FOUND data for url : " + url4Html + " , nlid : " + nlId);
			try {
				smtpMailer.processStart("DATA NOT FOUND for URL : " + url4Html + " , nlid : " + nlId  + " MSG : " + msg);
			} catch (Exception e) {
				logger.debug("EXception url : " + url4Html + " , nlid : " + nlId + e.getMessage()); ;
			}
		}
	}

	/**
	 * This method is used to add the newsLetterTracker if not found for that days, frequency, time and nlId.
	 * 
	 * @param myMailer
	 * @param newsid
	 * @param iTime
	 * @param itype
	 * @param skey1
	 */
	private String addNewsLetterDailyTrackerIfNotfound(Mailer myMailer, String newsid, int iTime, int itype, String skey1) {
		String uniqueCode= null;
		String yyymmdd = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		DBObject nlTracker = dao.getNewsLetterDailyTracker(newsid, yyymmdd, String.valueOf(iTime), String.valueOf(itype));
		if (nlTracker == null) {
			uniqueCode = getUniqueCodeByDate(skey1);
			NewsLetterDailyTracker nlDailyTracker = new NewsLetterDailyTracker(new Date(), Integer.parseInt(myMailer.getId()), uniqueCode, iTime, itype, yyymmdd);
			dao.addNewsLetterDailyTracker(nlDailyTracker); 

		} else {
			uniqueCode = String.valueOf(nlTracker.get(NewsLetterDailyTracker.NEWS_LETTER_CODE));
		}
		if (!InMemoryData.keyCodeMap.containsKey(skey1)) {
			InMemoryData.keyCodeMap.put(skey1, uniqueCode);
		}

		return uniqueCode;
	}

	/**
	 * This method is used to get the StringCode.
	 * @param str
	 * @return
	 */
	private String getUniqueCodeByDate(String key) {
		String date = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		String code = MailerUtil.getUniqueCodeByDate(date + key);
		return code;	
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {		
		
		
		logger.debug(" beforeStep method..");
		long dailyOrWeekly = 1;
		long morningOrEvening = 1;
		JobParameters jobParams = stepExecution.getJobParameters();
		this.newsLetterId = jobParams.getString(Constant.JOB_PARAM_NEWSLETTER_ID);
		
		
		
		// page no is applicable job is going to restart so that it will run from next page where batch running.
		if (jobParams.getString(Constant.JOB_PARAM_NL_RESTART_KEY) != null) {
			int pageNo = (int)jobParams.getLong(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO);
			this.pageNo = ++pageNo;
		} 

		//List<Mailer> mailers = travelNewsletterDao.getNewsletterMailer(newsLetterId, isDaily);
		List<Mailer> mailers = this.getMailer(newsLetterId, true);
		if (mailers != null && mailers.size() > 0) {
			this.myMailer = mailers.get(0);
		}
		if (myMailer != null) {
			// get mailer body content into memory
			this.initializeNewsletter(myMailer);
		}
		this.isInitialized = true;
		
		morningOrEvening = myMailer.getScheduleTime();
		dailyOrWeekly = myMailer.isDaily() ? 1 : 2;

		this.cacheUIKey = this.newsLetterId + Constant.KEY_BUILDER_SEPERATOR + dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + morningOrEvening;

	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");

		this.removeInMemoryData();

		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		return stepExecution.getExitStatus();
	}

	/**
	 * This method is used to remove the in-memory data for the that newsletter.
	 */
	private void removeInMemoryData() {
		if (InMemoryData.newsMailerDataMap.containsKey(this.cacheUIKey)){
			InMemoryData.newsMailerDataMap.remove(this.cacheUIKey);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_HTML)){
			InMemoryData.newsMailerUIMap.remove(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_HTML);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_TEXT)){
			InMemoryData.newsMailerUIMap.remove(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_TEXT);
		}
		if (InMemoryData.emailSentCounter.containsKey(this.cacheUIKey)) {
			InMemoryData.emailSentCounter.remove(this.cacheUIKey);
		}
	}


	/**
	 * @return
	 */
	public Mailer getMyMailer() {
		return myMailer;
	}

	/**
	 * @param myMailer
	 */
	public void setMyMailer(Mailer myMailer) {
		this.myMailer = myMailer;
	}

	/**
	 * @param smtpMailer the smtpMailer to set
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}


	public MailerDao getDao() {
		return dao;
	}


	public void setDao(MailerDao dao) {
		this.dao = dao;
	}
	

}