package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.mailer.dao.ImageOptimizerDao;
import com.times.mailer.model.ImageInfo;

public class ImageOptimizerReader implements ItemReader<ImageInfo>, StepExecutionListener {
	
	// -----------------------------------------------------------------------------------------------
	// Start Private members
		
	private static final Logger logger = LoggerFactory.getLogger(ImageOptimizerReader.class);
	
	private ImageOptimizerDao imageOptimizerDao;
	
	public List<ImageInfo> imageInfoListFromDB = new ArrayList<ImageInfo>();
	
	private long startTime = System.currentTimeMillis();
	
	private List<String> surveyIdList = new ArrayList<String>();
	
	private List<String> omnoList = new ArrayList<String>();
	
	// End Private members
	// -----------------------------------------------------------------------------------------------
	
	 ImageOptimizerReader(List<String> surveyIdList, List<String> omnoList){
		
		 this.surveyIdList = surveyIdList;
		 this.omnoList = omnoList;
	}
	
	
	// -----------------------------------------------------------------------------------------------
	// Start public methods

	@Override
	public ImageInfo read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		if (this.imageInfoListFromDB.size() > 0) {
			
			logger.info("Reading image optimiser,  size of list: "+ this.imageInfoListFromDB.size());
			
			ImageInfo imageInfo = this.imageInfoListFromDB.remove(0);
			
			return imageInfo;
		}
		return null;
	}
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		
		logger.debug(" Before Read Step method..");
		
		startTime = System.currentTimeMillis();
		
	    try {
			populateImageInfoList();
			
		} catch (InterruptedException e) {
			
			logger.error("Thread interrupted for Amazon Url hit: "+e.getMessage());
		}
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		
		logger.debug("After Step status : " + stepExecution.getExitStatus().toString());
		
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}
		
		return stepExecution.getExitStatus();
	}
	
	// End public methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start private methods
	
	public void populateImageInfoList() throws InterruptedException {
		
		List<ImageInfo> imageList = null;
		
		if(this.surveyIdList.size() > 0){
			
			for(int i =0 ; i < this.surveyIdList.size() ; i++){
				
				imageList  = this.getImageInfoList(this.surveyIdList.get(i), this.omnoList.get(i));
				
				if(imageList != null){
					
					this.imageInfoListFromDB.addAll(imageList);
				}
			}
		}
		
		logger.debug("Size of Image list: "+ this.imageInfoListFromDB.size());
	}
	
	
	
	private List<ImageInfo> getImageInfoList(String id, String omno){
		
		return this.imageOptimizerDao.getImageInfoList(id, omno);
	}
	
	// End private methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start getters/setters

	public ImageOptimizerDao getImageOptimizerDao() {
		return imageOptimizerDao;
	}

	public void setImageOptimizerDao(ImageOptimizerDao imageOptimizerDao) {
		this.imageOptimizerDao = imageOptimizerDao;
	}

	// End getters/setters
	// -----------------------------------------------------------------------------------------------

}
