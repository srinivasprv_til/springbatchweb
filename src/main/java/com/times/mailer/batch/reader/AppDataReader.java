package com.times.mailer.batch.reader;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.model.MailUser;


/**
 * This reader read all json docs from location 'https://go.urbanairship.com/api/channels' and store details in app_user_data collection.
 * Job keep running until APP_DATA_URL has next 1000 user details data.
 * @author Rajeev.Khatri
 *
 */
public class AppDataReader implements ItemReader<List<DBObject>>,StepExecutionListener {


	private final static Logger logger = LoggerFactory.getLogger(AppDataReader.class);
	private String APP_DATA_URL = "https://go.urbanairship.com/api/channels";
	private List<DBObject> resultList = Collections.synchronizedList(new ArrayList<DBObject>());
	private int pageNo = 0;

	public void beforeStep(StepExecution stepExecution) {		
		logger.debug(" beforeStep method..");	
	}



	@Override
	public List<DBObject> read() throws Exception, UnexpectedInputException,
	ParseException, NonTransientResourceException {

		if (APP_DATA_URL != null) {
			
			pageNo++;
			logger.debug(" read method url : " + APP_DATA_URL + " & Page No : " + pageNo);	
			// will try 3 times to fetch data
			getAppData(3);
			if (resultList != null && resultList.size() > 0) {
				return resultList;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}


	/**
	 * Read next 1000 data url and json array.
	 * @param jsonMap
	 * @return
	 * @throws IllegalArgumentException
	 * @throws ParseException
	 */
	public List<DBObject> transform(JsonNode jsonMap) throws IllegalArgumentException, ParseException {
		
		List<DBObject> resultList = new ArrayList<DBObject>();

		JsonNode entries = jsonMap.get("channels");
		if (jsonMap.get("next_page") != null) {
			APP_DATA_URL = jsonMap.get("next_page").asText();
		} else {
			APP_DATA_URL = null;
		}
		logger.debug(String.valueOf(entries.size()));

		for (JsonNode entry : entries) {
			DBObject dbo = (BasicDBObject)JSON.parse(entry.toString());	
			if (dbo.get("last_registration") != null) {
				dbo.put("last_reg_at", getDate(dbo.get("last_registration").toString()));
				//dbo.put("last_registration", getDateInString(dbo.get("last_registration").toString()));
			}
			if (dbo.get("created") != null) {
				dbo.put("c_at", getDate(dbo.get("created").toString()));
				//dbo.put("created", getDateInString(dbo.get("created").toString()));
			}
			
			if (dbo.get("alias") != null && !"null".equalsIgnoreCase(String.valueOf(dbo.get("alias")))) {
				String [] str = String.valueOf(dbo.get("alias")).split("\\|");
				if (str != null ) {
					if (str.length == 2 ) {
						dbo.put("email", str[0]);
						dbo.put("device_id", str[1]);
					} else if (str.length == 1 ) {
						if (str[0].indexOf("@") != -1) {
							dbo.put("email", str[0]);
						} else {
							dbo.put("device_id", str[0]);
						}
					}
				}
			}
			resultList.add(dbo);
		}

		return resultList;

	}
	
	private Date getDate(String date) {
		//SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date dateString = null;
		try {
			dateString = dateFormat1.parse(date);
			/*Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateString);
			Date dateFinal = calendar.getTime();*/
			return dateString;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dateString;
	}
	
	private String getDateInString(String date) {
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat1.setTimeZone(TimeZone.getTimeZone("IST"));
		String dateString = null;
		try {
			
			dateString = dateFormat1.format(dateFormat1.parse(date));
			/*Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateString);
			Date dateFinal = calendar.getTime();*/
			return dateString;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dateString;
	}
	
	


	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("Job complete. url : " + APP_DATA_URL);
		return null;
	}


	private void getAppData(int counter) {
		try {

			int i=0;
			InputStream contentStream = getAppDataCall();
			if (contentStream != null) {
				JsonNode jsonMap = null;
				ObjectMapper mapper = new ObjectMapper();
				jsonMap = mapper.readTree(contentStream);
				resultList = transform(jsonMap);

			} else {
				logger.debug("Data not found for url : " + APP_DATA_URL + ", in recursive call, times= " + i);

				i++;

				if (i < counter) {
					getAppData(i);
				} else {
					logger.debug("Data not found for url : " + APP_DATA_URL + ", in recursive call, times= " + i);

				}
			}


		}catch (Exception e) {

		}
	}

	/**
	 * Get data from APP_DATA_URL url with credentials.
	 * @return
	 */
	private InputStream getAppDataCall() {
		URL dataURLObj;
		try {
			dataURLObj = new URL(APP_DATA_URL);
			URLConnection uc = dataURLObj.openConnection();

			String appKey = "lHXpjzy8TL6g9NFezhFe8A";
			String master_secret = "dFGjfMaxRz2WN8l1EfB7LA";
			String userpass = appKey + ":" + master_secret;
			String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
			uc.setRequestProperty ("Authorization", basicAuth);

			return uc.getInputStream();
		} catch (MalformedURLException e) {
			logger.debug("exception caught : " + e.getMessage());
			
		} catch (IOException e) {
			logger.debug("exception caught : " + e.getMessage());
			
		} catch (Exception e) {
			logger.debug("exception caught : " + e.getMessage());
			
		}
		return null;

	}
}