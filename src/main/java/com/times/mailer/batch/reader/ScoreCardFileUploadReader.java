/**
 * 
 */
package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.util.Constant;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.AmazonS3Credits;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NetstorageInfo;
import com.times.mailer.model.FileLocationInfo.LocationType;

/**
 * This Spring batch reader class is used as a spring batch itemReader.
 * 
 * @author Ranjeet.Jha
 *
 */
public class ScoreCardFileUploadReader implements ItemReader<FileLocationInfo>, StepExecutionListener { //, JobExecutionListener

	private final static Logger logger = LoggerFactory.getLogger(ScoreCardFileUploadReader.class);
	
	//private EmailProcessor smtpMailer;
	private NetstorageFileInfoDao netstorageFileInfoDao;
	
	//private Boolean isExisting = false;
	private int perPage = 100;
	private int pageNo = 0;
	
	public List<FileLocationInfo> fileLocationInfoList = Collections.synchronizedList(new ArrayList<FileLocationInfo>());
	
	// hostid as key and value as msid arrays, set by launcher if any then run the batch else no need to run batch job.
	public static Map<Integer, List<Integer>> hostMsidMap = Collections.synchronizedMap(new HashMap<Integer, List<Integer>>()); 
	
	//code changes ... 
	boolean isInitialized = false;
	
	/**
	 * @param netstorageFileInfoDao the netstorageFileInfoDao to set
	 */
	public void setNetstorageFileInfoDao(NetstorageFileInfoDao netstorageFileInfoDao) {
		this.netstorageFileInfoDao = netstorageFileInfoDao;
	}
	
	/**
	 * This method is used to read the {@link FileLocationInfo} object.
	 * 
	 * @param args
	 */
	public FileLocationInfo read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		//logger.debug("TOIListLevelReader.read method start..");
		
		// if subscriber got then 
		if (fileLocationInfoList.size() > 0) {
			//counter update in memory
			FileLocationInfo fileUploadDtls = getFileLocationInfo();
			return fileUploadDtls;
		} 
		
		return null;
	}

	/**
	 * Get the {@link FileLocationInfo} domain model object , if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private FileLocationInfo getFileLocationInfo() {
		FileLocationInfo fileLocationInfo = null;
		synchronized (this) {
			if (fileLocationInfoList != null && fileLocationInfoList.size() > 0) {
				fileLocationInfo = fileLocationInfoList.remove(0);
			}
		}
		return fileLocationInfo;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		//logger.debug(" beforeStep method..");
		JobParameters jobParams = stepExecution.getJobParameters();
		int workType = Integer.parseInt(jobParams.getString(Constant.JOB_PARAM_JOBTYPE_ID));
		
		// page no is applicable job is going to restart so that it will run from next page where batch running.
		/*if (jobParams.getString(Constant.JOB_PARAM_NL_RESTART_KEY) != null) {
			int pageNo = (int)jobParams.getLong(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO);
			this.pageNo = ++pageNo;
		} */
		
		this.isInitialized = true;
		// this return without -#matchid url
		List<FileLocationInfo> fileLocationInfo = netstorageFileInfoDao.getFileLocationInfo(139, Constant.WORK_TYPE_SCORE_CARD, 0, 100);
		
		List<Integer> matchIdList = netstorageFileInfoDao.getActiveMatchIdList();
		if (matchIdList != null && matchIdList.size() > 0) {
			// this return with -#matchid url
			List<FileLocationInfo> fileLocationInfoWithMatchId = netstorageFileInfoDao.getFileLocationInfo(139, Constant.WORK_TYPE_SCORE_CARD_VAR, 0, 100);
			// return each match id list i.e. noOfList * noOfMatchId
			List<FileLocationInfo> fileLocListTemp = getFileLocationInfoByMatchIds(matchIdList, fileLocationInfoWithMatchId);
			
			// merge both into single List and finally assign into.
			if (fileLocListTemp != null && fileLocListTemp.size() > 0) {
				for (FileLocationInfo info : fileLocListTemp) {
					fileLocationInfo.add(info);
				}
			}
		}
		
		// assign final list.
		fileLocationInfoList = fileLocationInfo;
	}

	
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		/*JobParameters jobParams = stepExecution.getJobParameters(); 
		if (jobParams != null) {
			for (Map.Entry<String, JobParameter> entry : jobParams.getParameters().entrySet()) {
				logger.debug(entry.getKey() + " : " + entry.getValue().getValue());
			}
		}*/
		
		//this.removeInMemoryData();
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		
		return stepExecution.getExitStatus();
	}

	/**
	 * This method is used to build the FileLocationInfo list if multiple live blog active for the one host
	 * 
	 * @param matchIdList
	 * @param fileInfoList
	 * @return
	 */
	private List<FileLocationInfo> getFileLocationInfoByMatchIds(List<Integer> matchIdList, List<FileLocationInfo> fileInfoList) {
		try {
			if (matchIdList != null && matchIdList.size() > 0 && fileInfoList != null && fileInfoList.size() > 0) {
				List<FileLocationInfo> fileInfoByMsid = new ArrayList<FileLocationInfo>(fileInfoList.size() * matchIdList.size());
				
				for (Integer msid : matchIdList) {
					
					for (FileLocationInfo fInfo : fileInfoList) {
						FileLocationInfo info = new FileLocationInfo();
						info.setId(fInfo.getId());
						info.setSourceUrl(fInfo.getSourceUrl().replace("#matchid", String.valueOf(msid)));
						info.setStatus(fInfo.isStatus());
						info.setWorkType(fInfo.getWorkType());
						info.setLocationType(fInfo.getLocationType());
						
						if (fInfo.getLocationType() == LocationType.NET_STORAGE) {
							NetstorageInfo ftpInfo = new NetstorageInfo();
							ftpInfo.setFileName( fInfo.getNetstorageLocation().getFileName().replace("#matchid", String.valueOf(msid)));
							ftpInfo.setId(fInfo.getNetstorageLocation().getId());
							ftpInfo.setFolderLocation(fInfo.getNetstorageLocation().getFolderLocation());
							ftpInfo.setPassword(fInfo.getNetstorageLocation().getPassword());
							ftpInfo.setServerName(fInfo.getNetstorageLocation().getServerName());
							ftpInfo.setUserid(fInfo.getNetstorageLocation().getUserid());
							
							info.setNetstorageLocation(ftpInfo);
							
						} else if (fInfo.getLocationType() == LocationType.AWS) {

							info.setAmazonS3(new AmazonS3Credits(fInfo.getAmazonS3().getAccessKey(), fInfo.getAmazonS3().getSecretKey(), fInfo.getAmazonS3().getBucketName(), fInfo.getAmazonS3().getFileName().replace("#matchid", String.valueOf(msid)), null));
						}
						
						fileInfoByMsid.add(info);
					}
				}
				
				return fileInfoByMsid;
				
			}
		} catch (Exception e) {
			//e.printStackTrace();
			logger.debug("msg : " + e.getMessage());
		}
		return fileInfoList;
	}
	
	
}