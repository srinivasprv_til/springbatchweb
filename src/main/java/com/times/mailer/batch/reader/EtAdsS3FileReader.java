package com.times.mailer.batch.reader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ibm.icu.util.Calendar;
import com.times.common.service.FileContentServicesImpl;
import com.times.common.service.upload.FileUploadServiceFactory;
import com.times.common.service.upload.IUploadService;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.dao.NetstorageFileInfoDaoImpl;
import com.times.mailer.model.FileLocationInfo;

public class EtAdsS3FileReader {
	
	
	private FileContentServicesImpl fileServiceImpl;
	private final static Logger logger = LoggerFactory.getLogger(EtAdSenseReader.class);
	
	public static void main (String... args) throws Exception{
		
		ApplicationContext context = new ClassPathXmlApplicationContext("common-context.xml");
		
		BasicDataSource dataSource = (BasicDataSource) context.getBean("dataSource");
		
		NetstorageFileInfoDao netstorageDao = new NetstorageFileInfoDaoImpl(dataSource);
		
		if (args[0] !=null){
			
			int workType = Integer.parseInt(args[0]);
			
			FileLocationInfo fileLocationObj = getSourceFtpPath(workType, netstorageDao);
			
			if (fileLocationObj!=null){
				String sourceUrl = fileLocationObj.getSourceUrl();
				logger.debug("filelocationObj ----> {}", fileLocationObj);
				String ftpClient = parseSourceForFtpPath(sourceUrl, 0).split("//")[1];
				logger.debug("FTP Client value is {}" ,ftpClient );
				String userName = parseSourceForFtpPath(sourceUrl, 1);
				logger.debug("Username value is {}" ,userName );
				String userPasswd =  parseSourceForFtpPath(sourceUrl, 2);
				logger.debug("Password value is {}", userPasswd);
				String directoryPath = parseSourceForFtpPath(sourceUrl, 3);
				logger.debug("Directory path value is {}", directoryPath);
				
				IUploadService uploadService = FileUploadServiceFactory.getUploadServiceByLocation(fileLocationObj.getLocationType());
				
				uploadFileOnET(ftpClient, userName, userPasswd,directoryPath,uploadService,fileLocationObj);
			}
		}
	}

	private static FileLocationInfo getSourceFtpPath(int workType, NetstorageFileInfoDao netstorageFileInfoDao){
		
		List<FileLocationInfo> ftpLocation = netstorageFileInfoDao.getFileLocationInfoByWorkType(workType);
		logger.debug("getting Source path from sql for work type {}" , workType );
		if (ftpLocation !=null && ftpLocation.size() >0){
			logger.debug("successfully returning filelocation object with with work type {} for source url --> {}", workType , ftpLocation.get(0).getSourceUrl());
			return ftpLocation.get(0);
		}
		return null;
	}
	

	private static String parseSourceForFtpPath(String sourceUrl, int arrIndex){
		if (sourceUrl !=null && sourceUrl.contains("##")){
			return sourceUrl.split("##")[arrIndex];
		}
		return "";
	}
	
	public static void uploadFileOnET(String ftpClientIp, String ftpUserName,String ftpPasswd,String directoryPath, IUploadService uploadService, FileLocationInfo filelocationObj) throws Exception, UnexpectedInputException,
			ParseException, NonTransientResourceException {
		
		long startTime = System.currentTimeMillis();
			FTPClient ftpClient=new FTPClient();
			try{
				logger.debug("*** starting job at for copying from directoy {} **** ",directoryPath ," ***** at ***** " + Calendar.getInstance().getTime());
				
				FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
				ftpClient.configure(conf);
				ftpClient.connect(ftpClientIp);
				String username = ftpUserName;
				String password = ftpPasswd;
				
				boolean constatus = ftpClient.login(username, password);
				ftpClient.changeWorkingDirectory(directoryPath);
				
				if (constatus){
					FTPFile [] files = ftpClient.listFiles();
					String fileNameFromDb =  filelocationObj.getAmazonS3().getFileName();
					for (FTPFile file : files){
						String remoteFile1 = file.getName();
						
						InputStream is = ftpClient.retrieveFileStream(remoteFile1);
						
						String fileContent = getContentByInputStream(is);
						
						if (is!=null) {
							try{
								filelocationObj.getAmazonS3().setFileName(fileNameFromDb+remoteFile1);
								logger.debug("s3 file upload path --> {} ",filelocationObj.toString());
								uploadService.upload(filelocationObj, fileContent);
								logger.debug("File --> " + remoteFile1 + " has been uploaded successfully.");
							}
							catch(Exception e){
								logger.debug("Problem in uploading " + remoteFile1);
							}
						}
						else{
							logger.debug("Error in getting Input stream for file  ->> " + remoteFile1);
						}
						ftpClient.completePendingCommand();
					}
					logger.debug("total number of Files Uploaded in ET ADSENSE from directory "+directoryPath+" are " + files.length);
				}
			}
			
			catch (IOException io)
			{
				io.printStackTrace();
			}
			
			finally{
				if (ftpClient.isConnected()){
					try{
						ftpClient.disconnect();
					}
					catch (IOException io){
						logger.error("error while closing FTP Connection");
					}
				}
			}
		logger.debug("****Time taken by ET ADSENSE complete job in ms : " + (System.currentTimeMillis() - startTime)+"****");
		// TODO Auto-generated method stub
	}

	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		logger.debug(" beforeStep method..");
		
	}

	public FileContentServicesImpl getFileServiceImpl() {
		return fileServiceImpl;
	}

	public void setFileServiceImpl(FileContentServicesImpl fileServiceImpl) {
		this.fileServiceImpl = fileServiceImpl;
	}

	/*public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		return stepExecution.getExitStatus();
	}*/
	
	private static String getContentByInputStream(InputStream is)	throws IOException, UnsupportedEncodingException {
		String outData;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();  
		byte[] byteArray = new byte[1024];  
		int count = 0;  
		while ((count = is.read(byteArray, 0, byteArray.length)) > 0) {
			outputStream.write(byteArray, 0, count);
		}
		outData = new String(outputStream.toByteArray(), "UTF-8");
		
		if (outputStream != null) {
			outputStream.close();
			outputStream = null;
		}
		return outData;
	}

	
}
