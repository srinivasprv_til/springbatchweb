/**
 * 
 */
package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.MailUser;

/**
 * @author Ranjeet.Jha
 *
 */
public class TOIListLevelFileReader implements ItemReader<FileLocationInfo>, StepExecutionListener { //, JobExecutionListener

	private final static Logger logger = LoggerFactory.getLogger(TOIListLevelFileReader.class);
	
	//private EmailProcessor smtpMailer;
	private NetstorageFileInfoDao netstorageFileInfoDao;
	
	//private Boolean isExisting = false;
	private int perPage = 100;
	private int pageNo = 0;
	
	public List<FileLocationInfo> fileLocationInfoList = Collections.synchronizedList(new ArrayList<FileLocationInfo>());
	
	//code changes ... 
	boolean isInitialized = false;
	
	/**
	 * This method is used to read the {@link FileLocationInfo} object.
	 * 
	 * @param args
	 */
	public FileLocationInfo read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		//logger.debug("TOIListLevelReader.read method start..");
		
		// if subscriber got then 
		if (fileLocationInfoList.size() > 0) {
			//counter update in memory
			FileLocationInfo fileUploadDtls = getFileLocationInfo();
			return fileUploadDtls;
		} 
		
		return null;
	}

	/**
	 * Get the Newsletter user/subscriber, if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private FileLocationInfo getFileLocationInfo() {
		FileLocationInfo fileLocationInfo = null;
		synchronized (this) {
			if (fileLocationInfoList != null && fileLocationInfoList.size() > 0) {
				fileLocationInfo = fileLocationInfoList.remove(0);
			}
		}
		return fileLocationInfo;
	}
	
	/**
	 * This method is used to update stats finally for one newsletter.
	 * 
	 * @param sKey
	 */
	private void updateEmailStats(String sKey) {
		int iCount = InMemoryData.emailSentCounter.get(sKey);
		int ireadCount = 0;
		//this.getTravelNewsletterDao().addMailerStats(sKey, 0, iCount);
		String code = null;
		if (InMemoryData.keyCodeMap.containsKey(sKey)) {
			code = InMemoryData.keyCodeMap.get(sKey);
		}
		//travelNewsletterDao.updateNewsLetterDailyTracker(code, sKey, ireadCount, iCount);
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");
		JobParameters jobParams = stepExecution.getJobParameters();
		int workType = Integer.parseInt(jobParams.getString(Constant.JOB_PARAM_JOBTYPE_ID));
		
		// page no is applicable job is going to restart so that it will run from next page where batch running.
		if (jobParams.getString(Constant.JOB_PARAM_NL_RESTART_KEY) != null) {
			int pageNo = (int)jobParams.getLong(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO);
			this.pageNo = ++pageNo;
		} 
		
		this.isInitialized = true;
		//this.fileLocationInfoList = populateFileLocationInfo();
		if (workType != 0) {
			this.fileLocationInfoList = netstorageFileInfoDao.getFileLocationInfo(workType, pageNo, perPage);
			
		}
		
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		/*JobParameters jobParams = stepExecution.getJobParameters(); 
		if (jobParams != null) {
			for (Map.Entry<String, JobParameter> entry : jobParams.getParameters().entrySet()) {
				logger.debug(entry.getKey() + " : " + entry.getValue().getValue());
			}
		}*/
		
		//this.removeInMemoryData();
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		
		return stepExecution.getExitStatus();
	}

	/**
	 * This method is used to remove the in-memory data for the that newsletter.
	 */
	private void removeInMemoryData() {
		
	}
	
	/*public List<FileLocationInfo> populateFileLocationInfo() {
		List<FileLocationInfo> list = new ArrayList<FileLocationInfo>();
		
		NetstorageInfo akamaiInfo = new NetstorageInfo();
		FileLocationInfo fileLocationInfo = new FileLocationInfo();
		akamaiInfo.setFileName("myindex.html");
		akamaiInfo.setServerName("timescricket.upload.akamai.com");
		akamaiInfo.setFolderLocation("/92522/TOI/city");
		akamaiInfo.setUserid("toiupload");
		akamaiInfo.setPassword("user4upl0ad");
		
		fileLocationInfo.setNetstorageLocation(akamaiInfo);
		fileLocationInfo.setSourceUrl("http://timesofindia.indiatimes.com/city");
		fileLocationInfo.setStatus(true);
		list.add(fileLocationInfo);
		//list.add(new FileLocationInfo(2,"http://timesofindia.indiatimes.com/india", 2, akamaiInfo));
		
		return list;
	}*/

	/**
	 * @param netstorageFileInfoDao the netstorageFileInfoDao to set
	 */
	public void setNetstorageFileInfoDao(NetstorageFileInfoDao netstorageFileInfoDao) {
		this.netstorageFileInfoDao = netstorageFileInfoDao;
	}
	
}
	

