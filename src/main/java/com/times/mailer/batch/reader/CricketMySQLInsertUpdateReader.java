package com.times.mailer.batch.reader;

import java.io.FileNotFoundException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.util.ExternalHttpUtil;

/**
 * This class is used to insert / updata match id in MySql.
 * 
 * @author Geetanjali
 *
 */
public class CricketMySQLInsertUpdateReader implements ItemReader<Map<String,String>>, StepExecutionListener { 

	private final static Logger logger = LoggerFactory.getLogger(CricketMySQLInsertUpdateReader.class);
	private Map<String,String> matchMap = Collections.synchronizedMap(new HashMap<String,String>());
	//code changes ... 
	boolean isInitialized = false;
	private long startTime = System.currentTimeMillis();
	boolean isStart = true;

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");
		startTime = System.currentTimeMillis();
		this.isInitialized = true;
		// populate in list
		try {
			matchMap = getMapofMatchId();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		
		this.isInitialized = false;
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}
		
		return stepExecution.getExitStatus();
	}

	public Map<String,String> getMapofMatchId() throws FileNotFoundException {
				
		String cricketFeedUrl="http://feeds.gocricket.com/LiveMatchFeeds/getlivematchfeeds";
		Map<String,String> matchMap=new HashMap<String,String>();
		
		try {
			String extdata = ExternalHttpUtil.getExtDataForSolar(cricketFeedUrl);
			System.out.println(cricketFeedUrl);
			logger.debug(extdata);
			if(extdata !=null){
				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(new StringReader(extdata));
				XPathFactory xFactory = XPathFactory.instance();
				XPathExpression<Element> expression = xFactory.compile("match/schedule",Filters.element());
				List<Element> alldocument = expression.evaluate(doc);
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				Date today=new SimpleDateFormat("yyyy/MM/dd").parse(dateFormat.format(date));
				for (Element document : alldocument) {
					String sDate1=document.getChildText("match_Date");
					Date date1=new SimpleDateFormat("yyyy/MM/dd").parse(sDate1);					
					
					
					if(date1.equals(today) || date1.before(today)){
						matchMap.put(document.getChildText("match_Id"), document.getChildText("live"));
					}
					System.out.println(dateFormat.format(date)); 
				}	
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block
			 e.printStackTrace();
		}	
		  return matchMap;
		}
	
/*	public static void main(String[] args) {
		CricketMySQLInsertUpdateReader rr= new CricketMySQLInsertUpdateReader();
		try {
			rr.getMapofMatchId();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

	@Override
	public Map<String, String> read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		try{
			System.out.println(isStart);
			if (matchMap != null && matchMap.size() > 0 && isStart) {
				isStart = false;
				return matchMap;
			} else {
				return null;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}