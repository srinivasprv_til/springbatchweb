package com.times.mailer.batch.reader;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.Constant;
import com.times.mailer.batch.utilities.EPaperUtilities;
import com.times.mailer.batch.utilities.FTPEpaperService;
import com.times.mailer.batch.utilities.TimeoutCallable;
import com.times.mailer.model.EpaperFileDetails;
import com.times.mailer.model.EpaperInfo;
import com.times.mailer.model.FTPFileWithDirectory;

public class EpaperReader implements ItemReader<EpaperInfo>, StepExecutionListener {

	/*
	 * private final static String FTP_IP = "10.150.180.189"; private final
	 * static String USER_NAME = "user"; private final static String PASSWORD =
	 * "12345"; private final static String DIRECTORY = "";
	 */
	private final static Logger logger = LoggerFactory.getLogger(EpaperReader.class);

	private List<EpaperInfo> ePaperInfoList = Collections.synchronizedList(new ArrayList<EpaperInfo>());

	public static boolean fetchEPaperFileDetails(EpaperInfo ePaperInfo)
	        throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		try {

			FTPEpaperService ftpEpaperService = new FTPEpaperService(ePaperInfo, getDate());
			List<FTPFileWithDirectory> files = ftpEpaperService.listFiles();

			/* for varification of filtering logic */
			System.out.println("Unfiltered files:");
			for (FTPFileWithDirectory file : files) {
				System.out.println(file.getName());
			}

			files = filterFiles(files);

			/* for varification of filtering logic */
			System.out.println("Filtered files:");
			for (FTPFileWithDirectory file : files) {
				System.out.println(file.getName());
			}

			for (FTPFileWithDirectory file : files) {
				String remoteFileName = file.getName();

				DBObject query = new BasicDBObject();
				query.put("fileName", remoteFileName);

				DBObject existingRecord = MongoRestAPIService.get(Constant.EPAPER_RAW_DATA_COLLECTION, query);

				if (existingRecord == null) {
					InputStream is = ftpEpaperService.retrieveFileStream(file);

					File downLoadedFile = getContentByInputStream(is, remoteFileName);
					ftpEpaperService.completePendingCommands();
					String fileSaveID = null;

					if (downLoadedFile != null) {
						try {
							TimeoutCallable<String> timeoutCallable = new TimeoutCallable<>();
							Duration timeout = Duration.ofMinutes(1);
							try {
								fileSaveID = timeoutCallable.timeout(timeout, new Callable<String>() {

									@Override
									public String call() throws Exception {
										return MongoRestAPIService.addFile(Constant.EPAPER_FILE_COLLECTION,
										        downLoadedFile);
									}
								});
							} catch (Exception e) {
								logger.error("Error occured while saving file", e);
								throw e;
							}
							if (fileSaveID != null) {
								downLoadedFile.delete();
								EpaperFileDetails efd = new EpaperFileDetails();
								efd.setGsfid(fileSaveID);
								efd.setDate(getFormatedDate());
								efd.setCreationDate(new Date());
								efd.setFileName(remoteFileName);
								ePaperInfo.getFileDetails().add(efd);
							} else {
								logger.error("fileSaveId is null for file: {}", remoteFileName);
							}

							logger.debug("File --> " + remoteFileName + " has been saved successfully with gfsid "
							        + fileSaveID);
						} catch (Exception e) {
							logger.error("Problem in uploading " + remoteFileName, e.getMessage(), e.getStackTrace());

						}
					} else {
						logger.error("Failed downloading file from FTP {}", file.getName());
					}
				} else {
					logger.debug("skipping file already in mongo, file: {}", remoteFileName);
				}
			}

			ftpEpaperService.completePendingCommands();
			ftpEpaperService.closeSession();
			return true;
		} catch (Exception e) {
			logger.error("Error occured", e);
		}
		return false;
	}

	public static List<FTPFileWithDirectory> filterFiles(List<FTPFileWithDirectory> files) {
		Map<String, FileNameProperties> map = new java.util.HashMap<>();
		for (FTPFileWithDirectory file : files) {
			try {
				FileNameProperties fnp = getFileNameObj(file.getName());
				if (map.containsKey(fnp.getPrefix())) {
					// Map already has key
					FileNameProperties oldFnp = map.get(fnp.getPrefix());
					FileNameProperties recentFnp = recentFnp(oldFnp, fnp);
					map.put(fnp.getPrefix(), recentFnp);
				} else {
					map.put(fnp.getPrefix(), fnp);
				}
			} catch (Exception e) {
				logger.error("Error occured while processing file name: {}", file.getName(), e);
			}
		}
		/* Create collection of all recent file's names */
		Set<String> filteredFileNames = new HashSet<>();
		for (Map.Entry<String, FileNameProperties> entry : map.entrySet()) {
			filteredFileNames.add(entry.getValue().getName());
		}

		List<FTPFileWithDirectory> filteredFTPFileObjs = files.stream()
		        .filter(file -> filteredFileNames.contains(file.getName())).collect(Collectors.toList());

		return filteredFTPFileObjs;
	}

	private static FileNameProperties recentFnp(FileNameProperties fnp1, FileNameProperties fnp2) {
		if (fnp1.getMinerVersion() > fnp2.getMinerVersion()) {
			return fnp1;
		} else if (fnp1.getMinerVersion() < fnp2.getMinerVersion()) {
			return fnp2;
		} else {
			/* Both fnp has same miner version */
			if (fnp1.getRevision() > fnp2.getRevision())
				return fnp1;
			if (fnp1.getRevision() < fnp2.getRevision())
				return fnp2;
			// This should never happen as it would mean same file name
			return fnp1;
		}

	}

	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");

		BasicDBObject query = new BasicDBObject();
		query.put("STAT", 1);
		List<DBObject> dbObjectRes = MongoRestAPIService.getList(Constant.EPAPER_COLLECTION, query);

		for (DBObject mrDBObject : dbObjectRes) {
			ePaperInfoList.add(EPaperUtilities.prepareEpaperInfoObject(mrDBObject));
		}

	}

	private static File getContentByInputStream(InputStream is, String remoteFileName)
	        throws IOException, UnsupportedEncodingException {
		if (is == null) {
			logger.error("input stream is null, downloading file failed");
			return null;
		}

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		byte[] byteArray = new byte[1024];
		int count = 0;
		while ((count = is.read(byteArray, 0, byteArray.length)) > 0) {
			outputStream.write(byteArray, 0, count);
		}

		File temp = null;
		temp = new File(remoteFileName);
		convertByteArrayToFile(temp, outputStream.toByteArray());

		if (outputStream != null) {
			outputStream.close();
			outputStream = null;
		}

		return temp;

	}

	public static void convertByteArrayToFile(File outputFile, byte[] inputArray) throws IOException {
		BufferedOutputStream bos = null;
		try {
			FileOutputStream fos = new FileOutputStream(outputFile);
			bos = new BufferedOutputStream(fos);
			bos.write(inputArray);
		} finally {
			if (bos != null) {
				try {
					bos.flush();
					bos.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public EpaperInfo read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		// TEst1.uploadFile();
		if (ePaperInfoList != null && ePaperInfoList.size() > 0) {
			EpaperInfo epaperInfo = getEpaperInfo();
			fetchEPaperFileDetails(epaperInfo);

			return epaperInfo;
		} else {
			return null;
		}

	}

	private EpaperInfo getEpaperInfo() {
		EpaperInfo epaperInfo = null;
		synchronized (this) {
			if (ePaperInfoList != null && ePaperInfoList.size() > 0) {
				epaperInfo = ePaperInfoList.remove(0);
			}
		}
		return epaperInfo;
	}

	
	private static Date getDate() {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();

		/*
		 * By default job run for current date. It can be altered by passing
		 * option -Dcom.til.epaper.date=dd-MM-yy from command line
		 */
		String dateCommandLine = System.getProperty("com.til.epaper.date");
		if (dateCommandLine != null && dateCommandLine.length() > 0) {
			try {
				date = new SimpleDateFormat("dd-MM-yy").parse(dateCommandLine);
			} catch (java.text.ParseException e) {
				logger.error(
				        "Error parsing date form com.til.epaper.date, given date: {} expected date format: dd-MM-YY (e.g. 17-11-16). Will use current date.",
				        dateCommandLine);
			}
		}
		return date;

	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * This class stores details about pdf file name It is used to remove
	 * obsolete revisions.
	 * 
	 * @author arun.sharma2
	 *
	 */
	static class FileNameProperties {
		String	prefix;
		Integer	minerVersion;
		Integer	revision;
		String	color;
		String suffix;

		public String getSuffix() {
			return this.suffix;
		}

		public void setSuffix(String suffix) {
			this.suffix = suffix;
		}

		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public FileNameProperties(String prefix, Integer minerVersion, Integer revision, String color, String suffix) {
			super();
			this.prefix = prefix;
			this.minerVersion = minerVersion;
			this.revision = revision;
			this.color = color;
			this.suffix = suffix;
		}

		public String getName() {
			return prefix + "_" + minerVersion + "_" + color + "_" + "r" + revision + "." + suffix;
		}

		public String getPrefix() {
			return prefix;
		}

		public void setPrefix(String prefix) {
			this.prefix = prefix;
		}

		public Integer getMinerVersion() {
			return minerVersion;
		}

		public void setMinerVersion(Integer minerVersion) {
			this.minerVersion = minerVersion;
		}

		public Integer getRevision() {
			return revision;
		}

		public void setRevision(Integer revision) {
			this.revision = revision;
		}

	}

	private static FileNameProperties getFileNameObj(String name) throws Exception {
		// Ex: 18022016_nbtnd_mp_01_1_col_r1.pdf
		// 0 1 2 3 4 5 6
		String[] parts = name.split("_"); // [18022016, nbtnd, mp, 01, 1, col,
		                                  // r1.pdf]
		String prefix = String.join("_", Arrays.copyOfRange(parts, 0, 4));
		Integer minerVersion = Integer.parseInt(parts[4]);
		String color = parts[5];
		String[] revisionWithSuffix = parts[6].split("\\.", 2); // [r1 pdf] or [r1 qxd.pdf] 
		String suffix = revisionWithSuffix[1];
		
		Integer revision = Integer.parseInt(revisionWithSuffix[0].replace("r", ""));

		return new FileNameProperties(prefix, minerVersion, revision, color, suffix);

	}

	private static String getFormatedDate() {
		return new SimpleDateFormat("ddMMYY").format(getDate());
	}
}