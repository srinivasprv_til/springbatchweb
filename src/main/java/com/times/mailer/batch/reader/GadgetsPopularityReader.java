package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.dao.IBeatDAO;
import com.times.common.model.IBeatEntity;
import com.times.mailer.constant.GadgetConstant;

public class GadgetsPopularityReader implements ItemReader <IBeatEntity>, StepExecutionListener {
	
	private IBeatDAO iBeatDAO;
	
	List<IBeatEntity> gadgetsData= new ArrayList<IBeatEntity>();
	
	private static final Logger logger = LoggerFactory.getLogger(GadgetsPopularityReader.class);

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		
		//CLEAR MAP at the end
		if(gadgetsData!=null && gadgetsData.size()>0){
			gadgetsData.clear();
		}
		
		return stepExecution.getExitStatus();
	}

	@Override
	public void beforeStep(StepExecution arg0) {
		logger.info("In before Step of GadgetsPopularityReader" );
		preparePopularityData();
		
	}

	@Override
	public IBeatEntity read()  
			throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		if(gadgetsData!=null && gadgetsData.size()>0){
			
			IBeatEntity iBeatEntity=gadgetsData.remove(gadgetsData.size()-1);
			return iBeatEntity;
			
		}else{
			return null;
		}
		
	}
	
	
	private void preparePopularityData(){
		logger.info("Going to prepare data from Ibeat Topic Data API");
		
		gadgetsData=iBeatDAO.getLastHourData(GadgetConstant.GN_HOST_NAME);
		
		if(gadgetsData!=null && gadgetsData.size()>0){
			logger.info("Received a list of size={} records from Ibeat Topic Data API",gadgetsData.size());
		}else{
			logger.error("Null/Empty List received from IbeatDao");
		}
		
	}

	
	//Getters and Setters
	public IBeatDAO getiBeatDAO() {
		return iBeatDAO;
	}

	public void setiBeatDAO(IBeatDAO iBeatDAO) {
		this.iBeatDAO = iBeatDAO;
	}

	
	
}
