package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.joblauncher.LiveBlogFileUploadLauncher;
import com.times.common.util.Constant;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.MailUser;

/**
 * This batch reader is used for Feeds file upload.
 * 
 * @author Aakash.Gupta
 *
 */
public class FeedsReader implements ItemReader<FileLocationInfo>, StepExecutionListener , JobExecutionListener{
	//, JobExecutionListener

	private final static Logger logger = LoggerFactory.getLogger(FeedsReader.class);

	//private EmailProcessor smtpMailer;
	private NetstorageFileInfoDao netstorageFileInfoDao;

	//private Boolean isExisting = false;
	private int perPage = 100;
	private int pageNo = 0;

	public List<FileLocationInfo> fileLocationInfoList = Collections.synchronizedList(new ArrayList<FileLocationInfo>());

	//code changes ... 
	boolean isInitialized = false;

	/**
	 * @param netstorageFileInfoDao the netstorageFileInfoDao to set
	 */
	public void setNetstorageFileInfoDao(NetstorageFileInfoDao netstorageFileInfoDao) {
		this.netstorageFileInfoDao = netstorageFileInfoDao;
	}

	/**
	 * This method is used to read the {@link FileLocationInfo} object.
	 * 
	 * @param args
	 */
	public FileLocationInfo read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		//logger.debug("TOIListLevelReader.read method start..");

		// if subscriber got then 
		if (fileLocationInfoList.size() > 0) {
			//counter update in memory
			FileLocationInfo fileUploadDtls = getFileLocationInfo();
			//fileUploadDtls.setAmazonS3(new AmazonS3Credits(fileUploadDtls.getNetstorageLocation().getFileName()));

			return fileUploadDtls;
		} 

		return null;
	}

	/**
	 * Get the {@link FileLocationInfo} domain model object , if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private FileLocationInfo getFileLocationInfo() {
		FileLocationInfo fileLocationInfo = null;
		synchronized (this) {
			if (fileLocationInfoList != null && fileLocationInfoList.size() > 0) {
				fileLocationInfo = fileLocationInfoList.remove(0);
			}
		}
		return fileLocationInfo;
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		JobParameters jobParams = stepExecution.getJobParameters();
		int workType = Integer.parseInt(jobParams.getString(Constant.WORK_TYPE_FEEDS));
		String hostId = jobParams.getString(Constant.FEEDS_HOST_ID);
		pageNo = Integer.parseInt(jobParams.getString(Constant.FEEDS_PAGE_NO));
		perPage = Integer.parseInt(jobParams.getString(Constant.FEEDS_PER_PAGE));
		// page no is applicable job is going to restart so that it will run from next page where batch running.
		this.isInitialized = true;
		// add FeedsFileInfo in memory
		logger.debug("parameters receive in readers are :" + hostId + "," + workType +"," + pageNo + "," + perPage);
		addFeedsFileInfoIntoList(workType,hostId);
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		//System.out.println(" ++++++++++++++++++++++ afterStep +++++++++++++++++++++++");
		if (LiveBlogFileUploadLauncher.jobExecution != null ) {
			logger.debug("status : " + LiveBlogFileUploadLauncher.jobExecution.getStatus());
		}
			return stepExecution.getExitStatus();
	}

	/**
	 * @param workType
	 */
	private void addFeedsFileInfoIntoList(int workType, String hostId) {
		try {
			if (workType == Constant.WORK_TYPE_FEEDS_VALUE) {
				int hostid = Integer.parseInt(hostId);
				logger.debug("Going to retrieve file list from DB");
				List<FileLocationInfo> fileInfoList = netstorageFileInfoDao.getFileLocationInfo(hostid, workType, pageNo, perPage);
				if(fileInfoList!=null)
					this.fileLocationInfoList.addAll(fileInfoList);
				logger.debug("Feeds Found:" + fileInfoList.size());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void beforeJob(JobExecution jobExecution) {
		System.out.println("-----------------------------------------------Befor job");

	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		System.out.println("After finishing of job =======================================================================================");

	}

}
