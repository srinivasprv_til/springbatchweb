package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.joblauncher.OfflineFeedJobLauncher;
import com.times.common.model.UserFeed;
import com.times.common.service.OfflineFeedService;

public class OfflineFeedMergeReader implements ItemReader<UserFeed>, StepExecutionListener{
	
	private final static Logger logger = LoggerFactory.getLogger(OfflineFeedMergeReader.class);
	private List<UserFeed> userFeedList = Collections.synchronizedList(new ArrayList<UserFeed>());

	OfflineFeedService offlineFeedService;
	public OfflineFeedService getOfflineFeedService() {
		return offlineFeedService;
	}
	public void setOfflineFeedService(OfflineFeedService offlineFeedService) {
		this.offlineFeedService = offlineFeedService;
	}




	public UserFeed read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		try {			
			if (userFeedList != null && userFeedList.size() > 0) {
				UserFeed uf = getUserFeed();
				return uf;
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("Error in OFFLINEFEEDMERGEREADER read() : "+e.getMessage());
		}
		return null;

	}

	
	private UserFeed getUserFeed() {
		UserFeed userfeed = null;
		synchronized (this) {
			if (userFeedList != null && userFeedList.size() > 0) {
				userfeed = userFeedList.remove(0);
			}
		}
		return userfeed;
	}




	public void beforeStep(StepExecution stepExecution) {
		try {
			userFeedList = offlineFeedService.getUserFeedList();
		} catch (Exception e) {
			logger.error("Error in OFFLINEFEEDMERGEREADER beforeStep() : "+e.getMessage());
		}
	}
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		return null;
	}






	 

}
