package com.times.mailer.batch.reader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.ibm.icu.util.Calendar;
import com.times.common.service.FileContentServicesImpl;
import com.times.mailer.model.NetstorageInfo;

public class EtMobileAdSenseReader {
	
	private FileContentServicesImpl fileServiceImpl;
	private final static Logger logger = LoggerFactory.getLogger(EtMobileAdSenseReader.class);

	public static void main (String... args) throws Exception{
		uploadFileOnToi();
	}

	public static boolean uploadFileOnToi() throws Exception, UnexpectedInputException,
	ParseException, NonTransientResourceException {

		FTPClient ftpClient=new FTPClient();
		try{
			FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
			ftpClient.configure(conf);
			ftpClient.connect("192.168.37.217");
			String username = "tilguest";
			String password = "guest@79780";
			String directory = "/til/AdControlPanel/mobileads";
			boolean constatus = ftpClient.login(username, password);
			ftpClient.changeWorkingDirectory(directory);

			NetstorageInfo etWriteNetStorageDetails = new NetstorageInfo();

			etWriteNetStorageDetails.setFolderLocation("/93656/configspace/mobileads/");
			etWriteNetStorageDetails.setUserid("mobileftp");
			etWriteNetStorageDetails.setPassword("m0bILeADs87^%");
			etWriteNetStorageDetails.setServerName("et.upload.akamai.com");

			FileContentServicesImpl myclass = new FileContentServicesImpl();

			if (constatus){
				FTPFile [] files = ftpClient.listFiles();

				for (FTPFile file : files){
					String remoteFile1 = file.getName();

					InputStream is = ftpClient.retrieveFileStream(remoteFile1);

					String fileContent = getContentByInputStream(is);

					etWriteNetStorageDetails.setFileName(remoteFile1);

					if (is!=null) {
						try{
							myclass.uploadContentByFTP(etWriteNetStorageDetails, fileContent);
							logger.debug("File --> " + remoteFile1 + " has been uploaded successfully.");
						}
						catch(Exception e){
							logger.debug("Problem in uploading " + remoteFile1);
						}
					}
					else{
						logger.debug("Error in getting Input stream for file  ->> " + remoteFile1);
					}

					ftpClient.completePendingCommand();
				}
				logger.debug("total number of Files Uploaded in ET Mobile AD Sense from directory "+directory+" are " + files.length);
			}
			return true;
		}

		catch (IOException io)
		{
			io.printStackTrace();
		}

		finally{
			if (ftpClient.isConnected()){
				try{
					ftpClient.disconnect();
				}
				catch (IOException io){
					logger.error("error while closing FTP Connection");
				}
				catch (Exception e){
					logger.error("error in copying the ET Modile ad files");
				}
			}
		}
		// TODO Auto-generated method stub
		return false;
	}

	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		logger.debug(" beforeStep method..");

	}

	public FileContentServicesImpl getFileServiceImpl() {
		return fileServiceImpl;
	}

	public void setFileServiceImpl(FileContentServicesImpl fileServiceImpl) {
		this.fileServiceImpl = fileServiceImpl;
	}


	private static String getContentByInputStream(InputStream is)	throws IOException, UnsupportedEncodingException {
		String outData;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();  
		byte[] byteArray = new byte[1024];  
		int count = 0;  
		while ((count = is.read(byteArray, 0, byteArray.length)) > 0) {
			outputStream.write(byteArray, 0, count);
		}
		outData = new String(outputStream.toByteArray(), "UTF-8");

		if (outputStream != null) {
			outputStream.close();
			outputStream = null;
		}
		return outData;
	}

}
