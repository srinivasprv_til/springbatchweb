/**
 * 
 */
package com.times.mailer.batch.reader;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.internet.MimeUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.util.StringUtils;
import com.mongodb.DBObject;
import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
import com.times.common.util.Constant;
import com.times.common.util.DateUtils;
import com.times.common.util.InMemoryData;
import com.times.common.util.MailerUtil;
import com.times.mailer.batch.MailerReader;
import com.times.mailer.dao.CMSDataDao;
import com.times.mailer.dao.CustomParamDao;
import com.times.mailer.dao.TravelNewsletterDao;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetter;
import com.times.mailer.model.NewsLetterDailyTracker;


/**
 * This class is used as a reader for the TravelNewsletter.
 * 
 * @author Ranjeet.Jha
 *
 */
public class TravelNewsletterReader implements ItemReader<MailUser>, StepExecutionListener { //, JobExecutionListener

	//private final static Logger logger = Logger.getLogger(MailerReader.class);
	private final static Logger logger = LoggerFactory.getLogger(MailerReader.class);
	
	private EmailProcessor smtpMailer;

	private Mailer myMailer;
	private TravelNewsletterDao travelNewsletterDao;
	
	private CMSDataDao cmsDataDao;

	//private Boolean isExisting = false;
	private int perPage = 100;
	private int pageNo = 0;
	private String newsLetterId = null;
	private boolean isDaily = true; // if not daily then weekly newsletter.
	private long morningOrEvening = 1; // 1 for morning and 2 for evening.
	private long dailyOrWeekly = 1; //// 1 for daily and 2 for weekly.
	
	private List<MailUser> lstSubscribers = Collections.synchronizedList(new ArrayList<MailUser>());
	
	//code changes ... 
	boolean isInitialized = false;
	private String cacheUIKey = null;
	
	// This travelNewsletterDao interface is used to keep pageNo for the running job with some stuff
	private CustomParamDao customParamDao;
	
	public static final String NL_FORMAT_TEXT = "text";
	public static final String NL_FORMAT_HTML = "html";
	public static final String NL_FORMAT_PARAM = "nlformat";
	public static final long TIME_INTERVAL = 10;
	
	public static final int TOP = 30;
	public static final String HOST = "259";
	public static final int DURATION = 6;
	public static final int URTYPE = 0;
	public static final int LAST_UPDATE_DAYS = 7;
	
	
	/**
	 * This method is used to read the {@link MailUser} object.
	 * 
	 * @param args
	 */
	public MailUser read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		logger.debug("iread method start..");
		
	
		if (myMailer != null) {
			// push MailUser into the List
			
			this.pushSubscribersIntoList(myMailer, perPage, pageNo );
			
		}
		
		// if subscriber got then 
		if (lstSubscribers.size() > 0) {
			//counter update in memory
			MailUser subscriberUser = null;
			subscriberUser = getSubscriber();
			
			return subscriberUser;
		} else {
			//updates stats for the newsmailer ...
			for (Map.Entry<String, Integer> entry : InMemoryData.emailSentCounter.entrySet()) {
				this.updateEmailStats(entry.getKey());
			}
			
			//this.removeInMemoryData();
		}
		
		return null;
	}


	/**
	 * Get the Newsletter user/subscriber, if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private MailUser getSubscriber() {
		MailUser subscriberUser = null;
		synchronized (this) {
			if (lstSubscribers != null && lstSubscribers.size() > 0) {
				subscriberUser = lstSubscribers.remove(0);
				// update counter in map
				this.updateCounterInMap(subscriberUser);
			}
		}
		return subscriberUser;
	}
	
	/**
	 * @param subscriberUser
	 */
	private void updateCounterInMap(MailUser subscriberUser) {
		//logger.debug("updateCounterInMap method ..");
		try {
			int lastCounter = 1;
			if (subscriberUser != null && subscriberUser.getNewsletter() != null) {
				int dailyOrWeekly = subscriberUser.getNewsletter().isDaily() ? 1: 2;
				int morningOrEvening = subscriberUser.getNewsletter().getScheduleTime();
				String sKey = subscriberUser.getNewsletter().getId(); 
				sKey = sKey + Constant.KEY_BUILDER_SEPERATOR + dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + morningOrEvening;
				if ( InMemoryData.emailSentCounter.containsKey(sKey)) {
					lastCounter = InMemoryData.emailSentCounter.get(sKey);
					InMemoryData.emailSentCounter.put(sKey, ++lastCounter);
				} else {
					InMemoryData.emailSentCounter.put(sKey, new Integer(lastCounter));
				}
			}
		} catch (Exception e) {
			logger.error("in side the updateCounterInMap method, msg : " + e.getMessage());
		}
	}

	/**
	 * This method is used to update stats finally for one newsletter.
	 * 
	 * @param sKey
	 */
	private void updateEmailStats(String sKey) {
		int iCount = InMemoryData.emailSentCounter.get(sKey);
		int ireadCount = 0;
		this.getTravelNewsletterDao().addMailerStats(sKey, 0, iCount);
		String code = null;
		if (InMemoryData.keyCodeMap.containsKey(sKey)) {
			code = InMemoryData.keyCodeMap.get(sKey);
		}
		travelNewsletterDao.updateNewsLetterDailyTracker(code, sKey, ireadCount, iCount);
	}

	/**
	 * This method is used to push the subscriber into the List of {@link MailUser}.
	 *  
	 */
	private void pushSubscribersIntoList(Mailer myMailer, int perPage, int pageNo) {
		long dailyOrWeekly = 0;
		if (lstSubscribers == null || lstSubscribers.isEmpty()){
			synchronized (this) {
				this.pageNo = ++pageNo;
				
				try {
					if(myMailer.isDaily()) {
						dailyOrWeekly = 1;
					} else {
						dailyOrWeekly = 2;
					}
					// update the page no for this nlid, dailyWeekly.
					customParamDao.addOrupdatePageNo(myMailer.getId(), dailyOrWeekly, myMailer.getScheduleTime(), pageNo);
					/*Long instanceId = customParamDao.getLastHrFailedJobInstanceId();
					boolean isParamMatched = customParamDao.isFailedJob(myMailer.getId(), dailyOrWeekly, myMailer.getScheduleTime(), instanceId);
					if (isParamMatched) {
						int pgNo = customParamDao.getPageNo(myMailer.getId(), dailyOrWeekly, myMailer.getScheduleTime());
						this.pageNo = ++pgNo;
					}*/
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				logger.debug("NLID : " + myMailer.getId() + " , pageNo : " + pageNo);
				lstSubscribers = travelNewsletterDao.getSubscriber(myMailer, perPage, pageNo);
				
			}
			
		}
	}
	
	
	private Set<Integer> getMostReadArticle() {

		String msids = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		msids = customParamDao.getMostReadMsid(sdf.format(c.getTime()));

		if (!StringUtils.hasText(msids)) {
			StringBuilder sb = new StringBuilder();
			try {
				List<Map<String, Object>> topMsids = cmsDataDao.getMostReadArticalData(HOST, URTYPE, TOP, DURATION);

				if (topMsids != null) {

					Iterator it = topMsids.iterator();
					while (it.hasNext()) {
						Map mp = (Map) it.next();
						sb.append(mp.get("msid") + ",");
					}
				} 
			}catch (Exception e) {
				logger.debug("Error in getMostReadArticle api to pull data ...");
			}
			if (sb != null && sb.length() > 1) {
				msids = sb.toString().substring(0, sb.length()-1);
			} else {
				c.add(Calendar.DAY_OF_YEAR, -1);
				msids = customParamDao.getMostReadMsid(sdf.format(c.getTime()));
			}

			customParamDao.insertMostReadMsid(msids);
		}



		String[] idstr = msids.split("[,]");
		Set<Integer> newMsids = new HashSet<Integer>();
		for (int i = 0; i < idstr.length ; i ++) {
			newMsids.add(Integer.parseInt(idstr[i]));
		}

		Set<Integer> notSentmsid = new HashSet<Integer>();
		Set<Integer> msid = getNewMsidNotFoundInDB(newMsids, LAST_UPDATE_DAYS, notSentmsid);

		for (Integer it :msid) {
			try {
				travelNewsletterDao.updateTravelNLLogCollection(it.toString());
			} catch (Exception e) {				
				e.printStackTrace();
			}
		}

		return msid;

	}
	
	
	
	private Set<Integer> getNewMsidNotFoundInDB(Set<Integer> newMsids, int lastUpdateFrom, Set<Integer> notSentmsid) {

		Calendar c = Calendar.getInstance();

		c.setTime(new Date());		
		c.roll(Calendar.DAY_OF_YEAR, -lastUpdateFrom);

		Date s_Date = DateUtils.addHrMinutesToDate(c.getTime(), 5, 30);


		List<DBObject> dbObjects = null;
		try {
			dbObjects = travelNewsletterDao.getMsidDBObjectEmailSent(s_Date);
		} catch (Exception e) {			
			e.printStackTrace();
		}


		List<Integer> dbMsids = new ArrayList<Integer>();
		for (DBObject dbo : dbObjects) {
			if (dbo.get("MSID") != null) {
				dbMsids.add(Integer.parseInt(dbo.get("MSID").toString()));
			}
		}


		if (newMsids != null && dbObjects != null) {

			if (dbMsids != null && dbMsids.size() > 0) {
				Iterator<Integer> it = newMsids.iterator();
				while(it.hasNext()) {
					Integer i = it.next();	
					if (!dbMsids.contains(i)) {
						notSentmsid.add(i.intValue());
						if (notSentmsid.size() == 5) {
							break;
						}
					}
				} 
			}else  {

				for (Integer it : newMsids) {
					notSentmsid.add(it);
					if (notSentmsid.size() == 5) {
						break;
					}
				}
			}
		} else if (newMsids != null && dbObjects == null) {
			for (Integer it : newMsids) {
				notSentmsid.add(it);
				if (notSentmsid.size() == 5) {
					break;
				}
			}
		}

		if (newMsids.size() > 5 && notSentmsid.size() < 5 && lastUpdateFrom > 3) {
			getNewMsidNotFoundInDB(newMsids, lastUpdateFrom - 1,notSentmsid);
		}

		return notSentmsid;
	}


	/**
	 * This method is used to get the Newsletter masert i.e. Mailer List
	 */
	private List<Mailer> getMailer() {
		List<Mailer> lstMailer = getTravelNewsletterDao().getNewsletterScheduled(); 
		
		return lstMailer;
	}
	
	private List<Mailer> getMailer(String nlId, boolean isDaily) {
		List<Mailer> mailerMaster = travelNewsletterDao.getNewsletterMailer(nlId, isDaily);
		return mailerMaster;
	}
		 
	
	/**
	 * This method is used to add the newsletter Body part in in-memory i.e. collection.
	 * 
	 * @param myMailer
	 */
	private void initializeNewsletter(Mailer myMailer) {
		try {
			
			String newsid = myMailer.getId();
			logger.debug("nl id :" + newsid + " going to pull data ...");
			String cmsMappingId = myMailer.getCmsMappingId();
			int scheduledTime = myMailer.getScheduleTime();
			int daily_weekly = myMailer.isDaily() ? 1 : 2;
			String skey1 = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + scheduledTime ;
			String uniqueCode = addNewsLetterDailyTrackerIfNotfound(myMailer, newsid, scheduledTime, daily_weekly, skey1);
			
			//get unique code... and pass it to template ... hash code (timestamp +skey1)
			String urlNewsletter =  myMailer.getHtmlPath(); //.replace("#$nlcode", uniqueCode); // HTML >>MAIN TEMPLATE :
			String urlNewsletterWidget = myMailer.getTemplatepath();//.replace("#$nlcode",uniqueCode) ; // HTML FRAGMENT
						
			// track the details for each day and generate the code.
			 
			//for html data 
			String scachekey = skey1 +Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_HTML;
			if (!InMemoryData.newsMailerUIMap.containsKey(scachekey)) {
				//code generate newslid ... 
				//	urlNewsletter = urlNewsletter.indexOf("?") != -1 ? urlNewsletter + "?nlformat=" + NL_FORMAT_HTML : urlNewsletter + "&nlformat=" + NL_FORMAT_HTML;
				String url4Html = urlNewsletter.indexOf("?") == -1 ? urlNewsletter +"?" : urlNewsletter + "&";
				url4Html = url4Html  + NL_FORMAT_PARAM + "=" + NL_FORMAT_HTML;
				String sData = MailerUtil.getData(url4Html);
				
				
				if (!StringUtils.hasText(sData)) {
					sData = MailerUtil.getDataRecursively(url4Html, 1);
				}
				if (sData != null) {
					sData = sData.replace("##code##", uniqueCode);					
					InMemoryData.newsMailerUIMap.put(scachekey, sData);
				}
				
				Set<Integer> notSentmsid = getMostReadArticle();
				if (notSentmsid != null && notSentmsid.size() > 0) {
					String body = getEmailBody(myMailer.getTemplatepath(), myMailer, notSentmsid);
					InMemoryData.newsMailerUIMap.put(scachekey, body);
				} else {
					InMemoryData.newsMailerUIMap.put(scachekey, null);
				}
				
				 if (StringUtils.hasText(sData) && ("1031".equals(newsid))) {
					 if (sData.indexOf("<!--###") != -1) {
						 String subjectLine = sData.substring(sData.indexOf("<!--###") + 7, sData.indexOf("###-->"));
						 InMemoryData.newsMailerUIMap.put(skey1 + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1", subjectLine);
					 }
				 }
				// logg id data not found
				logIfDataNotFound(newsid, url4Html, sData);
				
			}
			
			// for text data
			/*String scachekeytext = skey1 + Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_TEXT;
			if (!InMemoryData.newsMailerUIMap.containsKey(scachekeytext)) {
				String url4Text = urlNewsletter.indexOf("?") == -1 ? urlNewsletter + "?nlformat=" + NL_FORMAT_TEXT : urlNewsletter + "&nlformat=" + NL_FORMAT_TEXT;
				String sDataText = MailerUtil.getData(url4Text);
				if (sDataText != null) {
					sDataText = sDataText.replace("##code##", uniqueCode);
					InMemoryData.newsMailerUIMap.put(scachekeytext, sDataText);
				}
			}
			logger.debug(scachekey + "  " + scachekeytext);
			*/
			logger.debug("cacheKey : " + scachekey);
			
			if ("1".equals(myMailer.getPersonalize())) {
				
				String[] arrID = null;
				if (cmsMappingId != null) {
					arrID = cmsMappingId.split("[,]");
				}
				
				for (String id : arrID) {
					logger.debug(id);
					int cmsid = Integer.parseInt(id.trim());
					String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + scheduledTime + Constant.KEY_BUILDER_SEPERATOR + id ;
					if (!InMemoryData.newsMailerDataMap.containsKey(skey+ Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_HTML)) {
						String sURL = urlNewsletterWidget.replaceAll("#msid", Integer.toString(cmsid)) ;
						sURL = sURL.indexOf("?") == -1 ? sURL + "?nlformat=" + NL_FORMAT_HTML : sURL + "&nlformat=" + NL_FORMAT_HTML;
						String sWidgetData = MailerUtil.getData(sURL);
						if (!StringUtils.hasText(sWidgetData)) { // retry once more as some time got null may be due to time out
							sWidgetData = MailerUtil.getData(sURL);
						}
						InMemoryData.newsMailerDataMap.put(skey+ Constant.KEY_BUILDER_SEPERATOR+"html", sWidgetData);
						
						// logg id data not found
						logIfDataNotFound(newsid, sURL, sWidgetData);
						
					}
					
				}
			}
		} catch (Exception e) {
			logger.error("exception occured while retreiving content , msg: " + e.getMessage());
		}
	}

	/**
	 * This method is used to logged the data.
	 * 
	 * @param url4Html
	 * @param sData
	 */
	private void logIfDataNotFound(String nlId, String url4Html, String sData) {
		String msg = null;
		if (sData != null) {
			msg =  "url : " + url4Html + " , len : " + sData.length() + " nlid : " + nlId;
			logger.debug(" url : " + url4Html + " , len : " + sData.length() + " nlid : " + nlId);
			
		} else {
			logger.debug("ALERT ALERT NOT FOUND data for url : " + url4Html + " , nlid : " + nlId);
			try {
				smtpMailer.processStart("DATA NOT FOUND for URL : " + url4Html + " , nlid : " + nlId  + " MSG : " + msg);
			} catch (Exception e) {
				logger.debug("EXception url : " + url4Html + " , nlid : " + nlId + e.getMessage()); ;
			}
		}
	}

	/**
	 * This method is used to add the newsLetterTracker if not found for that days, frequency, time and nlId.
	 * 
	 * @param myMailer
	 * @param newsid
	 * @param iTime
	 * @param itype
	 * @param skey1
	 */
	private String addNewsLetterDailyTrackerIfNotfound(Mailer myMailer, String newsid, int iTime, int itype, String skey1) {
		String uniqueCode= null;
		String yyymmdd = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		DBObject nlTracker = travelNewsletterDao.getNewsLetterDailyTracker(newsid, yyymmdd, String.valueOf(iTime), String.valueOf(itype));
		if (nlTracker == null) {
			uniqueCode = getUniqueCodeByDate(skey1);
			NewsLetterDailyTracker nlDailyTracker = new NewsLetterDailyTracker(new Date(), Integer.parseInt(myMailer.getId()), uniqueCode, iTime, itype, yyymmdd);
			travelNewsletterDao.addNewsLetterDailyTracker(nlDailyTracker); 
			
		} else {
			uniqueCode = String.valueOf(nlTracker.get(NewsLetterDailyTracker.NEWS_LETTER_CODE));
		}
		if (!InMemoryData.keyCodeMap.containsKey(skey1)) {
			InMemoryData.keyCodeMap.put(skey1, uniqueCode);
		}
		
		return uniqueCode;
	}
	
	/**
	 * This method is used to get the StringCode.
	 * @param str
	 * @return
	 */
	private String getUniqueCodeByDate(String key) {
		String date = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		String code = MailerUtil.getUniqueCodeByDate(date + key);
		return code;	
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");
		JobParameters jobParams = stepExecution.getJobParameters();
		this.newsLetterId = jobParams.getString(Constant.JOB_PARAM_NEWSLETTER_ID);
		//long dailyOrWeekly = jobParams.getLong(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY);
		if (Constant.JOB_PARAM_NL_DAILY == jobParams.getLong(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY)){
			this.isDaily = true;
			this.dailyOrWeekly = 1;
		} else {
			this.isDaily = false;
			this.dailyOrWeekly = 2;
		}
		
		//MorningEvening parameter
		//long morningOrEveningKey = jobParams.getLong(Constant.JOB_PARAM_NL_MORNING_EVE_KEY);
		if (Constant.JOB_PARAM_NL_DAILY == jobParams.getLong(Constant.JOB_PARAM_NL_MORNING_EVE_KEY)){
			this.morningOrEvening = 1;
		} else {
			this.morningOrEvening = 2; // for evening 2
		}
		
		// page no is applicable job is going to restart so that it will run from next page where batch running.
		if (jobParams.getString(Constant.JOB_PARAM_NL_RESTART_KEY) != null) {
			int pageNo = (int)jobParams.getLong(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO);
			this.pageNo = ++pageNo;
		} 
		
		//List<Mailer> mailers = travelNewsletterDao.getNewsletterMailer(newsLetterId, isDaily);
		List<Mailer> mailers = this.getMailer(newsLetterId, isDaily);
		if (mailers != null && mailers.size() > 0) {
			this.myMailer = mailers.get(0);
		}
		if (myMailer != null) {
			// get mailer body content into memory
			this.initializeNewsletter(myMailer);
		}
		this.isInitialized = true;
		
		this.cacheUIKey = this.newsLetterId + Constant.KEY_BUILDER_SEPERATOR + this.dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + this.morningOrEvening;
		
		/*if (jobParams != null) {
			for (Map.Entry<String, JobParameter> entry : jobParams.getParameters().entrySet()) {
				logger.debug(entry.getKey() + " : " + entry.getValue().getValue());
			}
		}*/
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		/*JobParameters jobParams = stepExecution.getJobParameters(); 
		if (jobParams != null) {
			for (Map.Entry<String, JobParameter> entry : jobParams.getParameters().entrySet()) {
				logger.debug(entry.getKey() + " : " + entry.getValue().getValue());
			}
		}*/
		reportMailer();
		this.removeInMemoryData();
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		
		return stepExecution.getExitStatus();
	}

	/**
	 * This method is used to remove the in-memory data for the that newsletter.
	 */
	private void removeInMemoryData() {
		//remove memory data for newsletter...
		//logger.debug("clearing newsMailerDataMap newsMailerUIMap emailSentCounter Map ..");
		if (InMemoryData.newsMailerDataMap.containsKey(this.cacheUIKey)){
			InMemoryData.newsMailerDataMap.remove(this.cacheUIKey);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_HTML)){
			InMemoryData.newsMailerUIMap.remove(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_HTML);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_TEXT)){
			InMemoryData.newsMailerUIMap.remove(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_TEXT);
		}
		if (InMemoryData.emailSentCounter.containsKey(this.cacheUIKey)) {
			InMemoryData.emailSentCounter.remove(this.cacheUIKey);
		}
	}

	/**
	 * @return
	 */
	public Mailer getMyMailer() {
		return myMailer;
	}

	/**
	 * @param myMailer
	 */
	public void setMyMailer(Mailer myMailer) {
		this.myMailer = myMailer;
	}

	/**
	 * @param smtpMailer the smtpMailer to set
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}
	/**
	 * @param customParamDao the customParamDao to set
	 */
	public void setCustomParamDao(CustomParamDao customParamDao) {
		this.customParamDao = customParamDao;
	}


	/**
	 * @return the travelNewsletterDao
	 */
	public TravelNewsletterDao getTravelNewsletterDao() {
		return travelNewsletterDao;
	}


	/**
	 * @param travelNewsletterDao the travelNewsletterDao to set
	 */
	public void setTravelNewsletterDao(TravelNewsletterDao travelNewsletterDao) {
		this.travelNewsletterDao = travelNewsletterDao;
	}


	public CMSDataDao getCmsDataDao() {
		return cmsDataDao;
	}


	public void setCmsDataDao(CMSDataDao cmsDataDao) {
		this.cmsDataDao = cmsDataDao;
	}
	
	private String getEmailBody(String templatePath,Mailer myMailer, Set<Integer> notSentmsid) {
		Integer msid;
		String contentPieces;
		if (myMailer != null) {
			int daily_weekly = myMailer.isDaily() ? 1: 2;
			//int morning_evening = news.getScheduleTime();
			int morning_evening = 1;
			String newsid = myMailer.getId();
			String displayformat = NL_FORMAT_HTML;


			String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + morning_evening;
			String sBody = InMemoryData.newsMailerUIMap.get(skey+Constant.KEY_BUILDER_SEPERATOR+displayformat);
			StringBuilder sb = new StringBuilder();
			// code for non personalised newsletter check
			if (sBody != null && notSentmsid != null && notSentmsid.size() > 0) {
				StringBuilder mailBodySB = new StringBuilder();

				int i = 0;
				boolean isDigit = false;
				for (Iterator<Integer> it = notSentmsid.iterator(); it.hasNext();) {
					msid = it.next();
					// get html body part for particular msid.
					String bodyPart = getTemplateDataInHTML(templatePath,msid);
					if (bodyPart != null && StringUtils.hasText(bodyPart)) {
						if (i==0 && bodyPart.indexOf("<!--###") != -1) {						
							String subjectLine = bodyPart.substring(bodyPart.indexOf("<!--###") + 7, bodyPart.indexOf("###-->"));
							isDigit = Character.isDigit(subjectLine.charAt(0));
							if (isDigit) {
								isDigit = true;
							}
							InMemoryData.newsMailerUIMap.put(skey + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1", subjectLine);						
						} else if (!isDigit) {
							if (bodyPart.indexOf("<!--###") != -1) {
								String subjectLine = bodyPart.substring(bodyPart.indexOf("<!--###") + 7, bodyPart.indexOf("###-->"));
								isDigit = Character.isDigit(subjectLine.charAt(0));
								if (isDigit) {
									InMemoryData.newsMailerUIMap.put(skey + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1", subjectLine);
								}
							}
						}
						i++;

						if (bodyPart != null) {
							mailBodySB.append(bodyPart);
						}
						sb.append(msid + ",");
					}
				}
				contentPieces = mailBodySB.toString();

				if (StringUtils.hasText(contentPieces)) {

					contentPieces = mailBodySB.toString();
					if (StringUtils.hasText(contentPieces)) {

						if (sBody != null) {
							sBody = sBody.replace("#articlebody#", contentPieces);
							if (sb != null && sb.length() > 1) {
								String msids = sb.toString().substring(0, sb.length()-1);
								sBody = sBody.replace("#msid_str#", msids);
							}
							
						}
					}

				}
			}
			return sBody;
		}
		return null;

	}
	
	private String getTemplateDataInHTML(String url, Integer msid) {

		if(StringUtils.hasText(InMemoryData.contentMap.get(msid))) {
			return InMemoryData.contentMap.get(msid);
		} else {

			url = url.replace("#msid", msid.toString());
			String content = MailerUtil.postData(url);
			if (StringUtils.hasText(content)) {
				InMemoryData.contentMap.put(msid, content);
				return content;			
			} else {
				return null;
			}
		}

	}
	
	private void reportMailer() {
		String newsid = myMailer.getId();

		int scheduledTime = myMailer.getScheduleTime();
		int daily_weekly = myMailer.isDaily() ? 1 : 2;

		String skey1 = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + scheduledTime ;
		String subjectTitle = InMemoryData.newsMailerUIMap.get(skey1 + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1");

		String scachekey = skey1 +Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_HTML;

		EmailVO vo = new EmailVO();
		vo.setBody(InMemoryData.newsMailerUIMap.get(scachekey));

		String prefix = "[NLID : " + newsid + ",DAILY_WEEKLY : " + daily_weekly +  ",Subscribers : "  + InMemoryData.emailSentCounter.get(skey1) + "]";

		try {
			if (StringUtils.hasText(subjectTitle)) {
				vo.setSubject(prefix + encodeText(myMailer.getMailSubject() + ": " + subjectTitle, myMailer.getMailSubject()));

			} else {
				vo.setSubject(prefix + encodeText(myMailer.getMailSubject(), myMailer.getMailSubject()));
			}
			vo.setRecipients("toi.daily.newsletter@gmail.com");

			smtpMailer.sendEMail(vo);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String encodeText(String subject, String baseSubject ) throws UnsupportedEncodingException {
		String encodedSubject = null;
		try {
			encodedSubject = MimeUtility.encodeText(subject);
			if (encodedSubject.indexOf("?Q?") != -1 || encodedSubject.indexOf("?B?")!= -1) {
				return baseSubject;
			}
		} catch (UnsupportedEncodingException e) {
			return baseSubject;
		}
		return encodedSubject;
	}

	
}