package com.times.mailer.batch.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.InMemoryData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

/**
 * 
 * @author praveen.bokka
 *
 */
public class ETEpaperCMSDataReader implements ItemReader<Document>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(ETEpaperCMSDataReader.class);
	
	private Date epaperJobDate = null;
	
	public Document read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		logger.debug("etEpaperCMS Data read method start..");
		
//		Date today = new Date();
//		
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
//		if(!sdf.format(today).equals(sdf.format(epaperJobDate))) {
//			logger.info("Epaper not yet updated. Will try after it is updated...");
//			return null;
//		}
		
		String solrURL = "http://jcmssolr.indiatimes.com/solr/ET/select?df=hostid&q=153&rows=10&wt=xslt&tr=custom.xsl&sort=updateDate+asc&fq=contenttypeid:2%20AND%20contentsubtypeid:0%20AND%20updateDate:";//[2016-03-29T18:30:00.000Z%20TO%202016-03-30T18:30:00.000Z]";
		//String solrURL = "http://solrsearch.indiatimes.com/TOISolrWebProject/ET/select?df=hostid&q=153%20AND%20msid:[##MSID%20TO%20*]&rows=10&wt=xslt&tr=custom.xsl&sort=updateDate+asc&fq=contenttypeid:2%20AND%20contentsubtypeid:0%20AND%20updateDate:";//[2016-03-29T18:30:00.000Z%20TO%202016-03-30T18:30:00.000Z]";
		//String solrURL = "http://solrsearch.indiatimes.com/TOISolrWebProject/ET/select?df=hostid&q=153%20AND%20msid:{51838518%20TO%20*}&rows=10&wt=xslt&tr=custom.xsl&sort=updateDate+asc&fq=contenttypeid:2%20AND%20contentsubtypeid:0%20AND%20updateDate:[2016-04-15T07:14:56Z%20TO%20NOW]";
		//Date now = new Date();
		solrURL = solrURL.replace("##MSID", Integer.toString(InMemoryData.etEpaperMsid));
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		solrURL = solrURL + "["+ f.format(InMemoryData.etEpaperDate) + "%20TO%20NOW]";
		//solrURL = URLEncoder.encode(solrURL, "UTF-8"); 
		
		StringBuffer responseBuffer = getExternalData(solrURL);
		System.out.println("got response" +solrURL);
		
		if(responseBuffer == null){
			logger.error("Solr Response from CMS empty");
			return null;
		}
		
        SAXBuilder saxBuilder = new SAXBuilder();
        Document document = null;
        try {
        	document = saxBuilder.build(new StringReader(responseBuffer.toString()));
        } catch(Exception e) {
        	logger.error("Exception while reading XML:"+e);
        }
		
		if(document == null) {
			logger.error("Unable to parse SOlr response from CMS");
			return null;
		}
		
		Element rootElem = document.getRootElement();
		String number = rootElem.getChild("Header").getChildText("numFound");
		if(number.equals("0")) {
			logger.debug("CMS Solr returned zero results. exiting batch.....");
			return null;
		}
		if(number.equals("1")) {
			Element doc = rootElem.getChild("Documents").getChild("Document");
			String id = doc.getChildText("msid");
			if(id.equals(Integer.toString(InMemoryData.etEpaperMsid))) {
				logger.debug("CMS Solr gave duplicate msid. exiting batch....."+ id);
				return null;
			}
		}
			
		logger.debug("etEpaperCMS Data read method start..");
		return document;
		
	}
	
	private static StringBuffer getExternalData(String GET_URL)
			throws IOException, ClientProtocolException {
		HttpClient  httpClient = HttpClients.custom().build();
		HttpResponse response = httpClient.execute(new HttpGet(GET_URL));
		BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String inputLine;
        StringBuffer respBuffer = new StringBuffer();
        while ((inputLine = reader.readLine()) != null) {
        	respBuffer.append(inputLine);
        }
        reader.close();
		return respBuffer;
	}


	public void beforeStep(StepExecution stepExecution) {		
		logger.debug(" beforeStep method..");
		
		//epaperjob status
		DBObject dbQuery = new BasicDBObject();
		dbQuery.put("type", "epaperJobStatus");
		DBObject result = MongoRestAPIService.get("ETEpaperStatus", dbQuery);
		String dateStr = (String) result.get("lastDate");
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			epaperJobDate = f.parse(dateStr);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//This job status
		dbQuery = new BasicDBObject();
		dbQuery.put("type", "status");
		result = MongoRestAPIService.get("ETEpaperStatus", dbQuery);
		dateStr = (String) result.get("lastDate");
//		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
//		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			InMemoryData.etEpaperDate = f.parse(dateStr);
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//(Date) result.get("lastDate");
		InMemoryData.etEpaperMsid = (int) result.get("lastMsid");
		logger.debug(" before step method exiting");
	}

	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		DBObject dbQuery = new BasicDBObject();
		dbQuery.put("type", "status");
		DBObject insertDoc = new BasicDBObject();
		insertDoc.put("type", "status");
		insertDoc.put("lastDate", f.format(InMemoryData.etEpaperDate));
		insertDoc.put("lastMsid", InMemoryData.etEpaperMsid);
		MongoRestAPIService.update("ETEpaperStatus",dbQuery, insertDoc,true);
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		return stepExecution.getExitStatus();
	}

}
