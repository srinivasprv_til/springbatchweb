package com.times.mailer.batch.reader;

import static com.times.mailer.constant.GadgetConstant.GN_HOSTID;
import static com.times.mailer.constant.GadgetConstant.PRIMARY_SOURCE;
import static com.times.mailer.constant.GadgetConstant.PRODUCT_DETAIL;
import static com.times.mailer.constant.GadgetConstant.Category.categoryMap;

import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.util.StringUtils;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.times.common.constant.AmazonItemFields;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
import com.times.common.service.FileContentServices;
import com.times.common.util.GadgetUtil;
import com.times.common.util.InMemoryData;
import com.times.mailer.constant.GadgetConstant;
import com.times.mailer.model.TechGadgetProductDetail;

/**
 * This class reads the Gadgets list for all brands by using 91Mobiles API.
 * It then saves all the brand list in 'gadgetBrandList'.
 * All Products list for all brands are saved in 'brandProductsList' by creating the model of 'TechGadgetProductDetail'.
 * 
 * Objects are transferred to writer one by one from 'brandProductsList' which is of type 'TechGadgetProductDetail'.
 * 
 * @author Anurag.Singhal
 *
 */
public class TechGadgetProductReaderNew implements ItemReader<TechGadgetProductDetail>, StepExecutionListener {
	
	// -----------------------------------------------------------------------------------------------
	// Start Private members
		
	private static final Logger logger = LoggerFactory.getLogger(TechGadgetProductReaderNew.class);
	
//	public Map<Integer, List<String>> gadgetBrandMap = new HashMap<Integer, List<String>>();
	
	public List<TechGadgetProductDetail> brandProductsList = new ArrayList<TechGadgetProductDetail>();
	
	private long startTime = System.currentTimeMillis();
	
	private FileContentServices fileContentServices;
	
	private EmailProcessor smtpMailer;
	
	private Map<Integer, String> statusMap;
	
//	private static final String BRAND_NAME = "##BRAND_NAME";
	private static final String PRODUCT_ID = "##PRODUCT_ID";
	private static final String CATEGORY = "##CATEGORY";
	private static final String START = "##START";
	private static final int LIMIT = 50;
	
	
	//Sample URLs
	//http://api1.91mobiles.com/nm/api/app/brand/all/553
	//http://api1.91mobiles.com/nm/api/product/list/553
	//http://api1.91mobiles.com/nm/api/product/25106/553
	
	private static final String CLIENT_TOKEN="clientId=23134";
	private static final String BRAND_LIST_URL = "http://api1.91mobiles.com/nm/api/app/brand/all/" + CATEGORY+"?"+CLIENT_TOKEN;
	private static final String PRODUCT_LIST_FOR_BRAND_URL = "http://api1.91mobiles.com/nm/api/product/list/" + CATEGORY + "?startRow=" + START + "&limit=" + LIMIT+"&"+CLIENT_TOKEN;
	//URL for Fetching Product Detail from Primary Source
	private static final String PRODUCT_DETAIL_URL = "http://api1.91mobiles.com/nm/api/product/" + PRODUCT_ID + "/" + CATEGORY+"?"+CLIENT_TOKEN;
	private static final String SHOPPING_DETAIL_URL="http://shop.gadgetsnow.com/api/productDetail.php?productId=#productId#&format=json";
	
	// End Private members
	// -----------------------------------------------------------------------------------------------
	
	
	// -----------------------------------------------------------------------------------------------
	// Start public methods

	
	public FileContentServices getFileContentServices() {
		return fileContentServices;
	}

	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}
	
	/**
	 * @param smtpMailer the smtpMailer to set
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}
	
	
	public TechGadgetProductDetail read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		if (this.brandProductsList.size() > 0) {
			TechGadgetProductDetail ProductDetail = getProductDetail();
			return ProductDetail;
		} 
		return null;
	}
	
	
	public void beforeStep(StepExecution stepExecution) {
		
		logger.debug(" Before Step method..");
		
		InMemoryData.alertList = new ArrayList<DBObject>();
		InMemoryData.brandAlertList = new ArrayList<DBObject>();
		
		startTime = System.currentTimeMillis();		
		
//		if(gadgetBrandMap.isEmpty())
//			initializeBrandMap();
		
		if(brandProductsList.isEmpty())
			initializeProductList();
		
//		Comparator<TechGadgetProductDetail> commonNameCmp = Comparator.nullsLast(Comparator.comparing(TechGadgetProductDetail::getCommonName));
		
		Comparator<TechGadgetProductDetail> cmpGadget = Comparator.comparing(TechGadgetProductDetail::getCategory)
				.thenComparing(TechGadgetProductDetail::getDiscontinued)//First 'non-discontinued' gadgets should come which will decide the primary gadget with highest MRP
				.thenComparing(Comparator.comparing(TechGadgetProductDetail::getCommonName, Comparator.nullsLast(Comparator.naturalOrder())))
				.thenComparing(TechGadgetProductDetail::getPrice, Comparator.reverseOrder())
				.thenComparing(TechGadgetProductDetail::getProductName)
				.thenComparing(TechGadgetProductDetail::getAnnouncedDate, Comparator.nullsLast(Comparator.reverseOrder()));
//		System.out.println(brandProductsList);
		Collections.sort(brandProductsList, cmpGadget);
		
		Map<String, String> unameMap = new HashMap<String, String>();
		for(TechGadgetProductDetail model : brandProductsList) {
			if(model.getVariants() != null && model.getVariants().length > 0)
				unameMap.put(model.getProductId(), GadgetUtil.getUName(model.getProductName()));
		}
		
		
		//CONVERT VARIANTS WHICH ARE GIVEN AS THE PRODUCT ID's BY 91 Mobile to Uname's
		for(TechGadgetProductDetail model : brandProductsList) {
			if(model.getVariants() != null && model.getVariants().length > 0) {
				for(int i = 0; i < model.getVariants().length; i++) {
					if(unameMap.containsKey(model.getVariants()[i])) {
						model.getVariants()[i] = unameMap.get(model.getVariants()[i]);
					}
					else {
						logger.error("VARIENTS NOT FOUND in DATA for model : {}, i:{}", model, i);
					}
				}
			}
		}
		
		logger.debug("Total Initialization time for Brand and Product Listing: " + ((System.currentTimeMillis() - startTime)/1000) + " second");		
	}

	
	public ExitStatus afterStep(StepExecution stepExecution) {
		
		logger.info("Going to update Amazon Data in After Step");
		updateAmazonData();
		updateShoppingDetails();
		
		logger.debug("After Step method..");
		
		logger.debug("After Step status : " + stepExecution.getExitStatus().toString());
		
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}
		
		statusMap = new HashMap<Integer, String>();
		statusMap.put(0, "INACTIVE");
		statusMap.put(1, "PENDING");
		statusMap.put(2, "ACTIVE");
		
		//Sending mail here
		EmailVO vo = generateMailObject();
		try {
			if(vo.getBody() != null && !"".equals(vo.getBody()))
				smtpMailer.sendEMail(vo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error while sending mail..", e);
		}
		
		return stepExecution.getExitStatus();
	}
	
	
	// End public methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start private methods
	
	
	private EmailVO generateMailObject() {
		EmailVO vo = new EmailVO();
		vo.setSender("gadgetsnow@timesinternet.in");
		vo.setRecipients(
				"abhishek.kumar@timesinternet.in,"
				+"Meenhaz.ahmed@timesinternet.in,"
				+"prateek.chouhan@timesinternet.in,daksh.verma@timesinternet.in,sweta.verma@timesinternet.in");

		vo.setSubject("New gadgets added in GN");

		String body = "";
		
		try {
			//REFRESH SITEMAP 

			URL url = new URL("http://www.gadgetsnow.com/sitemap/gadgets?upcache=2");
			   HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			   connection.setRequestMethod("GET"); 
			   connection.connect();
			   int code = connection.getResponseCode();
		} 
		catch (Exception e) { 
			logger.error("Failed to refresh sitemap for gadgets",e);
		} 

		if (InMemoryData.brandAlertList != null && !InMemoryData.brandAlertList.isEmpty()) {
			 body += "Brands Added by 91 Mobile: <br><br>";

			body += "Following Brands have been added and activated in CMS."
					+ " Please write a description for this brand and share with: <br><br> "
					+ "Meenhaz.ahmed@timesinternet.in <br><br>" + "Abhishek.kumar@timesinternet.in <br><br>";

			for (DBObject obj : InMemoryData.brandAlertList) {

				body += "Brand name:" + obj.get("name") + " | Brand category:" + obj.get("category") + " | Status:"
						+ statusMap.get(Integer.parseInt(String.valueOf(obj.get("status")))) + "<br><br>";

			}
		}
		String category ="";
		if (InMemoryData.alertList != null && !InMemoryData.alertList.isEmpty()) {

			body += "Following Gadgets have been added and activated in CMS. Please review these and edit incase its critical: <br><br>";

			for (DBObject obj : InMemoryData.alertList) {
				
				try{
					category = (String) obj.get("category");
				}
				catch(Exception e){
					logger.error("no category found! ");
				}
				if("mobile".equalsIgnoreCase(category)){
				body += "Gadget name:" + obj.get("name") + " | Category:" + obj.get("category") + " | Gadget MSID:"
						+ getParameterFromReviewList((List<DBObject>) obj.get("reviewid"), "msid") + " | Status:"
						+ statusMap.get(Integer.parseInt(String.valueOf(obj.get("status")))) + " | To edit"
						+ "<a href=http://gadget.indiatimes.com/gadgetedit.cms?productid=" + obj.get("uname")
						+ ">  click here</a>" + " | To compare" + "<a href=http://gadget.indiatimes.com/gadget_compare.cms?"+"gadget=" + obj.get("uname") +"&"+ "category="+obj.get("category") + ">  clicking here</a>" + "<br><br>";
			     }
				else{
					body += "Gadget name:" + obj.get("name") + " | Category:" + obj.get("category") + " | Gadget MSID:"
							+ getParameterFromReviewList((List<DBObject>) obj.get("reviewid"), "msid") + " | Status:"
							+ statusMap.get(Integer.parseInt(String.valueOf(obj.get("status")))) + "<br><br>";
				}
			}
			body += "As next step, please do the following :<br> Review & Rate the device <br>  Generate comparison URLs by <a href=http://gadget.indiatimes.com/gadget_compare.cms"
					+ ">  clicking here</a>" + "<br><br>";

		}
		InMemoryData.alertList = null;
		InMemoryData.brandAlertList = null;
		vo.setBody(body);
		return vo;
	}
	

	/**
	 * Update Amazon affiliated data embedded in the documents, both related and exact
	 * It is done category wise just to get rid of read timeout type issues
	 */
	private void updateAmazonData(){
		logger.info("Going to update Amazon Embedded Documents");
		Collection <String> categoryList= categoryMap.values();
		
		if (categoryList!=null) {
			for (String category : categoryList) {
				logger.info("Going to update Amazon Embedded Documents for category= "+category);

				Set<String> relatedAsinSet = MongoRestAPIService.getDistinct(PRODUCT_DETAIL,
						AmazonItemFields.AMAZON_SIMILAR_FROM_SEARCH + "." + AmazonItemFields.DB_ASIN,
						new BasicDBObject().append(GadgetConstant.CATEGORY, category));

				if (relatedAsinSet!=null) {
					updateAmazonItems(relatedAsinSet, AmazonItemFields.AMAZON_SIMILAR_FROM_SEARCH);
				}else{
					logger.info("No Related ASINS to update for category"+category);
				}
				
				Set<String> exactAsinList = MongoRestAPIService.getDistinct(PRODUCT_DETAIL,
						AmazonItemFields.AMAZON_EXACT_FROM_DUMP + "." + AmazonItemFields.DB_ASIN,
						new BasicDBObject().append(GadgetConstant.CATEGORY, category));
				
				if (exactAsinList!=null) {
					updateAmazonItems(exactAsinList, AmazonItemFields.AMAZON_EXACT_FROM_DUMP);
				}else{
					logger.info("No Related ASINS to update for category"+category);
				}

				logger.info("Update of  Amazon Embedded Documents for category= "+category+"completed successfully");

			} 
		}
		
	}
	
	/**
	 * Iterates over each asins and updates it {@link #updateSingleASINAcrossTheDocuments(DBObject, String)}
	 * @param asinSet Set of ASIN Id's
	 * @param field   Field name as in amazon_exact or amazon_related
	 */
	private void updateAmazonItems(Set<String> asinSet, String field){
		if(asinSet!=null && StringUtils.hasText(field)){
			for (String asin:asinSet){
				DBObject itemObject= getAmazonItemFromDB(asin);
				if(itemObject!=null){
					updateSingleASINAcrossTheDocuments(itemObject,field);
				}
			}
		}
		
	}
	
	
	/**
	 * @param asin Unique Id on which products are saved in amazon_product_detail
	 * @return DBObject based on the @param asin, null if not found
	 */
	private DBObject getAmazonItemFromDB(String asin) {
		if (StringUtils.hasText(asin)) {
			DBObject query = new BasicDBObject();
			query.put(AmazonItemFields.DB_ASIN, asin);
			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("key",
					new BasicDBObject("_id", 0).append(AmazonItemFields.DB_NAME, 1).append(AmazonItemFields.DB_ASIN, 1)
							.append(AmazonItemFields.DB_DETAIL_PAGE_URL, 1)
							.append(AmazonItemFields.DB_LOWEST_NEW_PRICE, 1)
							.append(AmazonItemFields.DB_LIST_PRICE, 1)
							.append(AmazonItemFields.DB_IS_PRIME, 1)
							.append(AmazonItemFields.DB_IS_PRIME, 1)
							.append(AmazonItemFields.DB_UPDATED_AT, 1));
			
			List<DBObject> amazonItems = MongoRestAPIService.getList(GadgetConstant.AMAZON_PRODUCT_DETAIL, query,
					optionMap);
			if (amazonItems != null && amazonItems.size() > 0 && !amazonItems.get(0).equals(new BasicDBObject())) {
				return amazonItems.get(0);
			}else{
				logger.error("No Entity found for ASIN = "+asin);
			}
		}
		
		return null;
	}
	
	
	/** 
	 * As many different products may have same related product, Hence same ASIN may repeat across the products, This function
	 * updates that ASIN in all the products
	 * @param amazonItemDBObject  AmazonItem DBObject
	 * @param fieldName  field name as in amazon_exact or amazon_related
	 */
	private  void updateSingleASINAcrossTheDocuments(DBObject amazonItemDBObject, String fieldName ){
		
		if (StringUtils.hasText(fieldName) && amazonItemDBObject!=null) {
			String asin = String.valueOf(amazonItemDBObject.get(AmazonItemFields.DB_ASIN));
			DBObject asinQuery = new BasicDBObject().append(fieldName + "."+AmazonItemFields.DB_ASIN, asin);
			DBObject setObject = getUpdateObjectForASIN(amazonItemDBObject, fieldName);
			if (setObject!=null) {
				MongoRestAPIService.updateWithPost(PRODUCT_DETAIL, asinQuery,
						new BasicDBObject().append("$set", setObject));
			}
		}
	}
	
	
	/**
	 * 
	 * @param amazonItemDBObject
	 * @param fieldName DBObject depends on field firstly because of name and secondly because amazon_exact is embedded documents
	 * 		  while amazon_related is an array of embedded documents, So the connector to be used is different in both cases, In
	 * 		  embedded doc case "." is used while in case of array positional operator ".$." has to be used
	 * @return DBobject to be used to update the field in mongoDB
	 */
	private DBObject getUpdateObjectForASIN(DBObject amazonItemDBObject,String fieldName){
		
		 String _CONNECTOR_=".$.";
		
		if (amazonItemDBObject!=null && StringUtils.hasText(fieldName)) {
			
			if(AmazonItemFields.AMAZON_EXACT_FROM_DUMP.equals(fieldName)){
				_CONNECTOR_=".";
			}
			
			DBObject setObject = new BasicDBObject();
			if (amazonItemDBObject.get(AmazonItemFields.DB_NAME) != null) {
				setObject.put(fieldName + _CONNECTOR_ + AmazonItemFields.DB_NAME, amazonItemDBObject.get(AmazonItemFields.DB_NAME));
			}
			if (amazonItemDBObject.get(AmazonItemFields.DB_LOWEST_NEW_PRICE) != null) {
				setObject.put(fieldName + _CONNECTOR_ + AmazonItemFields.DB_LOWEST_NEW_PRICE,
						amazonItemDBObject.get(AmazonItemFields.DB_LOWEST_NEW_PRICE));
			}
			if (amazonItemDBObject.get(AmazonItemFields.DB_LIST_PRICE) != null) {
				setObject.put(fieldName + _CONNECTOR_ + AmazonItemFields.DB_LIST_PRICE,
						amazonItemDBObject.get(AmazonItemFields.DB_LIST_PRICE));
			}
			if (amazonItemDBObject.get(AmazonItemFields.DB_DETAIL_PAGE_URL) != null) {
				setObject.put(fieldName + _CONNECTOR_ + AmazonItemFields.DB_DETAIL_PAGE_URL,
						amazonItemDBObject.get(AmazonItemFields.DB_DETAIL_PAGE_URL));
			}
			if (amazonItemDBObject.get(AmazonItemFields.DB_UPDATED_AT) != null) {
				setObject.put(fieldName + _CONNECTOR_ + AmazonItemFields.DB_UPDATED_AT,
						amazonItemDBObject.get(AmazonItemFields.DB_UPDATED_AT));
			} 
			return setObject;
		}
		
		return null;	
	}

	private void updateShoppingDetails(){
		
		logger.info("Going to update shopping details for gadgets");
		
		Set<String> shoppingIDs= getDisctintShoppingIDs();
		
		if(shoppingIDs!=null){
			for (String shoppingID : shoppingIDs){
				updateSingleShoppingID(shoppingID);
			}
		}else{
			logger.error("Empty/Null shopping IDs received, Reason: Socket/Read Timeout");
		}
		
	}
	
	private Set<String> getDisctintShoppingIDs(){
		
		DBObject query= new BasicDBObject();
		String fieldName=GadgetConstant.SHOPPING_ID;
		Set<String> shoppingIDSet= MongoRestAPIService.getDistinct(PRODUCT_DETAIL, fieldName, query);
		return shoppingIDSet;	
	}
	
	private void updateSingleShoppingID(String shoppingID){
		
		if(StringUtils.hasText(shoppingID)){
			DBObject shoppingDetails= getShoppingIDDetails(shoppingID);
			if(shoppingDetails!=null){
				
					DBObject query = new BasicDBObject().append(GadgetConstant.SHOPPING_ID, shoppingID);
					DBObject setObject=new BasicDBObject().append(GadgetConstant.SHOPPING_DATA, shoppingDetails);
					MongoRestAPIService.updateWithPost(PRODUCT_DETAIL, query, new BasicDBObject().
							append("$set", setObject));
			}
		}else{
			logger.error("Malformed shoppingId Received"+String.valueOf(shoppingID));
		}
	}
	
	private DBObject getShoppingIDDetails(String shoppingID){
		
		DBObject itemsObject=null;
		if(StringUtils.hasText(shoppingID)){
			
			String url=SHOPPING_DETAIL_URL.replace("#productId#", shoppingID);
			String shoppingDetailStr=null;
			BasicDBList itemsList= new BasicDBList();
			
			try {
				shoppingDetailStr=fileContentServices.getExtData(url);
			} catch (Exception e) {
				logger.error("Exception while getting data fromShopping API for shoppingID= "+shoppingID,e);
			}
			
			List<DBObject> shoppingDetailsList=null;
			if(StringUtils.hasText(shoppingDetailStr)){
				Object dbObjectRes=JSON.parse(shoppingDetailStr);
				if (dbObjectRes instanceof List<?>) {
					shoppingDetailsList= (List<DBObject>) dbObjectRes;
				}
			}
			
			List<DBObject> shoppingObjectsList= new ArrayList<>();
			if(shoppingDetailsList!=null){
				for(DBObject dbObject: shoppingDetailsList){
					shoppingObjectsList.add(getFormattedDBObject(dbObject));
				}
			}
			
			if(shoppingObjectsList.size()>0){
				itemsObject=new BasicDBObject().append(GadgetConstant.SHOPPING_ITEM, shoppingObjectsList);
			}
		}
		
		return itemsObject;
	}

	private DBObject getFormattedDBObject(DBObject shoppingDBObject){
		if(shoppingDBObject!=null){
			DBObject formattedDBObject=new BasicDBObject();
			
			if(shoppingDBObject.get(GadgetConstant.ShoppingFields.PRODUCT_ID)!=null){
				formattedDBObject.put(GadgetConstant.ShoppingFields.PRODUCT_ID, shoppingDBObject.get(GadgetConstant.ShoppingFields.PRODUCT_ID));
			}
			
			if(shoppingDBObject.get(GadgetConstant.ShoppingFields.PRODUCT_NAME)!=null){
				formattedDBObject.put(GadgetConstant.ShoppingFields.PRODUCT_NAME, shoppingDBObject.get(GadgetConstant.ShoppingFields.PRODUCT_NAME));
			}
			
			if(shoppingDBObject.get(GadgetConstant.ShoppingFields.MRP)!=null){
				formattedDBObject.put(GadgetConstant.ShoppingFields.MRP, shoppingDBObject.get(GadgetConstant.ShoppingFields.MRP));
			}
			
			if(shoppingDBObject.get(GadgetConstant.ShoppingFields.SELLING_PRICE)!=null){
				formattedDBObject.put(GadgetConstant.ShoppingFields.SELLING_PRICE, shoppingDBObject.get(GadgetConstant.ShoppingFields.SELLING_PRICE));
			}
			
			if(shoppingDBObject.get(GadgetConstant.ShoppingFields.SHIPPING_PRICE)!=null){
				formattedDBObject.put(GadgetConstant.ShoppingFields.SHIPPING_PRICE, shoppingDBObject.get(GadgetConstant.ShoppingFields.SHIPPING_PRICE));
			}
			
			if(shoppingDBObject.get(GadgetConstant.ShoppingFields.DISCOUNT)!=null){
				formattedDBObject.put(GadgetConstant.ShoppingFields.DISCOUNT, shoppingDBObject.get(GadgetConstant.ShoppingFields.DISCOUNT));
			}
			
			
			if(shoppingDBObject.get(GadgetConstant.ShoppingFields.AVAILABILITY)!=null){
				formattedDBObject.put(GadgetConstant.ShoppingFields.AVAILABILITY, shoppingDBObject.get(GadgetConstant.ShoppingFields.AVAILABILITY));
			}
			
			if(shoppingDBObject.get(GadgetConstant.ShoppingFields.COLOR_HEXCODE)!=null){
				formattedDBObject.put(GadgetConstant.ShoppingFields.COLOR_HEXCODE, shoppingDBObject.get(GadgetConstant.ShoppingFields.COLOR_HEXCODE));
			}
			
			if(shoppingDBObject.get(GadgetConstant.ShoppingFields.COLOR)!=null){
				formattedDBObject.put(GadgetConstant.ShoppingFields.COLOR, shoppingDBObject.get(GadgetConstant.ShoppingFields.COLOR));
			}
			return formattedDBObject;
		}
		
		return null;
		
	}
	private TechGadgetProductDetail getProductDetail() {

		TechGadgetProductDetail productDetail = new TechGadgetProductDetail();
	
		synchronized (this) {
			if (brandProductsList.size() > 0) {
				productDetail = this.brandProductsList.remove(0);
				return productDetail;
			}
		}
		
		return productDetail;
	}

	/*private void initializeBrandMap() {
		
		if(categoryMap != null && !categoryMap.isEmpty()) {
			for(int category : categoryMap.keySet()) {
				addBrandsList(category, BRAND_LIST_URL);
			}
		}
	}
	
	
	private void addBrandsList(int category, String brandListUrl) {

		try {
			String brandList = getFileContentServices().getContentByHTTP(brandListUrl, 5000, 20000);
			
			if(StringUtils.hasText(brandList)) {
				Document brandsDoc = XMLReader.loadXml(brandList);
				if(brandsDoc != null) {
					List<Element> brandElementLst = brandsDoc.getRootElement().getChildren("brand");
					if(brandElementLst != null && brandElementLst.size() > 0) {
						List<String> gadgetBrandList = new ArrayList<String>();
						for(Element brandEle : brandElementLst) {
							gadgetBrandList.add(brandEle.getText());
						}
						
						gadgetBrandMap.put(category, gadgetBrandList);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error while fetching TechGadget BrandList for category:{}, URL:{} ", category, brandListUrl, e.getMessage());
		}
	}*/


	private void initializeProductList() {
		
		if(categoryMap != null && !categoryMap.isEmpty()) {
			for(String category : categoryMap.keySet()) {
         		addProductList(category, PRODUCT_LIST_FOR_BRAND_URL);
			}
		}
	}
	
	
	private void addProductList(String category, String productListUrl) {
		
		int blankDataCount = 0;
		String url;
		for(int start=0 ; ; start += LIMIT) {
			
			if(blankDataCount >= 4)
				break;
			
			url = productListUrl.replace(CATEGORY, category).replace(START, String.valueOf(start));
			
			try {
				String productData = getFileContentServices().getContentByHTTP(url, 10000, 30000);
				
				if(StringUtils.hasText(productData)) {
					
					DBObject dbObject = (DBObject) JSON.parse(productData);
					if(dbObject == null || !"200".equals(getStringValue(dbObject.get("statusCode")))) {
						blankDataCount++;
						logger.error("Invalid Status Code while fetching TechGadget ProductList for URL: {}", url);
						continue;
					}
					
					
					dbObject = (DBObject) dbObject.get("results");
					if(dbObject == null || !"false".equals(getStringValue(dbObject.get("exception")))) {
						blankDataCount++;
						logger.error("Exception while fetching TechGadget ProductList for URL: {}, with msgs: {}", url, dbObject.get("msgs"));
						continue;
					}
					
					List<DBObject> productList = (List<DBObject>) dbObject.get("products");
					if(productList == null || productList.isEmpty()) {
						blankDataCount++;
						logger.error("No Product Found while fetching TechGadget ProductList for URL: {}, with msgs: {}", url, dbObject.get("msgs"));
						continue;
					}
					
					
					//Info Msg
					logger.info("Category: {}, URL: {}, ListSize: {}", category, url, productList.size());
					
					for(DBObject product : productList) {
						TechGadgetProductDetail model = getTechGadgetProductDetail(category, product);
						if(model != null)
							brandProductsList.add(model);
						
//						return;
					}
//		        return;
					
				} else {
					blankDataCount++;
					logger.error("BLANK JSON DATA while fetching TechGadget ProductList for URL: {}", url);
					continue;
				}
				
			} catch (Exception e) {
				blankDataCount++;
				logger.error("Error while fetching TechGadget ProductList for category:{}, URL:{}",category, url, e.getMessage());
			}
		}
		
	}


	private TechGadgetProductDetail getTechGadgetProductDetail(String category, DBObject product) {
		
		TechGadgetProductDetail productDetail = new TechGadgetProductDetail();
		productDetail.setProductId(getStringValue(product.get("id")));
		productDetail.setProductName(getStringValue(product.get("name")));
		
		if(productDetail.getProductName() == null || "".equals(productDetail.getProductName()))
				return null;
		
		productDetail.setProductImage(getStringArray(product.get("images")));
		productDetail.setBrandName(getStringValue(product.get("brand")));
		productDetail.setBrandImage(getStringValue(product.get("brandImage")));
		productDetail.setAnnouncedDate(getDateValue(product.get("announced")));
		productDetail.setModelName(getStringValue(product.get("modelName")));
		productDetail.setCommonName(getStringValue(product.get("commonName")));
		if("".equals(productDetail.getCommonName()))
			productDetail.setCommonName(null);
		
		productDetail.setCategory(categoryMap.get(category));
		productDetail.setPrice(getDoubleValue(product.get("MRP")));
		
		//SET VARIANTS ALSO HERE
		productDetail.setColors(getStringArray(product.get("colors")));
		productDetail.setVariants(getStringArray(product.get("variants")));
		
		productDetail.setDiscontinued(getIntValueFromBoolean(product.get("discontinue")));
		productDetail.setUpcoming(getIntValueFromBoolean(product.get("upcoming")));
		
		productDetail.setSource(PRIMARY_SOURCE);
		
		
		DBObject apiObject = getProductDetails(category, productDetail.getProductId());
		if(apiObject != null) {
			productDetail.setKey_features((DBObject) apiObject.get("keyFeatures"));
			productDetail.setSpecs((DBObject) apiObject.get("specs"));
		}
		return productDetail;
	}

	/**
	 * For a product Id, Fetches details from 91 Detail API, Details include fields like specs which are not given in the listing API
	 * @param category 
	 * @param productId
	 * @return
	 */
	private DBObject getProductDetails(String category, String productId) {
		
		String url = PRODUCT_DETAIL_URL.replace(PRODUCT_ID, productId).replace(CATEGORY, category);
		
		String productDetail = null;
		try {
			productDetail = fileContentServices.getContentByHTTP(url, 10000, 30000);
		} catch (Exception e) {
			logger.error("Error while fetching TechGadget ProductDetail for productId:{}, URL:{}", productId, url.replace(PRODUCT_ID, productId), e.getMessage());
		}
		
		if(StringUtils.hasText(productDetail)) {
			// convert JSON to DBObject directly
			try{
				
				DBObject dbObject = (DBObject) JSON.parse(productDetail);
				if(dbObject == null || !"200".equals(getStringValue(dbObject.get("statusCode")))) {
					logger.error("Invalid Status Code while fetching TechGadget ProductDetail for URL: {}", url);
					return null;
				}
				
				
				dbObject = (DBObject) dbObject.get("results");
				if(dbObject == null || !"false".equals(getStringValue(dbObject.get("exception")))) {
					logger.error("Exception while fetching TechGadget ProductDetail for URL: {}, with msgs: {}", url, dbObject.get("msgs"));
					return null;
				}
				
				return dbObject;
			} catch(Exception e) {
				logger.error("Error while parsing JSON of TechGadget ProductDetail for productId:{}, URL:{}", productId, url, e.getMessage());
			}
		}
		
		return null;
	}


	private String getStringValue(Object obj) {
		if(obj != null && !"".equals(String.valueOf(obj)) && !"null".equalsIgnoreCase(String.valueOf(obj)))
			return String.valueOf(obj).trim();
		return "";
	}
	
	private int getIntValueFromBoolean(Object obj) {
		if(obj != null && !"".equals(String.valueOf(obj)) && !"null".equalsIgnoreCase(String.valueOf(obj))) {
			return String.valueOf(obj).trim().equalsIgnoreCase("true") ? 1 : 0;
		}
		return 0;
	}
	
	/*private String[] getStringArray(Object obj) {
		if(obj != null && !"".equals(String.valueOf(obj)) && !"null".equalsIgnoreCase(String.valueOf(obj))
				&& obj instanceof BasicDBList) {
			
			BasicDBList list = (BasicDBList) obj;
			if(list != null && !list.isEmpty()) {
				return list.toArray(new String[0]);
			}
		}
		return new String[] {};
	}*/
	
	private String[] getStringArray(Object obj) {
		if(obj != null && !"".equals(String.valueOf(obj)) && !"null".equalsIgnoreCase(String.valueOf(obj))
				&& obj instanceof BasicDBList) {
			
			BasicDBList list = (BasicDBList) obj;
			if(list != null && !list.isEmpty()) {
				Object[] arry = list.toArray();
				String[] newArry = new String[arry.length];
				for(int i=0; i < arry.length; i++) {
					newArry[i] = getStringValue(arry[i]);
				}
				
				return newArry;
			}
		}
		return new String[] {};
	}
	
	
	private double getDoubleValue(Object obj) {
		try {
			if(obj != null && !"".equals(obj) && !"null".equalsIgnoreCase(String.valueOf(obj)))
				return Double.parseDouble(String.valueOf(obj));
		} catch (Exception e) {
			logger.error("Error Reading Double value:{}", obj);
		}
		return 0D;
	}
	
	private Date getDateValue(Object obj) {
		try {
			if(obj != null && !"".equals(obj) && !"null".equalsIgnoreCase(String.valueOf(obj))) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				return formatter.parse(String.valueOf(obj));
			}
		} catch (Exception e) {
			logger.error("Error Reading Date value:{}", obj);
		}
		return null;
	}
	private int getParameterFromReviewList(List<DBObject> reviewLst, String parameter) {
		for(DBObject reviewObj : reviewLst) {
			if(GN_HOSTID == getIntValue(reviewObj.get("hostid"))) {
				return getIntValue(reviewObj.get(parameter));
			}
		}
		// TODO Auto-generated method stub
		return 0;
	}
	
	private int getIntValue(Object obj) {
		try {
			if(obj != null && !"".equals(obj) && !"null".equalsIgnoreCase(String.valueOf(obj)))
				return Integer.parseInt(String.valueOf(obj));
		} catch (Exception e) {
			logger.error("Error Reading integer value:{}", obj);
		}
		return 0;
	}
	
	/*
	public static void main(String[] args){
		DBObject query= new BasicDBObject().append("lcuname","infocus-m812");
		DBObject product=MongoRestAPIService.get("gadget_product_detail", query);
		TechGadgetProductReaderNew t = new TechGadgetProductReaderNew();
		t.getTechGadgetProductDetail((String)product.get("category"),product);
		product.put("MRP", null);
		t.getTechGadgetProductDetail((String)product.get("category"),product);
	}
	*/
	
	

}
