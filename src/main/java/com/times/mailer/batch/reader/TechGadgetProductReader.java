package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.util.StringUtils;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
import com.times.common.service.FileContentServices;
import com.times.common.util.GadgetUtil;
import com.times.common.util.InMemoryData;
import com.times.common.util.XMLReader;
import com.times.mailer.model.TechGadgetProductDetail;

import static com.times.mailer.constant.GadgetConstant.Category.*;

/**
 * This class reads the Gadgets list for all brands by using BUYT API.
 * It then saves all the brand list in 'gadgetBrandList'.
 * All Products list for all brands are saved in 'brandProductsList' by creating the model of 'TechGadgetProductDetail'.
 * 
 * Objects are transferred to writer one by one from 'brandProductsList' which is of type 'TechGadgetProductDetail'.
 * 
 * @author Anurag.Singhal
 *
 */
public class TechGadgetProductReader implements ItemReader<TechGadgetProductDetail>, StepExecutionListener {
	
	// -----------------------------------------------------------------------------------------------
	// Start Private members
		
	private static final Logger logger = LoggerFactory.getLogger(TechGadgetProductReader.class);
	
	public Map<String, List<String>> gadgetBrandMap = new HashMap<String, List<String>>();
	
	public List<TechGadgetProductDetail> brandProductsList = new ArrayList<TechGadgetProductDetail>();
	
	private long startTime = System.currentTimeMillis();
	
	private FileContentServices fileContentServices;
	
	private EmailProcessor smtpMailer;
	
	private Map<Integer, String> statusMap;
	
	private static final String BRAND_NAME = "##BRAND_NAME";
	private static final String PRODUCT_ID = "##PRODUCT_ID";
	
	private static final String PRIMARY_SOURCE = "BUYT";
	
//	private static final String BRAND_LIST_URL = "http://ctl1.buyt.in/api/suggest.php?brand=all&url=http://timesofindia.indiatimes.com";
	private static final String BRAND_LIST_URL = "http://ctl.buyt.in/data/wid?wid=suggest&brand=all&url=http://timesofindia.indiatimes.com";
	private static final String LAPTOP_BRAND_LIST_URL = "http://ctl.buyt.in/data/wid?wid=suggest&brand=all&url=http://timesofindia.indiatimes.com&cat=laptop";
	private static final String CAMERA_BRAND_LIST_URL = "http://ctl.buyt.in/data/wid?wid=suggest&brand=all&url=http://timesofindia.indiatimes.com&cat=camera";
	
	
//	private static final String PRODUCT_LIST_FOR_BRAND_URL = "http://ctl1.buyt.in/api/suggest.php?brand="+BRAND_NAME+"&url=http://timesofindia.indiatimes.com";
	private static final String PRODUCT_LIST_FOR_BRAND_URL = "http://ctl.buyt.in/data/wid?wid=suggest&brand="+BRAND_NAME+"&url=http://timesofindia.indiatimes.com";
	private static final String LAPTOP_PRODUCT_LIST_FOR_BRAND_URL = "http://ctl.buyt.in/data/wid?wid=suggest&brand="+BRAND_NAME+"&url=http://timesofindia.indiatimes.com&cat=laptop";
	private static final String CAMERA_PRODUCT_LIST_FOR_BRAND_URL = "http://ctl.buyt.in/data/wid?wid=suggest&brand="+BRAND_NAME+"&url=http://timesofindia.indiatimes.com&cat=camera";
	
	//URL for Fetching Product Detail from BUYT
//	private static final String PRODUCT_DETAIL_URL = "http://ctl1.buyt.in/api/specs.php?pubid=toi&ids="+PRODUCT_ID+"&url=http://timesofindia.indiatimes.com";
	private static final String PRODUCT_DETAIL_URL = "http://ctl.buyt.in/data/wid?wid=specs&pubid=toi&ids="+PRODUCT_ID+"&url=http://timesofindia.indiatimes.com";
	private static final String LAPTOP_PRODUCT_DETAIL_URL = "http://ctl.buyt.in/data/wid?wid=specs&pubid=toi&ids="+PRODUCT_ID+"&url=http://timesofindia.indiatimes.com&cat=laptop";
	private static final String CAMERA_PRODUCT_DETAIL_URL = "http://ctl.buyt.in/data/wid?wid=specs&pubid=toi&ids="+PRODUCT_ID+"&url=http://timesofindia.indiatimes.com&cat=camera";

	
	// End Private members
	// -----------------------------------------------------------------------------------------------
	
	
	// -----------------------------------------------------------------------------------------------
	// Start public methods

	
	public TechGadgetProductDetail read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		if (this.brandProductsList.size() > 0) {
			TechGadgetProductDetail ProductDetail = getProductDetail();
			return ProductDetail;
		} 
		return null;
	}
	
	
	public void beforeStep(StepExecution stepExecution) {
		
		logger.debug(" Before Step method..");
		
		startTime = System.currentTimeMillis();
		
		if(gadgetBrandMap.isEmpty())
			initializeBrandMap();
		
		if(brandProductsList.isEmpty())
			initializeProductList();
		
		logger.debug("Total Initialization time for Brand and Product Listing: " + ((System.currentTimeMillis() - startTime)/1000) + " second");		
	}

	
	public ExitStatus afterStep(StepExecution stepExecution) {
		
		logger.debug("After Step method..");
		
		logger.debug("After Step status : " + stepExecution.getExitStatus().toString());
		
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}
		
		statusMap = new HashMap<Integer, String>();
		statusMap.put(0, "INACTIVE");
		statusMap.put(1, "PENDING");
		statusMap.put(2, "ACTIVE");
		
		//Sending mail here
		EmailVO vo = generateMailObject();
		try {
			if(vo.getBody() != null && !"".equals(vo.getBody()))
				smtpMailer.sendEMail(vo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error while sending mail..", e);
		}
		
		return stepExecution.getExitStatus();
	}
	
	
	// End public methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start private methods
	
	
	private EmailVO generateMailObject() {
		EmailVO vo = new EmailVO();
		vo.setSender("techgadget@timesinternet.in");
		vo.setRecipients("anurag.singhal@timesinternet.in");
		vo.setSubject("Gadgets Added by Primary DATA");
		
		String body = "";
		
		if(InMemoryData.brandAlertList != null && !InMemoryData.brandAlertList.isEmpty()) {
			body += "Brands Added by Primary Source: <br><br>";
			
			for(DBObject obj : InMemoryData.brandAlertList) {
				body += "Name:" + obj.get("name") + ", Status:" + statusMap.get(Integer.parseInt(String.valueOf(obj.get("status")))) 
						+ ", URL:" + "http://jcmsdev.indiatimes.com/tech-gadget/gadget/getBrandDetails?id=" + obj.get("uname") + "<br>";
			}
		}
		
		
		if(InMemoryData.alertList != null && !InMemoryData.alertList.isEmpty()) {
			body += "Primary Gadgets Added: <br><br>";
			
			for(DBObject obj : InMemoryData.alertList) {
				body += "Name:" + obj.get("name") + ", Status:" + statusMap.get(Integer.parseInt(String.valueOf(obj.get("status")))) 
						+ ", Review MSID:" + obj.get("reviewid.$.msid") + ", URL:" + "http://jcmsdev.indiatimes.com/gadgeteditform.cms?brandid=" + obj.get("uname") + "<br>";
			}
		}
		
		InMemoryData.alertList = null;
		InMemoryData.brandAlertList = null;
		vo.setBody(body);
		
		return vo;
	}

	private TechGadgetProductDetail getProductDetail() {

		TechGadgetProductDetail productDetail = new TechGadgetProductDetail();
	
		synchronized (this) {
			if (brandProductsList.size() > 0) {
				productDetail = this.brandProductsList.remove(0);
				return productDetail;
			}
		}
		
		return productDetail;
	}

	private void initializeBrandMap() {
		addBrandsList(MOBILES, BRAND_LIST_URL);
//		addBrandsList(LAPTOPS, LAPTOP_BRAND_LIST_URL);
//		addBrandsList(CAMERAS, CAMERA_BRAND_LIST_URL);
	}
	
	
	private void addBrandsList(String category, String brandListUrl) {

		try {
			String brandList = getFileContentServices().getContentByHTTP(brandListUrl, 5000, 20000);
			
			if(StringUtils.hasText(brandList)) {
				Document brandsDoc = XMLReader.loadXml(brandList);
				if(brandsDoc != null) {
					List<Element> brandElementLst = brandsDoc.getRootElement().getChildren("brand");
					if(brandElementLst != null && brandElementLst.size() > 0) {
						List<String> gadgetBrandList = new ArrayList<String>();
						for(Element brandEle : brandElementLst) {
							gadgetBrandList.add(brandEle.getText());
						}
						
						gadgetBrandMap.put(category, gadgetBrandList);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error while fetching TechGadget BrandList for category:{}, URL:{} ", category, brandListUrl, e.getMessage());
		}
	}


	private void initializeProductList() {
		
		addProductList(MOBILES, PRODUCT_LIST_FOR_BRAND_URL);
//		addProductList(LAPTOPS, LAPTOP_PRODUCT_LIST_FOR_BRAND_URL);
//		addProductList(CAMERAS, CAMERA_PRODUCT_LIST_FOR_BRAND_URL);
		
	}
	
	
	private void addProductList(String category, String productListUrl) {
		if(!gadgetBrandMap.containsKey(category))
			return;
		
		for(String brandName: gadgetBrandMap.get(category)) {
			
			try {
				String productList = getFileContentServices().getContentByHTTP(productListUrl.replace(BRAND_NAME, brandName), 5000, 20000);
				
				if(StringUtils.hasText(productList)) {
					Document productsDoc = XMLReader.loadXml(productList);
					if(productsDoc != null) {
						List<Element> productElementLst = productsDoc.getRootElement().getChildren("product");
						if(productElementLst != null && productElementLst.size() > 0) {
							
							logger.info("Category: {}, Brand: {}, ListSize: {}", category, brandName, productElementLst.size());
							for(Element productEle : productElementLst) {
								TechGadgetProductDetail model = getTechGadgetProductDetail(category, productEle, brandName);
								if(model != null)
									brandProductsList.add(model);
							}
//							return;
						}
					}
				}
			} catch (Exception e) {
				logger.error("Error while fetching TechGadget ProductList for category:{}, brand:{}, URL:{}",category, brandName, productListUrl.replace(BRAND_NAME, brandName), e.getMessage());
			}
		}
		
	}


	private TechGadgetProductDetail getTechGadgetProductDetail(String category, Element productEle, String brandName) {
		TechGadgetProductDetail productDetail = new TechGadgetProductDetail();
		productDetail.setProductId(getStringValue(productEle.getChildText("id")));
		productDetail.setProductName(getStringValue(productEle.getChildText("name")));
		
		if(productDetail.getProductName() == null || "".equals(productDetail.getProductName()))
			return null;
		
		productDetail.setProductImage(new String[] {getStringValue(productEle.getChildText("image"))});
		productDetail.setAdded(getStringValue(productEle.getChildText("added")));
		productDetail.setDeleted(getStringValue(productEle.getChildText("deleted")));
//		productDetail.setBrandName(productEle.getChildText("brand"));//Commented as this is not present in Laptop and Camera
		productDetail.setBrandName(brandName);
		productDetail.setBrandImage(getStringValue(productEle.getChildText("brand_image")));
		productDetail.setAnnouncedDate(GadgetUtil.getProperDateFormat(productEle.getChildText("announced")));
		productDetail.setModelName(getStringValue(productEle.getChildText("model_name")));
		productDetail.setCategory(category);
		productDetail.setPrice(0D);
		productDetail.setSource(PRIMARY_SOURCE);
		
		DBObject apiObject = getProductDetails(productDetail.getCategory(), productDetail.getProductId());
		if(apiObject != null) {
			productDetail.setKey_features((DBObject) apiObject.get("Key_features"));
			productDetail.setSpecs((DBObject) apiObject.get("specs"));
		}
		return productDetail;
	}


	private DBObject getProductDetails(String category, String productId) {
		
		String url;
		if(category.equals(MOBILES))
			url = PRODUCT_DETAIL_URL;
		else if(category.equals(LAPTOPS))
			url = LAPTOP_PRODUCT_DETAIL_URL;
		else if(category.equals(CAMERAS))
			url = CAMERA_PRODUCT_DETAIL_URL;
		else
			return null;
		
		String productDetail = null;
		try {
			productDetail = fileContentServices.getContentByHTTP(url.replace(PRODUCT_ID, productId), 5000, 20000);
		} catch (Exception e) {
			logger.error("Error while fetching TechGadget ProductDetail for productId:{}, URL:{}", productId, url.replace(PRODUCT_ID, productId), e.getMessage());
		}
		
		if(StringUtils.hasText(productDetail)) {
			// convert JSON to DBObject directly
			try{
				DBObject dbObject = (DBObject) JSON.parse(productDetail);
				return (DBObject) dbObject.get(productId);
			} catch(Exception e) {
				logger.error("Error while parsing JSON of TechGadget ProductDetail for productId:{}", productId, e.getMessage());
			}
		}
		
		return null;
	}

	private String getStringValue(Object obj) {
		if(obj != null && !"".equals(String.valueOf(obj)) && !"null".equalsIgnoreCase(String.valueOf(obj)))
			return String.valueOf(obj).trim();
		return "";
	}
	
	
	public FileContentServices getFileContentServices() {
		return fileContentServices;
	}

	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}
	
	/**
	 * @param smtpMailer the smtpMailer to set
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}

}
