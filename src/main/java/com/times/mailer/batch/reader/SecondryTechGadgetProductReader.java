package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
import com.times.common.service.FileContentServices;
import com.times.common.util.InMemoryData;
import com.times.mailer.constant.ProductField;
import com.times.mailer.model.ProductData;
import com.times.mailer.model.TechGadgetProductDetail;

import static com.times.mailer.constant.GadgetConstant.*;
import static com.times.mailer.constant.GadgetConstant.Category.*;

/**
 * This class reads the Gadgets list for all brands by using BUYT API.
 * It then saves all the brand list in 'gadgetBrandList'.
 * All Products list for all brands are saved in 'brandProductsList' by creating the model of 'TechGadgetProductDetail'.
 * 
 * Objects are transferred to writer one by one from 'brandProductsList' which is of type 'TechGadgetProductDetail'.
 * 
 * @author Anurag.Singhal
 *
 */
public class SecondryTechGadgetProductReader implements ItemReader<TechGadgetProductDetail>, StepExecutionListener {
	
	// -----------------------------------------------------------------------------------------------
	// Start Private members
		
	private static final Logger logger = LoggerFactory.getLogger(SecondryTechGadgetProductReader.class);
	
	public List<TechGadgetProductDetail> brandProductsList = new ArrayList<TechGadgetProductDetail>();
	private Map<Integer, String> statusMap;
	
	private long startTime = System.currentTimeMillis();
	
	private FileContentServices fileContentServices;
	private EmailProcessor smtpMailer;
	
	
	// End Private members
	// -----------------------------------------------------------------------------------------------
	
	
	// -----------------------------------------------------------------------------------------------
	// Start public methods

	
	public TechGadgetProductDetail read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		if (this.brandProductsList.size() > 0) {
			TechGadgetProductDetail ProductDetail = getProductDetail();
			return ProductDetail;
		} 
		return null;
	}
	
	
	public void beforeStep(StepExecution stepExecution) {
		
		logger.debug(" Before Step method..");
		
		InMemoryData.alertList = new ArrayList<DBObject>();
		InMemoryData.brandAlertList = new ArrayList<DBObject>();
		
		startTime = System.currentTimeMillis();
		
		if(brandProductsList.isEmpty())
			pickSecondaryData();
//			initializeProductList();
		
		logger.debug("Total Initialization time for Brand and Product Listing: " + ((System.currentTimeMillis() - startTime)/1000) + " second");		
	}

	
	public ExitStatus afterStep(StepExecution stepExecution) {
		
		logger.debug("After Step method..");
		
		logger.debug("After Step status : " + stepExecution.getExitStatus().toString());
		
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}
		
		statusMap = new HashMap<Integer, String>();
		statusMap.put(0, "INACTIVE");
		statusMap.put(1, "PENDING");
		statusMap.put(2, "ACTIVE");
		
		//Sending mail here
		EmailVO vo = generateMailObject();
		try {
			if(vo.getBody() != null && !"".equals(vo.getBody()))
				smtpMailer.sendEMail(vo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error while sending mail..", e);
		}
				
		return stepExecution.getExitStatus();
	}
	
	private EmailVO generateMailObject() {
		EmailVO vo = new EmailVO();
		
		vo.setSender("gadgetsnow@timesinternet.in");
		vo.setRecipients("anurag.singhal@timesinternet.in");
		vo.setSubject("Gadgets Added by Secondary DATA");
		
		String body = "";
		
		if(InMemoryData.brandAlertList != null && !InMemoryData.brandAlertList.isEmpty()) {
			body += "Brands Added by Secondary Source: <br><br>";
			
			for(DBObject obj : InMemoryData.brandAlertList) {
				body += "Category:" + obj.get("category") + ", Name:" + obj.get("name") + ", Status:" + statusMap.get(Integer.parseInt(String.valueOf(obj.get("status")))) 
//						+ ", URL: " + "http://jcmsdev.indiatimes.com/tech-gadget/gadget/getBrandDetails?id=" + obj.get("uname")
						+ "<br>";
			}
			
		}
		
		
		if(InMemoryData.alertList != null && !InMemoryData.alertList.isEmpty()) {
			body += "Secondary Gadgets Added: <br><br>";
			
			for(DBObject obj : InMemoryData.alertList) {
				body += "Category:" + obj.get("category") + ", Name:" + obj.get("name") + ", Status:" + statusMap.get(Integer.parseInt(String.valueOf(obj.get("status")))) 
						+ ", Review MSID:" + obj.get("reviewid") + ", URL: " + "http://jcmsdev.indiatimes.com/gadgeteditform.cms?brandid=" + obj.get("uname") + "<br>";
			}
		}
		
		InMemoryData.alertList = null;
		InMemoryData.brandAlertList = null;
		
		vo.setBody(body);
		
		return vo;
	}

	
	// End public methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start private methods
	
	private TechGadgetProductDetail getProductDetail() {

		TechGadgetProductDetail productDetail = new TechGadgetProductDetail();
	
		synchronized (this) {
			if (brandProductsList.size() > 0) {
				productDetail = this.brandProductsList.remove(0);
				return productDetail;
			}
		}
		
		return productDetail;
	}
	
	private void initializeProductList() {
		
		List<DBObject> secondaryProductList = null;
		try {
			secondaryProductList = MongoRestAPIService.getList(SECONDARY_PRODUCT_DETAIL, new BasicDBObject());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(secondaryProductList != null && !secondaryProductList.isEmpty()) {
			for(DBObject dbObject : secondaryProductList) {
				brandProductsList.add(getTechGadgetProductDetail(dbObject));
			}
		}
		
	}
	
	// End private methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start getters/setters

	private TechGadgetProductDetail getTechGadgetProductDetail(DBObject dbObject) {
		TechGadgetProductDetail productDetail = new TechGadgetProductDetail();
		productDetail.setProductId(String.valueOf(dbObject.get("pid")));
		
		if(dbObject.get("name") != null)
			productDetail.setProductName(String.valueOf(dbObject.get("name")));
		
		if(dbObject.get("image") != null)
			productDetail.setProductImage(new String[] {String.valueOf(dbObject.get("image"))});
		else
			productDetail.setProductImage(new String[] {});
		
		productDetail.setAdded("");
		productDetail.setDeleted("");
		
		if(dbObject.get("brand_name") != null)
			productDetail.setBrandName(String.valueOf(dbObject.get("brand_name")));
		
		//brand image is not currently available in secondry data source 
		productDetail.setBrandImage("");
		
		productDetail.setAnnouncedDate(null);
		productDetail.setModelName(productDetail.getProductName().replaceFirst("(?i)"+productDetail.getBrandName(), "").trim());
		productDetail.setCategory(MOBILES);
		if(dbObject.get("price") != null)
			productDetail.setPrice(Integer.parseInt(String.valueOf(dbObject.get("price"))));
		productDetail.setSource(String.valueOf(dbObject.get("source")));
		productDetail.setSpecs((DBObject)dbObject.get("specs"));
		productDetail.setKey_features((DBObject)dbObject.get("Key_features"));
		
		return productDetail;
	}


	public FileContentServices getFileContentServices() {
		return fileContentServices;
	}

	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}

	// End getters/setters
	// -----------------------------------------------------------------------------------------------

	
	
	
	
	
	
	
	
	
	public void pickSecondaryData() {

//		String jsonResponse = null;
		try {
			String mongoIpAddress = "192.168.37.235";
			int mongoPort = 27017;
			String DB_NAME = "crawl";
			String COLLECTION_NAME = "gadgets";

			
			MongoClient mongoClient = new MongoClient(mongoIpAddress, mongoPort);
			// Now connect to your databases
	        DB db = mongoClient.getDB(DB_NAME);
	        System.out.println("Connect to database successfully");
	        DBCollection collection = db.getCollection(COLLECTION_NAME);
	        
	        //This is written by assuming that job will run once a day after 12AM in night.
	        Calendar calendar = Calendar.getInstance();
	        calendar.set(Calendar.MILLISECOND, 0);
	        calendar.set(Calendar.SECOND, 0);
	        calendar.set(Calendar.MINUTE, 0);
	        calendar.set(Calendar.HOUR_OF_DAY, 0);
	        calendar.add(Calendar.DATE, -1);
	        
	        Calendar toCalendar = Calendar.getInstance();
	        toCalendar.set(Calendar.MILLISECOND, 0);
	        toCalendar.set(Calendar.SECOND, 0);
	        toCalendar.set(Calendar.MINUTE, 0);
	        toCalendar.set(Calendar.HOUR_OF_DAY, 0);
	        
	        DBCursor cursor = collection.find(new BasicDBObject("crawledTime", new BasicDBObject(/*"$gte", calendar.getTimeInMillis()*/).append("$lt", toCalendar.getTimeInMillis())));
			if(cursor == null)
				return;
			
			while(cursor.hasNext()) {
				DBObject secondaryObj = cursor.next();

				try {

					ProductData productData=new ProductData();
					productData.setPid("");

					//DO not consider product with Null Value String
					if(secondaryObj.containsField("name") && secondaryObj.get("name") != null && !"".equals(secondaryObj.get("name")) 
							&& !String.valueOf(secondaryObj.get("name")).toLowerCase().contains("null"))
						productData.setName(String.valueOf(secondaryObj.get("name")).trim());
					else
						continue;

					//DO not consider data with Brand Missing..
					if(secondaryObj.containsField("brand") && secondaryObj.get("brand") != null && !"".equals(secondaryObj.get("brand")))
						productData.setBrandName(String.valueOf(secondaryObj.get("brand")).trim());
					else
						continue;

					//				if(jsonObjectRoot.has("image"))
					productData.setImage(getStringValue(secondaryObj.get("image")));

					//				if(jsonObjectRoot.has("price"))
					productData.setPrice(getIntValue(secondaryObj.get("price")));


					DBObject specifications = (DBObject) secondaryObj.get("specifications");

					//Commented as raw data might have same product with different source, but we will add only one as secondary.
//					if(!"".equals(getStringValue(specifications.get("source"))))
//						productData.setSource(getStringValue(specifications.get("source")));
//					else
						productData.setSource("SECONDARY");


					DBObject specs=(DBObject) specifications.get("specs");

					//					productData.setId(jsonObjectRoot.getString("_id"));

					productData.setDT(new Date());

					//				if(jsonObject.has("Weight"))
					productData.setSpecs_body_weight(getStringValue(specs.get("Weight")));

					//				if(jsonObject.has("Dimensions"))
					productData.setSpecs_body_dimensions(getStringValue(specs.get("Dimensions")));

					//				if(jsonObject.has("SIM Slot(s)"))
					productData.setSpecs_body_sim(getStringValue(specs.get("SIM Slot(s)")));

					//				if(jsonObject.has("Loudspeaker"))
					productData.setSpecs_sound_loudspeaker(getStringValue(specs.get("Loudspeaker")));

					//				if(jsonObject.has("GPRS"))
					productData.setSpecs_network_gprs(getStringValue(specs.get("GPRS")));

					//				if(jsonObject.has("Operating Frequency"))
					productData.setSpecs_network_technology(getStringValue(specs.get("Operating Frequency")));

					//				if(jsonObject.has("4G"))
					productData.setSpecs_network_4gBands_value(getStringValue(specs.get("4G")));

					//				if(jsonObject.has("3G"))
					productData.setSpecs_network_3gBands_value(getStringValue(specs.get("3G")));

					//				if(jsonObject.has("2G"))
					productData.setSpecs_network_2gBands_value(getStringValue(specs.get("2G")));

					//				if(jsonObject.has("EDGE"))
					productData.setSpecs_network_edge(getStringValue(specs.get("EDGE")));

					//				if(jsonObject.has("Battery")) {
					productData.setSpecs_battery_type(getStringValue(specs.get("Battery")));
					productData.setKf_battery(getStringValue(specs.get("Battery")));
					//				}

					//				if(jsonObject.has("Front Camera"))
					productData.setSpecs_camera_front(getStringValue(specs.get("Front Camera")));

					//				if(jsonObject.has("Main Camera")) {
					productData.setSpecs_camera_rear(getStringValue(specs.get("Main Camera")));
					productData.setKf_camera(getStringValue(specs.get("Main Camera")));
					//				}

					//				if(jsonObject.has("Camera Features"))
					productData.setSpecs_camera_features(getStringValue(specs.get("Camera Features")));

					//				if(jsonObject.has("Video Recording"))
					productData.setSpecs_camera_video(getStringValue(specs.get("Video Recording")));

					//				if(jsonObject.has("Internal Memory")) {
					productData.setSpecs_memory_internal(getStringValue(specs.get("Internal Memory")));
					productData.setKf_storage(getStringValue(specs.get("Internal Memory")));
					//				}

					//				if(jsonObject.has("Expandable Memory"))
					productData.setSpecs_memory_card_slot(getStringValue(specs.get("Expandable Memory")));

					//				if(jsonObject.has("Touch Screen")) {
					productData.setSpecs_display_type(getStringValue(specs.get("Touch Screen")));
					productData.setKf_display(getStringValue(specs.get("Touch Screen")));
					//				}

					//				if(jsonObject.has("Screen Resolution"))
					productData.setSpecs_display_resolution(getStringValue(specs.get("Screen Resolution")));

					//				if(jsonObject.has("Screen Size"))
					productData.setSpecs_display_size(getStringValue(specs.get("Screen Size")));

					//				if(jsonObject.has("RAM"))
					productData.setKf_ram(getStringValue(specs.get("RAM")));

					//				if(jsonObject.has("Graphics"))
					productData.setPf_graphicsCard(getStringValue(specs.get("Graphics")));

					//				if(jsonObject.has("Processor")) {
					productData.setPf_cpu(getStringValue(specs.get("Processor")));
					productData.setPf_chipset(getStringValue(specs.get("Processor")));
					productData.setKf_processor(getStringValue(specs.get("Processor")));
					//				}

					//				if(jsonObject.has("Operating System")) {
					productData.setPf_os(getStringValue(specs.get("Operating System")));
					productData.setKf_os(getStringValue(specs.get("Operating System")));
					//				}


					BasicDBObject rawData = getMongoDBObject(productData);

					//Create data as per the writer MODEL
					brandProductsList.add(getTechGadgetProductDetail(rawData));
				} catch(Exception e) {
					logger.error("Error while reading raw Data with JSON:{}", secondaryObj, e);
				}
			}
			
//			jsonResponse = getFileContentServices().getContentByHTTP("http://mratest.indiatimes.com/mra/get/products_raw_11feb?query");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		if(jsonResponse == null)
//			return;
//		JSONArray jsonarray=new JSONArray(jsonResponse);
//		System.out.println(jsonarray.length());
//		for (int i = 0; i < jsonarray.length(); i++) {
//			JSONObject jsonObjectRoot=jsonarray.getJSONObject(i);
//			try {} catch(Exception e) {
//				logger.error("Error while reading raw Data with JSON:{}", jsonObjectRoot, e);
//			}
//			
//		}
		//System.out.println(jsonarray);

	}


	private BasicDBObject getMongoDBObject(ProductData productData) {
	
		BasicDBObject dbObject = new BasicDBObject();
	//	document.put(ProductField.ID, productData.getId());
		dbObject.put(ProductField.BRAND_NAME, productData.getBrandName());
		dbObject.put(ProductField.NAME, productData.getName());
		dbObject.put(ProductField.PID, productData.getPid());
		dbObject.put(ProductField.IMAGE, productData.getImage());
		dbObject.put(ProductField.ADDED, productData.getAdded());                
		dbObject.put(ProductField.DELETED, productData.getDeleted());    
		dbObject.put(ProductField.BRAND_IMAGE, productData.getBrandImage());
		dbObject.put(ProductField.MSID, productData.getMsid());
		dbObject.put(ProductField.DT, productData.getDT());
		dbObject.put(ProductField.SOURCE, productData.getSource()); 
		dbObject.put(ProductField.STATUS, productData.getStatus());  
		dbObject.put(ProductField.CATAGORY, productData.getCatagory());
		dbObject.put(ProductField.PRICE, productData.getPrice());
	
		BasicDBObject specs = new BasicDBObject();
	
		BasicDBObject body = new BasicDBObject();
		body.put(ProductField.WEIGHT, productData.getSpecs_body_weight());
		body.put(ProductField.DIMENSIONS, productData.getSpecs_body_dimensions());
		body.put(ProductField.SIM, productData.getSpecs_body_sim());
	
		specs.put(ProductField.BODY, body);
	
		BasicDBObject sound = new BasicDBObject();
		sound.put(ProductField.LOUDSPEAKER, productData.getSpecs_sound_loudspeaker());
		sound.put(ProductField.ALERT_TYPES, productData.getSpecs_sound_alertTypes());
	
		specs.put(ProductField.SOUND, sound);
	
		BasicDBObject network = new BasicDBObject();
	
		BasicDBObject _4G_bands = new BasicDBObject();
		_4G_bands.put(ProductField.VALUE, productData.getSpecs_network_4gBands_value());
		_4G_bands.put(ProductField.LEVEL, productData.getSpecs_network_4gBands_level());
	
		BasicDBObject _3G_bands = new BasicDBObject();
		_3G_bands.put(ProductField.VALUE, productData.getSpecs_network_3gBands_value());
		_3G_bands.put(ProductField.LEVEL, productData.getSpecs_network_3gBands_level());
	
		BasicDBObject _2G_bands = new BasicDBObject();
		_2G_bands.put(ProductField.VALUE, productData.getSpecs_network_2gBands_value());
		_2G_bands.put(ProductField.LEVEL, productData.getSpecs_network_2gBands_level());
	
		network.put(ProductField.GPRS, productData.getSpecs_network_gprs());
		network.put(ProductField._4G_BANDS, _4G_bands);
		network.put(ProductField._3G_BANDS,  _3G_bands);
		network.put(ProductField._2G_BANDS,  _2G_bands);
		network.put(ProductField.EDGE, productData.getSpecs_network_edge());
		network.put(ProductField.SPEED, productData.getSpecs_network_speed());
		network.put(ProductField.TECHNOLOGY, productData.getSpecs_network_technology());
	
		specs.put(ProductField.NETWORK, network);
	
		BasicDBObject battery = new BasicDBObject();
		battery.put(ProductField.TYPE, productData.getSpecs_battery_type());
		battery.put(ProductField.STAND_BY, productData.getSpecs_battery_standBy());
		battery.put(ProductField.TALK_TIME, productData.getSpecs_battery_talkTime());
	
	
		specs.put(ProductField.BATTERY, battery);
	
	
		BasicDBObject platform = new BasicDBObject();
		platform.put(ProductField.GRAPHICS_CARD, productData.getPf_graphicsCard());
		platform.put(ProductField.OS, productData.getPf_os());
		platform.put(ProductField.CPU, productData.getPf_cpu());
		platform.put(ProductField.CHIPSET, productData.getPf_chipset());
	
		specs.put(ProductField.PLATFORM, platform);
	
		
		BasicDBObject camera = new BasicDBObject();
		camera.put(ProductField.FRONT, productData.getSpecs_camera_front());
		camera.put(ProductField.FEATURES, productData.getSpecs_camera_features());
		camera.put(ProductField.VIDEO, productData.getSpecs_camera_video());
		camera.put(ProductField.REAR, productData.getSpecs_camera_rear());
	
		specs.put(ProductField.CAMERA, camera);
	
		BasicDBObject memory = new BasicDBObject();
		memory.put(ProductField.INTERNAL, productData.getSpecs_memory_internal());
		memory.put(ProductField.CARD_SLOT, productData.getSpecs_memory_card_slot());
	
		specs.put(ProductField.MEMORY, memory);
	
		BasicDBObject display = new BasicDBObject();
		display.put(ProductField.TYPE, productData.getSpecs_display_type());
		display.put(ProductField.MULTITOUCH, productData.getSpecs_display_multitouch());
		display.put(ProductField.RESOLUTION, productData.getSpecs_display_resolution());
		display.put(ProductField.SIZE, productData.getSpecs_display_size());
	
		specs.put(ProductField.DISPLAY, display);
	
		dbObject.put(ProductField.SPECS, specs);
	
		BasicDBObject keyFeatures = new BasicDBObject();
		keyFeatures.put(ProductField.BATTERY, productData.getKf_battery());
		keyFeatures.put(ProductField.RAM, productData.getKf_ram());
		keyFeatures.put(ProductField.STORAGE, productData.getKf_storage());
		keyFeatures.put(ProductField.DISPLAY, productData.getKf_display());
		keyFeatures.put(ProductField.CAMERA, productData.getKf_camera());
		keyFeatures.put(ProductField.PROCESSOR, productData.getKf_processor());
		keyFeatures.put(ProductField.OPERATING_SYSTEM, productData.getKf_os());
	
		dbObject.put(ProductField.KEY_FEATURES, keyFeatures);
	
		System.out.println(dbObject);
		// mongoCollection.insertOne(document);
	//	productData.setId( (String) document.get("_id"));
	
		return dbObject;
	}

	
	private String getStringValue(Object obj) {
		if(obj != null && !"".equals(String.valueOf(obj)) && !"null".equalsIgnoreCase(String.valueOf(obj)))
			return String.valueOf(obj).trim();
		return "";
	}
	
	
	private int getIntValue(Object obj) {
		try {
			if(obj != null && !"".equals(obj) && !"null".equalsIgnoreCase(String.valueOf(obj)))
				return Integer.parseInt(String.valueOf(obj));
		} catch (Exception e) {
			logger.error("Error Reading integer value:{}", obj);
		}
		return 0;
	}
	
	/**
	 * @param smtpMailer the smtpMailer to set
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}
	
}
