package com.times.mailer.batch.reader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.ibm.icu.util.Calendar;
import com.times.common.service.FileContentServicesImpl;
import com.times.mailer.model.NetstorageInfo;

public class EtDrAdSenseReader /*implements ItemReader<InputStream>,StepExecutionListener*/ {

	private FileContentServicesImpl fileServiceImpl;
	private final static Logger logger = LoggerFactory.getLogger(EtAdSenseReader.class);
	
	public static void main (String... args) throws Exception{
		uploadFileOnET();
	}

	public static void uploadFileOnET() throws Exception, UnexpectedInputException,
			ParseException, NonTransientResourceException {
		
		long startTime = System.currentTimeMillis();
		List<String> directoryList = new ArrayList<>();
		//directoryList.add("/til/AdControlPanel/AdControlPanelXml");
		directoryList.add("/til/cmsbckst");
		for (String directory:directoryList){
			FTPClient ftpClient=new FTPClient();
			try{
				logger.debug("*** starting ET ADSENSE job at ****" + Calendar.getInstance().getTime());
				
				FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
				ftpClient.configure(conf);
				ftpClient.connect("etstgusers.economictimes.indiatimes.com");
				String username = "ET_FTP";
				String password = "ftp@96572";
				
				boolean constatus = ftpClient.login(username, password);
				
				NetstorageInfo etWriteNetStorageDetails = new NetstorageInfo();
				
				etWriteNetStorageDetails.setFolderLocation("/93656/configspace/content");
				etWriteNetStorageDetails.setUserid("etnetftp");
				etWriteNetStorageDetails.setPassword("eTneTFTp$%67");
				etWriteNetStorageDetails.setServerName("et.upload.akamai.com");
				
				FileContentServicesImpl myclass = new FileContentServicesImpl();
				
				if (constatus){
					ftpClient.changeWorkingDirectory(directory);
					FTPFile [] files = ftpClient.listFiles();
					
					for (FTPFile file : files){
						String remoteFile1 = file.getName();
						
						InputStream is = ftpClient.retrieveFileStream(remoteFile1);
						
						String fileContent = getContentByInputStream(is);
						
						etWriteNetStorageDetails.setFileName(remoteFile1);
						
						if (is!=null) {
							try{
								myclass.uploadContentByFTP(etWriteNetStorageDetails, fileContent);
								logger.debug("File --> " + remoteFile1 + " has been uploaded successfully.");
							}
							catch(Exception e){
								logger.debug("Problem in uploading " + remoteFile1);
							}
						}
						else{
							logger.debug("Error in getting Input stream for file  ->> " + remoteFile1);
						}
						
						ftpClient.completePendingCommand();
					}
					logger.debug("total number of Files Uploaded in ET ADSENSE from directory "+directory+" are " + files.length);
				}
			}
			
			catch (IOException io)
			{
				io.printStackTrace();
			}
			
			finally{
				if (ftpClient.isConnected()){
					try{
						ftpClient.disconnect();
					}
					catch (IOException io){
						logger.error("error while closing FTP Connection");
					}
				}
			}
		}
		logger.debug("****Time taken by ET ADSENSE complete job in ms : " + (System.currentTimeMillis() - startTime)+"****");
		// TODO Auto-generated method stub
	}

	public void beforeStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		logger.debug(" beforeStep method..");
		
	}

	public FileContentServicesImpl getFileServiceImpl() {
		return fileServiceImpl;
	}

	public void setFileServiceImpl(FileContentServicesImpl fileServiceImpl) {
		this.fileServiceImpl = fileServiceImpl;
	}

	/*public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		return stepExecution.getExitStatus();
	}*/
	
	private static String getContentByInputStream(InputStream is)	throws IOException, UnsupportedEncodingException {
		String outData;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();  
		byte[] byteArray = new byte[1024];  
		int count = 0;  
		while ((count = is.read(byteArray, 0, byteArray.length)) > 0) {
			outputStream.write(byteArray, 0, count);
		}
		outData = new String(outputStream.toByteArray(), "UTF-8");
		
		if (outputStream != null) {
			outputStream.close();
			outputStream = null;
		}
		return outData;
	}
}