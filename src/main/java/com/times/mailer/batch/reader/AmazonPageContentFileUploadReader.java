package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.util.Constant;
import com.times.mailer.dao.NetstorageFileInfoDao;
import com.times.mailer.model.AmazonS3Credits;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NetstorageInfo;

/**
 * This class is used to upload the toi home page content on amazon location.
 * 
 * @author Rajeev Khatri
 *
 */
public class AmazonPageContentFileUploadReader implements ItemReader<FileLocationInfo>, StepExecutionListener { //, JobExecutionListener

	private final static Logger logger = LoggerFactory.getLogger(AmazonPageContentFileUploadReader.class);
	
	
	private NetstorageFileInfoDao netstorageFileInfoDao;
	
	private String accessKey="";
	private String secretKey="";
	private String bucketName="";
	private String contentType = "application/json;charset=UTF-8";
	
	
	private int perPage = 100;
	private int pageNo = 0;
	private static final int workType = Constant.WORK_TYPE_PAGE_CONTENT_AMAZON;  // 11 for toi home page content on amazon location
	
	public List<FileLocationInfo> fileLocationInfoList = Collections.synchronizedList(new ArrayList<FileLocationInfo>());
	
	//code changes ... 
	boolean isInitialized = false;
	private long startTime = System.currentTimeMillis();
	
	/**
	 * This method is used to read the {@link FileLocationInfo} object.
	 * 
	 * @param args
	 */
	public FileLocationInfo read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		
		// if subscriber got then 
		if (fileLocationInfoList.size() > 0) {
			//counter update in memory
			FileLocationInfo fileUploadDtls = getFileLocationInfo();
			bucketName = fileUploadDtls.getNetstorageLocation().getServerName();
			accessKey = fileUploadDtls.getNetstorageLocation().getUserid();
			secretKey = fileUploadDtls.getNetstorageLocation().getPassword();
			if (bucketName != null && accessKey != null && secretKey != null) {
				//String location = fileUploadDtls.getNetstorageLocation().getFolderLocation() + "/" + fileUploadDtls.getNetstorageLocation().getFileName();
				fileUploadDtls.setAmazonS3(new AmazonS3Credits(accessKey, secretKey, bucketName, fileUploadDtls.getNetstorageLocation().getFileName() , contentType));			
				return fileUploadDtls;
			}
		} 
		
		return null;
	}

	/**
	 * Get the Newsletter user/subscriber, if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private FileLocationInfo getFileLocationInfo() {
		FileLocationInfo fileLocationInfo = null;
		synchronized (this) {
			if (fileLocationInfoList != null && fileLocationInfoList.size() > 0) {
				fileLocationInfo = fileLocationInfoList.remove(0);
				if (fileLocationInfo == null) {
					pageNo = pageNo + perPage;
					this.fileLocationInfoList = netstorageFileInfoDao.getFileLocationInfo(workType, pageNo, perPage);
					if (fileLocationInfoList != null && fileLocationInfoList.size() > 0) {
						fileLocationInfo = fileLocationInfoList.remove(0);
					}
					
				}
			}
		}
		return fileLocationInfo;
	}
	
	
	/**
	 * This API generate multiple obejct based on geolocation. if #geolocation exists in source path, 
	 * we make 10 source url and corresponding 10 taget files are pushed on s3 storage.
	 * @param fileInfoList
	 * @return
	 */
	private List<FileLocationInfo> getFileLocationInfoListByGeo(List<FileLocationInfo> fileInfoList) {
		try {
			if (fileInfoList != null && fileInfoList.size() > 0) {
				List<FileLocationInfo> fileInfoByMsid = new ArrayList<FileLocationInfo>(fileInfoList.size() * 10);

				for (FileLocationInfo fInfo : fileInfoList) {
					String sourseUrl = fInfo.getSourceUrl();					
					String fileName = fInfo.getNetstorageLocation().getFileName();

					if (sourseUrl.indexOf("#geolocation") != -1 && fileName.indexOf("#geolocation") != -1) {

						for (int i=0; i < 10; i++) {
							String geoSourseUrl = sourseUrl.replace("#geolocation", String.valueOf(i));

							String geoFileName = fileName.replace("#geolocation", String.valueOf(i));

							NetstorageInfo ftpInfo = new NetstorageInfo();
							ftpInfo.setFileName(geoFileName);
							ftpInfo.setId(fInfo.getNetstorageLocation().getId());
							ftpInfo.setFolderLocation(fInfo.getNetstorageLocation().getFolderLocation());
							ftpInfo.setPassword(fInfo.getNetstorageLocation().getPassword());
							ftpInfo.setServerName(fInfo.getNetstorageLocation().getServerName());
							ftpInfo.setUserid(fInfo.getNetstorageLocation().getUserid());

							FileLocationInfo info = new FileLocationInfo();
							info.setId(fInfo.getId());
							info.setSourceUrl(geoSourseUrl);
							info.setStatus(fInfo.isStatus());
							info.setNetstorageLocation(ftpInfo);

							fileInfoByMsid.add(info);
						}
					} else {
						fileInfoByMsid.add(fInfo);
					}
				}

				return fileInfoByMsid;

			}
		} catch (Exception e) {
			//e.printStackTrace();
			logger.debug("msg : " + e.getMessage());
		}
		return fileInfoList;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");
		startTime = System.currentTimeMillis();
		JobParameters jobParams = stepExecution.getJobParameters();
		int workType = Integer.parseInt(jobParams.getString(Constant.JOB_PARAM_JOBTYPE_ID));
		
		// page no is applicable job is going to restart so that it will run from next page where batch running.
		if (jobParams.getString(Constant.JOB_PARAM_NL_RESTART_KEY) != null) {
			int pageNo = (int)jobParams.getLong(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO);
			this.pageNo = ++pageNo;
		} 
		
		this.isInitialized = true;
		
		if (workType != 0) {
			this.fileLocationInfoList = netstorageFileInfoDao.getFileLocationInfo(workType, pageNo, perPage);
			if (fileLocationInfoList != null && fileLocationInfoList.size() > 0) {
				this.fileLocationInfoList = getFileLocationInfoListByGeo(fileLocationInfoList);
			}
			
			
		}
		
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		
		this.isInitialized = false;
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}
		
		return stepExecution.getExitStatus();
	}

	/**
	 * @param netstorageFileInfoDao the netstorageFileInfoDao to set
	 */
	public void setNetstorageFileInfoDao(NetstorageFileInfoDao netstorageFileInfoDao) {
		this.netstorageFileInfoDao = netstorageFileInfoDao;
	}
	
}