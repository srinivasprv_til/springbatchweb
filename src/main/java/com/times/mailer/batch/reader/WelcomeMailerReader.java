package com.times.mailer.batch.reader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.ConnectException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.times.common.dao.MongoRestAPIService;

public class WelcomeMailerReader implements ItemReader<JSONArray>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(WelcomeMailerReader.class);

	public static final String DB_COLLECTION = "welcomeMailerTracker";
	public static String MONGO_REST_API_URL = "http://mra.indiatimes.com/mra/";
	private static final String getURL = "get/#collection?query=#query";
	private static final String MailToListURL_np = "http://mra.indiatimes.com/mra/get/offlineuserfeednp_coke?#query#&sort={\"inserted_date\":1}&limit=500";
	private static final String MailToListURL_toi = "http://mra.indiatimes.com/mra/get/offlineuserfeedtoi_coke?#query#&sort={\"inserted_date\":1}&limit=500";

	public static String returnInsertedData_np() throws IOException {

		String fileName = "/opt/mailer/logs_np.txt";
		String line = null;
		String insertDate = null;

		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while ((line = bufferedReader.readLine()) != null) {
				insertDate = line;
			}

			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			logger.error("Unable to open file '" + fileName + "'");
		}

		return insertDate;
	}

	public static String returnInsertedData_toi() throws IOException {

		String fileName = "/opt/mailer/logs_toi.txt";
		String line = null;
		String insertDate = null;

		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while ((line = bufferedReader.readLine()) != null) {
				insertDate = line;
			}

			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			logger.error("Unable to open file '" + fileName + "'");
		}

		return insertDate;
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonArrayText = readAll(rd);
			if(jsonArrayText != null && StringUtils.isNoneBlank(jsonArrayText)){
				JSONArray json = new JSONArray(jsonArrayText);
				return json;	
			}
			else{
				JSONArray json = new JSONArray();
				return json;
			}
		} finally {
			is.close();
		}
	}

	public static boolean exist(String collection, DBObject query) {
		String extData = null;
		String strURL = null;
		strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll("#collection",
				collection);
		strURL = MONGO_REST_API_URL + strURL;
		extData = MongoRestAPIService.getExtData(strURL); 
		if(extData != null && StringUtils.isNoneBlank(extData) && extData.length() > 2) {
			return true;
		}
		return false;
	}

	@Override
	public ExitStatus afterStep(StepExecution arg0) {
		return null;
	}

	@Override
	public void beforeStep(StepExecution arg0) {

	}

	@Override
	public JSONArray read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		logger.debug("Welcome-mailer read method start..");
		
		try {
			TimeUnit.MILLISECONDS.sleep(500);
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}

		DBObject queryObject = new BasicDBObject();
		queryObject.put("feed_type", "raw");

		String insertDate = returnInsertedData_np();
		if (insertDate != null) {
			SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

			Date insertdate = simpleFormat.parse(insertDate);
			Calendar c = Calendar.getInstance();

			c.setTime(insertdate);
			c.add(Calendar.HOUR, 5);
			c.add(Calendar.MINUTE, 30);

			BasicDBObject date = new BasicDBObject();
			date.append("$gt", c.getTime());
			queryObject.put("inserted_date", date);
		}
		else{
			Calendar c = Calendar.getInstance();
			BasicDBObject date = new BasicDBObject();
			date.append("$gt", c.getTime());
			queryObject.put("inserted_date", date);
		}

		String queryString = MailToListURL_np.replace("#query#",
				"query=" + URLEncoder.encode(queryObject.toString(), "UTF-8"));
		
		logger.debug("getting data for NP from mongo {}",queryString);
		JSONArray listOFJson_np = readJsonFromUrl(queryString);
		

		String insertDate_toi = returnInsertedData_toi();
		if (insertDate_toi != null) {
			SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

			Date insertdate = simpleFormat.parse(insertDate_toi);
			Calendar c = Calendar.getInstance();

			c.setTime(insertdate);
			c.add(Calendar.HOUR, 5);
			c.add(Calendar.MINUTE, 30);

			BasicDBObject date = new BasicDBObject();
			date.append("$gt", c.getTime());
			queryObject.put("inserted_date", date);
		}
		else{
			Calendar c = Calendar.getInstance();
			BasicDBObject date = new BasicDBObject();
			date.append("$gt", c.getTime());
			queryObject.put("inserted_date", date);
		}

		String queryString_toi = MailToListURL_toi.replace("#query#",
				"query=" + URLEncoder.encode(queryObject.toString(), "UTF-8"));
		
//		String queryString_toi = "http://mra.indiatimes.com/mra/get/offlineuserfeedtoi_coke?query=%7B+%22feed_type%22+%3A+%22raw%22+%2C+%22inserted_date%22+%3A+%7B+%22%24gt%22+%3A+%7B+%22%24date%22+%3A+%222016-05-25T18%3A33%3A29.854Z%22%7D%7D%7D&sort={%22inserted_date%22:1}";

		
		logger.debug("getting data for TOI from mongo {}",queryString_toi);
		JSONArray listOFJson_toi = readJsonFromUrl(queryString_toi);
		JSONArray newListOfJson = new JSONArray();
		String fileName_np = "/opt/mailer/logs_np.txt";
		String fileName_toi = "/opt/mailer/logs_toi.txt";
			try {
				FileWriter fileWriter = new FileWriter(fileName_np, false);
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
				String insertDate1 = null;
				for (int i = 0; i < listOFJson_np.length(); i++) {
					JSONObject jsonObject = listOFJson_np.getJSONObject(i);
					JSONObject newJsonObject = new JSONObject();
					JSONObject raw_data = (JSONObject) jsonObject.get("raw_data");
					if (raw_data != null && raw_data.has("email") && raw_data.has("channel")
							&& raw_data.has("languages")) {
						newJsonObject.put("email", raw_data.get("email"));
						newJsonObject.put("channel", raw_data.get("channel"));
						newJsonObject.put("languages", raw_data.get("languages"));
//						newJsonObject.put("email", "siddhantiitbmittal3@gmail.com");
						// newJsonObject.put("channel", "TOI");
//						newJsonObject.put("languages", "4");
						JSONObject newJsonObject1 = (JSONObject) jsonObject.get("inserted_date");
						insertDate1 = newJsonObject1.getString("$date");
						

						DBObject dbObject = (DBObject) JSON.parse(newJsonObject.toString());
						DBObject dbObject_verify_exist = dbObject;
						dbObject_verify_exist.removeField("languages");
						if (!exist(DB_COLLECTION, dbObject_verify_exist)
								&& MongoRestAPIService.insert(DB_COLLECTION, dbObject)) {
							logger.debug("Inserted into in welcomeMailerTracker collection! {}",newJsonObject.toString());
							newListOfJson.put(newJsonObject);
						}
					}

				}
				
				if(insertDate1 != null){
					bufferedWriter.write(insertDate1);
				}
				else{
					Date cDate = Calendar.getInstance().getTime();
					Calendar c = Calendar.getInstance();
					c.setTime(cDate);
					c.add(Calendar.HOUR, -5);
					c.add(Calendar.MINUTE, -30);
					SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
					bufferedWriter.write(simpleFormat.format(c.getTime()));
				}

				FileWriter fileWriter_toi = new FileWriter(fileName_toi, false);
				BufferedWriter bufferedWriter_toi = new BufferedWriter(fileWriter_toi);
				for (int i = 0; i < listOFJson_toi.length(); i++) {
					JSONObject jsonObject = listOFJson_toi.getJSONObject(i);
					JSONObject newJsonObject = new JSONObject();
					JSONObject raw_data = (JSONObject) jsonObject.get("raw_data");
					if (raw_data != null && raw_data.has("email") && raw_data.has("channel")) {
						newJsonObject.put("email", raw_data.get("email"));
						newJsonObject.put("channel", raw_data.get("channel"));
						newJsonObject.put("languages", "1");
						// newJsonObject.put("email",
						// "siddhantiitbmittal3@gmail.com");
						JSONObject newJsonObject1 = (JSONObject) jsonObject.get("inserted_date");
						insertDate1 = newJsonObject1.getString("$date");

						DBObject dbObject = (DBObject) JSON.parse(newJsonObject.toString());
						DBObject dbObject_verify_exist = dbObject;
						dbObject_verify_exist.removeField("languages");
						if (!exist(DB_COLLECTION, dbObject_verify_exist)
								&& MongoRestAPIService.insert(DB_COLLECTION, dbObject)) {
							logger.debug("Inserted into in welcomeMailerTracker collection! {}",newJsonObject.toString());
							newListOfJson.put(newJsonObject);
						}

					}
				}
				
				if(insertDate1 != null){
					bufferedWriter_toi.write(insertDate1);
				}
				else{
					Date cDate = Calendar.getInstance().getTime();
					Calendar c = Calendar.getInstance();
					c.setTime(cDate);
					c.add(Calendar.HOUR, -5);
					c.add(Calendar.MINUTE, -30);
					SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
					bufferedWriter_toi.write(simpleFormat.format(c.getTime()));
				}

				bufferedWriter_toi.close();
				bufferedWriter.close();
				fileWriter_toi.close();
				fileWriter.close();

			} catch (IOException ex) {
				logger.error("Error writing to file '" + fileName_np + "'");
			}
				return newListOfJson;


	}

}
