/**
 * 
 */
package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.util.StringUtils;

import com.mongodb.DBObject;
import com.times.common.mail.EmailProcessor;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.common.util.MailerUtil;
import com.times.common.util.TravelAlertUtil;
import com.times.common.wrapper.NewsletterDBObjectWrapper;
import com.times.mailer.batch.MailerReader;
import com.times.mailer.dao.CustomParamDao;
import com.times.mailer.dao.TravelNewsletterDao;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetterDailyTracker;
import com.times.mailer.model.TravelAlertUser;

/**
 * <p>
 *  This Reader class is used for Travel Alert Newsletter to do the following stuff:
 * <ul>
 *   <li>To Read the 100 users email id from the newsletter subscription for the travel mongoDB</li>
 *   <li>To Get the user preference from the SSO and pass the control to the processor to search the 
 *   article filled in the last 24 hours so that that content can be sent to the end users</li>
 * </ul>
 * </p>
 * 
 * @author ranjeet.jha
 *
 */
public class TravelAlertNewsletterReader  implements ItemReader<TravelAlertUser>, StepExecutionListener { //, JobExecutionListener

	//private final static Logger logger = Logger.getLogger(MailerReader.class);
	private final static Logger logger = LoggerFactory.getLogger(MailerReader.class);
	
	// this reference is there because if content not found then send mail to ..
	private EmailProcessor smtpMailer;
	private Mailer myMailer;
	private TravelNewsletterDao travelNewsletterDao;
	private static String alertSubject = null;
	
	private int perPage = 100;
	private int pageNo = 0;
	private String newsLetterId = null;
	//private boolean isDaily = true; // if not daily then weekly newsletter.
	//private long morningOrEvening = 1; // 1 for morning and 2 for evening.
	//private long dailyOrWeekly = 1; //// 1 for daily and 2 for weekly.
	
	private List<TravelAlertUser> lstSubscribers = Collections.synchronizedList(new ArrayList<TravelAlertUser>());
	
	//code changes ... 
	private boolean isInitialized = false;
	private String cacheUIKey = null;
	private String ssoUrl;
	
	// This travelNewsletterDao interface is used to keep pageNo for the running job with some stuff
	private CustomParamDao customParamDao;
	
	public static final String NL_FORMAT_TEXT = "text";
	public static final String NL_FORMAT_HTML = "html";
	public static final String NL_FORMAT_PARAM = "nlformat";
	public static final long TIME_INTERVAL = 10;
	
	/**
	 * This method is used to read the {@link MailUser} object.
	 * 
	 * @param args
	 */
	public TravelAlertUser read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		logger.debug("iread method start..");
		
	
		if (myMailer != null && lstSubscribers != null && lstSubscribers.size() == 0) {
			// push MailUser into the List
			
			this.pushSubscribersIntoList(myMailer, perPage, pageNo );
			
			// add user preferences into collection
			TravelAlertUtil.UpdatePreferencesForUsers(lstSubscribers,ssoUrl);
			
			// Update MSIds into the collection
			TravelAlertUtil.updateMSID(lstSubscribers);
			
			// initialize the content Piece for mailer body
			//TravelAlertUtil.setContentMap(lstSubscribers);
			
			
			// for testing purpose.
			//TravelAlertUtil.removeIfNoPreference(lstSubscribers);
			if (lstSubscribers != null && lstSubscribers.size() > 0) {
				Iterator<TravelAlertUser> it = lstSubscribers.iterator();
				while(it.hasNext()) {
					TravelAlertUser u = it.next();
					if(u.getPreferences() == null) {
						//System.out.println("emailId :" + u.getEmailId() + " no preferences");
						it.remove();
					} else {
						System.out.println("emailId :" + u.getEmailId() + ", prefrences: " + u.getPreferences());
					}
				}
			}
			
			logger.debug("Updatation done");
			
		}
		
		// if subscriber got then 
		if (lstSubscribers.size() > 0) {
			//counter update in memory
			TravelAlertUser subscriberUser = null;
			subscriberUser = getSubscriber();
			subscriberUser.setEmailSubject(alertSubject);
			return subscriberUser;
		} else {
			//updates stats for the newsmailer ...
			/*for (Map.Entry<String, Integer> entry : InMemoryData.emailSentCounter.entrySet()) {
				this.updateEmailStats(entry.getKey());
			}*/
			
			//this.removeInMemoryData();
		}
		
		return null;
	}


	/**
	 * Get the Newsletter user/subscriber, if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private TravelAlertUser getSubscriber() {
		TravelAlertUser subscriberUser = null;
		synchronized (this) {
			if (lstSubscribers != null && lstSubscribers.size() > 0) {
				subscriberUser = lstSubscribers.remove(0);
				// update counter in map
				//this.updateCounterInMap(subscriberUser);
			}
		}
		return subscriberUser;
	}
	
	/**
	 * @param subscriberUser
	 */
	private void updateCounterInMap(MailUser subscriberUser) {
		//logger.debug("updateCounterInMap method ..");
		try {
			int lastCounter = 1;
			if (subscriberUser != null && subscriberUser.getNewsletter() != null) {
				int dailyOrWeekly = subscriberUser.getNewsletter().isDaily() ? 1: 2;
				int morningOrEvening = subscriberUser.getNewsletter().getScheduleTime();
				String sKey = subscriberUser.getNewsletter().getId(); 
				sKey = sKey + Constant.KEY_BUILDER_SEPERATOR + dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + morningOrEvening;
				if ( InMemoryData.emailSentCounter.containsKey(sKey)) {
					lastCounter = InMemoryData.emailSentCounter.get(sKey);
					InMemoryData.emailSentCounter.put(sKey, ++lastCounter);
				} else {
					InMemoryData.emailSentCounter.put(sKey, new Integer(lastCounter));
				}
			}
		} catch (Exception e) {
			logger.error("in side the updateCounterInMap method, msg : " + e.getMessage());
		}
	}

	/**
	 * This method is used to update stats finally for one newsletter.
	 * 
	 * @param sKey
	 */
	private void updateEmailStats(String sKey) {
		int iCount = InMemoryData.emailSentCounter.get(sKey);
		int ireadCount = 0;
		this.getTravelNewsletterDao().addMailerStats(sKey, 0, iCount);
		String code = null;
		if (InMemoryData.keyCodeMap.containsKey(sKey)) {
			code = InMemoryData.keyCodeMap.get(sKey);
		}
		//travelNewsletterDao.updateNewsLetterDailyTracker(code, sKey, ireadCount, iCount);
	}

	//public void List<DBObject> get
	
	/**
	 * This method is used to push the subscriber into the List of {@link MailUser}.
	 *  
	 */
	private void pushSubscribersIntoList(Mailer myMailer, int perPage, int pageNo) {
		long dailyOrWeekly = 1;
		if (lstSubscribers == null || lstSubscribers.isEmpty()){
			this.pageNo = ++pageNo;
			
			try {
				
				// update the page no for this nlid, dailyWeekly.
				customParamDao.addOrupdatePageNo(myMailer.getId(), dailyOrWeekly, myMailer.getScheduleTime(), pageNo);
				
				logger.debug("NLID : " + myMailer.getId() + " , pageNo : " + pageNo);
				List<DBObject> subscriberDBObjects = travelNewsletterDao.getSubscribers(pageNo, perPage, myMailer);
				
				if (subscriberDBObjects != null && subscriberDBObjects.size() > 0) {
					lstSubscribers = NewsletterDBObjectWrapper.getTravelAlertUserList(subscriberDBObjects, myMailer);
				}
				
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	
			
	/**
	 * This method is used to get the Mailer i.e. Newsletter master object for the mailer.
	 * 
	 * @param nlId
	 * @param isDaily
	 * @return
	 */
	private List<Mailer> getMailer(String nlId, boolean isDaily) {
		List<Mailer> mailerMaster = travelNewsletterDao.getNewsletterMailer(nlId, isDaily);
		return mailerMaster;
	}
		 
	/**
	 * This method is used to add the newsletter Body part in in-memory i.e. collection.
	 * 
	 * @param myMailer
	 */
	private void initializeNewsletter(Mailer myMailer) {
		try {
			
			String newsid = myMailer.getId();
			logger.debug("nl id :" + newsid + " going to pull data ...");
			int scheduledTime = myMailer.getScheduleTime();
			int daily_weekly = myMailer.isDaily() ? 1 : 2;
			String skey1 = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + scheduledTime ;
			String uniqueCode = addNewsLetterDailyTrackerIfNotfound(myMailer, newsid, scheduledTime, daily_weekly, skey1);
			
			//get unique code... and pass it to template ... hash code (timestamp +skey1)
			String urlNewsletter =  myMailer.getHtmlPath(); //.replace("#$nlcode", uniqueCode); // HTML >>MAIN TEMPLATE :
			//String urlNewsletterWidget = myMailer.getTemplatepath();//.replace("#$nlcode",uniqueCode) ; // HTML FRAGMENT
			alertSubject = myMailer.getMailSubject();			
			// track the details for each day and generate the code.
			 
			//for html data 
			String scachekey = skey1 +Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_HTML;
			if (!InMemoryData.newsMailerUIMap.containsKey(scachekey)) {
				//code generate newslid ... 
				//	urlNewsletter = urlNewsletter.indexOf("?") != -1 ? urlNewsletter + "?nlformat=" + NL_FORMAT_HTML : urlNewsletter + "&nlformat=" + NL_FORMAT_HTML;
				String url4Html = urlNewsletter.indexOf("?") == -1 ? urlNewsletter +"?" : urlNewsletter + "&";
				url4Html = url4Html  + NL_FORMAT_PARAM + "=" + NL_FORMAT_HTML;
				String sData = MailerUtil.getData(url4Html);
				if (!StringUtils.hasText(sData)) {
					sData = MailerUtil.getData(url4Html);
				}
				if (sData != null) {
					sData = sData.replace("##code##", uniqueCode);
					InMemoryData.newsMailerUIMap.put(scachekey, sData);
				}
				
				// logg id data not found
				logIfDataNotFound(newsid, url4Html, sData);
			}
			
			logger.debug("cacheKey : " + scachekey);
		
		} catch (Exception e) {
			logger.error("exception occured while retreiving content , msg: " + e.getMessage());
		}
	}

	/**
	 * This method is used to logged the data.
	 * 
	 * @param url4Html
	 * @param sData
	 */
	private void logIfDataNotFound(String nlId, String url4Html, String sData) {
		String msg = null;
		if (sData != null) {
			msg =  "url : " + url4Html + " , len : " + sData.length() + " nlid : " + nlId;
			logger.debug(" url : " + url4Html + " , len : " + sData.length() + " nlid : " + nlId);
		} else {
			logger.debug("ALERT ALERT NOT FOUND data for url : " + url4Html + " , nlid : " + nlId);
			try {
				smtpMailer.processStart("DATA NOT FOUND for URL : " + url4Html + " , nlid : " + nlId  + " MSG : " + msg);
			} catch (Exception e) {
				logger.debug("EXception url : " + url4Html + " , nlid : " + nlId + e.getMessage()); ;
			}
		}
	}

	/**
	 * This method is used to add the newsLetterTracker if not found for that days, frequency, time and nlId.
	 * 
	 * @param myMailer
	 * @param newsid
	 * @param iTime
	 * @param itype
	 * @param skey1
	 */
	private String addNewsLetterDailyTrackerIfNotfound(Mailer myMailer, String newsid, int iTime, int itype, String skey1) {
		String uniqueCode= null;
		String yyymmdd = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		DBObject nlTracker = travelNewsletterDao.getNewsLetterDailyTracker(newsid, yyymmdd, String.valueOf(iTime), String.valueOf(itype));
		if (nlTracker == null) {
			uniqueCode = getUniqueCodeByDate(skey1);
			NewsLetterDailyTracker nlDailyTracker = new NewsLetterDailyTracker(new Date(), Integer.parseInt(myMailer.getId()), uniqueCode, iTime, itype, yyymmdd);
			travelNewsletterDao.addNewsLetterDailyTracker(nlDailyTracker); 
			
		} else {
			uniqueCode = String.valueOf(nlTracker.get(NewsLetterDailyTracker.NEWS_LETTER_CODE));
		}
		if (!InMemoryData.keyCodeMap.containsKey(skey1)) {
			InMemoryData.keyCodeMap.put(skey1, uniqueCode);
		}
		
		return uniqueCode;
	}
	
	/**
	 * This method is used to get the StringCode.
	 * @param str
	 * @return
	 */
	private String getUniqueCodeByDate(String key) {
		String date = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		String code = MailerUtil.getUniqueCodeByDate(date + key);
		return code;	
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");
		long dailyOrWeekly = 1;
		long morningOrEvening = 1;
		JobParameters jobParams = stepExecution.getJobParameters();
		this.newsLetterId = jobParams.getString(Constant.JOB_PARAM_NEWSLETTER_ID);
		
		
		// page no is applicable job is going to restart so that it will run from next page where batch running.
		if (jobParams.getString(Constant.JOB_PARAM_NL_RESTART_KEY) != null) {
			int pageNo = (int)jobParams.getLong(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO);
			this.pageNo = ++pageNo;
		} 
		
		//List<Mailer> mailers = travelNewsletterDao.getNewsletterMailer(newsLetterId, isDaily);
		List<Mailer> mailers = this.getMailer(newsLetterId, true);
		if (mailers != null && mailers.size() > 0) {
			this.myMailer = mailers.get(0);
		}
		if (myMailer != null) {
			// get mailer body content into memory
			this.initializeNewsletter(myMailer);
		}
		this.isInitialized = true;
		
		this.cacheUIKey = this.newsLetterId + Constant.KEY_BUILDER_SEPERATOR + dailyOrWeekly;
		
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		
		this.removeInMemoryData();
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		return stepExecution.getExitStatus();
	}

	/**
	 * This method is used to remove the in-memory data for the that newsletter.
	 */
	private void removeInMemoryData() {
		if (InMemoryData.newsMailerDataMap.containsKey(this.cacheUIKey)){
			InMemoryData.newsMailerDataMap.remove(this.cacheUIKey);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_HTML)){
			InMemoryData.newsMailerUIMap.remove(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_HTML);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_TEXT)){
			InMemoryData.newsMailerUIMap.remove(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_TEXT);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey)) {
			InMemoryData.emailSentCounter.remove(this.cacheUIKey);
		}
	}

	
	/**
	 * @return
	 */
	public Mailer getMyMailer() {
		return myMailer;
	}

	/**
	 * @param myMailer
	 */
	public void setMyMailer(Mailer myMailer) {
		this.myMailer = myMailer;
	}

	/**
	 * @param smtpMailer the smtpMailer to set
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}
	/**
	 * @param customParamDao the customParamDao to set
	 */
	public void setCustomParamDao(CustomParamDao customParamDao) {
		this.customParamDao = customParamDao;
	}


	/**
	 * @return the travelNewsletterDao
	 */
	public TravelNewsletterDao getTravelNewsletterDao() {
		return travelNewsletterDao;
	}


	/**
	 * @param travelNewsletterDao the travelNewsletterDao to set
	 */
	public void setTravelNewsletterDao(TravelNewsletterDao travelNewsletterDao) {
		this.travelNewsletterDao = travelNewsletterDao;
	}
	
	/**
	 * @param ssoUrl
	 */
	public void setSsoUrl(String ssoUrl) {
		this.ssoUrl = ssoUrl;
	}
	
}