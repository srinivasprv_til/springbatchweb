package com.times.mailer.batch.reader;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.ETWealthDaoInterface;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.InMemoryData;

public class ETWealthReader implements ItemReader<Map<String, Date>>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(ETWealthReader.class);

	private ETWealthDaoInterface etWealthdao;
	
	private static int year = 2010;
	
	private static int week = 1;
	
	private static Date today_date = new Date();
	
	private static final String ET_WEALTH_STATUS_COLLECTION = "ETWealthStatus";
	
	private static boolean first_time = true;
	
	private static Map<String, Date> bufferMap = new HashMap<String, Date>();
	
	public Map<String, Date> read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		logger.debug("etwealth read method start..");
		
		//set success status to false by default
		//InMemoryData.success_status_ibeat = false;
		
		Map<String, Date> batchMap = new HashMap<String, Date>();
		
		Date start_date = getMondayOfWeek(year, week);
		
		if(start_date.compareTo(today_date) > 0)
			return null;
		
		//just for debugging
		//if(week > 3)
		//	return null;
		
		//run only once
		if(first_time == false){
			return null;
		}
		
		//store values in bufferMap until we get atmost 50 articles
		//according to logic, loop may never break here
		while(bufferMap.size() < 50){
			Calendar c = Calendar.getInstance();
			c.setTime(start_date);
			c.add(Calendar.DATE, 1);
			Date end_date = c.getTime();//getMondayOfWeek(year, week+1);
			//update year and week at the end of year
			if(c.get(Calendar.YEAR) > year){
				year = c.get(Calendar.YEAR);
				week = c.get(Calendar.WEEK_OF_YEAR);
			}
			
			//greater than present week
			if(start_date.compareTo(new Date()) > 0)
				break;
			
			Map<String, Date> msidMap = etWealthdao.getMsids(start_date, end_date);
			//store into buffer only if total number of articles is lessthan 51
			if((msidMap.size() + bufferMap.size()) < 51) {
				bufferMap.putAll(msidMap);
			} else {
				//if msdiMap for singleday is morethan 50 we need to process in turns
				if(msidMap.size() > 50 && bufferMap.size() == 0){
					bufferMap.putAll(msidMap);
					int i = 0;
					//process 50 articles
					for(Iterator<Map.Entry<String, Date>> it = bufferMap.entrySet().iterator(); it.hasNext();){
						Map.Entry<String, Date> entry = it.next();
						Date date = entry.getValue();
						String msid = entry.getKey();
						batchMap.put(msid, date);
						it.remove();
						i++;
						if(i >= 50)
							break;
					}
					return batchMap;
					
				}
				break;
			}
			
			week = week + 1;
			start_date = getMondayOfWeek(year, week);
		}
		
		batchMap = bufferMap;
		
		first_time = false;
		
		return batchMap;
	}
	
	private Date getMondayOfWeek(int year, int week){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.WEEK_OF_YEAR, week);        
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		cal.set(Calendar.YEAR, year);
		return cal.getTime();
	}
	
	public ETWealthDaoInterface getEtWealthdao() {
		return etWealthdao;
	}


	public void setEtWealthdao(ETWealthDaoInterface etWealthdao) {
		this.etWealthdao = etWealthdao;
	}


	public void beforeStep(StepExecution stepExecution) {		
		logger.debug(" beforeStep method..");
		
		//set success status to false by default
		InMemoryData.success_status_ibeat = false;
		
		//Status of the batch is stored in MongoDB 
		//(year, week) tells us which day to start the process from
		DBObject queryJson = new BasicDBObject();
		queryJson.put("DOC_TYPE", "STATUS");
		DBObject status_object = MongoRestAPIService.get(ET_WEALTH_STATUS_COLLECTION, queryJson);
		if(status_object != null) {
			year = (Integer) status_object.get("PROCESSED_YEAR");
			week = (Integer) status_object.get("PROCESSED_WEEK");
		}
		
		logger.debug(" before step method exiting");
	}

	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		
		//Status of the batch is stored in MongoDB, if batch is success
		//(year, week) tells us until which day process is complete
		if(InMemoryData.success_status_ibeat == true){
			DBObject status_object = new BasicDBObject();
			status_object.put("DOC_TYPE", "STATUS");
			status_object.put("PROCESSED_YEAR", year);
			status_object.put("PROCESSED_WEEK", week);
			
			DBObject queryJson = new BasicDBObject();
			queryJson.put("DOC_TYPE", "STATUS");
			
			MongoRestAPIService.update(ET_WEALTH_STATUS_COLLECTION, queryJson, status_object, true);
		}
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		return stepExecution.getExitStatus();
	}

}
