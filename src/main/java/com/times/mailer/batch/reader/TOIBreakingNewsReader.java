package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.DBObject;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.common.util.MailerUtil;
import com.times.mailer.dao.CustomParamDao;
import com.times.mailer.dao.MailerDao;
import com.times.mailer.dao.SQLMailerDAO;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetterDailyTracker;
import com.times.nbt.dao.SqlServerDAO;
import com.times.nbt.dao.SqlServerDAOImpl;
import com.times.nbt.model.NewsInfo;


public class TOIBreakingNewsReader implements ItemReader<MailUser>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(TOIBreakingNewsReader.class);

	private MailerDao dao;
	private int perPage = 100;
	private int pageNo = 0;

	private List<MailUser> lstSubscribers = Collections.synchronizedList(new ArrayList<MailUser>());

	//code changes ... 
	boolean isInitialized = false;
	private long startTime = System.currentTimeMillis();
	private Mailer myMailer;
	private String newsLetterId = null;
	boolean isBatchExecutionValid = true;

	public static final String NL_FORMAT_TEXT = "text";
	public static final String NL_FORMAT_HTML = "html";
	public static final String NL_FORMAT_PARAM = "nlformat";
	public static final long TIME_INTERVAL = 10;

	private CustomParamDao customParamDao;

	private SqlServerDAO sqlServerDAO;
	
	private long morningOrEvening = 1; // 1 for morning and 2 for evening.
	private long dailyOrWeekly = 1; //// 1 for daily and 2 for weekly.


	public MailUser read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {	
		logger.debug("iread method start..");

		if (isBatchExecutionValid) {

			if (myMailer != null) {
				this.pushSubscribersIntoList(myMailer, perPage, pageNo);
			}

			// if subscriber got then 
			if (lstSubscribers.size() > 0) {
				//counter update in memory
				MailUser subscriberUser = null;
				subscriberUser = getSubscriber();

				return subscriberUser;
			} else {
				//updates stats for the newsmailer ...
				for (Map.Entry<String, Integer> entry : InMemoryData.emailSentCounter.entrySet()) {
					this.updateEmailStats(entry.getKey());
				}

			}
		}

		return null;
	}


	private void pushSubscribersIntoList(Mailer myMailer, int perPage, int pageNo) {
		long dailyOrWeekly = 0;
		if (lstSubscribers == null || lstSubscribers.isEmpty()){
			long startTime = System.currentTimeMillis();
			synchronized (this) {
				this.pageNo = ++pageNo;

				try {
					if(myMailer.isDaily()) {
						dailyOrWeekly = 1;
					} else {
						dailyOrWeekly = 2;
					}
					// update the page no for this nlid, dailyWeekly.
					customParamDao.addOrupdatePageNo(myMailer.getId(), dailyOrWeekly, myMailer.getScheduleTime(), pageNo);

				} catch (Exception e) {

				}

				logger.debug("NLID : " + myMailer.getId() + " , pageNo : " + pageNo);
				lstSubscribers = dao.getSubscriber(myMailer, perPage, pageNo);
				logger.debug("100 User pull time : " + (System.currentTimeMillis() - startTime) + " ms");
			}

		}
	}


	private MailUser getSubscriber() {
		MailUser subscriberUser = null;
		synchronized (this) {
			if (lstSubscribers != null && lstSubscribers.size() > 0) {
				subscriberUser = lstSubscribers.remove(0);
				// update counter in map
				this.updateCounterInMap(subscriberUser);
			}			

		}
		return subscriberUser;
	}


	private void updateEmailStats(String sKey) {
		int iCount = InMemoryData.emailSentCounter.get(sKey);
		int ireadCount = 0;
		this.getDao().addMailerStats(sKey, 0, iCount);
		String code = null;
		if (InMemoryData.keyCodeMap.containsKey(sKey)) {
			code = InMemoryData.keyCodeMap.get(sKey);
		}
		dao.updateNewsLetterDailyTracker(code, sKey, ireadCount, iCount);
	}


	/**
	 * @param subscriberUser
	 */
	private void updateCounterInMap(MailUser subscriberUser) {

		try {
			int lastCounter = 1;
			if (subscriberUser != null && subscriberUser.getNewsletter() != null) {

				String sKey = subscriberUser.getNewsletter().getId(); 
				
				sKey = sKey + Constant.KEY_BUILDER_SEPERATOR + dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + morningOrEvening;
				if ( InMemoryData.emailSentCounter.containsKey(sKey)) {
					lastCounter = InMemoryData.emailSentCounter.get(sKey);
					InMemoryData.emailSentCounter.put(sKey, ++lastCounter);
				} else {
					InMemoryData.emailSentCounter.put(sKey, new Integer(lastCounter));
				}
			}
		} catch (Exception e) {
			logger.error("in side the updateCounterInMap method, msg : " + e.getMessage());
		}
	}




	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");
		startTime = System.currentTimeMillis();
		JobParameters jobParams = stepExecution.getJobParameters();
		this.newsLetterId = jobParams.getString(Constant.JOB_PARAM_NEWSLETTER_ID);
		isBatchExecutionValid = jobPreExecutor(jobParams.getString("hostid"), newsLetterId);

		if (isBatchExecutionValid) {			
			List<Mailer> mailers = this.getMailer(newsLetterId, true);
			if (mailers != null && mailers.size() > 0) {
				this.myMailer = mailers.get(0);
			}
			if (myMailer != null) {
				// get mailer body content into memory
				this.initializeNewsletter(myMailer);
			}
		}

		// page no is applicable job is going to restart so that it will run from next page where batch running.
		if (jobParams.getString(Constant.JOB_PARAM_NL_RESTART_KEY) != null) {
			int pageNo = (int)jobParams.getLong(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO);
			this.pageNo = ++pageNo;
		} 

		this.isInitialized = true;	


	}


	private List<Mailer> getMailer(String nlId, boolean isDaily) {
		List<Mailer> mailerMaster = dao.getNewsletterMailer(nlId, isDaily);
		return mailerMaster;
	}



	private void initializeNewsletter(Mailer myMailer) {
		try {

			String newsid = myMailer.getId();
			logger.debug("nl id :" + newsid + " going to pull data ...");
			String cmsMappingId = myMailer.getCmsMappingId();
			int scheduledTime = myMailer.getScheduleTime();
			int daily_weekly = myMailer.isDaily() ? 1 : 2;
			this.dailyOrWeekly = daily_weekly;
			this.morningOrEvening = scheduledTime;

			String skey1 = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + scheduledTime ;
			String uniqueCode = addNewsLetterDailyTrackerIfNotfound(myMailer, newsid, scheduledTime, daily_weekly, skey1);


			String urlNewsletter =  myMailer.getHtmlPath();
			String urlNewsletterWidget = myMailer.getTemplatepath();

			//for html data 
			String scachekey = skey1 +Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_HTML;
			if (!InMemoryData.newsMailerUIMap.containsKey(scachekey)) {

				String url4Html = urlNewsletter.indexOf("?") == -1 ? urlNewsletter +"?" : urlNewsletter + "&";
				url4Html = url4Html  + NL_FORMAT_PARAM + "=" + NL_FORMAT_HTML;
				String sData = MailerUtil.getDataRecursively(url4Html, 1);

				if (sData != null) {
					sData = sData.replaceAll("##code##", uniqueCode);
					sData = com.times.common.util.StringUtils.replaceSpecialChar(sData);
					InMemoryData.newsMailerUIMap.put(scachekey, sData);
				}


				if (sData.indexOf("<!--###") != -1) {
					String subjectLine = sData.substring(sData.indexOf("<!--###") + 7, sData.indexOf("###-->"));
					InMemoryData.newsMailerUIMap.put(skey1 + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1", subjectLine);
				}


			}			


		} catch (Exception e) {
			logger.error("exception occured while retreiving content , msg: " + e.getMessage());

		}
	}


	private boolean jobPreExecutor(String hostid, String newsLetterId) {
		int lastBnewsId = customParamDao.getBNewsId(hostid);
		List<NewsInfo> breakingNewsList = sqlServerDAO.getBreakingNewsData(hostid, lastBnewsId);
		int latestFetchedBnewsId = 0;
		if (breakingNewsList != null && breakingNewsList.size() > 0) {
			for (NewsInfo newsInfo : breakingNewsList) {
				latestFetchedBnewsId = newsInfo.getBnid();
				
				if (InMemoryData.newsletterContentMap.get(newsLetterId) != null) {
					InMemoryData.newsletterContentMap.put(newsLetterId,
							InMemoryData.newsletterContentMap.get(newsLetterId) + "<Br/>" + newsInfo.getBntext());
				} else {
					InMemoryData.newsletterContentMap.put(newsLetterId,newsInfo.getBntext());
				}
				
				
				if (InMemoryData.newsMailerUIMap.get(newsLetterId + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1") == null
						 || InMemoryData.newsMailerUIMap.get(newsLetterId + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1").isEmpty()) {
					if (newsInfo.getSubject() != null && newsInfo.getSubject().indexOf("<emailsubject>") != -1) {
						String subject = newsInfo.getSubject();
						String subjectLine = newsInfo.getSubject().
								substring(subject.indexOf("<emailsubject>") + 14, subject.indexOf("</emailsubject>"));
						InMemoryData.newsMailerUIMap.put(newsLetterId + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1", subjectLine);
					}
				}
			}
			customParamDao.updateBNewsId(hostid, latestFetchedBnewsId);
			return true;
		} else {
			return false;
		}

	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");

		this.isInitialized = false;

		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}

		return stepExecution.getExitStatus();
	}


	public CustomParamDao getCustomParamDao() {
		return customParamDao;
	}

	public void setCustomParamDao(CustomParamDao customParamDao) {
		this.customParamDao = customParamDao;
	}

	public MailerDao getDao() {
		return dao;
	}

	/**
	 * @param dao
	 */
	public void setDao(MailerDao dao) {
		this.dao = dao;
	}

	/**
	 * This method is used to add the newsLetterTracker if not found for that days, frequency, time and nlId.
	 * 
	 * @param myMailer
	 * @param newsid
	 * @param iTime
	 * @param itype
	 * @param skey1
	 */
	private String addNewsLetterDailyTrackerIfNotfound(Mailer myMailer, String newsid, int iTime, int itype, String skey1) {
		String uniqueCode= null;
		String yyymmdd = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		DBObject nlTracker = dao.getNewsLetterDailyTracker(newsid, yyymmdd, String.valueOf(iTime), String.valueOf(itype));
		if (nlTracker == null) {
			uniqueCode = getUniqueCodeByDate(skey1);
			NewsLetterDailyTracker nlDailyTracker = new NewsLetterDailyTracker(new Date(), Integer.parseInt(myMailer.getId()), uniqueCode, iTime, itype, yyymmdd);
			dao.addNewsLetterDailyTracker(nlDailyTracker); 

		} else {
			uniqueCode = String.valueOf(nlTracker.get(NewsLetterDailyTracker.NEWS_LETTER_CODE));
		}
		if (!InMemoryData.keyCodeMap.containsKey(skey1)) {
			InMemoryData.keyCodeMap.put(skey1, uniqueCode);
		}

		return uniqueCode;
	}

	/**
	 * This method is used to get the StringCode.
	 * @param str
	 * @return
	 */
	private String getUniqueCodeByDate(String key) {
		String date = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		String code = MailerUtil.getUniqueCodeByDate(date + key);
		return code;	
	}


	public SqlServerDAO getSqlServerDAO() {
		return sqlServerDAO;
	}


	public void setSqlServerDAO(SqlServerDAO sqlServerDAO) {
		this.sqlServerDAO = sqlServerDAO;
	}


}

