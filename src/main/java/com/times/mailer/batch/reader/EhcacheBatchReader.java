package com.times.mailer.batch.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.HttpException;
import org.jdom2.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.ibm.icu.util.RangeValueIterator.Element;

import com.times.common.util.ExternalHttpUtil;
import com.times.common.util.XMLReader;

public class EhcacheBatchReader implements ItemReader<Integer>, StepExecutionListener{
	
	private final static Logger logger = LoggerFactory.getLogger(EhcacheBatchReader.class);
	private List<Integer> msidList = Collections.synchronizedList(new ArrayList<Integer>());

	public Integer read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {		
		try {			
			if (msidList != null && msidList.size() > 0) {
				Integer msid = getMsid();
				return msid;
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("Error in OFFLINEFEEDMERGEREADER read() : "+e.getMessage());
		}
		return null;

	}

	
	private Integer getMsid() {
		Integer msid = null;
		synchronized (this) {
			if (msidList != null && msidList.size() > 0) {
				msid = msidList.remove(0);
			}
		}
		return msid;
	}




	public void beforeStep(StepExecution stepExecution) {
		try {
			// msidList = getListFromPropertiesFile();
			 msidList = getListFromSolr();
		} catch (Exception e) {
			logger.error("Error in OFFLINEFEEDMERGEREADER beforeStep() : "+e.getMessage());
		}
	}
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<Integer> getListFromSolr() throws FileNotFoundException {
		/*String solrUrl="http://solrsearch.indiatimes.com/TOISolrWebProject/travel/select?q=hostid:259"
				+ "&fq=contenttypeid:2 AND contentsubtypeid:9 AND mData_TemplateLayout:"+"Layout1"+" AND "
						+ "multiPublishMaster:1&sort=effectivedate desc&wt=json&indent=true&fl=msid&rows=500";*/
		
		String solrUrl="http://solrsearch.indiatimes.com/TOISolrWebProject/travel/select?q=hostid:259&fq=contenttypeid:2 AND contentsubtypeid:9 AND mData_TemplateLayout:Layout1 AND multiPublishMaster:1&sort=effectivedate desc&wt=json&indent=true&fl=msid&start=0&rows=200";
		
		List<Integer> msidList = new ArrayList<Integer>();
		try {
			String extdata = ExternalHttpUtil.getExtDataForSolar(solrUrl);
			System.out.println(solrUrl);
			logger.debug(extdata);
			
			// URL url = new URL(solrUrl);
			 JSONObject jsonObjectforurl = new JSONObject(extdata);
			 JSONObject jsonObject = (JSONObject) jsonObjectforurl.get("response");
			 int numFound = (int) jsonObject.get("numFound");
			
			 if(numFound > 0){
				JSONArray jsonArrayHelper = (JSONArray) jsonObject.get("docs");
				for(int i=0;i<jsonArrayHelper.length();i++)
				{
					JSONObject doc = new JSONObject();
					doc = (JSONObject) jsonArrayHelper.get(i);
					
					int msid = doc.getInt("msid");
						msidList.add(msid);
                }
			 }
		}catch (Exception e) {
			// TODO Auto-generated catch block
			 e.printStackTrace();
		}
		
		  return msidList;
		}
	
	
     public List<Integer> getListFromPropertiesFile() throws FileNotFoundException {
		String filePath = "/opt/svn/Staging/SpringBatchWeb/cityguidelist.properties";
		//File file = new File(filePath);
		List<Integer> msidList = new ArrayList<Integer>();
			 msidList.add(Integer.parseInt("23956884")); // delhi travel Guide
			 msidList.add(Integer.parseInt("48984959")); // Dharmshala  
		//msidList.add(Integer.parseInt("48744446"));
		//msidList.add(Integer.parseInt("37116850")); // Mussoorie travel Guide
		// msidList.add(Integer.parseInt("47249707")); //Jordan Travel Guide
		//msidList.add(Integer.parseInt("30570289")); //new York Travel Guide
		//msidList.add(Integer.parseInt("24807656")); //goa  Travel Guide
		//msidList.add(Integer.parseInt("53004545")); //test guide - 5098988
		//msidList.add(Integer.parseInt("29108178")); 
		//msidList.add(Integer.parseInt("53927033"));  // srinagar
	    /*if(FilenameUtils.isExtension(file.getAbsolutePath(), "properties")){
	    	InputStream input= new FileInputStream(file.getAbsolutePath());
	    	Properties properties = new Properties();
	    	try {
				properties.load(input);
				for (String key : properties.stringPropertyNames()) {
					
					msidList.add(Integer.parseInt(key));
					logger.debug("msid added from property file key={}"+key);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
	    	
	    }*/
	    
	
		return msidList;
	}
	



	 

}
