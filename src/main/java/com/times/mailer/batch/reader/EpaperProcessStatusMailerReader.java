package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.EmailVO;
import com.times.common.util.Constant;


public class EpaperProcessStatusMailerReader implements ItemReader<List<EmailVO>>,StepExecutionListener {
	private final static Logger logger = LoggerFactory.getLogger(EpaperProcessStatusMailerReader.class);

	private int perPage = 1;
	private int pageNo = 0;

	private List<DBObject> feedMailerList = Collections.synchronizedList(new ArrayList<DBObject>());



	public void beforeStep(StepExecution stepExecution) {		
		logger.debug(" beforeStep method..");

		getFeedMailerData();

	}




	@Override
	public List<EmailVO> read() throws Exception, UnexpectedInputException,
	ParseException, NonTransientResourceException {
		getFeedMailerData();
		if (feedMailerList != null && feedMailerList.size() > 0) {
			List<EmailVO> mailUserList = getfeedMailerObjectListInfo();

			return mailUserList;
		} else {
			return null;
		}

	}

	private List<EmailVO> getfeedMailerObjectListInfo() {
		List<EmailVO> mailUserList = new ArrayList<>();
		synchronized (this) {
			if (feedMailerList != null && feedMailerList.size() > 0) {
				DBObject mrFeedMailerDBObject = feedMailerList.remove(0);
				mailUserList =getMailerVO(mrFeedMailerDBObject);
				
				ObjectId id= new ObjectId(String.valueOf(mrFeedMailerDBObject.get("_id")));
				DBObject query = new BasicDBObject();
				query.put("_id", id);
				
				BasicDBObject datatoupdate = new BasicDBObject();					
			
				datatoupdate.put("processed" , true);

				BasicDBObject newDocument = new BasicDBObject();
				newDocument.append("$set", datatoupdate);

				boolean isUpdate = MongoRestAPIService.update(Constant.EPAPER_PROCESSED_STATUS_LOG_COLLECTION, query, newDocument,true);
			}
		}
		return mailUserList;
	}


	private void getFeedMailerData() {

		if (feedMailerList == null || feedMailerList.isEmpty()){
			long startTime = System.currentTimeMillis();
			synchronized (this) {
				++pageNo;

				BasicDBObject query1 = new BasicDBObject();
				query1.put("processed", false);



				Map<String, Object> optionMap = new HashMap<String, Object>();
				optionMap.put("limit", perPage);
				optionMap.put("skip", String.valueOf((pageNo - 1) * perPage));

				BasicDBObject orderBy = new BasicDBObject();
				orderBy.put("currentDate", -1);

				optionMap.put("sort", orderBy);



				List<DBObject> dbObjectRes  = MongoRestAPIService.getList(Constant.EPAPER_PROCESSED_STATUS_LOG_COLLECTION, query1, optionMap);

				for (DBObject mrDBObject : dbObjectRes) {
					feedMailerList.add(mrDBObject);
				}

				logger.debug(perPage + " User pull time : " + (System.currentTimeMillis() - startTime) + " ms" );
				if (feedMailerList != null && feedMailerList.size() > 0) {
					logger.debug("User pulled : " + feedMailerList.size() + "for Page : " + pageNo);
				}
			}

		}
	}


	private static List<EmailVO> getMailerVO(DBObject dbObjectRe) {
		List<EmailVO> emailVOList = new ArrayList<>();

	
		if (dbObjectRe.get("toMailInternal") != null) {
			EmailVO emailVO = new EmailVO();
			
			emailVO.setRecipients(String.valueOf(dbObjectRe.get("internalMailIds")));
			emailVO.setSubject(String.valueOf(dbObjectRe.get("mailSubjectInternal")));
			emailVO.setBody(String.valueOf(dbObjectRe.get("mailBodyInternal")));
			emailVOList.add(emailVO);
		}

		

		return emailVOList;
	}



	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {		
		logger.debug("Job complete.");
		return null;
	}




}