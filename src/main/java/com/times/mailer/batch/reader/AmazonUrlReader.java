package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.util.Constant;
import com.times.common.util.SignedRequestsGenerator;
import com.times.mailer.dao.AmazonParamDao;
import com.times.mailer.model.FileLocationInfo;

public class AmazonUrlReader implements ItemReader<FileLocationInfo>, StepExecutionListener, Runnable {
	
	// -----------------------------------------------------------------------------------------------
	// Start Private members
		
	private static final Logger logger = LoggerFactory.getLogger(AmazonUrlReader.class);
	
	private AmazonParamDao amazonParamDao;
	
	private SignedRequestsGenerator signedRequestGenerator;
	
	public Set<String> amazonProductIdsSet =  Collections.synchronizedSet(new HashSet<String>());
	
	public List<String> amazonProductIdsList = new ArrayList<String>();
	
	public List<String> priorityIdsList = Collections.synchronizedList(new ArrayList<String>());
	
	private long startTime = System.currentTimeMillis();
	
	// This variable is used so that , once the job is started and product ids are fetched from DB
	// then the job will just iterate over the product ids list again and again.
	private static volatile int ITEM_NUMBER = 0;
	
	private ScheduledExecutorService scheduledThreadPool;
	
	// End Private members
	// -----------------------------------------------------------------------------------------------
	
	
	// -----------------------------------------------------------------------------------------------
	// Start public methods

	@Override
	public FileLocationInfo read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		if (this.amazonProductIdsList.size() > 0) {
			
			FileLocationInfo fileLocationInfo = getFileLocationInfo();
			
			return fileLocationInfo;
		} 
		return null;
	}
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		
		logger.debug(" Before Step method..");
		
		startTime = System.currentTimeMillis();
		
	    try {
			populateAmazonIdsList();
			
		} catch (InterruptedException e) {
			
			logger.error("Thread interrupted for Amazon Url hit: "+e.getMessage());
		}
	    
	    this.scheduledThreadPool = Executors.newScheduledThreadPool(1);
		
		this.scheduledThreadPool.scheduleAtFixedRate(this, 1, 10, TimeUnit.MINUTES);
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		
		logger.debug("After Step method..");
		
		logger.debug("After Step status : " + stepExecution.getExitStatus().toString());
		
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}
		
		return stepExecution.getExitStatus();
	}
	
	@Override
	public void run() {
		
		logger.debug("Starting thread:  "+ Thread.currentThread().getName());
		
		Set<String> productIds = new HashSet<String>();
		
		try {
			
			productIds = this.getAmazonProductIds();
			
		} catch (Exception e) {
			
			logger.error("Exception while getting data for Amazon Url hit: "+e.getMessage());
		}
		
		logger.debug("Checking for new elements:");
		
		if(productIds.size() > this.amazonProductIdsSet.size()){
			
			productIds.removeAll(this.amazonProductIdsSet);
			
			logger.debug("New elements found: "+ productIds.size());
			
			if(productIds.size() > 0){
				
				synchronized(this.priorityIdsList) {
					
					logger.warn("Adding in priority: "+productIds.toArray());
					
					this.priorityIdsList.addAll(productIds);
					
					this.amazonProductIdsSet.addAll(productIds);
				}
			}
		}
	}
	
	// End public methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start private methods
	
	public void populateAmazonIdsList() throws InterruptedException {
		
		Set<String> productIds = this.getAmazonProductIds();
		
		if(productIds != null){
			
			this.amazonProductIdsSet.addAll(productIds);
			
			this.amazonProductIdsList.addAll(productIds);
		}
		
		logger.debug("Size of Amazon Ids list: "+ this.amazonProductIdsList.size());
	}
	
	private FileLocationInfo getFileLocationInfo() {

		FileLocationInfo fileLocationInfo = new FileLocationInfo();
		
		while(this.priorityIdsList.size() > 0){

			synchronized (this.priorityIdsList) {
				
				String id = this.priorityIdsList.remove(0);

				this.amazonProductIdsList.add(id);
				
				logger.warn("Getting from priority : "+ id);

				fileLocationInfo.setSourceUrl(this.signedRequestGenerator.getURL(id));

				fileLocationInfo.setStateId(id);

				return fileLocationInfo;
			}
		}
		

		if (this.amazonProductIdsList != null && this.amazonProductIdsList.size() > 0) {

			if(ITEM_NUMBER >= this.amazonProductIdsList.size()){

				logger.warn("List Iterated Once..starting again...");

				ITEM_NUMBER = 0;
			}
			
			logger.warn("Index: "+ITEM_NUMBER);

			String itemId = this.amazonProductIdsList.get(ITEM_NUMBER++);

			fileLocationInfo.setSourceUrl(this.signedRequestGenerator.getURL(itemId));

			fileLocationInfo.setStateId(itemId);
		}
		return fileLocationInfo;
	}

	private Set<String> getAmazonProductIds(){
		
		return this.amazonParamDao.getProductIdsList(Constant.CONTENT_RULES_AMAZON_PRODUCT_ID_META_TYPE);
	}
	
	// End private methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start getters/setters

	public AmazonParamDao getAmazonParamDao() {
		return this.amazonParamDao;
	}

	public void setAmazonParamDao(AmazonParamDao amazonParamDao) {
		this.amazonParamDao = amazonParamDao;
	}

	public SignedRequestsGenerator getSignedRequestGenerator() {
		return this.signedRequestGenerator;
	}

	public void setSignedRequestGenerator(SignedRequestsGenerator signedRequestGenerator) {
		this.signedRequestGenerator = signedRequestGenerator;
	}

	// End getters/setters
	// -----------------------------------------------------------------------------------------------

}
