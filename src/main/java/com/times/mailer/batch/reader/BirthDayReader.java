/**
 * 
 */
package com.times.mailer.batch.reader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.util.StringUtils;

import com.mongodb.DBObject;
import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
import com.times.common.util.BirthDayReaderUtil;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.common.util.MailerUtil;
import com.times.mailer.dao.NewsAlertDao;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetter;
import com.times.mailer.model.NewsLetterDailyTracker;

public class BirthDayReader  implements ItemReader<MailUser>, StepExecutionListener { //, JobExecutionListener


	private final static Logger logger = LoggerFactory.getLogger(BirthDayReader.class);

	private EmailProcessor smtpMailer;
	private Mailer myMailer;
	private NewsAlertDao newsAlertDao;
	private static String alertSubject = null;

	private int rowEndNo = 100;
	private int rowStartNo = 0;
	private String newsLetterId = null;


	private List<MailUser> lstSubscribers = Collections.synchronizedList(new ArrayList<MailUser>());

	//code changes ... 
	private boolean isInitialized = false;
	private String cacheUIKey = null;
	private String ssoUrl;


	public static final String NL_FORMAT_TEXT = "text";
	public static final String NL_FORMAT_HTML = "html";
	public static final String NL_FORMAT_PARAM = "nlformat";
	public static final long TIME_INTERVAL = 10;

	/**
	 * This method is used to read the {@link MailUser} object.
	 * 
	 * @param args
	 */
	public MailUser read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		logger.debug("iread method start..");


		if (myMailer != null && lstSubscribers != null && lstSubscribers.size() == 0) {

			//this.pushSubscribersIntoList(myMailer, perPage, pageNo );
			lstSubscribers = this.pushSubscribersIntoList(myMailer);
			logger.debug("Fetch User Done");

		}

		// if subscriber got then 
		if (lstSubscribers != null && lstSubscribers.size() > 0) {
			//counter update in memory
			MailUser subscriberUser = null;
			subscriberUser = getSubscriber();
			subscriberUser.setEmailSubject(alertSubject);
			
			return subscriberUser;
		} else { 
			return null;
		}
	}


	/**
	 * Get the Newsletter user/subscriber, if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private MailUser getSubscriber() {
		MailUser subscriberUser = null;
		synchronized (this) {
			if (lstSubscribers != null && lstSubscribers.size() > 0) {
				subscriberUser = lstSubscribers.remove(0);
				// update counter in map
				this.updateCounterInMap(subscriberUser);
			}
		}
		return subscriberUser;
	}

	/**
	 * @param subscriberUser
	 */
	private void updateCounterInMap(MailUser subscriberUser) {
		//logger.debug("updateCounterInMap method ..");
		try {
			int lastCounter = 1;
			if (subscriberUser != null && subscriberUser.getNewsletter() != null) {
				int dailyOrWeekly = subscriberUser.getNewsletter().isDaily() ? 1: 2;
				int morningOrEvening = subscriberUser.getNewsletter().getScheduleTime();
				String sKey = subscriberUser.getNewsletter().getId(); 
				sKey = sKey + Constant.KEY_BUILDER_SEPERATOR + dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + morningOrEvening;
				if ( InMemoryData.emailSentCounter.containsKey(sKey)) {
					lastCounter = InMemoryData.emailSentCounter.get(sKey);
					InMemoryData.emailSentCounter.put(sKey, ++lastCounter);
				} else {
					InMemoryData.emailSentCounter.put(sKey, new Integer(lastCounter));
				}
			}
		} catch (Exception e) {
			logger.error("in side the updateCounterInMap method, msg : " + e.getMessage());
		}
	}

	/**
	 * This method is used to update stats finally for one newsletter.
	 * 
	 * @param sKey
	 */
	private void updateEmailStats(String sKey) {
		int iCount = InMemoryData.emailSentCounter.get(sKey);
		int ireadCount = 0;
		this.getNewsAlertDao().addMailerStats(sKey, 0, iCount);
		String code = null;
		if (InMemoryData.keyCodeMap.containsKey(sKey)) {
			code = InMemoryData.keyCodeMap.get(sKey);
		}
		//travelNewsletterDao.updateNewsLetterDailyTracker(code, sKey, ireadCount, iCount);
	}

	//public void List<DBObject> get

	/**
	 * This method is used to push the subscriber into the List of {@link MailUser}.
	 *  
	 */
	private  List<MailUser> pushSubscribersIntoList(Mailer myMailer) {

		if (lstSubscribers == null || lstSubscribers.isEmpty()){

			try {

				String nlid = myMailer.getId();
				String channelName = nlid.equalsIgnoreCase("1042") ? "maharashtratimes" : "navbharattimes";

				FileInputStream fis;
				String emaild = null;
				String dob = null;
				String firstName = null;
				String lastName = null;
				String channel = null;

				NewsLetter news = new NewsLetter();								
				news.setId(myMailer.getId());
				news.setDaily(true);
				news.setDisplayFormate(NL_FORMAT_HTML);
				try {

					//fis = new FileInputStream("D:/unverified/csvdata.xls");
					//fis = new FileInputStream("/home/tilguest/data.xls");
					fis = new FileInputStream("/home/tilftp/csvdata.xls");

					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					List<Map<String, Object>> list = BirthDayReaderUtil.getRecordsList(fis, rowStartNo, rowEndNo);

					if (list != null && list.size() > 0) {
						for (Map<String, Object> map : list) {

							try {
								emaild = (String)map.get("EMAILID");
								dob = (String)map.get("DATEOFBIRTH");
								channel = (String)map.get("CHANNEL");
								firstName = (String)map.get("FIRSTNAME");
								lastName = (String)map.get("LASTNAME");

								Calendar c = Calendar.getInstance();
								c.setTime(sdf.parse(dob));

								Calendar endDateC = Calendar.getInstance();
								endDateC.setTime(new Date());

								if (c.getTime().getMonth() == endDateC.getTime().getMonth() && c.getTime().getDate() == endDateC.getTime().getDate() 
										&& channelName.equalsIgnoreCase(channel)) {
									MailUser mu = new MailUser();
									mu.setEmailId(emaild);
									mu.setFirstName(firstName);
									mu.setLastName(lastName);
									mu.setNewsletter(news);
									lstSubscribers.add(mu);
								}
							} catch (Exception e) {
								System.out.println("Read Exception" );
								return null;
							}
						}
						rowStartNo = rowEndNo;
						rowEndNo = rowEndNo + 100;
						if (lstSubscribers.size() ==0) {							
							pushSubscribersIntoList(myMailer);
						}

					} else {
						return null;
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return null;
				}

			} catch (Exception e) {
				// TODO: handle exception
				return null;
			}
		}
		return lstSubscribers;
	}


	/**
	 * This method is used to get the Mailer i.e. Newsletter master object for the mailer.
	 * 
	 * @param nlId
	 * @param isDaily
	 * @return
	 */
	private List<Mailer> getMailer(String nlId, boolean isDaily) {
		List<Mailer> mailerMaster = newsAlertDao.getNewsletterMailer(nlId, isDaily);
		return mailerMaster;
	}

	/**
	 * This method is used to add the newsletter Body part in in-memory i.e. collection.
	 * 
	 * @param myMailer
	 */
	private void initializeNewsletter(Mailer myMailer) {
		try {

			String newsid = myMailer.getId();
			logger.debug("nl id :" + newsid + " going to pull data ...");
			int scheduledTime = myMailer.getScheduleTime();
			int daily_weekly = myMailer.isDaily() ? 1 : 2;
			String skey1 = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + scheduledTime ;
			String uniqueCode = addNewsLetterDailyTrackerIfNotfound(myMailer, newsid, scheduledTime, daily_weekly, skey1);

			//get unique code... and pass it to template ... hash code (timestamp +skey1)
			String urlNewsletter =  myMailer.getHtmlPath(); //.replace("#$nlcode", uniqueCode); // HTML >>MAIN TEMPLATE :
			//String urlNewsletterWidget = myMailer.getTemplatepath();//.replace("#$nlcode",uniqueCode) ; // HTML FRAGMENT
			alertSubject = myMailer.getMailSubject();			
			// track the details for each day and generate the code.

			//for html data 
			String scachekey = skey1 +Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_HTML;
			if (!InMemoryData.newsMailerUIMap.containsKey(scachekey)) {
				//code generate newslid ... 
				//	urlNewsletter = urlNewsletter.indexOf("?") != -1 ? urlNewsletter + "?nlformat=" + NL_FORMAT_HTML : urlNewsletter + "&nlformat=" + NL_FORMAT_HTML;
				String url4Html = urlNewsletter.indexOf("?") == -1 ? urlNewsletter +"?" : urlNewsletter + "&";
				url4Html = url4Html  + NL_FORMAT_PARAM + "=" + NL_FORMAT_HTML;
				String sData = MailerUtil.getData(url4Html);
				if (!StringUtils.hasText(sData)) {
					sData = MailerUtil.getData(url4Html);
				}
				if (sData != null) {
					sData = sData.replace("##code##", uniqueCode);
					InMemoryData.newsMailerUIMap.put(scachekey, sData);
				}

				// logg id data not found
				logIfDataNotFound(newsid, url4Html, sData);
			}



			logger.debug("initializeNewsletter : ");

		} catch (Exception e) {
			logger.error("exception occured while retreiving content , msg: " + e.getMessage());
		}
	}

	/**
	 * This method is used to logged the data.
	 * 
	 * @param url4Html
	 * @param sData
	 */
	private void logIfDataNotFound(String nlId, String url4Html, String sData) {
		String msg = null;
		if (sData != null) {
			msg =  "url : " + url4Html + " , len : " + sData.length() + " nlid : " + nlId;
			logger.debug(" url : " + url4Html + " , len : " + sData.length() + " nlid : " + nlId);
		} else {
			logger.debug("ALERT ALERT NOT FOUND data for url : " + url4Html + " , nlid : " + nlId);
			try {
				smtpMailer.processStart("DATA NOT FOUND for URL : " + url4Html + " , nlid : " + nlId  + " MSG : " + msg);
			} catch (Exception e) {
				logger.debug("EXception url : " + url4Html + " , nlid : " + nlId + e.getMessage()); ;
			}
		}
	}

	/**
	 * This method is used to add the newsLetterTracker if not found for that days, frequency, time and nlId.
	 * 
	 * @param myMailer
	 * @param newsid
	 * @param iTime
	 * @param itype
	 * @param skey1
	 */
	private String addNewsLetterDailyTrackerIfNotfound(Mailer myMailer, String newsid, int iTime, int itype, String skey1) {
		String uniqueCode= null;
		String yyymmdd = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		DBObject nlTracker = newsAlertDao.getNewsLetterDailyTracker(newsid, yyymmdd, String.valueOf(iTime), String.valueOf(itype));
		if (nlTracker == null) {
			uniqueCode = getUniqueCodeByDate(skey1);
			NewsLetterDailyTracker nlDailyTracker = new NewsLetterDailyTracker(new Date(), Integer.parseInt(myMailer.getId()), uniqueCode, iTime, itype, yyymmdd);
			newsAlertDao.addNewsLetterDailyTracker(nlDailyTracker); 

		} else {
			uniqueCode = String.valueOf(nlTracker.get(NewsLetterDailyTracker.NEWS_LETTER_CODE));
		}
		if (!InMemoryData.keyCodeMap.containsKey(skey1)) {
			InMemoryData.keyCodeMap.put(skey1, uniqueCode);
		}

		return uniqueCode;
	}

	/**
	 * This method is used to get the StringCode.
	 * @param str
	 * @return
	 */
	private String getUniqueCodeByDate(String key) {
		String date = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		String code = MailerUtil.getUniqueCodeByDate(date + key);
		return code;	
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {		


		logger.debug(" beforeStep method..");
		long dailyOrWeekly = 1;
		long morningOrEvening = 1;
		JobParameters jobParams = stepExecution.getJobParameters();
		this.newsLetterId = jobParams.getString(Constant.JOB_PARAM_NEWSLETTER_ID);


		if (jobParams.getString(Constant.JOB_PARAM_NL_RESTART_KEY) != null) {
			int pageNo = (int)jobParams.getLong(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO);
			this.rowStartNo = ++rowStartNo;
		} 


		List<Mailer> mailers = this.getMailer(newsLetterId, true);
		if (mailers != null && mailers.size() > 0) {
			this.myMailer = mailers.get(0);
		}
		if (myMailer != null) {

			this.initializeNewsletter(myMailer);
		}
		this.isInitialized = true;

		this.cacheUIKey = this.newsLetterId + Constant.KEY_BUILDER_SEPERATOR + dailyOrWeekly;

	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		reportMailer();
		this.removeInMemoryData();

		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		return stepExecution.getExitStatus();
	}

	/**
	 * This method is used to remove the in-memory data for the that newsletter.
	 */
	private void removeInMemoryData() {
		if (InMemoryData.newsMailerDataMap.containsKey(this.cacheUIKey)){
			InMemoryData.newsMailerDataMap.remove(this.cacheUIKey);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_HTML)){
			InMemoryData.newsMailerUIMap.remove(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_HTML);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_TEXT)){
			InMemoryData.newsMailerUIMap.remove(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_TEXT);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey)) {
			InMemoryData.emailSentCounter.remove(this.cacheUIKey);
		}
	}


	private void reportMailer() {
		String newsid = myMailer.getId();

		long dailyOrWeekly = 1;
		long morningOrEvening = 1;

		String skey1 = newsid + Constant.KEY_BUILDER_SEPERATOR + dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + morningOrEvening ;
		String subjectTitle = InMemoryData.newsMailerUIMap.get(skey1 + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1");

		String scachekey = skey1 +Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_HTML;

		EmailVO vo = new EmailVO();


		String sBody = InMemoryData.newsMailerUIMap.get(scachekey);	
		vo.setBody(sBody);


		String prefix = "[NLID : " + newsid + ",DAILY_WEEKLY : " + dailyOrWeekly +  ",Subscribers : "  + InMemoryData.emailSentCounter.get(skey1) + "]";

		try {
			if (StringUtils.hasText(subjectTitle)) {				
				vo.setSubject(prefix + subjectTitle + "true");
			} else {
				vo.setSubject(prefix + subjectTitle + "false");			
			}
			vo.setRecipients("toi.daily.newsletter@gmail.com");

			smtpMailer.sendEMail(vo);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * @return
	 */
	public Mailer getMyMailer() {
		return myMailer;
	}

	/**
	 * @param myMailer
	 */
	public void setMyMailer(Mailer myMailer) {
		this.myMailer = myMailer;
	}

	/**
	 * @param smtpMailer the smtpMailer to set
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}


	/**
	 * @return NewsAlertDao
	 */
	public NewsAlertDao getNewsAlertDao() {
		return newsAlertDao;
	}



	/**
	 * @param newsAlertDao
	 */
	public void setNewsAlertDao(NewsAlertDao newsAlertDao) {
		this.newsAlertDao = newsAlertDao;
	}

	/**
	 * @param ssoUrl
	 */
	public void setSsoUrl(String ssoUrl) {
		this.ssoUrl = ssoUrl;
	}

}