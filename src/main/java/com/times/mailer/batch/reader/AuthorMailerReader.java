package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;





import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.MailContent;
import com.times.common.util.InMemoryData;
import com.times.mailer.model.MailUser;

/**
 * 
 * @author Rajeev Khatri
 *
 */
public class AuthorMailerReader implements ItemReader<MailContent>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(AuthorMailerReader.class);
	private final static String URL = "http://192.168.27.156/mytimes/author/mail/?days=1";
	private final static String body_link = "<tr style='background-color:#eff3fb'><td><a href='<###link###>' target='_blank'><###articletitle###></a></td></tr>";
	private List<MailContent> lstSubscribers = Collections.synchronizedList(new ArrayList<MailContent>());

	public static boolean isJobExecution = true;

	public MailContent read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		if (lstSubscribers.size() > 0) {

			MailContent mContent = null;
			mContent = getSubscriber();

			return mContent;
		} else {
			return null;
		}

	}

	private MailContent getSubscriber() {
		MailContent mContent = null;
		synchronized (this) {
			if (lstSubscribers != null && lstSubscribers.size() > 0) {
				mContent = lstSubscribers.remove(0);

			}

		}
		return mContent;
	}

	/* beforeStep method call http://192.168.27.156/mytimes/author/mail/?days=1 servide to get author details _ msids details.
	 * (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {

		logger.debug(" beforeStep method..");
		String response = null;
		try {
			response = MongoRestAPIService.getExtData(URL);


		} catch (Exception e1) {

			e1.printStackTrace();
		}
		logger.info("got response from " + URL);


		try {
			JSONArray array = new JSONArray(response);


			for (int i = 0; i < array.length(); ++i) {
				JSONObject rec = array.getJSONObject(i);
				MailContent mContent = new MailContent();

				// set host for toi
				mContent.setHostId(83);

				// set mail type to 8 (Special Mailer)
				mContent.setMailType(8);

				// get list of msid for an author
				JSONArray ja =  rec.getJSONArray("articleList");
				for (int j = 0; j < ja.length(); ++j) {
					JSONObject obj = ((JSONObject)ja.get(j));

					// set email
					if (StringUtils.isEmpty(mContent.getEmailId())) {
						mContent.setEmailId(obj.getString("auth_em"));
						//mContent.setEmailId("rajeev.khatri.2007@gmail.com");
					}

					// no use of title
					mContent.setTitle(obj.getString("tet"));

					// set body
					if (mContent.getLink() != null) {
						mContent.setLink(mContent.getLink() +  
								body_link.replace("<###link###>", obj.getString("teu")).replace("<###articletitle###>", 
										obj.getString("tet")));
					} else {
						mContent.setLink(
								body_link.replace("<###link###>", obj.getString("teu")).replace("<###articletitle###>", 
										obj.getString("tet")));
					}
					

					// set author name
					if (StringUtils.isEmpty(mContent.getUserName())) {
						if (obj.get("auth_nm") != null && !"null".equalsIgnoreCase(obj.get("auth_nm").toString())) {
							mContent.setUserName(obj.getString("auth_nm"));
						}
					}
				}
				// default user name set to Author
				if (StringUtils.isEmpty(mContent.getUserName())) {
					mContent.setUserName("Author");
				}
				lstSubscribers.add(mContent);

			} 
		}catch (Exception ex) {
			ex.printStackTrace();
		}


		logger.debug(" before step method exiting");

	}

	public ExitStatus afterStep(StepExecution stepExecution) {
		return null;
	}


}
