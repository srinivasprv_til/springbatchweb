package com.times.mailer.batch.reader;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.model.TimeLineInstance;
import com.times.common.model.articleKeyWordDetails;
import com.times.common.util.InMemoryData;

/**
 * 
 * @author Dipali Patidar
 *
 */
public class TimeLineMonthlyReader implements ItemReader<List<articleKeyWordDetails>>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(TimeLineMonthlyReader.class);
	private final static String KeyWord_url = "http://etdev2.indiatimes.com/ETPersons/AI/getQuotes?url=http://economictimes.indiatimes.com/articleshow_body";
//	public static List<TimeLineInstance> timeLineInstanceList = new ArrayList<TimeLineInstance>();
	// Date lastProcessedDate = null;
	public static boolean isJobExecution = true;
	private boolean isFetchData =true;
	List<Integer> dayMsidList = new ArrayList<Integer>();
	
	public List<articleKeyWordDetails> read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		List<articleKeyWordDetails> articleKeyWordPojoList = new ArrayList<articleKeyWordDetails>();

		/*
		 * if (InMemoryData.etTimeLineDate == null) {
		 * InMemoryData.etTimeLineDate = new Date();
		 * 
		 * }
		 */

		/*
		 * if(timeLineInstanceList.size()>0) { Date today = new Date();
		 * 
		 * if(isJobExecution){ // Date today = new
		 * Date();//dateFormat.parse("20160525");// lastProcessedDate =today;
		 * InMemoryData.instance =
		 * getInstanceObject(timeLineInstanceList,lastProcessedDate);
		 * InMemoryData.etTimeLineDate=today; }
		 */
		//
		if (isJobExecution) {
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTimeZone(TimeZone.getTimeZone("IST"));

			int weekNumber = cal.get(Calendar.WEEK_OF_YEAR);
			int day = cal.get(Calendar.DAY_OF_WEEK)-1;
			cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
			cal.set(Calendar.WEEK_OF_YEAR, weekNumber);
			date = cal.getTime();

			SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));


			String dateStr = sdf.format(date);
			logger.debug("reading Documents from mongo");
			DBObject query = new BasicDBObject();
			query.put("DT", dateStr);
			query.put("TP", 10);
			query.put("hostId", 153);
			query.put("campaignId", "WebHP");
			// query.put("DAY", day);

			// Date date1 = sdf.parse("13_09_2016");
			// String etTimeLineDatestr =
			// sdf.format(InMemoryData.etTimeLineDate);
			
          if(isFetchData){
			DBObject existingDoc = MongoRestAPIService.get("Timeline", query);
			
			if (existingDoc != null && existingDoc.get("DAY") != null
					&& ((DBObject) existingDoc.get("DAY")).get(String.valueOf(day)) != null) {
				dayMsidList = (List<Integer>) ((DBObject) existingDoc.get("DAY")).get(String.valueOf(day));

			}
			isFetchData=false;
          }
          int msid=0;
          try{
			 msid = dayMsidList.remove(dayMsidList.size()-1);
          }catch(Exception e1){
        	  e1.printStackTrace();
          }
          
			if (dayMsidList == null || dayMsidList.isEmpty()) {
				isJobExecution = false;
			}
			String Final_url = KeyWord_url + "/" + String.valueOf(msid) + ".cms";
			String response = null;
			try {
				response = MongoRestAPIService.getExtData(Final_url);
				// time of getting data
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			logger.info("got response from " + Final_url);
          

			if (response == null) {
				return null;
			}

			ObjectMapper mapper = new ObjectMapper();
			try {
				// File json = new File("dayDoclist");
				articleKeyWordPojoList = (List<articleKeyWordDetails>) mapper.readValue(response,
						new TypeReference<List<articleKeyWordDetails>>(){});
			} catch (JsonGenerationException ex) {
				ex.printStackTrace();
			}

			return articleKeyWordPojoList;
          
		} else {
			return null;
		}

	}

	public void beforeStep(StepExecution stepExecution) {

		logger.debug(" beforeStep method..");

		/*
		 * JobParameters jobParams = stepExecution.getJobParameters(); int
		 * instanceId = Integer.parseInt(jobParams.getString("instanceId:"));
		 * 
		 * DBObject dbQuery = new BasicDBObject(); dbQuery.put("instanceId",
		 * instanceId); // dbQuery.put("hostId", 53);
		 * 
		 * List<DBObject> Objects =
		 * MongoRestAPIService.getList("TimelineMaster", dbQuery); for (int i =
		 * 0; i < Objects.size(); i++) { DBObject instanceObject =
		 * Objects.get(i); TimeLineInstance instance = new TimeLineInstance();
		 * instance.setUrl((String) (instanceObject.get("url")));
		 * instance.setHostId((Integer) (instanceObject.get("hostId")));
		 * instance.setInstanceId(instanceId); instance.setCampaignId((String)
		 * (instanceObject.get("campaignId")));
		 * instance.setLastProcessedDate((String)
		 * (instanceObject.get("lastProcessed"))); timeLineInstanceList.add(i,
		 * instance);
		 * 
		 * }
		 */

		logger.debug(" before step method exiting");

	}

	public ExitStatus afterStep(StepExecution stepExecution) {
		return null;
	}

	/*
	 * private TimeLineInstance
	 * getInstanceObject(List<TimeLineInstance>timeLineInstanceList,Date
	 * lastProcessedDate){ TimeLineInstance instanceObject =
	 * timeLineInstanceList.remove(0);
	 * 
	 * SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	 * f.setTimeZone(TimeZone.getTimeZone("IST"));
	 * 
	 * DBObject updateQuery = new BasicDBObject();
	 * updateQuery.put("instanceId",instanceObject.getInstanceId());
	 * updateQuery.put("hostId",instanceObject.getHostId()); DBObject dbQuery =
	 * new BasicDBObject(); dbQuery.put("lastProcessed",
	 * f.format(lastProcessedDate));
	 * 
	 * DBObject update = new BasicDBObject(); update.put("$set",dbQuery);
	 * MongoRestAPIService.update("TimelineMaster", updateQuery,update, true);
	 * return instanceObject; }
	 */

}
