package com.times.mailer.batch.reader;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.noggit.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONObject;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.model.TimeLineInstance;
import com.times.common.model.articleKeyWordDetails;
import com.times.common.util.InMemoryData;

/**
 * 
 * @author Dipali.Patidar
 *
 */
public class ChartbeatTrendingStoriesReader implements ItemReader<List<Integer>>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(ChartbeatTrendingStoriesReader.class);
	private final static String URL = "http://api.chartbeat.com/live/toppages/v3/?apikey=51cb5704d11ad31e46ebfc8268cbc037&host=economictimes.indiatimes.com&limit=200&sort_by=social";
	//private final static String URL  = "http://ibeatserv.indiatimes.com/iBeat/mostCommented.html?h=navbharattimes.indiatimes.com&c=-2128958273&d=1&pt=30&k=7b52a71c70ba356944817b85d377b";
	public static boolean isJobExecution = true;
	private final static String IsArticle="articleshow";
	
	
	public List<Integer> read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		List<Integer> articlesMsidList = new ArrayList<Integer>();

		if (isJobExecution) {
			
            String response = null;
			try {
				response = MongoRestAPIService.getExtData(URL);
				isJobExecution = false;
				// time of getting data
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			logger.info("got response from " + URL);
          

			if (response == null) {
				return null;
			}
		
			try {
				// File json = new File("dayDoclist");
				JSONObject json = new JSONObject(response);
				 JSONArray results =  json.getJSONArray("pages");
				 for (int i = 0; i < results.length(); ++i) {
					 JSONObject rec = results.getJSONObject(i);
					 String path = rec.getString("path");
					 if (path.contains(IsArticle)) {
						 path = path.substring(0, path.indexOf(".cms"));
						 if (path!= null && path.lastIndexOf("/") != -1) {
								String msidString = path.substring(path.lastIndexOf("/") + 1);
							//	if (msidString.length() > 0) {
							//		msidString = msidString.substring(0, msidString.length());
								articlesMsidList.add(Integer.valueOf(msidString));
					 }
				 }
				
			} 
			}catch (Exception ex) {
				ex.printStackTrace();
			}

			return articlesMsidList;
          
		} else {
			return null;
		}

	}

	public void beforeStep(StepExecution stepExecution) {

		logger.debug(" beforeStep method..");

		logger.debug(" before step method exiting");

	}

	public ExitStatus afterStep(StepExecution stepExecution) {
		return null;
	}

	
}
