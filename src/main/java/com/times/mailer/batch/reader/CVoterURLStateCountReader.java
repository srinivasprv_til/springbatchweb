package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.times.common.util.Constant;
import com.times.mailer.model.AmazonS3Credits;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NetstorageInfo;
import com.times.mailer.model.PollingConstants;

/**
 * This class is used to upload the breaking new accross all the channel.
 * 
 * @author Ranjeet.Jha
 *
 */
public class CVoterURLStateCountReader implements ItemReader<FileLocationInfo>, StepExecutionListener { //, JobExecutionListener

	private final static Logger logger = LoggerFactory.getLogger(CVoterURLStateCountReader.class);
	
	
	//private Boolean isExisting = false;
	private int perPage = 100;
	private int pageNo = 0;
	private static final int workType = Constant.WORK_TYPE_CVOTER_JSON;  
	//private static final int workType1 = Constant.WORK_TYPE_CVOTER_XML;
	
	public List<FileLocationInfo> fileLocationInfoList = Collections.synchronizedList(new ArrayList<FileLocationInfo>());
	
	//code changes ... 
	boolean isInitialized = false;
	private long startTime = System.currentTimeMillis();
	
	/**
	 * This method is used to read the {@link FileLocationInfo} object.
	 * 
	 * @param args
	 */
	public FileLocationInfo read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		logger.debug(" read method..");
		
		// if subscriber got then 
		if (fileLocationInfoList.size() > 0) {
			//counter update in memory
			FileLocationInfo fileUploadDtls = getFileLocationInfo();
			return fileUploadDtls;
		} 
		
		return null;
	}

	/**
	 * Get the Newsletter user/subscriber, if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private FileLocationInfo getFileLocationInfo() {
		FileLocationInfo fileLocationInfo = null;
		synchronized (this) {
			if (fileLocationInfoList != null && fileLocationInfoList.size() > 0) {
				fileLocationInfo = fileLocationInfoList.remove(0);
			
			}
		}
		return fileLocationInfo;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");
		startTime = System.currentTimeMillis();
		JobParameters jobParams = stepExecution.getJobParameters();
		int workType = Integer.parseInt(jobParams.getString(Constant.JOB_PARAM_JOBTYPE_ID));
	
		this.isInitialized = true;
	    populateFileLocationInfo();
		
		 
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		
		this.isInitialized = false;
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}
		
		return stepExecution.getExitStatus();
	}


	
	/**
	 * This method is used to add source url and details in list i.e. would be in memory.
	 * 
	 * @return
	 */
	public void populateFileLocationInfo() {
		//List<FileLocationInfo> list = new ArrayList<FileLocationInfo>();
		logger.debug("started to load in memory fileLocationInfo...");
		// statewise 
	
	
		
		for (int i = 101; i <= 135; i++) {
			FileLocationInfo fileLocationInfo = new FileLocationInfo();
			String fileName = "statewise_" + PollingConstants.getStateMapping(i) + ".htm";
			fileLocationInfo.setNetstorageLocation(getNetStorageInfo(fileName));
			fileLocationInfo.setCallbackName("statewise");
			fileLocationInfo.setStateId(String.valueOf(i));
			fileLocationInfo.setSourceUrl( "http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_party_ally_tally_gain_loss&type=data&stateid="+i);
			fileLocationInfo.setStatus(true);
			
			fileLocationInfo.setAmazonS3(new AmazonS3Credits(fileName));
			
			// add in list.
			fileLocationInfoList.add(fileLocationInfo);
		}
		
		
		FileLocationInfo fileLocationInfo = new FileLocationInfo();
		String fileName = "group.htm";
		fileLocationInfo.setNetstorageLocation(getNetStorageInfo(fileName));
		fileLocationInfo.setCallbackName("group");
		fileLocationInfo.setStateId("all");
		fileLocationInfo.setSourceUrl( "http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_overall&stateid=all&type=data");
		fileLocationInfo.setStatus(true);
		fileLocationInfo.setAmazonS3(new AmazonS3Credits(fileName));
		
		fileLocationInfoList.add(fileLocationInfo);
		
		FileLocationInfo fileLocationInfo1 = new FileLocationInfo();
		fileName = "party.htm";
		fileLocationInfo1.setNetstorageLocation(getNetStorageInfo(fileName));
		fileLocationInfo1.setCallbackName("party");
		fileLocationInfo1.setStateId("all");
		fileLocationInfo1.setSourceUrl("http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_party_all&stateid=all&type=data");
		fileLocationInfo1.setStatus(true);
		fileLocationInfo1.setAmazonS3(new AmazonS3Credits(fileName));
		
		fileLocationInfoList.add(fileLocationInfo1);
		
		
		for (int i = 101; i <= 135; i++) {
			FileLocationInfo fileLocationInfo2 = new FileLocationInfo();
			String fileName2 = "statewise_swing" + PollingConstants.getStateMapping(i) + ".htm";
			fileLocationInfo2.setNetstorageLocation(getNetStorageInfo(fileName2));
			fileLocationInfo2.setCallbackName("swing");
			fileLocationInfo2.setStateId(String.valueOf(i));
			fileLocationInfo2.setSourceUrl( "http://counting.teamcvoter.com/countxml/?username=cvoter&password=12&method=select_party_ally_vote_share&type=data&stateid="+i);
			fileLocationInfo2.setStatus(true);
			fileLocationInfo2.setAmazonS3(new AmazonS3Credits(fileName2));
			
			// add in list.
			fileLocationInfoList.add(fileLocationInfo2);
		}
		
		//list.add(new FileLocationInfo(2,"http://timesofindia.indiatimes.com/india", 2, akamaiInfo));
		logger.debug("loaded in memory fileLocationInfo...");
	}
	
	private NetstorageInfo getNetStorageInfo(String fileName) {
		NetstorageInfo netstorageInfo = new NetstorageInfo();
		netstorageInfo.setFileName(fileName);
		netstorageInfo.setServerName("et.upload.akamai.com");
		netstorageInfo.setFolderLocation("/93656/configspace");
		netstorageInfo.setUserid("cmsetdeploy");
		netstorageInfo.setPassword("pE0ple");
		
		return netstorageInfo;
	}
}