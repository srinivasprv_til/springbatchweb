package com.times.mailer.batch.reader;

public class PartyDetail {

	
	String partyName;
	String partyFullName;
	String colorName;
	String groupName;
	
	/**
	 * @return the partyName
	 */
	public String getPartyName() {
		return partyName;
	}
	/**
	 * @param partyName the partyName to set
	 */
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	/**
	 * @return the partyFullName
	 */
	public String getPartyFullName() {
		return partyFullName;
	}
	/**
	 * @param partyFullName the partyFullName to set
	 */
	public void setPartyFullName(String partyFullName) {
		this.partyFullName = partyFullName;
	}
	/**
	 * @return the colorName
	 */
	public String getColorName() {
		return colorName;
	}
	/**
	 * @param colorName the colorName to set
	 */
	public void setColorName(String colorName) {
		this.colorName = colorName;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @return the leading
	 */
	
	
	

}
