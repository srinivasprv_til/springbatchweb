package com.times.mailer.batch.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.InMemoryData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * 
 * @author praveen.bokka
 *
 */
public class CCIAliveReader implements ItemReader<Document>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(CCIAliveReader.class);
	
	private String lastUpDate; 
	
	private static final int ITER_COUNT = 100;
    
    private int index = 0;
    private int count = 1;
	
	public Document read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		logger.debug("CCIAlive read method start..");
		
		//all records processed
        if(index > count) {
        	return null;
        }
		
		String url = "http://192.169.38.107:9090/apis/search/advanced?securitykey=2992654ae785c210778b384d5cb39ef5"
				+ "&keyword=&filetype=TEXT&datasource=CCIALIVEAR&recordscount=##COUNT"
				+ "&ordertype=asc&metadata=true&startdatetime=##START&enddatetime=##END&startindex=";
		url = url + index;
		
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy+HH:mm:ss");
		Date now = new Date();
		String nowStr = f.format(now);
		url = url.replace("##END", nowStr);
		
		url = url.replace("##COUNT", String.valueOf(ITER_COUNT));
		
		if(lastUpDate != null) {
			url = url.replace("##START", lastUpDate);
		}
		
		StringBuffer responseBuffer = getExternalData(url);
		System.out.println(url);
		Document document = new Document();
		try {
            
            SAXBuilder saxBuilder = new SAXBuilder();
            document = saxBuilder.build(new StringReader(responseBuffer.toString()));
            if(index == 0) {
            	count = document.getRootElement().getAttribute("records").getIntValue();
            	index += ITER_COUNT;
            }else{ 
            	index += ITER_COUNT;
            }
//            lastProcessedDate = today;
			
            //String xml = new XMLOutputter(Format.getPrettyFormat()).outputString(document);
            //System.out.println(xml);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			logger.error("Socket exception while retrieving file. Will try after 1 hour....");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("File may not be existing in given location. Will try after 1 hour....");
			e.printStackTrace();
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			logger.error("Unable to parse file as XML");
			e.printStackTrace();
		}
		
		return document;
	}
	
	private static StringBuffer getExternalData(String GET_URL)
			throws IOException, ClientProtocolException {
		HttpClient  httpClient = HttpClients.custom().build();
		HttpResponse response = httpClient.execute(new HttpGet(GET_URL));
		BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String inputLine;
        StringBuffer respBuffer = new StringBuffer();
        while ((inputLine = reader.readLine()) != null) {
        	respBuffer.append(inputLine);
        }
        reader.close();
		return respBuffer;
	}


	
	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");
		
		DBObject dbQuery = new BasicDBObject();
		dbQuery.put("type", "CCIJobStatus");
		
		DBObject result = MongoRestAPIService.get("CCIAliveStatus", dbQuery);
		
		lastUpDate = (String) result.get("lastDate");
		
		logger.debug(" before step method exiting");
	}

	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy+HH:mm:ss");
		if(InMemoryData.CCIDate != null) {
			String lastdate = f.format(InMemoryData.CCIDate);
		
			DBObject dbQuery = new BasicDBObject();
			dbQuery.put("type", "CCIJobStatus");
			
			DBObject statusObject = new BasicDBObject();
			statusObject.put("lastDate", lastdate);
			statusObject.put("type", "CCIJobStatus");
			MongoRestAPIService.updateWithPost("CCIAliveStatus",dbQuery, statusObject,true);
		}
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		return stepExecution.getExitStatus();
	}

}
