package com.times.mailer.batch.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.Constant;
import com.times.mailer.model.EpaperFileDetails;

public class EpaperFileReader implements ItemReader<EpaperFileDetails>, StepExecutionListener {
	private final static Logger logger = LoggerFactory.getLogger(EpaperFileReader.class);

	private List<EpaperFileDetails> epaperFileDetailsList = Collections
	        .synchronizedList(new ArrayList<EpaperFileDetails>());

	private int	perPage	= 10;
	private int	pageNo	= 1;

	private void getDBContent() {

		logger.debug(" beforeStep method..");

		if (epaperFileDetailsList == null || epaperFileDetailsList.isEmpty()) {

			BasicDBObject statusQuery = new BasicDBObject();
			statusQuery.put("status", 1);

			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			// optionMap.put("skip", String.valueOf((pageNo - 1) * perPage));

			List<DBObject> fileList = MongoRestAPIService.getList(Constant.EPAPER_RAW_DATA_COLLECTION, statusQuery,
			        optionMap);

			for (DBObject mrDBObject : fileList) {

				EpaperFileDetails efd = new EpaperFileDetails();
				efd.setFileName(mrDBObject.get("fileName").toString());
				efd.setGsfid(mrDBObject.get("gfsid").toString());
				efd.setId(mrDBObject.get("_id").toString());
				efd.setStatus(Boolean.getBoolean(String.valueOf(mrDBObject.get("status"))));

				ObjectId id = new ObjectId(efd.getGsfid());
				DBObject fileQuery = new BasicDBObject();
				fileQuery.put("_id", id);
				byte fileContent[] = MongoRestAPIService.getFile(Constant.EPAPER_FILE_COLLECTION, fileQuery);
				logger.debug("read " + fileContent == null ? "null"
				        : fileContent.length + " bytes from mra for gfsid " + efd.getGsfid());
				efd.setPdfFileContent(fileContent);

				epaperFileDetailsList.add(efd);

			}
			pageNo++;
		}
	}

	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");

	}

	@Override
	public EpaperFileDetails read()
	        throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		getDBContent();
		if (epaperFileDetailsList != null && epaperFileDetailsList.size() > 0) {
			EpaperFileDetails epaperFileDetails = getEpaperFileDetails();
			return epaperFileDetails;
		} else {
			return null;
		}

	}

	private EpaperFileDetails getEpaperFileDetails() {
		synchronized (this) {
			EpaperFileDetails efd = epaperFileDetailsList.remove(0);
			return efd;
		}
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		return null;
	}

}