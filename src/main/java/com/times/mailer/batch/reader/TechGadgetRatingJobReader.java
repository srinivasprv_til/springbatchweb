package com.times.mailer.batch.reader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.Constant;

public class TechGadgetRatingJobReader  implements ItemReader<DBObject>, StepExecutionListener{
	
	// -----------------------------------------------------------------------------------------------
	// Start Private members
		
	private static final Logger logger = LoggerFactory.getLogger(TechGadgetRatingJobReader.class);
	
	List<DBObject> productList = new ArrayList<DBObject>();
	
	private long startTime = System.currentTimeMillis();
	Calendar c = Calendar.getInstance(); 	
	
	// End Private members
	// -----------------------------------------------------------------------------------------------
	
	
	// -----------------------------------------------------------------------------------------------
	// Start public methods

	public String returnInsertedData() throws IOException {

		String fileName = "/opt/TechGadget/logs.txt";
		String line = null;
		String insertDate = null;

		try {
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while ((line = bufferedReader.readLine()) != null) {
				insertDate = line;
			}

			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			logger.error("Unable to open file '" + fileName + "'");
		}

		return insertDate;
	}
	
	@Override
	public DBObject read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		if (this.productList.size() > 0) {
				
			return this.productList.remove(0);
		} 
		return null;
	}
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		
		logger.debug(" Before Step method..");
		
		startTime = System.currentTimeMillis();
		
	    try {
			populateMsidUnameObject();
			
		} catch (InterruptedException e) {
			
			logger.error("Thread interrupted for tech gadget rating: "+e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		
		logger.debug("After Step method..");
		
		logger.debug("After Step status : " + stepExecution.getExitStatus().toString());
		
		if (startTime != 0) {
			logger.debug("Total execution time : " + ((System.currentTimeMillis() - startTime)/1000) + " second");
		}
		
		return stepExecution.getExitStatus();
	}
	
	// End public methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start private methods
	
	public void populateMsidUnameObject() throws InterruptedException, IOException, java.text.ParseException {

		DBObject query = new BasicDBObject();
		
		
		Date prvJobDate  = new Date();
		String insertDate = returnInsertedData();
		if (insertDate != null &&!(insertDate.equals("")) ) {
			SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

			 prvJobDate = simpleFormat.parse(insertDate);
			
		}
		else{
			c.set(2016, 5, 16, 9, 10, 30);
			prvJobDate = c.getTime();
		}
	
		Date endDate = new Date();
		
		Map<String, Object> optionMap = new HashMap<String, Object>();

		DBObject select = new BasicDBObject();
		select.put("msid", "1");
		
		optionMap.put("key", select);

		List<DBObject> list = new ArrayList<DBObject>();
		
		//query1.put("H_ID", "307");
		query.put("C_AT", new BasicDBObject("$gte", prvJobDate));
		query.put("H_ID", 307);
		try {
			list = MongoRestAPIService.getList(Constant.MOVIE_RATING_COLLECTION, query,optionMap);
		}

		catch (Exception e) {
			logger.error("Error while getting data from Mongo DB for msid list of gadgets: " + e);
		}
		Map<String, Object> optionMap1 = new HashMap<String, Object>();
		List<DBObject> list1 = new ArrayList<DBObject>();
		if(list.size()>0){
		for(DBObject dbObject:list){
			
			DBObject query1 = new BasicDBObject();
			BasicDBList query2 = new BasicDBList();
			
			query2.add(new BasicDBObject("reviewid.msid",dbObject.get("msid")));
			query2.add(new BasicDBObject("reviewid.parentid",dbObject.get("msid")));
			
			
			DBObject select1 = new BasicDBObject();
			select1.put("uname", "1");
			select1.put("reviewid", "1");
			optionMap1.put("key", select1);
		
			query1.put("$or",query2 );
			try{
				List<DBObject> dbObj = MongoRestAPIService.getList(Constant.PRODUCT_DETAIL_COLLECTION, query1,optionMap1);
				
				list1.add(dbObj.get(0));	
			}
		
			catch(Exception e){
				logger.error("Error while getting data from Mongo DB for msid list of gadgets: " + e);
			}
		}
		}

		if (list1 != null && list1.size() > 0) {
			productList.addAll(list1);
		}
//		if (list1 != null && list1.size() > 0){
//			BasicDBObject currentDate = new BasicDBObject();
//			currentDate.put("date", endDate);
//			MongoRestAPIService.update("last_updated_job_date", currentDate, date, true);
//			
//		}
		
	
		logger.warn("Size of Msid  list : " + this.productList.size());
	
		FileWriter fileWriter = new FileWriter("/opt/TechGadget/logs.txt", false);
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		bufferedWriter.write(simpleFormat.format(endDate.getTime()));
		
		bufferedWriter.close();
		fileWriter.close();
		
	}
	
	// End private methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start getters/setters

	// End getters/setters
	// -----------------------------------------------------------------------------------------------

}
