package com.times.mailer.batch.reader;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.model.TimeLineInstance;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;

/**
 * 
 * @author Rajeev Khatri
 *
 */
public class TimeLineReader implements ItemReader<Document>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(TimeLineReader.class);

//	private static String PRIO_URL ="http://etdev.indiatimes.com:8243/xml/prioritylist_home.cms";
//	private String PRIO_URL ;//="http://economictimes.indiatimes.com/xml/prioritylist_home.cms";
	
	//public static boolean isJobExecution = true;
	
	public static List<TimeLineInstance> timeLineInstanceList = new ArrayList<TimeLineInstance>();
	 private int index = 0;
	 Date lastProcessedDate = null;
/*	public String getPRIO_URL() {
		return PRIO_URL;
	}

	public void setPRIO_URL(String pRIO_URL) {
		PRIO_URL = pRIO_URL;
	}
*/
	public Document read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		if(timeLineInstanceList.size()>0){
			
			String response = null;
			Date today = new Date();//dateFormat.parse("20160525");//
			lastProcessedDate =today;
	        InMemoryData.instance = getInstanceObject(timeLineInstanceList,lastProcessedDate);
		
	    try {
			response = MongoRestAPIService.getExtData(InMemoryData.instance.getUrl());
			//time of getting data 
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		logger.info("got response from " +InMemoryData.instance.getUrl());

		if(response == null){
			return null;
		}

		SAXBuilder saxBuilder = new SAXBuilder();
		Document document = null;
		try {
			document = saxBuilder.build(new StringReader(response.toString()));			
		} catch(Exception e) {

		}
		return document;
		}else {
			return null;
		}

	/*	if (isJobExecution) {
			isJobExecution = false;
			return document;
		} else {
			return null;
		}
	*/
	}




	public void beforeStep(StepExecution stepExecution) {
		
      logger.debug(" beforeStep method..");
      
      JobParameters jobParams = stepExecution.getJobParameters();
	  int instanceId = Integer.parseInt(jobParams.getString("instanceId:"));
	
	 
		
		DBObject dbQuery = new BasicDBObject();
		dbQuery.put("instanceId", instanceId);
		//dbQuery.put("hostId", 53);
		
		List<DBObject> Objects = MongoRestAPIService.getList("TimelineMaster", dbQuery);
		for(int i =0 ;i <Objects.size();i++)
		{
			DBObject instanceObject = Objects.get(i);
			TimeLineInstance instance = new TimeLineInstance();
			instance.setUrl((String)(instanceObject.get("url")));
			instance.setHostId((Integer)(instanceObject.get("hostId")));
			instance.setInstanceId(instanceId);
			instance.setCampaignId((String)(instanceObject.get("campaignId")));
			instance.setLastProcessedDate((String)(instanceObject.get("lastProcessed")));
			timeLineInstanceList.add(i,instance );
			
			
		}
		
		
		
		
		logger.debug(" before step method exiting");
		
	}

	public ExitStatus afterStep(StepExecution stepExecution) {
		return null;
	}
	
	private TimeLineInstance getInstanceObject(List<TimeLineInstance>timeLineInstanceList,Date lastProcessedDate){
		TimeLineInstance instanceObject = timeLineInstanceList.remove(0);
		
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		DBObject updateQuery  = new BasicDBObject();
		updateQuery.put("instanceId",instanceObject.getInstanceId());
		updateQuery.put("hostId",instanceObject.getHostId());
		DBObject dbQuery = new BasicDBObject();
		dbQuery.put("lastProcessed", f.format(lastProcessedDate));
		
		DBObject update = new BasicDBObject();
		update.put("$set",dbQuery);
		MongoRestAPIService.update("TimelineMaster", updateQuery,update, true);
	    return instanceObject;
	}

}
