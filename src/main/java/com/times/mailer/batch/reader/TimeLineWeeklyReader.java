package com.times.mailer.batch.reader;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.model.TimeLineInstance;
import com.times.common.util.InMemoryData;

/**
 * 
 * @author Rajeev Khatri
 *
 */
public class TimeLineWeeklyReader implements ItemReader<List<DBObject>>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(TimeLineWeeklyReader.class);

	
	public static List<TimeLineInstance> timeLineInstanceList = new ArrayList<TimeLineInstance>();
	private int index = 0;
	Date lastProcessedDate = null;
	public static boolean isJobExecution = true;
   
	

	public List<DBObject> read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		
			if(InMemoryData.etTimeLineDate==null){
				InMemoryData.etTimeLineDate =new Date();
				
			}
			
		
		
			if(timeLineInstanceList.size()>0) {
				Date today = new Date();//dateFormat.parse("20160525");//
				lastProcessedDate =today;
		        InMemoryData.instance = getInstanceObject(timeLineInstanceList,lastProcessedDate);
		        InMemoryData.etTimeLineDate=today;
			 Calendar cal = Calendar.getInstance();
	    	 cal.setTime(InMemoryData.etTimeLineDate);
			cal.add(Calendar.DATE, -1);
			 InMemoryData.etTimeLineDate = cal.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			String dateStr = sdf.format(InMemoryData.etTimeLineDate);
          logger.debug("reading Documents from mongo");
			DBObject query = new BasicDBObject();
			query.put("DT", dateStr);
			query.put("TP", 0);
			query.put("hostId", InMemoryData.instance.getHostId());
			query.put("campaignId", InMemoryData.instance.getCampaignId());
            
		//	Date date1 = sdf.parse("16_09_2016");

			List<DBObject> existingDoc =  MongoRestAPIService.getList("Timeline", query);
			
			
	//	if(date1.equals(InMemoryData.etTimeLineDate))
	//	   {
				//isJobExecution = false;
		   
	//	   }
			if(existingDoc==null || existingDoc.isEmpty()){
				return null;
			}else{
				logger.debug("reading completed");
			return existingDoc ;
			
			}
		} else {
			return null;
		}
	}

  public void beforeStep(StepExecution stepExecution) {

		logger.debug(" beforeStep method..");

		JobParameters jobParams = stepExecution.getJobParameters();
		int instanceId = Integer.parseInt(jobParams.getString("instanceId:"));

		DBObject dbQuery = new BasicDBObject();
		dbQuery.put("instanceId", instanceId);
		// dbQuery.put("hostId", 53);

		List<DBObject> Objects = MongoRestAPIService.getList("TimelineMaster", dbQuery);
		for (int i = 0; i < Objects.size(); i++) {
			DBObject instanceObject = Objects.get(i);
			TimeLineInstance instance = new TimeLineInstance();
			instance.setUrl((String) (instanceObject.get("url")));
			instance.setHostId((Integer) (instanceObject.get("hostId")));
			instance.setInstanceId(instanceId);
			instance.setCampaignId((String) (instanceObject.get("campaignId")));
			instance.setLastProcessedDate((String) (instanceObject.get("lastProcessed")));
			timeLineInstanceList.add(i, instance);

		}

		logger.debug(" before step method exiting");

	}

	public ExitStatus afterStep(StepExecution stepExecution) {
		return null;
	}
	
	private TimeLineInstance getInstanceObject(List<TimeLineInstance>timeLineInstanceList,Date lastProcessedDate){
		TimeLineInstance instanceObject = timeLineInstanceList.remove(0);
		
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		f.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		DBObject updateQuery  = new BasicDBObject();
		updateQuery.put("instanceId",instanceObject.getInstanceId());
		updateQuery.put("hostId",instanceObject.getHostId());
		DBObject dbQuery = new BasicDBObject();
		dbQuery.put("lastProcessed", f.format(lastProcessedDate));
		
		DBObject update = new BasicDBObject();
		update.put("$set",dbQuery);
		MongoRestAPIService.update("TimelineMaster", updateQuery,update, true);
	    return instanceObject;
	}

}
