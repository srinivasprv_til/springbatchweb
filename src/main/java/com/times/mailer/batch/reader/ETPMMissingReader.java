package com.times.mailer.batch.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.InMemoryData;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.bson.types.ObjectId;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * 
 * @author praveen.bokka
 *
 */
public class ETPMMissingReader implements ItemReader<List<DBObject>>, StepExecutionListener {

	private final static Logger logger = LoggerFactory.getLogger(ETPMMissingReader.class);
	
	private final static String PMCOLLECTION = "ETPMCollection";
	
	private static String lastId;
	
	
	public List<DBObject> read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		
		logger.debug("etPMMissing read start...");
		DBObject query = new BasicDBObject();
		if(lastId != null) {
			DBObject gtObject = new BasicDBObject();
			gtObject.put("$gt", new ObjectId(lastId));
			query.put("_id", gtObject);
		}
		
		Map<String, Object> options = new HashMap<>();
		options.put("skip", 0);
		options.put("limit", 100);
		options.put("sort", "{\"_id\":1}");
		
		List<DBObject> objects = MongoRestAPIService.getList(PMCOLLECTION, query, options);
		
		if(objects != null && objects.size() > 0) {
			DBObject lastObject = objects.get(objects.size()-1);
			
			lastId = (String) lastObject.get("_id").toString();
			
			logger.debug("etPMMissing read end...");
			return objects;
		} else {
			
			logger.debug("etPMMissing read end...");
			return null;
		}
		
		
		
	}


	public void beforeStep(StepExecution stepExecution) {
		logger.debug(" beforeStep method..");
		
		DBObject dbQuery = new BasicDBObject();
		dbQuery.put("type", "PMMissingStatusStatus");
		
		DBObject result  = MongoRestAPIService.get("ETPMMissingStatus", dbQuery);
		if(result == null) {
			lastId = null;
		} else {
			lastId = (String) result.get("lastId");
		}
		
		logger.debug(" before step method exiting");
	}

	
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		
		DBObject dbQuery = new BasicDBObject();
		dbQuery.put("type", "PMMissingStatusStatus");
		
		DBObject rowJson = new BasicDBObject();
		rowJson.put("type", "PMMissingStatusStatus");
		rowJson.put("lastId", lastId);
		
		MongoRestAPIService.updateWithPost("ETPMMissingStatus", dbQuery, rowJson);
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		return stepExecution.getExitStatus();
	}

}
