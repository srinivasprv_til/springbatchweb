package com.times.mailer.batch;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.mail.internet.MimeUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.util.StringUtils;

import com.mongodb.DBObject;
import com.times.common.mail.EmailProcessor;
import com.times.common.mail.EmailVO;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.common.util.MailerUtil;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.dao.CustomParamDao;
import com.times.mailer.dao.MailerDao;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetterDailyTracker;

/**
 * This batch reader class is used to read the {@link MailUser} object.
 * 
 * @author manish.chandra
 * @author ranjeet.jha
 *
 */
public class MailerReader implements ItemReader<MailUser>, StepExecutionListener { //, JobExecutionListener

	//private final static Logger logger = Logger.getLogger(MailerReader.class);
	private final static Logger logger = LoggerFactory.getLogger(MailerReader.class);
	
	private EmailProcessor smtpMailer;

	private Mailer myMailer;
	private MailerDao dao;

	//private Boolean isExisting = false;
	private int perPage = 250;
	private int pageNo = 0;
	private String newsLetterId = null;
	private boolean isDaily = true; // if not daily then weekly newsletter.
	private long morningOrEvening = 1; // 1 for morning and 2 for evening.
	private long dailyOrWeekly = 1; //// 1 for daily and 2 for weekly.
	
	private List<MailUser> lstSubscribers = Collections.synchronizedList(new ArrayList<MailUser>());
	//private List<Mailer> lstMailer =  Collections.synchronizedList(new ArrayList<Mailer>());
	
	//code changes ... 
	boolean isInitialized = false;
	private String cacheUIKey = null;
	
	// This dao interface is used to keep pageNo for the running job with some stuff
	private CustomParamDao customParamDao;
	
	public static final String NL_FORMAT_TEXT = "text";
	public static final String NL_FORMAT_HTML = "html";
	public static final String NL_FORMAT_PARAM = "nlformat";
	public static final long TIME_INTERVAL = 10;
	
	
	/**
	 * This method is used to read the {@link MailUser} object.
	 * 
	 * @param args
	 */
	public MailUser read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		logger.debug("iread method start..");
		
	
		if (myMailer != null) {
			// push MailUser into the List
			
			this.pushSubscribersIntoList(myMailer, perPage, pageNo );
		}
		
		// if subscriber got then 
		if (lstSubscribers.size() > 0) {
			//counter update in memory
			MailUser subscriberUser = null;
			subscriberUser = getSubscriber();
			
			return subscriberUser;
		} else {
			//updates stats for the newsmailer ...
			for (Map.Entry<String, Integer> entry : InMemoryData.emailSentCounter.entrySet()) {
				this.updateEmailStats(entry.getKey());
			}
			
			//this.removeInMemoryData();
		}
		
		return null;
	}


	/**
	 * Get the Newsletter user/subscriber, if Newsletter assigned to {@link MailUser}.
	 * 
	 * @return
	 */
	private MailUser getSubscriber() {
		MailUser subscriberUser = null;
		synchronized (this) {
			if (lstSubscribers != null && lstSubscribers.size() > 0) {
				subscriberUser = lstSubscribers.remove(0);
				// update counter in map
				this.updateCounterInMap(subscriberUser);
			}
			/*if (subscriberUser.getNewsletter() == null) {
				subscriberUser = getSubscriber();
			}*/
			
		}
		return subscriberUser;
	}

	
	/**
	 * @param subscriberUser
	 */
	private void updateCounterInMap(MailUser subscriberUser) {
		//logger.debug("updateCounterInMap method ..");
		try {
			int lastCounter = 1;
			if (subscriberUser != null && subscriberUser.getNewsletter() != null) {
				//int dailyOrWeekly = subscriberUser.getNewsletter().isDaily() ? 1: 2;
				//int morningOrEvening = subscriberUser.getNewsletter().getScheduleTime();
				String sKey = subscriberUser.getNewsletter().getId(); 
				sKey = sKey + Constant.KEY_BUILDER_SEPERATOR + dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + morningOrEvening;
				if ( InMemoryData.emailSentCounter.containsKey(sKey)) {
					lastCounter = InMemoryData.emailSentCounter.get(sKey);
					InMemoryData.emailSentCounter.put(sKey, ++lastCounter);
				} else {
					InMemoryData.emailSentCounter.put(sKey, new Integer(lastCounter));
				}
			}
		} catch (Exception e) {
			logger.error("in side the updateCounterInMap method, msg : " + e.getMessage());
		}
	}

	/**
	 * This method is used to update stats finally for one newsletter.
	 * 
	 * @param sKey
	 */
	private void updateEmailStats(String sKey) {
		int iCount = InMemoryData.emailSentCounter.get(sKey);
		int ireadCount = 0;
		this.getDao().addMailerStats(sKey, 0, iCount);
		String code = null;
		if (InMemoryData.keyCodeMap.containsKey(sKey)) {
			code = InMemoryData.keyCodeMap.get(sKey);
		}
		dao.updateNewsLetterDailyTracker(code, sKey, ireadCount, iCount);
	}

	/**
	 * This method is used to push the subscriber into the List of {@link MailUser}.
	 *  
	 */
	private void pushSubscribersIntoList(Mailer myMailer, int perPage, int pageNo) {
		long dailyOrWeekly = 0;
		if (lstSubscribers == null || lstSubscribers.isEmpty()){
			long startTime = System.currentTimeMillis();
			synchronized (this) {
				this.pageNo = ++pageNo;
				
				try {
					if(myMailer.isDaily()) {
						dailyOrWeekly = 1;
					} else {
						dailyOrWeekly = 2;
					}
					// update the page no for this nlid, dailyWeekly.
					customParamDao.addOrupdatePageNo(myMailer.getId(), dailyOrWeekly, myMailer.getScheduleTime(), pageNo);
					/*Long instanceId = customParamDao.getLastHrFailedJobInstanceId();
					boolean isParamMatched = customParamDao.isFailedJob(myMailer.getId(), dailyOrWeekly, myMailer.getScheduleTime(), instanceId);
					if (isParamMatched) {
						int pgNo = customParamDao.getPageNo(myMailer.getId(), dailyOrWeekly, myMailer.getScheduleTime());
						this.pageNo = ++pgNo;
					}*/
				} catch (Exception e) {
					
				}
				
				logger.debug("NLID : " + myMailer.getId() + " , pageNo : " + pageNo);
				lstSubscribers = dao.getSubscriber(myMailer, perPage, pageNo);
				logger.debug(perPage + " User pull time : " + (System.currentTimeMillis() - startTime) + " ms");
			}
			
		}
	}

	/**
	 * This method is used to get the Newsletter masert i.e. Mailer List
	 */
	private List<Mailer> getMailer() {
		List<Mailer> lstMailer = getDao().getNewsletterScheduled(); 
		
		return lstMailer;
	}
	
	private List<Mailer> getMailer(String nlId, boolean isDaily) {
		List<Mailer> mailerMaster = dao.getNewsletterMailer(nlId, isDaily);
		return mailerMaster;
	}
		 
	
	/**
	 * This method is used to add the newsletter Body part in in-memory i.e. collection.
	 * 
	 * @param myMailer
	 */
	private void initializeNewsletter(Mailer myMailer) {
		try {
			
			String newsid = myMailer.getId();
			logger.debug("nl id :" + newsid + " going to pull data ...");
			String cmsMappingId = myMailer.getCmsMappingId();
			int scheduledTime = myMailer.getScheduleTime();
			int daily_weekly = myMailer.isDaily() ? 1 : 2;
			this.dailyOrWeekly = daily_weekly;
			this.morningOrEvening = scheduledTime;
			String skey1 = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + scheduledTime ;
			String uniqueCode = addNewsLetterDailyTrackerIfNotfound(myMailer, newsid, scheduledTime, daily_weekly, skey1);
			
			//get unique code... and pass it to template ... hash code (timestamp +skey1)
			String urlNewsletter =  myMailer.getHtmlPath(); //.replace("#$nlcode", uniqueCode); // HTML >>MAIN TEMPLATE :
			String urlNewsletterWidget = myMailer.getTemplatepath();//.replace("#$nlcode",uniqueCode) ; // HTML FRAGMENT
						
			// track the details for each day and generate the code.
			 
			//for html data 
			String scachekey = skey1 +Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_HTML;
			if (!InMemoryData.newsMailerUIMap.containsKey(scachekey)) {
				//code generate newslid ... 
				//	urlNewsletter = urlNewsletter.indexOf("?") != -1 ? urlNewsletter + "?nlformat=" + NL_FORMAT_HTML : urlNewsletter + "&nlformat=" + NL_FORMAT_HTML;
				String url4Html = urlNewsletter.indexOf("?") == -1 ? urlNewsletter +"?" : urlNewsletter + "&";
				url4Html = url4Html  + NL_FORMAT_PARAM + "=" + NL_FORMAT_HTML;
				String sData = MailerUtil.getDataRecursively(url4Html, 1);
				/*if (!StringUtils.hasText(sData)) {
					sData = MailerUtil.getData(url4Html);
				}*/
				if (sData != null) {
					sData = sData.replaceAll("##code##", uniqueCode);
					sData = com.times.common.util.StringUtils.replaceSpecialChar(sData);
					InMemoryData.newsMailerUIMap.put(scachekey, sData);
				}
				
				// 1001: TOI TOP headline NL and 1005 means TOI TechNews NL,
				if (StringUtils.hasText(sData) && ("1001".equals(newsid) || "1005".equals(newsid) || "1003".equals(newsid) || "1004".equals(newsid) 
						|| "1010".equals(newsid) || "1011".equals(newsid) || "1019".equals(newsid) || Constant.NLID_WITH_SUBJECT_FROM_TEMPLATES.contains(newsid))) {
					 if (sData.indexOf("<!--###") != -1) {
						 String subjectLine = sData.substring(sData.indexOf("<!--###") + 7, sData.indexOf("###-->"));
						 InMemoryData.newsMailerUIMap.put(skey1 + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1", subjectLine);
					 }
				 }
				
				// set sender from db to memory.
				if (Constant.NLID_WITH_SENDER_FROM_DB.contains(newsid)) {
					InMemoryData.newsMailerDataMap.put(newsid + Constant.KEY_BUILDER_SEPERATOR+ Constant.SENDER, myMailer.getMailerFrom());
				}
				
				// logg id data not found
				logIfDataNotFound(newsid, url4Html, sData);
			}
			
			
			logger.debug("cacheKey : " + scachekey);
			
			if ("1".equals(myMailer.getPersonalize())) {
				
				String[] arrID = null;
				if (cmsMappingId != null) {
					arrID = cmsMappingId.split("[,]");
				}
				
				for (String id : arrID) {
					logger.debug(id);
					int cmsid = Integer.parseInt(id.trim());
					String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + scheduledTime + Constant.KEY_BUILDER_SEPERATOR + id ;
					if (!InMemoryData.newsMailerDataMap.containsKey(skey+ Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_HTML)) {
						String sURL = urlNewsletterWidget.replaceAll("#msid", Integer.toString(cmsid)) ;
						sURL = sURL.indexOf("?") == -1 ? sURL + "?nlformat=" + NL_FORMAT_HTML : sURL + "&nlformat=" + NL_FORMAT_HTML;
						String sWidgetData = MailerUtil.getDataRecursively(sURL, 1);
						if (StringUtils.hasText(sWidgetData)) {  // retry once more as some time got null may be due to time out
							//sWidgetData = MailerUtil.getData(sURL);
							sWidgetData = com.times.common.util.StringUtils.replaceSpecialChar(sWidgetData);
							sWidgetData = sWidgetData.replaceAll("##code##", uniqueCode);
							InMemoryData.newsMailerDataMap.put(skey + Constant.KEY_BUILDER_SEPERATOR+"html", sWidgetData);
						}
						
						// logg id data not found
						logIfDataNotFound(newsid, sURL, sWidgetData);
						
					}
					
					
				}
			}
		} catch (Exception e) {
			logger.error("exception occured while retreiving content , msg: " + e.getMessage());
			
		}
	}

	/**
	 * This method is used to logged the data.
	 * 
	 * @param url4Html
	 * @param sData
	 */
	private void logIfDataNotFound(String nlId, String url4Html, String sData) {
		String msg = null;
		if (sData != null) {
			msg =  "url : " + url4Html + " , len : " + sData.length() + " nlid : " + nlId;
			logger.debug(" url : " + url4Html + " , len : " + sData.length() + " nlid : " + nlId);
			
		} else {
			logger.debug("ALERT ALERT NOT FOUND data for url : " + url4Html + " , nlid : " + nlId);
			try {
				smtpMailer.processStart("DATA NOT FOUND for URL : " + url4Html + " , nlid : " + nlId  + " MSG : " + msg);
				new CMSCallExceptions("ALERT ALERT NOT FOUND data for url : " + url4Html + " , nlid : " + nlId,
						CMSExceptionConstants.EXT_LOG, new Exception("Data Not found"));
			} catch (Exception e) {
				logger.debug("EXception url : " + url4Html + " , nlid : " + nlId + e.getMessage()); 
				new CMSCallExceptions("ALERT ALERT NOT FOUND data for url : " + url4Html + " , nlid : " + nlId + e.getMessage(),
						CMSExceptionConstants.EXT_LOG, e);
			}
		}
	}

	/**
	 * This method is used to add the newsLetterTracker if not found for that days, frequency, time and nlId.
	 * 
	 * @param myMailer
	 * @param newsid
	 * @param iTime
	 * @param itype
	 * @param skey1
	 */
	private String addNewsLetterDailyTrackerIfNotfound(Mailer myMailer, String newsid, int iTime, int itype, String skey1) {
		String uniqueCode= null;
		String yyymmdd = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		DBObject nlTracker = dao.getNewsLetterDailyTracker(newsid, yyymmdd, String.valueOf(iTime), String.valueOf(itype));
		if (nlTracker == null) {
			uniqueCode = getUniqueCodeByDate(skey1);
			NewsLetterDailyTracker nlDailyTracker = new NewsLetterDailyTracker(new Date(), Integer.parseInt(myMailer.getId()), uniqueCode, iTime, itype, yyymmdd);
			dao.addNewsLetterDailyTracker(nlDailyTracker); 
			
		} else {
			uniqueCode = String.valueOf(nlTracker.get(NewsLetterDailyTracker.NEWS_LETTER_CODE));
		}
		if (!InMemoryData.keyCodeMap.containsKey(skey1)) {
			InMemoryData.keyCodeMap.put(skey1, uniqueCode);
		}
		
		return uniqueCode;
	}
	
	/**
	 * This method is used to get the StringCode.
	 * @param str
	 * @return
	 */
	private String getUniqueCodeByDate(String key) {
		String date = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		String code = MailerUtil.getUniqueCodeByDate(date + key);
		return code;	
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(org.springframework.batch.core.StepExecution)
	 */
	public void beforeStep(StepExecution stepExecution) {
		
		
		logger.debug(" beforeStep method..");
		JobParameters jobParams = stepExecution.getJobParameters();
		this.newsLetterId = jobParams.getString(Constant.JOB_PARAM_NEWSLETTER_ID);
		//long dailyOrWeekly = jobParams.getLong(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY);
		if (Constant.JOB_PARAM_NL_DAILY == jobParams.getLong(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY)){
			this.isDaily = true;
			this.dailyOrWeekly = 1;
		} else {
			this.isDaily = false;
			this.dailyOrWeekly = 2;
		}
		
		//MorningEvening parameter
		//long morningOrEveningKey = jobParams.getLong(Constant.JOB_PARAM_NL_MORNING_EVE_KEY);
		if (Constant.JOB_PARAM_NL_DAILY == jobParams.getLong(Constant.JOB_PARAM_NL_MORNING_EVE_KEY)){
			this.morningOrEvening = 1;
		} else {
			this.morningOrEvening = 2; // for evening 2
		}
		
		// page no is applicable job is going to restart so that it will run from next page where batch running.
		if (jobParams.getString(Constant.JOB_PARAM_NL_RESTART_KEY) != null) {
			int pageNo = (int)jobParams.getLong(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO);
			this.pageNo = ++pageNo;
		} 
		
		//List<Mailer> mailers = dao.getNewsletterMailer(newsLetterId, isDaily);
		List<Mailer> mailers = this.getMailer(newsLetterId, isDaily);
		if (mailers != null && mailers.size() > 0) {
			this.myMailer = mailers.get(0);
		}
		if (myMailer != null) {
			// get mailer body content into memory
			this.initializeNewsletter(myMailer);
		}
		this.isInitialized = true;
		
		this.cacheUIKey = this.newsLetterId + Constant.KEY_BUILDER_SEPERATOR + this.dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + this.morningOrEvening;
		
		
		/*if (jobParams != null) {
			for (Map.Entry<String, JobParameter> entry : jobParams.getParameters().entrySet()) {
				logger.debug(entry.getKey() + " : " + entry.getValue().getValue());
			}
		}*/
	}

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.StepExecutionListener#afterStep(org.springframework.batch.core.StepExecution)
	 */
	public ExitStatus afterStep(StepExecution stepExecution) {
		logger.debug("afterStep method..");
		/*JobParameters jobParams = stepExecution.getJobParameters(); 
		if (jobParams != null) {
			for (Map.Entry<String, JobParameter> entry : jobParams.getParameters().entrySet()) {
				logger.debug(entry.getKey() + " : " + entry.getValue().getValue());
			}
		}*/
		
		reportMailer();
		this.removeInMemoryData();
		
		logger.debug("afterStep status : " + stepExecution.getExitStatus().toString());
		
		return stepExecution.getExitStatus();
		//return null;
	}
	
	private String encodeText(String subject, String baseSubject ) throws UnsupportedEncodingException {
		String encodedSubject = null;
		try {
			encodedSubject = MimeUtility.encodeText(subject);
			if (encodedSubject.indexOf("?Q?") != -1 || encodedSubject.indexOf("?B?")!= -1) {
				return baseSubject;
			}
		} catch (UnsupportedEncodingException e) {
			return baseSubject;
		}
		return encodedSubject;
	}
	
	

	/*public void beforeJob(JobExecution jobExecution) {
		logger.debug(" +++++++++++++++++++++++++++++++++++++ beforeJob started..");
		String nlId = (String)jobExecution.getExecutionContext().get("nlid");
		logger.debug("nlID: " + nlId);
	}

	public void afterJob(JobExecution jobExecution) {
		logger.debug(" +++++++++++++++++++++++++++++++++++++ afterJob started..");
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			// job success
		} else if (jobExecution.getStatus() == BatchStatus.FAILED) {
			// job failure
		}
		logger.debug("job going to finish..");
		String nlId = (String)jobExecution.getExecutionContext().get("nlid");
		logger.debug("nlID: " + nlId);
	}*/

	/**
	 * This method is used to remove the in-memory data for the that newsletter.
	 */
	private void removeInMemoryData() {
		//remove memory data for newsletter...
		//logger.debug("clearing newsMailerDataMap newsMailerUIMap emailSentCounter Map ..");
		if (InMemoryData.newsMailerDataMap.containsKey(this.cacheUIKey)){
			InMemoryData.newsMailerDataMap.remove(this.cacheUIKey);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_HTML)){
			InMemoryData.newsMailerUIMap.remove(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_HTML);
		}
		if (InMemoryData.newsMailerUIMap.containsKey(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_TEXT)){
			InMemoryData.newsMailerUIMap.remove(this.cacheUIKey + Constant.KEY_BUILDER_SEPERATOR + Constant.NL_FORMAT_TEXT);
		}
		if (InMemoryData.emailSentCounter.containsKey(this.cacheUIKey)) {
			InMemoryData.emailSentCounter.remove(this.cacheUIKey);
		}
	}
	
	private void reportMailer() {
		String newsid = myMailer.getId();
		
		String skey1 = newsid + Constant.KEY_BUILDER_SEPERATOR + this.dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + this.morningOrEvening ;
		String subjectTitle = InMemoryData.newsMailerUIMap.get(skey1 + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1");

		String scachekey = skey1 +Constant.KEY_BUILDER_SEPERATOR + NL_FORMAT_HTML;

		EmailVO vo = new EmailVO();
		
		
		String sBody = InMemoryData.newsMailerUIMap.get(scachekey);
		if("1002".equals(newsid)) {
			String cmsMappingId = myMailer.getCmsMappingId();
			String[] arrID = null;
			int index = 0;
			if (cmsMappingId != null) {
				arrID = cmsMappingId.split("[,]");
			}
			String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + this.dailyOrWeekly + Constant.KEY_BUILDER_SEPERATOR + this.morningOrEvening + Constant.KEY_BUILDER_SEPERATOR;
			for (String entry : arrID) {
				entry = skey + entry + Constant.KEY_BUILDER_SEPERATOR + "html";
				String swidget = InMemoryData.newsMailerDataMap.get(entry);	
				if (swidget != null) {
					sBody = sBody.replace("##pattern-newsletter##", swidget +"##pattern-newsletter##");		
					if (index == 0 && swidget.indexOf("<!--###") != -1) {
						subjectTitle = swidget.substring(swidget.indexOf("<!--###") + 7, swidget.indexOf("###-->"));						
					}
				}
				index++;
			}
			if (sBody != null && StringUtils.hasText(sBody)) {
				sBody = sBody.replace("##pattern-newsletter##", " ");
			}
			vo.setBody(sBody);
		} else {
			vo.setBody(sBody);
		}

		String prefix = "[NLID : " + newsid + ",DAILY_WEEKLY : " + this.dailyOrWeekly +  ",Subscribers : "  + InMemoryData.emailSentCounter.get(skey1) + "]";

		try {
			if (StringUtils.hasText(subjectTitle)) {
				//vo.setSubject(prefix + encodeText(myMailer.getMailSubject() + ": " + subjectTitle, myMailer.getMailSubject()));
				vo.setSubject(prefix + subjectTitle + "true");

			} else {
				vo.setSubject(prefix + subjectTitle + "false");
				//vo.setSubject(prefix + encodeText(myMailer.getMailSubject() + ": " + subjectTitle, myMailer.getMailSubject()));
			}
			vo.setRecipients("toi.daily.newsletter@gmail.com");

			smtpMailer.sendEMail(vo);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @return
	 */
	public Mailer getMyMailer() {
		return myMailer;
	}

	/**
	 * @param myMailer
	 */
	public void setMyMailer(Mailer myMailer) {
		this.myMailer = myMailer;
	}

	/**
	 * @return
	 */
	public MailerDao getDao() {
		return dao;
	}

	/**
	 * @param dao
	 */
	public void setDao(MailerDao dao) {
		this.dao = dao;
	}

	/**
	 * @param smtpMailer the smtpMailer to set
	 */
	public void setSmtpMailer(EmailProcessor smtpMailer) {
		this.smtpMailer = smtpMailer;
	}
	/**
	 * @param customParamDao the customParamDao to set
	 */
	public void setCustomParamDao(CustomParamDao customParamDao) {
		this.customParamDao = customParamDao;
	}
}
