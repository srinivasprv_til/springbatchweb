package com.times.mailer.batch;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.MimeUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.StringUtils;

import com.times.common.mail.EmailVO;
import com.times.common.util.Constant;
import com.times.common.util.InMemoryData;
import com.times.mailer.dao.MailerDao;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NewsLetter;

/**
 * This class is used as processor of Batch mailer in which building body content based on user selected.
 * spring batch forward this to MailerWriter class.
 * 
 * @author manish.chandra
 * @author Ranjeet.Jha
 */
public class MailerProcessor implements ItemProcessor<MailUser, EmailVO> {

	//private static final Logger log = Log4jLoggerFactory.getLog(MailerProcessor.class);
	private static final Logger log = LoggerFactory.getLogger(MailerProcessor.class);
	
	static String sDelimiter = "#$#";
	private MailerDao dao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	public EmailVO process(MailUser user) throws Exception {

		long startTime = System.currentTimeMillis();
		
		EmailVO vo = new EmailVO();
		NewsLetter news = user.getNewsletter();
		try {
			if (news != null) {
				int daily_weekly = news.isDaily() ? 1: 2;
				int morning_evening = news.getScheduleTime();
				String newsid = news.getId();
				String displayformat = news.getDisplayFormate();

				log.debug("newsLetter Id : " + newsid + " , " + user.getSsoid() + " , format : " +  news.getDisplayFormate());
				String skey = newsid + Constant.KEY_BUILDER_SEPERATOR + daily_weekly + Constant.KEY_BUILDER_SEPERATOR + morning_evening;
				String sBody = InMemoryData.newsMailerUIMap.get(skey+Constant.KEY_BUILDER_SEPERATOR+displayformat);

				String subjectTitle = InMemoryData.newsMailerUIMap.get(skey + Constant.KEY_BUILDER_SEPERATOR+ "TITLE_LINE_1");

				String sNav = "";
				boolean blnShowNav = false;
				// code for non personalised newsletter check
				if (sBody != null && news.getUserPref() != null	&& (news.getUserPref().trim().length() > 1)) {
					boolean isContentFounD = false;
					String ids = news.getUserPref();
					if (ids != null && !"null".equals(ids)) {

						String[] idArray = ids.split("[,]");
						String[] sNavArray = news.getSecName().split("[|]");
						if ("1002".equalsIgnoreCase(newsid) && InMemoryData.newsMailerDataMap != null ) {
							log.debug("section : " + idArray + " , " + sNavArray + ", Keyset " + InMemoryData.newsMailerDataMap.keySet());
						}
						if (sNavArray.length == idArray.length) {
							blnShowNav = true;
						}

						StringBuilder finalWidget = null;
						if (idArray != null) {
							int index = 0;
							for (String id : idArray) {
								String skey1 = newsid + Constant.KEY_BUILDER_SEPERATOR  + daily_weekly + Constant.KEY_BUILDER_SEPERATOR  + morning_evening + Constant.KEY_BUILDER_SEPERATOR + id + Constant.KEY_BUILDER_SEPERATOR + displayformat;

								if (InMemoryData.newsMailerDataMap.get(skey1) != null) {

									StringBuilder swidget = new StringBuilder(InMemoryData.newsMailerDataMap.get(skey1));	
									if (index == 0 && swidget.indexOf("<!--###") != -1) {
										subjectTitle = swidget.substring(swidget.indexOf("<!--###") + 7, swidget.indexOf("###-->"));									
									}
									if (finalWidget != null) {
										finalWidget = finalWidget.append(swidget);
									} else {
										finalWidget = new StringBuilder(swidget);
									}
									swidget = null;
									isContentFounD = true;
								}
								index++;
							}						

						}

						if (finalWidget != null) {
							sBody = sBody.replace("##pattern-newsletter##", finalWidget);
							finalWidget = null;
						} else {
							sBody = sBody.replace("##pattern-newsletter##", " ");
						}

					} 

					// if digest newsletter and section is blank then not send the maile if section is not blank then send 
					sBody = sBody.replaceAll("#emailid#" , user.getEmailId());
					vo.setBody(sBody);
					sBody = null;

					if (StringUtils.hasText(subjectTitle)) {
						// MimeUtility.encodeText() is used to parsed encoding in properformate to avoid junk char 
						if("1005".equals(newsid) || "1004".equals(newsid) || "1010".equals(newsid) || "1011".equals(newsid) || "1019".equals(newsid) || Constant.NLID_WITH_SUBJECT_FROM_TEMPLATES.contains(newsid)) { // || "1010".equals(newsid) || "1011".equals(newsid)
							vo.setSubject(encodeText(subjectTitle, user.getEmailSubject()));
						} else if("1003".equals(newsid)) {
							vo.setSubject(encodeText(user.getEmailSubject() + ": " + subjectTitle + " and more", user.getEmailSubject()));
						} else {
							vo.setSubject(encodeText(user.getEmailSubject() + ": " + subjectTitle, user.getEmailSubject()));
						}
					} else {
						vo.setSubject(encodeText(user.getEmailSubject(), user.getEmailSubject()));
					}

					vo.setRecipients(user.getEmailId());
					
					
					// set sender details
					if (Constant.NLID_WITH_SENDER_FROM_DB.contains(newsid)) {
						String senderKey = newsid + Constant.KEY_BUILDER_SEPERATOR  + Constant.SENDER;
						if (InMemoryData.newsMailerDataMap.get(senderKey) != null) {
							vo.setSender(InMemoryData.newsMailerDataMap.get(senderKey));
						}
					}
				}
			}
		} catch (Exception e) {
			log.debug("MailerProcessor exception ");
			e.printStackTrace();			
		}
		
		log.debug("Mailer Process : " + (System.currentTimeMillis() - startTime) + " ms");
		return vo;
	}

	/**
	 * Encode the subject line and if any {@link UnsupportedEncodingException} exception raised then return base subject line.
	 * by separating colon char i.e. ":" else throw UnsupportedEncodingException.
	 * 
	 * @param subject
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String encodeText(String subject, String baseSubject ) throws UnsupportedEncodingException {
		String encodedSubject = null;
		try {
			
			for (String key : InMemoryData.SPECIAL_SYMBOL.keySet()) {
				subject = subject.replaceAll(key, InMemoryData.SPECIAL_SYMBOL.get(key));
			}
			
			encodedSubject = MimeUtility.encodeText(subject);
			if (encodedSubject.indexOf("?Q?") != -1 || encodedSubject.indexOf("?B?")!= -1) {
				return baseSubject;
			}
		} catch (UnsupportedEncodingException e) {
			return baseSubject;
		}
		return encodedSubject;
	}
	
	/**
	 * @param idArray
	 * @param sNavArray
	 * @return
	 */
	private static String getTableWidget(String[] sNavArray) {
		StringBuilder sb = new StringBuilder();
		int noOfCol = 4; 
		int iCount = 0;
		Map<String, String> styles = new HashMap<String, String>(){{
			
			this.put("00", "background-color:#4f4f4f;width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191;border-left:1px solid #000000;");
			this.put("01", "width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191");
			this.put("02", "background-color:#4f4f4f;width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191");
			this.put("03", "width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191;border-right:none;");

			this.put("10", "width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191;border-left:1px solid #000000;");
			this.put("11", "background-color:#4f4f4f;width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191");
			this.put("12", "width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191");
			this.put("13", "background-color:#4f4f4f;width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191;border-right:none;");
			
			
		}};
 		//noOfCol = StringUtils.getNoOfColInRow(sNavArray);
		int totalRow =(int) Math.ceil((double)sNavArray.length/(double)noOfCol);
		sb.append("<table cellpadding='0' cellspacing='0' style='float: left;list-style-type: none;margin: 0;padding: 0;swidth:479px;background-color:#353535;color:#0050a2;'>");
		int colSpan = (totalRow*noOfCol - sNavArray.length) ;
		for(int row=0 ; row<totalRow; ++row){
			sb.append("<tr>");
			int rowOddity = row % 2;
			for(int col = 0; col < noOfCol && iCount < sNavArray.length  ; col++){
				boolean colSpanApplicable = false;
				if(totalRow -1 == row && iCount ==  sNavArray.length - 1){
					colSpanApplicable = true;
				}
				
				sb.append("<td ").append(" style='"+styles.get(rowOddity+""+col)+"' >");
				if(colSpanApplicable){
					sb.append("<a style='color:#ffffff' href='#").append(sNavArray[iCount]).append("'>").append(sNavArray[iCount]).append("</a>");
					sb.append("</td>");
					sb.append("<td ").append(" colspan=").append("'"+colSpan+"'>");
					sb.append("</td>");
					iCount++;
					continue;
				}
				//sb.append("<a style='color:#ffffff' href='").append(InMemoryData.SECTION_MAP.get(idArray[iCount])).append("' />").append(sNavArray[iCount]).append("</a>");
				sb.append("<a style='color:#ffffff' href='#").append(sNavArray[iCount]).append("'>").append(sNavArray[iCount]).append("</a>");
				sb.append("</td>");
				++iCount;
			}
			sb.append("</tr>");
		}
		sb.append("</table>");
		//log.debug(sb.toString());
		return sb.toString();
	}
	
	public MailerDao getDao() {
		return dao;
	}

	public void setDao(MailerDao dao) {
		this.dao = dao;
	}
	
	
	
}
