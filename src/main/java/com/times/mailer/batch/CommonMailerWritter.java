package com.times.mailer.batch;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.EmailVO;
import com.times.common.util.ExternalHttpUtil;

/**
 * This batch writer is used to send the mail to the subscribed user.
 * 
 *
 * @author Rajeev Khatri
 *
 */
public class CommonMailerWritter implements ItemWriter<EmailVO> {	
	private static final Log log = LogFactory.getLog(CommonMailerWritter.class);
	
	
	public void write(List<? extends EmailVO> mail) throws Exception {
		log.debug("inside the write ");
		if (mail != null && mail.get(0) != null && mail.get(0).getBody() != null) {
			
			String result = ExternalHttpUtil.postRequestWithBody(com.times.common.util.Constant.mailerServiceUrl, mail.get(0).toString());
			System.out.println(result);
		}		
	}

}