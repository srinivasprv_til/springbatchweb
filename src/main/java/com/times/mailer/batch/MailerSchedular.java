/**
 * 
 */
package com.times.mailer.batch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import com.times.mailer.dao.MailerDao;
import com.times.mailer.model.Mailer;

/**
 * This class is used for partitioning of mail execution context.
 * 
 * @author manish.chandra
 *
 */
public class MailerSchedular implements Partitioner {

	private static final Log log = LogFactory.getLog(MailerSchedular.class);
	
	/**
	 * @param args
	 */
	private MailerDao dao;
	
	
	/* (non-Javadoc)
	 * @see org.springframework.batch.core.partition.support.Partitioner#partition(int)
	 */
	public Map<String, ExecutionContext> partition(int size) {
		List<Mailer> mailer = getDao().getNewsletterScheduled();
		int imax =(mailer!=null)? mailer.size():0;
		int imin =0;
	
		Map<String, ExecutionContext> result = new HashMap<String, ExecutionContext>();
		if (imax > 0){
		while (imin < imax){
			
			Mailer myMailer = mailer.get(imin);
	//		initializeNewsletter(myMailer);
			
			ExecutionContext ecValue = new ExecutionContext();
			result.put("newsletter", ecValue);
			ecValue.put("mailer", mailer.get(imin));
			
			imin+=1;
		}
		}
		return result;
	}

	/**
	 * @param myMailer
	 */
	public MailerDao getDao() {
		return dao;
	}

	public void setDao(MailerDao dao) {
		this.dao = dao;
	}

	 
}
