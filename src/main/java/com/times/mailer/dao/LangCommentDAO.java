package com.times.mailer.dao;

import java.util.List;
import java.util.Map;

import com.times.mailer.model.LangUserMailer;

public interface LangCommentDAO {

	List<LangUserMailer> getUserComments();
	List<LangUserMailer> getParentComments(int eroid);
	Map<String, Object> getCMSObject(int msid);
	Boolean UpdateMailerStats(int eroid);

}
