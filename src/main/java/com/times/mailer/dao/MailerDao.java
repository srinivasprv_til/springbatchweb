package com.times.mailer.dao;

import java.util.Date;
import java.util.List;

import com.mongodb.DBObject;
import com.times.mailer.model.LangConfiguration;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetterDailyTracker;
import com.times.mailer.model.SMSUser;

/**
 * This DAO implementation class is used to interact for mailer related Database.
 * 
 * @author manish.chandra
 * @author Ranjeet.Jha
 *
 */
public interface MailerDao {

	/**
	 * This method is used to get <code>List</code> of {@link Mailer} object.
	 * 
	 * @return
	 */
	public List<Mailer> getNewsletterScheduled();
	
	/**
	 * This method is used to get <code>List</code> of {@link Mailer} object.
	 * 
	 * @return
	 */
	public List<Mailer> getNewsletterMailer(String newsletterId, boolean isDaily);
	
	/**
	 * This method is used to get <code>List<MailUser></code> object by {@link Mailer}.
	 * 
	 * @param mailer
	 * @return
	 */
	public List<MailUser> getSubscriber(Mailer mailer, int perPage, int pageNo); 
	
	/**
	 * This method is used to get {@link DBObject} of emailTemplateBy Date and newsletterId.
	 * 
	 * @param newsletterId
	 * @param date
	 * @param scheduledTime
	 * @param schedledFrequency
	 * @return
	 * @throws Exception
	 */
	public DBObject getNewsLetterDailyTracker(String newsletterId, String date, String scheduledTime, String schedledFrequency) ;
	
	/**
	 * This method is used to add the {@link NewsLetterDailyTracker} in provided collection for each emailTemplate for each day.
	 * 
	 * @param dbObject
	 * @throws Exception
	 */
	public  void addNewsLetterDailyTracker(NewsLetterDailyTracker dbObject) ;
	
	/**
	 * @param skey
	 * @param ireadCount
	 * @param imailCount
	 */
	public void addMailerStats(String skey, int ireadCount, int imailCount);
	
	/**
	 * This method is used to update the {@link NewsLetterDailyTracker} for the provided parameter.
	 * 
	 * @param dbObject
	 * @throws Exception
	 */
	public  void updateNewsLetterDailyTracker(String code, String key, int userCount, int sentCount) ;
	
	/**
	 * This method is used to get <code>List</code> of DBObject between two dates and based on provided criteria.
	 *  
	 * @param nlId
	 * @param verifiedUser
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<DBObject> getUsersDBObjectBetweenDates(String nlId, boolean verifiedUser, Date startDate, Date endDate) ;
	
	/**
	 * @param nlId
	 * @return
	 */
	public List<DBObject> getNewsletterDBObjectById(String nlId);
	
	
	public List<LangConfiguration> getCommentsConfig();
	
	
	public List<MailUser> getSubscriber(Mailer mailer, int perPage, int pageNo, String collection);
	
	public List<SMSUser> getSMSSubscriberDetails(Mailer mailer, int perPage, int pageNo);
	
	public void updateSMSFLAGEntry(String id);
}
