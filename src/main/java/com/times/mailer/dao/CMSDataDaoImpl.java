/**
 * 
 */
package com.times.mailer.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

/**
 * This class provides apis to manipulate with meta data.
 *
 * @author Rajeev Khatri
 * @author Ranjeet.Jha
 *
 */
public class CMSDataDaoImpl implements CMSDataDao {

	private static final Logger log = LoggerFactory.getLogger(CMSDataDaoImpl.class);

	private SimpleJdbcTemplate jdbcTemplate;
	
	// to call for liveblog procedure, to get active Live Blog
	private SimpleJdbcCall simpleJdbcCall;

	/*
	public static final String getUserStatsArticlesByTimeSql = "select top #top m.msid"  + 
			" from objhourlystats o (nolock), hierarchy h (nolock), masters m (nolock), content c(nolock)" +
			" where  h.msid=o.msid and h.hostid=o.hostid and urtype=:urtype  and m.msid=o.msid and m.mstype=2 and " +
			" o.pmstartdatetime >= :sinceDate and h.hostid=:hostid and pmperiodcode=:periodtype and h.hisactive=2 and c.msid=m.msid " + 
			" group by m.msid,h.mas_msid,m.msname,c.art_date order by sum(o.ohscount) desc"; */
	
	public static final String getUserStatsArticlesByTime = "select top #top m.msid from objhourlystats o (nolock), hierarchy h (nolock)," +
	" masters m (nolock),hierarchy h1 (nolock) where  h.msid=o.msid and h.hostid=o.hostid AND URTYPE=#urtype and m.msid=o.msid and m.mstype=2 and m.mssubtype!=8 " + 
	" and datediff(d,pmstartdatetime, getdate()) <=#adddays and h.hostid=#hostid and pmperiodcode=5 and h.hisactive=2 and h1.msid=h.mas_msid " + 
	"and h.hostid=h1.hostid and h1.hisactive=2 group by m.msid,m.mstype,m.mssubtype,h.mas_msid order by sum(o.ohscount) desc";


	public CMSDataDaoImpl(DataSource ds) {
		this.jdbcTemplate = new SimpleJdbcTemplate(ds);
		this.simpleJdbcCall = new SimpleJdbcCall(ds);
	}

	public List<Map<String, Object>> getMostReadArticalData(String hostId, int urType, int top, int durationDays) {

		List<Map<String, Object>> mp = null;
		String query = getUserStatsArticlesByTime;

		try {
			query = query.replace("#top", String.valueOf(top));
			query = query.replace("#urtype", String.valueOf(urType));
			query = query.replace("#hostid", hostId);
			query = query.replace("#adddays", String.valueOf(durationDays));

			mp = jdbcTemplate.queryForList(query);

		} catch (Exception e) {
			log.error("Exception caught while addOrupdatePageNo , msg : " + e.getMessage());
		}

		return mp;
	}

	@Override
	public List<Map<String, Object>> getActiveLiveBlog(int hostid, String updatedDate)	throws Exception {
		
		Map<String, Object> args = new HashMap<String, Object>(2);
		args.put("@hostid", hostid);
		args.put("@lastupdated", updatedDate);
		simpleJdbcCall.setProcedureName("sp_getLiveBlogs");
		Map<String, Object> m = simpleJdbcCall.execute(args);
		if (m != null) {
			if (((List) m.get("#result-set-1")).size() > 0) {
				return (List<Map<String, Object>>) m.get("#result-set-1");
			} else {
				return null;
			}
		}
		return null;
	}

	/**
	 * @param simpleJdbcCall the simpleJdbcCall to set
	 */
	public void setSimpleJdbcCall(SimpleJdbcCall simpleJdbcCall) {
		this.simpleJdbcCall = simpleJdbcCall;
	}
}
