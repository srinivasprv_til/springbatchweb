package com.times.mailer.dao;

import java.util.List;

import com.times.mailer.model.ImageInfo;

public interface ImageOptimizerDao {
	
	public List<ImageInfo> getImageInfoList(String id, String omno);
	
	public int updateImageData(ImageInfo imageInfo);
	
}
