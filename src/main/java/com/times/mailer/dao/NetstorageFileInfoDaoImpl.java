/**
 * 
 */
package com.times.mailer.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.times.common.util.MailerUtil;
import com.times.mailer.model.AmazonS3Credits;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.NetstorageInfo;
import com.times.mailer.model.FileLocationInfo.LocationType;

/**
 * This DAO class is used to interact netstorage file upload related table to pull List of domain model object and update the status.
 * 
 * @author Ranjeet.Jha
 *
 */
public class NetstorageFileInfoDaoImpl implements NetstorageFileInfoDao {

	private static final Logger log = LoggerFactory.getLogger(NetstorageFileInfoDaoImpl.class);
	
	private SimpleJdbcTemplate jdbcTemplate;
	
	private static final String QUERY_GET_FILE_UPLOAD ="select u.id as id, m.id as mid, m.server_name, m.server_location, m.userid, m.passcode, m.location_type, u.source_url, u.active, u.work_type, u.target_filename "+
			" from  NETSTORAGE_LOCATION_MASTER m, netstorage_url_master as u where u.work_type = ? and u.netstorage_id=m.id and active = 1 limit ?, ?";
	
	private static final String QUERY_GET_FILE_UPLOAD_HOST_ID ="select u.id as id, m.id as mid, u.hostid, m.server_name, m.server_location, m.userid, m.passcode, m.location_type,  u.source_url, u.active, u.work_type, u.target_filename "+
			" from  NETSTORAGE_LOCATION_MASTER m, netstorage_url_master as u where u.hostid = ? and u.work_type = ? and u.netstorage_id=m.id and active = 1 limit ?, ?";
	
	private static final String QUERY_GET_FILE_UPLOAD_BY_WORK_TYPE ="select u.id as id, m.id as mid, u.hostid, m.server_name, m.server_location, m.userid, m.passcode, m.location_type,  u.source_url, u.active, u.work_type, u.target_filename "+
			" from  NETSTORAGE_LOCATION_MASTER m, netstorage_url_master as u where u.work_type = ? and u.netstorage_id=m.id and active = 1";
	
	private static final String QUERY_UPDATE_FILE_UPLOAD ="update netstorage_url_master set last_updated_at = now() where id = ?";
	
	private static final String QUERY_GET_ACTIVE_MATCH ="select id, matchid, status, startedAt, endedAt from cricket_match where status = 1";
	
	public NetstorageFileInfoDaoImpl(DataSource ds) {
		this.jdbcTemplate = new SimpleJdbcTemplate(ds);
	}
	
	@Override
	public List<FileLocationInfo> getFileLocationInfo(int workType, int offset, int length) {
		List<Map<String, Object>> dataList = jdbcTemplate.queryForList(QUERY_GET_FILE_UPLOAD, new Object[] {workType, offset, length});
		log.error("Query : " + QUERY_GET_FILE_UPLOAD + " ,offset : " + offset + " ,length : " + length );
		if (dataList != null && dataList.size() > 0) {
			return getFileLocationInfoList(dataList);
		}
		return null;
	}

	@Override
	public void updateFileLocationInfo(FileLocationInfo fileLocationInfo) {
		jdbcTemplate.update(QUERY_UPDATE_FILE_UPLOAD, new Object[] {fileLocationInfo.getId()});
		
	}

	/**
	 * This method convert to the domain model list.
	 * 
	 * @param listMap
	 * @return
	 */
	private List<FileLocationInfo> getFileLocationInfoList(List<Map<String, Object>> listMap) {
		List<FileLocationInfo> list = null;
		if (listMap != null && listMap.size() > 0) {
		 list = new ArrayList<FileLocationInfo>(listMap.size());
		 for (Map<String, Object> entry : listMap) {
			// TODO : as per value of new field in table either populate
				// netstorage or aws credentials
				FileLocationInfo fileLocation = new FileLocationInfo();

				fileLocation.setSourceUrl(String.valueOf(entry.get("source_url")).trim());
				log.error("source_url : " + fileLocation.getSourceUrl());
				fileLocation.setStatus("true".equals(String.valueOf(entry.get("active"))) ? true : false);
				fileLocation.setId(MailerUtil.getInt(String.valueOf(entry.get("id"))));
				fileLocation.setWorkType(MailerUtil.getInt(String.valueOf(entry.get("work_type"))));
				fileLocation.setLocationType(
						LocationType.fromInt(MailerUtil.getInt(String.valueOf(entry.get("location_type")))));
				if (LocationType.NET_STORAGE == fileLocation.getLocationType()) {
					NetstorageInfo nInfo = new NetstorageInfo();
					// m.server_name, m.server_location, m.userid, m.passcode,
					// u.source_url, u.active, u.work_type, u.target_filename
					nInfo.setId(MailerUtil.getInt(String.valueOf(entry.get("mid"))));
					nInfo.setServerName(String.valueOf(entry.get("server_name")));
					nInfo.setFolderLocation(String.valueOf(entry.get("server_location")));
					nInfo.setUserid(String.valueOf(entry.get("userid")));
					nInfo.setPassword(String.valueOf(entry.get("passcode")));
					nInfo.setFileName(String.valueOf(entry.get("target_filename")));

					fileLocation.setNetstorageLocation(nInfo);
				} else if (LocationType.AWS == fileLocation.getLocationType()) {
					String accessKey = String.valueOf(entry.get("userid"));
					String secretKey = String.valueOf(entry.get("passcode"));
					String bucketName = String.valueOf(entry.get("server_name"));
					String fileName = String.valueOf(entry.get("server_location")) + "/"
							+ String.valueOf(entry.get("target_filename"));

					fileLocation.setAmazonS3(new AmazonS3Credits(accessKey, secretKey, bucketName, fileName, null));

				}

				// add to list
				list.add(fileLocation);
			}
		}
		
		return list;
	}

	@Override
	public List<FileLocationInfo> getFileLocationInfo(int hostid, int workType,	int offset, int length) {
		if (length == 0) {
			length = 100;
			offset = 0;
		}
		List<Map<String, Object>> dataList = jdbcTemplate.queryForList(QUERY_GET_FILE_UPLOAD_HOST_ID, new Object[] {hostid, workType, offset, length});
		if (dataList != null && dataList.size() > 0) {
			return getFileLocationInfoList(dataList);
		}
		return null;
	}
	
	@Override
	public List<FileLocationInfo> getFileLocationInfoByWorkType(int workType) {
		
		List<Map<String, Object>> dataList = jdbcTemplate.queryForList(QUERY_GET_FILE_UPLOAD_BY_WORK_TYPE, new Object[] { workType});
		if (dataList != null && dataList.size() > 0) {
			return getFileLocationInfoList(dataList);
		}
		return null;
	}

	@Override
	public List<Integer> getActiveMatchIdList() {
		List<Integer> machList = null;
		List<Map<String, Object>> dataList = jdbcTemplate.queryForList(QUERY_GET_ACTIVE_MATCH);
		if (dataList != null && dataList.size() > 0) {
			machList = new ArrayList<Integer>(dataList.size());
			 for (Map<String, Object> entry : dataList) {
				 machList.add(MailerUtil.getInt(String.valueOf(entry.get("matchid"))));
			}
		}
		return machList;
	}
	
	
	public Date getFileUploadAuditExecutionDate(String url) {
		Date prevDate = null;
		String query = "SELECT LAST_EXECUTION_STAMP FROM FILE_UPLOAD_AUDIT WHERE URL=?";		
		List<Map<String, Object>> dataList = jdbcTemplate.queryForList(query, new Object[] {url});
		if (dataList != null && dataList.size() > 0) {

			for (Map<String, Object> entry : dataList) {
				prevDate = (Date) entry.get("LAST_EXECUTION_STAMP");
				break;
			}
		}
		return prevDate;
	}
	
	
	public void insertFileUploadAuditEntry(String msid, String url) {
		try {
			String query = "INSERT INTO FILE_UPLOAD_AUDIT(MSID, URL,LAST_EXECUTION_STAMP) VALUES(?,?,?)";
			jdbcTemplate.update(query, new Object[] {msid, url,new Date()});
		} catch (Exception e) {
			log.error("Exception caught while insertLiveBlogExecution , msg : " + e.getMessage());
		}
	}
	
	
	public void updateFileUploadAuditExecution(String url) {
		try {
			String query = "UPDATE FILE_UPLOAD_AUDIT SET LAST_EXECUTION_STAMP=? WHERE URL=?";
			jdbcTemplate.update(query, new Object[] {new Date(), url});
		} catch (Exception e) {
			log.error("Exception caught while updateLastExecution , msg : " + e.getMessage());
		}
	}
	
	
}
