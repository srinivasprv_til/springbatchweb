package com.times.mailer.dao;

public interface EpollDao {

	void  saveEpollData(int candidateId, int statusId);
	
}
