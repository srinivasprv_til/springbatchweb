package com.times.mailer.dao;

import java.util.Set;

public interface AmazonParamDao {
	
	public Set<String> getProductIdsList(int metaType);

}
