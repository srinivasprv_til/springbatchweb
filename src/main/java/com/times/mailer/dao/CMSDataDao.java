package com.times.mailer.dao;

import java.util.List;
import java.util.Map;

/**
 * This DAO interface is used to get connect to the CMS object.
 * 
 * @author Ranjeet.Jha
 * @author Rajeev Khatri
 * 
 *
 */
public interface CMSDataDao {
	
	List<Map<String, Object>> getMostReadArticalData(String hostId, int urType, int top, int durationDays);
	
	/**
	 * This method is used to get the active live blog for the provided hostid.
	 * 
	 * @param hostid
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getActiveLiveBlog(int hostid, String updatedDate) throws Exception;
}
