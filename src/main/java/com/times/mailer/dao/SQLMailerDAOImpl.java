package com.times.mailer.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.times.nbt.model.Newsletter;

public class SQLMailerDAOImpl implements SQLMailerDAO {

	NamedParameterJdbcTemplate simpleJDBCTemplate;
	DataSource dataSource;
	
	
	public SQLMailerDAOImpl(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
		if (simpleJDBCTemplate==null)
			simpleJDBCTemplate = new NamedParameterJdbcTemplate(dataSource);
	}


	public NamedParameterJdbcTemplate getSimpleJDBCTemplate() {
		return simpleJDBCTemplate;
	}


	public void setSimpleJDBCTemplate(NamedParameterJdbcTemplate simpleJDBCTemplate) {
		this.simpleJDBCTemplate = simpleJDBCTemplate;
	}


	public DataSource getDataSource() {
		return dataSource;
	}


	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}


	@Override
	public int SendMail(Newsletter mailer) {
		// TODO Auto-generated method stub
		String sql= "insert into msgq(MsgSenderName,MsgSenderMailID,MsgSubject,MsgToList,Msg, msgContentType) " +
				"values(:MsgSenderName,:MsgSenderMailID,:MsgSubject,:MsgToList,:Msg,:contentType)";
		
		Map<String, Object> mapParams = new HashMap<String, Object>();
		mapParams.put("MsgSenderName", mailer.getMsgSenderName());
		mapParams.put("MsgSenderMailID", mailer.getMsgSenderEmail());
		mapParams.put("MsgSubject", mailer.getMailSubject());
		mapParams.put("MsgToList", mailer.getMsgTo());
		mapParams.put("Msg", mailer.getMailContent());
		mapParams.put("contentType", "text/html");
		SqlParameterSource sqlParams = new MapSqlParameterSource(mapParams);
		return (simpleJDBCTemplate.update(sql, sqlParams));
	
	}

	
	
}
