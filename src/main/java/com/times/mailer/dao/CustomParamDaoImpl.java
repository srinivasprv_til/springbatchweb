/**
 * 
 */
package com.times.mailer.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.times.common.util.DateUtils;

/**
 * @author Ranjeet.Jha
 *
 */
public class CustomParamDaoImpl implements CustomParamDao {

	private static final Logger log = LoggerFactory.getLogger(CustomParamDaoImpl.class);
	
	private SimpleJdbcTemplate jdbcTemplate;

	private static final int PREV_HR = -3; 
	
	public CustomParamDaoImpl(DataSource ds) {
		this.jdbcTemplate = new SimpleJdbcTemplate(ds);
	}
	
	public void isJobFailed(SimpleJobOperator jobOperator, String nlid, String dailyWeekly, String mornEvening) {
		boolean status = false;
		if (jobOperator != null) {
			//jobOperator.get
		}
		
		//return status;
	}
	
	public long getInstanceId(String nlid, long dailyOrWeekly, long mornOrEven) {
		long instanceId = 0;
		String query = "select ex.JOB_EXECUTION_ID, ex.JOB_INSTANCE_ID, ex.STATUS, ex.EXIT_CODE  from BATCH_JOB_EXECUTION ex , BATCH_JOB_PARAMS pm where exit_code ='FAILED' and ex.JOB_INSTANCE_ID = pm.JOB_INSTANCE_ID and KEY_NAME ='nlid' and STRING_VAL = ?";
		//String query = "select JOB_EXECUTION_ID, JOB_INSTANCE_ID, STATUS, EXIT_CODE from BATCH_JOB_EXECUTION where exit_code ='FAILED' ";
		List<Map<String, Object>> dataList = jdbcTemplate.queryForList(query, new Object[] {nlid});
		if (dataList != null && dataList.size() > 0) {
			
			for (Map<String, Object> entry : dataList) {
				log.debug(entry.get("JOB_EXECUTION_ID") + " " + entry.get("JOB_INSTANCE_ID") + " " + entry.get("EXIT_CODE"));
			}
		}
		return instanceId;
	}
	
	private long getInstanceId(SimpleJobOperator jobOperator) {
		long instanceId = 0;
		
		return instanceId;
	}
	
	
	public void addCustomizeParams(String nlid, long dailyOrWeekly, long mornOrEven, int pageNo) {
		try {
			String query = "insert into CUSTOM_PARAMS(NL_ID, DL_WK, MR_EV, PG_NO, createdAt) values(?, ?, ?, ?, ?)";
			jdbcTemplate.update(query, new Object[] {nlid, dailyOrWeekly, mornOrEven, pageNo, new Date()});
		} catch (Exception e) {
			log.error("Exception caught while addCustomizeParams , msg : " + e.getMessage());
		}
		
		
	}
	
	public void updateCustomizeParams(String nlid, long dailyOrWeekly, long mornOrEven, int pageNo , Date prevDate) {
		//String curDate = null;
		try {
			//String query = "UPDATE CUSTOM_PARAMS SET PG_NO=1 Where NL_ID = 1001 and DL_WK = 1 and  MR_EV = 1 and createdAt < '2013-09-03 00:00:00' and createdAt > '2013-08-03 00:00:00'";
			String query = "UPDATE CUSTOM_PARAMS SET PG_NO= ? Where NL_ID = ? and DL_WK = ? and  MR_EV = ? and createdAt > ?";
			
			int recCount = jdbcTemplate.update(query, new Object[] {pageNo, nlid, dailyOrWeekly, mornOrEven, prevDate});
			log.debug("udpated : " + recCount);
		} catch (Exception e) {
			log.error("Exception caught while updateCustomizeParams , msg : " + e.getMessage());
		}
		
		
	}
	
	public void addOrupdatePageNo(String nlid, long dailyOrWeekly, long mornOrEven, int pageNo) {
		try {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, PREV_HR);
			//String prevDate = DateUtils.getformatedDate(null, cal.getTime());
			
			boolean status = isExistWithCustomizeParams(nlid, dailyOrWeekly,  mornOrEven, pageNo, cal.getTime());
			if (status) {
				updateCustomizeParams(nlid, dailyOrWeekly, mornOrEven, pageNo, cal.getTime());
			} else {
				addCustomizeParams(nlid, dailyOrWeekly, mornOrEven, pageNo);
			}
		} catch (Exception e) {
			log.error("Exception caught while addOrupdatePageNo , msg : " + e.getMessage());
		}
		
	}
	
	public int getPageNo(String nlid, long dailyOrWeekly, long mornOrEven) {
		int pageNo = 0;
		try {
			String query = "select PG_NO from CUSTOM_PARAMS where  NL_ID = ? and DL_WK = ? and  MR_EV = ? and createdAt > ?";
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.HOUR, PREV_HR);
			//String prevDate = DateUtils.getformatedDate(null, cal.getTime());
			
			pageNo = jdbcTemplate.queryForInt(query, new Object[] {nlid, dailyOrWeekly, mornOrEven, cal.getTime()});
			/*if (dataList != null && dataList.size() > 0) {
				Map<String, Object> map = (Map<String, Object>)dataList.get(0);
				pageNo = Integer.parseInt((String)map.get("PG_NO"));
			}*/
		} catch (Exception e) {
			log.error("Exception caught while getPageNo , msg : " + e.getMessage());
		}
		return pageNo;
	}
	
	public boolean isExistWithCustomizeParams(String nlid, long dailyOrWeekly, long mornOrEven, int pageNo, Date prevDate) {
		boolean status = false;
		try {
			//select count(*) as count from CUSTOM_PARAMS where  NL_ID = 1001 and DL_WK = 2 and  MR_EV = 2 and createdAt > '2013-09-03 11:18:37'
			String query = "select count(*) as count from CUSTOM_PARAMS where  NL_ID = ? and DL_WK = ? and  MR_EV = ? and createdAt > ?";
			/*Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.HOUR, PREV_HR);*/
			int count = jdbcTemplate.queryForInt(query, new Object[] {nlid, dailyOrWeekly, mornOrEven, prevDate});
			if (count != 0) {
				status = true;
			}
		} catch (Exception e) {
			log.error("Exception caught while isExistWithCustomizeParams , msg : " + e.getMessage());
		}
		return status;
	}
	
	public Map<String, Object> getCustomizeParams(String nlid, long dailyOrWeekly, long mornOrEven, int pageNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String query = "select id, NL_ID, DL_WK, MR_EV, PG_NO, createdAt from Custom_PARAMS WHERE NL_ID = ? and createdAt >= ? and DL_WK=? and MR_EV=?";
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, PREV_HR);
			String prevDate = DateUtils.getformatedDate(null, cal.getTime());
			List<Map<String, Object>> dataList = jdbcTemplate.queryForList(query, new Object[] {nlid, prevDate, dailyOrWeekly, mornOrEven});
			
			if (dataList != null && dataList.size() > 0) {
				for (Map<String, Object> entry : dataList) {
					log.debug(entry.get("id") + " " + entry.get("NL_ID") + " " + entry.get("DL_WK")+ " " + entry.get("MR_EV")+ " " + entry.get("PG_NO") + " " + entry.get("createdAt"));
				}
			}
		} catch (Exception e) {
			log.error("Exception caught while getCustomizeParams , msg : " + e.getMessage());
		}
		
		
		return map;
	}

	
	public Long getLastHrFailedJobInstanceId() {
		Long jobInstanceId = null;
		try {
			String query = "select ex.JOB_EXECUTION_ID, ex.JOB_INSTANCE_ID, ex.STATUS, ex.EXIT_CODE, ex.START_TIME, ex.END_TIME, ex.CREATE_TIME  from BATCH_JOB_EXECUTION ex , BATCH_JOB_PARAMS pm where exit_code ='FAILED' and ex.JOB_INSTANCE_ID = pm.JOB_INSTANCE_ID and pm.KEY_NAME ='nlid' and ex.CREATE_TIME > ?";
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, PREV_HR);
			List<Map<String, Object>> dataList = jdbcTemplate.queryForList(query, new Object[] {cal.getTime()});
			
			if (dataList != null && dataList.size() > 0) {
				for (Map<String, Object> entry : dataList) {
					log.debug(entry.get("id") + " " + entry.get("NL_ID") + " " + entry.get("DL_WK")+ " " + entry.get("MR_EV")+ " " + entry.get("PG_NO") + " " + entry.get("createdAt"));
					if(entry.get("JOB_INSTANCE_ID") != null) {
						Long instanceId =  (Long)entry.get("JOB_INSTANCE_ID");
						jobInstanceId = instanceId;
						break;
					}
				}
			}
			
		} catch (Exception e) {
			log.error("Exception caught while getLastHrFailedJobInstanceId , msg : " + e.getMessage());
		}
		return jobInstanceId;
	}
	
	public boolean isFailedJob(String nlid, long dailyOrWeekly, long mornOrEven, Long jobInstanceId) {
		boolean isNlIdmatched = false;
		boolean isDailyOrWeekly = false;
		boolean isMornOrEven = false;
		long dorW = 0;
		long morEve = 0;
		boolean status = false;
		//TODO: for testing.
		/*jobInstanceId = new Long(112);
		nlid = "1002";*/
		try {
			String query = "select TYPE_CD, KEY_NAME, STRING_VAL, DATE_VAL, LONG_VAL, DOUBLE_VAL from BATCH_JOB_PARAMS where job_instance_id = ?"; // 112
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR, PREV_HR);
			List<Map<String, Object>> dataList = jdbcTemplate.queryForList(query, new Object[] {jobInstanceId});
			
			if (dataList != null && dataList.size() > 0) {
				for (Map<String, Object> entry : dataList) {
					log.debug(entry.get("id") + " " + entry.get("KEY_NAME") + " " + entry.get("STRING_VAL")+ " " + entry.get("LONG_VAL"));
					
					if ("nlid".equals((String)entry.get("KEY_NAME")) && nlid.equals((String)entry.get("STRING_VAL"))){
						isNlIdmatched = true;
					}
					 
					if ("nl.dailyOrWeekly".equals((String)entry.get("KEY_NAME"))){
						dorW = (Long)entry.get("LONG_VAL");
						if (dorW == dailyOrWeekly) {
							isDailyOrWeekly = true;
						}
						
					}
					if ("nl.morningOrEve".equals((String)entry.get("KEY_NAME")) ){
						morEve = (Long)entry.get("LONG_VAL");
						if (morEve == mornOrEven) {
							isMornOrEven = true;
						}
					}
				}
			}
			
			if (isNlIdmatched && isDailyOrWeekly && isMornOrEven) {
				status = true;
			}
			
		} catch (Exception e) {
			log.error("Exception caught while getLastHrFailedJobInstanceId , msg : " + e.getMessage());
		}
		return status;
	}
	
	
	
	public Date getLastNewsAlertExecutionData(String hostid, String metatype) {
		Date prevDate = null;
		String query = "SELECT LAST_CHECKED_STAMP FROM NEWSALERT_BATCH_EXECUTION WHERE HOST_ID=? AND META_TYPE=?";		
		List<Map<String, Object>> dataList = jdbcTemplate.queryForList(query, new Object[] {hostid,metatype});
		if (dataList != null && dataList.size() > 0) {

			for (Map<String, Object> entry : dataList) {
				prevDate = (Date) entry.get("LAST_CHECKED_STAMP");
				break;
			}
		}
		return prevDate;
	}
	
	
	public void insertLastNewsAlertExecution(String hostid, String metatype, String metaDesc) {
		try {
			String query = "INSERT INTO NEWSALERT_BATCH_EXECUTION(HOST_ID, META_TYPE, META_DESC, LAST_EXECUTION_STAMP, LAST_CHECKED_STAMP) VALUES(?, ?, ?, ?, ?)";
			jdbcTemplate.update(query, new Object[] {hostid, metatype, metaDesc, new Date(), new Date()});
		} catch (Exception e) {
			log.error("Exception caught while insertLastNewsAlertExecution , msg : " + e.getMessage());
		}
	}
	
	
	public void updateLastExecutionData(String hostid, String metatype) {
		try {
			String query = "UPDATE NEWSALERT_BATCH_EXECUTION SET LAST_EXECUTION_STAMP=? , LAST_CHECKED_STAMP=? WHERE HOST_ID=? AND META_TYPE=?";
			jdbcTemplate.update(query, new Object[] {new Date(), new Date(), hostid, metatype});
		} catch (Exception e) {
			log.error("Exception caught while updateLastExecution , msg : " + e.getMessage());
		}
	}
	
	
	public void updateLastCheckedData(String hostid, String metatype) {
		try {
			String query = "UPDATE NEWSALERT_BATCH_EXECUTION SET LAST_CHECKED_STAMP=? WHERE HOST_ID=? AND META_TYPE=?";
			jdbcTemplate.update(query, new Object[] {new Date(), hostid, metatype});
		} catch (Exception e) {
			log.error("Exception caught while updateLastCheckedData , msg : " + e.getMessage());
		}
	}
	
	
	public String getMostReadMsid(String fetchDate) {
		String mostReadMsids = null;
		String query = "SELECT MSID FROM TRAVEL_NL_BATCH_DATA WHERE DATA_FETCH_DATE_TIME >=?";		
		List<Map<String, Object>> dataList = jdbcTemplate.queryForList(query, new Object[] {fetchDate});
		if (dataList != null && dataList.size() > 0) {

			for (Map<String, Object> entry : dataList) {
				mostReadMsids = (String) entry.get("MSID");
				break;
			}
		}
		return mostReadMsids;
	}
	
	
	public void insertMostReadMsid(String msids) {
		try {
			String query = "INSERT INTO TRAVEL_NL_BATCH_DATA(MSID, DATA_FETCH_DATE_TIME) VALUES( ?, ?)";
			int i = jdbcTemplate.update(query, new Object[] {msids, new Date()});
		} catch (Exception e) {
			log.error("Exception caught while insertMostReadMsid , msg : " + e.getMessage());
		}
	}
	
	
	@Override
	public int getBNewsId(String hostid) {
		String query = "SELECT BNEWS_ID FROM BREAKING_NEWS_BATCH_EXECUTION WHERE HOST_ID =?";		
		int data = jdbcTemplate.queryForInt(query, new Object[] {hostid});
		if (data != 0) {			
			return data;
		}
		return 0;
	}
	
	
	@Override
	public void insertBNewsId(String hostid, String bNewsId) {
		try {
			String query = "INSERT INTO BREAKING_NEWS_BATCH_EXECUTION(HOST_ID, BNEWS_ID, LAST_EXECUTION_STAMP) VALUES(?, ?, ?)";
			jdbcTemplate.update(query, new Object[] {hostid, bNewsId,new Date()});
		} catch (Exception e) {
			log.error("Exception caught while insertBNewsId , msg : " + e.getMessage());
		}
	}
	
	

	@Override
	public void updateBNewsId(String hostid, int bNewsId) {
		try {
			String query = "UPDATE BREAKING_NEWS_BATCH_EXECUTION SET BNEWS_ID=? , LAST_EXECUTION_STAMP = ? WHERE HOST_ID=? ";
			jdbcTemplate.update(query, new Object[] {bNewsId, new Date(), hostid});
		} catch (Exception e) {
			log.error("Exception caught while updateBNewsId , msg : " + e.getMessage());
		}
	}
	
}
