package com.times.mailer.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.times.mailer.model.ImageInfo;

public class ImageOptimizerDaoImpl implements ImageOptimizerDao{
	
	private static final Logger logger = LoggerFactory.getLogger(ImageOptimizerDaoImpl.class);
	
	
	public static SimpleDateFormat TIME_FORMAT_DB = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private SimpleJdbcTemplate jdbcTemplate;
	

	public ImageOptimizerDaoImpl(DataSource ds) {
		this.jdbcTemplate = new SimpleJdbcTemplate(ds);
	}
	

	@Override
	public int updateImageData(ImageInfo imageInfo) {
		
		Object [] obj = new Object[5];
		obj[0] = imageInfo.getOptimizedData();
		obj[1] = imageInfo.getMimeType();
		obj[2] = imageInfo.getId();
		obj[3] = imageInfo.getOmno();
		obj[4] = imageInfo.getUmd();
		
		String sql = "update UserResults  set URImageOptimised= ? , URImageMimeType= ?  where surveyid= ? and omno= ? and umd = ?";
		
		logger.info("Updating image data: "+ imageInfo.getId());
		
		int result =  this.jdbcTemplate.update(sql,obj);
		
		// Update status in case of success
		if(result == 1){
			
			Object [] params = new Object[2];
			params[0] = imageInfo.getId();
			params[1] = imageInfo.getUmd();
			
			String statusSql = "update UserResults  set isoptimised=1  where surveyid= ? and umd = ?";
			
			logger.info("Updating status after updating data: "+imageInfo.getId());
			result = this.jdbcTemplate.update(statusSql,params);
		}
		
		return result;
	}


	@Override
	public List<ImageInfo> getImageInfoList(String id, String omno) {

		String sql = "select SurveyID as id , OMNo as omn, UREntryDateTime as enterTime, URImage, URImageOptimised, URImageMimeType, urtext, umd "
						+ " from UserResults with (nolock) where isoptimised=0 and SurveyID = :surveyId and omno = :omno ";
		
		RowMapper<ImageInfo> rowMapper = new RowMapper<ImageInfo>() {
			public ImageInfo mapRow(ResultSet rs, int i) throws SQLException {
				ImageInfo imageInfo = new ImageInfo();
				
				imageInfo.setId(rs.getInt("id"));
				imageInfo.setOmno(rs.getString("omn"));
				
				Date creationDate = new Date();
				
				if(rs.getString("enterTime") != null){
					
					try {
						creationDate = TIME_FORMAT_DB.parse(rs.getString("enterTime"));
					} catch (ParseException e) {
						logger.error("Error while parsing date: "+rs.getString("enterTime") );
					}
					
				}
				imageInfo.setInsertTime(creationDate);
				imageInfo.setOriginalData(rs.getBytes("URImage"));
				imageInfo.setOptimizedData(rs.getBytes("URImageOptimised"));
				imageInfo.setMimeType(rs.getString("URImageMimeType"));
				imageInfo.setName(rs.getString("urtext"));
				imageInfo.setUmd(rs.getString("umd"));
				return imageInfo;
			}
		};
		
		Map<String , Integer> params = new HashMap<String , Integer>();
		params.put("surveyId", Integer.parseInt(id));
		params.put("omno", Integer.parseInt(omno));
		
		List<ImageInfo> list = this.jdbcTemplate.query(sql,rowMapper, params);
		
		logger.warn("Fecthing top images: "+ list.size());
		
		return (list!=null && list.size() > 0) ? list : new ArrayList<ImageInfo>();
	}
	
}
