package com.times.mailer.dao;

import java.util.List;
import java.util.Map;

public interface TechGadgetProductDAO {

	public String sqlGetOldGadgets ="select mb.MSID as msid, RTRIM(g.brand) as brand, RTRIM(g.Label) as label, RTRIM(g.Model) as model from MetaData mb, MetaData mm, Hierarchy h, tempSolrGadget g where "
			+ "mm.OMDnText = g.Model and mb.OMDnText =g.brand and mb.MSID=mm.MSID and mm.MetaType=190 and mb.MetaType=189 and mm.MSID=h.MSID and mb.MSID=h.MSID "
			+ "and h.HostID=83 and h.mas_msid = 20082925 order by brand, label, msid";
	public List<Map<String, Object>> getOldGadgetsList();
	
	
	public String sqlGetBrandFoldersList = "select m.msid, m.msname from dbo.Hierarchy h inner join masters m on m.msid = h.msid "
			+ "where MAS_MSID = :msid and h.HostID=83 and m.mstype=1";
	
	public List<Map<String, Object>> getBrandFoldersList(int categoryMsid);
	
	public String sqlSlideShowChildren = "select msid from dbo.Hierarchy where mas_msid =:msid";
	public int[] getSlideShowChildren(int slideShowId);
}
