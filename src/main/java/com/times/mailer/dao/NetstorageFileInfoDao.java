/**
 * 
 */
package com.times.mailer.dao;

import java.util.Date;
import java.util.List;

import com.times.mailer.model.FileLocationInfo;

/**
 * This Data Access Object interface is used for fileUpload activies to connect my sql .
 *  
 * @author Ranjeet.Jha
 *
 */
public interface NetstorageFileInfoDao {

	
	/**
	 * This method is used to pull the Source and destination file.
	 * 
	 * @param workType
	 * @param offset
	 * @param length
	 * @return
	 */
	public List<FileLocationInfo> getFileLocationInfo(int workType, int offset, int length);
	
	/**
	 * This method is used to pull the Source and destination file.
	 * 
	 * @param hosid
	 * @param workType
	 * @param offset
	 * @param length
	 * @return
	 */
	public List<FileLocationInfo> getFileLocationInfo(int hosid, int workType, int offset, int length);
	
	/**
	 * This method is used to update the uploaded last timestamp.
	 * 
	 * @param fileLocationInfo
	 */
	public void updateFileLocationInfo(FileLocationInfo fileLocationInfo);
	
	/**
	 * This method is used to get match id list.
	 * 
	 * @param fileLocationInfo
	 */
	public List<Integer> getActiveMatchIdList();
	
	/**
	 *  Used to get lastExecution time of particular url
	 * @param url
	 * @return
	 */
	public Date getFileUploadAuditExecutionDate(String url);
	
	/**
	 * @param msid
	 * @param url
	 */
	public void insertFileUploadAuditEntry(String msid, String url);
	
	/**
	 * @param url
	 */
	public void updateFileUploadAuditExecution(String url);
	
	/**
	 * @param workType
	 * @return
	 */
	public List<FileLocationInfo> getFileLocationInfoByWorkType(int workType) ;
	
	
}
