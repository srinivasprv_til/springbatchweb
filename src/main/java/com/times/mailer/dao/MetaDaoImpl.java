/**
 * 
 */
package com.times.mailer.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

/**
 * This class provides apis to manipulate with meta data.
 *
 * @author Rajeev Khatri
 * 
 *
 */
public class MetaDaoImpl implements MetaDao {

	private static final Logger log = LoggerFactory.getLogger(MetaDaoImpl.class);

	private SimpleJdbcTemplate jdbcTemplate;	

	public static final String query = "select m.MSID as msid from metaData m (nolock), hierarchy h (nolock) where h.hostid=? and m.metaType=? and m.msid = h.msid and m.OMDDateTime>?";

	public MetaDaoImpl(DataSource ds) {
		this.jdbcTemplate = new SimpleJdbcTemplate(ds);
	}

	public Collection getMetaData(String hostid, String metatype, Date lastCheckedDate) {
		String sql = query;
		List<Map<String, Object>> dataList = jdbcTemplate.queryForList(query, new Object[] {hostid, metatype, lastCheckedDate});
		return dataList;
	}
}
