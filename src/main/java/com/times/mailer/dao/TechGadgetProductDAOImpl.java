package com.times.mailer.dao;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class TechGadgetProductDAOImpl implements TechGadgetProductDAO {

	private final static Logger logger = LoggerFactory.getLogger(TechGadgetProductDAOImpl.class);
	private NamedParameterJdbcTemplate jdbcTemplate;
	DataSource ds;
	
	
	public NamedParameterJdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}


	public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	public TechGadgetProductDAOImpl(DataSource ds) {
		super();
		this.ds = ds;
		if (this.jdbcTemplate==null)
			this.jdbcTemplate = new NamedParameterJdbcTemplate(ds);
	}


	public DataSource getDs() {
		return ds;
	}


	public void setDs(DataSource ds) {
		this.ds = ds;
	}
	
	public List<Map<String, Object>> getOldGadgetsList() {
		try {
			SqlParameterSource namedParameters =  new MapSqlParameterSource();
			String sql= sqlGetOldGadgets;
			return getJdbcTemplate().queryForList(sql, namedParameters);
		} catch(Exception e) {
			logger.error("Error while fetching old gadgets", e);
		}
		
		return null;
	}
	
	
	public List<Map<String, Object>> getBrandFoldersList(int categoryMsid) {
		try {
			SqlParameterSource namedParameters =  new MapSqlParameterSource("msid", categoryMsid);
			String sql= sqlGetBrandFoldersList;
			return getJdbcTemplate().queryForList(sql, namedParameters);
		} catch(Exception e) {
			logger.error("Error while fetching BrandFoldersList", e);
		}
		
		return null;
	}
	
	public int[] getSlideShowChildren(int msid) {
		
		int[] children = null;
		try {
			SqlParameterSource namedParameters =  new MapSqlParameterSource("msid", msid);
			String sql= sqlSlideShowChildren;
			List<Map<String, Object>> params =  getJdbcTemplate().queryForList(sql, namedParameters);
			
			if(params != null && params.size() >0){
				children = new int[params.size()];
				int count=0;
				for(Map<String, Object> map: params){
					children[count++] = ((int) map.get("msid"));
				}
			}
		} catch(Exception e) {
			logger.error("Error while fetching BrandFoldersList", e);
		}
		
		return children;
	}
}
