package com.times.mailer.dao;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

public class AmazonParamDaoImpl implements AmazonParamDao{
	
	private static final Logger log = LoggerFactory.getLogger(AmazonParamDaoImpl.class);
	
	private SimpleJdbcTemplate jdbcTemplate;

	public AmazonParamDaoImpl(DataSource ds) {
		this.jdbcTemplate = new SimpleJdbcTemplate(ds);
	}

	@Override
	public Set<String> getProductIdsList(int metaType) {
		
		String sql = "select  OMDnText from metadata where metatype = :metaType";
		
		// Params map for query
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("metaType", metaType);
		
		List<Map<String, Object>> idsList = this.jdbcTemplate.queryForList(sql, params);
		
		Set<String> idsSet = new HashSet<String>();
		
		if(idsList != null && idsList.size() > 0){
			
			for(Map<String, Object> map: idsList){
				
				idsSet.add((String) map.get("OMDnText"));
			}
		}
		
		log.warn("Size of list for amazon product Url is:  "+ idsSet.size());
		
		return idsSet;
	}

}
