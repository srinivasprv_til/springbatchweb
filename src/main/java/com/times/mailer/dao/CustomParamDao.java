package com.times.mailer.dao;

import java.util.Date;
import java.util.Map;

/**
 * @author Ranjeet.Jha
 *
 */
public interface CustomParamDao {

	//public static void isJobFailed(SimpleJobOperator jobOperator, String nlid, String dailyWeekly, String mornEvening) ;
	
	public long getInstanceId(String nlid, long dailyOrWeekly, long mornOrEven) ;
	
	public void addCustomizeParams(String nlid, long dailyOrWeekly, long mornOrEven, int pageNo);
	
	public void updateCustomizeParams(String nlid, long dailyOrWeekly, long mornOrEven, int pageNo, Date startDate);
	
	public void addOrupdatePageNo(String nlid, long dailyOrWeekly, long mornOrEven, int pageNo) ;
	
	public boolean isExistWithCustomizeParams(String nlid, long dailyOrWeekly, long mornOrEven, int pageNo, Date startDate) ;
	
	public Map<String, Object> getCustomizeParams(String nlid, long dailyOrWeekly, long mornOrEven, int pageNo) ;
	
	public int getPageNo(String nlid, long dailyOrWeekly, long mornOrEven);
	
	public Long getLastHrFailedJobInstanceId();
	
	public boolean isFailedJob(String nlid, long dailyOrWeekly, long mornOrEven, Long jobInstanceId);
	
	
	public Date getLastNewsAlertExecutionData(String hostid, String metatype);
	
	public void insertLastNewsAlertExecution(String hostid, String metatype, String metaDesc);
	
	public void updateLastExecutionData(String hostid, String metatype);
	
	public void updateLastCheckedData(String hostid, String metatype);	
	
	public String getMostReadMsid(String fetchDate);
	
	public void insertMostReadMsid(String msids);
	
	public int getBNewsId(String hostid);
	
	public void insertBNewsId(String hostid, String bNewsId);
	
	public void updateBNewsId(String hostid, int bNewsId);
	
	
}
