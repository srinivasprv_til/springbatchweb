package com.times.mailer.dao;

import com.times.nbt.model.Newsletter;

public interface SQLMailerDAO {
	public int SendMail(Newsletter ltr);
}
