package com.times.mailer.dao;

import java.util.Collection;
import java.util.Date;

/**
 * @author Rajeev Khatri
 *
 */
public interface MetaDao {

	/**
	 * This class is used to fetch msid for articles best om meta type and hostid.
	 * @param hostid
	 * @param metatype
	 * @param lastCheckedDate
	 * @return collection of msid.
	 */
	public Collection getMetaData(String hostid, String metatype, Date lastCheckedDate);
}
