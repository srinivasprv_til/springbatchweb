package com.times.mailer.dao;

import java.io.IOException;
import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
import javax.swing.tree.RowMapper;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.times.mailer.model.LangUserMailer;
import com.times.nbt.model.UserInfo;
/***
 * the Dao will be used for the comment functionality on language sites.
 * @author manish.chandra
 *
 */

public class LangCommentsDAOImpl implements LangCommentDAO{

	private final static Logger logger = LoggerFactory.getLogger(LangCommentDAO.class);
	private NamedParameterJdbcTemplate jdbcTemplate;
	DataSource ds;
	
	
	public NamedParameterJdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}


	public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	public LangCommentsDAOImpl(DataSource ds) {
		super();
		this.ds = ds;
		if (this.jdbcTemplate==null)
			this.jdbcTemplate = new NamedParameterJdbcTemplate(ds);
	}


	public DataSource getDs() {
		return ds;
	}


	public void setDs(DataSource ds) {
		this.ds = ds;
	}


	
	public static String sqlGetMailerComments ="select top 50 MSID,eroid, hostid, isnull(eronmessage,eromessage) as EROMessage,EROAltDetails, altemailid, erouid from "
			+" editedreadersopinion(nolock) where  IsEmailed=2  and EROTime >= dateadd(hh,-1, getdate()) order by eroid desc";
	
	//for testing 
	/*
	public static String sqlGetMailerComments ="select top 50 MSID,eroid, hostid, isnull(eronmessage,eromessage) as EROMessage,EROAltDetails, altemailid, erouid from "
			+" editedreadersopinion where msid=31788689  order by eroid desc";
	
	*/
	public static String sqlCommentOn ="select MSID,eroid,hostid, isnull(eronmessage,eromessage) as EROMessage,EROAltDetails, altemailid, erouid  from editedreadersopinion(nolock) where eroid=:eroid";
	public static String sCMSDetails =" select msname, mstype, isnull(mssubtype,'')as mssubtype, isnull(omdntext,'')as metadata from masters m (nolock) , metadata md(nolock) where m.msid*=md.msid and  m.msid=:msid and md.metatype=124";
	public static String supdateCommentMailStats ="update editedreadersopinion set isemailed=1 where  eroid=:eroid";
	
	/**
	 * @param mapContent
	 * @param xmlData
	 * method will convert xml data to map
	 */
	private void getMapData(Map<String,String> mapContent, String xmlData) {
		if ((xmlData!=null) &&(!"null".equals(xmlData))){
			SAXBuilder builder = new SAXBuilder(); 
				try {
				Document doc = builder.build(new StringReader(xmlData));
				for(int i=0; i< doc.getRootElement().getChildren().size();i++){
					Element elm =(Element) doc.getRootElement().getChildren().get(i);
					mapContent.put(elm.getName(), elm.getValue());
				}
			} catch(Exception ex){
				logger.error(ex.getMessage());
			}
			
		}
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see com.times.mailer.dao.LangCommentDAO#getUserComments()
	 * will be used to get all mailer comments
	 */
	@Override
	public List<LangUserMailer> getUserComments() {
		SqlParameterSource namedParameters =  new MapSqlParameterSource();
		String sql= sqlGetMailerComments;
		ParameterizedRowMapper<LangUserMailer> rowMapper = new ParameterizedRowMapper<LangUserMailer>() {
			public LangUserMailer mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				LangUserMailer user = new LangUserMailer();
				Map<String, String> mapContent = new HashMap<String, String>();
				String xmlData =String.valueOf(rs.getString("EROAltDetails"));
				user.setMsid(Integer.parseInt(rs.getString("msid")));
				user.setEroid(Integer.parseInt(rs.getString("eroid")));
				
				getMapData(mapContent, xmlData);
				mapContent.put("msg", String.valueOf(rs.getString("EROMessage")));
				mapContent.put("altemailid", String.valueOf(rs.getString("altemailid")));
				mapContent.put("erouid",String.valueOf(rs.getString("erouid")));
				mapContent.put("hostid", String.valueOf(rs.getInt("hostid")));
				mapContent.put("usrcommentid", String.valueOf(rs.getInt("eroid")));
				user.setParamMap(mapContent);
				return user;
			}			
		};
		return getJdbcTemplate().query(sql, namedParameters, rowMapper);
	}

	
	/*
	 * (non-Javadoc)
	 * @see com.times.mailer.dao.LangCommentDAO#getParentComments(int)
	 * will be used to get parent comment details
	 */
	@Override
	public List<LangUserMailer> getParentComments(int eroid) {
		// TODO Auto-generated method stub
		SqlParameterSource namedParameters =  new MapSqlParameterSource("eroid", eroid);
		String sql=sqlCommentOn;
		ParameterizedRowMapper<LangUserMailer> rowMapper = new ParameterizedRowMapper<LangUserMailer>() {
			public LangUserMailer mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				LangUserMailer user = new LangUserMailer();
				Map mapContent = new HashMap<String, String>();
				String xmlData =String.valueOf(rs.getString("EROAltDetails"));
				user.setMsid(Integer.parseInt(rs.getString("msid")));
				user.setEroid(Integer.parseInt(rs.getString("eroid")));
				getMapData(mapContent, xmlData);
				mapContent.put("msg", String.valueOf(rs.getString("EROMessage")));
				mapContent.put("altemailid", String.valueOf(rs.getString("altemailid")));
				mapContent.put("erouid",String.valueOf(rs.getString("erouid")));
				mapContent.put("hostid", String.valueOf(rs.getInt("hostid")));
				user.setParamMap(mapContent);
				return user;
			}			
		};
		return  getJdbcTemplate().query(sql, namedParameters, rowMapper);
		
	}


	/*
	 * (non-Javadoc)
	 * @see com.times.mailer.dao.LangCommentDAO#getCMSObject(int)
	 * will be used to get cms object details for mapping..
	 */
	@Override
	public Map<String, Object> getCMSObject(int msid) {
		// TODO Auto-generated method stub
		SqlParameterSource namedParameters = new MapSqlParameterSource("msid", msid);
		String sql = sCMSDetails;
		return getJdbcTemplate().queryForMap(sql, namedParameters);
	
	}

/*
 * (non-Javadoc)
 * @see com.times.mailer.dao.LangCommentDAO#UpdateMailerStats(int)
 */
	@Override
	public Boolean UpdateMailerStats(int eroid) {
		String sql = supdateCommentMailStats;
		SqlParameterSource nameParams = new MapSqlParameterSource("eroid", eroid);
		return ((getJdbcTemplate().update(sql, nameParams)>0) ? true:false);
	}


	

	
	
}
