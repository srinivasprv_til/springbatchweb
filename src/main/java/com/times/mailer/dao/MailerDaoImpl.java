package com.times.mailer.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.Constant;
import com.times.common.util.DateUtils;
import com.times.common.wrapper.NewsletterDBObjectWrapper;
import com.times.mailer.model.LangConfiguration;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetter;
import com.times.mailer.model.NewsLetterDailyTracker;
import com.times.mailer.model.SMSUser;
import com.times.mailer.model.Subscription;

/**
 * This DAO implementation class is used to interact to the MONGO DataBase for the specified collections name in this class.
 * 
 * @author manish.chandra
 * @author Ranjeet.Jha
 *
 */
public class MailerDaoImpl  implements MailerDao { 
	
	//private final static Logger logger = Logger.getLogger(MailerDaoImpl.class);
	private final static Logger logger = LoggerFactory.getLogger(MailerDaoImpl.class);
	
	public static final String NEWSLETTER_MASTER_COLLECTION = "newsletter";
	public static final String SUBSCRIBER_COLLECTION = "newslettersubscription";
	public static final String NEWSLETTER_STATS ="mailerstats";
	public static final String NEWSLETTER_DAILY_TRACKET_COLLECTION = "nldailytracker";
	
	public static final String COMMENT_CONFIGURATION_COLLECTION ="langMailerConfiguration";
	
	public static final String SMS_SUBSCRIBER_COLLECTION = "smsUsers";
	
	

	/* (non-Javadoc)
	 * @see com.times.newsletter.dao.MailerDao#getNewsletterScheduled()
	 */
	public List<Mailer> getNewsletterScheduled() {
		//Calendar cal = Calendar.getInstance();
		//cal.setTime(lastAccess);
		
		String[] hrMin = DateUtils.getHourAndMin(new Date()).split(":");
		String hrs = hrMin[0];
		String min = hrMin[1];
		int ihr = (int)Math.ceil(Double.parseDouble(hrs + "." + min));
		List<Mailer> newsletterList = new ArrayList<Mailer>();
		
		//code done for testing
		DBObject match = new BasicDBObject();
		//match.put(Mailer.STATUS, 2);
		//match.put(Mailer.ID, "1004");
		//match.put(Mailer.DAILY, 1);
		//match.put(Mailer.WEEKLY, 1);
		//match.put(Mailer.SCHEDULE_TIME1, 1);
		
		//DBCursor cursor = mailerColl.find(match);
		
		List<DBObject> dbObjectList = MongoRestAPIService.getList(NEWSLETTER_MASTER_COLLECTION, match);
		if (dbObjectList != null) {
			for (DBObject objDB : dbObjectList) {
				
				/*if (ihr < 12) {
					long diffMinutes = DateUtils.getMinuteDiff(new Date(), String.valueOf(objDB.get(Mailer.SCHEDULED_MORNING_TIMING)));
					if (!(diffMinutes <= MailerReader.TIME_INTERVAL && diffMinutes > 0)) {
						continue;
					}
				}else{
					long diffMinutes = DateUtils.getMinuteDiff(new Date(), String.valueOf(objDB.get(Mailer.SCHEDULED_EVENING_TIMING)));
					if (!(diffMinutes <= MailerReader.TIME_INTERVAL && diffMinutes > 0)) {
						continue;
					}
				}*/
				
				if ("1.0".equals(String.valueOf(objDB.get(Mailer.DAILY))) || "1".equals(String.valueOf(objDB.get(Mailer.DAILY)))) {
					Mailer newsletterMaster = NewsletterDBObjectWrapper.getMailerFromDBObject(objDB);
					newsletterList.add(newsletterMaster);
					if (newsletterMaster != null) {
						logger.debug("nl id : " + newsletterMaster.getId() + " added for daily to proceed further...");
					}
				}else {
					Mailer newsletterMaster = NewsletterDBObjectWrapper.getMailerFromDBObject(objDB);
					newsletterList.add(newsletterMaster);
					if (newsletterMaster != null) {
						logger.debug("nl id : " + newsletterMaster.getId() + " added for weekly to proceed further ...");
					} 
				}
			}
		}
		return newsletterList;
	}
	
	/* (non-Javadoc)
	 * @see com.times.newsletter.dao.MailerDao#getSubscriber(com.times.newsletter.model.Mailer)
	 */
	public List<MailUser> getSubscriber(Mailer mailer, int perPage, int pageNo) {
		List<MailUser> subscribers = new ArrayList<MailUser>();
		int iDay = 0;
		try {

			String newsid = mailer.getId();
			boolean isDaily = mailer.isDaily();
			boolean isWeekly = mailer.isWeekly();
			int iperiod = mailer.getScheduleTime();
			BasicDBObject filter = new BasicDBObject();
			
			if (Constant.INDIVISUAL_NEWSLETTER_VERIFICATION.contains(newsid)) {
				BasicDBObject query = new BasicDBObject();
				query.put("ID", newsid.trim());

				if (isWeekly) {				
					query.append("WL", true);

					Calendar cal = Calendar.getInstance();
					cal.setTime(new Date());
					iDay = cal.get(Calendar.DAY_OF_WEEK);
					
					if (!Constant.NLID_WITHOUT_WEEKDAY_CONSIDERATION.contains(mailer.getId())) {
						query.append("WL_D", iDay);
					}

				} else if (isDaily) {
					query.append("DL", true);
				}

				query.append("V_NL", true);
				query.append("ST", 1);
				query.append("SCH", iperiod);


				DBObject finalQuery = new BasicDBObject("$elemMatch", query);				
				filter.put("NL_MAP", finalQuery);
				//filter.put("ST", "1"); //

			} else {
				BasicDBObject filter1 = new BasicDBObject();
				filter1.put("ID", newsid.trim());
				

				if (isWeekly) {
					filter1.append("WL", true);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new Date());

					iDay = cal.get(Calendar.DAY_OF_WEEK);
					// TODO: need to uncomment this in case of weekly.
					if (!Constant.NLID_WITHOUT_WEEKDAY_CONSIDERATION.contains(mailer.getId())) {
						filter1.append("WL_D", iDay);
					}


				} else if (isDaily) {
					filter1.append("DL", true);
				}
				filter1.append("ST", 1);
				// for toi breaking news
				if (!Constant.NLID_WITHOUT_SCHDULE_TIME.contains(newsid)) {
					filter1.append("SCH", iperiod);		
				}
				
				DBObject finalQuery = new BasicDBObject("$elemMatch", filter1);	
				BasicDBObject filter2 = new BasicDBObject();
				filter2.put("NL_MAP", finalQuery);
				
				BasicDBObject filter3 = new BasicDBObject();
				filter3.append("ST", "1"); 
				
				BasicDBObject filter4 = new BasicDBObject();
				filter4.append("V_USR", true);
				
				BasicDBList and = new BasicDBList();
				and.add(filter2);
				and.add(filter3);
				and.add(filter4);
				
				filter = new BasicDBObject("$and", and);
				
			}
			
			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", String.valueOf((pageNo - 1) * perPage));
			
			if (Constant.NEWSLETTER_WITH_SORT_FETCH.contains(newsid)) {
				BasicDBObject orderBy = new BasicDBObject();
				orderBy.put("U_AT", 1);
				optionMap.put("sort", orderBy);
			}
			
			List<DBObject> dbObjects = MongoRestAPIService.getList(SUBSCRIBER_COLLECTION, filter, optionMap);
			if (dbObjects != null) {
				for (DBObject objDB : dbObjects) {
					if (objDB != null) {
						/*if (mailer.getId().equals("1003") && iDay != 0) {
							continue;
						}*/
						MailUser user = this.getMailUserFromDBObject(objDB, mailer);
						subscribers.add(user);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception caugh : " + e.getMessage());
		}
		return subscribers;
	}

	/**
	 * Converting from {@link DBObject} to {@link MailUser} object.
	 * 
	 * @param objDB
	 * @return
	 */
	private MailUser getMailUserFromDBObject(DBObject objDB, Mailer mailer) {
		String mailerID= mailer.getId();
		MailUser user = new MailUser();
		user.setEmailId(String.valueOf(objDB.get(Subscription.ALTERNATE_EMAIL_ID)));
		user.setSsoid(String.valueOf(objDB.get(Subscription.SSO_USER_ID)));
		user.setEmailSubject(mailer.getMailSubject());
		user.setEmailFrom(mailer.getMailerFrom());
		
		if (objDB != null && objDB.get("NL_MAP") != null) {
			List<DBObject> nlListMap = (List<DBObject>) objDB.get(Subscription.NEWS_LETTER_MAP);
			if (nlListMap != null) {
				for(DBObject db : nlListMap){
					//logger.debug(db.get(NewsLetter.SCHEDULETIME) + " : " + mailer.getScheduleTime());
					int schTime = ((Number)db.get(NewsLetter.SCHEDULETIME)).intValue();
					if(db.containsField(NewsLetter.ID) && mailerID.equals(String.valueOf(db.get(NewsLetter.ID))) && (schTime == mailer.getScheduleTime() 
							||	Constant.NLID_WITHOUT_SCHDULE_TIME.contains(mailerID))){
						user.setNewsletter(NewsletterDBObjectWrapper.getNewsLetterFromBson(db));
						
						// No schedule time i.e Toi breaking news 1016.
						if (Constant.NLID_WITHOUT_SCHDULE_TIME.contains(mailerID)) {
							user.getNewsletter().setScheduleTime(mailer.getScheduleTime());
						}
						break;
					}
				}
			}
		}
		return user;
	}
	
	/* (non-Javadoc)
	 * @see com.times.mailer.dao.MailerDao#getNewsLetterDailyTracker(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	public DBObject getNewsLetterDailyTracker(String newsLetterId, String date, String scheduledTime, String schedledFrequency) {
		DBObject dbObject = null;
		
		BasicDBObject query = new BasicDBObject();
		query.put(NewsLetterDailyTracker.NEWS_LETTER_ID, new Integer(newsLetterId));
		query.put(NewsLetterDailyTracker.DATE, date);
		query.put(NewsLetterDailyTracker.SCHEDULED_FREQUENCY, new Integer(schedledFrequency));
		query.put(NewsLetterDailyTracker.SCHEDULED_TIME, new Integer(scheduledTime));
		
		dbObject = MongoRestAPIService.get(NEWSLETTER_DAILY_TRACKET_COLLECTION, query);
		return dbObject;
	}
	
	/* (non-Javadoc)
	 * @see com.times.newsletter.dao.MailerDao#addNewsLetterDailyTracker(com.times.newsletter.model.NewsLetterDailyTracker)
	 */
	public void addNewsLetterDailyTracker(NewsLetterDailyTracker dailyTracker)  {
		
		DBObject dbObject = NewsletterDBObjectWrapper.getDBObjectByNewsLetterTracker(dailyTracker);
		try {
			
			MongoRestAPIService.insert(NEWSLETTER_DAILY_TRACKET_COLLECTION, dbObject);
		} catch (MongoException e) {
			logger.error("MongoException caught while adding newsLetterDailyTracking , msg : " + e.getMessage(), e);
			//throw e;
		} catch (Exception e) {
			logger.error("Generic Exception caught while adding newsLetterDailyTracking ", e);
		} finally {
		}
	
	}
	
	public void addMailerStats(String skey, int ireadCount, int imailCount) {
		try {
			DBObject dbObject = new BasicDBObject();
			dbObject.put("DT", new Date());
			dbObject.put("NL_KEY", skey);
			dbObject.put("USR", ireadCount);
			dbObject.put("MAIL", imailCount);
			
			MongoRestAPIService.insert(NEWSLETTER_STATS, dbObject);
			
		} catch (MongoException e) {
			logger.error("MongoException caught while adding newsLetterDailyTracking , msg : " + e.getMessage(), e);
		} catch (Exception e) {
			logger.error("Generic Exception caught while adding newsLetterDailyTracking ", e);
		} finally {
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.times.newsletter.dao.MailerDao#updateNewsLetterDailyTracker(java.lang.String, java.lang.String, int, int)
	 */
	public void updateNewsLetterDailyTracker(String code, String key, int userCount, int sentCount) {

		try {
			DBObject query = new BasicDBObject();
			//dbObject.put("DT", new Date());
			query.put(NewsLetterDailyTracker.NEWS_LETTER_CODE, code);
			
			DBObject nldTrackerObject = getNewsLetterDailyTracker(code, key, NEWSLETTER_DAILY_TRACKET_COLLECTION);
			if (nldTrackerObject != null) {
//				nldTrackerObject.put(NewsLetterDailyTracker.NEWS_LETTER_SENT_COUNTER, sentCount);
//				nldTrackerObject.put(NewsLetterDailyTracker.NEWS_LETTER_USERS_COUNT, userCount);
//				MongoRestAPIService.update(NEWSLETTER_DAILY_TRACKET_COLLECTION, query, nldTrackerObject, true);
				
				BasicDBObject datatoupdate = new BasicDBObject();
				datatoupdate.put(NewsLetterDailyTracker.NEWS_LETTER_SENT_COUNTER, sentCount);
				datatoupdate.put(NewsLetterDailyTracker.NEWS_LETTER_USERS_COUNT, userCount);
				
				BasicDBObject newDocument = new BasicDBObject();
				newDocument.append("$set", datatoupdate);
				MongoRestAPIService.update(NEWSLETTER_DAILY_TRACKET_COLLECTION, query, newDocument);
			}
			
		} catch (MongoException e) {
			logger.error("MongoException caught while updating newsLetterDailyTracking , msg : " + e.getMessage(), e);
		} catch (Exception e) {
			logger.error("Generic Exception caught while updating newsLetterDailyTracking ", e);
		} finally {
		}
	}
	
	/**
	 * This method is used to get the {@link DBObject} for <code>NewsLetterDailyTracker</code>.
	 *   
	 * @param code
	 * @param key
	 * @param nldTrackerColl
	 * @return
	 */
	private DBObject getNewsLetterDailyTracker(String code, String key, String collectionName) {
		DBObject dbObject = null;
		try {
			String[] keysArray = key.split("[" + Constant.KEY_BUILDER_SEPERATOR + "]");
			DBObject query = new BasicDBObject();
			query.put(NewsLetterDailyTracker.NEWS_LETTER_CODE, code);
			query.put(NewsLetterDailyTracker.NEWS_LETTER_ID, Integer.parseInt(keysArray[0].trim()));
			query.put(NewsLetterDailyTracker.SCHEDULED_FREQUENCY,Integer.parseInt( keysArray[1].trim()));
			query.put(NewsLetterDailyTracker.SCHEDULED_TIME, Integer.parseInt(keysArray[2].trim()));
			//query.put(NewsLetterDailyTracker.DATE, MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD));
			
			//dbObject = nldTrackerColl.findOne(query);
			dbObject = MongoRestAPIService.get(collectionName, query);
			
		} catch (Exception e) {
			logger.error("Generic Exception caught while adding newsLetterDailyTracking ", e);
		} 
		
		return dbObject;
	}

	/* (non-Javadoc)
	 * @see com.times.mailer.dao.MailerDao#getNewsletterMailer(com.mongodb.DBObject)
	 */
	public List<Mailer> getNewsletterMailer(String nlId, boolean isDaily) {
		List<Mailer> mailerList = null;
		try {
			DBObject query = new BasicDBObject();
			query.put(Mailer.ID, nlId);
			if (isDaily) {
				query.put(Mailer.DAILY, 1);
			} else {
				query.put(Mailer.WEEKLY, 1);
			}
			
			List<DBObject> dbObjectList = MongoRestAPIService.getList(NEWSLETTER_MASTER_COLLECTION, query);
			if (dbObjectList != null) {
				mailerList = new ArrayList<Mailer>();
				for (DBObject dbObject : dbObjectList) {
					Mailer nlMaster = NewsletterDBObjectWrapper.getMailerFromDBObject(dbObject);
					if (nlMaster != null) {
						mailerList.add(nlMaster);
					}
				}
				
			}
		} catch (Exception e) {
			logger.debug("getNewsletterMailer method , ex : " + e.getMessage());
		}
		
		return mailerList;
	}

	
	/* (non-Javadoc)
	 * @see com.times.mailer.dao.MailerDao#getNewsletterMailer(com.mongodb.DBObject)
	 */
	public List<DBObject> getUsersDBObjectBetweenDates(String nlId, boolean verifiedUser, Date startDate, Date endDate) {
		List<DBObject> dbObjectList = null;
		try {
			DBObject query = new BasicDBObject();
			//query.put(Mailer.ID, nlId);
			query.put(Subscription.NEWS_LETTER_MAP + "." + NewsLetter.ID, nlId);
			query.put(Subscription.NEWS_LETTER_MAP + "." + NewsLetter.VERIFIED_NEWSLETTER, verifiedUser);
			
			query.put("NL_MAP.U_AT", new BasicDBObject("$gt", startDate).append("$lte", endDate));
			
			BasicDBObject date = new BasicDBObject();
			date.append("$gte", new Date(startDate.getTime()));
			date.append("$lte", new Date(endDate.getTime()));
			
			DBObject query1 = BasicDBObjectBuilder.start()
					.add(Subscription.NEWS_LETTER_MAP + "." + NewsLetter.ID, nlId)
					.add(Subscription.NEWS_LETTER_MAP + "." + NewsLetter.VERIFIED_NEWSLETTER, verifiedUser)
					.add("NL_MAP.U_AT", date)
					.get();
			
			dbObjectList = MongoRestAPIService.getList(SUBSCRIBER_COLLECTION, query1);
			/*if (dbObjectList != null) {
				mailerList = new ArrayList<Mailer>();
				for (DBObject dbObject : dbObjectList) {
					Mailer nlMaster = NewsletterDBObjectWrapper.getMailerFromDBObject(dbObject);
					if (nlMaster != null) {
						mailerList.add(nlMaster);
					}
				}
				
			}*/
		} catch (Exception e) {
			logger.debug("getNewsletterMailer method , ex : " + e.getMessage());
		}
		
		return dbObjectList;
	}
	
	/* (non-Javadoc)
	 * @see com.times.mailer.dao.MailerDao#getNewsletterMailer(com.mongodb.DBObject)
	 */
	public List<DBObject> getNewsletterDBObjectById(String nlId) {
		List<DBObject> dbObjectList = null;
		try {
			DBObject query = new BasicDBObject();
			query.put(Mailer.ID, nlId);
			//query.put(, nlId);
			
			
			dbObjectList = MongoRestAPIService.getList(NEWSLETTER_MASTER_COLLECTION, query);
			/*if (dbObjectList != null) {
				mailerList = new ArrayList<Mailer>();
				for (DBObject dbObject : dbObjectList) {
					Mailer nlMaster = NewsletterDBObjectWrapper.getMailerFromDBObject(dbObject);
					if (nlMaster != null) {
						mailerList.add(nlMaster);
					}
				}
				
			}*/
		} catch (Exception e) {
			logger.debug("getNewsletterMailer method , ex : " + e.getMessage());
		}
		
		return dbObjectList;
	}

	@Override
	public List<LangConfiguration> getCommentsConfig() {
		// TODO Auto-generated method stub
		DBObject match = new BasicDBObject();
		List<LangConfiguration> lstConfig = new ArrayList<LangConfiguration>();
		List<DBObject> dbObjects = MongoRestAPIService.getList("langMailerConfiguration", match);
	
		if (dbObjects!=null){
			for (DBObject dbObject : dbObjects) {
				LangConfiguration config = new LangConfiguration();
				config.setHostid(String.valueOf(dbObject.get("GID")));
				config.setHttpUrl(String.valueOf(dbObject.get("EXT")));
				config.setChannelName(String.valueOf(dbObject.get("CNAME")));
				config.setFrmName(String.valueOf(dbObject.get("FRM")));
				config.setReply(String.valueOf(dbObject.get("RPLY")));
				config.setCustomization(String.valueOf(dbObject.get("PR")));
				if (dbObject.containsField("KEY_MAP")){
					Map<String, String> mapKey = new HashMap<String, String>();
					List<DBObject> nlMap = (List<DBObject>) dbObject.get("KEY_MAP");
					for (DBObject dbObject2 : nlMap) {
						mapKey.put(String.valueOf(dbObject2.get("KEY")), String.valueOf(dbObject2.get("EXT")));
					}
					config.setMapKeyMapping(mapKey);
				}
			lstConfig.add(config);
			}
		}
		return lstConfig;
	}
	
	
	public List<MailUser> getSubscriber(Mailer mailer, int perPage, int pageNo, String collection) {
		List<MailUser> subscribers = new ArrayList<MailUser>();
		int iDay = 0;
		try {

			String newsid = mailer.getId();
			boolean isDaily = mailer.isDaily();
			boolean isWeekly = mailer.isWeekly();
			int iperiod = mailer.getScheduleTime();
			BasicDBObject filter = new BasicDBObject();


			BasicDBObject query = new BasicDBObject();
			query.put("ID", newsid.trim());

			if (isWeekly) {				
				query.append("WL", true);

				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				iDay = cal.get(Calendar.DAY_OF_WEEK);

			} else if (isDaily) {
				query.append("DL", true);
			}

			query.append("V_NL", true);
			query.append("ST", 1);
			query.append("SCH", iperiod);


			DBObject finalQuery = new BasicDBObject("$elemMatch", query);				
			filter.put("NL_MAP", finalQuery);


			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", String.valueOf((pageNo - 1) * perPage));
			List<DBObject> dbObjects = MongoRestAPIService.getList(collection, filter, optionMap);
			if (dbObjects != null) {
				for (DBObject objDB : dbObjects) {
					if (objDB != null) {

						MailUser user = this.getMailUserFromDBObject(objDB, mailer);
						subscribers.add(user);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception caugh : " + e.getMessage());
		}
		return subscribers;
	}
	
	
	/* (non-Javadoc)
	 * @see com.times.mailer.dao.MailerDao#getSMSSubscriberDetails(com.times.mailer.model.Mailer, int, int)
	 * This API fetch all unprocessed entries in smsUser collection based on sms and email flag.
	 */
	public List<SMSUser> getSMSSubscriberDetails(Mailer mailer, int perPage, int pageNo) {
		List<SMSUser> subscribers = new ArrayList<SMSUser>();
		
		try {
			BasicDBObject filter1 = new BasicDBObject();
			filter1.append("S_Flg", false);
			
			BasicDBObject filter2 = new BasicDBObject();
			filter2.append("E_Flg", false);
			
			BasicDBList or = new BasicDBList();
			or.add(filter1);
			or.add(filter2);


			DBObject ORquery = new BasicDBObject("$or", or);
			
			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", String.valueOf((pageNo - 1) * perPage));
			List<DBObject> dbObjects = MongoRestAPIService.getList(SMS_SUBSCRIBER_COLLECTION, ORquery, optionMap);
			if (dbObjects != null) {
				for (DBObject objDB : dbObjects) {
					if (objDB != null) {
						SMSUser user = this.getSMSUserFromDBObject(objDB, mailer);
						subscribers.add(user);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception caught : " + e.getMessage());
		}
		return subscribers;
	}
	
	private SMSUser getSMSUserFromDBObject(DBObject objDB, Mailer mailer) {
		
		SMSUser user = new SMSUser();
		user.setId(String.valueOf(objDB.get(SMSUser.ID)));
		
		user.setUsers(String.valueOf(objDB.get(SMSUser.USERS)));
		user.setChannelName(String.valueOf(objDB.get(SMSUser.CHANNEL_NAME)));
		user.setProgramName(String.valueOf(objDB.get(SMSUser.PROGRAM_NAME)));
		user.setSms(String.valueOf(objDB.get(SMSUser.SMS)));
		user.setEmail(String.valueOf(objDB.get(SMSUser.EMAIL)));
		user.setSmsFlag((Boolean)objDB.get(SMSUser.SMS_FLAG));
		user.setEmailFlag((Boolean)objDB.get(SMSUser.EMAIL_FLAG));
		user.setTimeBefore(String.valueOf(objDB.get(SMSUser.TIME_BEFORE)));
		
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
				
		user.setDateTime(sdf.format(objDB.get(SMSUser.DATE_TIME)));
		return user;
	}
	
	
	/* (non-Javadoc)
	 * @see com.times.mailer.dao.MailerDao#updateSMSFLAGEntry(java.lang.String)
	 * This API used to mark processed entries in smsUser mongo collection.
	 * Actual SMS and Email are send latter in SMSMailerWriter.
	 */
	public void updateSMSFLAGEntry(String id) {

		try {
			DBObject query = new BasicDBObject();	
			query.put(SMSUser.ID, new ObjectId(id));
			
			BasicDBObject datatoupdate = new BasicDBObject();
			datatoupdate.put(SMSUser.SMS_FLAG, true);
			datatoupdate.put(SMSUser.EMAIL_FLAG, true);

			BasicDBObject newDocument = new BasicDBObject();
			newDocument.append("$set", datatoupdate);
			MongoRestAPIService.update(SMS_SUBSCRIBER_COLLECTION, query, newDocument);


		} catch (MongoException e) {
			logger.error("MongoException caught while updateSMSFLAGEntry , msg : " + e.getMessage(), e);
		} catch (Exception e) {
			logger.error("Generic Exception caught while updateSMSFLAGEntry ", e);
		} finally {
		}
	}
}
