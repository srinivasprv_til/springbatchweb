package com.times.mailer.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;


public class EpollDAOImpl implements EpollDao {

	private SimpleJdbcCall simpleJdbcCall;

	public EpollDAOImpl(DataSource ds) {
		this.simpleJdbcCall = new SimpleJdbcCall(ds);
	}

	/**
	 * @return the simpleJdbcCall
	 */
	public SimpleJdbcCall getSimpleJdbcCall() {
		return simpleJdbcCall;
	}

	@Override
	public void saveEpollData(int candidateId, int statusId) {
		// TODO Auto-generated method stub
		Map<String, Object> args = new HashMap<String, Object>(2);
		args.put("@epollid", candidateId);
		args.put("@statusid", statusId);
		args.put("@candidateidType", 0);
		simpleJdbcCall.setProcedureName("sp_updateConsitutencyByEpollid");
		Map<String, Object> m = simpleJdbcCall.execute(args);
	}


}
