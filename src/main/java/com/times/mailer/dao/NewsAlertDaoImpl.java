/**
 * 
 */
package com.times.mailer.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.util.Constant;
import com.times.common.util.DateUtils;
import com.times.common.wrapper.NewsletterDBObjectWrapper;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetter;
import com.times.mailer.model.NewsLetterDailyTracker;
import com.times.mailer.model.Subscription;



/**
 * This DAO implementation is used for News Alert.
 * 
 * @author Ranjeet.Jha
 *
 */
public class NewsAlertDaoImpl implements NewsAlertDao { 


	private final static Logger logger = LoggerFactory.getLogger(MailerDaoImpl.class);

	public static final String NEWSLETTER_MASTER_COLLECTION = "newsletter";

	public static final String SUBSCRIPTION_COLLECTION = "newsAlertSubscription";
	public static final String NEWSLETTER_STATS ="mailerstats";
	public static final String NEWSLETTER_DAILY_TRACKET_COLLECTION = "nldailytracker";
	public static final String ALERT_LOG_COLLECTION = "newsAlertLog";

	/* (non-Javadoc)
	 * @see com.times.newsletter.dao.MailerDao#getNewsletterScheduled()
	 */
	public List<Mailer> getNewsletterScheduled() {

		String[] hrMin = DateUtils.getHourAndMin(new Date()).split(":");
		String hrs = hrMin[0];
		String min = hrMin[1];
		int ihr = (int)Math.ceil(Double.parseDouble(hrs + "." + min));
		List<Mailer> newsletterList = new ArrayList<Mailer>();

		//code done for testing
		DBObject match = new BasicDBObject();

		List<DBObject> dbObjectList = MongoRestAPIService.getList(NEWSLETTER_MASTER_COLLECTION, match);
		if (dbObjectList != null) {
			for (DBObject objDB : dbObjectList) {

				/*if (ihr < 12) {
					long diffMinutes = DateUtils.getMinuteDiff(new Date(), String.valueOf(objDB.get(Mailer.SCHEDULED_MORNING_TIMING)));
					if (!(diffMinutes <= MailerReader.TIME_INTERVAL && diffMinutes > 0)) {
						continue;
					}
				}else{
					long diffMinutes = DateUtils.getMinuteDiff(new Date(), String.valueOf(objDB.get(Mailer.SCHEDULED_EVENING_TIMING)));
					if (!(diffMinutes <= MailerReader.TIME_INTERVAL && diffMinutes > 0)) {
						continue;
					}
				}*/

				if ("1.0".equals(String.valueOf(objDB.get(Mailer.DAILY))) || "1".equals(String.valueOf(objDB.get(Mailer.DAILY)))) {
					Mailer newsletterMaster = NewsletterDBObjectWrapper.getMailerFromDBObject(objDB);
					newsletterMaster.setScheduleTime(1);  // as run morning daily this is fixed
					newsletterList.add(newsletterMaster);
					if (newsletterMaster != null) {
						logger.debug("nl id : " + newsletterMaster.getId() + " added for daily to proceed further...");
					}
				}else {
					Mailer newsletterMaster = NewsletterDBObjectWrapper.getMailerFromDBObject(objDB);
					newsletterList.add(newsletterMaster);
					if (newsletterMaster != null) {
						logger.debug("nl id : " + newsletterMaster.getId() + " added for weekly to proceed further ...");
					} 
				}
			}
		}
		return newsletterList;
	}

	/* (non-Javadoc)
	 * @see com.times.newsletter.dao.MailerDao#getSubscriber(com.times.newsletter.model.Mailer)
	 */
	public List<MailUser> getSubscriber(Mailer mailer, int perPage, int pageNo) {
		List<MailUser> subscribers = new ArrayList<MailUser>();
		int iDay = 0;
		try {

			String newsid = mailer.getId();
			boolean isDaily = mailer.isDaily();
			boolean isWeekly = mailer.isWeekly();
			int iperiod = mailer.getScheduleTime();

			BasicDBObject filter = new BasicDBObject();
			filter.put("NL_MAP.ID", newsid.trim());
			if (isWeekly) {
				filter.append("NL_MAP.WL", true);
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				iDay = cal.get(Calendar.DAY_OF_WEEK);


			} else if (isDaily) {
				filter.append("NL_MAP.DL", true);
			}
			filter.append("NL_MAP.V_NL", true);
			//filter.append("ST", "1"); // For active/validated user only

			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", String.valueOf((pageNo - 1) * perPage));
			List<DBObject> dbObjects = MongoRestAPIService.getList(SUBSCRIPTION_COLLECTION, filter, optionMap);
			if (dbObjects != null) {
				for (DBObject objDB : dbObjects) {
					if (objDB != null) {
						MailUser user = this.getMailUserFromDBObject(objDB, mailer);
						subscribers.add(user);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception caugh : " + e.getMessage());
		}
		return subscribers;
	}

	/**
	 * Converting from {@link DBObject} to {@link MailUser} object.
	 * 
	 * @param objDB
	 * @return
	 */
	private MailUser getMailUserFromDBObject(DBObject objDB, Mailer mailer) {
		String mailerID= mailer.getId();
		MailUser user = new MailUser();
		user.setEmailId(String.valueOf(objDB.get(Subscription.ALTERNATE_EMAIL_ID)));
		user.setSsoid(String.valueOf(objDB.get(Subscription.SSO_USER_ID)));
		user.setEmailSubject(mailer.getMailSubject());
		user.setEmailFrom(mailer.getMailerFrom());

		user.setTemplatePath(mailer.getTemplatepath());

		if (objDB != null && objDB.get("NL_MAP") != null) {
			List<DBObject> nlListMap = (List<DBObject>) objDB.get(Subscription.NEWS_LETTER_MAP);
			if (nlListMap != null) {
				for(DBObject db : nlListMap){
					//logger.debug(db.get(NewsLetter.SCHEDULETIME) + " : " + mailer.getScheduleTime());
					int schTime = ((Number)db.get(NewsLetter.SCHEDULETIME)).intValue();
					if(db.containsField(NewsLetter.ID) && mailerID.equals(String.valueOf(db.get(NewsLetter.ID))) && schTime == mailer.getScheduleTime()){
						user.setNewsletter(NewsletterDBObjectWrapper.getNewsLetterFromBson(db));
						break;
					}
				}
			}
		}
		return user;
	}

	/* (non-Javadoc)
	 * @see com.times.mailer.dao.MailerDao#getNewsLetterDailyTracker(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	public DBObject getNewsLetterDailyTracker(String newsLetterId, String date, String scheduledTime, String schedledFrequency) {
		DBObject dbObject = null;

		BasicDBObject query = new BasicDBObject();
		query.put(NewsLetterDailyTracker.NEWS_LETTER_ID, new Integer(newsLetterId));
		query.put(NewsLetterDailyTracker.DATE, date);
		query.put(NewsLetterDailyTracker.SCHEDULED_FREQUENCY, new Integer(schedledFrequency));
		query.put(NewsLetterDailyTracker.SCHEDULED_TIME, new Integer(scheduledTime));

		dbObject = MongoRestAPIService.get(NEWSLETTER_DAILY_TRACKET_COLLECTION, query);
		return dbObject;
	}

	/* (non-Javadoc)
	 * @see com.times.newsletter.dao.MailerDao#addNewsLetterDailyTracker(com.times.newsletter.model.NewsLetterDailyTracker)
	 */
	public void addNewsLetterDailyTracker(NewsLetterDailyTracker dailyTracker)  {

		DBObject dbObject = NewsletterDBObjectWrapper.getDBObjectByNewsLetterTracker(dailyTracker);
		try {

			MongoRestAPIService.insert(NEWSLETTER_DAILY_TRACKET_COLLECTION, dbObject);
		} catch (MongoException e) {
			logger.error("MongoException caught while adding newsLetterDailyTracking , msg : " + e.getMessage(), e);
			//throw e;
		} catch (Exception e) {
			logger.error("Generic Exception caught while adding newsLetterDailyTracking ", e);
		} finally {
		}

	}

	public void addMailerStats(String skey, int ireadCount, int imailCount) {
		try {
			DBObject dbObject = new BasicDBObject();
			dbObject.put("DT", new Date());
			dbObject.put("NL_KEY", skey);
			dbObject.put("USR", ireadCount);
			dbObject.put("MAIL", imailCount);

			MongoRestAPIService.insert(NEWSLETTER_STATS, dbObject);

		} catch (MongoException e) {
			logger.error("MongoException caught while adding newsLetterDailyTracking , msg : " + e.getMessage(), e);
		} catch (Exception e) {
			logger.error("Generic Exception caught while adding newsLetterDailyTracking ", e);
		} finally {
		}
	}


	/* (non-Javadoc)
	 * @see com.times.newsletter.dao.MailerDao#updateNewsLetterDailyTracker(java.lang.String, java.lang.String, int, int)
	 */
	public void updateNewsLetterDailyTracker(String code, String key, int userCount, int sentCount) {

		try {
			DBObject query = new BasicDBObject();
			//dbObject.put("DT", new Date());
			query.put(NewsLetterDailyTracker.NEWS_LETTER_CODE, code);

			DBObject nldTrackerObject = getNewsLetterDailyTracker(code, key, NEWSLETTER_DAILY_TRACKET_COLLECTION);
			if (nldTrackerObject != null) {
				nldTrackerObject.put(NewsLetterDailyTracker.NEWS_LETTER_SENT_COUNTER, sentCount);
				nldTrackerObject.put(NewsLetterDailyTracker.NEWS_LETTER_USERS_COUNT, userCount);

				// update the object.
				MongoRestAPIService.update(NEWSLETTER_DAILY_TRACKET_COLLECTION, query, nldTrackerObject);
			}

		} catch (MongoException e) {
			logger.error("MongoException caught while updating newsLetterDailyTracking , msg : " + e.getMessage(), e);
		} catch (Exception e) {
			logger.error("Generic Exception caught while updating newsLetterDailyTracking ", e);
		} finally {
		}
	}

	/**
	 * This method is used to get the {@link DBObject} for <code>NewsLetterDailyTracker</code>.
	 *   
	 * @param code
	 * @param key
	 * @param nldTrackerColl
	 * @return
	 */
	private DBObject getNewsLetterDailyTracker(String code, String key, String collectionName) {
		DBObject dbObject = null;
		try {
			String[] keysArray = key.split("[" + Constant.KEY_BUILDER_SEPERATOR + "]");
			DBObject query = new BasicDBObject();
			query.put(NewsLetterDailyTracker.NEWS_LETTER_CODE, code);
			query.put(NewsLetterDailyTracker.NEWS_LETTER_ID, Integer.parseInt(keysArray[0].trim()));
			query.put(NewsLetterDailyTracker.SCHEDULED_FREQUENCY,Integer.parseInt( keysArray[1].trim()));
			query.put(NewsLetterDailyTracker.SCHEDULED_TIME, Integer.parseInt(keysArray[2].trim()));
			//query.put(NewsLetterDailyTracker.DATE, MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD));

			dbObject = MongoRestAPIService.get(collectionName, query);

		} catch (Exception e) {
			logger.error("Generic Exception caught while adding newsLetterDailyTracking ", e);
		} 

		return dbObject;
	}

	/* (non-Javadoc)
	 * @see com.times.mailer.dao.MailerDao#getNewsletterMailer(com.mongodb.DBObject)
	 */
	public List<Mailer> getNewsletterMailer(String nlId, boolean isDaily) {
		List<Mailer> mailerList = null;
		try {
			DBObject query = new BasicDBObject();
			query.put(Mailer.ID, nlId);
			if (isDaily) {
				query.put(Mailer.DAILY, 1);
			} else {
				query.put(Mailer.WEEKLY, 1);
			}

			List<DBObject> dbObjectList = MongoRestAPIService.getList(NEWSLETTER_MASTER_COLLECTION, query);
			if (dbObjectList != null) {
				mailerList = new ArrayList<Mailer>();
				for (DBObject dbObject : dbObjectList) {
					Mailer nlMaster = NewsletterDBObjectWrapper.getMailerFromDBObject(dbObject);
					if (nlMaster != null) {
						nlMaster.setScheduleTime(1); //as always run in morning
						mailerList.add(nlMaster);
					}
				}

			}
		} catch (Exception e) {
			logger.debug("getNewsletterMailer method , ex : " + e.getMessage());
		}

		return mailerList;
	}


	/* (non-Javadoc)
	 * @see com.times.mailer.dao.MailerDao#getNewsletterMailer(com.mongodb.DBObject)
	 */
	public List<DBObject> getUsersDBObjectBetweenDates(String nlId, boolean verifiedUser, Date startDate, Date endDate) {
		List<DBObject> dbObjectList = null;
		try {
			DBObject query = new BasicDBObject();
			//query.put(Mailer.ID, nlId);
			query.put(Subscription.NEWS_LETTER_MAP + "." + NewsLetter.ID, nlId);
			query.put(Subscription.NEWS_LETTER_MAP + "." + NewsLetter.VERIFIED_NEWSLETTER, verifiedUser);

			query.put("NL_MAP.U_AT", new BasicDBObject("$gt", startDate).append("$lte", endDate));

			BasicDBObject date = new BasicDBObject();
			date.append("$gte", new Date(startDate.getTime()));
			date.append("$lte", new Date(endDate.getTime()));

			DBObject query1 = BasicDBObjectBuilder.start()
					.add(Subscription.NEWS_LETTER_MAP + "." + NewsLetter.ID, nlId)
					.add(Subscription.NEWS_LETTER_MAP + "." + NewsLetter.VERIFIED_NEWSLETTER, verifiedUser)
					.add("NL_MAP.U_AT", date)
					.get();

			dbObjectList = MongoRestAPIService.getList(SUBSCRIPTION_COLLECTION, query1);
			/*if (dbObjectList != null) {
				mailerList = new ArrayList<Mailer>();
				for (DBObject dbObject : dbObjectList) {
					Mailer nlMaster = NewsletterDBObjectWrapper.getMailerFromDBObject(dbObject);
					if (nlMaster != null) {
						mailerList.add(nlMaster);
					}
				}

			}*/
		} catch (Exception e) {
			logger.debug("getNewsletterMailer method , ex : " + e.getMessage());
		}

		return dbObjectList;
	}

	/* (non-Javadoc)
	 * @see com.times.mailer.dao.MailerDao#getNewsletterMailer(com.mongodb.DBObject)
	 */
	public List<DBObject> getNewsletterDBObjectById(String nlId) {
		List<DBObject> dbObjectList = null;
		try {
			DBObject query = new BasicDBObject();
			query.put(Mailer.ID, nlId);
			//query.put(, nlId);


			dbObjectList = MongoRestAPIService.getList(NEWSLETTER_MASTER_COLLECTION, query);
			/*if (dbObjectList != null) {
				mailerList = new ArrayList<Mailer>();
				for (DBObject dbObject : dbObjectList) {
					Mailer nlMaster = NewsletterDBObjectWrapper.getMailerFromDBObject(dbObject);
					if (nlMaster != null) {
						mailerList.add(nlMaster);
					}
				}

			}*/
		} catch (Exception e) {
			logger.debug("getNewsletterMailer method , ex : " + e.getMessage());
		}

		return dbObjectList;
	}

	@Override
	public List<MailUser> getSubscribers(int offset, int length, Mailer mailer) throws Exception{

		List<MailUser> subscribers = new ArrayList<MailUser>();
		List<DBObject> dbObjects = null;
		try {

			BasicDBObject filter = new BasicDBObject();
			filter.put("NL_MAP.ID", mailer.getId());
			filter.append("NL_MAP.V_NL", true); // For active/validated user only

			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", length);
			optionMap.put("skip", String.valueOf((offset - 1) * length));
			dbObjects = MongoRestAPIService.getList(SUBSCRIPTION_COLLECTION, filter, optionMap); 
			if (dbObjects != null) {
				for (DBObject objDB : dbObjects) {
					if (objDB != null) {
						MailUser user = this.getMailUserFromDBObject(objDB, mailer);
						subscribers.add(user);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception caugh : " + e.getMessage());
			throw e;
		}
		return subscribers;
	}



	@Override
	public void addAlertUser(MailUser alertUserList,  Map<String, Set<String>> map, Map<Integer, String> msidTimestampMap) {
		try {
			DBObject dbObject = NewsletterDBObjectWrapper.getDBObjectByAlertUser(alertUserList, map, msidTimestampMap);
			if (dbObject != null) {
				MongoRestAPIService.insert(ALERT_LOG_COLLECTION, dbObject);
			}
		} catch (MongoException e) {
			logger.error("MongoException caught while adding AlertUser , msg : " + e.getMessage(), e);
		} catch (Exception e) {
			logger.error("Generic Exception caught while adding AlertUser ", e);
		} finally {
		}
	}

	@Override
	public boolean isArticleSentAlertUser(int msid, String emailID) throws Exception {
		boolean isFound = false;
		List<DBObject> dbObjects = null;
		try {

			BasicDBObject filter = new BasicDBObject();
			filter.append("E_ID", emailID); // For active/validated user only
			dbObjects = MongoRestAPIService.getList(ALERT_LOG_COLLECTION, filter); 

			if (dbObjects != null) {
				//dbObjects.get()
				//for (String s : dbObjects.get)
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception caugh : " + e.getMessage());
			throw e;
		}

		return isFound;
	}

	/**
	 * @param emailID
	 * @return
	 * @throws Exception
	 */
	public DBObject getAlertDBObjectEmailSent(String emailId) throws Exception {
		DBObject dbObject = null;
		try {

			BasicDBObject filter = new BasicDBObject();
			filter.append("E_ID", emailId); // For active/validated user only
			dbObject = MongoRestAPIService.get(ALERT_LOG_COLLECTION, filter); 


		} catch (Exception e) {
			logger.error("exception caught : " + e.getMessage());
			throw e;
		}
		return dbObject;
	}

	/**
	 * @param emailID
	 * @return
	 * @throws Exception
	 */
	public void updateAlertLogCollection(DBObject updatedDBObject, Map<String, Set<String>> map, Map<Integer, String> msidTimestampMap) throws Exception {
		try {
			
			DBObject query = new BasicDBObject();
			query.put(Constant.EMAIL_ID, updatedDBObject.get(Constant.EMAIL_ID)); 

			BasicDBObject mapInEach = new BasicDBObject();
			
					
			for(String key : map.keySet()) {				
				mapInEach.put(Constant.NLID_MSID_COLLECTION+ "."+ key, new BasicDBObject("$each", map.get(key)));
			}

			BasicDBObject addToSet = new BasicDBObject("$addToSet", mapInEach);
			MongoRestAPIService.update(ALERT_LOG_COLLECTION, query, addToSet); 
			
			// save msid with timestamp
			BasicDBObject datatoupdate = new BasicDBObject();
			for(Integer key : msidTimestampMap.keySet()) {				
				datatoupdate.put(Constant.MSID+ "."+ key, msidTimestampMap.get(key));
			}
			
			BasicDBObject newDocument = new BasicDBObject();
			newDocument.append("$set", datatoupdate);		
			MongoRestAPIService.update(ALERT_LOG_COLLECTION, query, newDocument, true);


		} catch (Exception e) {
			logger.error("exception caught : " + e.getMessage());
			throw e;

		}
	}
}
