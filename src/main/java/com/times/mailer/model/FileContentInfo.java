package com.times.mailer.model;

public class FileContentInfo {
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSourceUrl() {
		return sourceUrl;
	}
	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public void setWorktype(int worktype) {
		this.worktype = worktype;
	}
	public int getWorktype() {
		return worktype;
	}
	private String sourceUrl;
	private int worktype;
	private String content;
	private String fileName;
	private NetstorageInfo netstorageLocation;
	
	// for Amazon S3 storage details
	private AmazonS3Credits amazonS3;
	
	public NetstorageInfo getNetstorageLocation() {
		return netstorageLocation;
	}
	public void setNetstorageLocation(NetstorageInfo netstorageLocation) {
		this.netstorageLocation = netstorageLocation;
	}
	private boolean status;
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	/**
	 * @return the amazonS3
	 */
	public AmazonS3Credits getAmazonS3() {
		return amazonS3;
	}
	/**
	 * @param amazonS3 the amazonS3 to set
	 */
	public void setAmazonS3(AmazonS3Credits amazonS3) {
		this.amazonS3 = amazonS3;
	}
}
