package com.times.mailer.model;

public class MsidObject {

	String msid;

	String msTitle;
	
	String overridelink;
	
	public String getOverridelink() {
		return overridelink;
	}

	public void setOverridelink(String overridelink) {
		this.overridelink = overridelink;
	}

	public String getMsid() {
		return msid;
	}

	public void setMsid(String msid) {
		this.msid = msid;
	}

	public String getMsTitle() {
		return msTitle;
	}

	@Override
	public String toString() {
		return "MsidObject [msid=" + msid + ", msTitle=" + msTitle + "]";
	}

	public void setMsTitle(String msTitle) {
		this.msTitle = msTitle;
	}

}
