package com.times.mailer.model;

public class CandidateConstituency {
	
	private String candidatename;
	private String vipname;
	private String imageInfo;
	private String constituency;
	private String state;
	private String partyname;
	private String groupname;
	private String partyName;
	private String status;
	private String votes;
	private Integer constituencyid;
	/**
	 * @return the candidatename
	 */
	public String getCandidatename() {
		return candidatename;
	}
	/**
	 * @param candidatename the candidatename to set
	 */
	public void setCandidatename(String candidatename) {
		this.candidatename = candidatename;
	}
	/**
	 * @return the vipname
	 */
	public String getVipname() {
		return vipname;
	}
	/**
	 * @param vipname the vipname to set
	 */
	public void setVipname(String vipname) {
		this.vipname = vipname;
	}
	/**
	 * @return the imageInfo
	 */
	public String getImageInfo() {
		return imageInfo;
	}
	/**
	 * @param imageInfo the imageInfo to set
	 */
	public void setImageInfo(String imageInfo) {
		this.imageInfo = imageInfo;
	}
	/**
	 * @return the constituency
	 */
	public String getConstituency() {
		return constituency;
	}
	/**
	 * @param constituency the constituency to set
	 */
	public void setConstituency(String constituency) {
		this.constituency = constituency;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the partyname
	 */
	public String getPartyname() {
		return partyname;
	}
	/**
	 * @param partyname the partyname to set
	 */
	public void setPartyname(String partyname) {
		this.partyname = partyname;
	}
	/**
	 * @return the groupname
	 */
	public String getGroupname() {
		return groupname;
	}
	/**
	 * @param groupname the groupname to set
	 */
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	/**
	 * @return the partyName
	 */
	public String getPartyName() {
		return partyName;
	}
	/**
	 * @param partyName the partyName to set
	 */
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the votes
	 */
	public String getVotes() {
		return votes;
	}
	/**
	 * @param votes the votes to set
	 */
	public void setVotes(String votes) {
		this.votes = votes;
	}
	/**
	 * @return the constituencyid
	 */
	public Integer getConstituencyid() {
		return constituencyid;
	}
	/**
	 * @param constituencyid the constituencyid to set
	 */
	public void setConstituencyid(Integer constituencyid) {
		this.constituencyid = constituencyid;
	}
	
	
	
}
