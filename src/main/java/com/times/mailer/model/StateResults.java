package com.times.mailer.model;

import java.util.List;

public class StateResults {
	
	/**
	 * @return the constituencies
	 */
	public List<CandidateConstituency> getConstituencies() {
		return constituencies;
	}
	/**
	 * @param constituencies the constituencies to set
	 */
	public void setConstituencies(List<CandidateConstituency> constituencies) {
		this.constituencies = constituencies;
	}
	String state;
	List<PartyGroupDetails> parties;
	List<CandidateConstituency> constituencies; 
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the parties
	 */
	public List<PartyGroupDetails> getParties() {
		return parties;
	}
	/**
	 * @param parties the parties to set
	 */
	public void setParties(List<PartyGroupDetails> parties) {
		this.parties = parties;
	}
	
	
	
}
