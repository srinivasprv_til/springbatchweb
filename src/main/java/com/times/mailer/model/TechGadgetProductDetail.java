package com.times.mailer.model;

import java.util.Arrays;
import java.util.Date;

import com.mongodb.DBObject;

/**
 * Model for Transferring Data from reader to writer, which is then used to save in mongo
 * @author Anurag.Singhal
 *
 */
public class TechGadgetProductDetail {
	
	private String productId;
	private String productName;
	private String[] productImage;
	private String added;
	private String deleted;
	private String brandName;
	private String brandImage;
	private Date announcedDate;
	private String modelName; //Name without Brand name(e.g. iphone 5s)
	private String commonName; //Common Name for Different Variants of Same Product (e.g. 'Xiaomi Redmi Note 3') for 'Xiaomi Redmi Note 3 32GB' and 'Xiaomi Redmi Note 3 16GB'.
	private String category;
	private double price;
	private String source;
	private String[] colors;
	private String[] variants;
	private DBObject specs;
	private DBObject key_features;
	private int status;
	private int discontinued; //0-false, 1-true (This is Integer for future perspective to have separate value for upcoming gadgets). 
	private int upcoming; //0-false, 1-true
	private int slideShowId;
	private int[] imageMsid;
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String[] getProductImage() {
		return productImage;
	}
	public void setProductImage(String[] productImage) {
		this.productImage = productImage;
	}
	public String getAdded() {
		return added;
	}
	public void setAdded(String added) {
		this.added = added;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getBrandImage() {
		return brandImage;
	}
	public void setBrandImage(String brandImage) {
		this.brandImage = brandImage;
	}
	public Date getAnnouncedDate() {
		return announcedDate;
	}
	public void setAnnouncedDate(Date announcedDate) {
		this.announcedDate = announcedDate;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getCommonName() {
		return commonName;
	}
	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String[] getColors() {
		return colors;
	}
	public void setColors(String[] colors) {
		this.colors = colors;
	}
	public String[] getVariants() {
		return variants;
	}
	public void setVariants(String[] variants) {
		this.variants = variants;
	}
	public DBObject getSpecs() {
		return specs;
	}
	public void setSpecs(DBObject specs) {
		this.specs = specs;
	}
	public DBObject getKey_features() {
		return key_features;
	}
	public void setKey_features(DBObject key_features) {
		this.key_features = key_features;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getDiscontinued() {
		return discontinued;
	}
	public void setDiscontinued(int discontinued) {
		this.discontinued = discontinued;
	}
	public int getUpcoming() {
		return upcoming;
	}
	public void setUpcoming(int upcoming) {
		this.upcoming = upcoming;
	}
	
//	@Override
//	public String toString() {
//		return "TechGadgetProductDetail [productId=" + productId
//				+ ", productName=" + productName + ", brandName=" + brandName
//				+ ", announcedDate=" + announcedDate + ", commonName="
//				+ commonName + ", category=" + category + ", price=" + price
//				+ ", variants=" + Arrays.toString(variants) + ", status="
//				+ status + "]";
//	}
	
	@Override
	public String toString() {
		return "TechGadgetProductDetail [productId=" + productId
				+ ", productName=" + productName + ", productImage="
				+ Arrays.toString(productImage) + ", added=" + added
				+ ", deleted=" + deleted + ", brandName=" + brandName
				+ ", brandImage=" + brandImage + ", announcedDate="
				+ announcedDate + ", modelName=" + modelName + ", commonName="
				+ commonName + ", category=" + category + ", price=" + price
				+ ", source=" + source + ", colors=" + Arrays.toString(colors)
				+ ", variants=" + Arrays.toString(variants) + ", specs="
				+ specs + ", key_features=" + key_features + ", status="
				+ status + ", discontinued=" + discontinued + ", upcoming="
				+ upcoming + ", slideShowId=" + slideShowId + ", imageMsid="
				+ Arrays.toString(imageMsid) + "]";
	}
	public int getSlideShowId() {
		return slideShowId;
	}
	public void setSlideShowId(int slideShowId) {
		this.slideShowId = slideShowId;
	}
	public int[] getImageMsid() {
		return imageMsid;
	}
	public void setImageMsid(int[] imageMsid) {
		this.imageMsid = imageMsid;
	}
	

	/*@Override
	public int compareTo(TechGadgetProductDetail obj) {
		
		int i = 0;
		if(obj.getPrice() > 0)
			i = Double.compare(obj.getPrice(), this.getPrice());
		
		if(i != 0)
			return i;
		
		if(obj.getProductName() != null && !"".equals(obj.getProductName()))
			i = obj.getProductName().compareTo(this.getProductName());
		
		return i;
	}
	
	
	
	public static Comparator<TechGadgetProductDetail> mrpComparator = new Comparator<TechGadgetProductDetail>() {
		
		public int compare(TechGadgetProductDetail product1, TechGadgetProductDetail product2) {
			
			int i = product2.getCategory().compareTo(product1.getCategory());
			i = Double.compare(product2.getPrice(), product1.getPrice());
			if(i == 0 && product2.getProductName() != null)
				i = product2.getProductName().compareTo(product1.getProductName());
			
			return i;
		}
		
	};*/
	
	
}
