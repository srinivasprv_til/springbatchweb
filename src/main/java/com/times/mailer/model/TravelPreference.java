package com.times.mailer.model;

import java.io.Serializable;
import java.util.Date;



/**
 * This domain model is used to for newsLetter i.e. for section based.
 * 
 * @author Rajeev Khatri
 *
 */
public class TravelPreference implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1566239104674103678L;
	private String id;
	private String preferenceValue;
	private Date lastUpdate;
	private boolean isVerified = false;
	private String hashcode;
	private String listType;
	
	public static final String ID = "ID";
	public static final String LAST_UPDATE = "U_AT";
	public static final String LIST = "LIST";
	public static final String PREF = "PREF";
	public static final String VERIFIED_PREF = "V_PF";	
	public static final String HASH_CODE = "H_CODE";
	
	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

		
	@Override
	public NewsLetter clone() {
	    try {
			return (NewsLetter)super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}

	/**
	 * @return the isVerified
	 */
	public boolean isVerified() {
		return isVerified;
	}

	/**
	 * @param isVerified the isVerified to set
	 */
	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	/**
	 * @return the hashcode
	 */
	public String getHashcode() {
		return hashcode;
	}

	/**
	 * @param hashcode the hashcode to set
	 */
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
	
	/**
	 * @param preferenceValue
	 */
	public void setPreferenceValue(String preferenceValue) {
		this.preferenceValue = preferenceValue;
	}

	
	/**
	 * @return preferenceValue
	 */
	public String getPreferenceValue() {
		return preferenceValue;
	}

	
	/**
	 * @return lastUpdate
	 */
	public Date getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @param lastUpdate
	 */
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getListType() {
		return listType;
	}

	public void setListType(String listType) {
		this.listType = listType;
	}
	
}
