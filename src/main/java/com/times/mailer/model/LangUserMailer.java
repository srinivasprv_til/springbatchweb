package com.times.mailer.model;

import java.util.HashMap;
import java.util.Map;


public class LangUserMailer {

	int msid;
	int eroid;
	Map<String, String> paramMap = new HashMap<String, String>();
	
	
	public int getEroid() {
		return eroid;
	}
	public void setEroid(int eroid) {
		this.eroid = eroid;
	}
	public int getMsid() {
		return msid;
	}
	public void setMsid(int msid) {
		this.msid = msid;
	}
	public Map<String, String> getParamMap() {
		return paramMap;
	}
	public void setParamMap(Map<String, String> paramMap) {
		this.paramMap = paramMap;
	}
	
	
	
	
}
