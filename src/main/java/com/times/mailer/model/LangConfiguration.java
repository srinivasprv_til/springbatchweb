package com.times.mailer.model;

import java.util.Map;

public class LangConfiguration {

	String hostid;
	String httpUrl;
	String channelName;
	String frmName;
	String reply;
	String customization;
	Map<String, String> mapKeyMapping;
	public String getHostid() {
		return hostid;
	}
	public void setHostid(String hostid) {
		this.hostid = hostid;
	}
	public String getHttpUrl() {
		return httpUrl;
	}
	public void setHttpUrl(String httpUrl) {
		this.httpUrl = httpUrl;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getFrmName() {
		return frmName;
	}
	public void setFrmName(String frmName) {
		this.frmName = frmName;
	}
	public String getReply() {
		return reply;
	}
	public void setReply(String reply) {
		this.reply = reply;
	}
	public String getCustomization() {
		return customization;
	}
	public void setCustomization(String customization) {
		this.customization = customization;
	}
	public Map<String, String> getMapKeyMapping() {
		return mapKeyMapping;
	}
	public void setMapKeyMapping(Map<String, String> mapKeyMapping) {
		this.mapKeyMapping = mapKeyMapping;
	}
	
}
