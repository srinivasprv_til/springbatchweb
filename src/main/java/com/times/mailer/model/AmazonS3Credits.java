package com.times.mailer.model;

public class AmazonS3Credits {

	private String accessKey="AKIAIKNFHRLGZK4B3NVA";
	private String secretKey="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";
	private String bucketName="toi-origin";
	private String fileName;
	private String contentType = "application/json;charset=UTF-8";
	
	public AmazonS3Credits(String fileName) {
		this.fileName = fileName;
	}
	
	public AmazonS3Credits(String accessKey, String secretKey, String bucketName, String fileName, String contentType) {
		this.accessKey = accessKey;
		this.secretKey = secretKey;
		this.bucketName = bucketName;
		this.fileName = fileName;
		this.contentType = contentType;
	}
	
	/**
	 * @return the bucketName
	 */
	public String getBucketName() {
		return bucketName;
	}
	/**
	 * @param bucketName the bucketName to set
	 */
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the accessKey
	 */
	public String getAccessKey() {
		return accessKey;
	}
	/**
	 * @param accessKey the accessKey to set
	 */
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	/**
	 * @return the secretKey
	 */
	public String getSecretKey() {
		return secretKey;
	}
	/**
	 * @param secretKey the secretKey to set
	 */
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	@Override
	public String toString() {
		return "AmazonS3Credits [accessKey=" + accessKey + ", secretKey=" + secretKey + ", bucketName=" + bucketName
				+ ", fileName=" + fileName + ", contentType=" + contentType + "]";
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
