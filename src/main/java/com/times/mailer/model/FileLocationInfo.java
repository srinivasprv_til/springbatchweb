package com.times.mailer.model;

/**
 * This domain model object is used to keep the information for soruce (jcms url details ) 
 * details and target details (Akamai netstorage).
 * 
 * @author Ranjeet.Jha
 *
 */
public class FileLocationInfo {

	private int id;
	private String sourceUrl;
	private boolean status;
	private int workType;
	
	// used for cvoter for json callback function name
	private String callbackName;
	private String stateId;
	
	/**
	 * @return the stateId
	 */
	public String getStateId() {
		return stateId;
	}

	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	// for Akamai storerage details
	private NetstorageInfo netstorageLocation;

	// for Amazon S3 storage details
	private AmazonS3Credits amazonS3;

	private LocationType locationType;
	
	public FileLocationInfo() {
	}
	
	public FileLocationInfo(int id, String srcUrl, boolean status, NetstorageInfo netStorageInfo) {
		this.id = id;
		this.sourceUrl = srcUrl;
		this.status = status;
		this.netstorageLocation = netStorageInfo;
	}
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the sourceUrl
	 */
	public String getSourceUrl() {
		return sourceUrl;
	}

	/**
	 * @param sourceUrl
	 *            the sourceUrl to set
	 */
	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * @return the netstorageLocation
	 */
	public NetstorageInfo getNetstorageLocation() {
		return netstorageLocation;
	}

	/**
	 * @param netstorageLocation
	 *            the netstorageLocation to set
	 */
	public void setNetstorageLocation(NetstorageInfo netstorageLocation) {
		this.netstorageLocation = netstorageLocation;
	}

	/**
	 * @return the callbackName
	 */
	public String getCallbackName() {
		return callbackName;
	}

	/**
	 * @param callbackName the callbackName to set
	 */
	public void setCallbackName(String callbackName) {
		this.callbackName = callbackName;
	}

	@Override
	public String toString() {
		return "FileLocationInfo [id=" + id + ", sourceUrl=" + sourceUrl + ", status=" + status + ", workType="
				+ workType + ", callbackName=" + callbackName + ", stateId=" + stateId + ", netstorageLocation="
				+ netstorageLocation + ", amazonS3=" + amazonS3 + ", locationType=" + locationType + "]";
	}

	/**
	 * @return the amazonS3
	 */
	public AmazonS3Credits getAmazonS3() {
		return amazonS3;
	}

	/**
	 * @param amazonS3 the amazonS3 to set
	 */
	public void setAmazonS3(AmazonS3Credits amazonS3) {
		this.amazonS3 = amazonS3;
	}

	/**
	 * @return the workType
	 */
	public int getWorkType() {
		return workType;
	}

	/**
	 * @param workType the workType to set
	 */
	public void setWorkType(int workType) {
		this.workType = workType;
	}
	
	public LocationType getLocationType() {
		return locationType;
	}

	public void setLocationType(LocationType locationType) {
		this.locationType = locationType;
	}

	// identify type of upload location
	public static enum LocationType {
		NET_STORAGE(0), AWS(1);

		private int type;

		LocationType(int type) {
			this.type = type;
		}

		public int getType() {
			return type;
		}

		public static LocationType fromInt(int type) {
			for (LocationType lType : LocationType.values()) {
				if (lType.type == type)
					return lType;
			}
			return null;
		}
	}
	
}
