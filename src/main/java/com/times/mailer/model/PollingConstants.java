package com.times.mailer.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class PollingConstants {




	// keyname :
	public static HashMap<Integer, Integer> stateMapping = new HashMap<Integer, Integer>();
	public static HashMap<Integer,String> states= new HashMap<Integer, String>();
	public static HashMap<Integer, Integer>candidatemapping = new HashMap<Integer, Integer>();
	public static HashMap<String, String> PartyGroupMap = new HashMap<String, String>();
	
//	public static HashMap<Integer, Integer> constituencyMapping = new HashMap<Integer, Integer>();
	//public static HashMap<String, Integer> partyMapping = new HashMap<String, Integer>();
	//public static HashMap<String, String> CMSPartyMap = new HashMap<String, String>();
	//public static HashMap<String, String> CVoterPartyMap = new HashMap<String, String>();

	/*
	public static Integer getConstituencyMapping(int constituencyId) {
		//String pathName = "C:/Users/ranjeet.jha/Desktop/constituency_map.csv";
		if (constituencyMapping == null || constituencyMapping.size() <= 1) {
			populateStateMappingId();
		}
		if (constituencyMapping.containsKey(constituencyId)) {
			return constituencyMapping.get(constituencyId);
		} else {
		System.out.println("not found" + constituencyId);
			return -1;
		}
	}*/
	
	
	public static Integer getStateMapping(int stateid) {
		//String pathName = "C:/Users/ranjeet.jha/Desktop/constituency_map.csv";
		if (stateMapping == null || stateMapping.size() <= 1) {
			populateStateMappingId();
		}
		if (stateMapping.containsKey(stateid)){
			return stateMapping.get(stateid);
		}else
			return -1;
	
	}
	
	
	public static String getStateNameforEpolling(int stateid){
		if (states.size()<1){
			populateStateName();
		}
		return String.valueOf( states.get(stateid));
	}
	
	public static void saveFile(String strGson, String path) throws IOException {
		FileWriter fw = new FileWriter(path);
		BufferedWriter  out = new BufferedWriter(fw);
		fw.write(strGson);
		out.close();
	}
	

	public static String cvoter_groupIdtoGroupName(String groupId) {
		try {
			int iGroupId = Integer.parseInt(groupId);
			String groupName = null;
			switch (iGroupId) {
			case 1:
				groupName = "NDA";
				break;
			case 2:
				groupName = "UPA";
				break;
			case 3:
				groupName = "AAP";
				break;
			case 4:
				groupName = "OTHERS";
				break;

			}
			return groupName;

		} catch (Exception ex) {
			return null;
		}
	}
	
	public static void partyGroupMapping(){
		String pathName = com.times.common.util.Constant.CVOTER_STATIC_FILE_PATH;
		String line;
		try {
			BufferedReader br;
			br = new BufferedReader(new FileReader(pathName+"party_group_csv.csv"));
			line = br.readLine();
			while ((line = br.readLine()) != null) {
				String[] contestant_mapping = line.split(",");
				PartyGroupMap.put(contestant_mapping[0].trim(), contestant_mapping[1].trim());
			}
		}catch(Exception ex){
			
		}
	}
	
	
	public static void constestant_idMapping(){
		String pathName = com.times.common.util.Constant.CVOTER_STATIC_FILE_PATH;
		String line;
		try {
			BufferedReader br;
			br = new BufferedReader(new FileReader(pathName+"candidate_mapping.csv"));
			line = br.readLine();
			while ((line = br.readLine()) != null) {
				String[] contestant_mapping = line.split(",");
				int cvoterid = Integer.parseInt(contestant_mapping[0]);
				int c_cmsid = Integer.parseInt(contestant_mapping[1]);	
				candidatemapping.put(cvoterid, c_cmsid);
			}
			
		}catch(Exception ex){
			
		}
		
	}
	

	/*public static boolean contestant_Mapping(String pathName) {
		String line;
		try {
			BufferedReader br;
			br = new BufferedReader(new FileReader(pathName));
			line = br.readLine();
			while ((line = br.readLine()) != null) {
				String[] contestant_mapping = line.split(",");
				int stateId = Integer.parseInt(contestant_mapping[0]);
				int constituencyid = Integer.parseInt(contestant_mapping[7]);
				String contestantName = contestant_mapping[12];
				String PartyFullName = contestant_mapping[14];
				String PartyAlias = contestant_mapping[15];

				if (!CVoterPartyMap.containsKey(PartyAlias)) {
					CVoterPartyMap.put(PartyAlias, PartyFullName);
					
				}

				// System.out.println("CVoter Info " + stateId +"  "+
				// constituencyid +" "+ contestantName +" "+ PartyFullName +" "+
				// PartyAlias);
				// System.out.println("Mapping info for the cms" +
				// String.valueOf(stateMapping.get(stateId)) + "  "+
				// String.valueOf(constituencyMapping.get(constituencyid)));
			}

		} catch (Exception ex) {

		}
		return true;
	}*/

	/*public static boolean updateConstituencyStateMapping(String pathName) {
		String line;

		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(pathName));
			while ((line = br.readLine()) != null) {
				String[] constituency_mapping = line.split(",");
				int cvoterStateid = Integer.parseInt(constituency_mapping[0]);
				int cmsStateid = Integer.parseInt(constituency_mapping[2]);
				int cvoterConstituencyid = Integer
						.parseInt(constituency_mapping[1]);
				int cmsConstituencyid = Integer
						.parseInt(constituency_mapping[3]);
				if (!stateMapping.containsKey(cvoterStateid)) {
					stateMapping.put(cvoterStateid, cmsStateid);
				}

				if (!constituencyMapping.containsKey(cvoterConstituencyid)) {
					constituencyMapping.put(cvoterConstituencyid,
							cmsConstituencyid);
				}

			}
			
		
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}*/

	public static void populateStateName(){
		states.put(30,"Andaman & Nicobar Islands");
		states.put(3,"Andhra Pradesh");
		states.put(4,"Arunachal Pradesh");
		states.put(5,"Assam");
		states.put(6,"Bihar");
		states.put(31,"Chandigarh");
		states.put(27,"Chhattisgarh");
		states.put(32,"Dadra & Nagar Haveli");
		states.put(33,"Daman & Diu");
		states.put(1,"Delhi");
		states.put(7,"Goa");
		states.put(8,"Gujarat");
		states.put(9,"Haryana");
		states.put(10,"Himachal Pradesh");
		states.put(11,"Jammu & Kashmir");
		states.put(28,"Jharkhand");
		states.put(12,"Karnataka");
		states.put(13,"Kerala");
		states.put(34,"Lakshadweep");
		states.put(14,"Madhya Pradesh");
		states.put(15,"Maharashtra");
		states.put(16,"Manipur");
		states.put(17,"Meghalaya");
		states.put(18,"Mizoram");
		states.put(19,"Nagaland");
		states.put(20,"Orissa");
		states.put(35,"Puducherry");
		states.put(21,"Punjab");
		states.put(22,"Rajasthan");
		states.put(23,"Sikkim");
		states.put(24,"Tamil Nadu");
		states.put(25,"Tripura");
		states.put(2,"Uttar Pradesh");
		states.put(29,"Uttarakhand");
		states.put(26,"West Bengal");
	
	}
	
	public static void populateStateMappingId () {
		stateMapping.put(101, 30);
		stateMapping.put(102, 3);
		stateMapping.put(103, 4);
		stateMapping.put(104, 5);
		stateMapping.put(105, 6);
		stateMapping.put(106, 31);
		stateMapping.put(107, 27);
		stateMapping.put(108, 32);
		stateMapping.put(109, 33);
		stateMapping.put(110, 1);
		stateMapping.put(111, 7);
		stateMapping.put(112, 8);
		stateMapping.put(113, 9);
		stateMapping.put(114, 10);
		stateMapping.put(115, 11);
		stateMapping.put(116, 28);
		stateMapping.put(117, 12);
		stateMapping.put(118, 13);
		stateMapping.put(119, 34);
		stateMapping.put(120, 14);
		stateMapping.put(121, 15);
		stateMapping.put(122, 16);
		stateMapping.put(123, 17);
		stateMapping.put(124, 18);
		stateMapping.put(125, 19);
		stateMapping.put(126, 20);
		stateMapping.put(127, 35);
		stateMapping.put(128, 21);
		stateMapping.put(129, 22);
		stateMapping.put(130, 23);
		stateMapping.put(131, 24);
		stateMapping.put(132, 25);
		stateMapping.put(133, 2);
		stateMapping.put(134, 29);
		stateMapping.put(135, 26);
		
	}
	
	public static int getCVoterEpollId(Integer iexternalId){
		if (candidatemapping.size()==0){
			//addCandidateState1();
			constestant_idMapping();
		}
		return candidatemapping.containsKey(iexternalId)? candidatemapping.get(iexternalId):-1;
	}

	

	/**
	 * 
	 */
	private static void addCandidateState1() {
		candidatemapping.put(2664,8120);
		candidatemapping.put(2663,8119);
		candidatemapping.put(2678,8110);
		candidatemapping.put(2677,8123);
		candidatemapping.put(2679,8124);
		candidatemapping.put(2680,8125);
		candidatemapping.put(2681,8126);
		candidatemapping.put(2682,8127);
		candidatemapping.put(2683,8128);
		candidatemapping.put(2684,8129);
		candidatemapping.put(2661,8117);
		candidatemapping.put(2662,8118);
		candidatemapping.put(2671,8122);
		candidatemapping.put(2667,8121);
		candidatemapping.put(2688,8133);
		candidatemapping.put(2685,8130);
		candidatemapping.put(2700,8134);
		candidatemapping.put(2701,8135);
		candidatemapping.put(2702,8136);
		candidatemapping.put(2703,8137);
		candidatemapping.put(2704,8138);
		candidatemapping.put(2705,8139);
		candidatemapping.put(2706,8140);
		candidatemapping.put(2707,8141);
		candidatemapping.put(2686,8131);
		candidatemapping.put(2687,8132);
		candidatemapping.put(2708,8142);
		candidatemapping.put(2709,8143);
		candidatemapping.put(2729,8151);
		candidatemapping.put(2726,8148);
		candidatemapping.put(2724,8146);
		candidatemapping.put(2725,8147);
		candidatemapping.put(2727,8149);
		candidatemapping.put(2728,8150);
		candidatemapping.put(2710,5043);
		candidatemapping.put(2711,8144);
		candidatemapping.put(2718,8145);
		candidatemapping.put(2732,8154);
		candidatemapping.put(2733,8155);
		candidatemapping.put(2752,5044);
		candidatemapping.put(2753,7300);
		candidatemapping.put(2754,7301);
		candidatemapping.put(2755,7302);
		candidatemapping.put(2756,7303);
		candidatemapping.put(2757,7304);
		candidatemapping.put(2659,8116);
		candidatemapping.put(2745,8157);
		candidatemapping.put(2746,8158);
		candidatemapping.put(2747,8159);
		candidatemapping.put(2748,8160);
		candidatemapping.put(2749,8161);
		candidatemapping.put(2750,8162);
		candidatemapping.put(2751,8163);
		candidatemapping.put(2731,8153);
		candidatemapping.put(2730,8152);
		candidatemapping.put(2743,8156);
		candidatemapping.put(2758,7305);
		candidatemapping.put(2760,7307);
		candidatemapping.put(2766,7309);
		candidatemapping.put(2767,7310);
		candidatemapping.put(2768,7311);
		candidatemapping.put(2769,7312);
		candidatemapping.put(2770,7313);
		candidatemapping.put(2771,7314);
		candidatemapping.put(2761,8111);
		candidatemapping.put(2759,7306);
		candidatemapping.put(2765,7308);
		candidatemapping.put(2773,7316);
		candidatemapping.put(2775,7318);
		candidatemapping.put(2783,7320);
		candidatemapping.put(2784,7321);
		candidatemapping.put(2786,7322);
		candidatemapping.put(2787,7323);
		candidatemapping.put(2788,7324);
		candidatemapping.put(2772,7315);
		candidatemapping.put(2774,7317);
		candidatemapping.put(2781,7319);
		candidatemapping.put(2791,7327);
		candidatemapping.put(2793,7329);
		candidatemapping.put(2797,7330);
		candidatemapping.put(2798,7331);
		candidatemapping.put(2799,7332);
		candidatemapping.put(2800,7333);
		candidatemapping.put(2801,7334);
		candidatemapping.put(2802,7335);
		candidatemapping.put(2803,8112);
		candidatemapping.put(2804,8113);
		candidatemapping.put(2805,8114);
		candidatemapping.put(2806,8115);
		candidatemapping.put(2792,7328);
		candidatemapping.put(2789,7325);
		candidatemapping.put(2790,7326);
	}
	
}
