package com.times.mailer.model;

import java.util.Date;


public class EpaperFileDetails {
	
	String id;
	String gsfid;	
	String content;
	String type;
	String date;
	Date creationDate;
	String fileName;
	byte pdfFileContent[];
	byte zipFileContent[];
	boolean status;
	
	public String getGsfid() {
		return gsfid;
	}
	public void setGsfid(String gsfid) {
		this.gsfid = gsfid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getPdfFileContent() {
		return pdfFileContent;
	}
	public void setPdfFileContent(byte[] pdfFileContent) {
		this.pdfFileContent = pdfFileContent;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public byte[] getZipFileContent() {
		return zipFileContent;
	}
	public void setZipFileContent(byte[] zipFileContent) {
		this.zipFileContent = zipFileContent;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@Override
	public String toString() {
		return "EpaperFileDetails [id=" + this.id + ", gsfid=" + this.gsfid + ", fileName=" + this.fileName + "]";
	}
	
}
