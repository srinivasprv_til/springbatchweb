package com.times.mailer.model;

import java.io.Serializable;

/**
 * @author Ranjeet.Jha
 * 
 */
public class Mailer implements Serializable {

	private static final long serialVersionUID = 3558355704869954545L;

	public static final String ID = "ID";
	public static final String NAME = "NM";
	public static final String GROUP_ID = "GID";
	public static final String MAPPING_ID = "MPID";
	public static final String SCHEDULE_TIME1 = "ST1";
	public static final String SCHEDULE_TIME2 = "ST2";
	public static final String PRIORITY = "PL";
	public static final String WEEKLY = "WK";
	public static final String DAILY = "DL";
	public static final String TEMPLATE = "TPL";
	public static final String STATUS = "ST";
	public static final String EXTPATH = "EXT";
	public static final String c = "P_F";
	public static final String PERSONALIZE = "PR";
	public static final String SUBJECT = "SUB";
	public static final String FROMMAIL = "FRM";
	public static final String SCHEDULED_MORNING_TIMING = "ST_MRN";
	public static final String SCHEDULED_EVENING_TIMING = "ST_EVE";

	private String id;
	private String name;
	private String groupId;
	private String cmsMappingId;
	private int scheduleTime; // for selection in db ---1 morning , 2 evening,
	private int priority;
	private boolean weekly;
	private boolean daily;
	private String templatepath; //
	private int status;
	private String htmlPath;
	private String personalize = "0"; // 0 for not personalise and 1 for
										// personalise
	private String mailSubject = " ";
	private String mailerFrom = "onlineeditor@indiatimes.com";

	// morning scheduled time in HH:mm format
	private String morningTime;
	private String eveningTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getCmsMappingId() {
		return cmsMappingId;
	}

	public void setCmsMappingId(String cmsMappingId) {
		this.cmsMappingId = cmsMappingId;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public boolean isWeekly() {
		return weekly;
	}

	public void setWeekly(boolean weekly) {
		this.weekly = weekly;
	}

	public boolean isDaily() {
		return daily;
	}

	public void setDaily(boolean daily) {
		this.daily = daily;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getHtmlPath() {
		return htmlPath;
	}

	public void setHtmlPath(String htmlPath) {
		this.htmlPath = htmlPath;
	}

	public String getTemplatepath() {
		return templatepath;
	}

	public void setTemplatepath(String templatepath) {
		this.templatepath = templatepath;
	}

	public int getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(int scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

	/**
	 * @return the personalize
	 */
	public String getPersonalize() {
		return personalize;
	}

	/**
	 * @param personalize
	 *            the personalize to set
	 */
	public void setPersonalize(String personalize) {
		this.personalize = personalize;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getMailerFrom() {
		return mailerFrom;
	}

	public void setMailerFrom(String mailerFrom) {
		this.mailerFrom = mailerFrom;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the morningTime
	 */
	public String getMorningTime() {
		return morningTime;
	}

	/**
	 * @param morningTime
	 *            the morningTime to set
	 */
	public void setMorningTime(String morningTime) {
		this.morningTime = morningTime;
	}

	/**
	 * @return the eveningTime
	 */
	public String getEveningTime() {
		return eveningTime;
	}

	/**
	 * @param eveningTime
	 *            the eveningTime to set
	 */
	public void setEveningTime(String eveningTime) {
		this.eveningTime = eveningTime;
	}

}
