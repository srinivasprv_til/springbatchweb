package com.times.mailer.model;

import java.util.Date;

public class ProductData {
	
	private String id = "";
	private String searchID = "";
	private String brandName = "";
	private String brandUname = "";
	private String name = "";
	private String pid = "";
	private String image = "";
	private String added = "";
	private String deleted = "";
	private String specs_body_weight = "";
	private String specs_body_dimensions = "";
	private String specs_body_sim = "";
	private String specs_sound_loudspeaker = "";
	private String specs_sound_alertTypes = "";
	private String specs_network_gprs = "";
	private String specs_network_technology = "";
	private String specs_network_4gBands_value = "";
	private String specs_network_4gBands_level = "4G_bands";
	private String specs_network_3gBands_value = "";
	private String specs_network_3gBands_level = "3G_bands";
	private String specs_network_2gBands_value = "";
	private String specs_network_2gBands_level = "2G_bands";
	private String specs_network_edge = "";
	private String specs_network_speed = "";
	private String specs_battery_type = "";
	private String specs_battery_standBy = "";
	private String specs_battery_talkTime = "";
	private String specs_camera_front = "";
	private String specs_camera_video = "";
	private String specs_camera_features = "";
	private String specs_camera_rear = "";
	private String specs_memory_internal = "";
	private String specs_memory_card_slot = "";
	private String specs_display_type = "";
	private String specs_display_multitouch = "";
	private String specs_display_resolution = "";
	private String specs_display_size = "";
	private String brandImage = "";
	private int msid;
	private Date DT;
	private String uname = "";
	private String kf_battery = "";
	private String kf_ram = "";
	private String kf_storage = "";
	private String kf_display = "";
	private String kf_camera = "";
	private String kf_os = "";
	private String kf_processor = "";
	private String source = "";
	private int status;
	private int catagory;
	private String uniqueId = "";
	private String lcName = "";
	private String announced = "";
	private String modelName = "";
	private int price;	
	private Date insertDT;
	private String pf_graphicsCard = "";
	private String pf_os = "";
	private String pf_cpu = "";
	private String pf_chipset = "";

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSearchID() {
		return searchID;
	}
	public void setSearchID(String searchID) {
		this.searchID = searchID;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getBrandUname() {
		return brandUname;
	}
	public void setBrandUname(String brandUname) {
		this.brandUname = brandUname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getAdded() {
		return added;
	}
	public void setAdded(String added) {
		this.added = added;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getSpecs_body_weight() {
		return specs_body_weight;
	}
	public void setSpecs_body_weight(String specs_body_weight) {
		this.specs_body_weight = specs_body_weight;
	}
	public String getSpecs_body_dimensions() {
		return specs_body_dimensions;
	}
	public void setSpecs_body_dimensions(String specs_body_dimensions) {
		this.specs_body_dimensions = specs_body_dimensions;
	}
	public String getSpecs_body_sim() {
		return specs_body_sim;
	}
	public void setSpecs_body_sim(String specs_body_sim) {
		this.specs_body_sim = specs_body_sim;
	}
	public String getSpecs_sound_loudspeaker() {
		return specs_sound_loudspeaker;
	}
	public void setSpecs_sound_loudspeaker(String specs_sound_loudspeaker) {
		this.specs_sound_loudspeaker = specs_sound_loudspeaker;
	}
	public String getSpecs_sound_alertTypes() {
		return specs_sound_alertTypes;
	}
	public void setSpecs_sound_alertTypes(String specs_sound_alertTypes) {
		this.specs_sound_alertTypes = specs_sound_alertTypes;
	}
	public String getSpecs_network_gprs() {
		return specs_network_gprs;
	}
	public void setSpecs_network_gprs(String specs_network_gprs) {
		this.specs_network_gprs = specs_network_gprs;
	}
	public String getSpecs_network_technology() {
		return specs_network_technology;
	}
	public void setSpecs_network_technology(String specs_network_technology) {
		this.specs_network_technology = specs_network_technology;
	}
	public String getSpecs_network_4gBands_value() {
		return specs_network_4gBands_value;
	}
	public void setSpecs_network_4gBands_value(String specs_network_4gBands_value) {
		this.specs_network_4gBands_value = specs_network_4gBands_value;
	}
	public String getSpecs_network_4gBands_level() {
		return specs_network_4gBands_level;
	}
	public void setSpecs_network_4gBands_level(String specs_network_4gBands_level) {
		this.specs_network_4gBands_level = specs_network_4gBands_level;
	}
	public String getSpecs_network_3gBands_value() {
		return specs_network_3gBands_value;
	}
	public void setSpecs_network_3gBands_value(String specs_network_3gBands_value) {
		this.specs_network_3gBands_value = specs_network_3gBands_value;
	}
	public String getSpecs_network_3gBands_level() {
		return specs_network_3gBands_level;
	}
	public void setSpecs_network_3gBands_level(String specs_network_3gBands_level) {
		this.specs_network_3gBands_level = specs_network_3gBands_level;
	}
	public String getSpecs_network_2gBands_value() {
		return specs_network_2gBands_value;
	}
	public void setSpecs_network_2gBands_value(String specs_network_2gBands_value) {
		this.specs_network_2gBands_value = specs_network_2gBands_value;
	}
	public String getSpecs_network_2gBands_level() {
		return specs_network_2gBands_level;
	}
	public void setSpecs_network_2gBands_level(String specs_network_2gBands_level) {
		this.specs_network_2gBands_level = specs_network_2gBands_level;
	}
	public String getSpecs_network_edge() {
		return specs_network_edge;
	}
	public void setSpecs_network_edge(String specs_network_edge) {
		this.specs_network_edge = specs_network_edge;
	}
	public String getSpecs_network_speed() {
		return specs_network_speed;
	}
	public void setSpecs_network_speed(String specs_network_speed) {
		this.specs_network_speed = specs_network_speed;
	}
	public String getSpecs_battery_type() {
		return specs_battery_type;
	}
	public void setSpecs_battery_type(String specs_battery_type) {
		this.specs_battery_type = specs_battery_type;
	}
	public String getSpecs_battery_standBy() {
		return specs_battery_standBy;
	}
	public void setSpecs_battery_standBy(String specs_battery_standBy) {
		this.specs_battery_standBy = specs_battery_standBy;
	}
	public String getSpecs_battery_talkTime() {
		return specs_battery_talkTime;
	}
	public void setSpecs_battery_talkTime(String specs_battery_talkTime) {
		this.specs_battery_talkTime = specs_battery_talkTime;
	}
	public String getSpecs_camera_front() {
		return specs_camera_front;
	}
	public void setSpecs_camera_front(String specs_camera_front) {
		this.specs_camera_front = specs_camera_front;
	}
	public String getSpecs_camera_video() {
		return specs_camera_video;
	}
	public void setSpecs_camera_video(String specs_camera_video) {
		this.specs_camera_video = specs_camera_video;
	}
	public String getSpecs_camera_features() {
		return specs_camera_features;
	}
	public void setSpecs_camera_features(String specs_camera_features) {
		this.specs_camera_features = specs_camera_features;
	}
	public String getSpecs_camera_rear() {
		return specs_camera_rear;
	}
	public void setSpecs_camera_rear(String specs_camera_rear) {
		this.specs_camera_rear = specs_camera_rear;
	}
	public String getSpecs_memory_internal() {
		return specs_memory_internal;
	}
	public void setSpecs_memory_internal(String specs_memory_internal) {
		this.specs_memory_internal = specs_memory_internal;
	}
	public String getSpecs_memory_card_slot() {
		return specs_memory_card_slot;
	}
	public void setSpecs_memory_card_slot(String specs_memory_card_slot) {
		this.specs_memory_card_slot = specs_memory_card_slot;
	}
	public String getSpecs_display_type() {
		return specs_display_type;
	}
	public void setSpecs_display_type(String specs_display_type) {
		this.specs_display_type = specs_display_type;
	}
	public String getSpecs_display_multitouch() {
		return specs_display_multitouch;
	}
	public void setSpecs_display_multitouch(String specs_display_multitouch) {
		this.specs_display_multitouch = specs_display_multitouch;
	}
	public String getSpecs_display_resolution() {
		return specs_display_resolution;
	}
	public void setSpecs_display_resolution(String specs_display_resolution) {
		this.specs_display_resolution = specs_display_resolution;
	}
	public String getSpecs_display_size() {
		return specs_display_size;
	}
	public void setSpecs_display_size(String specs_display_size) {
		this.specs_display_size = specs_display_size;
	}
	public String getBrandImage() {
		return brandImage;
	}
	public void setBrandImage(String brandImage) {
		this.brandImage = brandImage;
	}
	public int getMsid() {
		return msid;
	}
	public void setMsid(int msid) {
		this.msid = msid;
	}
	public Date getDT() {
		return DT;
	}
	public void setDT(Date dT) {
		DT = dT;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getKf_battery() {
		return kf_battery;
	}
	public void setKf_battery(String kf_battery) {
		this.kf_battery = kf_battery;
	}
	public String getKf_ram() {
		return kf_ram;
	}
	public void setKf_ram(String kf_ram) {
		this.kf_ram = kf_ram;
	}
	public String getKf_storage() {
		return kf_storage;
	}
	public void setKf_storage(String kf_storage) {
		this.kf_storage = kf_storage;
	}
	public String getKf_display() {
		return kf_display;
	}
	public void setKf_display(String kf_display) {
		this.kf_display = kf_display;
	}
	public String getKf_camera() {
		return kf_camera;
	}
	public void setKf_camera(String kf_camera) {
		this.kf_camera = kf_camera;
	}
	public String getKf_os() {
		return kf_os;
	}
	public void setKf_os(String kf_os) {
		this.kf_os = kf_os;
	}
	public String getKf_processor() {
		return kf_processor;
	}
	public void setKf_processor(String kf_processor) {
		this.kf_processor = kf_processor;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getCatagory() {
		return catagory;
	}
	public void setCatagory(int catagory) {
		this.catagory = catagory;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getLcName() {
		return lcName;
	}
	public void setLcName(String lcName) {
		this.lcName = lcName;
	}
	public String getAnnounced() {
		return announced;
	}
	public void setAnnounced(String announced) {
		this.announced = announced;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public Date getInsertDT() {
		return insertDT;
	}
	public void setInsertDT(Date insertDT) {
		this.insertDT = insertDT;
	}
	public String getPf_graphicsCard() {
		return pf_graphicsCard;
	}
	public void setPf_graphicsCard(String pf_graphicsCard) {
		this.pf_graphicsCard = pf_graphicsCard;
	}
	public String getPf_os() {
		return pf_os;
	}
	public void setPf_os(String pf_os) {
		this.pf_os = pf_os;
	}
	public String getPf_cpu() {
		return pf_cpu;
	}
	public void setPf_cpu(String pf_cpu) {
		this.pf_cpu = pf_cpu;
	}
	public String getPf_chipset() {
		return pf_chipset;
	}
	public void setPf_chipset(String pf_chipset) {
		this.pf_chipset = pf_chipset;
	}

}
