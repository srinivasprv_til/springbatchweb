package com.times.mailer.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class EpaperInfo {

	
	int id;
	String publisher;
	String edition;
	String location;
	String unique_key;
	String status;
	List<EpaperFileDetails> fileDetails = new ArrayList<EpaperFileDetails>();
	Map<String, List<String>> directoryDetails = new HashMap<>();
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getUnique_key() {
		return unique_key;
	}
	public void setUnique_key(String unique_key) {
		this.unique_key = unique_key;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<EpaperFileDetails> getFileDetails() {
		return fileDetails;
	}
	public void setFileDetails(List<EpaperFileDetails> fileDetails) {
		this.fileDetails = fileDetails;
	}
	public Map<String, List<String>> getDirectoryDetails() {
		return this.directoryDetails;
	}
	public void setDirectoryDetails(Map<String, List<String>> directoryDetails) {
		this.directoryDetails = directoryDetails;
	}
	
	public List<String> getDirectoriesForKey(String key) {
		return getDirectoryDetails().get(key);
	}
	@Override
	public String toString() {
		return "EpaperInfo [id=" + this.id + ", publisher=" + this.publisher + ", edition=" + this.edition
		        + ", location=" + this.location + ", unique_key=" + this.unique_key + ", status=" + this.status + "]";
	}
	

}
