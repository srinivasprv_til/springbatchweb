package com.times.mailer.model;

import java.io.Serializable;
import java.util.Date;

/**
 * This domain model is used to for newsLetter i.e. for section based.
 * 
 * @author Ranjeet.Jha
 *
 */
public class NewsLetter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 750630431817353444L;

	private String id;
	private String displayFormate;
	private Date lastUpdate;
	private String userPref;
	private int scheduleTime; // / enum of scheduleI and schedule
	private String ipAddress;
	private boolean weekly;
	private boolean daily;
	private int status;
	private String secName;
	private int day;
	private boolean isVerified;
	private String hashcode;

	public static final String ID = "ID";
	public static final String DISPLAY_FORMATE = "DF";
	public static final String LAST_UPDATE = "U_AT";
	public static final String USER_PREF = "U_PRE";
	public static final String SCHEDULETIME = "SCH";
	public static final String IP_ADDRESS = "C_IP";
	public static final String WEEKLY = "WL";
	public static final String DAILY = "DL";
	public static final String STATUS = "ST";
	public static final String SECTION_NAME = "S_NAME";
	public static final String WEEKLY_DAY = "WL_D";
	public static final String VERIFIED_NEWSLETTER = "V_NL";
	public static final String HASH_CODE = "H_CODE";
	
	public enum Day {
	    SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
	    THURSDAY, FRIDAY, SATURDAY 
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the displayFormate
	 */
	public String getDisplayFormate() {
		return displayFormate;
	}

	/**
	 * @param displayFormate
	 *            the displayFormate to set
	 */
	public void setDisplayFormate(String displayFormate) {
		this.displayFormate = displayFormate;
	}

	/**
	 * @return the schedule
	 */
/*	public String getSchedule() {
		return schedule;
	}

	*//**
	 * @param schedule
	 *            the schedule to set
	 *//*
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}*/

	/**
	 * @return the lastUpdate
	 */
	public Date getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @param lastUpdate
	 *            the lastUpdate to set
	 */
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @return the userPref
	 */
	public String getUserPref() {
		return userPref;
	}

	/**
	 * @param userPref
	 *            the userPref to set
	 */
	public void setUserPref(String userPref) {
		this.userPref = userPref;
	}

	

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress
	 *            the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the weekly
	 */
	public boolean isWeekly() {
		return weekly;
	}

	/**
	 * @param weekly
	 *            the weekly to set
	 */
	public void setWeekly(boolean weekly) {
		this.weekly = weekly;
	}

	/**
	 * @return the daily
	 */
	public boolean isDaily() {
		return daily;
	}

	/**
	 * @param daily
	 *            the daily to set
	 */
	public void setDaily(boolean daily) {
		this.daily = daily;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	public int getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(int scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

	public String getSecName() {
		return secName;
	}

	public void setSecName(String secName) {
		this.secName = secName;
	}

	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(int day) {
		this.day = day;
	}

	/**
	 * @return the isVerified
	 */
	public boolean isVerified() {
		return isVerified;
	}

	/**
	 * @param isVerified the isVerified to set
	 */
	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	/**
	 * @return the hashcode
	 */
	public String getHashcode() {
		return hashcode;
	}

	/**
	 * @param hashcode the hashcode to set
	 */
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}

}
