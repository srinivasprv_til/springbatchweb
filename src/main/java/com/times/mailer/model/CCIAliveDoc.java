package com.times.mailer.model;

import org.apache.solr.client.solrj.beans.Field;

public class CCIAliveDoc {
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getStory() {
		return story;
	}

	public void setStory(String story) {
		this.story = story;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSeopath() {
		return seopath;
	}

	public void setSeopath(String seopath) {
		this.seopath = seopath;
	}

	@Field
	public String id;
	@Field
	public String title;
	
	@Field
	public String subject;
	@Field
	public String story;
	@Field
	public String edition;
	@Field
	public String type;
	@Field
	public String seopath;

}
