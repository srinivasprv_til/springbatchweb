package com.times.mailer.model;

import java.io.Serializable;
import java.util.Date;

/**
 * This DTO class is used to track the emailTemplate for each newsletter for
 * daily. This is for analytic purpose as well.
 * 
 * @author Ranjeet.Jha
 * 
 */
public class NewsLetterDailyTracker implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1360443401678678191L;
	
	private String id;
	private Date addedAt; // date is in //
	private String date; // date is in //
	
	private int newsletterId;
	private String newsletterCode;
	/*private String scheduledTime; // morning/evening
	private String scheduleFrequence; // daily/ weekly
	 */	
	private int scheduledTime; // morning/evening  // it can be int
	private int scheduleFrequence; // daily weekly // it can be int
	private long readCounter; // read counter for this uniqueCode
	private long sentCounter; // mail Sent counter for this uniqueCoder
	private long clickCounter; // mail Sent counter for this uniqueCoder
	private long usersCount; // this the actual no of user when job started.
	
	public static final String DATE = "DT";
	public static final String ADDED_AT = "C_AT";
	public static final String NEWS_LETTER_ID = "NL_ID";
	public static final String NEWS_LETTER_CODE = "CODE";
	public static final String SCHEDULED_TIME = "ST_ME"; // for morning or evening.
	public static final String SCHEDULED_FREQUENCY = "ST_DW"; // for daily or weekly
	public static final String NEWS_LETTER_READ_COUNTER = "R_CTR";
	public static final String NEWS_LETTER_SENT_COUNTER = "S_CTR"; 
	public static final String NEWS_LETTER_CLICK_COUNTER = "C_CTR";
	public static final String NEWS_LETTER_USERS_COUNT = "U_CTR";
	
	//public static final String ADDED_DATE = "DT";
	
	
	public  NewsLetterDailyTracker(){}
	
	public  NewsLetterDailyTracker(Date date, int newsLetterId, String newsletterCode, int scheduledTime, int scheduleFrequence, String sDate) {
		this.addedAt = date;
		this.newsletterId = newsLetterId;
		this.newsletterCode = newsletterCode;
		this.scheduledTime = scheduledTime;
		this.scheduleFrequence = scheduleFrequence;
		this.date = sDate;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the addedAt
	 */
	public Date getAddedAt() {
		return addedAt;
	}

	/**
	 * @param addedAt the addedAt to set
	 */
	public void setAddedAt(Date addedAt) {
		this.addedAt = addedAt;
	}

	/**
	 * @return the newsletterId
	 */
	public int getNewsletterId() {
		return newsletterId;
	}

	/**
	 * @param newsletterId the newsletterId to set
	 */
	public void setNewsletterId(int newsletterId) {
		this.newsletterId = newsletterId;
	}

	/**
	 * @return the newsletterCode
	 */
	public String getNewsletterCode() {
		return newsletterCode;
	}

	/**
	 * @param newsletterCode the newsletterCode to set
	 */
	public void setNewsletterCode(String newsletterCode) {
		this.newsletterCode = newsletterCode;
	}

	/**
	 * @return the readCounter
	 */
	public long getReadCounter() {
		return readCounter;
	}

	/**
	 * @param readCounter the readCounter to set
	 */
	public void setReadCounter(long readCounter) {
		this.readCounter = readCounter;
	}

	/**
	 * @return the sentCounter
	 */
	public long getSentCounter() {
		return sentCounter;
	}

	/**
	 * @param sentCounter the sentCounter to set
	 */
	public void setSentCounter(long sentCounter) {
		this.sentCounter = sentCounter;
	}

	/**
	 * @return the clickCounter
	 */
	public long getClickCounter() {
		return clickCounter;
	}

	/**
	 * @param clickCounter the clickCounter to set
	 */
	public void setClickCounter(long clickCounter) {
		this.clickCounter = clickCounter;
	}

	/**
	 * @return the scheduledTime
	 */
	public int getScheduledTime() {
		return scheduledTime;
	}

	/**
	 * @param scheduledTime the scheduledTime to set
	 */
	public void setScheduledTime(int scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	/**
	 * @return the scheduleFrequence
	 */
	public int getScheduleFrequence() {
		return scheduleFrequence;
	}

	/**
	 * @param scheduleFrequence the scheduleFrequence to set
	 */
	public void setScheduleFrequence(int scheduleFrequence) {
		this.scheduleFrequence = scheduleFrequence;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the usersCount
	 */
	public long getUsersCount() {
		return usersCount;
	}

	/**
	 * @param usersCount the usersCount to set
	 */
	public void setUsersCount(long usersCount) {
		this.usersCount = usersCount;
	}

	
}
