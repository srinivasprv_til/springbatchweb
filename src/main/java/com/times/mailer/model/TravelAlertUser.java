/**
 * 
 */
package com.times.mailer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;



/**
 * This domain model is used as Data transfer object (DT) for Travel Alert.
 * 
 * @author ranjeet.jha
 *
 */
public class TravelAlertUser implements Serializable {

	private static final long serialVersionUID = 872621476861113722L;

	private Set<Integer> msid;
	private String emailId;
	private String nlid;
	private boolean isSSOUser;
	private Map<String, String> preferences;
	private List<NewsLetter> newsletters;
	private String templatePath;
	
	private Set<TravelPreference> travelSubscrptionPreference;
	
	

	

	private Map<String, String> map = new HashMap<String, String>();
	
	private Map<String, Set<Integer>> preferemceMsidsMap;
	
	private String emailSubject;

	// These are the key 
	public static final String MSID = "MSID";
	public static final String EMAIL_ID = "E_ID";
	public static final String NLID = "NLID";
	public static final String USER_PREFERENCES_MAP = "U_PREF";

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the nlid
	 */
	public String getNlid() {
		return nlid;
	}

	/**
	 * @param nlid the nlid to set
	 */
	public void setNlid(String nlid) {
		this.nlid = nlid;
	}

	/**
	 * @return the isSSOUser
	 */
	public boolean isSSOUser() {
		return isSSOUser;
	}

	/**
	 * @param isSSOUser the isSSOUser to set
	 */
	public void setSSOUser(boolean isSSOUser) {
		this.isSSOUser = isSSOUser;
	}

	/**
	 * @return the preferences
	 */
	public Map<String, String> getPreferences() {
		return preferences;
	}

	/**
	 * @param preferences the preferences to set
	 */
	public void setPreferences(Map<String, String> preferences) {
		this.preferences = preferences;
	}
	
	public void addNewsletter(NewsLetter newletter) {
		if (newsletters == null) {
			newsletters = new ArrayList<NewsLetter>();
		}
		newsletters.add(newletter);
	}

	/**
	 * @return the msid
	 */
	public Set<Integer> getMsid() {
		return msid;
	}

	/**
	 * @param msid the msid to set
	 */
	public void setMsid(Set<Integer> msid) {
		this.msid = msid;
	}

	/**
	 * @return the newsletters
	 */
	public List<NewsLetter> getNewsletters() {
		return newsletters;
	}

	/**
	 * @param newsletters the newsletters to set
	 */
	public void setNewsletters(List<NewsLetter> newsletters) {
		this.newsletters = newsletters;
	}

	/**
	 * @return the emailSubject
	 */
	public String getEmailSubject() {
		return emailSubject;
	}

	/**
	 * @param emailSubject the emailSubject to set
	 */
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	/**
	 * @param id
	 */
	public void addMsid(int id) {
		if (this.msid == null) {
			msid = new HashSet<Integer>();
		}
		this.msid.add(id);
	}

	public void addAlertMap(String key, String value) {
		this.map.put(key, value);
	}

	/**
	 * @return the map
	 */
	public Map<String, String> getMap() {
		return map;
	}

	/**
	 * @param map the map to set
	 */
	public void setMap(Map<String, String> map) {
		this.map = map;
	}
	
	public void addPreferenceMsidMap(String key, Set<Integer> value) {
		if( preferemceMsidsMap == null) {
			preferemceMsidsMap = new HashMap<String, Set<Integer>>();
		}
		preferemceMsidsMap.put(key, value);
	}

	/**
	 * @return the preferemceMsidsMap
	 */
	public Map<String, Set<Integer>> getPreferemceMsidsMap() {
		return preferemceMsidsMap;
	}

	/**
	 * @param preferemceMsidsMap the preferemceMsidsMap to set
	 */
	public void setPreferemceMsidsMap(Map<String, Set<Integer>> preferemceMsidsMap) {
		this.preferemceMsidsMap = preferemceMsidsMap;
	}
	
	/**
	 * @return templatePath of newsletter
	 */
	public String getTemplatePath() {
		return templatePath;
	}

	/**
	 * @param templatePath
	 */
	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}
	
	public Set<TravelPreference> getTravelSubscrptionPreference() {
		return travelSubscrptionPreference;
	}

	/**
	 * @param travelSubscrptionPreference
	 */
	public void setTravelSubscrptionPreference(
			Set<TravelPreference> travelSubscrptionPreference) {
		this.travelSubscrptionPreference = travelSubscrptionPreference;
	}
	
	
	/**
	 * @param tp
	 */
	public void addTravelSubscrptionPreference(TravelPreference tp) {
		if(travelSubscrptionPreference == null) {
			travelSubscrptionPreference = new HashSet<TravelPreference>();
		}
		travelSubscrptionPreference.add(tp);
	}
}
