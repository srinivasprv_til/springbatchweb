package com.times.mailer.model;

import java.io.Serializable;

/**
 * @author manish.chandra
 *
 */
public class MailUser implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String ssoid;
	private String emailId;
	private String emailSubject;
	private String emailFrom;
	private String firstName;
	private String lastName;
	private String mobile;
	private NewsLetter newsletter;
	
	
	private String templatePath;
	
	
	
	/**
	 * @return the ssoid
	 */
	public String getSsoid() {
		return ssoid;
	}

	/**
	 * @param ssoid
	 *            the ssoid to set
	 */
	public void setSsoid(String ssoid) {
		this.ssoid = ssoid;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the newsletter
	 */
	public NewsLetter getNewsletter() {
		return newsletter;
	}

	/**
	 * @param newsletter
	 *            the newsletter to set
	 */
	public void setNewsletter(NewsLetter newsletter) {
		this.newsletter = newsletter;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	
	
}
