package com.times.mailer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This domain model class is used for the main NewsLetters subscription .
 * 
 * @author Ranjeet.Jha
 *
 */
public class Subscription implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -676325424207167207L;
	private String id;
	private String ssoUID;
	private String alternateEmail;
	private int status;
	private Date createdAt;
	private Date updateAt;
	private List<NewsLetter> newsLetters;
	private int isnewuser;
	private boolean isVerified;
	private String hashcode;
	private String userName;
	
	public static final String SSO_USER_ID = "S_ID";
	public static final String ALTERNATE_EMAIL_ID = "E_ID";
	public static final String STATUS = "ST";
	public static final String CREATED_AT = "C_AT";
	public static final String UPDATED_AT = "U_AT";
	public static final String NEWS_LETTER_MAP = "NL_MAP";
	public static final String OBJECT_ID = "_id";
	public static final String NEW_USER = "N_USR";
	public static final String VERIFIED_USER = "V_USR";
	public static final String HASH_CODE = "H_CODE";
	public static final String USER_NAME = "U_NM";
	public static final String NEWS_LETTER_PREF = "NL_PREF";
	
	/**
	 * @return the ssoUID
	 */
	public String getSsoUID() {
		return ssoUID;
	}

	/**
	 * @param ssoUID
	 *            the ssoUID to set
	 */
	public void setSsoUID(String ssoUID) {
		this.ssoUID = ssoUID;
	}

	/**
	 * @return the alternateEmail
	 */
	public String getAlternateEmail() {
		return alternateEmail;
	}

	/**
	 * @param alternateEmail
	 *            the alternateEmail to set
	 */
	public void setAlternateEmail(String alternateEmail) {
		this.alternateEmail = alternateEmail;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updateAt
	 */
	public Date getUpdateAt() {
		return updateAt;
	}

	/**
	 * @param updateAt
	 *            the updateAt to set
	 */
	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	/**
	 * @return the newsLetters
	 */
	public List<NewsLetter> getNewsLetters() {
		return newsLetters;
	}

	/**
	 * @param newsLetters
	 *            the newsLetters to set
	 */
	public void setNewsLetters(List<NewsLetter> newsLetters) {
		this.newsLetters = newsLetters;
	}
	
	public void addNewsLetter(NewsLetter nl) {
		if(newsLetters == null) {
			newsLetters = new ArrayList<NewsLetter>();
		}
		newsLetters.add(nl);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the isnewuser
	 */
	public int getIsnewuser() {
		return isnewuser;
	}

	/**
	 * @param isnewuser the isnewuser to set
	 */
	public void setIsnewuser(int isnewuser) {
		this.isnewuser = isnewuser;
	}

	/**
	 * @return the isVerified
	 */
	public boolean isVerified() {
		return isVerified;
	}

	/**
	 * @param isVerified the isVerified to set
	 */
	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	/**
	 * @return the hashcode
	 */
	public String getHashcode() {
		return hashcode;
	}

	/**
	 * @param hashcode the hashcode to set
	 */
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

}
