package com.times.mailer.model;

import java.util.Arrays;
import java.util.Date;

public class ImageInfo {
	
	private int id;
	
	private byte[] originalData;
	
	private byte[] optimizedData;
	
	private String omno;
	
	private Date insertTime;
	
	private String mimeType;
	
	private String name ;
	
	private String umd;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getOriginalData() {
		return originalData;
	}

	public void setOriginalData(byte[] originalData) {
		this.originalData = originalData;
	}

	public byte[] getOptimizedData() {
		return optimizedData;
	}

	public void setOptimizedData(byte[] optimizedData) {
		this.optimizedData = optimizedData;
	}

	public String getOmno() {
		return omno;
	}

	public void setOmno(String omno) {
		this.omno = omno;
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUmd() {
		return umd;
	}

	public void setUmd(String umd) {
		this.umd = umd;
	}

	@Override
	public String toString() {
		return "ImageInfo [id=" + id + ", originalData="
				+ Arrays.toString(originalData) + ", optimizedData="
				+ Arrays.toString(optimizedData) + ", omno=" + omno
				+ ", insertTime=" + insertTime + ", mimeType=" + mimeType
				+ ", name=" + name + ", umd=" + umd + "]";
	}
}
