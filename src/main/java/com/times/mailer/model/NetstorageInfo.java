/**
 * 
 */
package com.times.mailer.model;

import java.io.Serializable;

/**
 * This class is used as a domain model object for NetStorage details info
 * to upload the content.
 * 
 * @author Ranjeet.Jha
 *
 */
public class NetstorageInfo implements Serializable {

	private static final long serialVersionUID = 2857740225849487870L;
	
	private int id;
	private String serverName;
	private String folderLocation;
	private String fileName;
	private String userid;
	private String password;
	
	public NetstorageInfo() {}
	
	public NetstorageInfo(int id, String serverIp, String folderLocation, String fileName, String userName, String pwd) {
		this.id = id;
		this.serverName = serverIp;
		this.folderLocation = folderLocation;
		this.fileName = fileName;
		this.userid = userName;
		this.password = pwd;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the folderLocation
	 */
	public String getFolderLocation() {
		return folderLocation;
	}
	/**
	 * @param folderLocation the folderLocation to set
	 */
	public void setFolderLocation(String folderLocation) {
		this.folderLocation = folderLocation;
	}
	/**
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}
	/**
	 * @param userid the userid to set
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}
	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
