package com.times.mailer.model;

import java.io.Serializable;

/**
 * for holding party group details information.
 * 
 * @author Ranjeet.Jha
 * 
 */
public class PartyGroupDetails implements Serializable {

	
	private static final long serialVersionUID = 4758790542310090107L;
	
	private String party;
	private String party_fullname;
	private String colorcode;
	private String groupname;
	private String leading;
	private String won;
	private String total;
	private String lastwon;
	private String gainloss;
	private String votes;
	private String totalvotes;
	private String voteshare;
	private String lastvotes;
	private String lasttotalvotes;
	private String lastvoteshare;
	private String voteswing;

	/**
	 * @return the votes
	 */
	public String getVotes() {
		return votes;
	}

	/**
	 * @param votes the votes to set
	 */
	public void setVotes(String votes) {
		this.votes = votes;
	}

	/**
	 * @return the totalvotes
	 */
	public String getTotalvotes() {
		return totalvotes;
	}

	/**
	 * @param totalvotes the totalvotes to set
	 */
	public void setTotalvotes(String totalvotes) {
		this.totalvotes = totalvotes;
	}

	/**
	 * @return the voteshare
	 */
	public String getVoteshare() {
		return voteshare;
	}

	/**
	 * @param voteshare the voteshare to set
	 */
	public void setVoteshare(String voteshare) {
		this.voteshare = voteshare;
	}

	/**
	 * @return the lastvotes
	 */
	public String getLastvotes() {
		return lastvotes;
	}

	/**
	 * @param lastvotes the lastvotes to set
	 */
	public void setLastvotes(String lastvotes) {
		this.lastvotes = lastvotes;
	}

	/**
	 * @return the lasttotalvotes
	 */
	public String getLasttotalvotes() {
		return lasttotalvotes;
	}

	/**
	 * @param lasttotalvotes the lasttotalvotes to set
	 */
	public void setLasttotalvotes(String lasttotalvotes) {
		this.lasttotalvotes = lasttotalvotes;
	}

	/**
	 * @return the lastvoteshare
	 */
	public String getLastvoteshare() {
		return lastvoteshare;
	}

	/**
	 * @param lastvoteshare the lastvoteshare to set
	 */
	public void setLastvoteshare(String lastvoteshare) {
		this.lastvoteshare = lastvoteshare;
	}

	/**
	 * @return the voteswing
	 */
	public String getVoteswing() {
		return voteswing;
	}

	/**
	 * @param voteswing the voteswing to set
	 */
	public void setVoteswing(String voteswing) {
		this.voteswing = voteswing;
	}

	/**
	 * @return the lastwon
	 */
	public String getLastwon() {
		return lastwon;
	}

	/**
	 * @param lastwon the lastwon to set
	 */
	public void setLastwon(String lastwon) {
		this.lastwon = lastwon;
	}

	/**
	 * @return the gainloss
	 */
	public String getGainloss() {
		return gainloss;
	}

	/**
	 * @param gainloss the gainloss to set
	 */
	public void setGainloss(String gainloss) {
		this.gainloss = gainloss;
	}

	/**
	 * @return the party
	 */
	public String getParty() {
		return party;
	}

	/**
	 * @param party the party to set
	 */
	public void setParty(String party) {
		this.party = party;
	}

	/**
	 * @return the party_fullname
	 */
	public String getParty_fullname() {
		return party_fullname;
	}

	/**
	 * @param party_fullname the party_fullname to set
	 */
	public void setParty_fullname(String party_fullname) {
		this.party_fullname = party_fullname;
	}

	/**
	 * @return the colorcode
	 */
	public String getColorcode() {
		return colorcode;
	}

	/**
	 * @param colorcode the colorcode to set
	 */
	public void setColorcode(String colorcode) {
		this.colorcode = colorcode;
	}

	/**
	 * @return the groupname
	 */
	public String getGroupname() {
		return groupname;
	}

	/**
	 * @param groupname the groupname to set
	 */
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}


	
	

	/**
	 * @return the leading
	 */
	public String getLeading() {
		return leading;
	}

	/**
	 * @param leading
	 *            the leading to set
	 */
	public void setLeading(String leading) {
		this.leading = leading;
	}

	/**
	 * @return the won
	 */
	public String getWon() {
		return won;
	}

	/**
	 * @param won
	 *            the won to set
	 */
	public void setWon(String won) {
		this.won = won;
	}

	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}

}
