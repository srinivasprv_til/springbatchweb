package com.times.mailer.model;

import org.apache.commons.net.ftp.FTPFile;

public class FTPFileWithDirectory {
	private FTPFile ftpFile;
	private String path;
	
	public FTPFileWithDirectory(FTPFile ftpFile, String path) {
		super();
		this.ftpFile = ftpFile;
		this.path = path;
	}
	
	public FTPFile getFtpFile() {
		return this.ftpFile;
	}
	public void setFtpFile(FTPFile ftpFile) {
		this.ftpFile = ftpFile;
	}
	public String getPath() {
		return this.path;
	}
	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return getFtpFile().getName();
	}
}
