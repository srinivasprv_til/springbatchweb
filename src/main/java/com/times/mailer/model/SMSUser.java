package com.times.mailer.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author rajeev khatri
 *
 */
// TODO SMSUser represent each entry in smsUsers collection in mongo.
public class SMSUser implements Serializable {

	private String id;
	private int hostid;
	private Date createdAt;
	private Date updateAt;
	private String users;
	private String channelName;
	private String programName;
	private String dateTime;
	private String timeBefore;
	private String sms;
	private String email;
	private boolean smsFlag;
	private boolean emailFlag;
	
	private List<MailUser> mailUserSubscribers;

	public static final String ID = "_id";
	public static final String HOSTID = "HID";
	public static final String CREATED_AT = "C_AT";
	public static final String UPDATED_AT = "U_AT";
	public static final String USERS = "USERS";
	public static final String DATE_TIME = "DT";
	public static final String TIME_BEFORE = "TB";
	public static final String CHANNEL_NAME = "CH";
	public static final String PROGRAM_NAME = "PRG";
	public static final String SMS = "SMS";
	public static final String EMAIL = "EMAIL";
	public static final String SMS_FLAG = "S_Flg";
	public static final String EMAIL_FLAG = "E_Flg";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getHostid() {
		return hostid;
	}
	public void setHostid(int hostid) {
		this.hostid = hostid;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}
	public String getUsers() {
		return users;
	}
	public void setUsers(String users) {
		this.users = users;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getTimeBefore() {
		return timeBefore;
	}
	public void setTimeBefore(String timeBefore) {
		this.timeBefore = timeBefore;
	}
	public String getSms() {
		return sms;
	}
	public void setSms(String sms) {
		this.sms = sms;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isSmsFlag() {
		return smsFlag;
	}
	public void setSmsFlag(boolean smsFlag) {
		this.smsFlag = smsFlag;
	}
	public boolean isEmailFlag() {
		return emailFlag;
	}
	public void setEmailFlag(boolean emailFlag) {
		this.emailFlag = emailFlag;
	}
	public List<MailUser> getMailUserSubscribers() {
		return mailUserSubscribers;
	}
	public void setMailUserSubscribers(List<MailUser> mailUserSubscribers) {
		this.mailUserSubscribers = mailUserSubscribers;
	}


}
