package com.times.parser;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConvertXMLtoJSON {
	
	private static final Logger logger = LoggerFactory.getLogger(ConvertXMLtoJSON.class);
	
	public static JSONObject getJSONObjectfromXML(String xml){
		
		 try {
			JSONObject xmlJSONObj = XML.toJSONObject(xml);
			return xmlJSONObj;
		} catch (JSONException e) {
			logger.error("Error while coverting XML to JSON");
			
		}
		 
		return null;
	}

}
