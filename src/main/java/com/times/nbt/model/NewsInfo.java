package com.times.nbt.model;

import java.util.Date;

/***
 * 
 * @author Rajeev Khatri
 * 
 */
public class NewsInfo {

	
	int bnid;	
	int hostid;
	String bntext;
	Date bnInsertDateTime;
	Date bnUpdateDateTime;
	boolean status;
	String subject;
	
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public int getBnid() {
		return bnid;
	}
	public void setBnid(int bnid) {
		this.bnid = bnid;
	}
	public int getHostid() {
		return hostid;
	}
	public void setHostid(int hostid) {
		this.hostid = hostid;
	}
	public String getBntext() {
		return bntext;
	}
	public void setBntext(String bntext) {
		this.bntext = bntext;
	}
	public Date getBnInsertDateTime() {
		return bnInsertDateTime;
	}
	public void setBnInsertDateTime(Date bnInsertDateTime) {
		this.bnInsertDateTime = bnInsertDateTime;
	}
	public Date getBnUpdateDateTime() {
		return bnUpdateDateTime;
	}
	public void setBnUpdateDateTime(Date bnUpdateDateTime) {
		this.bnUpdateDateTime = bnUpdateDateTime;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}

}
