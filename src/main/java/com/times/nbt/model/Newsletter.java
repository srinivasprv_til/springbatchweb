package com.times.nbt.model;

public class Newsletter {
	
	String msgSenderName;
	String msgSenderEmail;
	int msgPriority=2;
	String mailSubject;
	String msgTo;
	String MailContent ;
	int cmtid;
	int hostid;
	
	/**
	 * @return the hostid
	 */
	public int getHostid() {
		return hostid;
	}

	/**
	 * @param hostid the hostid to set
	 */
	public void setHostid(int hostid) {
		this.hostid = hostid;
	}

	public int getCmtid() {
		return cmtid;
	}

	public void setCmtid(int cmtid) {
		this.cmtid = cmtid;
	}

	public String getMsgSenderName() {
		return msgSenderName;
	}

	public void setMsgSenderName(String msgSenderName) {
		this.msgSenderName = msgSenderName;
	}

	public String getMsgSenderEmail() {
		return msgSenderEmail;
	}

	public void setMsgSenderEmail(String msgSenderEmail) {
		this.msgSenderEmail = msgSenderEmail;
	}

	public int getMsgPriority() {
		return msgPriority;
	}

	public void setMsgPriority(int msgPriority) {
		this.msgPriority = msgPriority;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getMsgTo() {
		return msgTo;
	}

	public void setMsgTo(String msgTo) {
		this.msgTo = msgTo;
	}

	public String getMsgContentType() {
		return msgContentType;
	}

	public void setMsgContentType(String msgContentType) {
		this.msgContentType = msgContentType;
	}

	public String getMsgCharacterSet() {
		return msgCharacterSet;
	}

	public void setMsgCharacterSet(String msgCharacterSet) {
		this.msgCharacterSet = msgCharacterSet;
	}

	String msgContentType="text/html";
	String msgCharacterSet ="us-ascii";
	

	public String getMailContent() {
		return MailContent;
	}

	public void setMailContent(String mailContent) {
		MailContent = mailContent;
	}
	
		
}
