package com.times.nbt.model;

import java.util.Map;

/***
 * 
 * @author manish.chandra
 * Defines the user information
 */
public class UserInfo {

	public Map<String, String> getUserParams() {
		return userParams;
	}
	public void setUserParams(Map<String, String> userParams) {
		this.userParams = userParams;
	}
	int imsid;
	int eroid;
	int hostid;
	String comment;
	String userName;
	String userEmail;
	String userDetails =null;
	String cmtMsg;
	Map<String, String> userParams;
	
	public int getHostid() {
		return hostid;
	}
	public void setHostid(int hostid) {
		this.hostid = hostid;
	}
	public int getImsid() {
		return imsid;
	}
	public void setImsid(int imsid) {
		this.imsid = imsid;
	}
	public int getEroid() {
		return eroid;
	}
	public void setEroid(int eroid) {
		this.eroid = eroid;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(String userDetails) {
		this.userDetails = userDetails;
	}
	public String getCmtMsg() {
		return cmtMsg;
	}
	public void setCmtMsg(String cmtMsg) {
		this.cmtMsg = cmtMsg;
	}
	
	
	

}
