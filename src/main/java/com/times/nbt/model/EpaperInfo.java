package com.times.nbt.model;

import java.util.List;


public class EpaperInfo {

	
	int id;
	int publisher;
	int edition;
	String location;
	String unique_key;
	String status;
	List<EpaperFileDetails> fileDetails;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPublisher() {
		return publisher;
	}
	public void setPublisher(int publisher) {
		this.publisher = publisher;
	}
	public int getEdition() {
		return edition;
	}
	public void setEdition(int edition) {
		this.edition = edition;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getUnique_key() {
		return unique_key;
	}
	public void setUnique_key(String unique_key) {
		this.unique_key = unique_key;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<EpaperFileDetails> getFileDetails() {
		return fileDetails;
	}
	public void setFileDetails(List<EpaperFileDetails> fileDetails) {
		this.fileDetails = fileDetails;
	}

}
