package com.times.nbt.model;


public class EpaperFileDetails {
	
	String gsfid;	
	String content;
	String type;
	
	public String getGsfid() {
		return gsfid;
	}
	public void setGsfid(String gsfid) {
		this.gsfid = gsfid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
