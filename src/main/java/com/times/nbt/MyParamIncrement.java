package com.times.nbt;

import java.util.Date;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;

public class MyParamIncrement implements JobParametersIncrementer {

	public JobParameters getNext(JobParameters parameters) {
		
		/*if (parameters==null || parameters.isEmpty()) 
		{            return new JobParametersBuilder().addLong("run.id", 20L).toJobParameters();    
		}
		long id = parameters.getLong("run.id",20L) + 1;    
		*/
		Date date = new Date();
		long id = date.getTime();
		return new JobParametersBuilder().addLong("run.id", id).toJobParameters();
	}

	

}
