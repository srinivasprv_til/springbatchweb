package com.times.nbt;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.batch.item.ItemReader;
import org.springframework.stereotype.Component;

import com.times.nbt.dao.PostGresDAO;
import com.times.nbt.dao.SqlServerDAO;
import com.times.nbt.model.CampaignMapping;
import com.times.nbt.model.UserInfo;

/**
 * {@link ItemReader} with hard-coded input data.
 */


public class NewsletterReader implements ItemReader<UserInfo> {
	
	SqlServerDAO sqlService;
	String poisonPillPath ;
	
	public String getPoisonPillPath() {
		return poisonPillPath;
	}


	public void setPoisonPillPath(String poisonPillPath) {
		this.poisonPillPath = poisonPillPath;
	}


	public SqlServerDAO getSqlService() {
		return sqlService;
	}


	public void setSqlService(SqlServerDAO sqlService) {
		this.sqlService = sqlService;
	}

	private List<UserInfo> lstMailer = null;
	
	private  int index = 0;
	
	
	private List<UserInfo> getUser(){
		try {
			lstMailer =null;
			Thread.sleep(120000);
			lstMailer = getSqlService().getMailUsers();
			if ((lstMailer ==null) || (lstMailer.size()<1)){
				getUser();
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return lstMailer;
	}
	
	/**
	 * Reads next record from input
	 */
	public  UserInfo read() throws Exception {
		

		File f = new File(poisonPillPath);
		if(f.exists()){
			return null;
		}
		
		if ((lstMailer ==null || lstMailer.size()==0)){
			index =0;
			getUser();
			//lstMailer = getSqlService().getMailUsers();
		}else if(index>= lstMailer.size()){
			//lstMailer = null;
			index =  0;
			getUser();
			
		}
		
		if ((lstMailer !=null) &&(index < lstMailer.size())) {
			index=index+1;
			return lstMailer.get(index-1);
		}else{
			
			return null;
		}
		
	}

}
