package com.times.nbt.servicetest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestMain {

	private String jobName="nbtMailer";
	
	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	static ClassPathXmlApplicationContext ctx = null;

	public void scheduleJob(){
		//System.out.println("inside the method at "+ new Date());\
		if (ctx == null){
			ctx = new ClassPathXmlApplicationContext(new String[]{"launch-context.xml"});
		}
		Job jb = (Job) ctx.getBean(jobName);
		JobLauncher jbLauncher =(JobLauncher) ctx.getBean("jobLauncher");
		try {
			JobParameter jbp = new JobParameter(new Date());
			Map<String, JobParameter> parameters = new HashMap<String, JobParameter>();
			parameters.put("param1", jbp);
			jbLauncher.run(jb, new JobParameters(parameters));

		} catch (Exception e) {
			// TODO: handle exception
		} finally{
			
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
    	ctx = new ClassPathXmlApplicationContext(new String[]{"launch-context.xml"});
    	
	}

}
