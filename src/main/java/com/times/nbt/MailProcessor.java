package com.times.nbt;

import java.io.StringReader;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.batch.item.ItemProcessor;

import com.times.common.util.MailerUtil;
import com.times.nbt.dao.PostGresDAO;
import com.times.nbt.dao.SqlServerDAO;
import com.times.nbt.model.CampaignMapping;
import com.times.nbt.model.Newsletter;
import com.times.nbt.model.UserInfo;

public class MailProcessor implements ItemProcessor<UserInfo, Newsletter> {

	
	SqlServerDAO sqlService;
	PostGresDAO postGres;
	
	private Timestamp lastAccess = null;
	private Map<String, CampaignMapping> campaign =null;
	private Map<String, String> newsletter;
	
	
	public PostGresDAO getPostGres() {
		return postGres;
	}

	public void setPostGres(PostGresDAO postGres) {
		this.postGres = postGres;
	}

	
	private boolean initializeCampaign(){
		Date presentDate = new Date();
		lastAccess = new Timestamp(presentDate.getTime());
		try {
			Collection<CampaignMapping> lst = getPostGres().getMapping();
			if (lst!=null){
				Iterator<CampaignMapping> itr = lst.iterator();
				campaign = new HashMap<String, CampaignMapping>();
				while (itr.hasNext()){
					CampaignMapping mapping = itr.next();
					campaign.put(mapping.getCampaign(), mapping);
				}	
			}
			
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}

	
	private boolean getViewforCampaign(String skey){
		if (!campaign.containsKey(skey)){
			initializeCampaign();
		}
		
		if (campaign.containsKey(skey) ){
			CampaignMapping object = campaign.get(skey); 
			String httpUrl = object.getTemplateUrl();
			//String httpUrl =  "http://jcmsdev.indiatimes.com:8002/testnewsletter.cms";
			String outData = MailerUtil.getData(httpUrl);
			if (outData==null){
				return false;
			}else{
				if (newsletter ==null){
					newsletter =new HashMap<String, String>();
				}
				newsletter.put(skey, outData);
				return true;	
			}
		
		
		}else
			return false;
		
		
		/*
		try {
			
			if ((httpUrl !=null) && (httpUrl.trim()!="")){
				HttpClientParams clientParams = new HttpClientParams();
				clientParams.setParameter(HttpClientParams.MAX_REDIRECTS, "2");
				clientParams.setParameter(HttpClientParams.RETRY_HANDLER, 2);
				clientParams.setConnectionManagerTimeout( new Long(3000));
				clientParams.setSoTimeout(6000);
				String outData = null;
				HttpClient httpClient = new HttpClient();
				HttpMethod httpMethod = new GetMethod(httpUrl);
				httpMethod.setParams(clientParams);
				try {
					int statusCode = httpClient.executeMethod(httpMethod);
					if (statusCode != HttpStatus.SC_OK) {
						System.out.println("statusCode of URI:" + httpUrl + " statusCode : "+ statusCode);
						return false;
					} else {
						
						outData = new String(httpMethod.getResponseBody(), "UTF-8");
						if (newsletter ==null){
							newsletter =new HashMap<String, String>();
						}
						newsletter.put(skey, outData);
						return true;
					}
				} catch (Exception ex) {
					System.out.println("Exception caught during reading of URI:" + httpUrl + " msg : "+ ex.getMessage());
					return false;
				} finally {
					httpClient = null;
					clientParams =null;
					httpMethod =null;
				}
			}else{
				return false;
			}
			
		} catch (Exception e) {
			return false;
		}
		*/	
	} 
	
	
	public Newsletter process(UserInfo user) throws Exception {
		
		String strView =null;
		if ((campaign ==null) ||(lastAccess==null)){
			initializeCampaign();
		}
		String sCampaignKey = getSqlService().getNewsletterCampaignAssociated(user.getImsid(), user.getHostid());
		if  ((newsletter==null)|| (! newsletter.containsKey(sCampaignKey.trim()))){
			if (! getViewforCampaign(sCampaignKey.trim())){
				return null;
			}
		}
		
		strView = newsletter.get(sCampaignKey.trim());
	
		if ((strView !=null) && (strView.trim() !="")){
			strView = strView.replaceAll("\\[#trendimage]", MailerUtil.getUniqueCodeByDate(sCampaignKey.trim()+"_"+MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD)));
			String xmlData =  new String(user.getUserDetails().getBytes("UTF-8"));
			if ((xmlData!=null) &&(!"null".equals(xmlData))){
				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(new StringReader(xmlData));
				if (doc!=null){
					Map<String, String> userProp = new HashMap<String, String>();
					
					for(int i=0; i< doc.getRootElement().getChildren().size();i++){
						Element elm =(Element) doc.getRootElement().getChildren().get(i);
						userProp.put(elm.getName(), elm.getValue());
					}
				user.setUserParams(userProp);
				}
			}
			
			Newsletter userMailer = new Newsletter();
			CampaignMapping  objectMapping = campaign.get(sCampaignKey.trim());
			String subject =objectMapping.getSubject()!=null?objectMapping.getSubject():"Test Comments made live";
			String fromName = objectMapping.getFromMail()!=null? objectMapping.getFromEmail():"nbt editor";
			String fromemail = objectMapping.getFromEmail()!=null?objectMapping.getFromEmail():"nbtonline@indiatimes.com";
			String userEmail = user.getUserEmail();
			
			userMailer.setMailContent(personalizeView(strView, user));
			userMailer.setMailSubject(subject);
			userMailer.setMsgSenderEmail(fromemail);
		//	userMailer.setMsgTo("varun.verma@indiatimes.co.in");
			userMailer.setMsgTo(userEmail);
			userMailer.setMsgSenderName(fromName);
			
			//update usertable so as to ensure that email has been processed. 
			if (!getSqlService().updateEmailState(user.getEroid())){
				return null;
			}
			return userMailer;
		}else{	
			return null;
		}
	}	
	
	
	private String personalizeView(String view, UserInfo user){
		Iterator<String> i = user.getUserParams().keySet().iterator(); 
		while (i.hasNext()) {
			String key = (String) i.next();
			String value = (String)  user.getUserParams().get(key);
			view = view.replaceAll("\\[#"+key+"]", value);
		}
		
		return view;
	}

	public SqlServerDAO getSqlService() {
		return sqlService;
	}

	public void setSqlService(SqlServerDAO sqlService) {
		this.sqlService = sqlService;
	}
	

}
