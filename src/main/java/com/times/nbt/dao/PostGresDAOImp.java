package com.times.nbt.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import javax.sql.DataSource;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import com.times.nbt.model.CampaignMapping;

public class PostGresDAOImp implements PostGresDAO {

	
	NamedParameterJdbcTemplate simpleJdbcTemplate;
	DataSource dataSource;
	

	public PostGresDAOImp(DataSource dataSource) {
		super();
		this.simpleJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		
	}


	public Collection<CampaignMapping> getMapping() {
	
		String sql="select mappingid,campaign,url,hostid, frommail,subject,emailfrom  from mappingmaster";
		SqlParameterSource sqlParams = new MapSqlParameterSource();
		ParameterizedRowMapper rowMapper = new ParameterizedRowMapper(){
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				CampaignMapping campaignMapping = new CampaignMapping();
				campaignMapping.setCampaignId(rs.getInt("mappingid"));
				campaignMapping.setCampaign(String.valueOf(rs.getString("campaign")));
				campaignMapping.setHostid(rs.getInt("hostid"));
				campaignMapping.setTemplateUrl(rs.getString("url"));
				campaignMapping.setFromEmail(rs.getString("emailfrom"));
				campaignMapping.setFromMail(rs.getString("frommail"));
				campaignMapping.setSubject(String.valueOf(rs.getString("subject")));
				return campaignMapping;
			}
			
		};
		return simpleJdbcTemplate.query(sql, sqlParams, rowMapper);
		//return getSimpleJdbcTemplate().query(sql, rowMapper);
		
	}

	

	

	
	
	

}
