package com.times.nbt.dao;

import java.util.List;

import com.times.nbt.model.NewsInfo;
import com.times.nbt.model.UserInfo;

public interface SqlServerDAO {

	String sqlGetUsers ="select  MSID,eroid, isnull(eronmessage,eromessage) as EROMessage,EROAltDetails, altemailid, erouid from editedreadersopinion where IsEmailed=2 and EROTime >= dateadd(dd,-2, getdate())  ";
	public List<UserInfo> getMailUsers();

	String updateSql="update editedreadersopinion set isemailed=1 where  eroid=:eroid";
	
	// fetch breaking news for last 3 hr after given BnewsId for particuler host.
	String getBreakingNewsSql = "select top 10 * from bnews with (nolock) where hostid=:host  and bntype=9 and bninsertdatetime  >= dateadd(hh,-3, getdate()) and bnid > :bnewsid order by bnid desc ";
	
	public boolean updateEmailState(int eroid);
	
	public String getNewsletterCampaignAssociated(int msid, int hostid);
	
	/**
	 * This method fetch breaking news data from JCMS sql server.Breaking News after given BnewsId is feteched.
	 * @param hostid
	 * @param lastBnewsId
	 * @return
	 */
	public List<NewsInfo> getBreakingNewsData(String hostid, int lastBnewsId);
}
