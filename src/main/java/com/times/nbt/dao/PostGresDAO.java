package com.times.nbt.dao;


import java.util.Collection;
import java.util.Map;

import com.times.nbt.model.CampaignMapping;

public interface PostGresDAO {

	public Collection<CampaignMapping> getMapping();
	
}
