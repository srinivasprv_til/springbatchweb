package com.times.nbt.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import com.times.nbt.model.NewsInfo;
import com.times.nbt.model.UserInfo;

public class SqlServerDAOImpl implements SqlServerDAO {

	NamedParameterJdbcTemplate simpleJdbcTemplate;
	//DataSource sqlDataSource;
	

	public SqlServerDAOImpl(DataSource sqlDataSource) {
		super();
		this.simpleJdbcTemplate = new NamedParameterJdbcTemplate(sqlDataSource);
	}

	
	public NamedParameterJdbcTemplate getSimpleJdbcTemplate() {
		return simpleJdbcTemplate;
	}

	public void setSimpleJdbcTemplate(NamedParameterJdbcTemplate simpleJdbcTemplate) {
		this.simpleJdbcTemplate = simpleJdbcTemplate;
	}

	public List<UserInfo> getMailUsers() {
	
		SqlParameterSource namedParameters =  new MapSqlParameterSource();
		String sql= sqlGetUsers;
		ParameterizedRowMapper<UserInfo> rowMapper = new ParameterizedRowMapper<UserInfo>() {
			public UserInfo mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				UserInfo user = new UserInfo();
				user.setCmtMsg(String.valueOf(rs.getString("EROMessage")));
				user.setEroid(rs.getInt("eroid"));
				user.setUserDetails(String.valueOf(rs.getString("EROAltDetails")));
				user.setImsid(rs.getInt("msid"));
				user.setUserEmail(rs.getString("altemailid"));
				user.setUserName(String.valueOf(rs.getString("erouid")));
				return user;
			}
			
		};
		return getSimpleJdbcTemplate().query(sql, namedParameters, rowMapper);
		
	}

	public String getNewsletterCampaignAssociated(int msid, int hostid) {
		// TODO Auto-generated method stub
		
		String sql="select  omdntext from icms..metadata where  msid=:msid and metatype=124";
		SqlParameterSource namedParameters =  new MapSqlParameterSource("msid", msid);
		try {
			return (String) getSimpleJdbcTemplate().queryForObject(sql, namedParameters, String.class);	
		} catch (Exception e) {
			
			return "nbt";
		}
		
		
	}


	public boolean updateEmailState(int eroid) {
		// TODO Auto-generated method stub
		String sql=updateSql;
		SqlParameterSource namedParameters =  new MapSqlParameterSource("eroid", eroid);
		try {
			return (getSimpleJdbcTemplate().update(sql, namedParameters)>0);
			
				
		} catch (Exception e) {
			return false;
		}
	
	}
	
	public List<NewsInfo> getBreakingNewsData(String hostid, int lastBnewsId) {
		String sql= getBreakingNewsSql;

		SqlParameterSource namedParameters =  new MapSqlParameterSource("host", hostid).addValue("bnewsid", lastBnewsId);
		ParameterizedRowMapper<NewsInfo> rowMapper = new ParameterizedRowMapper<NewsInfo>() {
			public NewsInfo mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				NewsInfo newsInfo = new NewsInfo();
				newsInfo.setBnid(Integer.parseInt(rs.getString("bnid")));
				newsInfo.setHostid(Integer.parseInt(rs.getString("hostid")));
				newsInfo.setBntext(String.valueOf(rs.getString("bntext")));
				newsInfo.setBnInsertDateTime(rs.getDate("bnInsertDateTime"));
				newsInfo.setBnUpdateDateTime(rs.getDate("bnUpdateTime"));
				newsInfo.setSubject(rs.getString("bnewsDescXml"));
				return newsInfo;
			}

		};
		return getSimpleJdbcTemplate().query(sql, namedParameters, rowMapper);

	}

	
}
