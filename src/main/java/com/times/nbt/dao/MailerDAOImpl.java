package com.times.nbt.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.times.nbt.model.Newsletter;

public class MailerDAOImpl implements MailerDAO {

	
	NamedParameterJdbcTemplate simpleJDBCTemplate;
	DataSource dataSource;
	

	public MailerDAOImpl(DataSource dataSource) {
		super();
		this.simpleJDBCTemplate = new NamedParameterJdbcTemplate(dataSource);
	}


	public boolean sendMailer(Newsletter mailer) {
		
		
		String sql= "insert into msgq(MsgSenderName,MsgSenderMailID,MsgSubject,MsgToList,Msg, msgContentType) " +
				"values(:MsgSenderName,:MsgSenderMailID,:MsgSubject,:MsgToList,:Msg,'text/html')";
		
		Map<String, Object> mapParams = new HashMap<String, Object>();
		mapParams.put("MsgSenderName", mailer.getMsgSenderName());
		mapParams.put("MsgSenderMailID", mailer.getMsgSenderEmail());
		mapParams.put("MsgSubject", mailer.getMailSubject());
		mapParams.put("MsgToList", mailer.getMsgTo());
		mapParams.put("Msg", mailer.getMailContent());
	
		SqlParameterSource sqlParams = new MapSqlParameterSource(mapParams);
		return (simpleJDBCTemplate.update(sql, sqlParams)>0);
			
		
	}


	
}
