package com.times.nbt.dao;

import com.times.nbt.model.Newsletter;

public interface MailerDAO {

	public boolean sendMailer(Newsletter mailer);
}
