package com.times.nbt;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import com.times.nbt.dao.MailerDAO;
import com.times.nbt.dao.SqlServerDAO;
import com.times.nbt.model.Newsletter;


/**
 * Dummy {@link ItemWriter} which only logs data it receives.
 */

public class NewsletterWriter implements ItemWriter<Newsletter> {

	MailerDAO mailerDao;
	
	
	
	public MailerDAO getMailerDao() {
		return mailerDao;
	}



	public void setMailerDao(MailerDAO mailerDao) {
		this.mailerDao = mailerDao;
	}



	public void write(List<? extends Newsletter> data) throws Exception {
		if (data!=null && data.size()>0){ 
		Newsletter newsletter = data.get(0);
		if (newsletter !=null){
			getMailerDao().sendMailer(newsletter);
		}
		}
	}

}
