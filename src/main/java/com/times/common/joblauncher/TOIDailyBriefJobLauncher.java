package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.client.TOINewsletterEntryPoint;
import com.times.common.util.Constant;
import com.times.common.util.DateUtils;

/**
 * This class is used to launch the toi job at any point of time by hooking with nlID, MorningEve, DailyWeekly.
 * 
 * mvn exec:java -Dexec.mainClass="com.times.common.joblauncher.TOIDailyBriefJobLauncher"
 * 
 * @author Ranjeet.Jha
 *
 */
public class TOIDailyBriefJobLauncher {
	
	private static final Logger logger = LoggerFactory.getLogger(TOIManualJobLauncher.class);

	private static SimpleJobOperator jobOperator = null;
	private static ApplicationContext ctx = null;
	
	public static void main(String[] args) {
		logger.debug("status of job : ");
		ctx = new ClassPathXmlApplicationContext(new String[]{TOINewsletterEntryPoint.LAUNCH_CONTEXT_NAME});
		int iHr = getHour();
		long morningEvening = 2;
		if (iHr < 12) {
			morningEvening = 1;
		}
		new TOIDailyBriefJobLauncher().launchJob("1019", 1, morningEvening);
	}

	public TOIDailyBriefJobLauncher() {
		
	}
	
	
	private static int getHour(){
		String[] hrMin = DateUtils.getHourAndMin(new Date()).split(":");
		String hrs = hrMin[0];
		String min = hrMin[1];
		int ihr = (int)Math.ceil(Double.parseDouble(hrs + "." + min));
		return ihr;
	}
	
	/**
	 * @param newsletterId
	 * @param dailyWeekly
	 * @param morningEvening
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String newsletterId, long dailyWeekly, long morningEvening) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter(Constant.JOB_PARAM_NEWSLETTER_ID, new JobParameter(newsletterId));
		String formatedDate = DateUtils.getformatedDate(null, new Date());
		logger.debug("date : " + formatedDate);
		if (dailyWeekly == 1) {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_DAILY));
		} else {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_WEEKLY));
		}
		
		if (1 == morningEvening) {
			builder.addParameter(Constant.JOB_PARAM_NL_MORNING_EVE_KEY, new JobParameter(Constant.JOB_PARAM_NL_MORNING));
		} else {
			builder.addParameter(Constant.JOB_PARAM_NL_MORNING_EVE_KEY, new JobParameter(Constant.JOB_PARAM_NL_EVENING));
		}
		
		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}
	
	/**
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String newsletterId, long dailyWeekly, long morningEvening) {
		boolean isStatus = false;
		logger.debug("going to launch job for nlid : .................... " + newsletterId);
		
		Job job = null;
		JobLauncher jobLauncher = null;
		try {
			
			if (TOINewsletterEntryPoint.getApplicationContext() == null) {
				TOINewsletterEntryPoint.setApplicationContext( new ClassPathXmlApplicationContext(new String[]{TOINewsletterEntryPoint.LAUNCH_CONTEXT_NAME}));
			}
	    	
			job =(Job) ctx.getBean(TOINewsletterEntryPoint.NEWS_MAILER_JOB_ID);
	    	jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");
			
			JobParametersBuilder builder = getNewsletterJobParameterBuilder(newsletterId, dailyWeekly, morningEvening);
			jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;
			
		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
		} catch (JobRestartException e) {
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}
		
		return isStatus;
	}
	
	/**
	 * @return the jobOperator
	 */
	public static SimpleJobOperator getJobOperator() {
		return jobOperator;
	}

}
