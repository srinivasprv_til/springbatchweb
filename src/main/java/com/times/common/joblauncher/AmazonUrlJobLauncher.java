package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;

/*
 * This job caches the amazon url data in mongo db, for ContentRulesAmazonProductId , of whose
 * meta type is 313. IcmsPrev DB metadata table. 
 * 
 * This job will fetch the amazon product ids from ICMSPrev DB in metadata table with metatype=313.
 * It will iterate over each id and does the following:
 * Reader:- Fetch id from list and create url for amazon.
 * Writer:- Sleep for 1 second (constraint is one hit/second on amazon) , hit the url, Get meta data and save it in MongoDB with id as itemId.
 * And then iterate again.
 * 
 * One thread is also invoked in beforeStep method which fetches the items from db every 10 mins and if any new item is found then , 
 * Reader/Writer processes that itemId.
 * 
 * Currently the above process of iterating over list one time takes around 50 mins(1000 items).
 * This job is stopped via timeout after 2 hrs 58 mins.
 * Another cron job is there which checks at 2nd minute of each hour if the above task is running or not..
 * If running then do nothing else start the task.
 */

public class AmazonUrlJobLauncher {
	
	// -----------------------------------------------------------------------------------------------
	// Start Private members
		
	private static final String JOB_ID = "amazonProductUrlJob";
	
	private static final String APPLICATION_CONTEXT_FILE_NAME = "amazon-jobContext.xml";

	private static final Logger logger = LoggerFactory.getLogger(AmazonUrlJobLauncher.class);
	
	private static ApplicationContext ctx = null;
	
	// End Private members
	// -----------------------------------------------------------------------------------------------
	
	
	// -----------------------------------------------------------------------------------------------
	// Start public methods
	
	public static void main(String[] args) {
		
		ctx = new ClassPathXmlApplicationContext(new String[]{APPLICATION_CONTEXT_FILE_NAME});
		
		new AmazonUrlJobLauncher().launchJob();

	}
	
	public boolean launchJob() {
		
		boolean isStatus = false;
		
		logger.debug("Going to launch job for Amazon Url : .................... ");

		Job job = null;
		
		JobLauncher jobLauncher = null;
		try {

			if (getApplicationContext() == null) {
				
				setApplicationContext( new ClassPathXmlApplicationContext(new String[]{APPLICATION_CONTEXT_FILE_NAME}));
			}

			job =(Job) ctx.getBean(JOB_ID);
			
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");

			JobParametersBuilder builder = getJobParameterBuilder();
			
			jobLauncher.run(job, builder.toJobParameters());
			
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
			
			e.printStackTrace();
			
		} catch (JobRestartException e) {
			
			e.printStackTrace();
			
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
			
		} catch (JobInstanceAlreadyCompleteException e) {
			
			e.printStackTrace();
			
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
			
		} catch (JobParametersInvalidException e) {
			
			e.printStackTrace();
			
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}

		return isStatus;
	}


	// End public methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start private methods
	
	private synchronized JobParametersBuilder getJobParameterBuilder() {
		
		JobParametersBuilder builder = new JobParametersBuilder();
		
		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		
		return builder;
	}
	
	// End private methods
	// -----------------------------------------------------------------------------------------------
	
	// -----------------------------------------------------------------------------------------------
	// Start getters/setters
	
	public static ApplicationContext getApplicationContext() {
		if (ctx == null) {
			ctx = new ClassPathXmlApplicationContext(new String[]{APPLICATION_CONTEXT_FILE_NAME});
		}
		return ctx;
	}
	
	public static void setApplicationContext(ApplicationContext applicationContext) {
		ctx = applicationContext;
	}
	
	// End getters/setters
	// -----------------------------------------------------------------------------------------------
	

}
