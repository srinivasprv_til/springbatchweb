package com.times.common.joblauncher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * this job is to fetch the data from icms db.
 * this will read the individual city guide id from file and get article, poi, guide and slideshow from the db
 * once its done create a file on netstorage.
 * @author Geetanjali
 *
 */
public class EhcacheBatchJobLauncher {
	
	private final static Logger logger = LoggerFactory.getLogger(EhcacheBatchJobLauncher.class);

	public static final String JOB_CONTEXT_NAME = "ehcacheBatch-jobcontext.xml";
	public static final String JOB_ID = "ehcacheBatchJobContext";
	private static ApplicationContext ctx = null;
	

	public static void main(String[] args) {
     ctx = new ClassPathXmlApplicationContext(new String[]{JOB_CONTEXT_NAME});
		
		new EhcacheBatchJobLauncher().launchJob();

	}

	
	
	public boolean launchJob() {
		boolean isStatus = false;
		logger.debug("going to launch ehcacheBatch job");

		Job job = null;
		JobLauncher jobLauncher = null;
		try {

			if (ctx == null) {
				ctx = new ClassPathXmlApplicationContext(new String[]{JOB_CONTEXT_NAME});
			}

			job =(Job) ctx.getBean(JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");
			JobParametersBuilder builder = getFeedsJobParameterBuilder();
			jobLauncher.run(job,builder.toJobParameters());
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
		} catch (JobRestartException e) {
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}

		return isStatus;
	}

	/**
	 * @param hostid
	 * 
	 * @return
	 */
	private synchronized JobParametersBuilder getFeedsJobParameterBuilder() {
		JobParametersBuilder builder = new JobParametersBuilder();
		
		return builder;
	}
}
