/**
 * 
 */
package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;

public class SMSJobLauncher {

	private static final Logger logger = LoggerFactory.getLogger(SMSJobLauncher.class);

	private static SimpleJobOperator jobOperator = null;
	private static ApplicationContext ctx = null;

	public static final String SMS_JOB_CONTEXT_NAME = "sms-JobContext.xml";
	public static final String SMS_JOB_ID = "smsMailerJob";

	/**
	 * default constructor
	 */
	public SMSJobLauncher() {

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String nlid = null;
		//args = new String[]{"1015"};

		String[] paramsArray = null;
		paramsArray = args[0].split(",");
		
		nlid = paramsArray[0];
		

		ctx = new ClassPathXmlApplicationContext(new String[]{SMS_JOB_CONTEXT_NAME});
		new SMSJobLauncher().launchJob(nlid);

	}

	/**
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String newsletterId) {
		boolean isStatus = false;
		logger.debug("going to launch job for nlid : .................... " + newsletterId);

		Job job = null;
		JobLauncher jobLauncher = null;
		try {

			if (ctx == null) {
				ctx = new ClassPathXmlApplicationContext(new String[]{SMS_JOB_CONTEXT_NAME});
			}

			job =(Job) ctx.getBean(SMS_JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");

			JobParametersBuilder builder = getNewsletterJobParameterBuilder(newsletterId);
			jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
			e.printStackTrace();
		} catch (JobRestartException e) {
			e.printStackTrace();
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			e.printStackTrace();
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			e.printStackTrace();
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}

		return isStatus;
	}

	/**
	 * @return the jobOperator
	 */
	public static SimpleJobOperator getJobOperator() {
		return jobOperator;
	}

	/**
	 * @param jobOperator the jobOperator to set
	 */
	public static void setJobOperator(SimpleJobOperator jobOperator) {
		SMSJobLauncher.jobOperator = jobOperator;
	}

	/**
	 * @param newsletterId
	 * @param dailyWeekly
	 * @param morningEvening
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String newsletterId) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter(Constant.JOB_PARAM_NEWSLETTER_ID, new JobParameter(newsletterId));
		String formatedDate = DateUtils.getformatedDate(null, new Date());
		logger.debug("date : " + formatedDate);		

		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}


}
