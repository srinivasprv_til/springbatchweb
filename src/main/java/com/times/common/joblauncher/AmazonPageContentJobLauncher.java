/**
 * 
 */
package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;

/**
 * This job launcher is for page content file Upload into amazon specified location.
 * 
 * @author Rajeev Khatri
 *
 */
public class AmazonPageContentJobLauncher { 

	private static final Logger logger = LoggerFactory.getLogger(AmazonPageContentJobLauncher.class);

	public static JobExecution jobExecution = null;
	private static SimpleJobOperator jobOperator = null;
	private static ApplicationContext ctx = null;

	private static final String AMAZON_PAGE_CONTENT_BATCH_JOB_CONFIG_NAME = "amazonPageContentFileUpload-JobContext.xml";
	private static final String AMAZON_PAGE_CONTENT_FILE_UPLOAD_JOB_ID = "amazonPageContentFileUploadJob";

	/**
	 * default constructor
	 */
	public AmazonPageContentJobLauncher() {
	}

	/**
	 * 
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		ctx = new ClassPathXmlApplicationContext(new String[]{AMAZON_PAGE_CONTENT_BATCH_JOB_CONFIG_NAME});
		int work_type_arg = Constant.WORK_TYPE_PAGE_CONTENT_AMAZON;
		if (args != null && args.length > 0) {
			System.out.println(args[0] + " work type");
			work_type_arg =  Integer.parseInt(args[0]);
		} 
		// first parameter is the job type i.e. 11 for Page Content Upload on amazon.
		new AmazonPageContentJobLauncher().launchJob(String.valueOf(work_type_arg));
	}

	/**
	 * 
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String fileUploadType) {
		boolean isStatus = false;
		logger.debug("Amazon page content launchJob ... " );

		Job job = null;
		JobLauncher jobLauncher = null;
		try {

			if (ctx == null) {
				ctx = new ClassPathXmlApplicationContext(new String[]{AMAZON_PAGE_CONTENT_BATCH_JOB_CONFIG_NAME});
			}

			job =(Job) ctx.getBean(AMAZON_PAGE_CONTENT_FILE_UPLOAD_JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");

			JobParametersBuilder builder = getNewsletterJobParameterBuilder(fileUploadType);
			jobExecution = jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught While launching job for Page Content Upload on amazon, msg : " + e.getMessage());
			new CMSCallExceptions("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		} catch (JobRestartException e) {
			new CMSCallExceptions("JobRestartException exception caught While launching job for Page Content Upload on amazon, msg : " + e.getMessage(),	CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			new CMSCallExceptions("JobInstanceAlreadyCompleteException exception caught While launching job for Page Content Upload on amazon , msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobInstanceAlreadyCompleteException exception caught While launching job for Page Content Upload on amazon , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			new CMSCallExceptions("JobParametersInvalidException exception caught While launching job for Page Content Upload on amazon, msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobParametersInvalidException exception caught While launching job for Page Content Upload on amazon, msg : " + e.getMessage());
		}

		return isStatus;
	}

	/**
	 * @return the jobOperator
	 */
	public static SimpleJobOperator getJobOperator() {
		return jobOperator;
	}

	/**
	 * @param jobOperator the jobOperator to set
	 */
	public void setJobOperator(SimpleJobOperator jobOperator) {
		this.jobOperator = jobOperator;
	}

	/**
	 * This method is used to build the parameter for the job.
	 * 
	 * @param workType
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String type) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter(Constant.JOB_PARAM_JOBTYPE_ID, new JobParameter(type));
		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}
}