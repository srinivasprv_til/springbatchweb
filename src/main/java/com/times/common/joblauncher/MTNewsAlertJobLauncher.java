package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;

/**
 * This launcher class is used for MT Alert job launcher.
 * 
 * @author Ranjeet.Jha
 *
 */
public class MTNewsAlertJobLauncher {

	private static final Logger logger = LoggerFactory.getLogger(MTNewsAlertJobLauncher.class);

	public static final String NEWS_ALERT_JOB_CONTEXT_NAME = "mt-news-alert-JobContext.xml";
	public static final String NEWS_ALERT_JOB_ID = "mtNewsAlertJob";
	private static ApplicationContext ctx = null;
	

	/**
	 * default constructor
	 */
	public MTNewsAlertJobLauncher() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String hostid = null; //155
		String metatype = null;
		String nlid = null;
		args = new String[]{"155,1041,257"}; // new String[]{"53,1051,257"}; for NBT
		String[] paramsArray = null;
		if (args != null) {
			paramsArray = args[0].split(",");
			hostid = paramsArray[0];
			nlid = paramsArray[1];
			metatype = paramsArray[2];
			 
			ctx = new ClassPathXmlApplicationContext(new String[]{NEWS_ALERT_JOB_CONTEXT_NAME});
			
		
			new MTNewsAlertJobLauncher().launchJob(hostid,metatype, nlid);
				
				
			
		}

	}

	/**
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String hostid,String metatype ,String newsletterId) {
		boolean isStatus = false;
		logger.debug("going to launch job for MT news Alert nlid :  " + newsletterId);

		Job job = null;
		JobLauncher jobLauncher = null;
		try {

			if (ctx == null) {
				ctx = new ClassPathXmlApplicationContext(new String[]{NEWS_ALERT_JOB_CONTEXT_NAME});
			}

			job =(Job) ctx.getBean(NEWS_ALERT_JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");

			JobParametersBuilder builder = getNewsletterJobParameterBuilder(hostid,metatype, newsletterId);
			jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
		} catch (JobRestartException e) {
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}

		return isStatus;
	}

	/**
	 * @param newsletterId
	 * @param dailyWeekly
	 * @param morningEvening
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String hostid, String metatype,String newsletterId) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter("hostid", new JobParameter(hostid));
		builder.addParameter("metatype", new JobParameter(metatype));
		builder.addParameter(Constant.JOB_PARAM_NEWSLETTER_ID, new JobParameter(newsletterId));
		builder.addParameter(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO, new JobParameter(new Long(1)));
		String formatedDate = DateUtils.getformatedDate(null, new Date());
		logger.debug("date : " + formatedDate);

		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}
	
	
}
