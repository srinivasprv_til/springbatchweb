package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;

/**
 * 
 * @author Dipali Patidar
 *
 */
public class TimeLineMonthlyJobLauncher {
		
	private static final String JOB_ID = "timeLineMonthlyJob";
	private static final String APPLICATION_CONTEXT_FILE_NAME = "timeLineMonthly-Job-context.xml";
	private static final Logger logger = LoggerFactory.getLogger(TimeLineMonthlyJobLauncher.class);
	private static ApplicationContext ctx = null;
	
	public static void main(String[] args) {
		/*if(args == null || args.length==0){
			args =  new String[]{"1"};
			}*/
		if (args != null) {
			System.out.println();
		}
		ctx = new ClassPathXmlApplicationContext(new String[]{APPLICATION_CONTEXT_FILE_NAME});
		new TimeLineMonthlyJobLauncher().launchJob();
	}
	
	public boolean launchJob() {
		boolean isStatus = false;
		logger.debug("Going to launch job for TimeLineMonthlyJobLauncher : .................... ");

		Job job = null;
		JobLauncher jobLauncher = null;
		try {
			if (getApplicationContext() == null) {
				setApplicationContext( new ClassPathXmlApplicationContext(new String[]{APPLICATION_CONTEXT_FILE_NAME}));
			}
			job =(Job) ctx.getBean(JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");
			JobParametersBuilder builder = getJobParameterBuilder();
			jobLauncher.run(job, builder.toJobParameters());

			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
			e.printStackTrace();
		} catch (JobRestartException e) {
			e.printStackTrace();
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			e.printStackTrace();
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			e.printStackTrace();
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}
		return isStatus;
	}
	
	private synchronized JobParametersBuilder getJobParameterBuilder() {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
	//	builder.addParameter("instanceId:", new JobParameter(instanceIdStr));
		return builder;
	}
	
	public static ApplicationContext getApplicationContext() {
		if (ctx == null) {
			ctx = new ClassPathXmlApplicationContext(new String[]{APPLICATION_CONTEXT_FILE_NAME});
		}
		return ctx;
	}
	
	public static void setApplicationContext(ApplicationContext applicationContext) {
		ctx = applicationContext;
	}
	

}
