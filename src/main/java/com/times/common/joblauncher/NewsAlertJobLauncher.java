/**
 * 
 */
package com.times.common.joblauncher;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.client.NewsAlertEntryPoint;
import com.times.common.util.Constant;
import com.times.common.util.DateUtils;
import com.times.common.util.InMemoryData;
import com.times.mailer.dao.CustomParamDao;
import com.times.mailer.dao.MetaDao;

/**
 * This class is hooked by the {@link NewsAlertEntryPoint}
 * 
 * @author ranjeet.jha
 *
 */
public class NewsAlertJobLauncher {

	private static final Logger logger = LoggerFactory.getLogger(NewsAlertJobLauncher.class);

	private static SimpleJobOperator jobOperator = null;
	private static ApplicationContext ctx = null;
	
	
	private static CustomParamDao customParamDao;	
	private static MetaDao metaDao;

	/**
	 * default constructor
	 */
	public NewsAlertJobLauncher() {

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String hostid = "53"; //155
		String metatype = "257";
		String nlid = null;
		//TODO: for testing only,  add value in the array 
		//args[0] = "1031,2,1";
		args = new String[]{"53,1051,257"}; // for NBT
		//args[0] = "155,1041,257"; // MT
		String[] paramsArray = null;
		if (args != null) {
			paramsArray = args[0].split(",");
			hostid = paramsArray[0];
			nlid = paramsArray[1];
			metatype = paramsArray[2];
			 
			ctx = new ClassPathXmlApplicationContext(new String[]{NewsAlertEntryPoint.NEWS_ALERT_JOB_CONTEXT_NAME});
			
			metaDao =  (MetaDao) ctx.getBean("metaDao");		
			customParamDao =  (CustomParamDao) ctx.getBean("customParamDao");
			boolean isBatchExecutionValid = jobPreExecutor(hostid, metatype, nlid);
			isBatchExecutionValid = true;
			if (isBatchExecutionValid) {
	
				/*if (args != null && args.length > 2) {
					System.out.println(args[0] + " , " + args[1] + " , " + args[2]);
				}*/
				// uncomment these 2 line if more than one newsletter
				/*String[] argsss = args[0].split("[,]");
				new TravelNewsletterJobLauncher().launchJob(argsss[0], Long.parseLong(argsss[1]), Long.parseLong(argsss[2]));*/
	
				new NewsAlertJobLauncher().launchJob(nlid);
				//new NewsAlertJobLauncher().launchJob("1051", Long.parseLong("1"), Long.parseLong("1"));
	
				customParamDao.updateLastExecutionData(hostid, metatype);
			}
		} else {
			logger.debug("Parameter is not valid : " + args[0]);
		}

	}

	/**
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String newsletterId) {
		boolean isStatus = false;
		logger.debug("going to launch job for nlid : .................... " + newsletterId);

		Job job = null;
		JobLauncher jobLauncher = null;
		try {

			if (NewsAlertEntryPoint.getApplicationContext() == null) {
				NewsAlertEntryPoint.setApplicationContext( new ClassPathXmlApplicationContext(new String[]{NewsAlertEntryPoint.NEWS_ALERT_JOB_CONTEXT_NAME}));
			}

			job =(Job) ctx.getBean(NewsAlertEntryPoint.NEWS_ALERT_JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");

			JobParametersBuilder builder = getNewsletterJobParameterBuilder(newsletterId);
			jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
			e.printStackTrace();
		} catch (JobRestartException e) {
			e.printStackTrace();
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			e.printStackTrace();
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			e.printStackTrace();
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}

		return isStatus;
	}

	/**
	 * @return the jobOperator
	 */
	public static SimpleJobOperator getJobOperator() {
		return jobOperator;
	}

	/**
	 * @param jobOperator the jobOperator to set
	 */
	public static void setJobOperator(SimpleJobOperator jobOperator) {
		NewsAlertJobLauncher.jobOperator = jobOperator;
	}

	/**
	 * @param newsletterId
	 * @param dailyWeekly
	 * @param morningEvening
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String newsletterId) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter(Constant.JOB_PARAM_NEWSLETTER_ID, new JobParameter(newsletterId));
		String formatedDate = DateUtils.getformatedDate(null, new Date());
		logger.debug("date : " + formatedDate);
		/*if (dailyWeekly == 1) {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_DAILY));
		} else {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_WEEKLY));
		}

		if (1 == morningEvening) {
			builder.addParameter(Constant.JOB_PARAM_NL_MORNING_EVE_KEY, new JobParameter(Constant.JOB_PARAM_NL_MORNING));
		} else {
			builder.addParameter(Constant.JOB_PARAM_NL_MORNING_EVE_KEY, new JobParameter(Constant.JOB_PARAM_NL_EVENING));
		}*/

		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}
	
	
	/**
	 * This api is used to fetch msid and to check if batch need to execute.
	 * if return false, batch will not execute and LAST_CHECKED_STAMP field of NEWSALERT_BATCH_EXECUTION will set with current time.
	 * if yes, batch will execute and store fetched msids in memory.
	 * @param hostid
	 * @param metatype
	 * @param newsLetterId
	 * @return boolean
	 */
	private static boolean jobPreExecutor(String hostid, String metatype, String newsLetterId) {
		

		Date lastChecked = customParamDao.getLastNewsAlertExecutionData(hostid, metatype);

		if (lastChecked != null) {

			Collection<Map<String, String>> c = metaDao.getMetaData(hostid, metatype, lastChecked);

			if (c != null && c.size() > 0) {
				Iterator<Map<String, String>> it =  c.iterator();

				StringBuilder sb = new StringBuilder("");
				int i = 0;
				while (it.hasNext()) {
					Map obj = it.next();
					System.out.println(obj.get("MSID"));
					if (i == 0) {
						sb.append(obj.get("MSID"));
					} else {
						sb.append(",").append(obj.get("MSID"));
					}
					i++;
				}
				InMemoryData.newsletterContentMap.put(newsLetterId, sb.toString());
				return true;
			} else {
				customParamDao.updateLastCheckedData(hostid, metatype);
				return false;
			}
			
		}

		return false;
	}
}
