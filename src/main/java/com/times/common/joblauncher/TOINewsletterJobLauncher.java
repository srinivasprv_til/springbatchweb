/**
 * 
 */
package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.client.TOINewsletterEntryPoint;
import com.times.common.util.Constant;
import com.times.common.util.DateUtils;

/**
 * This class is used to launch the job by Timer if any scheduled job find.
 * 
 * @author ranjeet.jha
 *
 */
public class TOINewsletterJobLauncher {
	
	private static final Logger logger = LoggerFactory.getLogger(TOINewsletterJobLauncher.class);
	//private static final Log log = LogFactory.getLog(TOINewsletterJobLauncher.class);

	private static TOINewsletterJobLauncher newsletterJobLauncher = null;
	
	private static SimpleJobOperator jobOperator = null;
	private JobLauncher jobLauncher = null;
	private static ApplicationContext ctx = null;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//boolean status = getInstance().launchJob("1001", 1, 2);
		//TODO: for testing only,  add value in the array 
		//args[0] = "1001,2,1"; 
		
		logger.debug("status of job : ");
		if (args != null && args.length > 2) {
			System.out.println(args[0] + " , " + args[1] + " , " + args[2]);
		}
		String[] argsss = args[0].split(",");
		ctx = new ClassPathXmlApplicationContext(new String[]{TOINewsletterEntryPoint.LAUNCH_CONTEXT_NAME});
		
		new TOINewsletterJobLauncher().launchJob(argsss[0], Long.parseLong(argsss[1]), Long.parseLong(argsss[2]));
		
	}

	public TOINewsletterJobLauncher() {
		
	}
	
	public static TOINewsletterJobLauncher getInstance() {
		if (newsletterJobLauncher == null) {
			synchronized (TOINewsletterJobLauncher.class) {
				if (newsletterJobLauncher == null) {
					newsletterJobLauncher = new TOINewsletterJobLauncher();
				}
			}
		}
		return newsletterJobLauncher;
	}
	
	/**
	 * @param newsletterId
	 * @param dailyWeekly
	 * @param morningEvening
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String newsletterId, long dailyWeekly, long morningEvening) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter(Constant.JOB_PARAM_NEWSLETTER_ID, new JobParameter(newsletterId));
		String formatedDate = DateUtils.getformatedDate(null, new Date());
		logger.debug("date : " + formatedDate);
		if (dailyWeekly == 1) {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_DAILY));
		} else {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_WEEKLY));
		}
		
		if (1 == morningEvening) {
			builder.addParameter(Constant.JOB_PARAM_NL_MORNING_EVE_KEY, new JobParameter(Constant.JOB_PARAM_NL_MORNING));
		} else {
			builder.addParameter(Constant.JOB_PARAM_NL_MORNING_EVE_KEY, new JobParameter(Constant.JOB_PARAM_NL_EVENING));
		}
		
		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}
	
	
	
	/**
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String newsletterId, long dailyWeekly, long morningEvening) {
		boolean isStatus = false;
		logger.debug("going to launch job for nlid : .................... " + newsletterId);
		
		Job job = null;
		JobLauncher jobLauncher = null;
		try {
			
			if (TOINewsletterEntryPoint.getApplicationContext() == null) {
				TOINewsletterEntryPoint.setApplicationContext( new ClassPathXmlApplicationContext(new String[]{TOINewsletterEntryPoint.LAUNCH_CONTEXT_NAME}));
			}
	    	
			job =(Job) ctx.getBean(TOINewsletterEntryPoint.NEWS_MAILER_JOB_ID);
	    	jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");
			
			JobParametersBuilder builder = getNewsletterJobParameterBuilder(newsletterId, dailyWeekly, morningEvening);
			jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;
			
		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
			e.printStackTrace();
		} catch (JobRestartException e) {
			e.printStackTrace();
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			e.printStackTrace();
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			e.printStackTrace();
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}
		
		return isStatus;
	}

	/**
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public  boolean launchJob(JobLauncher jobLauncher, Job job, String newsletterId, long dailyWeekly, long morningEvening) {
		boolean isStatus = false;
		logger.debug("going to launch job for nlid : " + newsletterId);
		
		try {
			JobParametersBuilder builder = getNewsletterJobParameterBuilder(newsletterId, dailyWeekly, morningEvening);
			jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;
		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
			e.printStackTrace();
		} catch (JobRestartException e) {
			e.printStackTrace();
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			e.printStackTrace();
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			e.printStackTrace();
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}
		
		return isStatus;
	}

	/**
	 * @return the jobOperator
	 */
	public static SimpleJobOperator getJobOperator() {
		return jobOperator;
	}

	/**
	 * @param jobOperator the jobOperator to set
	 */
	public static void setJobOperator(SimpleJobOperator jobOperator) {
		TOINewsletterJobLauncher.jobOperator = jobOperator;
	}
	
}
