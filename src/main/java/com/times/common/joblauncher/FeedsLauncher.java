/**
 * 
 */
package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;

/**
 * This job launcher is used to launch the job to upload the feeds file in Akamai location.
 * @author aakash.gupta
 *
 */
public class FeedsLauncher {
	private final static Logger logger = LoggerFactory.getLogger(FeedsLauncher.class);

	public static final String FEEDS_JOB_CONTEXT_NAME = "feeds-Config.xml";
	public static final String FEEDS_JOB_ID = "feedsJob";
	private static ApplicationContext ctx = null;
	

	/**
	 * default constructor
	 */
	public FeedsLauncher() {
	}

	
	public static void main(String[] args) {
		logger.debug("nbtFeedsLauncher JOb Started");
		String hostid = null;
		String workType = null;
		String pageNo = null;
		String perPage = null;
		if(args.length == 0)
			args = new String[]{"53,7,0,100"}; // new String[]{"53,1051,257"}; for NBT
		else{
			logger.info("Parameters receive from script is" + args[0]);
		}
		String[] paramsArray = null;
		if (args != null) {
			paramsArray = args[0].split(",");
			if(paramsArray != null){
				hostid = paramsArray[0];
				workType = paramsArray[1];
				if(paramsArray.length>=3)
					pageNo = paramsArray[2];
				else
					pageNo = "0";
				if(paramsArray.length==4)
					perPage = paramsArray[3];
				else 
					perPage = "100";
			}
			ctx = new ClassPathXmlApplicationContext(new String[]{FEEDS_JOB_CONTEXT_NAME});
			new FeedsLauncher().launchJob(hostid,workType,pageNo,perPage);
		}

	}

	/**
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param hostid
	 */
	public boolean launchJob(String hostid, String workType,String pageNo, String perPage) {
		boolean isStatus = false;
		logger.debug("going to launch feeds job for hostId :  " + hostid);

		Job job = null;
		JobLauncher jobLauncher = null;
		try {

			if (ctx == null) {
				ctx = new ClassPathXmlApplicationContext(new String[]{FEEDS_JOB_CONTEXT_NAME});
			}

			job =(Job) ctx.getBean(FEEDS_JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");

			JobParametersBuilder builder = getFeedsJobParameterBuilder(hostid,workType,pageNo,perPage);
			jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
		} catch (JobRestartException e) {
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}

		return isStatus;
	}

	/**
	 * @param hostid
	 * 
	 * @return
	 */
	private synchronized JobParametersBuilder getFeedsJobParameterBuilder(String hostid,String workType,String pageNo, String perPage) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter("hostId", new JobParameter(hostid));
		builder.addParameter("workType",new JobParameter(workType));
		builder.addParameter("pageNo",new JobParameter(pageNo));
		builder.addParameter("perPage",new JobParameter(perPage));
		String formatedDate = DateUtils.getformatedDate(null, new Date());
		logger.debug("date : " + formatedDate);
		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}
	
}
