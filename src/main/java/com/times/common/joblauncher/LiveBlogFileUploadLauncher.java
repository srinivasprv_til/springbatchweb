/**
 * 
 */
package com.times.common.joblauncher;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.batch.reader.LiveBlogFileUploadReader;
import com.times.mailer.dao.CMSDataDao;
import com.times.mailer.dao.CMSDataDaoImpl;

/**
 * This job launcher is used to launch the job to upload the file in Akamai location.
 * 
 * @author Ranjeet.Jha
 *
 */
public class LiveBlogFileUploadLauncher {

	private static final Logger logger = LoggerFactory.getLogger(LiveBlogFileUploadLauncher.class);
	
	public static JobExecution jobExecution = null;
	private static SimpleJobOperator jobOperator = null;
	private static ApplicationContext ctx = null;
	
	private static final String LIVEBLOG_BATCH_JOB_CONFIG_NAME = "liveBlogFileUpload-JobContext.xml";
	private static final String LIVEBLOG_FILE_UPLOAD_JOB_ID = "liveBlogFileUploadJob";
	
	/**
	 * default constructor
	 */
	public LiveBlogFileUploadLauncher() {
	}
	
	/**
	 * This is the entry point of job hooked by shell script.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String msidsStr = "";
		String hostidStr = "";
		ctx = new ClassPathXmlApplicationContext(new String[]{LIVEBLOG_BATCH_JOB_CONFIG_NAME});
		
		CMSDataDao cmsDataDao = ctx.getBean(CMSDataDaoImpl.class);
		
		try {
			if (jobExecution == null || !jobExecution.isRunning() ) {
				for (int hostId : Constant.LIVE_BLOG_ENABLED_HOST_IDS) {
					//System.out.println("hostid: " + hostId);
					List<Map<String, Object>> liveblogList = cmsDataDao.getActiveLiveBlog(hostId, null);
					if (liveblogList != null && liveblogList.size() > 0) {
						for(Map<String, Object> map : liveblogList) {
							
							// if msid and active status then add to run batch job
							if ( map.get("msid") != null && "2".equals(String.valueOf(map.get("active_status")))) {
								pushHostIdAndMsid(hostId , new Integer(String.valueOf(map.get("msid"))));
								hostidStr = hostidStr + hostId + ",";
								msidsStr = msidsStr + String.valueOf(map.get("msid")) + ",";
							}
							logger.debug("hostid : " + map.get("hostid") + " , msid : " + map.get("msid") + " , status : " + map.get("active_status") + " , parentid : " + map.get("parentid"));
						}
					}
				}
				
				//if (LiveBlogFileUploadReader.hostMsidMap.size() > 0) {
					// first parameter is the job type i.e. 4 for LiveBlog file upload job type .
					new LiveBlogFileUploadLauncher().launchJob(String.valueOf(Constant.WORK_TYPE_LIVE_BLOG), msidsStr, hostidStr);
				//}
			}
		} catch (Exception e) {
			new CMSCallExceptions("Exception exception caught While getting Active LiveBlog list for FileUpload, msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("Exception exception caught While getting Active LiveBlog list for FileUpload, msg : " + e.getMessage());
		}
		
		
	}
	
	private static void pushHostIdAndMsid(Integer hostId, int msid) {
		
		if (hostId != 0 && msid != 0) {
			if (LiveBlogFileUploadReader.hostMsidMap.containsKey(hostId) && LiveBlogFileUploadReader.hostMsidMap.get(hostId) != null) {
				LiveBlogFileUploadReader.hostMsidMap.get(hostId).add(msid);
			} else {
				List<Integer> msidList = new ArrayList<Integer>(1);
				msidList.add(msid);
				LiveBlogFileUploadReader.hostMsidMap.put(hostId, msidList);
			}
		}
	}
	
	/**
	 * 
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String fileUploadType, String msidStr, String hostidStr) {
		boolean isStatus = false;
		logger.debug("LiveBlog fileUpload launchJob : .................... " );
		
		Job job = null;
		JobLauncher jobLauncher = null;
		try {
			if (ctx == null) {
				ctx = new ClassPathXmlApplicationContext(new String[]{LIVEBLOG_BATCH_JOB_CONFIG_NAME});
			}
			
			job =(Job) ctx.getBean(LIVEBLOG_FILE_UPLOAD_JOB_ID);
	    	jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");
			
			JobParametersBuilder builder = getNewsletterJobParameterBuilder(fileUploadType, msidStr, hostidStr);
			jobExecution = jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;
			
		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught While launching job for LiveBlog FileUpload context , msg : " + e.getMessage());
			new CMSCallExceptions("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		} catch (JobRestartException e) {
			new CMSCallExceptions("JobRestartException exception caught While launching job for LiveBlog FileUpload context, msg : " + e.getMessage(),	CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			new CMSCallExceptions("JobInstanceAlreadyCompleteException exception caught While launching job LiveBlog FileUpload context , msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobInstanceAlreadyCompleteException exception caught While launching job for LiveBlog FileUpload context , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			new CMSCallExceptions("JobParametersInvalidException exception caught While launching job for LiveBlog FileUpload context, msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobParametersInvalidException exception caught While launching job for LiveBlog FileUpload context, msg : " + e.getMessage());
		}  catch (Exception e) {
			new CMSCallExceptions("Exception  caught While launching job for LiveBlog FileUpload context, msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		}
		
		return isStatus;
	}
	
	/**
	 * @return the jobOperator
	 */
	public static SimpleJobOperator getJobOperator() {
		return jobOperator;
	}
	
	/**
	 * @param jobOperator the jobOperator to set
	 */
	public void setJobOperator(SimpleJobOperator jobOperator) {
		this.jobOperator = jobOperator;
	}
	
	/**
	 * This method is used to build the parameter for the job.
	 * 
	 * @param workType
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String type, String msidStr, String hostidStr) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter(Constant.JOB_PARAM_JOBTYPE_ID, new JobParameter(type));
		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		builder.addParameter("msid:", new JobParameter(msidStr));
		builder.addParameter("hostid:", new JobParameter(hostidStr));
		return builder;
	}

}
