/**
 * 
 */
package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;

/**
 * This class is used to launch the job with the specified parameter.
 * This clas is invoked by Shell script and shell script invoked by CronTab.
 * 
 * @author Ranjeet.Jha
 *
 */
public class TOINetstorageFileUploaderJobLauncher  { 

	private static final Logger logger = LoggerFactory.getLogger(TOINetstorageFileUploaderJobLauncher.class);
	
	private static SimpleJobOperator jobOperator = null;
	private static ApplicationContext ctx = null;
	
	private static final String TOI_LIST_PAGE_BATCH_JOB_CONFIG_NAME = "toiListPageFileUpload-JobContext.xml";
	private static final String TOI_FILE_UPLOAD_JOB_ID = "toiListLevelFileUploadJob";
	
	/**
	 * default constructor
	 */
	public TOINetstorageFileUploaderJobLauncher() {
	
	}
	
	/**
	 * This is the entry point of job hooked by sheel script.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		ctx = new ClassPathXmlApplicationContext(new String[]{TOI_LIST_PAGE_BATCH_JOB_CONFIG_NAME});
				
		// first parameter is the job type i.e. 1 for Akamai file upload job type for list page.
		new TOINetstorageFileUploaderJobLauncher().launchJob("1");
	}
	
	/**
	 * 
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String fileUploadType) {
		boolean isStatus = false;
		logger.debug("going to launch job for akamai netstorage TOI list page : .................... " );
		
		Job job = null;
		JobLauncher jobLauncher = null;
		try {
			
			if (ctx == null) {
				ctx = new ClassPathXmlApplicationContext(new String[]{TOI_LIST_PAGE_BATCH_JOB_CONFIG_NAME});
			}
	    	
			job =(Job) ctx.getBean(TOI_FILE_UPLOAD_JOB_ID);
	    	jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");
			
			JobParametersBuilder builder = getNewsletterJobParameterBuilder(fileUploadType);
			jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;
			
		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
			new CMSCallExceptions("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		} catch (JobRestartException e) {
			new CMSCallExceptions("JobRestartException exception caught , msg : " + e.getMessage(),	CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			new CMSCallExceptions("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			new CMSCallExceptions("JobParametersInvalidException exception caught , msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}
		
		return isStatus;
	}
	
	/**
	 * @return the jobOperator
	 */
	public static SimpleJobOperator getJobOperator() {
		return jobOperator;
	}
	
	/**
	 * @param jobOperator the jobOperator to set
	 */
	public void setJobOperator(SimpleJobOperator jobOperator) {
		this.jobOperator = jobOperator;
	}
	
	/**
	 * This method is used to build the parameter for the job.
	 * 
	 * @param workType
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String type) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter(Constant.JOB_PARAM_JOBTYPE_ID, new JobParameter(type));
		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}
}