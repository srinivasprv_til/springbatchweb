/**
 * 
 */
package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.client.TOINewsletterEntryPoint;
import com.times.common.util.Constant;
import com.times.common.util.DateUtils;

/**
 * This class is used to launch tech weekly newsletter job by Timer if any scheduled job find.
 * 
 * @author Aakash 
 *
 */
public class TOITopTechWeeklyJobLauncher {

	private static final Logger logger = LoggerFactory.getLogger(TOITopTechWeeklyJobLauncher.class);

	private static TOITopTechWeeklyJobLauncher newsletterJobLauncher = null;

	private static SimpleJobOperator jobOperator = null;

	private static ApplicationContext ctx = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		logger.debug("TOIDigestWeekly JOb Started");
		String nlid = null;
		String dailyWeekly = null;
		String morningEvening = null;
		ctx = new ClassPathXmlApplicationContext(new String[]{TOINewsletterEntryPoint.LAUNCH_CONTEXT_NAME});
		if(args.length == 0)
			args = new String[]{"1005,2"}; //1005 : for top tech newsletter
		else{
			logger.info("Parameters receive from script is" + args[0]);
		}
		String[] paramsArray = null;
		if (args != null) {
			paramsArray = args[0].split(",");
			if(paramsArray != null){
				nlid = paramsArray[0];
				dailyWeekly = paramsArray[1];
				morningEvening = getHour() < 12 ? "1" : "2";
			}
			new TOITopTechWeeklyJobLauncher().launchJob(nlid, Long.parseLong(dailyWeekly), Long.parseLong(morningEvening));

		}

	}

	public TOITopTechWeeklyJobLauncher() {

	}


	private static int getHour(){
		String[] hrMin = DateUtils.getHourAndMin(new Date()).split(":");
		String hrs = hrMin[0];
		String min = hrMin[1];
		int ihr = (int)Math.ceil(Double.parseDouble(hrs + "." + min));
		return ihr;
	}

	/**
	 * @param newsletterId
	 * @param dailyWeekly
	 * @param morningEvening
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String newsletterId, long dailyWeekly, long morningEvening) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter(Constant.JOB_PARAM_NEWSLETTER_ID, new JobParameter(newsletterId));
		String formatedDate = DateUtils.getformatedDate(null, new Date());
		logger.debug("date : " + formatedDate);
		if (dailyWeekly == 1) {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_DAILY));
		} else {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_WEEKLY));
		}

		if (1 == morningEvening) {
			builder.addParameter(Constant.JOB_PARAM_NL_MORNING_EVE_KEY, new JobParameter(Constant.JOB_PARAM_NL_MORNING));
		} else {
			builder.addParameter(Constant.JOB_PARAM_NL_MORNING_EVE_KEY, new JobParameter(Constant.JOB_PARAM_NL_EVENING));
		}

		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}



	/**
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String newsletterId, long dailyWeekly, long morningEvening) {
		boolean isStatus = false;
		logger.debug("going to launch job for nlid : .................... " + newsletterId);

		Job job = null;
		JobLauncher jobLauncher = null;
		try {

			if (TOINewsletterEntryPoint.getApplicationContext() == null) {
				TOINewsletterEntryPoint.setApplicationContext( new ClassPathXmlApplicationContext(new String[]{TOINewsletterEntryPoint.LAUNCH_CONTEXT_NAME}));
			}

			job =(Job) ctx.getBean(TOINewsletterEntryPoint.NEWS_MAILER_JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");

			JobParametersBuilder builder = getNewsletterJobParameterBuilder(newsletterId, dailyWeekly, morningEvening);
			jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
			e.printStackTrace();
		} catch (JobRestartException e) {
			e.printStackTrace();
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			e.printStackTrace();
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			e.printStackTrace();
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}

		return isStatus;
	}



}
