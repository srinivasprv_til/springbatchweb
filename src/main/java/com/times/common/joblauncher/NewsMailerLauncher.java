/**
 * 
 */
package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.client.TravelNewsletterEntryPoint;
import com.times.common.util.Constant;
import com.times.common.util.DateUtils;

/**
 * This class is hooked by the {@link TravelNewsletterEntryPoint}
 * 
 * @author Ranjeet.Jha
 *
 */
public class NewsMailerLauncher {

	private static final Logger logger = LoggerFactory.getLogger(NewsMailerLauncher.class);
	private static ApplicationContext ctx = null;
	public static final String MAILER_JOB_CONTEXT_NAME = "NewsMailer-Config.xml";
	public static final String MAILER_JOB_ID = "newsMailerJob";

	/**
	 * default constructor
	 */
	public NewsMailerLauncher() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		logger.debug("NewsMailerLauncher JOb Started");
		String nlid = null;
		String dailyWeekly = null;
		String morningEvening = null;
		ctx = new ClassPathXmlApplicationContext(new String[]{MAILER_JOB_CONTEXT_NAME});
		if(args.length == 0)
			args = new String[]{"37,1,2"}; // new String[]{"53,1051,257"}; for NBT
		else{
			logger.info("Parameters receive from script is" + args[0]);
		}
		String[] paramsArray = null;
		if (args != null) {
			paramsArray = args[0].split(",");
			if(paramsArray != null){
				nlid = paramsArray[0];
				dailyWeekly = paramsArray[1];
				morningEvening = paramsArray[2];
			}
			new NewsMailerLauncher().launchJob(nlid, dailyWeekly, morningEvening);

		}
	}
	/**
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String newsletterId, String dailyWeekly, String morningEvening) {
		boolean isStatus = false;
		logger.debug("going to launch job for nlid : .................... " + newsletterId);

		Job job = null;
		JobLauncher jobLauncher = null;
		try {

			if (ctx == null) {
				ctx =  new ClassPathXmlApplicationContext(new String[]{MAILER_JOB_CONTEXT_NAME});
			}

			job =(Job) ctx.getBean(MAILER_JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");

			JobParametersBuilder builder = getNewsletterJobParameterBuilder(newsletterId, dailyWeekly, morningEvening);
			jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
			e.printStackTrace();
		} catch (JobRestartException e) {
			e.printStackTrace();
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			e.printStackTrace();
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			e.printStackTrace();
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}

		return isStatus;
	}
	/**
	 * @param newsletterId
	 * @param dailyWeekly
	 * @param morningEvening
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String newsletterId, String dailyWeekly, String morningEvening) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter(Constant.JOB_PARAM_NEWSLETTER_ID, new JobParameter(newsletterId));
		String formatedDate = DateUtils.getformatedDate(null, new Date());
		logger.debug("date : " + formatedDate);
		if ("1".equals(dailyWeekly)) {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_DAILY));
		} else {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_WEEKLY));
		}

		if ("1".equals(morningEvening)) {
			builder.addParameter(Constant.JOB_PARAM_NL_MORNING_EVE_KEY, new JobParameter(Constant.JOB_PARAM_NL_MORNING));
		} else {
			builder.addParameter(Constant.JOB_PARAM_NL_MORNING_EVE_KEY, new JobParameter(Constant.JOB_PARAM_NL_EVENING));
		}
		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}

}
