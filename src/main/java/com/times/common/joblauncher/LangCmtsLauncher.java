package com.times.common.joblauncher;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LangCmtsLauncher {
	
	static ClassPathXmlApplicationContext ctx =null;
	
	public static void main(String[] args) {
		
		if (ctx ==null)
			ctx = new ClassPathXmlApplicationContext(new String[]{"LangComment-JobContext.xml"});
		try {
			Job jb=(Job) ctx.getBean("langCommentsJob");
			JobLauncher jbLauncher =(JobLauncher) ctx.getBean("jobLauncher");
			JobParameter jbp = new JobParameter(new Date());
			Map<String, JobParameter> parameters = new HashMap<String, JobParameter>();
			parameters.put("param1", jbp);
			jbLauncher.run(jb, new JobParameters(parameters));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally{
			
		}
	
	}
	
	public void scheduleBatch(){
	if (ctx ==null)
		ctx = new ClassPathXmlApplicationContext(new String[]{"LangComment-JobContext.xml"});
	try {
		Job jb=(Job) ctx.getBean("langCommentsJob");
		JobLauncher jbLauncher =(JobLauncher) ctx.getBean("jobLauncher");
		JobParameter jbp = new JobParameter(new Date());
		Map<String, JobParameter> parameters = new HashMap<String, JobParameter>();
		parameters.put("param1", jbp);
		jbLauncher.run(jb, new JobParameters(parameters));
	} catch (Exception e) {
		System.out.println(e.getMessage());
	} finally{
		
	}
	}
}
	


