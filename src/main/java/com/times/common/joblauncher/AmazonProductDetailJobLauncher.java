package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;

public class AmazonProductDetailJobLauncher {
	
	
	
	public static JobExecution jobExecution = null;
	private static SimpleJobOperator jobOperator = null;
	private static ApplicationContext ctx = null;
	
	private static final String AMAZON_PRODUCT_DETAIL_JOB_CONFIG = "amazon-job-affiliated.xml";
	private static final String AMAZON_PD_JOB_ID = "amazon_product_detail_job_id";
    private static final Logger logger = LoggerFactory.getLogger(AmazonProductDetailJobLauncher.class);
	

public static void main(String[] args) { 
	
	ctx = new ClassPathXmlApplicationContext(new String[]{AMAZON_PRODUCT_DETAIL_JOB_CONFIG});
	
	logger.error("Job for Amazon Affiliate Data Dumping: ENTERED MAIN");
	
	new AmazonProductDetailJobLauncher().launchJob();

}
		


public boolean launchJob() {
	
	boolean isStatus = false;
	
	logger.info("Going to launch job for Amazon Affiliated data Dump: .................... ");

	Job job = null;
	
	JobLauncher jobLauncher = null;
	try {

		if (getApplicationContext() == null) {
			
			setApplicationContext( new ClassPathXmlApplicationContext(new String[]{AMAZON_PRODUCT_DETAIL_JOB_CONFIG}));
		}

		job =(Job) ctx.getBean(AMAZON_PD_JOB_ID);
		
		jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");

		JobParametersBuilder builder = getJobParameterBuilder();
		
		jobLauncher.run(job, builder.toJobParameters());
		
		isStatus = true;

	} catch (JobExecutionAlreadyRunningException e) {
		
		logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
		
		e.printStackTrace();
		
	} catch (JobRestartException e) {
		
		e.printStackTrace();
		
		logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		
	} catch (JobInstanceAlreadyCompleteException e) {
		
		e.printStackTrace();
		
		logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		
	} catch (JobParametersInvalidException e) {
		
		e.printStackTrace();
		
		logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
	}

	return isStatus;
}
private synchronized JobParametersBuilder getJobParameterBuilder() {
	
	JobParametersBuilder builder = new JobParametersBuilder();
	
	builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
	
	return builder;
}


	public static ApplicationContext getApplicationContext() {
		if (ctx == null) {
			ctx = new ClassPathXmlApplicationContext(new String[]{AMAZON_PRODUCT_DETAIL_JOB_CONFIG});
		}
		return ctx;
	}
	
	public static void setApplicationContext(ApplicationContext applicationContext) {
		ctx = applicationContext;
	}
	
	// End getters/setters
	// -----------------------------------------------------------------------------------------------
	


}
