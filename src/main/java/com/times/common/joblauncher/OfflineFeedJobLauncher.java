package com.times.common.joblauncher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * this job is to fetch the data from mongo consumed from /offlineFeed/ api of mycms.
 * this will read the individual records where status is inserted and post the data to relevant api
 * once it done it will change the status and updated 
 * @author ajitkumar
 *
 */
public class OfflineFeedJobLauncher {
	
	private final static Logger logger = LoggerFactory.getLogger(OfflineFeedJobLauncher.class);

	public static final String OFFLINEFEEDS_MERGE_JOB_CONTEXT_NAME = "offlinefeedsmerge-Config.xml";
	public static final String OFFLINEFEEDS_MERGE_JOB_ID = "offlinefeedsmergeJob";
	private static ApplicationContext ctx = null;
	

	public static void main(String[] args) {
     ctx = new ClassPathXmlApplicationContext(new String[]{OFFLINEFEEDS_MERGE_JOB_CONTEXT_NAME});
		
		new OfflineFeedJobLauncher().launchJob();

	}

	
	
	public boolean launchJob() {
		boolean isStatus = false;
		logger.debug("going to launch offlinefeedsmerge job");

		Job job = null;
		JobLauncher jobLauncher = null;
		try {

			if (ctx == null) {
				ctx = new ClassPathXmlApplicationContext(new String[]{OFFLINEFEEDS_MERGE_JOB_CONTEXT_NAME});
			}

			job =(Job) ctx.getBean(OFFLINEFEEDS_MERGE_JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");
			JobParametersBuilder builder = getFeedsJobParameterBuilder();
			jobLauncher.run(job,builder.toJobParameters());
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
		} catch (JobRestartException e) {
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}

		return isStatus;
	}

	/**
	 * @param hostid
	 * 
	 * @return
	 */
	private synchronized JobParametersBuilder getFeedsJobParameterBuilder() {
		JobParametersBuilder builder = new JobParametersBuilder();
		
		return builder;
	}
}
