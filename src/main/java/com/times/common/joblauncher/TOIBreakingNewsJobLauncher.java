/**
 * 
 */
package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;


public class TOIBreakingNewsJobLauncher { 

	private static final Logger logger = LoggerFactory.getLogger(TOIBreakingNewsJobLauncher.class);

	public static JobExecution jobExecution = null;

	private static ApplicationContext ctx = null;

	private static final String TOI_BREAKING_NEWS_BATCH_JOB_CONFIG_NAME = "toiBreakingNews-JobContext.xml";
	private static final String TOI_BREAKING_NEWS_FILE_UPLOAD_JOB_ID = "toiBreakingNewsJob";

	/**
	 * default constructor
	 */
	public TOIBreakingNewsJobLauncher() {
	}

	/**
	 * This is the entry point of job hooked by shell script.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		ctx = new ClassPathXmlApplicationContext(new String[]{TOI_BREAKING_NEWS_BATCH_JOB_CONFIG_NAME});
		
		args =  new String[]{"1016,1,83"}; 

		if (args != null && args.length > 2) {
			System.out.println(args[0] + " , " + args[1]);
		}

		String[] argsss = args[0].split(",");

		new TOIBreakingNewsJobLauncher().launchJob(argsss[0], Long.parseLong(argsss[1]), argsss[2]);
	}

	/**
	 * 
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String newsletterId, long dailyWeekly, String hostid) {
		boolean isStatus = false;
		logger.debug("TOIBreakingNewsJobLauncher launchJob ... " );

		Job job = null;
		JobLauncher jobLauncher = null;
		try {

			if (ctx == null) {
				ctx = new ClassPathXmlApplicationContext(new String[]{TOI_BREAKING_NEWS_BATCH_JOB_CONFIG_NAME});
			}

			job =(Job) ctx.getBean(TOI_BREAKING_NEWS_FILE_UPLOAD_JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");

			JobParametersBuilder builder = getNewsletterJobParameterBuilder(newsletterId, dailyWeekly, hostid);
			jobExecution = jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught While launching job for TOI Breaking News, msg : " + e.getMessage());
			new CMSCallExceptions("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		} catch (JobRestartException e) {
			new CMSCallExceptions("JobRestartException exception caught While launching job for TOI Breaking News, msg : " + e.getMessage(),	CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			new CMSCallExceptions("JobInstanceAlreadyCompleteException exception caught While launching job for TOI Breaking News , msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobInstanceAlreadyCompleteException exception caught While launching job for TOI Breaking News , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			new CMSCallExceptions("JobParametersInvalidException exception caught While launching job for TOI Breaking News, msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
			logger.error("JobParametersInvalidException exception caught While launching job for TOI Breaking News, msg : " + e.getMessage());
		}

		return isStatus;
	}


	/**
	 * This method is used to build the parameter for the job.
	 * 
	 * @param workType
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String newsletterId, long dailyWeekly, String hostid) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter(Constant.JOB_PARAM_NEWSLETTER_ID, new JobParameter(newsletterId));
		String formatedDate = DateUtils.getformatedDate(null, new Date());
		logger.debug("date : " + formatedDate);
		if (dailyWeekly == 1) {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_DAILY));
		} else {
			builder.addParameter(Constant.JOB_PARAM_NL_DAILY_WEEKLY_KEY, new JobParameter(Constant.JOB_PARAM_NL_WEEKLY));
		}
		
		builder.addParameter("hostid", new JobParameter(hostid));
		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}
}