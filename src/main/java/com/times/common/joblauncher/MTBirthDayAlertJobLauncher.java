/**
 * 
 */
package com.times.common.joblauncher;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;

/**
 * @author Ranjeet.Jha
 *
 */
public class MTBirthDayAlertJobLauncher {

	private static final Logger logger = LoggerFactory.getLogger(MTBirthDayAlertJobLauncher.class);

	public static final String BIRTHDAY_ALERT_JOB_CONTEXT_NAME = "mtbirthday-alert-JobContext.xml";
	public static final String BIRTHDAY_ALERT_JOB_ID = "mtbirthDayAlertJob";

	private static ApplicationContext ctx = null;


	/**
	 * default constructor
	 */
	public MTBirthDayAlertJobLauncher() {

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String hostid = "53"; //155
		String metatype = "257";
		String nlid = null;
		//args = new String[]{"53,1052,257"}; // for NBT
		args =  new String[]{"155,1042"}; // MT
		String[] paramsArray = null;
		if (args != null) {
			paramsArray = args[0].split(",");
			hostid = paramsArray[0];
			nlid = paramsArray[1];
			
			ctx = new ClassPathXmlApplicationContext(new String[]{BIRTHDAY_ALERT_JOB_CONTEXT_NAME});

			new MTBirthDayAlertJobLauncher().launchJob(hostid,nlid);

		}

	}

	/**
	 * This method is used to launch the job with required parameters.
	 * 
	 * @param newsletterId
	 */
	public boolean launchJob(String hostid,String newsletterId) {
		boolean isStatus = false;
		logger.debug("going to launch job for nlid : .................... " + newsletterId);

		Job job = null;
		JobLauncher jobLauncher = null;
		try {

			if (ctx == null) {
				ctx = new ClassPathXmlApplicationContext(new String[]{BIRTHDAY_ALERT_JOB_CONTEXT_NAME});
			}

			job =(Job) ctx.getBean(BIRTHDAY_ALERT_JOB_ID);
			jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");

			JobParametersBuilder builder = getNewsletterJobParameterBuilder(hostid,newsletterId);
			jobLauncher.run(job, builder.toJobParameters());
			isStatus = true;

		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException exception caught , msg : " + e.getMessage());
			//e.printStackTrace();
		} catch (JobRestartException e) {
			//e.printStackTrace();
			logger.error("JobRestartException exception caught , msg : " + e.getMessage());
		} catch (JobInstanceAlreadyCompleteException e) {
			//e.printStackTrace();
			logger.error("JobInstanceAlreadyCompleteException exception caught , msg : " + e.getMessage());
		} catch (JobParametersInvalidException e) {
			//e.printStackTrace();
			logger.error("JobParametersInvalidException exception caught , msg : " + e.getMessage());
		}

		return isStatus;
	}

	/**
	 * @param newsletterId
	 * @param dailyWeekly
	 * @param morningEvening
	 * @return
	 */
	private synchronized JobParametersBuilder getNewsletterJobParameterBuilder(String hostid,String newsletterId) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addParameter("hostid", new JobParameter(hostid));
		
		builder.addParameter(Constant.JOB_PARAM_NEWSLETTER_ID, new JobParameter(newsletterId));
		builder.addParameter(Constant.JOB_PARAM_NL_NL_ID_PAGE_NO, new JobParameter(new Long(1)));		
		String formatedDate = DateUtils.getformatedDate(null, new Date());
		logger.debug("date : " + formatedDate);

		builder.addParameter(Constant.JOB_PARAM__NL_DATETIME, new JobParameter(DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date())));
		return builder;
	}

}
