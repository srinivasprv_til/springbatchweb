package com.times.common.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.times.common.constant.AmazonItemFields;

/*
 * Model for AmazonItem
 * Wraps JSONObject Class and gives required property on demand.
 */

public class AmazonItem {

	/** The item json. */
	private JSONObject itemJson;
	


	/**
	 * Instantiates a new amazon item.Parameterized Constructor.
	 *
	 * @param json the json
	 */
	public AmazonItem(JSONObject json){
		super();
		this.itemJson=json;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle(){
		String title=null;
		try{
			title=this.itemJson.getJSONObject(AmazonItemFields.ITEM_ATTRIBUTES).getString(AmazonItemFields.TITLE);
		}catch(JSONException e){
			//TODO log title exception
		}

		return title;
	}

	/**
	 * Gets the list price.
	 * List price is the original undiscounted price, Returns formatted price INR 15,000
	 * @return the list price
	 */
	
	
	public DBObject getListPriceDBObject(){
		JSONObject priceJSON=null;
		try{
			priceJSON=this.itemJson.getJSONObject(AmazonItemFields.ITEM_ATTRIBUTES)
					.getJSONObject(AmazonItemFields.LISTPRICE);
			
			if (priceJSON!=null) {
				DBObject listPrice = (DBObject) JSON.parse(priceJSON.toString());
				return listPrice;
			}
			
		}catch(JSONException e){
			//TODO log title exception
		}

		return null;	
	}
	
	
	public String getListPrice(){
		String listPrice=null;
		try{
			listPrice=this.itemJson.getJSONObject(AmazonItemFields.ITEM_ATTRIBUTES)
					.getJSONObject(AmazonItemFields.LISTPRICE).getString(AmazonItemFields.FORMATTED_PRICE);
		}catch(JSONException e){
			//TODO log title exception
		}

		return listPrice;
		
	}
	
	public Double getListAmount(){
		String amount=null;
		Double doubleAmount=null;
		try{
			amount=this.itemJson.getJSONObject(AmazonItemFields.ITEM_ATTRIBUTES)
					.getJSONObject(AmazonItemFields.LISTPRICE).getString(AmazonItemFields.AMOUNT);
			doubleAmount=Double.parseDouble(amount);
			if(doubleAmount>0){
				return doubleAmount;
			}
		}catch(JSONException e){
			//TODO log price exception
		}

		return null;
		
	}
	
	
	
	public DBObject getLowestNewPriceDBObject(){
		JSONObject priceJSON=null;
		try{
			priceJSON=this.itemJson.getJSONObject(AmazonItemFields.OFFER_SUMMARY).
					getJSONObject(AmazonItemFields.LOWEST_NEW_PRICE);
			
			if (priceJSON!=null) {
				DBObject lowestPrice = (DBObject) JSON.parse(priceJSON.toString());
				return lowestPrice;
			}
			
		}catch(JSONException e){
			//TODO log price exception
		}

		return null;
	}

	/**
	 * Gets the LowestNew Price 
	 * Most discounted Price of 'new' item Example INR 1,100.00
	 * @return the formatted price
	 */
	public String getLowestNewPrice(){
		String fmtPrice=null;
		try{
			fmtPrice=this.itemJson.getJSONObject(AmazonItemFields.OFFER_SUMMARY).
					getJSONObject(AmazonItemFields.LOWEST_NEW_PRICE).getString(AmazonItemFields.FORMATTED_PRICE);
		}catch(JSONException e){
			//TODO log price exception
		}

		return fmtPrice;
	}
	
	public Double getLowestNewAmount(){
		String amount=null;
		Double doubleAmount=null;
		try{
			amount=this.itemJson.getJSONObject(AmazonItemFields.OFFER_SUMMARY).
					getJSONObject(AmazonItemFields.LOWEST_NEW_PRICE).getString(AmazonItemFields.AMOUNT);
			doubleAmount=Double.parseDouble(amount);
			if(doubleAmount>0){
				return doubleAmount;
			}
		}catch(JSONException e){
			//TODO log price exception
		}

		return null;
		
	}
	
	
	public DBObject getLowestUsedPriceDBObject(){
		JSONObject priceJSON=null;
		try{
			priceJSON=this.itemJson.getJSONObject(AmazonItemFields.OFFER_SUMMARY).
					getJSONObject(AmazonItemFields.LOWEST_USED_PRICE);

			if (priceJSON!=null) {
				DBObject lowestPrice = (DBObject) JSON.parse(priceJSON.toString());
				return lowestPrice;
			}

		}catch(JSONException e){
			//TODO log price exception
		}

		return null;
	}


	/**
	 * Gets the asin.Amazon Standard Identification Number 10 digit AlphaNumeric Code
	 * Example: B01MRL7WAV
	 * @return the asin
	 */
	public  String getASIN(){
		String asin=null;
		try{
			asin=this.itemJson.getString(AmazonItemFields.ASIN);
		}catch(JSONException e){
			//TODO log asin not found exception
		}
		return asin;
	}


	/**
	 * Gets the detail page URL.Detail page URL contains associate tag for identification
	 *  Example:https://www.amazon.in/Philips-CTE103BK-94-Feature-Phone-Black/dp/B01CEE2SSG?SubscriptionId=AKIAJX7PDW7DEP2LLJLA&tag=timofind-21&linkCode=xm2&camp=2025&creative=165953&creativeASIN=B01CEE2SSG
	 * @return the detail page URL
	 */
	public String getDetailPageURL(){
		String detailPageUrl=null;
		try{
			detailPageUrl=this.itemJson.getString(AmazonItemFields.DETAIL_PAGE_URL);
		}catch(JSONException e){
			//TODO log asin not found exception
		}
		return detailPageUrl;
	}


	/**
	 * Gets the sales rank.
	 * Sales Rank(As per Amazon,updates hourly)
	 * @return the sales rank
	 */
	public Long getSalesRank(){
		Long salesRank=null;
		try{
			salesRank=this.itemJson.getLong(AmazonItemFields.SALES_RANK);
		}catch(JSONException e){
			//TODO log salesRank not found exception
		}

		return salesRank;
	}


	/**
	 * Gets the brand.
	 * Brand field related to the item
	 * @return the brand
	 */
	public String getBrand(){
		String brand=null;
		try{
			brand=this.itemJson.getJSONObject(AmazonItemFields.ITEM_ATTRIBUTES).getString(AmazonItemFields.BRAND);
		}catch(JSONException e){

		}

		return brand;
	}


	/**
	 * Gets the manufacturer.
	 *	We are using manufacturer field as a one to one mapping with brand
	 * @return the manufacturer
	 */
	public String getManufacturer(){
		String manufacturer=null;

		try{
			manufacturer=this.itemJson.getJSONObject(AmazonItemFields.ITEM_ATTRIBUTES).getString(AmazonItemFields.MANUFACTURER);
		}catch(JSONException e){

		}
		return manufacturer;
	}


	/**
	 * Checks if is eligible for prime.
	 * Checks if a product is prime enabled
	 * @return the int
	 */
	public int IsEligibleForPrime(){
		int isEligibleForPrime=0;
		try{
			isEligibleForPrime=this.itemJson.getJSONObject(AmazonItemFields.OFFERS)
					.getJSONObject(AmazonItemFields.OFFER)
					.getJSONObject(AmazonItemFields.OFFER_LISTING).getInt(AmazonItemFields.IS_ELIGIBLE_FOR_PRIME);
		}catch(JSONException e){

		}
		return isEligibleForPrime;

	}


	/**
	 * Gets the color.
	 * Color of the product, can be used in name matching
	 * @return the color
	 */
	public String getColor(){
		String color=null;
		try{
			color=this.itemJson.getJSONObject(AmazonItemFields.ITEM_ATTRIBUTES).getString(AmazonItemFields.COLOR);
		}catch(JSONException e){

		}

		return color;
	}


	/**
	 * Gets the product type name.
	 * @return the product type name
	 */
	public String getProductTypeName(){
		String productTypeName=null;
		try{
			productTypeName=this.itemJson.getJSONObject(AmazonItemFields.ITEM_ATTRIBUTES)
					.getString(AmazonItemFields.PRODUCT_TYPE_NAME);
		}catch(JSONException e){

		}
		return productTypeName;
	}

	public String getEditorialReviews(){
		String review=null;
		try{
			review=this.itemJson.getJSONObject(AmazonItemFields.EDITORIAL_REVIEWS)
					.getJSONObject(AmazonItemFields.EDITORIAL_REVIEW).getString(AmazonItemFields.CONTENT);
		
		}catch(JSONException e){
			//TODO log price exception
		}

		return review;

	}
	
	public String getBrowseNode(){
		String browseNode=null;
		
		try{
			browseNode=String.valueOf(this.itemJson.getJSONObject(AmazonItemFields.BROWSE_NODES).getJSONArray(AmazonItemFields.BROWSE_NODE).
					getJSONObject(0).get(AmazonItemFields.BROWSE_NODE_ID));
		}catch(JSONException e){
			
		}
		
		try{
			browseNode=String.valueOf(this.itemJson.getJSONObject(AmazonItemFields.BROWSE_NODES).getJSONObject(AmazonItemFields.BROWSE_NODE).
					get(AmazonItemFields.BROWSE_NODE_ID));
		}catch(JSONException e){
			
		}
		
		return browseNode;
	}
	
	public DBObject getSmallImageDBObject(){
		
		try{
			JSONObject smallImageJSON= this.itemJson.getJSONObject(AmazonItemFields.SMALL_IMAGE);
			if (smallImageJSON!=null) {
				
				DBObject smallImage = (DBObject) JSON.parse(smallImageJSON.toString());
				return smallImage;
			}
		}catch(JSONException e){
			
		}
		
		return null;
		
	}
	
	public DBObject toSimpleDBObject(){

		DBObject itemDBObject= new BasicDBObject();
		itemDBObject.put(AmazonItemFields.DB_NAME, this.getTitle());
		
		itemDBObject.put(AmazonItemFields.DB_ASIN, this.getASIN());
		
		itemDBObject.put(AmazonItemFields.DB_DETAIL_PAGE_URL, this.getDetailPageURL());
		
		DBObject lowestPriceDBObject=this.getLowestNewPriceDBObject();
		if(lowestPriceDBObject!=null){
			itemDBObject.put(AmazonItemFields.LOWEST_NEW_PRICE,lowestPriceDBObject);
		}
		
		itemDBObject.put(AmazonItemFields.DB_IS_PRIME, this.IsEligibleForPrime());
		
		DBObject listPriceDBObject=this.getListPriceDBObject();
		if(listPriceDBObject!=null){
			itemDBObject.put(AmazonItemFields.LISTPRICE,listPriceDBObject);
		}
		
		return itemDBObject;
	
		
	}


	/*
	 * Getter and Setters
	 */
	public JSONObject getItemJson() {
		return itemJson;
	}

	public void setItemJson(JSONObject itemJson) {
		this.itemJson = itemJson;
	}

	@Override
	public String toString() {
		return "AmazonItem [itemJson=" + itemJson + "]";
	}



}
