package com.times.common.model;

import java.util.Date;

public class UserComment {
	
	private String commentText;
	private short hostId;
	private int parentmsid;
	private Date commentTime;
	private short langId;
	
	
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public short getHostId() {
		return hostId;
	}
	public void setHostId(short hostId) {
		this.hostId = hostId;
	}
	public int getParentmsid() {
		return parentmsid;
	}
	public void setParentmsid(int parentmsid) {
		this.parentmsid = parentmsid;
	}
	public Date getCommentTime() {
		return commentTime;
	}
	public void setCommentTime(Date commentTime) {
		this.commentTime = commentTime;
	}
	public short getLangId() {
		return langId;
	}
	public void setLangId(short langId) {
		this.langId = langId;
	}
	
	
	 
	
	

}

