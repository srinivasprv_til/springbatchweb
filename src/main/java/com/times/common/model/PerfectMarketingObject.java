package com.times.common.model;
import java.io.Serializable;
import java.util.Date;

public class PerfectMarketingObject implements Serializable,Cloneable {

	private static final long serialVersionUID = -6413203754841412573L;

	private int serialnumber;
	private int msid;
	private String redirecturl;
	private Date dateadded;
	private int noattempt;
	private String extractedfrom;

	public static final String SERIAL_NUMBER = "S_N";
	public static final String MSID = "M_ID";
	public static final String REDIRECT_URL = "R_URL";
	public static final String DATE_ADDED = "C_AT";
	public static final String NO_ATTEMPT = "N_AT";
	public static final String EXT_FROM = "E_FRM";
	
	public String getExtractedfrom() {
		return extractedfrom;
	}
	public void setExtractedfrom(String extractedfrom) {
		this.extractedfrom = extractedfrom;
	}
	
	public int getSerialnumber() {
		return serialnumber;
	}
	public void setSerialnumber(int serialnumber) {
		this.serialnumber = serialnumber;
	}
	public int getMsid() {
		return msid;
	}
	public void setMsid(int msid) {
		this.msid = msid;
	}
	public String getRedirecturl() {
		return redirecturl;
	}
	public void setRedirecturl(String redirecturl) {
		this.redirecturl = redirecturl;
	}
	public Date getDateadded() {
		return dateadded;
	}
	public void setDateadded(Date dateadded) {
		this.dateadded = dateadded;
	}
	public int getNoattempt() {
		return noattempt;
	}
	public void setNoattempt(int noattempt) {
		this.noattempt = noattempt;
	}
	
	public PerfectMarketingObject clone(){
		PerfectMarketingObject obj = new PerfectMarketingObject();
		obj.serialnumber = this.serialnumber;
		obj.msid = this.msid;
		obj.redirecturl = this.redirecturl;
		obj.dateadded = this.dateadded;
		obj.noattempt = this.noattempt;
		return obj;
	}

}
