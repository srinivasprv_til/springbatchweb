package com.times.common.model;

import java.util.Date;

import org.apache.solr.client.solrj.beans.Field;

public class ETEpaperArticle {

	@Field
	String id;
	
	@Field
	String name;
	@Field
	String seq_no;
	
	@Field
	String type;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Field
	String title;
	
	@Field
	String pageTitle;
	
	@Field
	String pageId;
	
	@Field
	String editionTitle;
	
	@Field
	String description;
	
	@Field
	Date edDate;
	
	@Field
	String editionName;
	
	@Field
	String pubid;
	
	@Field
	String image;
	
	@Field
	String author;
	
	@Field
	String source;
	
	@Field
	String location;
	
	@Field
	String pubDate;
	
	@Field
	String guid;
	
	@Field
	String comments;
	
//	@Field
//	Double score;
//	
//	public Double getScore() {
//		return score;
//	}
//
//	public void setScore(Double score) {
//		this.score = score;
//	}
	
	@Field
	String synopsis;
	
	@Field
	String description200;
	
	@Field
	boolean multipublished = false;

	public boolean isMultipublished() {
		return multipublished;
	}

	public void setMultipublished(boolean multipublished) {
		this.multipublished = multipublished;
	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public String getDescription200() {
		return description200;
	}

	public void setDescription200(String description200) {
		this.description200 = description200;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSeq_no() {
		return seq_no;
	}

	public void setSeq_no(String seq_no) {
		this.seq_no = seq_no;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public String getEditionTitle() {
		return editionTitle;
	}

	public void setEditionTitle(String editionTitle) {
		this.editionTitle = editionTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEdDate() {
		return edDate;
	}

	public void setEdDate(Date edDate) {
		this.edDate = edDate;
	}

	public String getEditionName() {
		return editionName;
	}

	public void setEditionName(String editionName) {
		this.editionName = editionName;
	}

	public String getPubid() {
		return pubid;
	}

	public void setPubid(String pubid) {
		this.pubid = pubid;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
