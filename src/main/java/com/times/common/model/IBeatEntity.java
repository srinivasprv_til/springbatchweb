package com.times.common.model;

/**
 * Model class for representing IbeatEntity for Ibeat topicdata api
 * Sample IbeatEntityJSON  --> "56947286": {"Views": 66844,"Comments": 0,"Social": 0}
 * @author daksh.verma
 */
public class IBeatEntity {

	private int msid;

	private long views;

	private long comments;

	private long social;


	//Getters And Setters
	
	public int getMsid() {
		return msid;
	}

	public void setMsid(int msid) {
		this.msid = msid;
	}

	public long getViews() {
		return views;
	}

	public void setViews(long views) {
		this.views = views;
	}

	public long getComments() {
		return comments;
	}

	public void setComments(long comments) {
		this.comments = comments;
	}

	public long getSocial() {
		return social;
	}

	public void setSocial(long social) {
		this.social = social;
	}

	
	//toString
	@Override
	public String toString() {
		return "IBeatEntity [msid=" + msid + ", views=" + views + ", comments=" + comments + ", social=" + social + "]";
	}

	
	//HashCode and Equals
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + msid;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IBeatEntity other = (IBeatEntity) obj;
		if (msid != other.msid)
			return false;
		return true;
	}



}
