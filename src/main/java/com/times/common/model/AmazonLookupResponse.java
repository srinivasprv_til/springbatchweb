package com.times.common.model;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import com.times.common.constant.AmazonResponseFields;

public class AmazonLookupResponse  {
	
	/** The search response. */
	JSONObject lookupResponse;

	/**
	 * Instantiates a new amazon search response.Parameterized Constructor.
	 * @param searchResponse the search response
	 */
	public AmazonLookupResponse(JSONObject searchResponse) {
		super();
		this.lookupResponse = searchResponse;
	}
	
	
	public List<AmazonItem> getItemList(){
		List<AmazonItem> itemList= new ArrayList<AmazonItem>();
		try{
			JSONArray itemArray=null;
			try {
				itemArray = this.lookupResponse.getJSONObject(AmazonResponseFields.ITEM_LOOKUP_RESPONSE)
						.getJSONObject(AmazonResponseFields.ITEMS).getJSONArray(AmazonResponseFields.ITEM);
				int numItems=itemArray.length();
				
				if(numItems>0){
					itemList=new ArrayList<AmazonItem>();
					for (int i=0;i<numItems;++i){
						itemList.add(new AmazonItem(itemArray.getJSONObject(i)));
					}
				}
				
				return itemList;
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			JSONObject singleItem=null;
			try{
				singleItem = this.lookupResponse.getJSONObject(AmazonResponseFields.ITEM_LOOKUP_RESPONSE)
						.getJSONObject(AmazonResponseFields.ITEMS).getJSONObject(AmazonResponseFields.ITEM);
				
				if(singleItem!=null){
					itemList.add(new AmazonItem(singleItem));
				}
				
				return itemList;
				
			}catch(Exception e){
				
			}
			
		}catch(Exception e){
			
		}
		
		return itemList;
	}


	@Override
	public String toString() {
		return "AmazonLookupResponse [lookupResponse=" + lookupResponse + "]";
	}
	
	

		
		

}
