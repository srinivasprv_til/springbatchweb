package com.times.common.model;

import java.sql.Date;

public class TimeLineInstance {
	private String url;
	private int hostId;
	private int instanceId;
	private String campaignId;
	private String lastProcessedDate;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getHostId() {
		return hostId;
	}
	public void setHostId(int hostId) {
		this.hostId = hostId;
	}
	public int getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(int instanceId) {
		this.instanceId = instanceId;
	}
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getLastProcessedDate() {
		return lastProcessedDate;
	}
	public void setLastProcessedDate(String lastProcessedDate) {
		this.lastProcessedDate = lastProcessedDate;
	}
}
