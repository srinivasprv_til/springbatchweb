package com.times.common.model;

public class UserActivity {
 
	private int msid;
	private short wishlist;
	private double userRating;
	private long userUpvote;
	private short beenThere;
	
	private UserComment userComment;
	
	
	
	public int getMsid() {
		return msid;
	}
	public void setMsid(int msid) {
		this.msid = msid;
	}
	public short getWishlist() {
		return wishlist;
	}
	public void setWishlist(short wishlist) {
		this.wishlist = wishlist;
	}
	public double getUserRating() {
		return userRating;
	}
	public void setUserRating(double userRating) {
		this.userRating = userRating;
	}
	public long getUserUpvote() {
		return userUpvote;
	}
	public void setUserUpvote(long userUpvote) {
		this.userUpvote = userUpvote;
	}
	public short getBeenThere() {
		return beenThere;
	}
	public void setBeenThere(short beenThere) {
		this.beenThere = beenThere;
	}
	public UserComment getUserComment() {
		return userComment;
	}
	public void setUserComment(UserComment userComment) {
		this.userComment = userComment;
	} 
	
	
	
	
}
