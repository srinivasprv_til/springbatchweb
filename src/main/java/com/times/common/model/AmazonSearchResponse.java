package com.times.common.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.times.common.constant.AmazonResponseFields;


/**
 * The Class AmazonSearchResponse.
 * Wrapper around JSONObject which contains json response received after hitting Amazon Product Advertisement API
 */
public class AmazonSearchResponse {
	
	/** The search response. */
	JSONObject searchResponse;

	/**
	 * Instantiates a new amazon search response.Parameterized Constructor.
	 *
	 * @param searchResponse the search response
	 */
	public AmazonSearchResponse(JSONObject searchResponse) {
		super();
		this.searchResponse = searchResponse;
	}
	
	/**
	 * Gets the item list.
	 * Returns empty list when no results are found
	 * @return the item list
	 */
	public List<AmazonItem> getItemList(){
		List<AmazonItem> itemList= new ArrayList<AmazonItem>();
		try{
			JSONArray itemArray=null;
			try {
				itemArray = this.searchResponse.getJSONObject(AmazonResponseFields.ITEM_SEARCH_RESPONSE)
						.getJSONObject(AmazonResponseFields.ITEMS).getJSONArray(AmazonResponseFields.ITEM);
				int numItems=itemArray.length();
				
				if(numItems>0){
					itemList=new ArrayList<AmazonItem>();
					for (int i=0;i<numItems;++i){
						itemList.add(new AmazonItem(itemArray.getJSONObject(i)));
					}
				}
				
				return itemList;
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			JSONObject singleItem=null;
			try{
				singleItem = this.searchResponse.getJSONObject(AmazonResponseFields.ITEM_SEARCH_RESPONSE)
						.getJSONObject(AmazonResponseFields.ITEMS).getJSONObject(AmazonResponseFields.ITEM);
				
				if(singleItem!=null){
					itemList.add(new AmazonItem(singleItem));
				}
				
				return itemList;
				
			}catch(Exception e){
				
			}
			
		}catch(Exception e){
			
		}
		
		return itemList;
	}
	
	/**
	 * Checks if is response valid.
	 *
	 * @return the int
	 */
	public boolean isRequestValid(){
		boolean isRequestValid=false;
		
		try{
			isRequestValid=this.searchResponse.getJSONObject(AmazonResponseFields.ITEM_SEARCH_RESPONSE)
							.getJSONObject(AmazonResponseFields.ITEMS).getJSONObject(AmazonResponseFields.REQUEST)
							.getBoolean(AmazonResponseFields.IS_VALID);
			
			return isRequestValid;
			
		}catch(JSONException e){
			//TODO log excetion
		}
		
		return isRequestValid;
	}
	
	/**
	 * Gets the total results.
	 * Total Results for the query tried. 
	 * @return the total results
	 */
	public int getTotalResults(){
		int totalResults=0;
		
		try{
			totalResults=this.searchResponse.getJSONObject(AmazonResponseFields.ITEM_SEARCH_RESPONSE)
						.getJSONObject(AmazonResponseFields.ITEMS).getInt(AmazonResponseFields.TOTAL_RESULTS);
		}catch(JSONException e){
			
		}
		
		return totalResults;
	}
	
	/**
	 * Gets the total pages.
	 * 
	 * @return the total pages
	 */
	public int getTotalPages(){
		int totalPages=0;
		
		try{
			totalPages=this.searchResponse.getJSONObject(AmazonResponseFields.ITEM_SEARCH_RESPONSE)
					.getJSONObject(AmazonResponseFields.ITEMS).getInt(AmazonResponseFields.TOTAL_PAGES);
			
		}catch(JSONException e){
			
		}
		
		return totalPages;
	}
	
	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode(){
		String errorCode=null;
		
		try{
			errorCode=this.searchResponse.getJSONObject(AmazonResponseFields.ITEM_SEARCH_RESPONSE)
					.getJSONObject(AmazonResponseFields.ITEMS).getJSONObject(AmazonResponseFields.REQUEST)
					.getJSONObject(AmazonResponseFields.ERRORS).getJSONObject(AmazonResponseFields.ERROR)
					.getString(AmazonResponseFields.CODE);
		}catch(JSONException e){
			
		}
		
		return errorCode;
	
		/*
		 * Getter and Setters
		 */		

	}

	public JSONObject getSearchResponse() {
		return searchResponse;
	}

	public void setSearchResponse(JSONObject searchResponse) {
		this.searchResponse = searchResponse;
	}

	@Override
	public String toString() {
		return "AmazonSearchResponse [searchResponse=" + searchResponse + "]";
	}
	
	
	
	
	

}
