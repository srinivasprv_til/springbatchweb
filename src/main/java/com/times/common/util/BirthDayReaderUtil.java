package com.times.common.util;

/**
 * 
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 * This class is used as XLS reader for the provided details.
 * 
 * @author Ranjeet.Jha
 * 
 */
public class BirthDayReaderUtil {

	private final static Logger logger = Logger.getLogger(XlsReaderUtil.class);

	/**
	 * creates an {@link HSSFWorkbook} the specified OS filename.
	 */
	public static HSSFWorkbook readFile(InputStream file) throws IOException {

		return new HSSFWorkbook(file);
	}

	public static final String[] UNVRIFICATION_MAIL_TOI = { "USERID", "FIRSTNAME", "LASTNAME", "EMAILID", "DATEOFBIRTH",  "CHANNEL"};



	/**
	 * @param file
	 * @return
	 */
	public static List<Map<String, Object>> getRecordsList(InputStream file) {
		List<Map<String, Object>> list = null;

		try {
			if (file != null ) {
				HSSFWorkbook wb = readFile(file);

				HSSFSheet sheet = wb.getSheetAt(0);
				int rows = sheet.getPhysicalNumberOfRows();

				list = getRows(sheet, rows);
				logger.debug("no of rows : " + rows);
				if (list != null && list.size() > 0) {
					logger.debug("readed... , size : " + list.size());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
	
	
	public static List<Map<String, Object>> getRecordsList(InputStream file, int start, int end) {
		List<Map<String, Object>> list = null;

		try {
			if (file != null ) {
				HSSFWorkbook wb = readFile(file);

				HSSFSheet sheet = wb.getSheetAt(0);
				int rows = sheet.getPhysicalNumberOfRows();

				list = getRows(sheet, rows, start, end);
				logger.debug("no of rows : " + rows);
				if (list != null && list.size() > 0) {
					logger.debug("readed... , size : " + list.size());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	/**
	 * @param file
	 * @return
	 */
	public static HSSFSheet getWorkSheet(InputStream file) {
		HSSFWorkbook hssfWorkbook = null;
		HSSFSheet sheet = null;
		try {
			if (file != null) {
				hssfWorkbook = XlsReaderUtil.readFile(file);
				sheet = hssfWorkbook.getSheetAt(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sheet;
	}

	/**
	 * This method is used to get List of {@link Map<String, String>} by
	 * reading row by row of excel sheet.
	 * 
	 * @param sheet
	 * @return
	 */
	public static List<Map<String, Object>> getRows(HSSFSheet sheet,  int noOfRows) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(noOfRows);

		int rows = sheet.getPhysicalNumberOfRows();

		for (int r = 1; r < rows; r++) {
			HSSFRow row = sheet.getRow(r);
			if (row != null) {
				Map<String, Object> map = getRow(sheet.getRow(r));
				if (isContentInMap(map)) {
					list.add(map);
				}
			} else {
				continue;
			}
		}
		return list;
	}
	
	
	public static List<Map<String, Object>> getRows(HSSFSheet sheet,  int noOfRows, int start, int end ) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(end-start);

		int rows = sheet.getPhysicalNumberOfRows();

		
		for (int r = start + 1; r <= end && r < rows; r++) {
			HSSFRow row = sheet.getRow(r);
			if (row != null) {
				Map<String, Object> map = getRow(sheet.getRow(r));
				if (isContentInMap(map)) {
					list.add(map);
				}
			} else {
				continue;
			}
		}
		return list;
	}



	/**
	 * This method is used to get {@link GadgetTechSpecDetails} for one Row.
	 * 
	 * @param row
	 * @return
	 */
	private static Map<String, Object> getRow(HSSFRow row) {
		Map<String, Object> map = new LinkedHashMap<String, Object>(UNVRIFICATION_MAIL_TOI.length);

		int cells = row.getPhysicalNumberOfCells();

		for (int i = 0; i < UNVRIFICATION_MAIL_TOI.length; i++) {
			map.put(UNVRIFICATION_MAIL_TOI[i], getValueByCell(row.getCell(i)));
		}		

		return map;
	}



	/**
	 * This method read the value from
	 * 
	 * @param cell
	 * @return
	 */
	private static String getValueByCell(HSSFCell cell) {
		String value = null;
		if (cell != null) {
			switch (cell.getCellType()) {

			/*
			 * case HSSFCell.CELL_TYPE_FORMULA: value = cell.getCellFormula();
			 * break;
			 */
			case HSSFCell.CELL_TYPE_STRING:
				value = cell.getStringCellValue();
				break;

			case HSSFCell.CELL_TYPE_NUMERIC:
				value = "" + cell.getNumericCellValue();
				break;

			default:
			}
		}
		return value;
	}

	/**
	 * This method is used to check whether map contains value of not;
	 * 
	 * @param map
	 * @return
	 */
	private static boolean isContentInMap(Map<String, Object> map) {
		boolean status = false;
		try {
			if (map != null) {
				for (int i = 0; i < UNVRIFICATION_MAIL_TOI.length; i++) {
					if (map.get(UNVRIFICATION_MAIL_TOI[i]) != null && !"".equalsIgnoreCase(((String)map.get(UNVRIFICATION_MAIL_TOI[i])).trim())) {
						status = true;
						break;
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return status;
	}
}
