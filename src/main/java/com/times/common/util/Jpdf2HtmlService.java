package com.times.common.util;

import java.io.File;
import java.util.Arrays;

import org.jpedal.examples.html.PDFtoHTML5Converter;
import org.jpedal.exception.PdfException;
import org.jpedal.render.output.IDRViewerOptions;
import org.jpedal.render.output.html.HTMLConversionOptions;
import org.jpedal.render.output.html.HTMLConversionOptions.TextMode;

public class Jpdf2HtmlService {
	public void convertToPdf(byte[] pdfContent, File outputDir) throws PdfException {
		HTMLConversionOptions conversionOptions = new HTMLConversionOptions();// Set
																				// conversion
																				// options
																				// here
		conversionOptions.setDisableComments(true);
		TextMode mode = TextMode.IMAGE_REALTEXT;
		conversionOptions.setTextMode(mode);
		System.out.println("Omit name dir: " + conversionOptions.getOmitNameDir());
		conversionOptions.setOmitNameDir(true);
		IDRViewerOptions viewerOptions = new IDRViewerOptions();// Set viewer
																// options here

		// Alternative constructor exists for converting from byte array
		if (pdfContent != null) {
			System.out.println("pdfContent.length: " + pdfContent.length);
			System.out.println("output dir: " + outputDir);
		} else {
			System.out.println("pdfContent is null");
		}
		PDFtoHTML5Converter converter = new PDFtoHTML5Converter(pdfContent, outputDir, conversionOptions, viewerOptions);
		converter.convert();
	}
}
