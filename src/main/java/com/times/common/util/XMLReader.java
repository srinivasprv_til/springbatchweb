/**
 * 
 */
package com.times.common.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.model.TravelAlertUser;

/**
 * @author ranjeet.jha
 *
 */
public class XMLReader {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static Document load(Reader reader) throws IOException, Exception {
		SAXBuilder builder = new SAXBuilder();
		try {
			// Load XML into JDOM Document
			Document doc = builder.build(reader);
			return doc;
		} catch (JDOMException e) {
			throw new CMSCallExceptions(" JDOMException( IOException ) occured in load for reader: " + reader,
					CMSExceptionConstants.CMS_XML_exception, e);

		} finally {
			builder = null;
		}
	}

	/**
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	public static Document load(InputStream inputStream) throws Exception {
		return load(new InputStreamReader(inputStream, "UTF-8"));
	}

	/**
	 * @param xslString
	 * @return
	 * @throws Exception
	 */
	public static Document loadXml(String xslString) throws Exception {
		ByteArrayInputStream bas = null;
		try {
			bas = new ByteArrayInputStream(xslString.getBytes("UTF-8"));
			return load(bas);
		} finally {
			if (bas != null) {
				try {
					bas.close();
					bas = null;
				} catch (Exception ex) {
				}
			}
		}
	}
	
	
}
