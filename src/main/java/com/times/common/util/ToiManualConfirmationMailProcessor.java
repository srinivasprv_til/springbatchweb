package com.times.common.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import com.times.common.mail.MailSender;

/**
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.ToiManualConfirmationMailProcessor"
 * @author Rajeev.Khatri
 *
 */
public class ToiManualConfirmationMailProcessor {
	
	
	public ToiManualConfirmationMailProcessor() {
		
	}	
	
	
	private final static String subject = "Your TOI newsletter subscription has been activated ";
	public static final String FROM = "The Times Of India <mailerservice@timesofindia.com>";
	public static final String replyBackId = "The Times Of India <mailerservice@timesofindia.com>";
	
	//public static final String host = "cmailer.indiatimes.com";
	public static final String host = "nmailer.indiatimes.com";
	public static final String conentPath = "timesofindia@bounce.indiatimes.com";
	
	public static final String templateURL = "http://timesofindia.indiatimes.com/newsletter_unsubscribe_all.cms";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FileInputStream fis;
		String emaild = null;
		String contentBody = null;
		MailSender mailSender = null;
		try {
			String mailBody = MailerUtil.getDataRecursively(templateURL, 1);
			if (mailBody == null || "".equals(mailBody.trim())) {
				mailBody = MailerUtil.getDataRecursively(templateURL, 1);
			}
			mailSender = new MailSender(host,  FROM,  conentPath,  replyBackId);
			//fis = new FileInputStream("D:/unsubscribed-users.xls");
			
			fis = new FileInputStream("/opt/svn/docs/unsubscribed-users.xls");
			
			List<Map<String, Object>> list = XlsReaderUtil.getRecordsList(fis);
			int i = 1;
			for (Map<String, Object> map : list) {
				emaild = (String)map.get("email");
				
				contentBody = mailBody.replace("#unsubscriptionLink#", "http://timesofindia.indiatimes.com/newsletter/unsubscribetoinl/?eid=" + emaild);
				mailSender.sendMessage((String)map.get("email"), subject , contentBody);
				
				System.out.println(i++ + " , email: " + map.get("email"));
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		

	}

}
