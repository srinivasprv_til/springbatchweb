/**
 * 
 */
package com.times.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import org.jdom.Document;
import org.jdom.Element;
import org.springframework.util.StringUtils;

import com.times.exception.CMSException;
import com.times.mailer.model.TravelAlertUser;

/**
 * This utility class is used to manage the TravelAlert.
 * 
 * @author ranjeet.jha
 *
 */
public class TravelAlertUtil {
	
	private static final Log log = LogFactory.getLog(TravelAlertUtil.class);
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {						
			List<TravelAlertUser> list = pululateDummyAlertList();
			UpdatePreferencesForUsers(list, Constant.SSO_URL_4_PREFERENCES);
			//updateMSID(list);
		
			
			System.out.println("done");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	/**
	 * This API append all emailIds comma separated.
	 * @param travelAlertUserList
	 * @return
	 */
	private static String getEmailIds(List<TravelAlertUser> travelAlertUserList) {
		StringBuffer sb = new StringBuffer("");
		int i = 1;
		if (travelAlertUserList != null && travelAlertUserList.size() > 0) {
			for (TravelAlertUser travelAlertUser : travelAlertUserList) {
				if (travelAlertUser.getEmailId() != null) {
					if (i != 1) {
						sb.append(",");
					}
					sb.append(travelAlertUser.getEmailId());
					i++;
				}
			}
		}
		return sb.toString();
	}
	
	
	
	/**
	 * This API fetch preference for user and set them in emailIdPerferenceMap<email, Map>.
	 * @param travelAlertUserList
	 */
	public static void UpdatePreferencesForUsers(List<TravelAlertUser> travelAlertUserList, String ssoUrl) {

		Map<String, Map<String, String>> emailIdPerferenceMap = new HashMap<String, Map<String, String>>();
		String emailId = null;
		String preferenceContent = null;
		try {
			
			String emailIds = getEmailIds(travelAlertUserList);
			if (emailIds != null && StringUtils.hasText(emailIds)) {
				preferenceContent = initPreferenceContent(emailIds, ssoUrl);
			}
			
			if (preferenceContent != null) {

				JSONObject json = new JSONObject(preferenceContent);
				JSONArray ja = json.getJSONArray("dataList");
				if (ja != null && ja.length() > 0) {
					for (int i = 0; i < ja.length(); i++) {
						Map<String, String> perferenceMap = new HashMap<String, String>();
						JSONObject json1 = ja.getJSONObject(i);
						emailId = json1.getString("emailId");
						if (json1.get("data") != null && StringUtils.hasText(json1.get("data").toString())) {
							JSONObject json2 = new JSONObject(json1.get("data").toString());
	
							Iterator<String> it = json2.keys();
							while(it.hasNext()) {
								String listKey = (String) it.next();
								if (json2.get(listKey) != null && !json2.get(listKey).toString().equals("[]")) {
									if (Arrays.asList(Constant.listTypes).contains(listKey)) {
										perferenceMap.put(listKey, json2.get(listKey).toString().replace("[", "{").replace("]", "}"));
									}
								}
							}
	
							emailIdPerferenceMap.put(emailId, perferenceMap);
						}
					}
				}

				for (TravelAlertUser travelAlertUser : travelAlertUserList) {
					if (travelAlertUser.getEmailId() != null && emailIdPerferenceMap.get(travelAlertUser.getEmailId()) != null) {
						travelAlertUser.setPreferences(emailIdPerferenceMap.get(travelAlertUser.getEmailId()));
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			/* new CMSCallExceptions("Error Occured while getting user preferences for travel alert emailId=" + emailId,
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION);*/
		}
	}
	
	private static String initPreferenceContent(String emailId, String url) {	
		
		if (emailId != null) {
			//String url = Constant.SSO_URL_4_PREFERENCES;
			url = url.replace("#emailids#", emailId);
			return getPreference(url);
		}
		
		return null;
	}

	
	
	/**
	 * This API call SSO_URL_4_PREFERENCES with POST method.
	 * @param url4Json
	 * @return
	 */
	private static String getPreference(String url4Json) {
		String content = MailerUtil.postData(url4Json);
		if (StringUtils.hasText(content)) {
			return content;			
		} else {
			return null;
		}
	}
	
	/**
	 * This method is used to build the query.
	 * 
	 * @param prefrences
	 * @return
	 */
	public static String prepareQuery(String[] prefrences) {
		StringBuilder query = new StringBuilder();
		if (prefrences != null) {
			for (int i = 0; i < prefrences.length; i++) {
				/*if (i == 0) {
					query.append("\"").append(prefrences[i]).append("\"");
				} else {
					query.append(" OR ").append("\"").append(prefrences[i]).append("\"");
				}*/
				
				if (i == 0) {
					query.append("(").append(prefrences[i]).append(")");
				} else {
					query.append(" ").append("(").append(prefrences[i]).append(")");
				}
			}
		}
		return query.toString();
	}
	
	
	public static String prepareQuery(String prefrences) {
		StringBuilder query = new StringBuilder();
		if (prefrences != null) {

			query.append("(").append(prefrences).append(")");

		}
		return query.toString();
	}

	/**
	 * This method is hooked by travelAlertReader class
	 * 
	 * @param travelAlertUserList
	 */
	public static void updateMSID(List<TravelAlertUser> travelAlertUserList) {
		try {
			if (travelAlertUserList != null && travelAlertUserList.size() > 0) {
				int listSize = travelAlertUserList.size();
				for (int listIndex = 0; listIndex < listSize; listIndex++) {
						updateMSID(travelAlertUserList.get(listIndex), travelAlertUserList.get(listIndex).getEmailId(), listIndex);
					}
				}
		} catch (Exception e) {
			new CMSException("Exception caught while updating msid in updateMSID method, msg : " + e.getMessage());
		}
	}
	
	
	/**
	 * @param travelAlertUser
	 * @param emailid
	 * @throws Exception
	 */
	public static void updateMSID(TravelAlertUser travelAlertUser, String emailid, int listIndex) throws Exception {
		// for testing only hardcoded
		//String[] dummyLocations = {"Delhi", "GOA"};
		//String[] dummyActivities = {"THINGS TO DO", "MUMBAI"}; 
		Document solrDoc = null;
		try {
			
			Map<String, String> preferencesMap = travelAlertUser.getPreferences();
			log.debug(emailid + " : " + preferencesMap);
			if (preferencesMap != null && preferencesMap.size() > 0) {
				
				/*
				 * Add searchable key into list and finally will convert into String[] to build the query.
				 */
				List<String> searchInputList = new ArrayList<String>();
				for (Map.Entry<String, String> entry : preferencesMap.entrySet()) {
					//System.out.println(entry.getKey() + " : " + entry.getValue());
					if (entry.getValue() != null) {
						String[] prefSplit = entry.getValue().replace("{", "").replace("}", "").split(",");
						for (String pref : prefSplit) {
							pref = pref.replaceAll("\"", "");
							searchInputList.add(pref);
						}
					}
				}
				
				if (searchInputList != null && searchInputList.size() > 0) {
					String[] searchInputArray = searchInputList.toArray(new String[searchInputList.size()]);
					for (String searchInput : searchInputArray) {
						String alertTextVal = prepareQuery(searchInput);
						String solrQuery = Constant.SOLR_QUERY_TRAVELO;
						solrQuery = solrQuery.replace("#query#", alertTextVal);
						String xmlData = ExternalHttpUtil.getExtDataForSolar(solrQuery);
						if (xmlData != null) {
							solrDoc = XMLReader.loadXml(xmlData);
							
							if (solrDoc != null) {
								updateMSID(travelAlertUser, solrDoc, emailid, searchInput);
							}
						}
					}
				}
			}
			
		} catch (Exception e) {
			new CMSException("Exception caught while calling solr for Travel Search : " + e.getMessage());
		}
	}
	
	/**
	 * @param travelAlertUser
	 * @param solrDoc
	 * @param emailid
	 * @throws Exception
	 */
	public static void updateMSID(TravelAlertUser travelAlertUser, Document solrDoc, String emailid, String searchInput) throws Exception {
		
		Element docElement = null;
		List<Element> documentList = null;
		try {
			if (solrDoc != null) {
				docElement = solrDoc.getRootElement().getChild("Documents");
				if (emailid != null && emailid.equals(travelAlertUser.getEmailId()) && docElement != null) {
					documentList = docElement.getChildren();
					if (documentList != null) {
						Set<Integer> msidsforParticulerPreference = new HashSet<Integer>();
						for (Element e : documentList) {
							Element msidEl = e.getChild("msid");
							msidsforParticulerPreference.add(Integer.parseInt(msidEl.getValue()));
							//travelAlertUser.addMsid(MailerUtil.getInt(msidEl.getValue()));
							
							Element titleNameE = e.getChild("name");
							travelAlertUser.addAlertMap(titleNameE.getName(), titleNameE.getValue());
							 
						}
						travelAlertUser.addPreferenceMsidMap(searchInput, msidsforParticulerPreference);
						
					}
				}
			}
					
		} catch (Exception e) {
			throw e;
		} finally {
			docElement = null;
			documentList = null;
		}
	}

	
	//TODO: this is dummy for testing.
	public static List<TravelAlertUser> pululateDummyAlertList() {
		List<TravelAlertUser> travelAlertUsers = new ArrayList<TravelAlertUser>();
		
		TravelAlertUser  t1 = new TravelAlertUser();
		
		t1.setEmailId("test@gmail.com");
		
		travelAlertUsers.add(t1);
		
		return travelAlertUsers;
	}
	
	
	
	/**
	 * This method is initialize the content piece for travel alert.
	 * 
	 * @param msid
	 * @return
	 */
	private static String initContent(Integer msid) {	
		
		if (msid != null) {
			String url = Constant.MAIN_TEMPLATE_SECTION_URL;
			url = url.replace("#msid", msid.toString());
			return getContent(url);
		}
		
		return null;
	}
	
	/**
	 * This API call Constant.MAIN_TEMPLATE_SECTION_URL for specific msid with GET method.
	 * 
	 * @param url4Json
	 * @return
	 */
	private static String getContent(String url4Json) {
		String content = MailerUtil.getData(url4Json);
		if (StringUtils.hasText(content)) {
			return content;			
		} else {
			return null;
		}
	}
	
	public static void removeIfNoPreference(List<TravelAlertUser> travelAlerts) {
		if (travelAlerts != null && travelAlerts.size() > 0) {
			Iterator<TravelAlertUser> it = travelAlerts.iterator();
			while(it.hasNext()) {
				TravelAlertUser u = it.next();
				if(u.getPreferences() == null) {
					System.out.println("emailId :" + u.getEmailId() + " no preferences");
					it.remove();
				} else {
					System.out.println("emailId :" + u.getEmailId() + " GOT prefrences");
				}
			}
		}
	}
	
	public static String getMSID(String searchInput) throws Exception {

		Document solrDoc = null;
		StringBuilder msids = new StringBuilder();

		Element docElement = null;
		List<Element> documentList = null;

		String alertTextVal = prepareQuery(searchInput);
		String solrQuery = Constant.SOLR_QUERY_TRAVELO;
		solrQuery = solrQuery.replace("#query#", alertTextVal);
		String xmlData = ExternalHttpUtil.getExtDataForSolar(solrQuery);
		if (xmlData != null) {
			solrDoc = XMLReader.loadXml(xmlData);

			if (solrDoc != null) {				
				docElement = solrDoc.getRootElement().getChild("Documents");
				if (docElement != null) {
					documentList = docElement.getChildren();
					if (documentList != null) {
						Set<Integer> msidsforParticulerPreference = new HashSet<Integer>();
						int i = 0;
						for (Element e : documentList) {
							Element msidEl = e.getChild("msid");
							if (i==0) {
								msids.append(msidEl.getValue());
							} else {
								msids.append("," + msidEl.getValue());
							}							
							i++;
						}
						return msids.toString();
					}
				}

			}
		}
		return null;
	}
}
