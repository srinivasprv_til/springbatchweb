package com.times.common.util;

/**
 *	command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.ToiConfirmationUtil"
 * 
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.times.common.dao.MongoRestAPIService;
import com.times.mailer.model.Subscription;


/**
 * This class is used as XLS reader for the provided details.
 * 
 * @author Ranjeet.Jha
 * 
 */
public class ToiConfirmationUtil {

	private final static Logger logger = Logger.getLogger(ToiConfirmationUtil.class);

	/**
	 * creates an {@link HSSFWorkbook} the specified OS filename.
	 */
	public static HSSFWorkbook readFile(InputStream file) throws IOException {

		return new HSSFWorkbook(file);
	}

	public static final String[] UNVRIFICATION_MAIL_TOI = { "email"};
	
	public static void main(String[] args) throws FileNotFoundException {

		//FileInputStream fis = new FileInputStream("D:/unsubscribed-users.xls");
		FileInputStream fis = new FileInputStream("/opt/svn/docs/unsubscribed-users.xls");

		List<Map<String, Object>> list = getRecordsList(fis);
		int i = 1;
		for (Map<String, Object> map : list) {			
			
			updateRecord(map.get("email").toString());
		
			System.out.println();
		}
		
	}
	
	/*private static void getRecord(String email) {
		Mongo mongo = null;
		try {			
		
			BasicDBObject query = new BasicDBObject();		
			query.put("E_ID", email);

			List<DBObject> dbObjectList = MongoRestAPIService.getList("newslettersubscriptiontravel", query);
			if (dbObjectList != null) {
				for (DBObject objDB : dbObjectList) {
					String s1031 = "false";
					String s1031Verification = "false";
					String s1032 ="false";
					String s1032Verification = "false";
					System.out.print("\t");
					System.out.print(objDB.get("E_ID"));
					System.out.print("\t");
					List<DBObject> nlListMap = (List<DBObject>) objDB.get(Subscription.NEWS_LETTER_MAP);
					if (nlListMap != null) {
						for(DBObject db : nlListMap){
							if (db.get("ID") != null) {
								if ("1031".equalsIgnoreCase(db.get("ID").toString())) {
									s1031 = "true";
									s1031Verification = db.get("V_NL").toString();
								} else if ("1032".equalsIgnoreCase(db.get("ID").toString())) {
									s1032 = "true";
									s1032Verification = db.get("V_NL").toString();
								} 
							}
							
						}
						System.out.print(s1031);
						System.out.print("\t");
						System.out.print(s1031Verification);
						System.out.print("\t");
						System.out.print(s1032);
						System.out.print("\t");
						System.out.print(s1032Verification);
					}
					
					
				}				

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mongo != null) {
				mongo.close();
			}
		}
	}*/
	
	
	private static void updateRecord(String email) {
		Mongo mongo = null;
		try {

			BasicDBObject query = new BasicDBObject();		
			query.put("E_ID", email);


			BasicDBObject datatoupdate = new BasicDBObject();					
			datatoupdate.put("V_USR", true);
			datatoupdate.put("ST", "1");

			BasicDBObject newDocument = new BasicDBObject();
			newDocument.append("$set", datatoupdate);

			boolean isUpdate = MongoRestAPIService.update("newslettersubscription", query, newDocument,true);
			System.out.println(" \t email: " + email + "\t isUpdate: " + isUpdate);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mongo != null) {
				mongo.close();
			}
		}
	}


	/**
	 * @param file
	 * @return
	 */
	public static List<Map<String, Object>> getRecordsList(InputStream file) {
		List<Map<String, Object>> gadgetList = null;

		try {
			if (file != null ) {
				HSSFWorkbook wb = XlsReaderUtil.readFile(file);

				HSSFSheet sheet = wb.getSheetAt(0);
				int rows = sheet.getPhysicalNumberOfRows();
				
				gadgetList = getUnverificationRows(sheet, rows);
				logger.debug("no of rows : " + rows);
				if (gadgetList != null && gadgetList.size() > 0) {
					logger.debug("readed... , size : " + gadgetList.size());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return gadgetList;
	}

	/**
	 * @param file
	 * @return
	 */
	public static HSSFSheet getWorkSheet(InputStream file) {
		HSSFWorkbook hssfWorkbook = null;
		HSSFSheet sheet = null;
		try {
			if (file != null) {
				hssfWorkbook = XlsReaderUtil.readFile(file);
				sheet = hssfWorkbook.getSheetAt(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sheet;
	}
	
	/**
	 * This method is used to get List of {@link Map<String, String>} by
	 * reading row by row of excel sheet.
	 * 
	 * @param sheet
	 * @return
	 */
	public static List<Map<String, Object>> getUnverificationRows(HSSFSheet sheet,  int noOfRows) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(noOfRows);

		int rows = sheet.getPhysicalNumberOfRows();
		
		for (int r = 1; r < rows; r++) {
			HSSFRow row = sheet.getRow(r);
			if (row != null) {
				Map<String, Object> map = getUnverificationRow(sheet.getRow(r));
				if (isContentInMap(map)) {
					list.add(map);
				}
			} else {
				continue;
			}
		}
		return list;
	}


	
	/**
	 * This method is used to get {@link GadgetTechSpecDetails} for one Row.
	 * 
	 * @param row
	 * @return
	 */
	private static Map<String, Object> getUnverificationRow(HSSFRow row) {
		Map<String, Object> map = new LinkedHashMap<String, Object>(UNVRIFICATION_MAIL_TOI.length);
				
		int cells = row.getPhysicalNumberOfCells();
		
		for (int i = 0; i < UNVRIFICATION_MAIL_TOI.length; i++) {
			map.put(UNVRIFICATION_MAIL_TOI[i], getValueByCell(row.getCell(i)));
		}
		 
		
		return map;
	}



	/**
	 * This method read the value from
	 * 
	 * @param cell
	 * @return
	 */
	private static String getValueByCell(HSSFCell cell) {
		String value = null;
		if (cell != null) {
			switch (cell.getCellType()) {

			/*
			 * case HSSFCell.CELL_TYPE_FORMULA: value = cell.getCellFormula();
			 * break;
			 */
			case HSSFCell.CELL_TYPE_STRING:
				value = cell.getStringCellValue();
				break;

			case HSSFCell.CELL_TYPE_NUMERIC:
				value = "" + cell.getNumericCellValue();
				break;

			default:
			}
		}
		return value;
	}

	/**
	 * This method is used to check whether map contains value of not;
	 * 
	 * @param map
	 * @return
	 */
	private static boolean isContentInMap(Map<String, Object> map) {
		boolean status = false;
		try {
			if (map != null) {
				for (int i = 0; i < UNVRIFICATION_MAIL_TOI.length; i++) {
					if (map.get(UNVRIFICATION_MAIL_TOI[i]) != null && !"".equalsIgnoreCase(((String)map.get(UNVRIFICATION_MAIL_TOI[i])).trim())) {
						status = true;
						break;
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return status;
	}
}
