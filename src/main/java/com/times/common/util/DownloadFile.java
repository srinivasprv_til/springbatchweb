package com.times.common.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;

public class DownloadFile {
	public static void main(String[] args) throws IOException {
		DBObject query = new BasicDBObject();
		query.put("_id", new ObjectId("580f192f40d54ee55cdfa91a"));
		byte[] fileContent = MongoRestAPIService.getFile(Constant.EPAPER_FILE_COLLECTION, query);
		Files.write(Paths.get("/Users/arun.sharma2/580f192f40d54ee55cdfa91a.pdf"), fileContent, StandardOpenOption.CREATE);
	}

}
