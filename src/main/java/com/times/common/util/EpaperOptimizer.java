package com.times.common.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.zeroturnaround.zip.ZipUtil;

import com.mongodb.BasicDBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.mailer.model.EpaperFileDetails;

import com.til.jcms.woff.Woff;

public class EpaperOptimizer {
	static final String collection = "epaperfilesTest";
	
	String webPPath;
	
	/**
	 * @return
	 * @throws Exception 
	 */
	public File optimize(EpaperFileDetails fileDetails) throws Exception {
	
		String pdfFileName = fileDetails.getFileName();
		
		// Save zip to local disk
		Path tempZipFile = Files.createTempFile("epaper_temp_file", ".zip");
		saveFile(tempZipFile.toFile(), fileDetails);
		
		return optimizeZip(pdfFileName, tempZipFile);
		
	}

	private File optimizeZip(String pdfFileName, Path tempZipFile) throws Exception {
		// Extract zip to direcotry
		Path targetDir = Files.createTempDirectory("epaper_temp_dirs");
		extractZip(tempZipFile.toFile(), targetDir.toFile());
		
		// Delete pdf
		deletePdf(targetDir);
		
		// Get all images in folder
		List<Path> images = getImages(targetDir);
		
		// Image processing
		convertToWebp(images);
		deleteOldImages(images);
		
		String htmlFileName = pdfFileName.replace(".zip", ".html");
		
		changeSrc(images, targetDir, htmlFileName);
		
		List<Path> fonts = getFonts(targetDir);
		
		convertToTTF(fonts);
		
		deleteOldFonts(fonts);
		
		changeSrcForFonts(fonts, targetDir);
		
		Path finalZipFile = Files.createTempFile("epaper_temp_files", ".zip");
		
		ZipUtil.pack(targetDir.toFile(), finalZipFile.toFile());
		
		return finalZipFile.toFile();
	}
	
	private void changeSrcForFonts(List<Path> fonts, Path targetDir) throws IOException {
		String cssFileName = "main.css";
		Path cssFile = Paths.get(targetDir.toString(), cssFileName);
	
		String htmlText = readFile(cssFile, Charset.forName("UTF-8"));
		htmlText = htmlText.replaceAll("\\.woff", ".ttf");
		htmlText = htmlText.replaceAll("\\\"woff\\\"", "\"ttf\"");
		OpenOption options = StandardOpenOption.WRITE;
		Files.write(cssFile, htmlText.getBytes(), options);
	}
		
	
	private void deleteOldFonts(List<Path> fonts) throws IOException {
			for (Path font : fonts ) {
				Files.delete(font);
			}
	}

	private void convertToTTF(List<Path> fonts) throws Exception {
		for (Path font : fonts) {
			Path outputFile = Paths.get(font.toString().replace(".woff", ".ttf"));
			byte[] woffData = Files.readAllBytes(font);
			byte[] ttfData = Woff.woffDecode(woffData);
			Files.write(outputFile, ttfData, StandardOpenOption.CREATE);
		}
	}

	private List<Path> getFonts(Path targetDir) throws IOException {
		List<Path> fonts = Files.walk(targetDir).filter(this::isFontFile).collect(Collectors.toList());
		return fonts;
	}

	private void deleteOldImages(List<Path> images) throws IOException {
		for(Path image : images) {
			Files.delete(image);
		}
	}

	public void saveFile(File outputFile, EpaperFileDetails fileDetails) throws IOException {
		
		if(!outputFile.exists())
			outputFile.createNewFile();
		
		FileOutputStream fos = new FileOutputStream(outputFile);
		
		IOUtils.write(fileDetails.getZipFileContent(), fos);
		fos.close();
	}
	
	public void extractZip(File zipFile, File dir) {
		ZipUtils zip = new ZipUtils();
		zip.unZip(zipFile, dir);
	}
	
	public static void main(String[] args) throws Exception {
		(new EpaperOptimizer()).test();
	}
	
	public void deletePdf(Path dir) throws IOException {
		Stream<Path> paths = Files.walk(dir);
		List<Path> pdfs = paths.filter(path -> path.toString().toLowerCase().endsWith(".pdf")).collect(Collectors.toList());
  
		for(Path pdf : pdfs) {
			Files.delete(pdf);
		}
		
		paths.close();
	}
	
	public void test() throws Exception {
//		String id = "56c6f3480cf28d83b1f64a87"; //ObjectId("56c6f3480cf28d83b1f64a87")
//		String collection = "epaperfilesTest";
//		String zipPath = "/Users/arun.sharma2/epaperTesting/03022016_nbtnd_mp_02_1_col_r2.html.zip";
//		File zipFile = new File(zipPath);
//		String dirPath = "/Users/arun.sharma2/epaperTesting/testdir";
//		File outputDir = new File(dirPath);
//		
//		//this.saveFile(id, collection, outputFile);
//		this.extractZip(zipFile, outputDir);
//		
//		Path targetDir = Paths.get(outputDir.getPath());
//		deletePdf(targetDir);
//		
//		
//		
//		List<Path> images = getImages(targetDir);
//		
//		convertToWebp(images);
//		
//		changeSrc(images, targetDir, "hehe");
//		
//		
//		
		
		String pdfFileName = "03022016_nbtnd_mp_02_1_col_r2.zip";
		Path tempZipFile = Paths.get("/Users/arun.sharma2/epaperTesting/03022016_nbtnd_mp_02_1_col_r2.zip");
		optimizeZip(pdfFileName, tempZipFile);
		
	}

	private List<Path> getImages(Path targetDir) throws IOException {
		List<Path> images = Files.walk(targetDir).filter(this::isImageFile).collect(Collectors.toList());
		return images;
	}
	
	static String readFile(Path file, Charset encoding) throws IOException {
	  byte[] encoded = Files.readAllBytes(file);
	  return new String(encoded, encoding);
	}

	private void changeSrc(List<Path> images, Path targetDir, String htmlFileName) throws IOException {
		Path htmlFile = Paths.get(targetDir.toString(), htmlFileName);
		
		String htmlText = readFile(htmlFile, Charset.forName("UTF-8"));
		String processedText = htmlText;
		for(Path image : images) {
			String name = image.getFileName().toString();
			String webpFileName = webpFileName(image.getFileName());
			System.out.println(String.format("changing image url from [%s] to [%s]", getImageTokenInHtml(name), getImageTokenInHtml(webpFileName)));
			processedText = processedText.replaceAll(getImageTokenInHtml(name), getImageTokenInHtml(webpFileName));
		}
		
		OpenOption options = StandardOpenOption.WRITE;
 		Files.write(htmlFile, processedText.getBytes(), options);
	}

	private String getImageTokenInHtml(String name) {
		return String.format("src=\"%s\"", name);
	}

	
	
	private void convertToWebp(List<Path> images) throws IOException, InterruptedException {
		Runtime r = Runtime.getRuntime();
		for (Path image : images) {
			Process p = r.exec(getWebpCommand(image));
			int status = p.waitFor();
			if (status != 0) throw new IOException();
		}
	}
	
	private String getWebpCommand(Path image) {
		System.out.println(image);
		String cmd = webPPath + " " + image.toString() + " -o " + webpFileName(image);
		System.out.println("webp command : " + cmd);
		return cmd;
	}

	private String webpFileName(Path image) {
		return image.toString().replace(".png", ".webp");
	}

	public boolean isImageFile(Path p) {
		String lowerCaseFileName = p.toString().toLowerCase();
		return (lowerCaseFileName.endsWith("jpg") || lowerCaseFileName.endsWith("png") || lowerCaseFileName.endsWith("jpeg"));
		
	}
	
	public boolean isFontFile(Path p) {
		String lowerCaseFileName = p.toString().toLowerCase();
		return lowerCaseFileName.endsWith(".woff");
		
	}

	public String getWebPPath() {
		return webPPath;
	}

	public void setWebPPath(String webPPath) {
		this.webPPath = webPPath;
	}

}
