package com.times.common.util;

/**
 * 
 */

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.times.common.dao.MongoRestAPIService;
import com.times.mailer.model.Subscription;


/**
 * 
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.AdageVerificationSubsUtil"
 * @author Rajeev Khatri
 * 
 */
public class AdageVerificationSubsUtil {
	

	private final static Logger logger = Logger.getLogger(AdageVerificationSubsUtil.class);
	public static void main(String[] args) throws FileNotFoundException {	
		
		fetchRecord();
	}
	
	

	private static void fetchRecord() {		

		Mongo mongo = null;
		try {			

			int perPage = 50;
			int pageNo = 1;
			int offset = 0;
			BasicDBObject filter = new BasicDBObject();
			filter.put("ID", "1017");
			filter.append("V_NL", false);

			DBObject query = new BasicDBObject("$elemMatch", filter);	

			BasicDBObject finalQuery = new BasicDBObject();
			finalQuery.put("NL_MAP", query);


			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", "0");

			int loop = 100;
			int count = 1;
			for (int i =1; i <= loop; i++)  {

				List<DBObject> dbObjectList = MongoRestAPIService.getList("newslettersubscription", finalQuery , optionMap);		

				if (dbObjectList != null) {
					for (DBObject objDB : dbObjectList) {

						List<DBObject> nlListMap = (List<DBObject>) objDB.get(Subscription.NEWS_LETTER_MAP);
						if (nlListMap != null) {
							for(DBObject db : nlListMap){
								if (db.get("ID") != null && db.get("ID").toString().equalsIgnoreCase("1017") && db.get("V_NL") != null 
										&& db.get("V_NL").toString().equalsIgnoreCase("false") && objDB.get("E_ID") != null 
										) {							


									String emaild = (String) objDB.get("E_ID");
									String hashcode = (String) db.get("H_CODE");


									BasicDBObject query1 = new BasicDBObject();		
									query1.put("E_ID", emaild);
									query1.put("NL_MAP.ID", "1017");

									BasicDBObject datatoupdate = new BasicDBObject();					
									datatoupdate.put("NL_MAP.$.V_NL", true);

									BasicDBObject newDocument = new BasicDBObject();
									newDocument.append("$set", datatoupdate);
									MongoRestAPIService.update("newslettersubscription", query1, newDocument,true);
									System.out.println(count++ + " , email verified : " + emaild);
								}

							}
						}


					}				

				}
				/*offset = offset + perPage;
				optionMap.put("skip", String.valueOf(offset));*/
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mongo != null) {
				mongo.close();
			}
		}
	}




}
