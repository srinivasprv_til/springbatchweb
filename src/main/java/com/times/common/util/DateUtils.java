/**
 * 
 */
package com.times.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author ranjeet.jha
 *
 */
public class DateUtils {


	public static Date getISTDate(Date dbDate, int hr, int min) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dbDate);
		cal.add(Calendar.HOUR, hr);
		cal.add(Calendar.MINUTE, min);
		return cal.getTime();
	}
	
	public static Date getISTDate(String hr_min) {
		Calendar cal = Calendar.getInstance();
		//cal.setTime(dbDate);
		String hrmn = getHourAndMin(new Date());
		if (hrmn != null) {
			String[] hrmmArray = hrmn.split("[:]");
			cal.add(Calendar.HOUR, StringUtils.getIntValue(hrmmArray[0]));
			cal.add(Calendar.MINUTE, StringUtils.getIntValue(hrmmArray[1]));
		} else {
			/*cal.add(Calendar.HOUR, hr);
			cal.add(Calendar.MINUTE, min);*/
		}
		return cal.getTime();
	}
	
	/**
	 * @param cal
	 * @param dbValue
	 * @return
	 */
	public static long getDiffInMinute(Calendar cal, Calendar dbValue) {
		Calendar now = Calendar.getInstance();
		
		//now.setTime(dateStr.getTime());
		now.set(Calendar.YEAR, cal.get(Calendar.YEAR));
		now.set(Calendar.DATE, cal.get(Calendar.DATE));
		now.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		now.set(Calendar.HOUR, dbValue.get(Calendar.HOUR));
		now.set(Calendar.MINUTE, dbValue.get(Calendar.MINUTE));
		//logger.debug("now : " + now.getTime() + " cal : " + cal.getTime());
		long diff = now.getTime().getTime() - cal.getTime().getTime();
		long diffMinutes = diff / (60 * 1000);
		return diffMinutes;
	}
	
	
	/**
	 * This method is used to set the hour and minute in current system date.
	 * and return the updated date object of java.util.Date
	 * 
	 * @param hr - hour
	 * @param minute
	 * @return
	 */
	public static Date getDate(int hr, int minute) {
		Calendar now = Calendar.getInstance();
		now.set(Calendar.HOUR, hr);
		now.set(Calendar.MINUTE, minute);
		return now.getTime();
	}
	
	/**
	 * @param cal
	 * @param dbValue
	 * @return
	 */
	public static long getDiffInMinute(Date sysDate, int hr, int min) {
		
		Calendar now = Calendar.getInstance();
		now.setTime(sysDate);
		
		Calendar dbCal = Calendar.getInstance();
		dbCal.set(Calendar.HOUR, hr);
		dbCal.set(Calendar.MINUTE, min);
		
		long diff = dbCal.getTime().getTime() - sysDate.getTime();
		long diffMinutes = diff / (60 * 1000);
		return diffMinutes;
	}
	
	/**
	 * get minute differences 
	 * 
	 * @param now
	 * @param entity
	 */
	public static long getMinuteDiff(Date now, String hh_mm) {
		long minDiff = 0;
		try {
			if(hh_mm != null) {
				String[] morningTimes = String.valueOf(hh_mm).split("[,]");
				int hour = StringUtils.getIntValue(morningTimes[0]);
				int minute = StringUtils.getIntValue(morningTimes[0]);
				minDiff = DateUtils.getDiffInMinute(now, hour, minute);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return minDiff;
	}
	
	public static Date getStrTime(String hrs, String min) {
		Calendar cal = Calendar.getInstance();
		cal.set(2012, 9, 1, Integer.parseInt(hrs), Integer.parseInt(min));
		cal.setTimeZone(TimeZone.getTimeZone("GMT"));
		return cal.getTime();
	}
	
	/**
	 * This method is  used to get the Hr and min with colon  seperated in 2 digit both.
	 * 
	 * @param date
	 * @return a string containing  HH:mm
	 */
	public static String getHourAndMin(Date date) {
       SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
       String hrMin = displayFormat.format(date);
       return hrMin;
	}
	
	/*public static Date startOfMonth(Date date) {
		Calendar calendar = new GregorianCalendar(getYear(date), getMonth(date) - 1, 1, 0, 0, 0);
		return calendar.getTime();
	}

	public static Date startOfWeek(Date dateTime) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(dateTime);
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		day = (day - 2) * (24 * 60 * 60 * 1000);
		dateTime = new Date(dateTime.getTime() - day);
		Calendar resultCalendar = new GregorianCalendar(getYear(dateTime), getMonth(dateTime) - 1, getDay(dateTime), 0,
				0, 0);
		return resultCalendar.getTime();
	}

	public static Date startOfDay(Date dateTime) {
		Calendar calendar = new GregorianCalendar(getYear(dateTime), getMonth(dateTime) - 1, getDay(dateTime), 0, 0, 0);
		return calendar.getTime();
	}

	public static Date startOfHour(Date dateTime) {
		Calendar calendar = new GregorianCalendar(getYear(dateTime), getMonth(dateTime) - 1, getDay(dateTime),
				getHour(dateTime), 0, 0);
		return calendar.getTime();
	}

	public static int getMonth(Date date) {
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("MM");
		return Integer.parseInt(dateFormat1.format(date));
	}

	public static int getYear(Date date) {
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy");
		return Integer.parseInt(dateFormat1.format(date));
	}

	public static int getDay(Date date) {
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd");
		return Integer.parseInt(dateFormat1.format(date));
	}
*/
	public static int getWeek(Date date) {
		Calendar calendar = Calendar.getInstance();
		int week = calendar.get(Calendar.DAY_OF_WEEK);
		return week;
	}

	public static int getHour(Date date) {
		SimpleDateFormat dateFormat1 = new SimpleDateFormat("HH");
		return Integer.parseInt(dateFormat1.format(date));
	}
	
	public static int diffInMinutes(Date sysDate, Date dbDate) {
		int mm = 0;
		try {
			
		} catch (Exception e) {
		}
		return mm;
	}
	
	public static int getMinute(Date date) {
		/*SimpleDateFormat dateFormat1 = new SimpleDateFormat("mm");
		return Integer.parseInt(dateFormat1.format(date));*/
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int mm = cal.get(Calendar.MINUTE);
		
		return mm;
			
		
	}

	public static Date getDate(String year, String month, String date) {
		String dateString = date + "-" + month + "-" + year;
		Date resultDate = convertStringToDate(dateString, "dd-MM-yyyy");
		return resultDate;
	}

	public static Date convertStringToDate(String date, String format) {
		SimpleDateFormat dateFormat1 = new SimpleDateFormat(format);
		Date dateNew = null;
		try {
			dateNew = dateFormat1.parse(date);
		} catch (ParseException e) {
			/*CMSCallExceptions tmperr = new CMSCallExceptions("Error Occured in Convert String to Date: " + date
					+ " dateTimeFormat is: " + format + " Error Details: " + e.getMessage(),
					CMSExceptionConstants.CMSDateTimeFormat, e);
			tmperr = null;*/
		}
		return dateNew;
	}

	/**
	 * This function takes the input numberOfDays, it adds numberOfDays to
	 * current date and returns the new Date
	 * 
	 * @throws ParseException
	 */
	public static String addDaysToDate(int daysToAdd) throws ParseException {
		String strDate = null;
		try {
			SimpleDateFormat lv_formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss");
			strDate = lv_formatter.format(new Date());
			Date parsedDate = lv_formatter.parse(strDate);
			Calendar now = Calendar.getInstance();
			now.setTime(parsedDate);
			now.add(Calendar.DAY_OF_MONTH, daysToAdd);
			strDate = lv_formatter.format(now.getTime());
			lv_formatter = null;
		} catch (Exception e) {
		/*	CMSCallExceptions tmperr = new CMSCallExceptions("Error Occured in addDaysToDateError Details: "
					+ e.getMessage(), CMSExceptionConstants.CMSDateTimeFormat, e);
			tmperr = null;*/
		}
		return strDate;
	}

	/**
	 * This function takes the input numberOfDays, it adds numberOfDays to date
	 * and returns the new Date
	 * 
	 * @throws ParseException
	 */
	public static Date addDaysToDate(int daysToAdd, Date date) throws ParseException {
		Date strDate = null;
		try {
			Calendar now = Calendar.getInstance();
			now.clear();
			now.setTime(date);
			now.add(Calendar.DAY_OF_MONTH, daysToAdd);
			strDate = now.getTime();
		} catch (Exception e) {
			
		}
		return strDate;
	}

	public static Date addMinutesToDate(int minutesToAdd) {
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		now.add(Calendar.MINUTE, minutesToAdd);
		return now.getTime();
	}

	public static Date addMinutesToDate(Date date, int minutesToAdd) {
		Calendar now = Calendar.getInstance();
		now.setTime(date);
		now.add(Calendar.MINUTE, minutesToAdd);
		return now.getTime();
	}

	public static Date addHrMinutesToDate(Date date, int hr, int minutes) {
		Calendar now = Calendar.getInstance();
		now.setTime(date);
		now.add(Calendar.HOUR, hr);
		now.add(Calendar.MINUTE, minutes);
		return now.getTime();
	}
	
	public static Date addSecondsToDate(int secondsToAdd) {
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		now.add(Calendar.SECOND, secondsToAdd);
		return now.getTime();
	}

	/**
	 * This method is used to get the formatedDate.
	 * 
	 * @param format
	 * @param date
	 * @return
	 */
	public static String getformatedDate(String format, Date date) {
		String formatedDate = null;
		SimpleDateFormat dateFormat = null;
		try {
			if (format != null) {
				dateFormat = new SimpleDateFormat(format);
			} else {
				dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			}
			formatedDate = dateFormat.format(date);
		} catch (Exception e) {
			/*new CMSCallExceptions("Error Occured in Date time Formatting date: " + date	+ " dateTimeFormat is: " 
					+ format + " Error Details: " + e.getMessage(),	CMSExceptionConstants.CMSDateTimeFormat, e);*/
		} finally {
			dateFormat = null;
		}
		return formatedDate;
	}
	
	/*public static Date getISTDate(Date dbDate, int hr, int min) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dbDate);
		cal.add(Calendar.HOUR, hr);
		cal.add(Calendar.MINUTE, min);
		return cal.getTime();
	}
	*//**
	 * @param cal
	 * @param dbValue
	 * @return
	 *//*
	public static getDiffInMinute(Calendar cal, Calendar dbValue) {
		Calendar now = Calendar.getInstance();
		
		//now.setTime(dateStr.getTime());
		now.set(Calendar.YEAR, cal.get(Calendar.YEAR));
		now.set(Calendar.DATE, cal.get(Calendar.DATE));
		now.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		now.set(Calendar.HOUR, dbValue.get(Calendar.HOUR));
		now.set(Calendar.MINUTE, dbValue.get(Calendar.MINUTE));
		//logger.debug("now : " + now.getTime() + " cal : " + cal.getTime());
		long diff = now.getTime().getTime() - cal.getTime().getTime();
		long diffMinutes = diff / (60 * 1000);
		return diffMinutes;
	}*/
	
	

	public static String convertToISOString(Date date) {
		String isoDateFormatPattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
		String dateString = "";
		if (date != null) {
			SimpleDateFormat formatter = new SimpleDateFormat(isoDateFormatPattern);
			dateString = formatter.format(date);
		}
		return dateString;

	}
}
