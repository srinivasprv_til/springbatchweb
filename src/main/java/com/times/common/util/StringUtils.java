package com.times.common.util;

public class StringUtils {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static int getMaxCharLen(String[] sectionNames) {
		
		int charCount = 0;
		int tempCharCount = 0;
		if (sectionNames != null) {
			for (String sec : sectionNames) {
				tempCharCount = sec.length();
				if (charCount < tempCharCount) {
					charCount = tempCharCount;
				}
			}
		}
		return charCount;
	}
	
	public static int getNoOfColInRow(String[] secArray) {
		int n = 3;
		int maxChar = getMaxCharLen(secArray);
		if (maxChar < 90) {
			n = 4;
		}
		return n;
		
	}
	
	/*public static int getColSpanForLastTd(String[] sections, int noOfCol, int exactTdNo) {
		int num = 0;
		if (noOfCol < exactTdNo) {
			num = 1;
		} else if ((sections.length - exactTdNo) > noOfCol) {
			num = 1;
		} else if((sections.length - exactTdNo) < noOfCol && (sections.length - exactTdNo) == 0) {
			int n = (sections.length - exactTdNo) ;
			if (n == 0) {
				num = (n + 1);
			}
		} else {
			num = 1;
		}
		return num;
	}*/

	private static String getTableWidget(String[] idArray, String[] sNavArray) {
		StringBuilder sb = new StringBuilder();
		//String[] idArray = "-2128936835,1898055,2647163,296589292,1081479906,-2128672765,4719148,5880659,913168846,000,-2128838597,-2128839596,-2128833038,-2128816011,2950623,-2128821153,3947060,4118235,10744190,-2128816762,123,12".split("[,]");
		//String[] sNavArray = "India|Business".split("[|]");
		int ctr = 1;
		int noOfCol = 4; 
		int iCount = 0;
		noOfCol = StringUtils.getNoOfColInRow(sNavArray);
		int totalRow =(int) Math.ceil((double)sNavArray.length/(double)noOfCol);
		sb.append("<table style='float: left;list-style-type: none;margin: 0;padding: 0;width:479px;background-color:#353535'>");
		int colSpan = (totalRow*noOfCol - sNavArray.length) ;
		for(int row=0 ; row<totalRow; ++row){
			sb.append("<tr>");
			
			for(int col = 0; col < noOfCol && iCount < sNavArray.length  ; col++){
				boolean colSpanApplicable = false;
				if(totalRow -1 == row && iCount ==  sNavArray.length - 1){
					colSpanApplicable = true;
				}
				if(col != 0){
					sb.append("<td ").append(" style='width:100px;padding-top:8px;height:26px;text-align: center;color:#0050a2;font-size:12px;font-family:Georgia;border-left:1px solid #fff'>");
				} else {
						sb.append("<td ").append(" style='width:100px;padding-top:8px;height:26px;text-align:center;color:#0050a2;font-size:12px;font-family:Georgia;'>");
				}
				if(colSpanApplicable){
					sb.append("<a style='color:#ffffff' href='").append(idArray[iCount]).append("' />").append(sNavArray[iCount]).append("</a>");
					sb.append("</td>");
					sb.append("<td ").append(" colspan=").append("'"+colSpan+"'>");
					sb.append("</td>");
					iCount++;
					continue;
				}
			
				sb.append("<a style='color:#ffffff' href='").append(idArray[iCount]).append("' />").append(sNavArray[iCount]).append("</a>");
				sb.append("</td>");
				++iCount;
			}
			sb.append("</tr>");
		}
		sb.append("</table>");
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	public static int getIntValue(String input) {
		int i = 0;
		try {
			i = Integer.parseInt(input);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return i;
	}
	
	public static String replaceSpecialChar(String str) {

		String strRTF = str;
		strRTF = strRTF.replaceAll((char) 146 + "" + "", (char) 39 + "");
		strRTF = strRTF.replaceAll((char) 127 + "", " ");
		strRTF = strRTF.replaceAll((char) 160 + "", "&#160;");
		strRTF = strRTF.replaceAll((char) 161 + "", "i");
		strRTF = strRTF.replaceAll((char) 162 + "", "&#162;");
		strRTF = strRTF.replaceAll((char) 163 + "", "&#163;");
		strRTF = strRTF.replaceAll((char) 164 + "", "");
		strRTF = strRTF.replaceAll((char) 165 + "", "&#165;");
		strRTF = strRTF.replaceAll((char) 166 + "", "&#166;");
		strRTF = strRTF.replaceAll((char) 167 + "", "");
		strRTF = strRTF.replaceAll((char) 168 + "", (char) 34 + "");
		strRTF = strRTF.replaceAll((char) 169 + "", "&#169;");
		strRTF = strRTF.replaceAll((char) 170 + "", (char) 97 + "");
		strRTF = strRTF.replaceAll((char) 171 + "", "&#171;");
		strRTF = strRTF.replaceAll((char) 172 + "", "");
		strRTF = strRTF.replaceAll((char) 173 + "", (char) 45 + "");
		strRTF = strRTF.replaceAll((char) 174 + "", "&#174;");
		strRTF = strRTF.replaceAll((char) 175 + "", "&#175;");
		strRTF = strRTF.replaceAll((char) 176 + "", "&#176;");
		strRTF = strRTF.replaceAll((char) 177 + "", "&#177;");
		strRTF = strRTF.replaceAll((char) 178 + "", "&#178;");
		strRTF = strRTF.replaceAll((char) 179 + "", "&#179;");
		strRTF = strRTF.replaceAll((char) 180 + "", (char) 39 + "");
		strRTF = strRTF.replaceAll((char) 181 + "", "");
		strRTF = strRTF.replaceAll((char) 182 + "", "");
		strRTF = strRTF.replaceAll((char) 183 + "", "&#183;");
		strRTF = strRTF.replaceAll((char) 184 + "", "");
		strRTF = strRTF.replaceAll((char) 185 + "", "&#185;");
		strRTF = strRTF.replaceAll((char) 186 + "", (char) 111 + "");
		strRTF = strRTF.replaceAll((char) 187 + "", "&#187;");
		strRTF = strRTF.replaceAll((char) 188 + "", "&#188;");
		strRTF = strRTF.replaceAll((char) 189 + "", "&#189;");
		strRTF = strRTF.replaceAll((char) 190 + "", "&#190;");
		strRTF = strRTF.replaceAll((char) 191 + "", "");
		strRTF = strRTF.replaceAll((char) 192 + "", (char) 65 + "");
		strRTF = strRTF.replaceAll((char) 193 + "", (char) 65 + "");
		strRTF = strRTF.replaceAll((char) 194 + "", (char) 65 + "");
		strRTF = strRTF.replaceAll((char) 195 + "", (char) 65 + "");
		strRTF = strRTF.replaceAll((char) 196 + "", (char) 65 + "");
		strRTF = strRTF.replaceAll((char) 197 + "", (char) 65 + "");
		strRTF = strRTF.replaceAll((char) 198 + "", (char) 65 + (char) 69 + "");
		strRTF = strRTF.replaceAll((char) 199 + "", (char) 67 + "");
		strRTF = strRTF.replaceAll((char) 200 + "", (char) 69 + "");
		strRTF = strRTF.replaceAll((char) 201 + "", (char) 69 + "");
		strRTF = strRTF.replaceAll((char) 202 + "", (char) 69 + "");
		strRTF = strRTF.replaceAll((char) 203 + "", (char) 69 + "");
		strRTF = strRTF.replaceAll((char) 204 + "", (char) 73 + "");
		strRTF = strRTF.replaceAll((char) 205 + "", (char) 73 + "");
		strRTF = strRTF.replaceAll((char) 206 + "", (char) 73 + "");
		strRTF = strRTF.replaceAll((char) 207 + "", (char) 73 + "");
		strRTF = strRTF.replaceAll((char) 208 + "", (char) 68 + "");
		strRTF = strRTF.replaceAll((char) 209 + "", (char) 78 + "");
		strRTF = strRTF.replaceAll((char) 210 + "", (char) 79 + "");
		strRTF = strRTF.replaceAll((char) 211 + "", (char) 79 + "");
		strRTF = strRTF.replaceAll((char) 212 + "", (char) 79 + "");
		strRTF = strRTF.replaceAll((char) 213 + "", (char) 79 + "");
		strRTF = strRTF.replaceAll((char) 214 + "", (char) 79 + "");
		strRTF = strRTF.replaceAll((char) 215 + "", (char) 120 + "");
		strRTF = strRTF.replaceAll((char) 216 + "", (char) 79 + "");
		strRTF = strRTF.replaceAll((char) 217 + "", (char) 85 + "");
		strRTF = strRTF.replaceAll((char) 218 + "", (char) 85 + "");
		strRTF = strRTF.replaceAll((char) 219 + "", (char) 85 + "");
		strRTF = strRTF.replaceAll((char) 220 + "", (char) 85 + "");
		strRTF = strRTF.replaceAll((char) 221 + "", (char) 89 + "");
		strRTF = strRTF.replaceAll((char) 222 + "", "");
		strRTF = strRTF.replaceAll((char) 223 + "", "&#223;");
		strRTF = strRTF.replaceAll((char) 224 + "", (char) 97 + "");
		strRTF = strRTF.replaceAll((char) 225 + "", (char) 97 + "");
		strRTF = strRTF.replaceAll((char) 226 + "", (char) 97 + "");
		strRTF = strRTF.replaceAll((char) 227 + "", (char) 97 + "");
		strRTF = strRTF.replaceAll((char) 228 + "", (char) 97 + "");
		strRTF = strRTF.replaceAll((char) 229 + "", (char) 97 + "");
		strRTF = strRTF.replaceAll((char) 230 + "", (char) 97 + (char) 101 + "");
		strRTF = strRTF.replaceAll((char) 231 + "", (char) 99 + "");
		strRTF = strRTF.replaceAll((char) 232 + "", (char) 101 + "");
		strRTF = strRTF.replaceAll((char) 233 + "", (char) 101 + "");
		strRTF = strRTF.replaceAll((char) 234 + "", (char) 101 + "");
		strRTF = strRTF.replaceAll((char) 235 + "", (char) 101 + "");
		strRTF = strRTF.replaceAll((char) 236 + "", (char) 105 + "");
		strRTF = strRTF.replaceAll((char) 237 + "", (char) 105 + "");
		strRTF = strRTF.replaceAll((char) 238 + "", (char) 105 + "");
		strRTF = strRTF.replaceAll((char) 239 + "", (char) 105 + "");
		strRTF = strRTF.replaceAll((char) 240 + "", (char) 111 + "");
		strRTF = strRTF.replaceAll((char) 241 + "", (char) 110 + "");
		strRTF = strRTF.replaceAll((char) 242 + "", (char) 111 + "");
		strRTF = strRTF.replaceAll((char) 243 + "", (char) 111 + "");
		strRTF = strRTF.replaceAll((char) 244 + "", (char) 111 + "");
		strRTF = strRTF.replaceAll((char) 245 + "", (char) 111 + "");
		strRTF = strRTF.replaceAll((char) 246 + "", (char) 111 + "");
		strRTF = strRTF.replaceAll((char) 247 + "", "&#247;");
		strRTF = strRTF.replaceAll((char) 248 + "", (char) 111 + "");
		strRTF = strRTF.replaceAll((char) 249 + "", (char) 117 + "");
		strRTF = strRTF.replaceAll((char) 250 + "", (char) 117 + "");
		strRTF = strRTF.replaceAll((char) 251 + "", (char) 117 + "");
		strRTF = strRTF.replaceAll((char) 252 + "", (char) 117 + "");
		strRTF = strRTF.replaceAll((char) 253 + "", (char) 121 + "");
		strRTF = strRTF.replaceAll((char) 254 + "", (char) 112 + "");
		strRTF = strRTF.replaceAll((char) 255 + "", (char) 121 + "");
		strRTF = strRTF.replaceAll((char) 8211 + "", (char) 45 + "");
		//strRTF = strRTF.replaceAll((char) 8212 + "", (char) 45 + "");
		strRTF = strRTF.replaceAll((char) 8216 + "", (char) 39 + "");
		strRTF = strRTF.replaceAll((char) 8217 + "", (char) 39 + "");
		strRTF = strRTF.replaceAll((char) 8218 + "", (char) 44 + "");
		strRTF = strRTF.replaceAll((char) 8220 + "", (char) 34 + "");
		strRTF = strRTF.replaceAll((char) 8221 + "", (char) 34 + "");
		strRTF = strRTF.replaceAll((char) 8222 + "", (char) 44 + "");
		strRTF = strRTF.replaceAll((char) 8230 + "", "...");
		strRTF = strRTF.replaceAll("&#8211;" + "", (char) 45 + "");
		//strRTF = strRTF.replaceAll("&#8212;" + "", (char) 45 + "");
		strRTF = strRTF.replaceAll("&#8216;" + "", (char) 39 + "");
		strRTF = strRTF.replaceAll("&#8217;" + "", (char) 39 + "");
		strRTF = strRTF.replaceAll("&#8218;" + "", (char) 44 + "");
		strRTF = strRTF.replaceAll("&#8220;" + "", (char) 34 + "");
		strRTF = strRTF.replaceAll("&#8221;" + "", (char) 34 + "");
		strRTF = strRTF.replaceAll("&#8222;" + "", (char) 44 + "");

		strRTF = strRTF.replaceAll("&nbsp;" + "", "&#160;");
		strRTF = strRTF.replaceAll("&iexcl;" + "", "&#161;");
		strRTF = strRTF.replaceAll("&cent;" + "", "&#162;");
		strRTF = strRTF.replaceAll("&pound;" + "", "&#163;");
		strRTF = strRTF.replaceAll("&curren;" + "", "&#164;");
		strRTF = strRTF.replaceAll("&yen;" + "", "&#165;");
		strRTF = strRTF.replaceAll("&brvbar;" + "", "&#166;");
		strRTF = strRTF.replaceAll("&sect;" + "", "&#167;");
		strRTF = strRTF.replaceAll("&uml;" + "", "&#168;");
		strRTF = strRTF.replaceAll("&copy;" + "", "&#169;");
		strRTF = strRTF.replaceAll("&ordf;" + "", "&#170");
		strRTF = strRTF.replaceAll("&laquo;" + "", "&#171;");
		strRTF = strRTF.replaceAll("&not;" + "", "&#172;");
		strRTF = strRTF.replaceAll("&shy;" + "", "&#173;");
		strRTF = strRTF.replaceAll("&reg;" + "", "&#174;");
		strRTF = strRTF.replaceAll("&macr;" + "", "&#175;");
		strRTF = strRTF.replaceAll("&deg;" + "", "&#176;");
		strRTF = strRTF.replaceAll("&plusmn;" + "", "&#177;");
		strRTF = strRTF.replaceAll("&ordf;" + "", "&#178;");
		strRTF = strRTF.replaceAll("&sup3;" + "", "&#179;");
		strRTF = strRTF.replaceAll("&acute;" + "", "&#180;");
		strRTF = strRTF.replaceAll("&micro;" + "", "&#181;");
		strRTF = strRTF.replaceAll("&para;" + "", "&#182;");
		strRTF = strRTF.replaceAll("&middot;" + "", "&#183;");
		strRTF = strRTF.replaceAll("&cedil;" + "", "&#184;");
		strRTF = strRTF.replaceAll("&sup1;" + "", "&#185;");
		strRTF = strRTF.replaceAll("&ordm;" + "", "&#186;");
		strRTF = strRTF.replaceAll("&raquo;" + "", "&#187;");
		strRTF = strRTF.replaceAll("&frac14;" + "", "&#188;");
		strRTF = strRTF.replaceAll("&frac12;" + "", "&#189;");
		strRTF = strRTF.replaceAll("&frac34;" + "", "&#190;");
		strRTF = strRTF.replaceAll("&iquest;" + "", "&#191;");
		strRTF = strRTF.replaceAll("&Agrave;" + "", "&#192;");
		strRTF = strRTF.replaceAll("&Aacute;" + "", "");

		strRTF = strRTF.replaceAll("&Acirc;" + "", "");

		strRTF = strRTF.replaceAll("&Atilde;" + "", "&#195;");
		strRTF = strRTF.replaceAll("&Auml;" + "", "&#196;");
		strRTF = strRTF.replaceAll("&Aring;" + "", "&#197;");
		strRTF = strRTF.replaceAll("&AElig;" + "", "&#198;");
		strRTF = strRTF.replaceAll("&Ccedil;" + "", "&#199;");
		strRTF = strRTF.replaceAll("&Egrave;" + "", "&#200;");
		strRTF = strRTF.replaceAll("&Eacute;" + "", "&#201;");
		strRTF = strRTF.replaceAll("&Ecirc;" + "", "&#202;");
		strRTF = strRTF.replaceAll("&Euml;" + "", "&#203;");
		strRTF = strRTF.replaceAll("&Igrave;" + "", "&#204;");
		strRTF = strRTF.replaceAll("&Iacute;" + "", "&#205;");
		strRTF = strRTF.replaceAll("&Icirc;" + "", "&#206;");
		strRTF = strRTF.replaceAll("&Iuml;" + "", "&#207;");
		strRTF = strRTF.replaceAll("&ETH;" + "", "&#208;");
		strRTF = strRTF.replaceAll("&Ntilde;" + "", "&#209;");
		strRTF = strRTF.replaceAll("&Ograve;" + "", "&#210;");
		strRTF = strRTF.replaceAll("&Oacute;" + "", "&#211;");
		strRTF = strRTF.replaceAll("&Ocirc;" + "", "&#212;");
		strRTF = strRTF.replaceAll("&Otilde;" + "", "&#213;");
		strRTF = strRTF.replaceAll("&Ouml;" + "", "&#214;");
		strRTF = strRTF.replaceAll("&times;" + "", "&#215;");
		strRTF = strRTF.replaceAll("&Oslash;" + "", "&#216;");
		strRTF = strRTF.replaceAll("&Ugrave;" + "", "&#217;");
		strRTF = strRTF.replaceAll("&Uacute;" + "", "&#218;");
		strRTF = strRTF.replaceAll("&Ucirc;" + "", "&#219;");
		strRTF = strRTF.replaceAll("&Uuml;" + "", "&#220;");
		strRTF = strRTF.replaceAll("&Yacute;" + "", "&#221;");
		strRTF = strRTF.replaceAll("&THORN;" + "", "&#222;");
		strRTF = strRTF.replaceAll("&szlig;" + "", "&#223;");
		strRTF = strRTF.replaceAll("&agrave;" + "", "&#224;");
		strRTF = strRTF.replaceAll("&aacute;" + "", "&#225;");
		strRTF = strRTF.replaceAll("&acirc;" + "", "&#226;");
		strRTF = strRTF.replaceAll("&atilde;" + "", "&#227;");
		strRTF = strRTF.replaceAll("&auml;" + "", "&#228;");
		strRTF = strRTF.replaceAll("&aring;" + "", "&#229;");
		strRTF = strRTF.replaceAll("&aelig;" + "", "&#230;");
		strRTF = strRTF.replaceAll("&ccedil;" + "", "&#231;");
		strRTF = strRTF.replaceAll("&egrave;" + "", "&#232;");
		strRTF = strRTF.replaceAll("&eacute;" + "", "&#233;");
		strRTF = strRTF.replaceAll("&ecirc;" + "", "&#234;");
		strRTF = strRTF.replaceAll("&euml;" + "", "&#235;");
		strRTF = strRTF.replaceAll("&igrave;" + "", "&#236;");
		strRTF = strRTF.replaceAll("&iacute;" + "", "&#237;");
		strRTF = strRTF.replaceAll("&icirc;" + "", "&#238;");
		strRTF = strRTF.replaceAll("&iuml;" + "", "&#239;");
		strRTF = strRTF.replaceAll("&eth;" + "", "&#240;");
		strRTF = strRTF.replaceAll("&ntilde;" + "", "&#241;");
		strRTF = strRTF.replaceAll("&ograve;" + "", "&#242;");
		strRTF = strRTF.replaceAll("&oacute;" + "", "&#243;");
		strRTF = strRTF.replaceAll("&ocirc;" + "", "&#244;");
		strRTF = strRTF.replaceAll("&otilde;" + "", "&#245;");
		strRTF = strRTF.replaceAll("&ouml;" + "", "&#246;");
		strRTF = strRTF.replaceAll("&divide;" + "", "&#247;");
		strRTF = strRTF.replaceAll("&oslash;" + "", "&#248;");
		strRTF = strRTF.replaceAll("&ugrave;" + "", "&#249;");
		strRTF = strRTF.replaceAll("&uacute;" + "", "&#250;");
		strRTF = strRTF.replaceAll("&ucirc;" + "", "&#251;");
		strRTF = strRTF.replaceAll("&uuml;" + "", "&#252;");
		strRTF = strRTF.replaceAll("&yacute;" + "", "&#253;");
		strRTF = strRTF.replaceAll("&thorn;" + "", "&#254;");
		strRTF = strRTF.replaceAll("&yuml;" + "", "&#255;");
		strRTF = strRTF.replaceAll("&rsquo;" + "", "&#x2019;");

		return strRTF;

	}

}
