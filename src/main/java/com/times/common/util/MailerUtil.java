package com.times.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriUtils;

import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;

/**
 * This utilities class is used to get the common utility method for this project.
 * 
 * @author Ranjeet.Jha
 *
 */
public class MailerUtil {

	private static final Log log = LogFactory.getLog(MailerUtil.class);
	
	public static final String DATE_FORMATE_DDMMYY_DEFULT = "MM/dd/yyyy";
	public static final String DATE_FORMATE_DDMMYY_SLASH = "DD/MM/YYYY";
	public static final String DATE_FORMATE_YYYMMDD = "yyyyMMdd";
	
	/**
	 * This method is used to get the StringCode.
	 * 
	 * @param str
	 * @return
	 */
	public static String getUniqueCodeByDate(String key) {
		
		byte[] md5 = DigestUtils.md5(key.getBytes());
    	String md5String = DigestUtils.md5Hex(md5); 
    	return md5String;
	}
	
	
	/**
	 * This method is  used to get the Hr and min with colon  seperated in 2 digit both.
	 * 
	 * @param date
	 * @return a string containing  HH:mm
	 */
	public static String getFromatedDate(Date date, String format) {
		if (format == null) {
			format = DATE_FORMATE_DDMMYY_DEFULT;
		}
       SimpleDateFormat displayFormat = new SimpleDateFormat(format);
       String formatedDate = displayFormat.format(date);
       return formatedDate;
	}
	
	/**
	 * This method is used to get the template html data by calling external call by provided URI.
	 * 
	 * @param url
	 * @return
	 */
	public static String getData(String url) {
		long startTime = System.currentTimeMillis();
		/*HttpClientParams clientParams = new HttpClientParams();
		clientParams.setParameter(HttpClientParams.MAX_REDIRECTS, "5");
		clientParams.setParameter(HttpClientParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, false));

		Long connectionTimeout = new Long(600000); // 10 * 60 * 1000 = 10 min
		clientParams.setConnectionManagerTimeout(connectionTimeout);
		clientParams.setSoTimeout(600000);

		String outData = null;
		HttpClient httpClient = new HttpClient();
		HttpMethod httpMethod = new GetMethod(url);
		httpMethod.setParams(clientParams);*/
		String outData = null;
		HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
		RequestConfig config = RequestConfig.custom().setMaxRedirects(5)
				.setConnectTimeout(600000).setSocketTimeout(600000).build();

		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try {
			url = UriUtils.encodeQuery(url, "UTF-8");
			gMethod = new HttpGet(url);
			gMethod.setConfig(config);
			hResponse = hClient.execute(gMethod);
			int status = hResponse.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				outData = new BasicResponseHandler().handleResponse(hResponse);
			//int statusCode = httpClient.executeMethod(httpMethod);
			//if (statusCode != HttpStatus.SC_OK) {
				//log.debug();
				//log.debug("URL: " + url + " , statusCode : "+ statusCode + " , " + httpMethod.getStatusText() + " : " + httpMethod.getResponseBodyAsString());
				log.debug("pulling time in ms : " + (System.currentTimeMillis() - startTime) + " of URL : " + url);
			} else {
				//outData = new String(httpMethod.getResponseBody(), "UTF-8");
				/*InputStream is = httpMethod.getResponseBodyAsStream();
				if (is != null) {
					outData = getStringFromInputStream(is);
				}*/
				
				/*ByteArrayOutputStream outputStream = new ByteArrayOutputStream();  
			    byte[] byteArray = new byte[1024];  
			    int count = 0;  
				while ((count = httpMethod.getResponseBodyAsStream().read(byteArray, 0, byteArray.length)) > 0) {
					outputStream.write(byteArray, 0, count);
				}
				//System.out.println("contnet type : " + httpMethod.getParams().getContentCharset());
				outData = new String(outputStream.toByteArray(), "UTF-8");*/
				//log.debug("pulling time in ms : " + (System.currentTimeMillis() - startTime) + " of URL : " + url);
				log.debug("URL: " + url + " , statusCode : "+ status   );
			}
			
		} catch (IOException e) {
			log.error("IOException caught during reading of URL: " + url + " msg : "+ e.getMessage());
			log.debug("pulling time in ms : " + (System.currentTimeMillis() - startTime) + " of URL : " + url);
			new CMSCallExceptions(url + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
			//outData = getData(url);
		} catch (Exception e) {
			log.error("Exception caught during reading of URL: " + url + " msg : "+ e.getMessage());
			new CMSCallExceptions(url + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
		} finally {
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return outData;
	}
	
	
	public static String postData(String url) {
		long startTime = System.currentTimeMillis();
		/*HttpClientParams clientParams = new HttpClientParams();
		clientParams.setParameter(HttpClientParams.MAX_REDIRECTS, "5");
		clientParams.setParameter(HttpClientParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, false));

		Long connectionTimeout = new Long(600000); // 10 * 60 * 1000 = 10 min
		clientParams.setConnectionManagerTimeout(connectionTimeout);
		clientParams.setSoTimeout(600000);

		String outData = null;
		HttpClient httpClient = new HttpClient();
		HttpMethod httpMethod = new PostMethod(url);
		httpMethod.setParams(clientParams);
		try {
			int statusCode = httpClient.executeMethod(httpMethod);
			if (statusCode != HttpStatus.SC_OK) {
				log.debug("URL: " + url + " , statusCode : "+ statusCode + " , " + httpMethod.getStatusText() + " : " + httpMethod.getResponseBodyAsString());
			} else {
				
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();  
			    byte[] byteArray = new byte[1024];  
			    int count = 0;  
				while ((count = httpMethod.getResponseBodyAsStream().read(byteArray, 0, byteArray.length)) > 0) {
					outputStream.write(byteArray, 0, count);
				}
				outData = new String(outputStream.toByteArray(), "UTF-8");
				log.debug("pulling time in ms : " + (System.currentTimeMillis() - startTime) + " of URL : " + url);
			}*/
		String outData = null;
		HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
		RequestConfig config = RequestConfig.custom().setMaxRedirects(2)
				.setConnectTimeout(600000).setSocketTimeout(600000).build();

		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try {
			url = UriUtils.encodeQuery(url, "UTF-8");
			gMethod = new HttpPost(url);
			gMethod.setConfig(config);
			hResponse = hClient.execute(gMethod);
			int status = hResponse.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				outData = new BasicResponseHandler().handleResponse(hResponse);
			}
			
		} catch (IOException e) {
			//log.error("IOException caught during reading of URL: " + url + " msg : "+ e.getMessage());
			log.error("pulling time in ms : " + (System.currentTimeMillis() - startTime) + " of URL : " + url);
			new CMSCallExceptions(url + " - Exception Occured in get the external data Exception : " + e.getMessage(),
					CMSExceptionConstants.EXT_LOG, e);
			outData = postData(url);
		} catch (Exception e) {
			//log.error("Exception caught during reading of URL: " + url + " msg : "+ e.getMessage());
			new CMSCallExceptions(url + " - Exception Occured in get the external data Exception : " + e.getMessage(),	CMSExceptionConstants.EXT_LOG, e);
		} finally {
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return outData;
	}
	
	public static String getDataRecursively(String htmlURL, int i) {
		String content = null;
		int counter = 7;
		try {
			content = getData(htmlURL);
			
			if (StringUtils.hasText(content) && !isAnySectionDataNotFound(content)) {
				return content;
			} else {
				log.debug("Data not found for url : " + htmlURL + ", in recursive call, times= " + i);
				new CMSCallExceptions("Data not found for url : " + htmlURL + ", Exception Occured  while recursive call for external data Exception times= " + i, 
						CMSExceptionConstants.EXT_LOG, new Exception("Data not found " + htmlURL));
				i++;
				
				if (i < counter) {
					content = getDataRecursively(htmlURL, i);
				} else {
					log.debug("Data not found for url : " + htmlURL + ", in recursive call, times= " + i);
					return null;
				}
			}
		} catch (Exception e) {
			new CMSCallExceptions("Exception Occured in while recursive call for external data Exception : " + e.getMessage(),
					CMSExceptionConstants.EXT_LOG, e);
		}
		
		return content;
		
	}
	
	public static boolean isAnySectionDataNotFound(String content) {
		String[] commentedMsgArray = new String[] {"<!--Data not found from most read stories-->", "<!--Data not found from most shared stories-->", 
				"<!--Data not found from most commented stories-->", "<!--contentNotFound4DNL-->", "<!--contentNotFound4NL-->"};
		boolean isCommentMsgFound = false;
		try {
			if ( content != null) {
				for (String commentedMsg :commentedMsgArray) {
					if (content.indexOf(commentedMsg) != -1)  {
						isCommentMsgFound = true;
						new CMSCallExceptions("Exception Occured while checking msg in content data Exception msg : " + commentedMsg,
								CMSExceptionConstants.EXT_LOG, new Exception("reason for not found data: " + commentedMsg));
						break;
					}
				}
			}
		} catch (Exception e) {
			new CMSCallExceptions("Exception Occured while checking msg in content data Exception msg : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
		}
		
		return isCommentMsgFound;
	}
	
	public static void main(String[] args) {
		String content = getDataRecursively("http://www2.timesofindia.indiatimes.com/newsletternew.cms", 1);
		System.out.println(content);
	}
	
	/**
	 * convert InputStream to String
	 * 
	 * @param is
	 * @return
	 */
	private static String getStringFromInputStream(InputStream is) {
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			log.error("IOException caught during reading from InputStream msg : "+ e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					log.error("IOException caught while closing InputStream msg : "+ e.getMessage());
				}
			}
		}
		return sb.toString();
	}
	
	public static int getInt(String input) {
		int i = 0;
		try {
			i = Integer.parseInt(input);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return i;
	}
	
}
