package com.times.common.util;


import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSInputFile;
import com.times.common.dao.MongoRestAPIService;


public class EPaperDBUpload {
	
	
	
	
	
	/**
	 * @param args
	 */

	public static void main(String[] args) {

		Map<String, Object> map = new HashMap<String, Object>();
		/*map.put(Constant.PUBLISHER, "NBT");
		map.put(Constant.EDITION, "Delhi");
		map.put(Constant.LOCATION, "NBT NOIDA");
		map.put(Constant.STATUS, 1);*/
		map.put(Constant.PUBLISHER, "TOI CAP");
		map.put(Constant.EDITION, "Delhi");
		map.put(Constant.LOCATION, "TOI CAP");
		map.put(Constant.STATUS, 1);
		
		
		try {
			insertData(map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

	
	
	private static Date getDate(){
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		return date;
	}
	
	
	public static String getHashKey(String value) {
		MessageDigest md;
		StringBuffer sb = new StringBuffer();
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(value.getBytes());

			byte byteData[] = md.digest();

			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sb.toString();
	}


	
	public static void insertData(Map<String, Object> map) throws Exception {
		BasicDBObject mrDBObject = new BasicDBObject();

		mrDBObject.put(Constant.PUBLISHER, map.get(Constant.PUBLISHER));
		mrDBObject.put(Constant.EDITION, map.get(Constant.EDITION));
		mrDBObject.put(Constant.LOCATION, map.get(Constant.LOCATION));
		mrDBObject.put(Constant.UNIQUE_KEY, getHashKey(String.valueOf(map.get(Constant.EDITION)) + "_"+ String.valueOf(map.get(Constant.LOCATION)).replaceAll("\\s", "_")));		
		mrDBObject.put(Constant.STATUS, map.get(Constant.STATUS));
		mrDBObject.put(Constant.CREATED_AT, getDate());
		mrDBObject.put(Constant.UPDATED_AT, getDate());	

		MongoRestAPIService.insert(Constant.EPAPER_COLLECTION, mrDBObject);

	} 
}
