package com.times.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.MailSender;
import com.times.mailer.dao.MailerDaoImpl;

/**
 * This classs is used to send the ET template for times verified used.
 * 
 * @author Ranjeet.Jha
 * @see nohup mvn exec:java -Dexec.mainClass="com.times.common.util.ManualNLSender"
 *
 */
public class ManualNLSender {
	private final static Logger logger = LoggerFactory.getLogger(ManualNLSender.class);
	public static final String conentPath = "timesofindia@bounce.indiatimes.com";
	public static final String from = "The Times Of India <mailerservice@timesofindia.com>";
	public static final String subject = "New iphone is here. Check out TOI Tech from 10.30 PM for live coverage";
	public static final String templateURL = "http://timesofindia.indiatimes.com/iphone_event_2014.cms";
	public static final String replyBackId = "The Times Of India <mailerservice@timesofindia.com>";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		MailSender mailSender = null;
		
		String to = null;
		List<DBObject> dbObjectList = null;
		int offset = 0;
		
		String[] emailArray =  {"ranjeet.jha@timesinternet.in", "rajesh.kumar@timesinternet.in", "rajeev.khatri@timesinternet.in"};
		try {
			mailSender = new MailSender("nmailer.indiatimes.com",  from,  conentPath,  replyBackId);
			//mailSender = new MailSender("cmailer.indiatimes.com",  from,  conentPath,  replyBackId);
			
			String mailBody = MailerUtil.getDataRecursively(templateURL, 1);
			if (mailBody == null || "".equals(mailBody.trim())) {
				mailBody = MailerUtil.getDataRecursively(templateURL, 1);
			}
			
			int perPage = 50;
		
			BasicDBList and = new BasicDBList();
			
			//BasicDBObject queryV_USR = new BasicDBObject("V_USR", true);
			BasicDBObject queryST = new BasicDBObject("ST", "1");
			
			/*BasicDBList orCond = new BasicDBList();
			orCond.add(queryV_USR);
			orCond.add(queryST);
			DBObject query = new BasicDBObject("$or", orCond);*/
			
			
			/*BasicDBObject query1 = new BasicDBObject("NL_MAP.ID", "1001");		
			BasicDBObject query2 = new BasicDBObject("NL_MAP.ID", "1002");		
			BasicDBObject query3 = new BasicDBObject("NL_MAP.ID", "1003");		
			BasicDBObject query4 = new BasicDBObject("NL_MAP.ID", "1004");		
			BasicDBObject query5 = new BasicDBObject("NL_MAP.ID", "1005");		
			BasicDBObject query6 = new BasicDBObject("NL_MAP.ID", "1006");
			BasicDBList or = new BasicDBList();
			or.add(query1);
			or.add(query2);
			or.add(query3);
			or.add(query4);
			or.add(query5);
			or.add(query6);
			DBObject ORquery = new BasicDBObject("$or", or);*/
			
			BasicDBObject query5 = new BasicDBObject("NL_MAP.ID", "1005");
			
			and.add(queryST);			
			and.add(query5);
			
			DBObject Andquery = new BasicDBObject("$and", and);
			
			
			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", "0");
			
			
			
			
			if (mailBody != null) {
				
				for (String email : emailArray) {
					mailSender.sendMessage(email, subject, mailBody);
				}
				
				for (int i = 0; i < 1000;i++) {
					dbObjectList = MongoRestAPIService.getList(MailerDaoImpl.SUBSCRIBER_COLLECTION, Andquery, optionMap);
					
					if (dbObjectList != null) {
						for (DBObject dbObj : dbObjectList) {
							logger.info("offset : " + offset +  " " + String.valueOf(dbObj.get("S_ID")));							
							to = String.valueOf(dbObj.get("S_ID"));
							if (to != null) {
								 mailSender.sendMessage(to, subject, mailBody);
							}
						}
					}
					offset = offset + perPage;
					optionMap.put("skip", String.valueOf(offset));
					dbObjectList = null;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
