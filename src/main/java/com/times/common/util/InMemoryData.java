package com.times.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.mongodb.DBObject;
import com.times.common.model.TimeLineInstance;
import com.times.mailer.model.Mailer;

/**
 * This class is used to keep the data in in-memory for the required stuff.
 * 
 * @author Ranjeet.Jha
 *
 */
public class InMemoryData {

	public static Map<String, String> newsMailerDataMap = new HashMap<String, String>();
	public static Map<String, String> newsMailerUIMap = new HashMap<String, String>();
	public static Map<String,Mailer> mailer = Collections.synchronizedMap(new HashMap<String, Mailer>()); 
	
	// to update finally with this code for the key
	public static Map<String, String> keyCodeMap = new HashMap<String, String>();

	public static Map<String, Integer> emailSentCounter = new HashMap<String, Integer>();

	//Send Alert List for Gadgets
    public static List<DBObject> alertList = new ArrayList<DBObject>();
    
    //Send Alert List for Brands
    public static List<DBObject> brandAlertList = new ArrayList<DBObject>();
	
	public static final Map<String, String> SECTION_MAP = new HashMap<String, String>();
	
	public static final Map<String, String> SPECIAL_SYMBOL = new HashMap<String, String>();
	//-2128936835,296589292,4719148,1898055,5880659,9978101,2647163,-2128672765,-2128838597,-2128839596,-2128833038,-2128816011, 2950623,-2128821153,3947060,4118235,10744190,-2128816762
	static {
		// india-
		SECTION_MAP.put("-2128936835", "http://timesofindia.indiatimes.com/india/indiaarticlelist/-2128936835.cms");
		// business-
		SECTION_MAP.put("1898055", "http://timesofindia.indiatimes.com/business/bizarticlelist/1898055.cms");
		// Environment-
		SECTION_MAP.put("2647163", "http://timesofindia.indiatimes.com/home/environment/articlelist/2647163.cms");
		// World-
		SECTION_MAP.put("296589292", "http://timesofindia.indiatimes.com/world/worldarticlelist/296589292.cms");
		// Entertainment-
		SECTION_MAP.put("1081479906", "http://timesofindia.indiatimes.com/entertainment/articlelistls/1081479906.cms");
		// Science-
		SECTION_MAP.put("-2128672765", "http://timesofindia.indiatimes.com/home/science/articlelist/-2128672765.cms");
		SECTION_MAP.put("4719148", "http://timesofindia.indiatimes.com/sports/sphome/4719148.cms");
		// Technology-
		SECTION_MAP.put("5880659", "http://timesofindia.indiatimes.com/tech/techhome/5880659.cms");
		// Education- 913168846
		SECTION_MAP.put("913168846", "http://timesofindia.indiatimes.com/home/education/news/articlelist/913168846.cms");

		// city
		// Mumbai-
		SECTION_MAP.put("-2128838597", "http://timesofindia.indiatimes.com/city/mumbai/articlelist/-2128838597.cms");
		// Delhi-
		SECTION_MAP.put("-2128839596", "http://timesofindia.indiatimes.com/city/delhi/articlelist/-2128839596.cms");
		// Bangalore-
		SECTION_MAP.put("-2128833038", "http://timesofindia.indiatimes.com/city/bangalore/articlelist/-2128833038.cms");
		// Hyderabad-
		SECTION_MAP.put("-2128816011", "http://timesofindia.indiatimes.com/city/hyderabad/articlelist/-2128816011.cms");
		// Chennai-
		SECTION_MAP.put("2950623", "http://timesofindia.indiatimes.com/city/chennai/articlelist/2950623.cms");
		// Ahmedabad-
		SECTION_MAP.put("-2128821153", "http://timesofindia.indiatimes.com/city/ahmedabad/articlelist/-2128821153.cms");
		// Allahabad-
		SECTION_MAP.put("3947060", "http://timesofindia.indiatimes.com/city/allahabad/articlelist/3947060.cms");
		// Bhubaneswar-
		SECTION_MAP.put("4118235", "http://timesofindia.indiatimes.com/city/bhubaneswar/articlelist/4118235.cms");
		// Bhopal-
		SECTION_MAP.put("10744190", "http://timesofindia.indiatimes.com/city/bhopal/articlelist/10744190.cms");
		// Chandigrah-
		SECTION_MAP.put("-2128816762", "http://timesofindia.indiatimes.com/city/chandigarh/articlelist/-2128816762.cms");
		
		
		SPECIAL_SYMBOL.put("�", "'");
		SPECIAL_SYMBOL.put("�", "'");
		SPECIAL_SYMBOL.put("�", "\"");
		SPECIAL_SYMBOL.put("�", "\"");
		SPECIAL_SYMBOL.put("&nbsp;", " ");
		SPECIAL_SYMBOL.put("&rsquo;", "'");
		SPECIAL_SYMBOL.put("&#160;", " ");
		
	}
	
	// Travel related map
	public Map<String, String> TRAVEL_USER_PREFERENCES_MAP = new HashMap<String, String>();
	
	
	public static Map<Integer, String> contentMap = new ConcurrentHashMap<Integer, String>(); 
	
	public static Map<String, String> newsletterContentMap = new ConcurrentHashMap<String, String>(); 
	
	//etWealth-ibeat process
	public static boolean success_status_ibeat = false; 
	
	//ETEpaper last processed msid and date
	public static int etEpaperMsid = 0;
	public static Date etEpaperDate;
	
	//Perfect marketing new url creation job
	public static int perfectMarketMsid = 0;
	public static Date perfectMarketDate;
	
	public static boolean etEpaperSuccess;
	
	public static int actual_status_ibeat;
	
	public static Date etTimeLineDate;
	
	public static TimeLineInstance instance;
	public static boolean CCIAliveSuccess;
	public static int CCIAliveMsid;
	public static Date CCIAliveDate;
	public static Date CCIDate;
}
