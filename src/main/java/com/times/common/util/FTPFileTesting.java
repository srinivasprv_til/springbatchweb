package com.times.common.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.times.mailer.model.EpaperInfo;

public class FTPFileTesting {
	private final static String	FTP_IP		= "10.100.109.86";
	private final static String	USER_NAME	= "online";
	private final static String	PASSWORD	= "online@321";
	private final static String	DIRECTORY	= "PDF@epaper";
	FTPClient					ftpClient;

	private final static Logger logger = LoggerFactory.getLogger(FTPFileTesting.class);

	public FTPFileTesting(EpaperInfo ePaperInfo, String date) throws Exception {
		String directory = ePaperInfo.getEdition() + "/" + ePaperInfo.getLocation() + "/" + date + "/" + DIRECTORY;

		ftpClient = new FTPClient();
		/*
		 * FTPClientConfig conf = new
		 * FTPClientConfig(FTPClientConfig.SYST_UNIX);
		 * ftpClient.configure(conf);
		 */
		ftpClient.connect(FTP_IP);
		boolean constatus = ftpClient.login(USER_NAME, PASSWORD);
		if (!constatus) {
			logger.error("login in ftp server failed");
			throw new Exception("login in ftp server failed");
		}
		ftpClient.changeWorkingDirectory(directory);
		ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

	}

	public FTPFile[] listFiles() throws IOException {
		FTPFile[] files = ftpClient.listFiles();
		return files;
	}

	public InputStream retrieveFileStream(FTPFile file) throws IOException {
		String remoteFileName = file.getName();
		InputStream is = ftpClient.retrieveFileStream(remoteFileName);
		return is;
	}

	public void completePendingCommands() {
		try {
			if (!ftpClient.completePendingCommand()) {
				logger.error("Error while comleting pending commands");
			}
		} catch (Exception e) {
			logger.error("Error while completing ftp connection:", e);
		}
	}
	
	public static void main(String[] args) throws Exception {
		FTPClient ftp = new FTPClient();
		/*
		 * FTPClientConfig conf = new
		 * FTPClientConfig(FTPClientConfig.SYST_UNIX);
		 * ftpClient.configure(conf);
		 */
		String directory = "/Delhi/TOI CAP/161116/PDF@epaper";
		ftp.connect(FTP_IP);
		boolean constatus = ftp.login(USER_NAME, PASSWORD);
		if (!constatus) {
			logger.error("login in ftp server failed");
			throw new Exception("login in ftp server failed");
		}
		ftp.changeWorkingDirectory(directory);
		ftp.setFileType(FTP.BINARY_FILE_TYPE);
		
	}

}
