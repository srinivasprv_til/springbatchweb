/**
 * 
 */
package com.times.common.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.mailer.model.Subscription;



/**
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.DailyBriefUserFetchUtil"
 * @author Rajeev.Khatri
 *
 */
public class DailyBriefUserFetchUtil {

	private static final Log log = LogFactory.getLog(DailyBriefUserFetchUtil.class);


	/**
	 * @param args
	 * @throws IOException 
	 * @throws ProtocolException 
	 * @throws MalformedURLException 
	 * @throws UnsupportedEncodingException 
	 */
	public static void main(String[] args)  {

		int perPage = 50;
		
		int offset = 0;

		BasicDBObject filter1 = new BasicDBObject();
		filter1.put("ID", "1016");
		filter1.append("WL", true);
		
		DBObject finalQuery = new BasicDBObject("$elemMatch", filter1);	
		BasicDBObject filter2 = new BasicDBObject();
		filter2.put("NL_MAP", finalQuery);
	
		
		BasicDBList and = new BasicDBList();
		and.add(filter2);
		
		BasicDBObject filter = new BasicDBObject();
		filter = new BasicDBObject("$and", and);


		Map<String, Object> optionMap = new HashMap<String, Object>();
		optionMap.put("limit", perPage);
		optionMap.put("skip", "0");
		Long count = MongoRestAPIService.getCount("newslettersubscription", filter);
		long loop = (count / perPage) + 1;
		for (int i =1; i <= loop; i++)  {

			List<DBObject> dbObjectList = MongoRestAPIService.getList("newslettersubscription", filter , optionMap);		

			if (dbObjectList != null) {
				for (DBObject objDB : dbObjectList) {

					List<DBObject> nlListMap = (List<DBObject>) objDB.get(Subscription.NEWS_LETTER_MAP);
					if (nlListMap != null) {
						for(DBObject db : nlListMap){
							if (db.get("ID") != null && db.get("ID").toString().equalsIgnoreCase("1016")) {							


								String emaild = (String) objDB.get("E_ID");
								String hashcode = (String) db.get("H_CODE");


								BasicDBObject query1 = new BasicDBObject();		
								query1.put("E_ID", emaild);
								query1.put("NL_MAP.ID", "1016");

								BasicDBObject datatoupdate = new BasicDBObject();					
								datatoupdate.put("NL_MAP.$.WL", false);
								datatoupdate.put("NL_MAP.$.DL", true);

								BasicDBObject newDocument = new BasicDBObject();
								newDocument.append("$set", datatoupdate);
								MongoRestAPIService.update("newslettersubscription", query1, newDocument,true);
								System.out.println(count++ + " , email verified : " + emaild);
							}

						}
					}


				}				

			}
			//offset = offset + perPage;
			//optionMap.put("skip", String.valueOf(offset));
			log.debug("skip : " + offset + " page " + i);
		}
		
	}


}
