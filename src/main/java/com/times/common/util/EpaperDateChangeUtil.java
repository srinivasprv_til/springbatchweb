package com.times.common.util;

/**
 *	command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.ToiConfirmationUtil"
 * 
 */

import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;


/**
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.EpaperDateChangeUtil"
 * 
 * @author Rajeev Khatri
 * 
 */
public class EpaperDateChangeUtil {

	private final static Logger logger = Logger.getLogger(EpaperDateChangeUtil.class);

	
	
	public static void main(String[] args) throws FileNotFoundException {

		DBObject dbObject = new BasicDBObject();
		DateFormat lv_formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		lv_formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		String strDate = lv_formatter.format(new Date());
		Date parsedDate = null;
		try {
			parsedDate = lv_formatter.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar now = Calendar.getInstance();
		now.setTime(parsedDate);
		now.add(Calendar.HOUR, -18);
		
		Date finalDate = now.getTime();
		String finalDateInString = lv_formatter.format(finalDate);
		
		dbObject.put("type", "status");
		//dbObject.put("lastDate", "2016-12-03T05:30:54Z");	
		dbObject.put("lastDate",finalDateInString);	
		dbObject.put("lastMsid", 55273148 );
		
		DBObject queryObject = new BasicDBObject();
		queryObject.put("type", "status");
		
		BasicDBObject newDocument = new BasicDBObject();
		newDocument.append("$set", dbObject);
	
		MongoRestAPIService.updateWithPost("ETEpaperStatus", queryObject, newDocument, true);
	
	}
	
	
}
