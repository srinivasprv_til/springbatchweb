package com.times.common.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
/**
 * Created by arun.sharma2 on 23/02/16.
 */

public class ZipUtils
{
    List<String> fileList;
    private static final String INPUT_ZIP_FILE = "/Users/arun.sharma2/Desktop/pdf-sample.zip";
    private static final String OUTPUT_FOLDER = "/Users/arun.sharma2/Desktop/testzipOutput";

    public static void main( String[] args )
    {
        ZipUtils zipUtils = new ZipUtils();
        zipUtils.unZip(new File(INPUT_ZIP_FILE), new File (OUTPUT_FOLDER));
    }

    /**
     * Unzip it
     * @param zipFile input zip file
     * @param output zip file output folder
     */
    public void unZip(File zipFile, File outputDir) {

        byte[] buffer = new byte[1024];

        try{

        	if(!outputDir.exists()){
                outputDir.mkdir();
            }

            //get the zip file content
            ZipInputStream zis =
                    new ZipInputStream(new FileInputStream(zipFile));
            //get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();

            while(ze!=null){

                String fileName = ze.getName();
                File newFile = new File(outputDir + File.separator + fileName);

                System.out.println("file unzip : "+ newFile.getAbsoluteFile());

                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                ze = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();

            System.out.println("Done");

        }catch(IOException ex){
            ex.printStackTrace();
        }
    }
}