/**
 * 
 */
package com.times.common.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.web.util.UriUtils;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;



/**
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.InboxerUtil"
 * @author Rajeev.Khatri
 *
 */
public class InboxerUtil {

	private static final Log log = LogFactory.getLog(InboxerUtil.class);

	private static String SSO_VC = "http://jssoprofile.indiatimes.com/sso/GetXMLUserProfile?emailid=#emailId#&siteid=ef55d27920fbf01f58c0da43430f5c6a";
	
	private static int counter = 1;

	public static void main1(String[] args) {
		try {
			String result = getInboxer("rprava2013@gmail.com");
			
			if (result != null) {
				if (result.indexOf("Congratulations! You have just unlocked the Inboxer Badge for subscribing to the Timesofindia.com daily newsletter.") == -1) {
					System.out.println("rk");
				}
			}
			/*activateInboxer("TILTOI_3@yahoo.in");
			getInboxer("TILTOI_3@yahoo.in");*/
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * @param args
	 * @throws IOException 
	 * @throws ProtocolException 
	 * @throws MalformedURLException 
	 * @throws UnsupportedEncodingException 
	 */
	public static void main(String[] args)  {

		int perPage = 50;
		
		int offset = 0;

		BasicDBList and = new BasicDBList();

		BasicDBObject query = new BasicDBObject();	
		query.put("V_USR", true);

		BasicDBObject querySt = new BasicDBObject();	
		querySt.put("ST", "1");

		BasicDBObject query1 = new BasicDBObject("NL_MAP.ID", "1001");		
		BasicDBObject query2 = new BasicDBObject("NL_MAP.ID", "1002");		
		BasicDBObject query3 = new BasicDBObject("NL_MAP.ID", "1003");		
		BasicDBObject query4 = new BasicDBObject("NL_MAP.ID", "1004");		
		BasicDBObject query5 = new BasicDBObject("NL_MAP.ID", "1005");		
		BasicDBObject query6 = new BasicDBObject("NL_MAP.ID", "1006");
		BasicDBList or = new BasicDBList();
		or.add(query1);
		or.add(query2);
		or.add(query3);
		or.add(query4);
		or.add(query5);
		or.add(query6);


		DBObject ORquery = new BasicDBObject("$or", or);
		
		String startDate = "2015-05-04";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(startDate));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date s_Date = DateUtils.addHrMinutesToDate(c.getTime(), 5, 30);		
		DBObject dateObj =  new BasicDBObject("$gte", s_Date);

		BasicDBList ORDatequery = new BasicDBList();
		
		
		DBObject createdateRaneQuery = new BasicDBObject();
		createdateRaneQuery.put("C_AT",dateObj);
		ORDatequery.add(createdateRaneQuery);
		
		DBObject updatedateRaneQuery = new BasicDBObject();
		updatedateRaneQuery.put("U_AT",dateObj);		
		ORDatequery.add(updatedateRaneQuery);
		
		DBObject ORDtquery = new BasicDBObject("$or", ORDatequery);
		
		
		and.add(query);
		and.add(querySt);
		and.add(ORquery);
		and.add(ORDtquery);


		DBObject Andquery = new BasicDBObject("$and", and);


		Map<String, Object> optionMap = new HashMap<String, Object>();
		optionMap.put("limit", perPage);
		optionMap.put("skip", "0");
		
		for (int i =0; i <= 110; i++)  {

			List<DBObject> dbObjectList = MongoRestAPIService.getList("newslettersubscription", Andquery , optionMap);
			if (dbObjectList != null) {
				for (DBObject objDB : dbObjectList) {
					if (objDB.get("E_ID") != null && objDB.get("H_CODE") != null && !"null".equalsIgnoreCase(objDB.get("H_CODE").toString()) ) {
						if (objDB.get("E_ID").toString().indexOf("mailinator.com") == -1 && objDB.get("E_ID").toString().indexOf("maildrop.cc")== -1 
								&& objDB.get("E_ID").toString().indexOf("mailtothis.com") == -1 && objDB.get("E_ID").toString().indexOf("monumentmail.com") == -1
								&& objDB.get("E_ID").toString().indexOf("reallymymail.com") == -1 && objDB.get("E_ID").toString().indexOf("sendspamhere.com") == -1) {
							

							//System.out.println(objDB.get("E_ID"));
							try {
								String result = getInboxer(objDB.get("E_ID").toString());
								if (result != null) {
									if (result.indexOf("Congratulations! You have just unlocked the Inboxer Badge for subscribing to the Timesofindia.com daily newsletter.") == -1) {
										activateInboxer(objDB.get("E_ID").toString());
									}
								}
								
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (MalformedURLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (ProtocolException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}				

			}

			offset = offset + perPage;
			optionMap.put("skip", String.valueOf(offset));
			log.debug("skip : " + offset + " page " + i);
		}
	}


	/**
	 * API activate inboxer batch activity.
	 * @param uemail
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ProtocolException
	 */
	private static String activateInboxer(String uemail) throws UnsupportedEncodingException, MalformedURLException, IOException, ProtocolException {
		String retval = "";
		try {

			String uid = getUserVC(uemail);
			
			if (uid != null) {

				//String postData = "http://test.reward.indiatimes.com/bp/api/alog/al";
				String postData = "http://rewards.indiatimes.com/bp/api/alog/al";

				String pcode = "TOI"; 
				String scode  = "News";
				String aname = "sub_dn";			

				List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
				urlParameters.add(new BasicNameValuePair("uid", uid));
				urlParameters.add(new BasicNameValuePair("uemail", uemail));
				urlParameters.add(new BasicNameValuePair("pcode", pcode));
				urlParameters.add(new BasicNameValuePair("scode", scode));
				urlParameters.add(new BasicNameValuePair("aname", aname));

				int status = postData(postData,urlParameters);
				log.debug("Email id :" + uemail + "\t" +"Activate Status :" + status + "\t" + "user id :" + uid + "\t" + "Counter :" + counter);
				counter++;
				//System.out.println("Email id :" + uemail + "\t" +"Activate Status :" + status + "\t" + "user id :" + uid);
			} else {
				log.debug("Email id : " + uemail + "\t" + "user vc : " + uid);
				//System.out.println("Email id : " + uemail + "\t" + "user vc : " + uid);
			}

		} catch (Exception e) {

		}
		return retval;
	}



	private static String getInboxer(String uemail) throws UnsupportedEncodingException, MalformedURLException, IOException, ProtocolException {
		String retval = "";
		try {

			String postData = "";	
			
			String uid = getUserVC(uemail);
			
			log.debug("Email id :" + uemail + " uid " + uid);
			if (uid != null) {

				String pcode = "TOI";
				postData += "uid=" + uid + "&pcode="+ pcode;

				retval = MailerUtil.getData("http://rewards.indiatimes.com/bp/api/urs/bhtry?" +  postData);				
				
			} else {
				return null;
			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		return retval;
	}


	private static String getUserVC(String emailId) {
		String url = SSO_VC.replace("#emailId#",  emailId);
		String user_vc = null;
		String result = MongoRestAPIService.getExtData(url);
		if (result.indexOf("<usr_id_vc>") != -1) {
			user_vc = result.substring(result.indexOf("<usr_id_vc>") +11, result.indexOf("</usr_id_vc>"));
		}

		if (user_vc != null) {
			return user_vc;
		}
		return user_vc;
	}

	/** 
	 * http call
	 * @param url
	 * @param urlParameters
	 * @return
	 */
	private static int postData(String url, List<NameValuePair> urlParameters) {
		HttpClient hClient = HttpClients.custom()
				.setRetryHandler(new DefaultHttpRequestRetryHandler(3, false))
				.build();
		RequestConfig config = RequestConfig.custom()
				.setMaxRedirects(2)
				.setConnectTimeout(5000)
				.setSocketTimeout(5000)
				.build();

		HttpPost pMethod = null;
		HttpResponse hResponse = null;
		try {
			url = UriUtils.encodeQuery(url, "UTF-8");
			URI uri = new URIBuilder(url).build();
			url = uri.toString();
			pMethod = new HttpPost(url);
			pMethod.setHeader("Content-Type", "application/x-www-form-urlencoded");
			pMethod.setEntity(new UrlEncodedFormEntity(urlParameters));
			hResponse = hClient.execute(pMethod);

			return hResponse.getStatusLine().getStatusCode();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
	}




}
