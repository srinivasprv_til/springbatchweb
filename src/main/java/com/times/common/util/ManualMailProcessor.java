package com.times.common.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import com.times.common.mail.MailSender;

/**
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.ManualMailProcessor"
 * @author Rajeev.Khatri
 *
 */
public class ManualMailProcessor {

	//private MailSender mailSender = null;
	
	public ManualMailProcessor() {
		
	}
	
	
	private final static String subject = "Gift of music specially for you";
	public static final String FROM = "The Times Of India <mailerservice@timesofindia.com>";
	public static final String replyBackId = "The Times Of India <mailerservice@timesofindia.com>";
	
	public static final String host = "nmailer.indiatimes.com";
	public static final String conentPath = "timesofindia@bounce.indiatimes.com";
	
	public static final String templateURL = "http://timesofindia.indiatimes.com/toi_apple_mailer.cms";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FileInputStream fis;

		MailSender mailSender = null;
		try {
			String mailBody = MailerUtil.getDataRecursively(templateURL, 1);
			if (mailBody == null || "".equals(mailBody.trim())) {
				mailBody = MailerUtil.getDataRecursively(templateURL, 1);
			}
			mailSender = new MailSender(host,  FROM,  conentPath,  replyBackId);

			//fis = new FileInputStream("D:/unverified/promotions.xls");

			fis = new FileInputStream("/opt/svn/docs/promotion.xls");

			List<Map<String, Object>> list = XlsReader.getRecordsList(fis);
			int i=0;
			for (Map<String, Object> map : list) {
				String emaild = (String)map.get("email");				
				String contentBody = mailBody.replace("xxxxxx", (String)map.get("code"));
				mailSender.sendMessage((String)map.get("email"), subject , contentBody);
				System.out.println(i++ + " , email: " + map.get("email"));

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}



	}

}
