package com.times.common.util;

import java.io.FileInputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.MailSender;

public class SendStaticMailToExcelUsers {

	public static final String FILE_PATH="/opt/svn/docs/SendMail.xls";
//	public static final String FILE_PATH="C:\\Users\\aakash.gupta\\Desktop\\SendMail.xls";

	public static final String FROM = "Ei Samay Online Editor <eisamaynewsletter@timesinternet.in>";
	private static String SUBJECT = "এবার এসে গেল এই সময় নিউজলেটার... সাবস্ক্রাইব করুন আজই!!!";
	private static String BODY = null;

	public static final String HOST = "nmailer.indiatimes.com";
	public static final String conentPath = "eisamaynewsletter@timesinternet.in";
	public static final String replyBackId = "Ei Samay Online Editor <eisamaynewsletter@timesinternet.in>";

	private static String templateURL = "http://eisamay.indiatimes.com/usersubscription.cms?email=";

	public static final boolean paginationRequired = true;
	public static final int pageSize = 400;

	public static int totalMailSent = 0;
	public static int totalAlreadySent = 0;

	public static void main(String[] args) {
		reader(FILE_PATH);
		System.out.println("Mail Sent to users : " + totalMailSent);
		System.out.println("Already exists users : " + totalAlreadySent);
	}

	/**
	 * To read the excel sheet and return data.
	 * @param fileName
	 * @return
	 */
	public static Vector reader(String fileName,String ... args)    {
		Vector cellVectorHolder = new Vector();
		try{
			FileInputStream myInput = new FileInputStream(fileName);
			POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
			HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);
			HSSFSheet mySheet = myWorkBook.getSheetAt(0);

			if(paginationRequired){
				int totalRows = mySheet.getPhysicalNumberOfRows();
				int numOfPages = totalRows/pageSize + 1;
				int counter = 1;
				for(int i=0; i<numOfPages; i++){
					Iterator rowIter = mySheet.rowIterator(); 
					for(int j = 0; j < i*pageSize; j++)
						rowIter.next();
					counter = 1;
					while(rowIter.hasNext() && counter++ <= pageSize){
						HSSFRow myRow = (HSSFRow) rowIter.next();
						Iterator cellIter = myRow.iterator();
						cellVectorHolder.addElement(myRow.getCell(0));
					}
					processor(cellVectorHolder);
					cellVectorHolder = new Vector();
					System.out.print("PageNo ");System.out.print(i+1);System.out.println(" Processed");
				}
			}else{
				Iterator rowIter = mySheet.rowIterator(); 
				while(rowIter.hasNext()){
					HSSFRow myRow = (HSSFRow) rowIter.next();
					Iterator cellIter = myRow.iterator();
					cellVectorHolder.addElement(myRow.getCell(0));
				}
				processor(cellVectorHolder);
			}
		}catch (Exception e){e.printStackTrace(); }
		return null;
	}

	/**
	 * @param users
	 * @param args
	 * @return
	 */
	public static Vector processor(Vector users, String ... args){          
		writter(users);
		return null;
	}


	public static Vector writter(Vector users, String ... args){
		sendMail(users);
		return null;
	}


	/**
	 * this will send mail to all users in users parameter one by one
	 * @param users : list of users you want to send mails
	 */
	private static void sendMail(Vector users) {
		MailSender mailSender = null;
		mailSender = new MailSender(HOST,  FROM,  conentPath,  replyBackId);
		HSSFCell myCell = null;
		if (users != null && users.isEmpty() == false) {
			for(int i=0; i<users.size(); i++){
				myCell = (HSSFCell)users.elementAt(i);
				String emailId = myCell.toString();
				if (emailId != null && !"".equalsIgnoreCase(emailId)) {
					BasicDBObject filter = new BasicDBObject();
					filter.put("ID", "36");      
					DBObject query = new BasicDBObject("$elemMatch", filter);     
					BasicDBObject finalQuery = new BasicDBObject();
					finalQuery.put("NL_MAP", query);
					BasicDBObject eMailIdQuery = new BasicDBObject();
					eMailIdQuery.put("E_ID", emailId);
					BasicDBList and = new BasicDBList();
					and.add(finalQuery);
					and.add(eMailIdQuery);
					DBObject Andquery = new BasicDBObject("$and", and);
					List<DBObject> dbObjectList = MongoRestAPIService.getList("newsAlertSubscription", Andquery);
					if (dbObjectList != null && dbObjectList.size() > 0) {
						System.out.println("Aready added : "+ myCell.toString());
						totalAlreadySent++;
					}else{
						//can change body anywhere wherever required
						BODY = readBodyFromTemplate(templateURL+emailId);
						BODY = BODY.replaceAll("#emailid", emailId);
						if(BODY != null){
							mailSender.sendMessage(emailId, SUBJECT, BODY);
							//mailSender.sendMessage("eisamaytest3@mailinator.com", SUBJECT, BODY);
							System.out.println("mail sent to : "+ myCell.toString());
						}else{
							System.out.println("Body not found for "+ emailId);
						}
						totalMailSent++;
						BODY = "";
					}
				}
			}
		} else  {
			System.out.println("No user found in excel sheet");
		}
	}
	


	private static String readBodyFromTemplate(String templateURL) {
		String mailBody = MailerUtil.getDataRecursively(templateURL, 1);
		if (mailBody == null || "".equals(mailBody.trim())) {
			mailBody = MailerUtil.getDataRecursively(templateURL, 1);
		}
		return mailBody;
	}


}
