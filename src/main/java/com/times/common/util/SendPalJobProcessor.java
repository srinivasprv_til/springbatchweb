package com.times.common.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.times.common.mail.MailSender;



/**
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.SendPalJobProcessor"
 * @author Rajeev.Khatri
 *
 */
public class SendPalJobProcessor {

	private static String ADD_USER = "#domain/newsletter/addsubscription/?id=#emailid" +
			"&frequency=1&scheduletime=#sch&status=0&host=#host&nlid=#nlid&templatename=subscription&adHoc=true&verified=#verified";

	private static String addUserURL = "http://jsso.indiatimes.com/sso/InsertIntegraUserProfile?emailid=#emailid#&firstname=#firstname#&lastname=#lastname#&password=#password#&siteid=#siteid#";
	private static String isValidUserURL = "http://jsso.indiatimes.com/sso/isEmailIdAvailable?emailid=#emailid#&siteid=#siteid#";
	private static String activateUserURL = "http://jsso.indiatimes.com/sso/ActivateEmailId?hashcode=#hashcode#&siteid=#siteid#";
	private static String siteid = "ef55d27920fbf01f58c0da43430f5c6a";
	
	

	public static String addUser(Map<String, String> userMap) throws Exception {

		String addURL=new String(addUserURL);
		for(String key:userMap.keySet()){
			addURL = addURL.replace("#"+key+"#", userMap.get(key));
		}
		addURL = addURL.replace("#siteid#", siteid);

		String extData = MailerUtil.getDataRecursively(addURL, 1);
		String verificationCode =null;
		
		if(extData.indexOf("<VerificationCode>")!=-1 && extData.indexOf("Error") == -1){
			int beginIndex = extData.lastIndexOf("<VerificationCode>")+"<VerificationCode>".length();
			int endIndex = extData.lastIndexOf("</VerificationCode>");
			System.out.println("beginIndex : " + beginIndex + " , endIndex" + endIndex);
			verificationCode = extData.subSequence(beginIndex, endIndex).toString();
		}else{
			verificationCode = "ERROR-ERR_MS:"+extData;
		}
		return verificationCode;
	}


	private static Map<String, String> getUserMap(String emailId, String password) {

		Map m = new HashMap();


		m.put("emailid",emailId );

		String tmp[] = emailId.split("@");

		String fullname = tmp[0].replaceAll("\\P{L}", "");

		String firstName=fullname;
		String lastName=fullname;



		if(password.length()<6){
			password = password+"45";
		}

		if(password.length()>=14){
			password = password.substring(0, 13);
		}


		m.put("firstname", firstName);
		m.put("lastname", lastName);
		m.put("password", password);
		
		return m;
	}

	public static boolean isValidUser(String ssoId) throws Exception {
		String newisValidUserURL = isValidUserURL;
		newisValidUserURL = newisValidUserURL.replace("#emailid#", ssoId);
		newisValidUserURL = newisValidUserURL.replace("#siteid#", siteid);
		try {
			String extData = MailerUtil.getDataRecursively(newisValidUserURL,1 );
			if (extData.indexOf("<status>0</status>") != -1) {
				return true;
			}
		} catch (Exception e) {

			return false;
		}
		return false;
	}


	public static boolean activateUser(Map<String, String> userMap) throws Exception {

		String activateURL=new String(activateUserURL);
		for(String key:userMap.keySet()){
			activateURL = activateURL.replace("#"+key+"#", userMap.get(key));
		}
		activateURL = activateURL.replace("#siteid#", siteid);
		//System.out.println(activateURL);
		String extData = MailerUtil.getDataRecursively(activateURL,1 );
		if (extData.indexOf("<status>true</status>") != -1) {
			return true;
		}
		return false;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FileInputStream fis;

		try {
			
			fis = new FileInputStream("/opt/svn/docs/Subscription.xls");
			//fis = new FileInputStream("E:/GN_Subscription.xls");
			//fis = new FileInputStream("/opt/jcms/TJusers/Timesjobs_subscribers.xls");
            //fis = new FileInputStream("/opt/jcms/topNlData/TOI_topNL.xls");
			

			List<Map<String, Object>> list = SendPalUserReader.getRecordsList(fis);
			int count = 1;
			for (Map<String, Object> map : list) {
				String emaild = (String)map.get("emailaddress");
				emaild = emaild.trim();
				String channel = (String)map.get("channel");
				String isVerified = (String)map.get("isVerified");
				if (channel != null && emaild != null) {

					String url4Html = ADD_USER;
					url4Html = url4Html.replaceAll("#emailid", emaild);
					url4Html = url4Html.replaceAll("#verified", "true");
					
					url4Html = changeURL(url4Html,channel);
					
					try {
						
						boolean isSSOUser = isValidUser(emaild);
						if (!isSSOUser) {
							String verificationCode  = addUser(getUserMap(emaild, "12345"));
							Map<String, String> verificationCodeMap = new HashMap<String, String>();
							verificationCodeMap.put("hashcode", verificationCode);
							boolean ssoUserActivated = activateUser(verificationCodeMap);
							if (ssoUserActivated) {
								addRecord(url4Html);								
								System.out.println(count++ + " , email  : " + emaild);
							} else {
								System.out.println("Invalid User :" + emaild);
							}
						} else {
							addRecord(url4Html);
							
							System.out.println(count++ + " , email : " + emaild);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}



				}


			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	
	private static String changeURL(String url4Html, String channel) {
		if ("techradar".equalsIgnoreCase(channel)) {
			return  url4Html.replace("#domain", "http://in.techradar.com").replace("#host", "278").replace("#nlid", "1022").replace("#sch", "1");
		} else if ("lifehacker".equalsIgnoreCase(channel)) {
			return  url4Html.replace("#domain", "http://www.lifehacker.co.in").replace("#host", "257").replace("#nlid", "1020").replace("#sch", "1");
		} else if ("Gizmodo".equalsIgnoreCase(channel)) {
			return  url4Html.replace("#domain", "http://www.gizmodo.in").replace("#host", "256").replace("#nlid", "1021").replace("#sch", "1");
		} else if ("bi".equalsIgnoreCase(channel)) {
			return  url4Html.replace("#domain", "http://businessinsider.in").replace("#host", "261").replace("#nlid", "1007").replace("#sch", "2");
		} else if ("adageindia".equalsIgnoreCase(channel)) {
			return  url4Html.replace("#domain", "http://www.adageindia.in").replace("#host", "283").replace("#nlid", "1017").replace("#sch", "1");
		}
		else if ("Timesjob".equalsIgnoreCase(channel)) {
			url4Html = url4Html +"&day=4";
			return  url4Html.replace("#domain", "http://content.timesjobs.com").replace("#host", "316").replace("#nlid", "1028").replace("#sch", "1").replace("frequency=1", "frequency=2");
		}
        else if ("Timesofindia".equalsIgnoreCase(channel)) {            
            return  url4Html.replace("#domain", "http://timesofindia.indiatimes.com").replace("host=#host", "").replace("#nlid", "1001").replace("#sch", "1");
        } else if ("gadgetsnow".equalsIgnoreCase(channel)) {            
            return  url4Html.replace("#domain", "http://www.gadgetsnow.com").replace("#host", "307").replace("#nlid", "1025").replace("#sch", "1");
        }

		return url4Html;
	}

	private static void addRecord(String url4Html) {

		try {

			String sData = MailerUtil.getDataRecursively(url4Html, 6);

			if (sData != null) {
				System.out.println(sData);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} 
	}



}
