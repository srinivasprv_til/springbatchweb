package com.times.common.util;

import java.io.File;
import java.nio.file.Files;

import com.times.common.dao.MongoRestAPIService;

public class Job2 {
	public static void main(String[] args) throws Exception {
		String[] files = {"2016 October 18.pdf" };
		
		for (int i = 0; i < files.length; i++) {
			String file = files[i];
			File pdfFile = new File("/Users/arun.sharma2/jpdftest24Oct/" + file);
			File targetZipFile = new File("/Users/arun.sharma2/jpdftest24Oct/" + file + ".zip");
			byte[] content = Files.readAllBytes(pdfFile.toPath());
			(new EpaperOptimizerJpdf()).optimize(content, targetZipFile);
			System.out.println(MongoRestAPIService.addFile(Constant.EPAPER_FILE_COLLECTION, targetZipFile));
		}
	}

}
