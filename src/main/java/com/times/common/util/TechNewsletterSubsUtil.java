package com.times.common.util;

/**
 * 
 */

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.times.common.dao.MongoRestAPIService;
import com.times.mailer.model.Subscription;


/**
 * 
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.TechNewsletterSubsUtil"
 * @author Rajeev Khatri
 * 
 */
public class TechNewsletterSubsUtil {
	

	private final static Logger logger = Logger.getLogger(TechNewsletterSubsUtil.class);
	public static void main(String[] args) throws FileNotFoundException {	
		
		fetchRecord();
	}
	
	

	private static void fetchRecord() {		

		Mongo mongo = null;
		try {			

			int perPage = 50;
			int pageNo = 1;
			int offset = 0;
			BasicDBObject filter = new BasicDBObject();
			filter.put("ID", "1005");
			//filter.append("V_NL", false);

			DBObject query = new BasicDBObject("$elemMatch", filter);	

			BasicDBObject finalQuery = new BasicDBObject();
			finalQuery.put("NL_MAP", query);
			
			/*BasicDBObject filter4 = new BasicDBObject();
			filter4.append("V_USR", true);
			
			BasicDBList and = new BasicDBList();
			and.add(finalQuery);
			and.add(filter4);
			
			
			filter = new BasicDBObject("$and", and);*/


			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", "0");

			int totalUser = (int) MongoRestAPIService.getCount("newslettersubscription", finalQuery);
			System.out.println(" ++++++++++++++++ TOTAL USER +++++++++++++" + totalUser);
			int loop = totalUser / perPage + 1;
			int count = 1;
			for (int i =1; i <= loop; i++)  {

				List<DBObject> dbObjectList = MongoRestAPIService.getList("newslettersubscription", finalQuery , optionMap);		

				if (dbObjectList != null) {
					for (DBObject objDB : dbObjectList) {
						boolean isUpdateRequired = false;
						boolean isGNExist = false;
						List<DBObject> nlListMap = (List<DBObject>) objDB.get(Subscription.NEWS_LETTER_MAP);
						if (nlListMap != null) {
							
							for(DBObject db : nlListMap){
								if (db.get("ID") != null && db.get("ID").toString().equalsIgnoreCase("1025")
										) {
									isGNExist = true;
									break;
								}
							}
							
							for(DBObject db : nlListMap){
								if (db.get("ID") != null && db.get("ID").toString().equalsIgnoreCase("1005") && db.get("V_NL") != null 
										&& objDB.get("E_ID") != null 
										) {	
									if (!isGNExist) {
										db.put("V_NL", true);
										db.put("ID", "1025");
										db.put("H_ID", "307");
									} else {
										nlListMap.remove(db);
									}
									isUpdateRequired = true;
									break;
								}

							}
							if (isUpdateRequired) {
								String emaild = (String) objDB.get("E_ID");
								BasicDBObject query1 = new BasicDBObject();		
								query1.put("E_ID", emaild);

								BasicDBObject newDocument = new BasicDBObject();
								newDocument.append("$set", objDB);

								MongoRestAPIService.updateWithPost("newslettersubscription", query1, newDocument,true);
								System.out.println(emaild + "\t" + count++ );
							}
						}

					}				

				}
				offset = offset + perPage;
				optionMap.put("skip", String.valueOf(offset));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mongo != null) {
				mongo.close();
			}
		}
	}




}
