package com.times.common.util;

/**
 * 
 */

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.util.StringUtils;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.MailSender;


/**
 * This service is used to adhoc verify unsubscribed user and send activation mail.
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.TOIVerificationUtil"
 * @author Rajeev Khatri
 * 
 */
public class TOIVerificationUtil {

	public static final String FROM = "The Times Of India <mailerservice@timesofindia.com>";
	public static final String replyBackId = "The Times Of India <mailerservice@timesofindia.com>";
	
	public static final String host = "nmailer.indiatimes.com";
	public static final String conentPath = "timesofindia@bounce.indiatimes.com";
	
	private static String subject = "Your TOI newsletter subscription has been activated";
	
	private static String TEMPLATE = "http://timesofindia.indiatimes.com/toinlverification.cms";
	
	private static String UNSUBS = "http://timesofindia.indiatimes.com/newsletter/unsubscribetoinl/?eid=#email";

	private final static Logger logger = Logger.getLogger(TOIVerificationUtil.class);
	public static void main(String[] args) throws FileNotFoundException {		
		fetchRecord();
	}

	private static void fetchRecord() {


		Mongo mongo = null;
		try {			
			MailSender mailSender =  new MailSender(host,  FROM,  conentPath,  replyBackId);
			int perPage = 50;
			
			int offset = 0;

			
			BasicDBObject query = new BasicDBObject();	
			query.put("V_USR", false);

			BasicDBObject querySt = new BasicDBObject();	
			querySt.put("ST", "0");

			BasicDBObject query1 = new BasicDBObject("NL_MAP.ID", "1001");		
			BasicDBObject query2 = new BasicDBObject("NL_MAP.ID", "1002");		
			BasicDBObject query3 = new BasicDBObject("NL_MAP.ID", "1003");		
			BasicDBObject query4 = new BasicDBObject("NL_MAP.ID", "1004");	
			BasicDBObject query5 = new BasicDBObject("NL_MAP.ID", "1005");
			BasicDBObject query6 = new BasicDBObject("NL_MAP.ID", "1006");
			BasicDBObject query7 = new BasicDBObject("NL_MAP.ID", "1018");
			BasicDBObject query8 = new BasicDBObject("NL_MAP.ID", "1019");
			BasicDBList or = new BasicDBList();
			or.add(query1);
			or.add(query2);
			or.add(query3);
			or.add(query4);
			or.add(query5);
			or.add(query6);
			or.add(query7);
			or.add(query8);


			DBObject ORquery = new BasicDBObject("$or", or);
			BasicDBList and = new BasicDBList();

			and.add(query);
			and.add(querySt);		
			and.add(ORquery);
			

			DBObject finalQuery = new BasicDBObject("$and", and);


			long totalSubscriber = MongoRestAPIService.getCount("newslettersubscription", finalQuery);
			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", "0");

			long loop =(totalSubscriber/50) + 1;
			int count = 1;
			for (int i =1; i <= loop; i++)  {

				List<DBObject> dbObjectList = MongoRestAPIService.getList("newslettersubscription", finalQuery , optionMap);		

				if (dbObjectList != null) {
					for (DBObject objDB : dbObjectList) {
						if (objDB.get("E_ID") != null && objDB.get("H_CODE") != null && !"null".equalsIgnoreCase(objDB.get("H_CODE").toString()) ) {
							if (objDB.get("E_ID").toString().indexOf("mailinator.com") == -1 && objDB.get("E_ID").toString().indexOf("maildrop.cc")== -1 
									&& objDB.get("E_ID").toString().indexOf("mailtothis.com") == -1 && objDB.get("E_ID").toString().indexOf("monumentmail.com") == -1
									&& objDB.get("E_ID").toString().indexOf("reallymymail.com") == -1 && objDB.get("E_ID").toString().indexOf("sendspamhere.com") == -1) {			

									String email = (String) objDB.get("E_ID");	
									String id = objDB.get("_id").toString();	
									
																	
									boolean isMailSend = true;
									try {
										isMailSend = mailSender.sendMessage(email, subject, getContnet(TEMPLATE));
									}
									catch (Exception e) {
										System.out.println("Exception caught while sending mail to  " + email );
									}
									
									if (!isMailSend) {
										getContnet(UNSUBS.replace("#email", email));
									} else {
										updateRecord(email, id);
									}

								}

							}
						}


					}	
				
				offset = offset + perPage;
				optionMap.put("skip", String.valueOf(offset));
				System.out.println("offset: " + offset );

				}
				
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mongo != null) {
				mongo.close();
			}
		}
	}
	
	private static void updateRecord(String email, String id) {
		Mongo mongo = null;
		try {

			BasicDBObject query = new BasicDBObject();		
			//query.put("E_ID", email);
			
			ObjectId cid= new ObjectId(id);
			
			query.put("_id", cid);
			
			


			BasicDBObject datatoupdate = new BasicDBObject();					
			datatoupdate.put("V_USR", true);
			datatoupdate.put("ST", "1");

			BasicDBObject newDocument = new BasicDBObject();
			newDocument.append("$set", datatoupdate);

			boolean isUpdate = MongoRestAPIService.update("newslettersubscription", query, newDocument,true);
			System.out.println("email: " + email + "\t isUpdate: " + isUpdate);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mongo != null) {
				mongo.close();
			}
		}
	}


	private static String getContnet(String url4Html) {
		
		String content = MailerUtil.getDataRecursively(url4Html, 1);
		
		return content;
	}


}
