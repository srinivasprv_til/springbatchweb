package com.times.common.util;

import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.times.common.constant.UserFeedConstant;
import com.times.common.model.UserActivity;
import com.times.common.model.UserComment;
import com.times.common.model.UserFeed;

public class OfflineFeedUtilities {
	
	private final static Logger logger = LoggerFactory.getLogger(OfflineFeedUtilities.class);
	static final int httpTimeout = 2000;
	static final int httpTimeoutForSolr = 12000;
	private static final String updateURL = "update/#collection?queryJson=#query&rowJson=#rowJson";
	private static final String getURL = "get/#collection?query=#query";
	public static final String OFFLINE_USER_FEED_COLLECTION = "offlineuserfeed";
	public static String MONGO_REST_API_URL = "http://mratest.indiatimes.com/mra/";
	
		
	public static String getExtData(String strURL) {
		HttpClient hClient = HttpClients.custom()
				.setRetryHandler(new DefaultHttpRequestRetryHandler(3, false))
				.build();
		RequestConfig config = RequestConfig.custom()
				.setMaxRedirects(2)
				.setConnectTimeout(5000)
				.setSocketTimeout(5000)
				.build();
		
		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try {
			strURL = UriUtils.encodeQuery(strURL, StandardCharsets.UTF_8.toString());
			gMethod = new HttpGet(strURL);
			gMethod.setConfig(config);
			hResponse = hClient.execute(gMethod);
			String extData = EntityUtils.toString(hResponse.getEntity(), StandardCharsets.UTF_8);
			return extData;
		} catch (Exception e) {
			logger.error("Error in getExtData : "+e.getMessage()+ " | strURL : "+strURL);
		} finally {
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	}
	

	public static int getExtStatus(String strURL) {
        HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
        RequestConfig config = RequestConfig.custom()
                     .setConnectTimeout(httpTimeout)
                     .setSocketTimeout(httpTimeout)
                     .build();
        HttpRequestBase gMethod = null;
        HttpResponse httpResponse = null;
        int status = 0;
        try{
               gMethod = new HttpGet(strURL);
               gMethod.setConfig(config);
               httpResponse = hClient.execute(gMethod);
               status = httpResponse.getStatusLine().getStatusCode();
              
		}catch(Exception e){
			logger.error("Error in getExtStatus : "+e.getMessage()+ " | strURL : "+strURL);
		}
        return status;
	}
	
	public static String convertDateToStringForCache(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");//2014-08-21 16:40:10.13
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		String formatted = null;
		try {
			formatted = dateFormat.format(date);
		} catch (Exception e) {
			logger.error("Error in convertDateToStringForCache : "+e.getMessage());
		}
		return formatted;
	}
	
	public static String getExtDataForencode(String strURL, String queryString) {
		HttpClient hClient = HttpClients.custom()
				.setRetryHandler(new DefaultHttpRequestRetryHandler(3, false))
				.build();
		RequestConfig config = RequestConfig.custom()
				.setConnectTimeout(httpTimeoutForSolr)
				.setSocketTimeout(httpTimeoutForSolr)
				.build();
		HttpRequestBase gMethod = null;
		HttpResponse httpResponse = null;
		try{
			strURL = strURL.trim();
			if(strURL.indexOf("?") != -1){
				strURL = strURL.substring(0,strURL.indexOf("?"))+ UriUtils.encodeQuery(queryString, "UTF-8");
			}else{
				strURL = strURL+"?"+ UriUtils.encodeQuery(queryString, "UTF-8");
			}
			gMethod = new HttpGet(strURL);
			gMethod.setConfig(config);
			httpResponse = hClient.execute(gMethod);
			int status = httpResponse.getStatusLine().getStatusCode();
			if(status == HttpStatus.SC_OK){
				String extData = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8);
				if(extData==""){
					extData = strURL;
				}
				return extData;
			}
		} catch(Exception e){
			logger.error("Error in getExtDataForencode : "+e.getMessage()+ " | strURL : "+strURL);
		} finally{
			HttpClientUtils.closeQuietly(httpResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	}
	
	public static UserFeed getDBObjectByUserFeed(DBObject dbObject) throws Exception {
		UserFeed userFeed = new UserFeed();
		DBObject userFeedDBObject = new BasicDBObject();
		userFeed.setId(String.valueOf(dbObject.get(UserFeedConstant.ID)));
		userFeed.setUserId(String.valueOf(dbObject.get(UserFeedConstant.USERID)));
		userFeed.setFromaddress(String.valueOf(dbObject.get(UserFeedConstant.FROMADDRESS)));
		userFeed.setFromname(String.valueOf(dbObject.get(UserFeedConstant.FROMNAME)));
		userFeed.setLocation(String.valueOf(dbObject.get(UserFeedConstant.LOCATION)));
		
		if(dbObject.get(UserFeedConstant.ACTIVITY)!=null){
			List<DBObject> activityDocuments =  (List<DBObject>) dbObject.get(UserFeedConstant.ACTIVITY);
			List<UserActivity> userActivityList = new ArrayList<UserActivity>();
			for (DBObject activityDBObject : activityDocuments ) {
				userActivityList.add(getDBObjectByUserActivity(activityDBObject));
			}
			userFeed.setUserActivityList(userActivityList);
		}
		return userFeed;
	}
	
	public static UserActivity getDBObjectByUserActivity(DBObject activityDBObject) throws Exception {
		UserActivity userActivity = new UserActivity();
			userActivity.setMsid(Integer.parseInt(String.valueOf(activityDBObject.get(UserFeedConstant.MSID))));
			userActivity.setWishlist(Short.parseShort(String.valueOf(activityDBObject.get(UserFeedConstant.WISHLIST))));
			userActivity.setUserRating(Double.parseDouble(String.valueOf(activityDBObject.get(UserFeedConstant.USERRATING))));
			userActivity.setUserUpvote(Long.parseLong(String.valueOf(activityDBObject.get(UserFeedConstant.USERUPVOTE))));
			userActivity.setBeenThere(Short.parseShort(String.valueOf(activityDBObject.get(UserFeedConstant.BEENTHERE))));
			if(activityDBObject.get(UserFeedConstant.COMMENT)!=null){
				UserComment userComment=getDBObjectByUserComment((DBObject)activityDBObject.get(UserFeedConstant.COMMENT));
				userActivity.setUserComment(userComment);
			}
		return userActivity;
	}

	public static UserComment getDBObjectByUserComment(DBObject commentDBObject) throws Exception {
				UserComment userComment= new UserComment();			
				userComment.setCommentText(String.valueOf(commentDBObject.get(UserFeedConstant.COMMENTTEXT)));
				userComment.setHostId(Short.parseShort(String.valueOf(commentDBObject.get(UserFeedConstant.HOSTID))));
				userComment.setParentmsid(Integer.parseInt(String.valueOf(commentDBObject.get(UserFeedConstant.PARENTMSID))));
				userComment.setCommentTime((Date) commentDBObject.get(UserFeedConstant.COMMENTTIME));
				userComment.setLangId(Short.parseShort(String.valueOf(commentDBObject.get(UserFeedConstant.LANGID))));
		return userComment;
	}

	/*	public static List<DBObject> getList(String collection, DBObject query) {
	String extData = null;
	String strURL = null;
	List<DBObject> dbObj = null;
	strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
			"#collection", collection);
	strURL = MONGO_REST_API_URL + strURL;
	extData = getExtData(strURL);
	logger.info("URL:{"+strURL+"} extData{"+extData+"}");
	if(extData != null && !"".equals(extData)){
		Object dbObjectRes = JSON.parse(extData);
		if (dbObjectRes instanceof List<?>) {
				return ((List<DBObject>) dbObjectRes);
		} 
	}
	return null;
}

public static List<DBObject> getAllList() {
	String extData = null;
	String strURL = null;
	String collection = OFFLINE_USER_FEED_COLLECTION;
	List<DBObject> dbObj = null;
	strURL = getURL.replaceAll("#query", "").replaceAll("#collection", collection);
	strURL = MONGO_REST_API_URL + strURL;
	extData = getExtData(strURL);
	logger.info("URL:{"+strURL+"} extData{"+extData+"}");
	if(extData != null && !"".equals(extData) && !"".equals(strURL)){
		Object dbObjectRes = JSON.parse(extData);
		if (dbObjectRes instanceof List<?>) {
				return ((List<DBObject>) dbObjectRes);
		} 
	}
	return null;
}

public static boolean update(String collection, DBObject queryJson,	DBObject rowJson) {
	String strURL = null;
	strURL = updateURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))
			.replaceAll("#collection", collection).replaceAll("#query",
					Matcher.quoteReplacement(queryJson.toString()));
	strURL = MONGO_REST_API_URL + strURL;
	if (getExtData(strURL) != null && !"".equals(strURL)) {
		return true;
	}
	return false;
}*/


}
