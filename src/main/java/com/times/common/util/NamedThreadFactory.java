package com.times.common.util;

import java.util.concurrent.ThreadFactory;

public class NamedThreadFactory implements ThreadFactory {
	private String threadPoolPrefix;
    private int counter = 0;
    private int workType = 0;

   public Thread newThread(Runnable r) {
     return new Thread(r, threadPoolPrefix+"_"+workType + "-" + counter++);
   }

	public String getThreadPoolPrefix() {
		return threadPoolPrefix;
	}
	
	public void setThreadPoolPrefix(String threadPoolPrefix) {
		this.threadPoolPrefix = threadPoolPrefix;
	}

	public int getWorkType() {
		return workType;
	}

	public void setWorkType(int workType) {
		this.workType = workType;
	}	
   
}
