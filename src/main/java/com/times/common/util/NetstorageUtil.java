/**
 * 
 */
package com.times.common.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;

/**
 * This utility class is used to upload the File on FTP location.
 * 
 * @author Ranjeet.Jha
 *
 */
public class NetstorageUtil {

	private static final Log log = LogFactory.getLog(NetstorageUtil.class); 
	
	
	//private static HttpClientParams clientParams = new HttpClientParams();
	private static final int httpTimeout = 10000; //10 sec
	private static final int httpConnectionTimeout =2000;
	
	/*static {
		clientParams.setParameter(HttpClientParams.MAX_REDIRECTS, "5");
		clientParams.setParameter(HttpClientParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, false));
		clientParams.setConnectionManagerTimeout(httpConnectionTimeout);		
		clientParams.setSoTimeout(httpTimeout);
	}*/
	 
	 /**
	 * @param ftp
	 */
	public static void disconnect(FTPClient ftp){
        if (ftp.isConnected()) {
            try {
                ftp.logout();
                ftp.disconnect();
            } catch (IOException e) {
                // do nothing as file is already saved to server
            	log.error("IOException caught while connecting , url=" +  " , msg="+ e.getMessage());
            }
        }
    }
	 
	public static String getContentByInputStream(InputStream is)	throws IOException, UnsupportedEncodingException {
		String outData;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();  
		byte[] byteArray = new byte[1024];  
		int count = 0;  
		while ((count = is.read(byteArray, 0, byteArray.length)) > 0) {
			outputStream.write(byteArray, 0, count);
		}
		outData = new String(outputStream.toByteArray(), "UTF-8");
		return outData;
	}
	
	/* public static void main(String[] args) {
		 String server = "timescricket.upload.akamai.com";
	        int port = 21;
	        String user = "toiupload";
	        String pass = "user4upl0ad";
	        //test(null);
	        FTPClient ftpClient = new FTPClient();
	        try {
	 
	            ftpClient.connect(server, port);
	            ftpClient.login(user, pass);
	            ftpClient.enterLocalPassiveMode();
	            ftpClient.changeWorkingDirectory("/92522/TOI/city");
	            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
	 
	            
	            String content = getData("http://timesofindia.indiatimes.com/city");
	            String firstRemoteFile = "myindex.html";
	            //InputStream inputStream = new FileInputStream(firstLocalFile);
	            InputStream inputStream = new ByteArrayInputStream(content.getBytes());
	            log.debug("Start uploading first file");
	            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
	            inputStream.close();
	            if (done) {
	            	log.debug("The first file is uploaded successfully.");
	            }
	 
	            // APPROACH #2: uploads second file using an OutputStream
	            File secondLocalFile = new File("E:/Test/Report.doc");
	            String secondRemoteFile = "test/Report.doc";
	            inputStream = new FileInputStream(secondLocalFile);
	 
	            content = getData("http://timesofindia.indiatimes.com/city");
	            String secondRemoteFile = "myindex2.html";
	            inputStream = new ByteArrayInputStream(content.getBytes());
	            
	            log.debug("Start uploading second file");
	            OutputStream outputStream = ftpClient.storeFileStream(secondRemoteFile);
	            byte[] bytesIn = new byte[4096];
	            int read = 0;
	 
	            while ((read = inputStream.read(bytesIn)) != -1) {
	                outputStream.write(bytesIn, 0, read);
	            }
	            inputStream.close();
	            outputStream.close();
	 
	           String fileContent = getData("http://timesofindia.indiatimes.com/city");
	            BufferedReader reader = null;
	    		OutputStreamWriter out = null;
	    		OutputStream os = new BufferedOutputStream(ftpClient.storeFileStream("myname.html"));
				out = new OutputStreamWriter(os);
				// out = new OutputStreamWriter(os,"UTF-8");
				out.write(fileContent);
				out.flush();
				out.close();
				os.close();
	            
	            boolean completed = ftpClient.completePendingCommand();
	            if (completed) {
	            	log.debug("The second file is uploaded successfully.");
	            }
	 
	        } catch (IOException ex) {
	        	log.error("Error: " + ex.getMessage());
	            ex.printStackTrace();
	        } finally {
	            try {
	                if (ftpClient.isConnected()) {
	                    ftpClient.logout();
	                    ftpClient.disconnect();
	                }
	            } catch (IOException ex) {
	                ex.printStackTrace();
	            }
	        }
	    }*/
	 
	
}
