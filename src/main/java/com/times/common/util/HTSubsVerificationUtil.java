package com.times.common.util;

/**
 * 
 */

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.MailSender;
import com.times.mailer.model.Subscription;


/**
 * This service is used to adhoc verify unsubscribed user and send activation mail.
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.HTSubsVerificationUtil"
 * @author Ranjeet.Jha
 * 
 */
public class HTSubsVerificationUtil {

	private final static String subject_HT = "Subscription Activated for HappyTrips Daily Newsletter";
	public static final String templateURL = "http://www.happytrips.com/unverified_subscriber.cms";

	public static final String FROM = "HappyTrips <mailerservice@happytrips.com>";
	public static final String host = "nmailer.indiatimes.com";	
	public static final String conentPath = "timesofindia@bounce.indiatimes.com";
	public static final String replyBackId = "HappyTrips <mailerservice@happytrips.com>";

	private final static Logger logger = Logger.getLogger(HTSubsVerificationUtil.class);
	public static void main(String[] args) throws FileNotFoundException {		
		updateRecord();
	}

	private static void updateRecord() {
		MailSender mailSender = null;

		String mailBody = MailerUtil.getDataRecursively(templateURL, 1);
		if (mailBody == null || "".equals(mailBody.trim())) {
			mailBody = MailerUtil.getDataRecursively(templateURL, 1);
		}
		mailSender = new MailSender(host,  FROM,  conentPath,  replyBackId);

		Mongo mongo = null;
		try {			

			int perPage = 50;
			int pageNo = 1;
			int offset = 0;
			BasicDBObject filter = new BasicDBObject();
			filter.put("ID", "1031");
			filter.append("V_NL", false);

			DBObject query = new BasicDBObject("$elemMatch", filter);	

			BasicDBObject finalQuery = new BasicDBObject();
			finalQuery.put("NL_MAP", query);


			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", "0");

			int loop =200;
			int count = 1;
			for (int i =1; i <= loop; i++)  {

				List<DBObject> dbObjectList = MongoRestAPIService.getList("newslettersubscriptiontravel", finalQuery , optionMap);		

				if (dbObjectList != null) {
					for (DBObject objDB : dbObjectList) {

						List<DBObject> nlListMap = (List<DBObject>) objDB.get(Subscription.NEWS_LETTER_MAP);
						if (nlListMap != null) {
							for(DBObject db : nlListMap){
								if (db.get("ID") != null && db.get("ID").toString().equalsIgnoreCase("1031") && db.get("V_NL") != null 
										&& db.get("V_NL").toString().equalsIgnoreCase("false") && objDB.get("E_ID") != null 
										) {							

									System.out.print(objDB.get("E_ID"));								

									String emaild = (String) objDB.get("E_ID");
									BasicDBObject query1 = new BasicDBObject();		
									query1.put("E_ID", emaild);
									query1.put("NL_MAP.ID", "1031");

									BasicDBObject datatoupdate = new BasicDBObject();					
									datatoupdate.put("NL_MAP.$.V_NL", true);

									BasicDBObject newDocument = new BasicDBObject();
									newDocument.append("$set", datatoupdate);
									MongoRestAPIService.update("newslettersubscriptiontravel", query1, newDocument,true);


									String contentBody = mailBody.replace("#emailid#", emaild);
									mailSender.sendMessage(emaild, subject_HT , contentBody);

									System.out.println(count++ + " , email sent to : " + emaild);
								}

							}
						}


					}				

				}
				offset = offset + perPage;
				optionMap.put("skip", String.valueOf(offset));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mongo != null) {
				mongo.close();
			}
		}
	}




}
