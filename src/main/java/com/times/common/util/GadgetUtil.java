package com.times.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GadgetUtil {

	private static String getMonth(String d) {
		String result = null;
		String helper = d.substring(0, 3);
		if (helper.equalsIgnoreCase("Jan")) {
			result = "01";
		}
		if (helper.equalsIgnoreCase("Feb")) {
			result = "02";
		}
		if (helper.equalsIgnoreCase("Mar")) {
			result = "03";
		}
		if (helper.equalsIgnoreCase("Apr")) {
			result = "04";
		}
		if (helper.equalsIgnoreCase("May")) {
			result = "05";
		}
		if (helper.equalsIgnoreCase("Jun")) {
			result = "06";
		}
		if (helper.equalsIgnoreCase("Jul")) {
			result = "07";
		}
		if (helper.equalsIgnoreCase("Aug")) {
			result = "08";
		}
		if (helper.equalsIgnoreCase("Sep")) {
			result = "09";
		}
		if (helper.equalsIgnoreCase("Oct")) {
			result = "10";
		}
		if (helper.equalsIgnoreCase("Nov")) {
			result = "11";
		}
		if (helper.equalsIgnoreCase("Dec")) {
			result = "12";
		}
		return result;
	}

	private static String getQuater(String q) {
		String result = null;
		if (q.equals("1Q") || q.equals("Q1")) {
			result = "-01-01";
		}
		if (q.equals("2Q") || q.equals("Q2")) {
			result = "-04-01";
		}
		if (q.equals("3Q") || q.equals("Q3")) {
			result = "-07-01";
		}
		if (q.equals("4Q") || q.equals("Q4")) {
			result = "-10-01";
		}
		return result;
	}

	public static Date getProperDateFormat(Object dateString) {
		
		if(dateString == null || "".equals(String.valueOf(dateString)) || "null".equalsIgnoreCase(String.valueOf(dateString)))
			return null;
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String[] str_array = dateString.toString().split(",");

		String s1 = "";
		String s2 = "";
		String s2_helper = "";
		if (str_array[0] != null) {
			s1 = str_array[0];
		}
		if (str_array.length > 1) {
			s2_helper = str_array[1];
		}
		if (s2_helper != "") {
			s2 = s2_helper.substring(1);
		}
		if (s1.isEmpty() && s2.isEmpty()) {
			return null;
		}

		Date date;
		try {

			if (s1.equals("Exp")) {
				date = formatter.parse("2015-01-01");
				return date;
			}
			if (s1.equals("Not officially announced yet")
					|| s1.equals("Not announced yet")) {
				date = formatter.parse("2016-05-01");
				return date;
			}
			if (s1.equals("1Q") || s1.equals("2Q") || s1.equals("3Q")
					|| s1.equals("4Q")) {
				date = formatter.parse(s2 + getQuater(s1));
				return date;
			}
			if (s2.equals("1Q") || s2.equals("2Q") || s2.equals("3Q")
					|| s2.equals("4Q")) {
				date = formatter.parse(s1 + getQuater(s2));
				return date;
			}
			if (s1.equals("Q1") || s1.equals("Q2") || s1.equals("Q3")
					|| s1.equals("Q4")) {
				date = formatter.parse(s2 + getQuater(s1));
				return date;
			}
			if (s2.equals("Q1") || s2.equals("Q2") || s2.equals("Q3")
					|| s2.equals("Q4")) {
				date = formatter.parse(s1 + getQuater(s2));
				return date;
			}
			if (getMonth(s1) != null && s2 != "") {
				date = formatter.parse(s2 + "-" + getMonth(s1) + "-01");
				return date;
			}
			if (s2 == "" && s1.length() == 4) {
				date = formatter.parse(s1 + "-01-01");
				return date;
			}
			if (s2 == "" && s1.length() > 4) {
				String[] arrayDate = s1.split("-");
				if (arrayDate.length == 3) {
					date = formatter.parse(s1);
					return date;
				} else {
					arrayDate = s1.split(" ");
					String day = null;
					String month = null;
					String year = null;
					for (String s : arrayDate) {
						if (s.length() < 3) {
							if (s.equals("?")) {
								break;
							}
							day = s;
						} else if (s.length() == 4) {
							year = s;
						} else {
							month = getMonth(s);
						}
					}
					if (day == null) {
						day = "01";
					}
					if (month == null) {
						month = "01";
					}
					if (year == null) {
						year = "2016";
					}
					if (getQuater(day) != null) {
						date = formatter.parse(year + getQuater(day));
						return date;
					}
					date = formatter.parse(year + "-" + month + "-" + day);
					return date;
				}
			}

			date = formatter.parse(s1 + "-" + getMonth(s2) + "-01");

		} catch(Exception e) {
			System.out.println("Error while parsing Date.. " + dateString + e);
			return null;
		}
		return date;
	}
	
	
	/**
	 * This method return SEO Compatible name for the product.
	 * This should also uniquely identifies the product, but for more accuracy, we have  {@link #getUniqueID getUniqueID} method.
	 * 
	 * Replaces '(', ')' and whitespaces to HYPHEN.
	 * Then replaces everything else apart from Digits, Lower/Upper case alphabets, +, and Hyphen(-) to BLANK.
	 * Then replace Multiple Hyphen to Single Hyphen.
	 * Then replace '+' to 'plus' string.
	 * Then delete '-' from first and last character, if present.
	 * 
	 * @param name
	 * @return
	 */
	public static String getUName(String name) {
		//Updated Name
//		StringBuilder uname = new StringBuilder(name.replaceAll("\\("," ").replaceAll("\\)", " ").replaceAll("\\s+","-").replaceAll("-+","-").replaceAll("\\+","plus"));
		StringBuilder uname = new StringBuilder(name.replaceAll("[\\(\\)\\s]", "-").replaceAll("[^\\dA-Za-z+-]", "").replaceAll("-+","-").replaceAll("\\+","plus"));
		if('-' == uname.charAt(0)) {
			uname.deleteCharAt(0);	
		} 
		if('-' == uname.charAt(uname.length()-1)) {
			uname.deleteCharAt(uname.length()-1);
		}

		return uname.toString();
	}
	
	
	/**
	 * This method identifies a gadget with a unique String.
	 * We keep only Digits and Alphabets as valid character to Uniquely identify a gadget.
	 * '+' is also considered as there may be two different gadgets with 'plus' appended in its name.
	 * e.g. 'HTC One X' and 'HTC One X+'
	 * 
	 * As check in current Data with BuyT feeds, Below are the special characters present in gadget name:
	 * .@/':,&;?*#_\"
	 * 
	 * @param name
	 * @return
	 */
	public static String getUniqueID(String name) {
		//Unique ID to identify a gadget
		return name.replaceAll("[^\\dA-Za-z+]", "").replaceAll("\\+","plus").toLowerCase();
//		StringBuilder uname = new StringBuilder(name.replaceAll("\\("," ").replaceAll("\\)", " ").replaceAll("\\s+","-").replaceAll("-+","-").replaceAll("\\+","plus"));
//		return uname.toString().replace("-", "").toLowerCase();
	}

	/*public static void main(String[] args) throws ParseException, IOException {
		// Open the file
		FileInputStream fstream = new FileInputStream(
				"/Users/siddhant.mittal/Desktop/testData.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		String strLine;

		// Read File Line By Line
		while ((strLine = br.readLine()) != null) {
			// Print the content on the console
			System.out.println(strLine);
			String[] dateToBeFound = strLine.split(":");
			System.out.println(getProperDateFormat(dateToBeFound[1].substring(
					2, dateToBeFound[1].length() - 2)));
		}

		// Close the input stream
		br.close();

		System.out.println(getProperDateFormat("Oct 2002"));
	}*/

}
