package com.times.common.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.jpedal.exception.PdfException;
import org.zeroturnaround.zip.ZipUtil;

import com.til.jcms.woff.Woff;
import com.times.common.dao.MongoRestAPIService;

public class EpaperOptimizerJpdf {
	String webPPath = "/home/tilsuper/libwebp-0.5.1-linux-x86-32/bin/cwebp";
//	String webPPath = "/Users/arun.sharma2/code/libwebp/examples/cwebp";

	public void optimize(byte[] pdfContent, File targetDestination) throws Exception {
		Path workingDirectory = convertToHtml5(pdfContent);

		// htmlFiles = *.html
		optimizeWorkingDirectory(targetDestination, workingDirectory);

	}

	private void optimizeWorkingDirectory(File targetDestination, Path workingDirectory)
	        throws IOException, InterruptedException, Exception {
		File[] htmlFiles = workingDirectory.toFile().listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".html") && !name.contains("index.html");
			}
		});

		// folders = htmlFiles.gsub('.html', '')
		File[] imageDirs = new File[htmlFiles.length];
		for (int i = 0; i < htmlFiles.length; i++) {
			imageDirs[i] = getImageDir(htmlFiles[i]);
		}

		// for each folder:
		for (int i = 0; i < imageDirs.length; i++) {
			// backgrounds = Files.walk(*.jpg, folder)
			List<Path> images = getImages(imageDirs[i].toPath());
			// convert each file to webp
			convertToWebp(images);
			// delete old file
			deleteFiles(images);
		}
		//
		// fontsFolder = 'fonts'
		List<Path> fonts = getFonts(workingDirectory);

		// fonts = fontsFolder.walk(*.woff)
		//
		// for each fonts:
		// convert to ttf
		convertToTTF(fonts);

		// and delete old fonts
		deleteFiles(fonts);

		// for each htmlFile:
		// change "woff" to "ttf" (twice for each font)
		// and change jpg to webp
		for (int i = 0; i < htmlFiles.length; i++) {
			processHtmlFile(htmlFiles[i]);
		}

		// Zip and Ship Yey!!
		ZipUtil.pack(workingDirectory.toFile(), targetDestination);
	}

	private Path convertToHtml5(byte[] pdfContent) throws IOException, PdfException {
		Path workingDirectory = Files.createTempDirectory(Paths.get("/home/tilsuper/epaper_workspace"), "epaper_");
//		Path workingDirectory = Files.createTempDirectory("epaper_");
		Jpdf2HtmlService htmlService = new Jpdf2HtmlService();
		// use jpdf2html to convert to folder
		htmlService.convertToPdf(pdfContent, workingDirectory.toFile());
		return workingDirectory;
	}
	

	private void processHtmlFile(File htmlFile) throws IOException {
		String htmlContent = readFile(htmlFile.toPath(), StandardCharsets.UTF_8);
//		Pattern pattern = Pattern.compile("(\\d)+/\\d+\\.jpg");
//		Matcher matcher = pattern.matcher(htmlContent);
//		while(matcher.find()) {
//			m
//		}
		String processedContent = htmlContent;
		for (int i = 1; i <= 50; i++) {
			processedContent = processedContent.replace(i + ".jpg", i + ".webp");
		}
		processedContent = processedContent.replace(".woff", ".ttf");
		processedContent = processedContent.replace("\"woff\"", "\"truetype\"");
		OpenOption options = StandardOpenOption.WRITE;
 		Files.write(htmlFile.toPath(), processedContent.getBytes(), options);
		

	}
	public boolean isFontFile(Path p) {
		String lowerCaseFileName = p.toString().toLowerCase();
		return lowerCaseFileName.endsWith(".woff");
		
	}

	private List<Path> getFonts(Path workingDirectory) throws IOException {
		List<Path> images = Files.walk(workingDirectory).filter(this::isFontFile).collect(Collectors.toList());
		return images;
	}

	private Path getFontsFolder(Path outputDirectory) {
		// TODO Auto-generated method stub
		return null;
	}

	private File getImageDir(File file) {
		String path = file.getAbsolutePath();
		String imageDir = FilenameUtils.concat(FilenameUtils.getFullPath(path), FilenameUtils.getBaseName(path));
		return new File(imageDir);
	}

	public boolean isImageFile(Path p) {
		String lowerCaseFileName = p.toString().toLowerCase();
		return (lowerCaseFileName.endsWith("jpg") || lowerCaseFileName.endsWith("png")
		        || lowerCaseFileName.endsWith("jpeg"));

	}

	private List<Path> getImages(Path targetDir) throws IOException {
		List<Path> images = Files.walk(targetDir).filter(this::isImageFile).collect(Collectors.toList());
		return images;
	}

	private void convertToWebp(List<Path> images) throws IOException, InterruptedException {
		Runtime r = Runtime.getRuntime();
		for (Path image : images) {
			Process p = r.exec(getWebpCommand(image));
			int status = p.waitFor();
			if (status != 0)
				throw new IOException();
		}
	}

	private void deleteFiles(List<Path> images) throws IOException {
		for (Path image : images) {
			Files.delete(image);
		}
	}

	private void changeSrc(List<Path> images, Path targetDir, String htmlFileName) throws IOException {
		Path htmlFile = Paths.get(targetDir.toString(), htmlFileName);

		String htmlText = readFile(htmlFile, Charset.forName("UTF-8"));
		String processedText = htmlText;
		for (Path image : images) {
			String name = image.getFileName().toString();
			String webpFileName = webpFileName(image.getFileName());
			System.out.println(String.format("changing image url from [%s] to [%s]", getImageTokenInHtml(name),
			        getImageTokenInHtml(webpFileName)));
			processedText = processedText.replaceAll(getImageTokenInHtml(name), getImageTokenInHtml(webpFileName));
		}

		OpenOption options = StandardOpenOption.WRITE;
		Files.write(htmlFile, processedText.getBytes(), options);
	}

	private String webpFileName(Path image) {
		return image.toString().replace(".jpg", ".webp");
	}

	static String readFile(Path file, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(file);
		return new String(encoded, encoding);
	}

	private String getWebpCommand(Path image) {
		System.out.println(image);
		String cmd = webPPath + " " + image.toString() + " -o " + webpFileName(image);
		System.out.println("webp command : " + cmd);
		return cmd;
	}

	private String getImageTokenInHtml(String name) {
		return String.format("src=\"%s\"", name);
	}

	private void convertToTTF(List<Path> fonts) throws Exception {
		for (Path font : fonts) {
			Path outputFile = Paths.get(font.toString().replace(".woff", ".ttf"));
			byte[] woffData = Files.readAllBytes(font);
			byte[] ttfData = Woff.woffDecode(woffData);
			Files.write(outputFile, ttfData, StandardOpenOption.CREATE);
		}
	}

	public static void main(String[] args) throws Exception {
		String pdfFile = "/Users/arun.sharma2/Downloads/04112016_cap_mp_03_2_col_r1_new.pdf";
		byte[] pdfContent = Files.readAllBytes(Paths.get(pdfFile));
		String destinationPath = "/Users/arun.sharma2/Downloads/04112016_cap_mp_03_2_col_r1_new.zip";
		(new EpaperOptimizerJpdf()).optimize(pdfContent, new File(destinationPath));
		System.out.println(MongoRestAPIService.addFile(Constant.EPAPER_FILE_COLLECTION, new File(destinationPath)));
		
	}

}

