package com.times.common.util;

/**
 * 
 */

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.MailSender;
import com.times.mailer.model.Subscription;


/**
 * This service is used to adhoc verify unsubscribed user and send activation mail.
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.BISubsMailerUtil"
 * @author Ranjeet.Jha
 * 
 */
public class BISubsMailerUtil {

	private final static String subject_BI = "Verify Your Business Insider Newsletter Subscription";
	public static final String templateURL = "http://www.businessinsider.in/newsletter_confirm_mail.cms";

	public static final String verificationLink = "http://www.businessinsider.in/newslettersubscription.cms?emailid=#emailid#&hashcode=#hashcode#&pwd=1&nlid=1007";
	
	public static final String FROM = "Business Insider <mailerservice@businessinsider.in>";
	public static final String host = "nmailer.indiatimes.com";	
	public static final String conentPath = "binsider@bounce.indiatimes.com";
	public static final String replyBackId = "Business Insider <mailerservice@businessinsider.in>";

	private final static Logger logger = Logger.getLogger(BISubsMailerUtil.class);
	public static void main(String[] args) throws FileNotFoundException {		
		updateRecord();
	}

	private static void updateRecord() {
		MailSender mailSender = null;

		String mailBody = MailerUtil.getDataRecursively(templateURL, 1);
		if (mailBody == null || "".equals(mailBody.trim())) {
			mailBody = MailerUtil.getDataRecursively(templateURL, 1);
		}
		mailSender = new MailSender(host,  FROM,  conentPath,  replyBackId);

		Mongo mongo = null;
		try {			

			int perPage = 50;
			int pageNo = 1;
			int offset = 0;
			BasicDBObject filter = new BasicDBObject();
			filter.put("ID", "1007");
			filter.append("V_NL", false);

			DBObject query = new BasicDBObject("$elemMatch", filter);	

			BasicDBObject finalQuery = new BasicDBObject();
			finalQuery.put("NL_MAP", query);


			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", "0");

			int loop = 100;
			int count = 1;
			for (int i =1; i <= loop; i++)  {

				List<DBObject> dbObjectList = MongoRestAPIService.getList("newslettersubscription", finalQuery , optionMap);		

				if (dbObjectList != null) {
					for (DBObject objDB : dbObjectList) {

						List<DBObject> nlListMap = (List<DBObject>) objDB.get(Subscription.NEWS_LETTER_MAP);
						if (nlListMap != null) {
							for(DBObject db : nlListMap){
								if (db.get("ID") != null && db.get("ID").toString().equalsIgnoreCase("1007") && db.get("V_NL") != null 
										&& db.get("V_NL").toString().equalsIgnoreCase("false") && objDB.get("E_ID") != null 
										) {							

									
									String emaild = (String) objDB.get("E_ID");
									String hashcode = (String) db.get("H_CODE");
									
									String contentBody = mailBody.replace("#verificationlink#", verificationLink.replace("#emailid#", emaild).replace("#hashcode#", hashcode));
									mailSender.sendMessage(emaild, subject_BI , contentBody);

									System.out.println(count++ + " , email sent to : " + emaild);
								}

							}
						}


					}				

				}
				offset = offset + perPage;
				optionMap.put("skip", String.valueOf(offset));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mongo != null) {
				mongo.close();
			}
		}
	}




}
