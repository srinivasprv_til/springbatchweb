package com.times.common.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriUtils;

import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;

/**
 * This class is used as external HTTP call 
 * 
 * @author ranjeet.jha
 *
 */
public class ExternalHttpUtil {

	private static final int httpConnectTimeout = 10000;
	private static final int httpReadTimeout = 30000;
	private static final int httpTimeoutForSolr = 7000;
	private static final Logger logger = LoggerFactory.getLogger(ExternalHttpUtil.class); 

	/**
	 * @param strURL
	 * @return
	 * @throws HttpException
	 * @throws IOException
	 */
	public static String getExtDataForSolar(String strURL) throws HttpException, IOException{
		/*HttpClient hClient = new HttpClient();
		HttpMethod gMethod = null;
		String extData = null; */ 
		String extData = null;
		HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
		RequestConfig config = RequestConfig.custom().setMaxRedirects(2)
				.setConnectTimeout(httpTimeoutForSolr).setSocketTimeout(httpTimeoutForSolr).build();

		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try{
			
			if(strURL.indexOf("?") != -1){
				
				strURL = strURL.substring(0,strURL.indexOf("?")) + UriUtils.encodeQuery(strURL.replace("/", " ").substring(strURL.indexOf("?")),  "UTF-8");
			}
		
			gMethod = new HttpGet(strURL);
			gMethod.setConfig(config);
			hResponse = hClient.execute(gMethod);
			int status = hResponse.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				 extData = new BasicResponseHandler().handleResponse(hResponse);
				return extData;
			}
		}catch(Exception e){
			new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);			
		}finally{
			/*if(gMethod != null){
				gMethod.releaseConnection();
				gMethod = null;
			}*/
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	}
	
	/**
	 * @param strURL
	 * @return
	 * @throws HttpException
	 * @throws IOException
	 */
	public static String getExtDataForSolr(String strURL) throws HttpException, IOException{
		
		String extData = null;
		HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
		RequestConfig config = RequestConfig.custom().setMaxRedirects(2)
				.setConnectTimeout(httpTimeoutForSolr).setSocketTimeout(httpTimeoutForSolr).build();

		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try{
			
			if(strURL.indexOf("?") != -1){
				strURL = strURL.substring(0,strURL.indexOf("?")) + UriUtils.encodeQuery(strURL.substring(strURL.indexOf("?")),  "UTF-8");
			}
			
			strURL = strURL.replace("&amp;", "%26amp%3b");
			gMethod = new HttpGet(strURL);
			gMethod.setConfig(config);
			hResponse = hClient.execute(gMethod);
			int status = hResponse.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				 extData = new BasicResponseHandler().handleResponse(hResponse);
				return extData;
			}
		}catch(Exception e){
			new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);			
		}finally{
			/*if(gMethod != null){
				gMethod.releaseConnection();
				gMethod = null;
			}*/
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	}
	
	
	/**
	 * @param strURL
	 * @return
	 * @throws HttpException
	 * @throws IOException
	 */
	public static String getExtData_PostCall(String strURL, JSONObject json) throws HttpException, IOException{
		String extData = null;
		HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
		RequestConfig config = RequestConfig.custom().setMaxRedirects(2)
				.setConnectTimeout(httpConnectTimeout).setSocketTimeout(httpReadTimeout).build();

		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try{
			
			if(strURL.indexOf("?") != -1){
				strURL = strURL.substring(0,strURL.indexOf("?")) + UriUtils.encodeQuery(strURL.replace("/", " ").substring(strURL.indexOf("?")),  "UTF-8");
			}
			
			gMethod = new HttpPost(strURL);
			gMethod.setConfig(config);
			HttpEntity e = new StringEntity(json.toString());
			((HttpPost)gMethod).setEntity(e);
			
			hResponse = hClient.execute(gMethod);
			int status = hResponse.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				 extData = new BasicResponseHandler().handleResponse(hResponse);
				return extData;
			}
		}catch(Exception e){
			new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);			
		}finally{
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	}
	
	
	public static String getExtData_PostCall(String strURL, List<NameValuePair> urlParameters) throws HttpException, IOException{

		String extData = null;
		HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
		RequestConfig config = RequestConfig.custom().setMaxRedirects(2)
				.setConnectTimeout(httpConnectTimeout).setSocketTimeout(httpReadTimeout).build();

		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try{
			
			if(strURL.indexOf("?") != -1){
				strURL = strURL.substring(0,strURL.indexOf("?")) + UriUtils.encodeQuery(strURL.replace("/", " ").substring(strURL.indexOf("?")),  "UTF-8");
			}
			
			gMethod = new HttpPost(strURL);
			gMethod.setConfig(config);
			((HttpPost)gMethod).setEntity(new UrlEncodedFormEntity(urlParameters));
			
			hResponse = hClient.execute(gMethod);
			int status = hResponse.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				 extData = new BasicResponseHandler().handleResponse(hResponse);
				 if (extData.equals("[0]")){
					 logger.error("0 returned while making MSID : "+urlParameters.toString()+"\n" );
					 
				 }
					 
				 
				return extData;
			}
			else{
				logger.error("Error while creating MSID for : "+urlParameters.toString()+"\n" );
			}
				
		}catch(Exception e){
			new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);			
		}finally{
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	
	}

	/**
	 * @param gMethod
	 * @return
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	/*private static String getString(HttpMethod gMethod) throws IOException, UnsupportedEncodingException {
		
		String extData;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();  
		byte[] byteArray = new byte[1024];  
		int count = 0;  
		while ((count = gMethod.getResponseBodyAsStream().read(byteArray, 0, byteArray.length)) > 0) {
			outputStream.write(byteArray, 0, count);
		}
		//System.out.println("contnet type : " + httpMethod.getParams().getContentCharset());
		extData = new String(outputStream.toByteArray(), "UTF-8");
		
		realeaseOutputStream(outputStream);
		return extData;
	}*/
	
	
	/**
	 * @param outputStream
	 */
	public static void realeaseOutputStream(ByteArrayOutputStream outputStream) {
		try {
			if (outputStream != null) {
				outputStream.close();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	public static String postRequestWithBody(String strURL, String jsonEntity) {
		HttpClient hClient = HttpClients.custom()
				.setRetryHandler(new DefaultHttpRequestRetryHandler(3, false))
				.build();
		RequestConfig config = RequestConfig.custom()
				.setConnectTimeout(httpConnectTimeout)
				.setSocketTimeout(httpConnectTimeout)
				.build();
		HttpRequestBase pMethod = null;
		HttpResponse httpResponse = null;
		try{
			pMethod = new HttpPost(strURL);
			pMethod.setConfig(config);
			StringEntity requestBody = new StringEntity(jsonEntity, ContentType.APPLICATION_JSON);
			((HttpPost)pMethod).setEntity(requestBody);
			httpResponse = hClient.execute(pMethod);
			int status = httpResponse.getStatusLine().getStatusCode();
			String extData = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8);
			if(status == HttpStatus.SC_OK){
				return extData;
			}else{
				CMSCallExceptions tmperr = new CMSCallExceptions(strURL+
						"-status-"+status+" - BLANK - ResponseBody"+extData,
						CMSExceptionConstants.CMS_IO_Exception);
				tmperr = null;
			}
		}catch(Exception e){
			logger.error(strURL + " - Exception Occured in get the external data", e);
			new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
		}finally{
			HttpClientUtils.closeQuietly(httpResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	}
}
