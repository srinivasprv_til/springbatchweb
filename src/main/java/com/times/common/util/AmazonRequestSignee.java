
package com.times.common.util;

/**
 * @author ajitkumar
 * This class will be used to generate the signature of amazon api
 */

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

import com.times.common.constant.AmazonRequestFields;

@Component
public class AmazonRequestSignee {

	private static final String UTF8_CHARSET = "UTF-8";
	private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
	private static final String REQUEST_URI = "/onca/xml";
	private static final String REQUEST_METHOD = "GET";

	private String endpoint = "webservices.amazon.in";
	private String awsAccessKeyId = "AKIAJX7PDW7DEP2LLJLA";
	private String awsSecretKey = "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";
	private String associateTag = "timofind-21";
	private String idType = "ASIN";
	private String operation = "ItemLookup";
	private String responseGroup = "Images,ItemAttributes,Offers";
	private String service = "AWSECommerceService";
	private String version = "2011-08-01";

	private SecretKeySpec secretKeySpec = null;
	private Mac mac = null;

	public AmazonRequestSignee() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		byte[] secretyKeyBytes = awsSecretKey.getBytes(UTF8_CHARSET);
		secretKeySpec =new SecretKeySpec(secretyKeyBytes, HMAC_SHA256_ALGORITHM);
		mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(secretKeySpec);
	}


	public String sign(Map<String, String> params) {
		params.put(AmazonRequestFields.AWS_ACCESS_KEYID, "AKIAJX7PDW7DEP2LLJLA");
		params.put(AmazonRequestFields.ASSOCIATE_TAG, "timofind-21");
		params.put(AmazonRequestFields.SERVICE, "AWSECommerceService");
		params.put(AmazonRequestFields.TIMESTAMP, timestamp());

		SortedMap<String, String> sortedParamMap =
				new TreeMap<String, String>(params);
		String canonicalQS = canonicalize(sortedParamMap);
		String toSign =
				REQUEST_METHOD + "\n"
						+ endpoint + "\n"
						+ REQUEST_URI + "\n"
						+ canonicalQS;

		String hmac = hmac(toSign);
		String sig = percentEncodeRfc3986(hmac);
		String url = "http://" + endpoint + REQUEST_URI + "?" +
				canonicalQS + "&Signature=" + sig;

		return url;
	}

	private String hmac(String stringToSign) {
		String signature = null;
		byte[] data;
		byte[] rawHmac;
		try {
			data = stringToSign.getBytes(UTF8_CHARSET);
			rawHmac = mac.doFinal(data);
			Base64 encoder = new Base64();
			signature = new String(encoder.encode(rawHmac));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(UTF8_CHARSET + " is unsupported!", e);
		}
		return signature;
	}

	private String timestamp() {
		String timestamp = null;
		Calendar cal = Calendar.getInstance();
		DateFormat dfm = new SimpleDateFormat(AmazonRequestFields.REQUEST_DATE_FORMAT);
		dfm.setTimeZone(TimeZone.getTimeZone("GMT"));
		timestamp = dfm.format(cal.getTime());
		return timestamp;
	}

	private String canonicalize(SortedMap<String, String> sortedParamMap)
	{
		if (sortedParamMap.isEmpty()) {
			return "";
		}

		StringBuffer buffer = new StringBuffer();
		Iterator<Map.Entry<String, String>> iter =
				sortedParamMap.entrySet().iterator();

		while (iter.hasNext()) {
			Map.Entry<String, String> kvpair = iter.next();
			buffer.append(percentEncodeRfc3986(kvpair.getKey()));
			buffer.append("=");
			buffer.append(percentEncodeRfc3986(kvpair.getValue()));
			if (iter.hasNext()) {
				buffer.append("&");
			}
		}
		String canonical = buffer.toString();
		return canonical;
	}

	private String percentEncodeRfc3986(String s) {
		String out;
		try {
			out = URLEncoder.encode(s, UTF8_CHARSET)
					.replace("+", "%20")
					.replace("*", "%2A")
					.replace("%7E", "~");
		} catch (UnsupportedEncodingException e) {
			out = s;
		}
		return out;
	}



	public static void main(String args[]) throws InvalidKeyException, NoSuchAlgorithmException, IOException{
		Map<String, String> params = new HashMap<String, String>();

		params.put("Service", "AWSECommerceService");
		params.put("Operation", "ItemSearch");
		params.put("AWSAccessKeyId", "AKIAJX7PDW7DEP2LLJLA");
		params.put("AssociateTag", "timofind-21");
		params.put("SearchIndex", "Electronics");
		params.put("ResponseGroup", "Large");
		params.put("BrowseNode", "1805560031");
		params.put("MinimumPrice", "100000");
		params.put("MaximumPrice", "120000");

		AmazonRequestSignee a = new AmazonRequestSignee();
		System.out.println(a.sign(params));



	}
}
