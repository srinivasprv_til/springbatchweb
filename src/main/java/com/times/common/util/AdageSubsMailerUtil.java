package com.times.common.util;

/**
 * 
 */

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.MailSender;
import com.times.mailer.dao.MailerDao;
import com.times.mailer.dao.MailerDaoImpl;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetterDailyTracker;
import com.times.mailer.model.Subscription;


/**
 * This service is used to adhoc verify unsubscribed user and send activation mail.
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.AdageSubsMailerUtil"
 * @author Rajeev Khatri
 * 
 */
public class AdageSubsMailerUtil {

	private final static String subject_adage = "Verify Your Adage Newsletter Subscription";
	public static final String templateURL = "http://www.adageindia.in/newsletter_confirm_mail.cms";
	

	public static final String verificationLink =              "http://www.adageindia.in/newsletternew.cms?type=confirmation&utm_source=Verification_email&utm_medium=Email&utm_campaign=Email&emailid=#emailid#&hashcode=#hashcode#&pwd=1&nlid=1017&host=283";
	//public static final String verificationLink =   "http://toidev.indiatimes.com:8388/newsletternew.cms?type=confirmation&utm_source=Verification_email&utm_medium=Email&utm_campaign=Email&emailid=#emailid#&hashcode=#hashcode#&pwd=1&nlid=1017&host=283";
	public static final String FROM = "Advertising Age India <mailerservice@adageindia.in>";
	public static final String host = "nmailer.indiatimes.com";	
	public static final String conentPath = "timesofindia@bounce.indiatimes.com";
	public static final String replyBackId = "Advertising Age India <mailerservice@adageindia.in>";

	private final static Logger logger = Logger.getLogger(AdageSubsMailerUtil.class);
	public static void main(String[] args) throws FileNotFoundException {
		
		String skey1 = "10171" + Constant.KEY_BUILDER_SEPERATOR + 1 + Constant.KEY_BUILDER_SEPERATOR + 1 ;
		String uniqueCode = addNewsLetterDailyTrackerIfNotfound("10171", 1, 1, skey1);
		fetchRecord(uniqueCode);
	}
	
	private static String addNewsLetterDailyTrackerIfNotfound(String newsid, int iTime, int itype, String skey1) {
		String uniqueCode= null;
		MailerDao dao = new MailerDaoImpl();
		String yyymmdd = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		DBObject nlTracker = dao.getNewsLetterDailyTracker(newsid, yyymmdd, String.valueOf(iTime), String.valueOf(itype));
		if (nlTracker == null) {
			uniqueCode = getUniqueCodeByDate(skey1);
			NewsLetterDailyTracker nlDailyTracker = new NewsLetterDailyTracker(new Date(), Integer.parseInt(newsid), uniqueCode, iTime, itype, yyymmdd);
			dao.addNewsLetterDailyTracker(nlDailyTracker); 
			
		} else {
			uniqueCode = String.valueOf(nlTracker.get(NewsLetterDailyTracker.NEWS_LETTER_CODE));
		}
	
		
		return uniqueCode;
	}
	/**
	 * This method is used to get the StringCode.
	 * @param str
	 * @return
	 */
	private static String getUniqueCodeByDate(String key) {
		String date = MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD);
		String code = MailerUtil.getUniqueCodeByDate(date + key);
		return code;	
	}

	private static void fetchRecord(String uniqueCode) {
		MailSender mailSender = null;

		String mailBody = MailerUtil.getDataRecursively(templateURL, 1);
		if (mailBody == null || "".equals(mailBody.trim())) {
			mailBody = MailerUtil.getDataRecursively(templateURL, 1);
		}
		mailSender = new MailSender(host,  FROM,  conentPath,  replyBackId);

		Mongo mongo = null;
		try {			

			int perPage = 50;
			int pageNo = 1;
			int offset = 0;
			BasicDBObject filter = new BasicDBObject();
			filter.put("ID", "1017");
			filter.append("V_NL", false);

			DBObject query = new BasicDBObject("$elemMatch", filter);	

			BasicDBObject finalQuery = new BasicDBObject();
			finalQuery.put("NL_MAP", query);


			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", "0");

			int loop = 100;
			int count = 1;
			for (int i =1; i <= loop; i++)  {

				List<DBObject> dbObjectList = MongoRestAPIService.getList("newslettersubscription", finalQuery , optionMap);		

				if (dbObjectList != null) {
					for (DBObject objDB : dbObjectList) {

						List<DBObject> nlListMap = (List<DBObject>) objDB.get(Subscription.NEWS_LETTER_MAP);
						if (nlListMap != null) {
							for(DBObject db : nlListMap){
								if (db.get("ID") != null && db.get("ID").toString().equalsIgnoreCase("1017") && db.get("V_NL") != null 
										&& db.get("V_NL").toString().equalsIgnoreCase("false") && objDB.get("E_ID") != null 
										) {							

									
									String emaild = (String) objDB.get("E_ID");
									String hashcode = (String) db.get("H_CODE");
									
									String contentBody = mailBody.replace("#verificationlink#", verificationLink.replace("#emailid#", emaild).replace("#hashcode#", hashcode)).replaceAll("##code##", uniqueCode);
									mailSender.sendMessage(emaild, subject_adage , contentBody);

									System.out.println(count++ + " , email sent to : " + emaild);
								}

							}
						}


					}				

				}
				offset = offset + perPage;
				optionMap.put("skip", String.valueOf(offset));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mongo != null) {
				mongo.close();
			}
		}
	}




}
