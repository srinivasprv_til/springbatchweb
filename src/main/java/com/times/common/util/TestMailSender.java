
package com.times.common.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.times.common.mail.EmailVO;
import com.times.common.mail.MailSender;

/**
 * This class is used to test mail for the fluid template as per request by Neeraj for testing purpose.
 * nohup mvn exec:java -Dexec.mainClass="com.times.common.util.TestMailSender" -Dexec.args="5"
 * @author Ranjeet.Jha
 *
 */
public class TestMailSender {

	private static String[] TEMPLATE_ARRAYS = {
		//"http://www2.timesofindia.indiatimes.com/newsletternew.cms",
		"http://jcmsdev.indiatimes.com/newsletternew.cms",
		"http://jcmsdev.indiatimes.com/digestnewsletter.cms",
		"http://toidev.indiatimes.com/mvrnewsletter.cms",
		"http://jcmsdev.indiatimes.com/lsnewsletter.cms",
		"http://toidev.indiatimes.com/toitechnewsletter_preview.cms?daily=1&browseview=1", 
		"http://toidev.indiatimes.com/newsletternew.cms",
		"http://toidev.indiatimes.com/entnewsletterhead.cms",
		"http://toidev.indiatimes.com:8365/newsletter_delhi.cms",
		"http://www.businessinsider.in/newsletter_main.cms",
		"http://gocricket.com/newsletter/34732194.cms",
		"http://gocricket.com/newsletter/34732235.cms"
		};
	
	//private static String[] EMAILS_IDS = {"mastermind699@gmail.com", "manika487@yahoo.in", "eris222@rediffmail.com", "monijalan@gmail.com", "deben2008@gmail.com", "Amitm1979@gmail.com", "Amit.monga@indiatimes.co.in", "waliaspuneet@gmail.com", "rajesh.kumar@timesinternet.in", "rajeshworldmail@gmail.com"}; 
	
	//private static String[] EMAILS_IDS = {"eshaan.gupta@timesinternet.in", "chaman.kalra@timesinternet.in", "hem.sharma@timesinternet.in", "egupta171@gmail.com", "luckykalra@gmail.com", "kumar.shekhawat@gmail.com", "hemk78@gmail.com", "hem.sharma@timesinternet.in"}; 
	
	//private static String[] EMAILS_IDS = {"rajeev.khatri@timesinternet.in", "ritesh.prakash@timesinternet.in", "riteshprakash@gmail.com", "puneet.gupt@timesinternet.in", "guptpuneet@gmail.com", "mathew.george@timesinternet.in", "matgeo.2008.til@gmail.com", "amit.monga@timesinternet.in", "amitm1979@gmail.com", "travelnewsletter@hotmail.com", "mddy2013@rediffmail.com", "christ131@rediffmail.com"};
	private static String[] EMAILS_IDS = {"rajeev.khatri@timesinternet.in","Rajeshworldmail@gmail.com","Rajesh.kumar@timesinternet.in", "Dominic.ferrao@timesinternet.in", "Mahima.jalan@timesinternet.in","Puneet.walia@timesinternet.in"};
	
	private static String[] nlidArrays = {"1001", "1002", "1003", "1004", "1005", "1001"};
	private static String[] subjectArray = {"Top Headlines", "Daily Digest Newsletter", "Movie Review Newsletter",  "Life and Style Newsletter", "Tech Newsletter", "Top Headlines", "Entertainment Newsletter", "Travel tips for you next trip to Delhi", "Business Insider India Newsletter", "National Newsletter", "Global Newsletter"};
	private static List<String> CONTENT_LIST = new ArrayList<String>(10);
	private static String subject = "Times Of India Newsletter";
	
	
	// for testing only uncomment
	/*public static final String FROM = "TOI Online Editor <toi.onlineeditor@indiatimes.com>";*/
	//public static final String host = "10.157.211.113";
	
	public static final String FROM = "The Times Of India <mailerservice@timesofindia.com>";
	public static final String replyBackId = "The Times Of India <mailerservice@timesofindia.com>";
	
	public static final String host = "nmailer.indiatimes.com";
	public static final String conentPath = "timesofindia@bounce.indiatimes.com";
	
	/*public static final String replyBackId = "HappyTrips <mailerservice@happytrips.com>";
	public static final String FROM = "HappyTrips <mailerservice@happytrips.com>";*/
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MailSender mailSender = null;
		try {
			int index = 2; 
			//initMailContnet();
			if (args == null || args.length == 0) {
				initMailContnet(index);
			} else {
				initMailContnet(Integer.parseInt(args[0]));
			}
			mailSender = new MailSender(host,  FROM,  conentPath,  replyBackId);
			
			if (args == null || args.length == 0) {
				int k = 0;
				for (String content : CONTENT_LIST) {
					for (String emailId : EMAILS_IDS) {
						if (index != 0) {
							mailSender.sendMessage(emailId, subjectArray[index], content);
						} else {
							mailSender.sendMessage(emailId, subjectArray[k], content);
						}
						System.out.println(emailId + " sent " + new Date());
					}
					k++;
				}
				
			} else if (args != null && args.length == 1) {
				String arg = args[0];
				int i = Integer.parseInt(arg);
				String content = CONTENT_LIST.get(0);
				for (String emailId : EMAILS_IDS) {
					mailSender.sendMessage(emailId, subjectArray[i], content);
					System.out.println(emailId + " sent " + new Date());
				}
			} else if (args != null && args.length == 2) {
				String arg = args[0];
				int i = Integer.parseInt(arg);
				String emailid = args[1];
				String[] emailIdArray = emailid.split(",");
				for (int j = 0; j < emailIdArray.length; j++) {
					String content = CONTENT_LIST.get(i);
					mailSender.sendMessage(emailIdArray[j], subjectArray[j], content);
					System.out.println(emailIdArray[j] + " sent " + new Date());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	private static void initMailContnet() {
		int i = 0;
		for (String url : TEMPLATE_ARRAYS) {
			System.out.println("Template URL : " + url);
			pushContnet(url);
		}
	}
	
	private static void initMailContnet(int index) {
		int i = 0;
			System.out.println("Template URL : " + TEMPLATE_ARRAYS[index]);
			if (index == 6) {
				entertainmentContnet(TEMPLATE_ARRAYS[index]);
			} else {
				pushContnet(TEMPLATE_ARRAYS[index]);
			}
	}

	private static void pushContnet(String url4Html) {
		//String content = MailerUtil.getData(url4Html);
		String content = MailerUtil.getDataRecursively(url4Html, 1);
		if (StringUtils.hasText(content)) {
			CONTENT_LIST.add(content);
		}
		/*if (StringUtils.hasText(content)) {
			CONTENT_LIST.add(content);
		} else {
			pushContnet(url4Html);
		}*/
	}
	
	
	
	private static void entertainmentContnet(String url4Html) {
		String sNav = "";
		String newsid = "1031";
		String secName = "Hindi|English|Tamil|Telgu|Malayalam|Kannada|Bengali|Punjabi|Marathi|Bhojpuri|Gujarati";
		String urlNewsletterWidget = "http://toidev.indiatimes.com/entnewsletterbody/#msid.cms";
		url4Html = url4Html  +  "/?nlformat=html";
		String sBody = MailerUtil.getDataRecursively(url4Html, 1);

		if (sBody != null) {			
			sBody = com.times.common.util.StringUtils.replaceSpecialChar(sBody);

		}
		boolean blnShowNav = false;

		if (sBody != null) {
			boolean isContentFounD = false;
			String ids = "27135527,27135489,27135454,27135440,27135428,27135414,27135398,27135289,27134572,27134497,29982848";
			if (ids != null && !"null".equals(ids)) {

				String[] idArray = ids.split("[,]");
				String[] sNavArray = secName.split("[|]");

				if (sNavArray.length == idArray.length) {
					blnShowNav = true;
				}

				if (idArray != null) {
					int index = 0;
					for (String id : idArray) {
						String sURL = urlNewsletterWidget.replaceAll("#msid", id);
						sURL = sURL + "?nlformat=html";
						String sWidgetData = MailerUtil.getDataRecursively(sURL, 1);
						if (StringUtils.hasText(sWidgetData)) {
							sWidgetData = com.times.common.util.StringUtils.replaceSpecialChar(sWidgetData);

						}

						sBody = sBody.replace("##pattern-newsletter##", sWidgetData +"##pattern-newsletter##");
						isContentFounD = true;

						index++;
					}
					sNav = getTableWidget(sNavArray);

				}

				if (blnShowNav) {
					sBody = sBody.replace("##patten-navigation##", sNav);
				}

				sBody = sBody.replace("##pattern-newsletter##", " ");
				CONTENT_LIST.add(sBody);
			}
		}

	}
		
		
	private static String getTableWidget(String[] sNavArray) {
		StringBuilder sb = new StringBuilder();
		int noOfCol = 4; 
		int iCount = 0;
		Map<String, String> styles = new HashMap<String, String>(){{

			this.put("00", "background-color:#4f4f4f;width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191;border-left:1px solid #000000;");
			this.put("01", "width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191");
			this.put("02", "background-color:#4f4f4f;width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191");
			this.put("03", "width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191;border-right:none;");

			this.put("10", "width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191;border-left:1px solid #000000;");
			this.put("11", "background-color:#4f4f4f;width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191");
			this.put("12", "width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191");
			this.put("13", "background-color:#4f4f4f;width:142px;padding-top:4px;height:20px;text-align: center;float:left;color:#0050a2;font-size:14px;font-family:Georgia;border:1px solid #919191;border-right:none;");


		}};
		//noOfCol = StringUtils.getNoOfColInRow(sNavArray);
		int totalRow =(int) Math.ceil((double)sNavArray.length/(double)noOfCol);
		sb.append("<table cellpadding='0' cellspacing='0' style='float: left;list-style-type: none;margin: 0;padding: 0;swidth:479px;background-color:#353535;color:#0050a2;'>");
		int colSpan = (totalRow*noOfCol - sNavArray.length) ;
		for(int row=0 ; row<totalRow; ++row){
			sb.append("<tr>");
			int rowOddity = row % 2;
			for(int col = 0; col < noOfCol && iCount < sNavArray.length  ; col++){
				boolean colSpanApplicable = false;
				if(totalRow -1 == row && iCount ==  sNavArray.length - 1){
					colSpanApplicable = true;
				}

				sb.append("<td ").append(" style='"+styles.get(rowOddity+""+col)+"' >");
				if(colSpanApplicable){
					sb.append("<a style='color:#ffffff' href='#").append(sNavArray[iCount]).append("'>").append(sNavArray[iCount]).append("</a>");
					sb.append("</td>");
					sb.append("<td ").append(" colspan=").append("'"+colSpan+"'>");
					sb.append("</td>");
					iCount++;
					continue;
				}
				//sb.append("<a style='color:#ffffff' href='").append(InMemoryData.SECTION_MAP.get(idArray[iCount])).append("' />").append(sNavArray[iCount]).append("</a>");
				sb.append("<a style='color:#ffffff' href='#").append(sNavArray[iCount]).append("'>").append(sNavArray[iCount]).append("</a>");
				sb.append("</td>");
				++iCount;
			}
			sb.append("</tr>");
		}
		sb.append("</table>");
		//log.debug(sb.toString());
		return sb.toString();
	}	
		
	private static List<EmailVO> populateEMailVO() {
		List<EmailVO> emailList = new ArrayList<EmailVO>();
		if (EMAILS_IDS != null) {
			for (String email : EMAILS_IDS) {
				
				EmailVO emailVO = new EmailVO();
				emailVO.setRecipients(email);
				emailVO.setSender(FROM);
				emailVO.setBody(email);
				emailVO.setContentType("text/html");
				
				emailList.add(emailVO);
			}
		}
		
		return emailList;
	}
}
