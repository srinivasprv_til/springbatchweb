package com.times.common.util;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class is used for constant for this application.
 * 
 * @author Ranjeet.Jha
 *
 */
public interface Constant {

	public final static String KEY_BUILDER_SEPERATOR = "-";
	public static final String NL_FORMAT_TEXT = "text";
	public static final String NL_FORMAT_HTML = "html";
	public static final String NL_FORMAT_PARAM = "nlformat";
	public static final String SENDER = "sender";
	
	//parameter for Job Launch.
	public static final String JOB_PARAM_NEWSLETTER_ID = "nlid";
	public static final String JOB_PARAM__NL_DATETIME = "nl.batch.job.date";
	public static final String JOB_PARAM_NL_DAILY_WEEKLY_KEY = "nl.dailyOrWeekly";
	public static final long JOB_PARAM_NL_DAILY = 1; //daily
	public static final long JOB_PARAM_NL_WEEKLY = 2; //weekly
	
	public static final String JOB_PARAM_NL_MORNING_EVE_KEY = "nl.morningOrEve";
	public static final long JOB_PARAM_NL_MORNING = 1; //morning
	public static final long JOB_PARAM_NL_EVENING = 2; //evening
	public static final String JOB_PARAM_NL_NL_ID_PAGE_NO = "nlid.pageNo";
	public static final String JOB_PARAM_NL_RESTART_KEY = "restart";
	public static final String JOB_PARAM_NL_RESTART_VALUE = "nl.restart";
	
	public static final String DATE_FORMAT_YYY_MM_DD_HH_MM_SSS = "yyyy-MM-dd'T'HH:mm:sss"; 
	
	//public static final String SOLR_QUERY_TRAVELO = "http://192.168.27.64:9080/TOISolrWebProject/travel/select?q=*:*&fq=contenttypeid:2&fq=mData_TravelLocationTag:%28%22DELHI%22%20OR%20%22MUMBAI%22%29&fq=mData_TravelActivityTag:%28%22THINGS%20TO%20DO%22%20OR%20%22SIGHTS%22%29&sort=effectivedate%20desc&fq=effectivedate:[NOW-10DAYS%20TO%20NOW]&start=0&rows=10&fl=msid&wt=xslt&tr=custom.xsl";
	//public static final String SOLR_QUERY_TRAVELO = "http://192.168.27.64:9080/TOISolrWebProject/travel/select?q=*:*&fq=contenttypeid:2&fq=mData_TravelLocationTag:(#LOCATIONS#)&fq=mData_TravelActivityTag:(#ACTIVITIES#)&sort=effectivedate desc&fq=effectivedate:[NOW-10DAYS TO NOW]&start=0&rows=10&fl=msid&wt=xslt&tr=custom.xsl";
	public static final String SOLR_QUERY_TRAVELO="http://192.168.27.64:9080/TOISolrWebProject/travel/select?q=#query#&wt=xslt&tr=custom.xsl&defType=dismax&qf=title+text+synpsis&q.op=OR";
	public static final String SSO_URL_4_PREFERENCES = "http://userreco.indiatimes.com/userreco/travel/fetchEmailIdsData?secret=dqpzqqgeq8tvotmhcauodhzez&emailIds=#emailids#"; // emailId must be comma seperated
	
	// travel alert key/value
	public static final String KEY_LOCATION_LIST = "localtionList";
	public static final String KEY_ACTIVITY_LIST = "activityList";
	public static final String KEY_WISHLIST_LIST = "wishList";
	
	public static final String FILE_UPLOAD_JOB_4_TOI_LIST_PAGE = "toiListPage";
	public static final String JOB_PARAM_JOBTYPE_ID = "fileUploadType";
	
	
	public static final String MSID = "MSID";
	public static final String EMAIL_ID = "E_ID";
	public static final String NLID_MSID_COLLECTION = "NL_MS_ID";
	public static final String NLID = "NLID";
	
	
	public static final String TRAVEL_ALERT_NLID = "1032";
	public static final String[] listTypes = {"alertList"};
	
	public static final String VERIFIED_PREFERENCE = "V_PF";
	public static final String PREFERENCE = "PREF";
	
	
	public static final String MAIN_TEMPLATE_MAIN_URL = "http://timesofindia.indiatimes.com/digestnewsletter.cms";
	public static final String MAIN_TEMPLATE_SECTION_URL = "http://timesofindia.indiatimes.com/digestseweekly.cms?msid=#msid&day=1";
	
	// workTyp`e for listLevelFileUpload, breakingNews, Scorecard, polling, liveblog
	public static final int WORK_TYPE_TOI_LIST_PAGE = 1;
	public static final int WORK_TYPE_BREAKING_NEWS = 2;
	public static final int WORK_TYPE_SCORE_CARD = 5; // 5 for fixed set of url and 6 with #matchid
	public static final int WORK_TYPE_SCORE_CARD_VAR = 6;
	public static final int WORK_TYPE_LIVE_BLOG = 4;
	public static final int WORK_TYPE_POLL = 7;
	public static final int WORK_TYPE_CVOTER_JSON = 8;
	public static final int WORK_TYPE_CVOTER_XML = 9;
	public static final int WORK_TYPE_PAGE_CONTENT_AMAZON = 11;
	public static final int WORK_TYPE_BREAKING_NEWS_AMAZON = 12;
	public static final int WORK_TYPE_LANG_PAGE_CONTENT_AMAZON = 13;
	public static final int WORK_TYPE_LANG_SAMYAM_PAGE_CONTENT_AMAZON = 16;
	
	public static final int CONTENT_RULES_AMAZON_PRODUCT_ID_META_TYPE = 313;
	
	public static final int[] LIVE_BLOG_ENABLED_HOST_IDS = {262,53,83,153,155,245,252,256,270,261,278,25,285,286,287,268,284,263,282,266,267,271,281,265,307,257,247,264,275,276}; //53,83,153,155,245,252,256,270
	
	// LIVE BLOG HOST ID Enable.
	public static final int HOSTID_TOI = 83;
	public static final int HOSTID_ET = 153;
	public static final int HOSTID_NBT = 53;
	public static final int HOSTID_MT = 155;
	public static final int HOSTID_VK = 245;
	public static final int HOSTID_EISAMAY = 252;
	public static final int HOSTID_GIZMODO = 256;
	public static final int HOSTID_NGS = 270;
	public static final int HOSTID_CRICKET = 262;
	public static final int HOSTID_BI = 261;
	
	public static final String CVOTER_STATIC_FILE_PATH = "/opt/svn/script/";
	//public static final String CVOTER_STATIC_FILE_PATH = "C:/Users/manish.chandra/Desktop/";
	public static String[] CVOTER_STATIC_FILE_NAME = {"C-voter-data-recon.xslx"};

	//Feeds HostID
	public static final String WORK_TYPE_FEEDS = "workType";
	public static final int WORK_TYPE_FEEDS_VALUE = 7;
	public static final String FEEDS_HOST_ID = "hostId";
	public static final String FEEDS_PER_PAGE = "perPage";
	public static final String FEEDS_PAGE_NO = "pageNo";
	
	ArrayList<String> NLID_WITH_SUBJECT_FROM_TEMPLATES = new ArrayList(Arrays.asList("1025","1020","1021","1022","1018", "1007", "1017","1028"));
	ArrayList<String> NLID_WITH_SENDER_FROM_DB = new ArrayList(Arrays.asList("1003","1007","1025","1020","1021","1022","1018","1028"));
	ArrayList<String> INDIVISUAL_NEWSLETTER_VERIFICATION = new ArrayList(Arrays.asList("1025","1007","1017","1010","1011","1020","1021","1022","1028"));
	ArrayList<String> NEWSLETTER_WITH_SORT_FETCH = new ArrayList(Arrays.asList("1007","1017","1010","1011","1020","1021","1022","1028"));
	ArrayList<String> NLID_WITHOUT_SCHDULE_TIME = new ArrayList(Arrays.asList("1016","1019","1028"));
	ArrayList<String> NLID_WITHOUT_WEEKDAY_CONSIDERATION = new ArrayList(Arrays.asList("1003","1005","1028"));
	
	// for epaper
	public static final String EPAPER_COLLECTION = "epaper_master";
	public static final String EPAPER_FILE_COLLECTION = "epaperfile";
	public static final String FEEDMAILER_COLLECTION = "nprss_mail";
	public static final String EPAPER_RAW_DATA_COLLECTION = "epaper_raw_data";
	public static final String EPAPER_PROCESSED_DATA_COLLECTION = "epaper_processed_data";
	public static final String DEFAULT_DIRECTORY_KEY = "DEF";
	public static final String CURRENT_DATE_KEY = "C_D";
	public static final String PREVIOUS_DATE_KEY = "P_D";
	
	public static final String EPAPER_PROCESSED_STATUS_LOG_COLLECTION = "EPAPER_PROCESS_LOG";
	
	public static final String RAW_STATUS = "RAW";
	
	public static final String PUBLISHER = "PUB";
	public static final String EDITION = "EDITN";
	public static final String LOCATION = "LOC";
	public static final String UNIQUE_KEY = "U_KEY";
	public static final String STATUS= "STAT";
	public static final String CREATED_AT= "C_AT";
	public static final String UPDATED_AT= "U_AT";
	public static final String EPAPER_FTP_LOCATION = "PATH";
	
	public static final String PRODUCT_DETAIL_COLLECTION = "gadget_product_detail";
	public static final String MOVIE_RATING_COLLECTION = "movieratinglog";
	
	public static final String PRODUCT_DETAIL_REVIEW_ID = "reviewid";
	public static final String PRODUCT_DETAIL_MSID = "msid";
	public static final String PRODUCT_DETAIL_FULL_MSID = PRODUCT_DETAIL_REVIEW_ID +"."+PRODUCT_DETAIL_MSID;
	public static final String PRODUCT_DETAIL_PARENT_ID = "parentid";
	public static final String PRODUCT_DETAIL_FULL_PARENT_ID = PRODUCT_DETAIL_REVIEW_ID +"."+PRODUCT_DETAIL_PARENT_ID;
	
	public final String SERVEY_DATA_COLLECTION = "serveyDataDetails";
	public final String SERVEY_DATA_ZIP_COLLECTION = "serveyDataZipDetails";
	
	String mailerServiceUrl = "http://cmsgenericmailer.indiatimes.com/MailServiceWeb/messages/sendMail";
	String uploadMailerServiceUrl = "http://cmsgenericmailer.indiatimes.com/MailServiceWeb/messages/uploadMessage";
}

