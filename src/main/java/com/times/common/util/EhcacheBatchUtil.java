package com.times.common.util;

import java.util.StringTokenizer;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EhcacheBatchUtil {
	private static final Logger log = LoggerFactory.getLogger(EhcacheBatchUtil.class);
	public static boolean toLowerSeo = false;
    public static String SeoCharacter = "-";
	public static String getSimpleString(String msName) {
        String sHTML = "";
        if (toLowerSeo) {
            sHTML = getSimpleString(msName, true);
        } else {
            sHTML = getSimpleString(msName, false);
        }
        return sHTML;
    }
	public static String getSimpleString(String msName, boolean lower) {
		int i;
		String sHTML = "";
		try {
			msName = msName.trim();
			sHTML = stripHTMLTags(msName).replaceAll(" ", SeoCharacter);
			sHTML = sHTML.replaceAll("\\.", SeoCharacter);
			if (sHTML.length() <= 0) {
				return sHTML;
				// 45=-, 48-57= 0-9, 65-90 =A-Z, 95=_, 97-122 = a-z
			}

			for (i = 0; i < sHTML.length(); i++) {
				if ((sHTML.charAt(i) != 45) && (sHTML.charAt(i) != 47) && (sHTML.charAt(i) != 95)
						&& ((sHTML.charAt(i) < 48) || (sHTML.charAt(i) > 57))
						&& ((sHTML.charAt(i) < 97) || (sHTML.charAt(i) > 122))
						&& ((sHTML.charAt(i) < 65) || (sHTML.charAt(i) > 90))) {
					sHTML = sHTML.replace(sHTML.charAt(i), '~');
				}
			}
			sHTML = sHTML.replaceAll("~", "");
		} catch (Exception e) {
			log.error("Error Occured in getSimpleString for String : " + msName, e);
		}
		sHTML = sHTML.replaceAll("([-]+)", "-");
		if (lower) {
			sHTML = sHTML.toLowerCase();
		}
		try {
			StringTokenizer strToken = new StringTokenizer(sHTML, "/");
			String strPrev = "";
			String stripedSHTML = "";
			while (strToken.hasMoreTokens()) {
				String temp = strToken.nextToken();
				if (!strPrev.equalsIgnoreCase(temp)) {
					stripedSHTML = stripedSHTML + "/" + temp;
					strPrev = temp;
				}
			}
			sHTML = stripedSHTML.length() > 1 ? stripedSHTML.substring(1) : sHTML;
			strToken = null;
			strPrev = null;
			stripedSHTML = null;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return sHTML;
	}
	
    public static String stripHTMLTags(String msName) {
        int i = 0, j, k, iCtr;
        String sTemp, sTempBuff;
        sTempBuff = msName;
        i = sTempBuff.indexOf("<");
        j = sTempBuff.indexOf(">");
        k = sTempBuff.length();
        iCtr = 0;
        try {
            while ((i > -1) && (iCtr < k) && i < j) {
                sTemp = sTempBuff.substring(i, j + 1);
                sTempBuff = sTempBuff.substring(j + 1, sTempBuff.length());
                if ((!sTemp.equals("<BR>")) && !(sTemp.equals("<br>"))) {
                    msName = msName.replace(sTemp, "");
                }
                i = sTempBuff.indexOf("<");
                j = sTempBuff.indexOf(">");
                iCtr++;
            }
            return msName;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return StringEscapeUtils.escapeHtml4(msName);
        }
    }
}
