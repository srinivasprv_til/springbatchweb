package com.times.common.util;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.MailSender;
import com.times.common.wrapper.NewsletterDBObjectWrapper;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.NewsLetter;
import com.times.mailer.model.Subscription;

/**
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.HTManualVerificationMailProcessor"
 * @author Rajeev.Khatri
 *
 */
public class HTManualVerificationMailProcessor {

	//private MailSender mailSender = null;
	
	public HTManualVerificationMailProcessor() {
		//mailSender = new MailSender(TestMailSender.host,  TestMailSender.FROM,  TestMailSender.conentPath,  TestMailSender.replyBackId);
	}
	
	
	
	
	private static final String MAIL_BODY_HT_4_VERIFICTION = "<font size='3' color='black' face='Calibri'>Dear Reader, <br /><br />" +
			"You recently subscribed to HappyTrips' Daily Newsletter. However we are unable to start sending newsletters to your inbox since your subscription request is still not verified."
			+ "<br /><br /> To verify, click on the link below. "
			+ "<br /> <a href=\"<#verificationLink#>\" style=\"color:#ED5001\" ><b>Click here to activate your subscription</b></a>"
		+ "<br /><br />Thanks, <br />"
		+ "Team HappyTrips</font>";
	
	
	
	private final static String subject_HT = "Reminder to confirm newsletter subscription";
	
	public static final String FROM = "HappyTrips <mailerservice@happytrips.com>";
	public static final String host = "nmailer.indiatimes.com";
	// for testing only uncomment
	/*public static final String FROM = "HappyTrips <mailerservice@happytrips.com>";
	public static final String host = "10.157.211.113";*/
	public static final String conentPath = "timesofindia@bounce.indiatimes.com";
	public static final String replyBackId = "HappyTrips <mailerservice@happytrips.com>";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FileInputStream fis;
		String emaild = null;
		String contentBody = null;
		MailSender mailSender = null;
		try {
			Mongo mongo = new Mongo("192.168.33.157", 27017);
			//Mongo mongo = new Mongo("10.157.211.92", 27017);;
			DB db = mongo.getDB("jcmsDB");
			DBCollection collection = db.getCollection("newslettersubscriptiontravel");
			long unverifiedSubs = getUnverifiedSubscriberCount(collection, "1031");


			mailSender = new MailSender(host,  FROM, conentPath,  replyBackId);
			Map<String, String> uSubs = getUnverifiedSubscriber(unverifiedSubs,"1031");;
			int i = 1;
			for (String emailId : uSubs.keySet()) {

				contentBody = MAIL_BODY_HT_4_VERIFICTION;
				contentBody = contentBody.replace("<#verificationLink#>", (String)uSubs.get(emailId));
				mailSender.sendMessage(emailId, subject_HT , contentBody);
				System.out.println(i++ + " , email: " +emailId + " , link : " + uSubs.get(emailId));
				//System.out.println(contentBody);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}



	}
	
	public static long getUnverifiedSubscriberCount(DBCollection collection, String nlId) throws Exception {

		long count = 0;

		BasicDBObject filter = new BasicDBObject();
		filter.put("ID", nlId);
		filter.append("V_NL", false);

		DBObject query = new BasicDBObject("$elemMatch", filter);	
		
		BasicDBObject finalQuery = new BasicDBObject();
		finalQuery.put("NL_MAP", query);	

		count = collection.count(finalQuery);

		return count;

	}
	
	public static Map<String, String> getUnverifiedSubscriber(long totalUnverifiedSubs, String nlId) {

		Map<String, String> ls = new HashMap<String, String>();

		int perPage = 50;
		int pageNo = 1;
		int offset = 0;
		BasicDBObject filter = new BasicDBObject();
		filter.put("ID", nlId);
		filter.append("V_NL", false);

		DBObject query = new BasicDBObject("$elemMatch", filter);	
		
		BasicDBObject finalQuery = new BasicDBObject();
		finalQuery.put("NL_MAP", query);
	

		Map<String, Object> optionMap = new HashMap<String, Object>();
		optionMap.put("limit", perPage);
		optionMap.put("skip", "0");
		
		int loop = (int) ((totalUnverifiedSubs/perPage) + 1);

		for (int i =1; i <= loop; i++)  {

			List<DBObject> dbObjectList = MongoRestAPIService.getList("newslettersubscriptiontravel", finalQuery , optionMap);
			/*if (dbObjectList != null) {
				for (DBObject objDB : dbObjectList) {
					//String verificationLink = "http://www.happytrips.com/?activatesubscription=1&emailid=#emailid#&hashcode=#hashcode#&pwd=1&nlid=1031&configid=23450765&utm_source=newsletter&utm_medium=email&utm_campaign=NewsletterEmailer";
					String verificationLink = "http://jcmsdev.indiatimes.com:8365/?activatesubscription=1&emailid=#emailid#&hashcode=#hashcode#&pwd=1&nlid=1031&configid=23450765&utm_source=newsletter&utm_medium=email&utm_campaign=NewsletterEmailer";
					if (objDB.get("E_ID") != null && StringUtils.hasText(String.valueOf(objDB.get("E_ID")))
							&& objDB.get("H_CODE") != null && StringUtils.hasText(String.valueOf(objDB.get("H_CODE")))) {
						verificationLink = verificationLink.replaceAll("#emailid#", String.valueOf(objDB.get("E_ID")));
						verificationLink = verificationLink.replaceAll("#hashcode#", String.valueOf(objDB.get("H_CODE")));
						ls.put(String.valueOf(objDB.get("E_ID")), verificationLink);	
					}
				}				

			}*/
			
			for (DBObject objDB : dbObjectList) {
				String verificationLink = "http://www.happytrips.com/?activatesubscription=1&emailid=#emailid#&hashcode=#hashcode#&pwd=1&nlid=1031&configid=23450765&utm_source=newsletter&utm_medium=email&utm_campaign=NewsletterEmailer";
				//String verificationLink = "http://jcmsdev.indiatimes.com:8365/?activatesubscription=1&emailid=#emailid#&hashcode=#hashcode#&pwd=1&nlid=1031&configid=23450765&utm_source=newsletter&utm_medium=email&utm_campaign=NewsletterEmailer";
				if (objDB != null) {
					MailUser user = getMailUserFromDBObject(objDB);
					if(user != null && user.getEmailId() != null && user.getNewsletter() != null && user.getNewsletter().getId().equalsIgnoreCase("1031")) {
						verificationLink = verificationLink.replaceAll("#emailid#", user.getEmailId());
						verificationLink = verificationLink.replaceAll("#hashcode#", user.getNewsletter().getHashcode());
						ls.put(String.valueOf(objDB.get("E_ID")), verificationLink);	
					}
				}
			}

			offset = offset + perPage;
			optionMap.put("skip", String.valueOf(offset));
		}

		return ls;
	}
	
	
	private static MailUser getMailUserFromDBObject(DBObject objDB) {
		
		MailUser user = new MailUser();
		user.setEmailId(String.valueOf(objDB.get(Subscription.ALTERNATE_EMAIL_ID)));
		user.setSsoid(String.valueOf(objDB.get(Subscription.SSO_USER_ID)));
		
		
		if (objDB != null && objDB.get("NL_MAP") != null) {
			List<DBObject> nlListMap = (List<DBObject>) objDB.get(Subscription.NEWS_LETTER_MAP);
			if (nlListMap != null) {
				for(DBObject db : nlListMap){					
					if(db.containsField(NewsLetter.ID) && "1031".equals(String.valueOf(db.get(NewsLetter.ID)))){
						user.setNewsletter(NewsletterDBObjectWrapper.getNewsLetterFromBson(db));
						break;
					}
				}
			}
		}
		return user;
	}

}
