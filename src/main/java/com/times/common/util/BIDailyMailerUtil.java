package com.times.common.util;

/**
 * 
 */

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.times.common.dao.MongoRestAPIService;
import com.times.mailer.model.Subscription;


/**
 * db.newsletter.update({"ID" : "1007"}, {$set : {DL : NumberInt(1), WK : NumberInt(0)}})
 * This service is used to adhoc verify unsubscribed user and send activation mail.
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.BIDailyMailerUtil"
 * @author Rajeev Khatri
 * 
 */
public class BIDailyMailerUtil {

	
	private final static Logger logger = Logger.getLogger(BIDailyMailerUtil.class);
	public static void main(String[] args) throws FileNotFoundException {		
		updateRecord();
	}

	private static void updateRecord() {		

		Mongo mongo = null;
		try {			

			int perPage = 50;
			int pageNo = 1;
			int offset = 0;
			BasicDBObject filter = new BasicDBObject();
			filter.put("ID", "1007");			

			DBObject query = new BasicDBObject("$elemMatch", filter);	

			BasicDBObject finalQuery = new BasicDBObject();
			finalQuery.put("NL_MAP", query);


			Map<String, Object> optionMap = new HashMap<String, Object>();
			optionMap.put("limit", perPage);
			optionMap.put("skip", "0");

			int loop = 101;
			int count = 1;
			for (int i =1; i <= loop; i++)  {

				List<DBObject> dbObjectList = MongoRestAPIService.getList("newslettersubscription", finalQuery , optionMap);		

				if (dbObjectList != null) {
					for (DBObject objDB : dbObjectList) {

						List<DBObject> nlListMap = (List<DBObject>) objDB.get(Subscription.NEWS_LETTER_MAP);
						if (nlListMap != null) {
							for(DBObject db : nlListMap){
								if (db.get("ID") != null && db.get("ID").toString().equalsIgnoreCase("1007") && db.get("V_NL") != null 
										 && objDB.get("E_ID") != null ) {
									

									String emaild = (String) objDB.get("E_ID");
									String hashcode = (String) db.get("H_CODE");									
									
								
										BasicDBObject queryForUpdate = new BasicDBObject();		
										queryForUpdate.put("H_CODE", hashcode);
										queryForUpdate.put("ID", "1007");

										DBObject queryMatch = new BasicDBObject("$elemMatch", queryForUpdate);	

										BasicDBObject finalMatch = new BasicDBObject();
										finalMatch.put("NL_MAP", queryMatch);




										BasicDBObject datatoupdate = new BasicDBObject();					
										datatoupdate.put("NL_MAP" + ".$." + "DL", true);
										datatoupdate.put("NL_MAP" + ".$." + "WL", false);

										BasicDBObject newDocument = new BasicDBObject();
										newDocument.append("$set", datatoupdate);

										boolean isUpdate = MongoRestAPIService.update("newslettersubscription", finalMatch, newDocument);

										System.out.println(count++ + " ,updated for  : " + emaild);
									
								}

							}
						}


					}				

				}
				
				offset = offset + perPage;
				System.out.println("offset" + offset); 
				optionMap.put("skip", String.valueOf(offset));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (mongo != null) {
				mongo.close();
			}
		}
	}




}
