package com.times.common.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import com.times.common.mail.MailSender;

/**
 * command : nohup mvn exec:java -Dexec.mainClass="com.times.common.util.ManualVerificationMailProcessor"
 * @author Rajeev.Khatri
 *
 */
public class ManualVerificationMailProcessor {

	//private MailSender mailSender = null;
	
	public ManualVerificationMailProcessor() {
		//mailSender = new MailSender(TestMailSender.host,  TestMailSender.FROM,  TestMailSender.conentPath,  TestMailSender.replyBackId);
	}
	
	private static final String MAIL_BODY_TOI_4_VERIFICTION = "Dear Reader, <br /><br />" +
			"You recently subscribed to one or more of The Times of India newsletters, however we are unable to start sending them since your subscription request is still not verified."
			+ "<br /><br /> Click on the below link to verify your request. "
			+ "<br /> <a href=\"<#verificationLink#>\">Verification Link</a>"
		+ "<br /><br />Thanks, <br />"
		+ "The Times of India Team";
	
	private final static String subject = "You are one click away from your newsletter subscription ";
	public static final String FROM = "The Times Of India <mailerservice@timesofindia.com>";
	public static final String replyBackId = "The Times Of India <mailerservice@timesofindia.com>";
	
	public static final String host = "nmailer.indiatimes.com";
	public static final String conentPath = "timesofindia@bounce.indiatimes.com";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FileInputStream fis;
		String emaild = null;
		String contentBody = null;
		MailSender mailSender = null;
		try {
			//if (args != null && )
			mailSender = new MailSender(host,  FROM,  conentPath,  replyBackId);
			//fis = new FileInputStream("G:/log/unverified users.xls");
			//fis = new FileInputStream("G:/log/test-mail.xls");
			//fis = new FileInputStream("/opt/svn/docs/test-mail.xls");
			fis = new FileInputStream("/opt/svn/docs/unverified-users.xls");
			
			List<Map<String, Object>> list = XlsReaderUtil.getRecordsList(fis);
			int i = 1;
			for (Map<String, Object> map : list) {
				emaild = (String)map.get("email");
				contentBody = MAIL_BODY_TOI_4_VERIFICTION;
				contentBody = contentBody.replace("<#verificationLink#>", (String)map.get("link"));
				mailSender.sendMessage((String)map.get("email"), subject , contentBody);
				System.out.println(i++ + " , email: " + map.get("email") + " , link : " + map.get("link"));
				//System.out.println(contentBody);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		

	}

}
