package com.times.common.constant;

public class AmazonResponseFields {

	public static final String ITEM_SEARCH_RESPONSE="ItemSearchResponse";
	public static final String ITEM_LOOKUP_RESPONSE="ItemLookupResponse";
	public static final String ITEMS="Items";
	public static final String ITEM="Item";
	public static final String REQUEST="Request";
	public static final String IS_VALID="IsValid";
	public static final String TOTAL_RESULTS="TotalResults";
	public static final String TOTAL_PAGES="TotalPages";
	public static final String ERRORS="Errors";
	public static final String ERROR="Error";
	public static final String CODE="Code";
	

}
