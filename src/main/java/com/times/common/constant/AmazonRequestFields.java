package com.times.common.constant;

import java.util.HashMap;
import java.util.Map;

public class AmazonRequestFields {

	public static final String SERVICE  ="Service";
	public static final String OPERATION ="Operation";
	public static final String AWS_ACCESS_KEYID  ="AWSAccessKeyId";
	public static final String ASSOCIATE_TAG  ="AssociateTag";
	public static final String SEARCH_INDEX="SearchIndex";
	public static final String ID_TYPE="IdType";
	public static final String RESPONSE_GROUP ="ResponseGroup";
	public static final String BROWSE_NODE="BrowseNode";
	public static final String MINIMUM_PRICE="MinimumPrice";
	public static final String MAXIMUM_PRICE="MaximumPrice";
	public static final String ITEM_PAGE="ItemPage";
	public static final String SORT="Sort";
	public static final String SORT_PRICE_ASC="price";
	public static final String SORT_PRICE_DESC="-price";
	public static final String KEYWORDS="Keywords";
	public static final String CONDITION="Condition";
	public static final String TIMESTAMP="Timestamp";
	public static final String REQUEST_DATE_FORMAT="yyyy-MM-dd'T'HH:mm:ss'Z'";
	public static final String MANUFACTURER="Manufacturer";
	public static final String ELECTRONICS_SEARCH_INDEX="Electronics";
	public static final String ID_TYPE_ASIN="ASIN";
	public static final String ITEM_ID = "ItemId";

	
	
	public static final String ITEMSEARCH="ItemSearch";
	public static final String ITEM_LOOKUP ="ItemLookup";
	
	//Defaults
	public static final String DEFAULT_RESPONSE_GROUP = "Large";
	public static final String DEFAULT_ID_TYPE = ID_TYPE_ASIN;
	
	
	
	

}
