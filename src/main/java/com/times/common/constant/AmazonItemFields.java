package com.times.common.constant;

public class AmazonItemFields {
	
	/*
	 * FIELD NAMES FROM AMAZON JSON >>>START
	 */
	public static final String ITEM_ATTRIBUTES="ItemAttributes";
	public static final String BROWSE_NODES="BrowseNodes";
	public static final String BROWSE_NODE="BrowseNode";
	public static final String BROWSE_NODE_ID="BrowseNodeId";
	public static final String TITLE="Title";
	public static final String OFFER_SUMMARY="OfferSummary";
	public static final String LISTPRICE="ListPrice";
	public static final String LOWEST_NEW_PRICE="LowestNewPrice";
	public static final String FORMATTED_PRICE="FormattedPrice";
	public static final String ASIN= "ASIN";
	public static final String DETAIL_PAGE_URL="DetailPageURL";
	public static final String SALES_RANK="SalesRank";
	public static final String BRAND="Brand";
	public static final String MANUFACTURER="Manufacturer";
	public static final String OFFERS="Offers";
	public static final String OFFER="Offer";
	public static final String OFFER_LISTING="OfferListing";
	public static final String IS_ELIGIBLE_FOR_PRIME="IsEligibleForPrime";
	public static final String COLOR="Color";
	public static final String SMALL_IMAGE = "SmallImage";
	public static final String PRODUCT_TYPE_NAME="ProductTypeName";
	public static final String AMOUNT = "Amount";
	public static final String ITEM_ID="ItemId";
	public static final String ID_TYPE="IdType";
	public static final String EDITORIAL_REVIEWS="EditorialReviews";
	public static final String CONTENT="Content";
	public static final String AMAZON_SIMILAR_FROM_SEARCH="amazon_related";
	public static final String AMAZON_EXACT_FROM_DUMP="amazon_exact";
	public static final String LOWEST_USED_PRICE = "LowestUsedPrice";
	public static final String EDITORIAL_REVIEW="EditorialReview";
	
	/*
	 * FIELD NAMES FROM AMAZON JSON >>>END
	 */

	
	

	/*
	 * FIELD NAMES WITH WHICH WE ARE SAVING ITEM DETAILS IN OUR DB >>>START
	 */
	
	public static final String DB_NAME="name";
	public static final String DB_LCNAME="lcname";
	public static final String DB_LIST_PRICE="ListPrice";
	public static final String DB_LOWEST_NEW_PRICE="LowestNewPrice";
	public static final String DB_ASIN="asin";
	public static final String DB_IS_PRIME="is_prime";
	public static final String DB_DETAIL_PAGE_URL="url";
	public static final String DB_THUMB="thumb";
	public static final String DB_SMALL_IMAGE="SmallImage";
	public static final String DB_BRAND = "Brand";
	public static final String DB_COLOR = "Color";
	public static final String DB_MANUFACTURER = "Manufacturer";
	public static final String DB_SALES_RANK = "SalesRank";
	public static final String DB_BROWSE_NODE = "BrowseNode";
	public static final String DB_LOWEST_USED_PRICE = "LowestUsedPrice";
	public static final String DB_LCUNAME = "lcuname";
	public static final String DB_UPDATED_AT="updatedAt";
	
	/*
	 * FIELD NAMES WITH WHICH WE ARE SAVING ITEM DETAILS IN OUR DB >>>END
	 */
	
	
	
	

}
