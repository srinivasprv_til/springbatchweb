package com.times.common.constant;

public class UserFeedConstant {

	public static String ID = "_id";
	public static String USERID = "user_Id";
	public static String FROMADDRESS = "from_address";
	public static String LOCATION = "location";
	public static String FROMNAME = "from_name";
	public static String INSERTEDDATE = "inserted_date";
	public static String UPDATEDDATE = "updated_date";
	public static String STATUS = "status";
	public static String STATUSVAL = "inserted";
	public static String STATUSVALUPD = "updated";
	public static String USERACTIVITYLIST = "user_activity_list";
	
	public static String ACTIVITY = "user_activity";
	public static String MSID = "msid";
	public static String WISHLIST = "wishlist";
	public static String USERRATING = "userRating";
	public static String USERUPVOTE = "userUpvote";
	public static String BEENTHERE = "beenThere";
	
	public static String COMMENT = "user_comment";
	public static String COMMENTTEXT = "commentText";
	public static String HOSTID = "hostId";
	public static String PARENTMSID = "parentmsid";
	public static String COMMENTTIME = "commentTime";
	public static String LANGID = "langId";
	public static String RAWFEEDKEY = "raw_key";
	public static String RAWFEEDDATA = "raw_data";
	public static String FEEDTYPE = "feed_type";
}

