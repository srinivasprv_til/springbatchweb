package com.times.common.constant;

/**
 * The Class AmazonBrowseNodes.
 * Mapping of our categories to corresponding Amazon BrowseNode Id's
 */
public class AmazonBrowseNodes {
	
	public static final String SMART_AND_BASIC_PHONES_ID="1389432031";
	public static final String TABLETS_ID="1375458031";
	public static final String LAPTOPS_ID="1375424031";
	public static final String ACTIVITY_TRACKERS="4730577031";
	public static final String TELEVISIONS="1389396031";
	public static final String CAMERAS_AND_PHOTOGRAPHY="1388977031";
	public static final String SMART_WATCH_AND_ACCESSORIES="5605728031";
	public static final String POWERBANKS="6612025031";
	public static final String AIR_CONDITIONER="3474656031";
	public static final String REFRIDGERATORS="1380365031";
	public static final String WASHING_MACHINE="1380369031";

}
