package com.times.common.service;

import java.util.List;

import com.times.common.model.UserComment;
import com.times.common.model.UserFeed;

public interface OfflineFeedService {
	
	public void postUserActivity(UserFeed userFeed);
	public int postMytimesActivity(int msid, String uuid, String activityType,String rating) throws Exception;
	public String postComments(int msid, String uuid, UserComment userComment, UserFeed userFeed) throws Exception;
	public int removeMytimesActivity(int msid, String uuid, String activityType) throws Exception;
	public List<UserFeed> getUserFeedList() throws Exception;

}
