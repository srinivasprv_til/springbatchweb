package com.times.common.service;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.times.common.constant.AmazonRequestFields;
import com.times.common.model.AmazonItem;
import com.times.common.model.AmazonSearchResponse;
import com.times.common.util.AmazonRequestSignee;
import com.times.parser.ConvertXMLtoJSON;

/**
 * The Class AmazonItemSearchService.
 * DEFAULTS: 
 * 		If operation is not specified ItemSearch is taken
 * 		If response group is not specified, Large is taken
 * 
 * @author daksh.verma
 */

public class AmazonItemSearchService implements AmazonProductsAPIService{
	
	private AmazonRequestSignee amazonRequestSignee;
	
	private FileContentServices fileContentServices;
	
	private static final Logger logger = LoggerFactory.getLogger(AmazonItemSearchService.class);
	
	/** The Constant MAX_TRIES.
	 *  Since Amazons throttles requests if made frequently, So, for a throttled request (which returns 503),retries are made
	 **/
	private static final int MAX_TRIES=2;
	
	
	/** The Constant SLEEP_DURATION_MILISECOND.
	 *  For throttled requests, sleep the thread for this duration
	 **/
	private static final int SLEEP_DURATION_MILISECOND=1800;
	

	/* 
	 * gets Products/Items from Amazon Product Advertisement API on the basis of parameters
	 */
	@Override
	public AmazonSearchResponse getProductsFromAmazon(Map<String, String> params) {
		
		//If response group is not specified, take it as large
		if(!params.containsKey(AmazonRequestFields.RESPONSE_GROUP)){
			params.put(AmazonRequestFields.RESPONSE_GROUP, AmazonRequestFields.DEFAULT_RESPONSE_GROUP);
		}
		
		//DEFAULT OPERATION FOR THE METHOD IS ITEM SEARCH, SEND OPERATION EXPLICITLY IF REQUIRED.
		if(!params.containsKey(AmazonRequestFields.OPERATION) ){
			params.put(AmazonRequestFields.OPERATION, AmazonRequestFields.ITEMSEARCH);
		}
		
		//Default Search Index when operation is item search is Electronics
		if(params.containsKey(AmazonRequestFields.OPERATION) &&
				AmazonRequestFields.ITEMSEARCH.equals(params.get(AmazonRequestFields.OPERATION)) &&
				!params.containsKey(AmazonRequestFields.SEARCH_INDEX)){
			params.put(AmazonRequestFields.SEARCH_INDEX, AmazonRequestFields.ELECTRONICS_SEARCH_INDEX);
		}
		

		for (int retryCount=1;retryCount<=MAX_TRIES;++retryCount){
			AmazonSearchResponse amazonResponse=getProductsFromAmazonHelper(params,retryCount);
			if(amazonResponse!=null){
				return amazonResponse;
			}
			try {
				Thread.sleep(SLEEP_DURATION_MILISECOND);
			} catch (InterruptedException e) {
				
			}
			
			logger.error("Error getting response from amazon API, retryCount="+retryCount+" params ="+params);
		}
		
		return null;
		
		
	}
	
	/**
	 * Gets the products from amazon helper.
	 *
	 * @param  params the params
	 * @param  retryCount the retry count
	 * @return the products from amazon helper
	 */
	private AmazonSearchResponse getProductsFromAmazonHelper(Map<String,String> params,int retryCount){
		
		if(retryCount>MAX_TRIES){
			return null;
		}
		
		//Safety check, If somehow amazonRequestSignee is null, EXTREMELY CRITICAL ERROR
		if(amazonRequestSignee==null){
			try {
				amazonRequestSignee= new AmazonRequestSignee();
			} catch (InvalidKeyException | NoSuchAlgorithmException | IOException e) {
				logger.error("CRITICAL: AmazonRequest Signee, Expired",e);
			}
		}
		
		if(params==null){
			return null;
		}
		
		
		//If operation is not specified, Item search is done
		if (!params.containsKey(AmazonRequestFields.OPERATION)){
			params.put(AmazonRequestFields.OPERATION, AmazonRequestFields.ITEMSEARCH);
		}
		
		
		String signedURL=amazonRequestSignee.sign(params);
		
		//get Contents of the url, returns null when STATUS_CODE!=200 or readTimeOut etc
		String extData=fileContentServices.getExtDataWithoutRetry(signedURL);
		
		//parse into JSONObject
		if (extData!=null) {
			JSONObject responseJSON = ConvertXMLtoJSON.getJSONObjectfromXML(extData);
			return new AmazonSearchResponse(responseJSON);
		}else{
			return null;
		}
	
		
	}
	
	
    /**
     * The main method.Driver
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
    	AmazonProductsAPIService a= new  AmazonItemSearchService();
    	
    	Map<String, String> params = new HashMap<String, String>();
    	 
        params.put("Service", "AWSECommerceService");
        params.put("Operation", "ItemSearch");
        params.put("AWSAccessKeyId", "AKIAJX7PDW7DEP2LLJLA");
        params.put("AssociateTag", "timofind-21");
        params.put("SearchIndex", "Electronics");
        params.put("ResponseGroup", "Large");
        params.put("BrowseNode", "1805560031");
        params.put("MinimumPrice", "1000000");
        params.put("MaximumPrice", "1200000");
        
        AmazonSearchResponse src=(AmazonSearchResponse) a.getProductsFromAmazon(params);
        
       // System.out.println(src.getTotalPages());
        //System.out.println(src.getErrorCode());
        //System.out.println(src.getTotalResults());
        List<AmazonItem> items=src.getItemList();
        for (AmazonItem item: items){
        	System.out.println(item.getTitle());
        	System.out.println(item.getASIN());
        	System.out.println(item.getDetailPageURL());
        	System.out.println(items.get(0).getBrowseNode());
        	
        }
        
        if(true)
        return;
       
        }

    
	/**
	 * Gets the amazon request signee.
	 *
	 * @return the amazon request signee
	 */
	public AmazonRequestSignee getAmazonRequestSignee() {
		return amazonRequestSignee;
	}

	/**
	 * Sets the amazon request signee.
	 *
	 * @param amazonRequestSignee the new amazon request signee
	 */
	public void setAmazonRequestSignee(AmazonRequestSignee amazonRequestSignee) {
		this.amazonRequestSignee = amazonRequestSignee;
	}

	/**
	 * Gets the file content services.
	 *
	 * @return the file content services
	 */
	public FileContentServices getFileContentServices() {
		return fileContentServices;
	}

	/**
	 * Sets the file content services.
	 *
	 * @param fileContentServices the new file content services
	 */
	public void setFileContentServices(FileContentServices fileContentServices) {
		this.fileContentServices = fileContentServices;
	}
        
        
    }
	



