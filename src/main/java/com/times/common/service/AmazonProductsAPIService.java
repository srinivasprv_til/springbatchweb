package com.times.common.service;

import java.util.Map;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.times.common.model.AmazonSearchResponse;
import com.times.common.util.AmazonRequestSignee;

public interface AmazonProductsAPIService {
	
	public AmazonSearchResponse getProductsFromAmazon(Map<String,String> params);
	
	
}
