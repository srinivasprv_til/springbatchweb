package com.times.common.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.Permission;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.transfer.TransferManager;

public class AmazonS3StorageService {
	private static String ACCESS_KEY_ID = "AKIAI2WOA2MZ3XKKNWPQ";
	private static String SECRET_ACESS_KEY = "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";
	
	
//	private static final String FILE_NAME = "testZipFile.zip";
	private static final String BUCKET_NAME = "newspoint";

	public void upload(byte[] zipFileContent, String fileName) throws IOException {
		
//		Path testFilePath = Paths.get("/Users/arun.sharma2/Downloads/testZipFile.zip");
//		byte[] zipFileContent = Files.readAllBytes(testFilePath);

		AWSCredentials awsCredentials = new BasicAWSCredentials(ACCESS_KEY_ID, SECRET_ACESS_KEY);
		TransferManager tm = new TransferManager(awsCredentials);
		
		
		int len = zipFileContent.length;
	    InputStream inputStream = new ByteArrayInputStream(zipFileContent);
		ObjectMetadata meta =  new ObjectMetadata();
		
		meta.setContentLength(len);
		meta.setContentType("application/zip");
		
		
		AccessControlList acl = new AccessControlList();
		acl.grantPermission(GroupGrantee.AllUsers, Permission.Read);
		AmazonS3 s3client = tm.getAmazonS3Client();
		PutObjectResult putObject = s3client.putObject(new PutObjectRequest(BUCKET_NAME, fileName , inputStream, meta).withAccessControlList(acl));
		System.out.println(putObject);
		
	}

}
