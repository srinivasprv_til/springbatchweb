package com.times.common.service;

import java.util.Map;

import com.times.mailer.model.AmazonS3Credits;
import com.times.mailer.model.ImageInfo;
import com.times.mailer.model.NetstorageInfo;

/**
 * This inerfce is used to upload the file based on the source and target information.
 * 
 * @author Ranjeet.Jha
 *
 */
public interface FileContentServices {

	
	/**
	 * This method is used to upload the content in specified FTP location.
	 * In the domain model ftp location mentioned.
	 * 
	 * @param ftpDtls
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public boolean uploadContentByFTP(NetstorageInfo ftpDtls, String content)throws Exception;
	
	/**
	 * This method is used to get the content by provided source location by using Apache HTTPConnection.
	 * 
	 * @param url
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public String getContentByHTTP(String url) throws Exception;
	
	public String getContentByHTTP(String url, int coonectTimeout, int socketTimeout) throws Exception;
	
	
	/**
	 * This method is used for overloaded .
	 * 
	 * @param url
	 * @param failoverUrl
	 * @return
	 * @throws Exception
	 */
	public String getContentByHTTP(String url, String failoverUrl) throws Exception;
	
	/**
	 * This method is used to store the file in local specified location for testing puporse
	 * 
	 * @param netstorageInfo
	 * @param fileConent
	 * @return
	 */
	public boolean writeFileInLocalFileSystem(NetstorageInfo netstorageInfo, String fileConent) ;

	/**
	 * Upload content at amazon storage destination using bucket name, access key etc.  
	 * @param credits
	 * @param str
	 * @param headerDetails
	 * @return boolean
	 */
	public boolean uploadFileAmazonS3(AmazonS3Credits credits, String str, Map<?, ?> headerDetails);
	
	public String getExtData(String url);
	
	
	/**
	 * This API fetch content and headers for given url. Both Content and headers are returned in map with keys 'Content' and 'Header'.
	 * @param url
	 * @return Map
	 * @throws Exception
	 */
	public Map getContentAndHeaderByHTTP(String url) throws Exception;
	
	public ImageInfo getOptimizedImage(ImageInfo imageInfo) throws Exception; 
	
	public String getExtDataWithoutRetry(String strURL);
	
}
