package com.times.common.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.times.common.constant.AmazonRequestFields;
import com.times.common.model.AmazonItem;
import com.times.common.model.AmazonLookupResponse;
import com.times.common.model.AmazonSearchResponse;


public class AmazonItemLookupService  {
	
	private static final String COMMA_SEPRATOR=",";
	
	AmazonProductsAPIService amazonItemSearchService;
	
	public AmazonLookupResponse getProductsFromAmazon(Map<String, String> params){
		
		if(!params.containsKey(AmazonRequestFields.OPERATION)){
			params.put(AmazonRequestFields.OPERATION, AmazonRequestFields.ITEM_LOOKUP);
		}
		
		if(!params.containsKey(AmazonRequestFields.ID_TYPE)){
			params.put(AmazonRequestFields.ID_TYPE, AmazonRequestFields.DEFAULT_ID_TYPE);
		}
		
		AmazonSearchResponse amazonSearchResponse=amazonItemSearchService.getProductsFromAmazon(params);
		
		if(amazonSearchResponse!=null){
			AmazonLookupResponse amazonLookupResponse= new AmazonLookupResponse(amazonSearchResponse.getSearchResponse());
			return amazonLookupResponse;
		}
		
		return null;
			
	}
	
	
	public AmazonLookupResponse getProductByASIN(String asin){
		
		Map<String,String> params= new HashMap<>();
		
		if(StringUtils.isNotBlank(asin)){
			params.put(AmazonRequestFields.ITEM_ID, asin);
		}
		
		AmazonLookupResponse amazonLookupResponse= getProductsFromAmazon(params);
		
		return amazonLookupResponse;
	}
	
	public List<AmazonItem>  getProductsOnASINList(List <String> ASINSList){
		
		List <AmazonItem> amazonItems= new ArrayList<>();
		
		if(ASINSList!=null && ASINSList.size()>0){
			
			
			int numASINS= ASINSList.size();
			int numPages=(int) Math.ceil(numASINS/10.0);
			
			for (int pageNum=0;pageNum<numPages;++pageNum){
				
				int start=pageNum*10;
				int end=(pageNum+1)*10;
				if(end>numASINS) end=numASINS;
				
				List<String> tempList= ASINSList.subList(start, end);
				
				String asinQuery="";
				
				for (String asin: tempList){
					if(StringUtils.isNotBlank(asin)){
						asinQuery=asinQuery+COMMA_SEPRATOR+asin.trim();
					}
				}
				
				asinQuery=asinQuery.substring(1);
				
				AmazonLookupResponse amazonLookupResponse=getProductByASIN(asinQuery);
				
				if(amazonLookupResponse!=null){
					amazonItems.addAll(amazonLookupResponse.getItemList());
				}
				
				
			}
			
			
		}
		
		return amazonItems;
		
	}


	public AmazonProductsAPIService getAmazonItemSearchService() {
		return amazonItemSearchService;
	}


	public void setAmazonItemSearchService(AmazonProductsAPIService amazonItemSearchService) {
		this.amazonItemSearchService = amazonItemSearchService;
	}

}
