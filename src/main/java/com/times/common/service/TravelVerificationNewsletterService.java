/**
 * 
 */
package com.times.common.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.StringUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.mail.MailSender;
import com.times.common.util.DateUtils;
import com.times.common.util.MailerUtil;
import com.times.mailer.dao.MailerDaoImpl;
import com.times.mailer.dao.TravelNewsletterDao;
import com.times.mailer.dao.TravelNewsletterDaoImpl;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetter;
import com.times.mailer.model.Subscription;

/**
 * This class is used to send the newsletter to user who has verified just few minutes back.
 * This is for Travel site as of now.
 * 
 * @author Ranjeet.Jha
 *
 */
public class TravelVerificationNewsletterService {
	private final static Logger logger = LoggerFactory.getLogger(TravelVerificationNewsletterService.class);
	
	private static ApplicationContext ctx = null;
	//private String jobName="nbtMailer";
	
	private static final String TRAVEL_VERIFICATION_CONTEXT = "travel-verification-mail-context.xml";
	private static final String  TRAVEL_NEWSLETTER_DAO_ID = "travelNewsletterDao";
	private static final String  TRAVEL_NEWSLETTER_MAIL_SENDER_ID = "mailSender";
	private static Map<String, String> TEMPLATE_CONTENT_MAP = new HashMap<String, String>();
	
	private static String nlid = "1031";
	
	//@Scheduled(cron="*/5 * * * * ?")
	public static void schedule(){
		System.out.println("inside the method at "+ new Date());
		String subject = null;
		String url4Html = null;
		String htmlContent = null;
		try {
			if (ctx == null){
				System.out.println("ctx reinitialized");
				ctx = new ClassPathXmlApplicationContext(new String[]{TRAVEL_VERIFICATION_CONTEXT});
			}
			TravelNewsletterDao travelNewsletterDao = ctx.getBean(TRAVEL_NEWSLETTER_DAO_ID, TravelNewsletterDaoImpl.class);
			MailSender mailSender = ctx.getBean(TRAVEL_NEWSLETTER_MAIL_SENDER_ID, MailSender.class);
			
			List<DBObject> dbObjects = travelNewsletterDao.getNewsletterDBObjectById(nlid);
			if (dbObjects != null) {
				for (DBObject dbobject : dbObjects) {
					if (dbobject.get(Mailer.EXTPATH) != null) {
						url4Html = (String)dbobject.get(Mailer.EXTPATH);
						htmlContent = getContnetByURL(url4Html, (String)dbobject.get(Mailer.ID));
						subject = (String) dbobject.get(Mailer.SUBJECT);
						 
						if (StringUtils.hasText(htmlContent)) {
							// In mongoDB date time is in GMT so we need to subtract 5:30 from system time to get actual timestamp. 
							/*Date startDate = DateUtils.addHrMinutesToDate(new Date(), -6, -60);
							Date endDate =  DateUtils.addHrMinutesToDate(new Date(), -5, -30);*/
							
							Date startDate = DateUtils.addMinutesToDate(-5);
							Date endDate =  new Date();
							
							// add 2 hour previous stuff.
							//startDate = DateUtils.addHrMinutesToDate(startDate, -2, 0);
							
							//List<DBObject> userdbObjects = mailerDao.getUsersDBObjectBetweenDates(nlid, true, startDate, endDate);
							List<DBObject> userdbObjects = getUsersDBObjectBetweenDates(nlid, true, startDate, endDate);
							//List<BasicDBObject> userdbObjects1 = findByDateRange(mongoOperation, nlid, true, startDate, endDate);
							if (userdbObjects != null) {
								for (DBObject userDBobj : userdbObjects) {
									String emailId = (String) userDBobj.get(Subscription.SSO_USER_ID);
									logger.debug("going to sent nl after verfication on emailId: " + emailId);
									if (emailId != null && subject != null && htmlContent != null) {
										htmlContent = htmlContent.replace("#emailid#", emailId);
										mailSender.sendMessage(emailId, subject, htmlContent);
										logger.debug("sent nl after verfication on emailId: " + emailId);
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("exception logged when schedule method called. " + e.getMessage());
		} finally{
			
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
    	ctx = new ClassPathXmlApplicationContext(new String[]{TRAVEL_VERIFICATION_CONTEXT});
    	schedule();
	}
	
	private static String getContnetByURL(String url4Html, String nlid) {
		String content = MailerUtil.getData(url4Html);
		if (StringUtils.hasText(content)) {
			return content;
		} else {
			getContnetByURL(url4Html, nlid);
		}
		
		return content;
	}
	
	public List<DBObject> getNewsletterDBObjectById(String nlId) {
		List<DBObject> dbObjectList = null;
		try {
			DBObject query = new BasicDBObject();
			query.put(Mailer.ID, nlId);
			//query.put(, nlId);
			
			dbObjectList = MongoRestAPIService.getList(MailerDaoImpl.NEWSLETTER_MASTER_COLLECTION, query);
			
		} catch (Exception e) {
			logger.debug("getNewsletterMailer method , ex : " + e.getMessage());
		}
		
		return dbObjectList;
	}
	
	/* (non-Javadoc)
	 * @see com.times.mailer.dao.MailerDao#getNewsletterMailer(com.mongodb.DBObject)
	 */
	public static List<DBObject> getUsersDBObjectBetweenDates(String nlId, boolean verifiedUser, Date startDate, Date endDate) {
		List<DBObject> dbObjectList = null;
		try {
			DBObject query = new BasicDBObject();
			//query.put(Mailer.ID, nlId);
			query.put(Subscription.NEWS_LETTER_MAP + "." + NewsLetter.ID, nlId);
			query.put(Subscription.NEWS_LETTER_MAP + "." + NewsLetter.VERIFIED_NEWSLETTER, verifiedUser);
			query.put("NL_MAP.U_AT", new BasicDBObject("$gt", startDate).append("$lt", endDate));
			//System.out.println("startDate : " + startDate + " , endDate : " +endDate);
			//System.out.println(query);
			dbObjectList = MongoRestAPIService.getList(TravelNewsletterDaoImpl.SUBSCRIPTION_TRAVEL_COLLECTION, query);
			
		} catch (Exception e) {
			logger.debug("getNewsletterMailer method , ex : " + e.getMessage());
		}
		
		return dbObjectList;
	}
	
}
