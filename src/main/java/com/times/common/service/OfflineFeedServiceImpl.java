package com.times.common.service;
import java.util.Date;
import java.util.List;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.times.common.dao.OfflineFeedDao;
import com.times.common.joblauncher.OfflineFeedJobLauncher;
import com.times.common.model.UserActivity;
import com.times.common.model.UserComment;
import com.times.common.model.UserFeed;
import com.times.common.util.OfflineFeedUtilities;

public class OfflineFeedServiceImpl implements OfflineFeedService{
	
	OfflineFeedDao offlineFeedDao;
	
	private final static Logger logger = LoggerFactory.getLogger(OfflineFeedServiceImpl.class);
	

	private String activity = "http://myt.indiatimes.com/mytimes/addActivity?"
			+ "appKey=Travel&baseEntityType=ARTICLE&objectType=B&activityType=@activityType&"
			+ "uniqueAppID=@uniqueAppID&uuId=@uuid";
	private String activityDetail = "http://myt.indiatimes.com/mytimes/travel/activity?"
			+ "appKey=Travel&activityType=@activityType&msid=@msid&uuId=@uuid";
	private String rating = "http://www.happytrips.com/feeds/ratingapi_test.cms?getuserrating=1&msid=@msid";
	private String removeActivity = "http://myt.indiatimes.com/mytimes/removeActivity?"
			+ "activityId=@activityId&uuId=@uuid";
	private static String postroUrl = "http://www.happytrips.com/postro_offlinefeed.cms";	
	private String postCommetQueryString = "ArticleID=@msid&fromaddress=@fromaddress&fromname=@fromname&location=@location&"
			+ "commentdate=@commentdate&loggedstatus=1&message=@message&msid=@msid&roaltdetails=1&parentid=@parentid&userid=@uuid";

	
	
	
	/*	
		Add Been There/ Wishlist / Upvote :
	
		http://myt.indiatimes.com/mytimes/addActivity?appKey=Travel&baseEntityType=ARTICLE&objectType=B&
		activityType=@activityType&uniqueAppID=@uniqueAppID&uuId=@uuid
	
		@activityType : WishList/BeenThere/Happy
	
		Remove BeenThere / Wishlist :
	
		1) getActivity ID : 
		http://myt.indiatimes.com/mytimes/travel/activity?appKey=Travel&activityType=@activityType&msid=@msid&uuId=@uuid
	
		2) Remove Activity ID : 
		http://myt.indiatimes.com/mytimes/removeActivity?activityId=@activityId&uuId=@uuid
	
		Submit rating:
	
		http://myt.indiatimes.com/mytimes/addActivity?activityType=Rated&appKey=Travel&uniqueAppID=@msid&baseEntityType=ARTICLE&objectType=B&url=@url&userrating=@rating&uuId=@uuId
		http://www.happytrips.com/feeds/ratingapi_test.cms?getuserrating=1&msid=27236125
		
		Post comment :
	
		http://www.happytrips.com/postro.cms?ArticleID=@articleID&fromaddress=@fromAddress&fromname=@fromName&location=@location&loggedstatus=1&message=@message&
		msid=@articleID&roaltdetails=1&rootid=@rootID&userid=@uuid
	*/
	
	
	public void postUserActivity(UserFeed userFeed){
		String activityType = "";
		String strURL = "";
		String uuid = userFeed.getUserId();
		if(userFeed.getUserActivityList()!=null){
			List<UserActivity> userActivityList = userFeed.getUserActivityList();
			for(UserActivity userActivity : userActivityList){
				int msid = userActivity.getMsid();
				int status;
				if(userActivity.getBeenThere() == 1){
					activityType = "BeenThere";
					try{
						status = postMytimesActivity(msid, uuid, activityType,"");
					}catch(Exception e){
						logger.error("Error in BeenThere : "+e.getMessage());
					}
				}else if(userActivity.getBeenThere() == 0){
					activityType = "BeenThere";
					try{
						status = removeMytimesActivity(msid, uuid, activityType);
					}catch(Exception e){
						logger.error("Error in remove BeenThere : "+e.getMessage());
					}
				}
				if(userActivity.getWishlist() == 1){
					activityType = "WishList";
					try{
						status = postMytimesActivity(msid, uuid, activityType,"");
					}catch(Exception e){
						logger.error("Error in WishList : "+e.getMessage());
					}

				}else if(userActivity.getWishlist() == 0){
					activityType = "WishList";
					try{
						status =  removeMytimesActivity(msid, uuid, activityType);
					}catch(Exception e){
						logger.error("Error in Remove WishList : "+e.getMessage());
					}
				}
				if(userActivity.getUserUpvote() == 1){
					activityType = "Happy";
					try{
						status = postMytimesActivity(msid, uuid, activityType,"");
					}catch(Exception e){
						logger.error("Error in Happy : "+e.getMessage());
					}

				}
				if(userActivity.getUserRating() > 0){
					activityType = "Rated";
					try{
						status = postMytimesActivity(msid, uuid, activityType, String.valueOf(userActivity.getUserRating()));
					}catch(Exception e){
						logger.error("Error in Rated : "+e.getMessage());
					}
				}
				
				if(userActivity.getUserComment() != null){
					if(userActivity.getUserComment()!=null){
						try{
							UserComment userComment = userActivity.getUserComment();
							logger.info("Comment post updates : "+ postComments(msid, uuid, userComment, userFeed));
						}catch(Exception e){
							logger.error("Error in Comment post updates : "+e.getMessage());
						}

					}				
				}		
			}
			
			try {
				boolean mongoUpdates = updateStatus(userFeed);
				logger.info("Mongo Updates : "+mongoUpdates);
			} catch (Exception e) {
				logger.error("Error in Mongo Updates : "+e.getMessage());
			}
		}
	}
	
	public int postMytimesActivity(int msid,  String uuid, String activityType, String rating){
		String strURL = activity;
		strURL = strURL.replace("@activityType", activityType);
		strURL = strURL.replace("@uniqueAppID", msid + "");
		strURL = strURL.replace("@uuid", uuid);
		if("Rated".equals(activityType)){
			strURL = strURL+"&userrating="+rating;
			String ratingURL = rating;
			ratingURL = strURL.replace("@msid", msid + "");
			String statusJcms = OfflineFeedUtilities.getExtData(strURL);
			logger.info("activityType : "+activityType + " | strURL : "+strURL + " | status : "+statusJcms);
		}
		int status = OfflineFeedUtilities.getExtStatus(strURL);
		logger.info("activityType : "+activityType + " | strURL : "+strURL + " | status : "+status);
		return status;
	}
	
	public int removeMytimesActivity(int msid, String uuid, String activityType){
		String sActivity = activityDetail;
		String rActivity = removeActivity;
		int status=0;
		sActivity = sActivity.replace("@activityType", activityType);
		sActivity = sActivity.replace("@msid", msid + "");
		sActivity = sActivity.replace("@uuid", uuid);
		String actDetail = OfflineFeedUtilities.getExtData(sActivity);
		//logger.info("actDetail ::"+ actDetail);
		logger.info("ActivityId for REMOVE activity : "+activityType + " | strURL : "+sActivity + " | status : "+status);
		if(actDetail != null && !"[]".equals(actDetail) && !"[null]".equals(actDetail)){
			actDetail=actDetail.replace("[", "");
			actDetail=actDetail.replace("]", "");
			//logger.info("actDetail ::"+ actDetail);
			JSONObject jobj = new JSONObject(actDetail);
			String activityId = String.valueOf(jobj.get("_id"));
			rActivity = rActivity.replace("@activityId", activityId);
			rActivity = rActivity.replace("@uuid", uuid);
			status = OfflineFeedUtilities.getExtStatus(rActivity);
		}
		logger.info("Remove activityType : "+activityType + " | strURL : "+rActivity + " | status : "+status);
		return status;
	}
	
	public String postComments(int msid, String uuid, UserComment userComment, UserFeed userFeed){
		Date date = userComment.getCommentTime();
		String strDate = OfflineFeedUtilities.convertDateToStringForCache(date);
		String strURL = postCommetQueryString;
		strURL = strURL.replace("@articleID",  msid + "");
		strURL = strURL.replace("@fromaddress", userFeed.getFromaddress());
		strURL = strURL.replace("@fromname", userFeed.getFromname());
		strURL = strURL.replace("@location", userFeed.getLocation());
		strURL = strURL.replace("@message", userComment.getCommentText());
		strURL = strURL.replace("@msid", msid + "");
		strURL = strURL.replace("@parentid", userComment.getParentmsid()+"");
		strURL = strURL.replace("@commentdate", strDate);
		strURL = strURL.replace("@uuid", uuid);
		String postresult =  OfflineFeedUtilities.getExtDataForencode(postroUrl, strURL);
		logger.info(" strURL : "+postroUrl+"?"+strURL + " | Comment status : "+postresult);
		return postresult;
	}
	

	public List<UserFeed> getUserFeedList() throws Exception{	
		return offlineFeedDao.getUserFeedList();
	}
	
	public boolean updateStatus(UserFeed userFeed) throws Exception{
		return offlineFeedDao.updateStatus(userFeed);
		
		
	}

	public OfflineFeedDao getOfflineFeedDao() {
		return offlineFeedDao;
	}

	public void setOfflineFeedDao(OfflineFeedDao offlineFeedDao) {
		this.offlineFeedDao = offlineFeedDao;
	}

}