package com.times.common.service;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.constant.AmazonItemFields;
import com.times.common.constant.AmazonRequestFields;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.model.AmazonItem;
import com.times.common.model.AmazonSearchResponse;
import com.times.mailer.constant.GadgetConstant;


/**
 * The Class SimilarGadgetMatcherServiceImpl.
 * @author daksh.verma
 */
public class SimilarGadgetMatcherServiceImpl implements SimilarGadgetMatcherService  {
	
	/**  The Constant SIMILAR_COUNT  Number of similar names to be saved. */
	private static final int SIMILAR_COUNT=3;
	
	/** The amazon products API service. */
	private AmazonProductsAPIService amazonProductsAPIService;
	
	
	private static final Logger logger = LoggerFactory.getLogger(SimilarGadgetMatcherServiceImpl.class);

	/* 
	 * On the basis of productName, brandName and category, get related products from amazon search API
	 */
	@Override
	public DBObject getSimilarProductsFromAmazon(String productName, String brandName,String category) {
		
		DBObject similarFromAmazonSearch=getSimilarProductsFromAmazonHelper(productName, brandName, category);
		if(similarFromAmazonSearch!=null){
			return  similarFromAmazonSearch;
		}else{
			similarFromAmazonSearch=getSimilarProductsFromAmazonHelper(null, brandName, category);
			if(similarFromAmazonSearch!=null){
				similarFromAmazonSearch.put("brand_match", 1);
				return similarFromAmazonSearch;
			}
		}
		
		return null;
		
	}
	
	
	private  DBObject getSimilarProductsFromAmazonHelper(String productName, String brandName,String category) {
		
		AmazonSearchResponse searchResponse=null;
		
		Map<String,String> params= getQueryMap(productName, brandName, category);
		
		try {
			searchResponse = amazonProductsAPIService.getProductsFromAmazon(params);
		} catch (Exception e) {
			logger.error("Error while getting similar products from Amazon for name{} , brand{}, category{}",
					productName,brandName,category);
		}
		
		BasicDBList similarItemsList= new BasicDBList();
		DBObject finalResponse= null;
		
		if(searchResponse!=null && searchResponse.isRequestValid() && searchResponse.getErrorCode()==null){
			List<AmazonItem> items=searchResponse.getItemList();
			int maxCount=items.size();
			
			if(maxCount>SIMILAR_COUNT){
				maxCount=SIMILAR_COUNT;
			}
			
			for (int i=0;i<maxCount;++i){
				DBObject itemDBObject= getDBObjectFromAmazonItem(items.get(i));
				similarItemsList.add(itemDBObject);
			} 
			
			finalResponse=new BasicDBObject();
			finalResponse.put(AmazonItemFields.AMAZON_SIMILAR_FROM_SEARCH, similarItemsList);
		}else{
			logger.error("No products found for the search"+params);
		}
		
		return finalResponse;
	}

	
	
	/* 
	 * getExact name match by querying 
	 */
	@Override
	public DBObject getExactMatchFromAmazonDump(String productName, String brandName,String category) {
		/*
	    DBObject query= new BasicDBObject();
	    query.put("lcname",productName.toLowerCase());
	    DBObject product=MongoRestAPIService.get(COLL_NAME, query);
	    */
		
		
		return null;
	}
	
	
	/* 
	 * getSimilar products from amazon dump based on heuristic rules
	 */
	@Override
	public DBObject getSimilarProductsFromAmazonDumpRuleBased(String productName, String brandName,String category) {
		
		
		
		return null;
	}



	/* 
	 * accumulate all results :
	 * 	1) exact matches
	 * 	2) rule based matches
	 * 	3) amazon search api results
	 */
	@Override
	public DBObject getAccumulatedSimilarProducts(String productName, String brandName,String category) {
		DBObject similarProducts= new BasicDBObject();
		
		//Amazon Similar
		//Try to find amazon similar on the basis of brand,product and category
		//If not found try again without product name
		DBObject similarFromAmazonSearch=getSimilarProductsFromAmazon(productName, brandName, category);
		if(similarFromAmazonSearch!=null){
			similarProducts.putAll(similarFromAmazonSearch);
		}
		
		
		//Exact Match
		DBObject exactMatchFromDump=getExactMatchFromAmazonDump(productName, brandName, category);
		if(exactMatchFromDump!=null){
			similarProducts.putAll(similarFromAmazonSearch);
		}
		
		
		//Rule Based
		DBObject matchOnRuleBase=getSimilarProductsFromAmazonDumpRuleBased(productName, brandName, category);
		if(matchOnRuleBase!=null){
			similarProducts.putAll(matchOnRuleBase);
		}
		
		return similarProducts;
	}
	
	
	/*
	 * UtilityFunctions
	 */
	
	/**
	 * Gets the browse node for category.
	 *
	 * @param amazonItem the amazon item
	 * @return the browse node for category
	 */
	
	private DBObject getDBObjectFromAmazonItem(AmazonItem amazonItem){
		DBObject itemDBObject= new BasicDBObject();
		itemDBObject.put(AmazonItemFields.DB_NAME, amazonItem.getTitle());
		
		itemDBObject.put(AmazonItemFields.DB_ASIN, amazonItem.getASIN());
		
		itemDBObject.put(AmazonItemFields.DB_DETAIL_PAGE_URL, amazonItem.getDetailPageURL());
		
		DBObject lowestPriceDBObject=amazonItem.getLowestNewPriceDBObject();
		if(lowestPriceDBObject!=null){
			itemDBObject.put(AmazonItemFields.LOWEST_NEW_PRICE,lowestPriceDBObject);
		}
		
		itemDBObject.put(AmazonItemFields.DB_IS_PRIME, amazonItem.IsEligibleForPrime());
		
		DBObject listPriceDBObject=amazonItem.getListPriceDBObject();
		if(listPriceDBObject!=null){
			itemDBObject.put(AmazonItemFields.LISTPRICE,listPriceDBObject);
		}
		
		return itemDBObject;
	}
	
	
	/**
	 * @param productName
	 * @param brandName
	 * @param category
	 * @return return query map on the basis of parameters
	 */
	private Map<String,String> getQueryMap(String productName, String brandName,String category){
		Map<String,String> params= new HashMap<String, String>();
		params.put(AmazonRequestFields.SEARCH_INDEX, AmazonRequestFields.ELECTRONICS_SEARCH_INDEX);
		
		if(StringUtils.isNotBlank(productName)){
			params.put(AmazonRequestFields.KEYWORDS, productName.trim());
		}
		
		if(StringUtils.isNotBlank(brandName)){
			if(brandName.equalsIgnoreCase("Moto")){brandName="Motorola";}
			if(brandName.equalsIgnoreCase("Xiaomi")){brandName="Mi";}
			
			params.put(AmazonRequestFields.MANUFACTURER, brandName.trim());
		}
		
		if(StringUtils.isNotBlank(category) && StringUtils.isNotBlank(getBrowseNodeForCategory(category)) ){
			params.put(AmazonRequestFields.BROWSE_NODE, getBrowseNodeForCategory(category));
		}
		
		return params;
	}
	
	/**
	 * Gets the browse node for category. 
	 * Corresponding browse node id for category
	 * @param category the category
	 * @return the browse node for category
	 */
	private String getBrowseNodeForCategory(String category) {
		return GadgetConstant.Category.CATEGORY_TO_BROWSE_NODE_MAP.get(category);
	}	
	
	
	/**
	 * Are products same.
	 *
	 * @param product_91 the product 91
	 * @param product_amazon the product amazon
	 * @param color the color
	 * @return true, if successful
	 */
	private boolean areProductsSame(String product_91, String product_amazon, String color){
		return false;
	}
	
	/**
	 * Normalize amazon name.
	 *
	 * @param name the name
	 * @param color the color
	 * @return the string
	 */
	private  String normalizeAmazonName(String name,String color){
		
		if(StringUtils.isNotBlank(name)){
			name=name.toLowerCase().trim();
			name=removeSpecialCharacters(name);
		
		
			if (StringUtils.isNotBlank(color)) {
				color = color.toLowerCase().trim();
				name=name.replace(color, StringUtils.EMPTY);
			}
		
		}
		return name;
		
	}
	
	/**
	 * Normalize 91 name.
	 *
	 * @param name the name
	 * @return the string
	 */
	private String normalize91Name(String name){
		if(StringUtils.isNotBlank(name)){
			name=name.toLowerCase().trim();
			name=removeSpecialCharacters(name);
		}
		return name;
	}
	
	/**
	 * Removes the special characters.(Not space and Not words)
	 *
	 * @param name the name
	 * @return the string
	 */
	private String removeSpecialCharacters(String name){
		if(StringUtils.isNotBlank(name)){
			return name.replaceAll("[^a-zA-Z0-9\\s]",StringUtils.EMPTY);
		}else{
			return name;
		}
	}


	/**
	 * Gets the amazon products API service.
	 *
	 * @return the amazon products API service
	 */
	public AmazonProductsAPIService getAmazonProductsAPIService() {
		return amazonProductsAPIService;
	}


	/**
	 * Sets the amazon products API service.
	 *
	 * @param amazonProductsAPIService the new amazon products API service
	 */
	public void setAmazonProductsAPIService(AmazonProductsAPIService amazonProductsAPIService) {
		this.amazonProductsAPIService = amazonProductsAPIService;
	}
	

}
