package com.times.common.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FilenameUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriUtils;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.times.common.dao.EhcacheBatchDaoInterface;
import com.times.common.util.EhcacheBatchUtil;
import com.times.common.util.ExternalHttpUtil;


public class EhcacheBatchServiceImpl implements EhcacheBatchService {
	//private final static Logger logger = LoggerFactory.getLogger(EhcacheBatchServiceImpl.class);
	private static final Logger logger = LoggerFactory.getLogger("customlogger");
	String filePath = "/opt/svn/Staging/SpringBatchWeb/cityguidelist.properties";
	
	EhcacheBatchDaoInterface ehcacheBatchDaoInterface;
	public EhcacheBatchDaoInterface getEhcacheBatchDaoInterface() {
		return ehcacheBatchDaoInterface;
	}
	public void setEhcacheBatchDaoInterface(EhcacheBatchDaoInterface ehcacheBatchDaoInterface) {
		this.ehcacheBatchDaoInterface = ehcacheBatchDaoInterface;
	}
	
	public List<Integer> getListFromPropertiesFile() throws FileNotFoundException {
		File file = new File(filePath);
		List<Integer> msidList = new ArrayList<Integer>();

	    if(FilenameUtils.isExtension(file.getAbsolutePath(), "properties")){
	    	InputStream input= new FileInputStream(file.getAbsolutePath());
	    	Properties properties = new Properties();
	    	try {
				properties.load(input);
				for (String key : properties.stringPropertyNames()) {
					
					msidList.add(Integer.parseInt(key));
					logger.info("msid added from property file key={}",key);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
	    	
	    }
	    
	
		return msidList;
	}
	
	public String getJson(String msid, Map<String, List<Map<String,String>>> contentMap){
		List<Map<String,String>> contentList = new ArrayList();
		for (Entry<String, List<Map<String,String>>> lst : contentMap.entrySet()) {
			contentList.addAll(lst.getValue());
		}
		Map<String, Object> map= new HashMap();
		map.put("requestedMsid", msid);
		map.put("childrens", contentList);
		Gson gson = new Gson(); 
		String json = gson.toJson(map);
		return json;
	}
	
	public Element getXML(Map<String, Element> eleMap){
		Element xmlElement = new Element("mainnode");
		Element numFound = new Element("numFound");
		numFound.addContent(eleMap.size()+"");
		xmlElement.addContent(numFound);
		for (Entry<String, Element> entElement : eleMap.entrySet()) {
			xmlElement.addContent(entElement.getValue());
		}
		return xmlElement;
	}
	
	public List<Element> getElementList(Map<String, Element> eleMap){
		Collection<Element> coll = eleMap.values();
		return new ArrayList<Element>(coll);
	}
	
	public Element getXML(List<Element> lst, int pageno, int perpage){
		int size = lst.size();
		Element xmlElement = new Element("mainnode");
		Element numFound = new Element("numFound");
		numFound.addContent(size+"");
		xmlElement.addContent(numFound);
		
		int from = pageno;
		if(pageno > 1){
			from = (pageno-1)*perpage;
			Element prev = new Element("prev");
			prev.addContent((pageno-1)+"");
			xmlElement.addContent(prev);
		}else{
			from = 0;
		}
		int to = from + perpage;
		if(to >= size){
			to = size;
		}else{
			Element next = new Element("next");
			next.addContent((pageno+1)+"");
			xmlElement.addContent(next);
		}
		
		
		for(int i=from;i<to;i++){
			xmlElement.addContent(lst.get(i));
		}
		return xmlElement;
	}
	
	public Element getXMLPOI(Map<String, Element> eleMap){
		Element xmlElement = new Element("mainnode");
		for (Entry<String, Element> entElement : eleMap.entrySet()) {
			xmlElement.addContent(entElement.getValue());
		}
		return xmlElement;
	}
	
	public Map<String, Map<String, Element>> getDataXML(String msid){
		Map<String, Element> articleMap = new HashMap<String, Element>();
		Map<String, Element> poiMap = new HashMap<String, Element>();
		Map<String, Element> guideMap = new HashMap<String, Element>();
		Map<String, Element> slideshowMap = new HashMap<String, Element>();
		
		return getDataMapXML(msid, articleMap, poiMap, guideMap, slideshowMap);
	}
	
	public String getFileContent(Document doc){
		XMLOutputter outputter = new XMLOutputter();
		outputter.setFormat(Format.getPrettyFormat());
		String fileContent = outputter.outputString(doc);
		return fileContent;
	}
	
	public String getJsonContent(String str){
		int PRETTY_PRINT_INDENT_FACTOR = 4;
		JSONObject xmlJSONObj = XML.toJSONObject(str);
        String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
        return jsonPrettyPrintString;
	}
	
	public Map<String, Map<String, Element>> getDataMapXML(String msid,Map<String, Element> articleMap, Map<String, Element> poiMap, Map<String, Element> guideMap,Map<String, Element> slideshowMap ){
		boolean firstVisit = true;
		List<Map<String, Object>> lst = ehcacheBatchDaoInterface.getdbData(msid,"guide");
		getElement(lst, articleMap, poiMap, guideMap, slideshowMap, firstVisit);
		if(!articleMap.isEmpty()){
			articleMap=getArticleXML(articleMap);
			articleMap = getSeoPath(articleMap);
			articleMap = getMetaXML(articleMap);
			articleMap = getImageXML(articleMap);
		}
		if(!guideMap.isEmpty()){
			guideMap=getArticleXML(guideMap);
			guideMap = getSeoPath(guideMap);
			guideMap = getMetaXML(guideMap);
			guideMap = getImageXML(guideMap);
		}
		
		if(!poiMap.isEmpty()){
			poiMap=getArticleXML(poiMap);
			//poiMap = getSeoPath(poiMap);
			poiMap = getMetaXMLPOI(poiMap);
			//poiMap = getImageXML(poiMap);
		}
		
		/*if(!slideshowMap.isEmpty()){
			slideshowMap=getImageXML(slideshowMap);
		}*/
		
		Map<String, Map<String, Element>> dataMap = new HashMap<String, Map<String, Element>>();
		dataMap.put("articles", articleMap);
		dataMap.put("poi", poiMap);
		dataMap.put("guide", guideMap);
		dataMap.put("slideshow", slideshowMap);
		
		return dataMap;
	}
	

	public void getElement(List<Map<String, Object>> lst, Map<String, Element> articleMap, Map<String, Element> poiMap, Map<String, Element> guideMap,Map<String, Element> slideshowMap, boolean firstVisit ){
		if (lst != null && lst.size() > 0) {
			String msids = "";
			for (Map<String, Object> rs : lst) {
				
				List<Element> dataList = new ArrayList<Element>();
				String mstype = String.valueOf(rs.get("mstype"));
				String mssubtype = String.valueOf(rs.get("mssubtype"));
				String id = String.valueOf(rs.get("msid"));
				String refmsid = String.valueOf(rs.get("multipublishmsid"));
				String msname = String.valueOf(rs.get("msname"));

				Element root = new Element("Document");
				Element child1=new Element("contenttypeid");
				child1.addContent(mstype);
				Element child2=new Element("contentsubtypeid");
				child2.addContent(mssubtype);
				Element child3=new Element("stname");
				child3.addContent(msname);
				Element child3_1=new Element("stnameseo");
				child3_1.addContent(EhcacheBatchUtil.getSimpleString(msname));
				Element child4=new Element("msid");
				child4.addContent(id);
				root.addContent(child1);
				root.addContent(child2);
				root.addContent(child3);
				root.addContent(child3_1);
				root.addContent(child4);
				Element child5=new Element("thumb");
				if(String.valueOf(rs.get("photodatalenght"))!=null){
					child5.addContent(id);
					root.addContent(child5);
				}
				Element child6=new Element("refmsid");
				if(!"null".equals(refmsid)){
					child6.addContent(refmsid);
					root.addContent(child6);
				}
				dataList.add(root);
				
				
				if("2".equals(mstype) && "0".equals(mssubtype)){
					if(!"null".equals(refmsid)){
						articleMap.put(refmsid, root);
					}else{
						articleMap.put(id, root);
					}
				}else if("2".equals(mstype) && "8".equals(mssubtype)){
					if(!"null".equals(refmsid)){
						poiMap.put(refmsid, root);
					}else{
						poiMap.put(id, root);
					}
				}else if("2".equals(mstype) && "9".equals(mssubtype)){
					if(!"null".equals(refmsid)){
						guideMap.put(refmsid, root);
					}else{
						guideMap.put(id, root);
					}
					msids = msids+id+",";
				}else if("8".equals(mstype) && "1".equals(mssubtype)){
					if(!"null".equals(refmsid)){
						slideshowMap.put(refmsid, root);
					}else{
						slideshowMap.put(id, root);
					}
				}else if("43".equals(mstype) && "0".equals(mssubtype)){
					if(firstVisit || !msname.toLowerCase().contains("travel guide")){
						msids = msids+id+",";
						logger.trace(mstype+"---"+id +"---"+msname);
					}
				}
			if(("2".equals(mstype) && "9".equals(mssubtype) && !msname.toLowerCase().contains("travel guide"))){
				if(!"null".equals(refmsid)){
					msids = msids+refmsid+",";
					logger.trace(mstype+"---"+refmsid +"---"+msname);
				}else{
					msids = msids+id+",";
					logger.trace(mstype+"---"+id +"---"+msname);
				}
				
			}
			}
			firstVisit=false;
			if(!"".equals(msids)) {
				msids = msids.substring(0, msids.lastIndexOf(","));
				getElement(ehcacheBatchDaoInterface.getdbData(msids,"guide"), articleMap, poiMap, guideMap, slideshowMap, false);
			}
		}
	}
	
	public Map<String, Element> getArticleXML(Map<String, Element> articleMap){
		String msid = articleMap.keySet().toString();
		msid = msid.replace("[", "");
		msid = msid.replace("]", "");
		List<Map<String, Object>> lst = ehcacheBatchDaoInterface.getdbData(msid, "article");
		if(!lst.isEmpty()){
			for (Map<String, Object> rs : lst) {
				String id = String.valueOf(rs.get("msid"));
				String text = String.valueOf(rs.get("artntext"));
				Element mainNode = articleMap.get(id);
				Element child=new Element("text");
				Element child1=new Element("agencyid");
				Element child2=new Element("source");
				Element child3=new Element("authorid");
				Element child4=new Element("authorname");
				Element child5=new Element("authoremail");
				Element child6=new Element("art_date");
				if(text != null){
					child.addContent(text);
				}
				if(rs.get("agency_id") != null){
					child1.addContent(String.valueOf(rs.get("agency_id")));
					
					mainNode.addContent(child1);
					
				}
				if(rs.get("AGENCY_NAME") != null){
					
					child2.addContent(String.valueOf(rs.get("AGENCY_NAME")));
					
					mainNode.addContent(child2);
				}
				if(rs.get("auid") != null){
					child3.addContent(String.valueOf(rs.get("auid")));
					
					mainNode.addContent(child3);
					
				}
				if(rs.get("AUNAME") != null){
					
					child4.addContent(String.valueOf(rs.get("AUNAME")));
					
					mainNode.addContent(child4);
				}
				if(rs.get("AUEmail") != null){
					child5.addContent(String.valueOf(rs.get("AUEmail")));
					mainNode.addContent(child5);
				}
				if(rs.get("art_date") != null){
					child6.addContent(String.valueOf(rs.get("art_date")));
					mainNode.addContent(child6);
				}
				Element child7 = new Element("refstname");
				Element child8 = new Element("refstnameseo");
				if(mainNode.getChild("refmsid") != null){
					child7.addContent(String.valueOf(rs.get("art_title")));
					mainNode.addContent(child7);
					child8.addContent(EhcacheBatchUtil.getSimpleString(String.valueOf(rs.get("art_title"))));
					mainNode.addContent(child8);
				}
				mainNode.addContent(child);
			}
		}
		return articleMap;
		
	}
		
	public Map<String, Element> getImageXML(Map<String, Element> elementMap){
		String msid = elementMap.keySet().toString();
		msid = msid.replace("[", "");
		msid = msid.replace("]", "");
		
		List<Map<String, Object>> lst = ehcacheBatchDaoInterface.getdbData(msid, "image");
		
		if(!lst.isEmpty()){
			Map<String, Element> imgMap = new HashMap<String, Element>();
			Map<String, String> slideidMsids = new HashMap<String, String>();
			Element imageNode = null;
			for (Map<String, Object> rs : lst) {	
				String id = String.valueOf(rs.get("msid"));
				String slideid = String.valueOf(rs.get("slideid"));
				slideidMsids.put(slideid, id);
				
				imageNode = new Element("image");
				Element child=new Element("imageid");
				child.addContent(String.valueOf(rs.get("slideid")));
				imageNode.addContent(child);
				
				Element child1=new Element("agencyid");
				Element child2=new Element("source");
				if(rs.get("agency_id") != null){
					child1.addContent(String.valueOf(rs.get("agency_id")));
					imageNode.addContent(child1);
				}
				if(rs.get("AGENCY_NAME") != null){
					child2.addContent(String.valueOf(rs.get("AGENCY_NAME")));
					imageNode.addContent(child2);
				}
				imgMap.put(slideid, imageNode);
			}	
			
			imgMap = getMetaXML(imgMap);
			
			for (Entry<String, String> slideidMsid : slideidMsids.entrySet()) {
				Element mainNode =elementMap.get(slideidMsid.getValue());
				mainNode.addContent(imgMap.get(slideidMsid.getKey()).detach());
			}
			
			
		}
		return elementMap;
		
	}

	public Map<String, Element> getAgencyXML(Map<String, Element> elementMap){		
		Map<String,String> MsidagencyMap = new HashMap<String, String>();
		Map<String,Element> agencyMap = new HashMap<String, Element>();
		for (Entry<String, Element> msId : elementMap.entrySet()) { 
			Element docElement = msId.getValue();
			String agencyId = docElement.getChildText("agencyid");
			if(agencyId != null){
				MsidagencyMap.put(msId.getKey(), docElement.getChildText("agencyid"));
			}
		}
		HashSet<String> agencySet = new HashSet<String>();
		agencySet.addAll(MsidagencyMap.values());
		String msid = agencySet.toString();
		msid = msid.replace("[", "");
		msid = msid.replace("]", "");
		List<Map<String, Object>> lst = ehcacheBatchDaoInterface.getdbData(msid, "agency");
		if(!lst.isEmpty()){
			for (Entry<String, String> msId : MsidagencyMap.entrySet()) {
				for (Map<String, Object> rs : lst) {
					if (rs.get("adrulesxml") != null && !"".equals(rs.get("adrulesxml").toString().trim()) && msId.getValue().equals(String.valueOf(rs.get("agencyid")))) {
						try {
							SAXBuilder builder = new SAXBuilder();
							Document doc = builder.build(new StringReader(String.valueOf(rs.get("adrulesxml"))));
							Element adrulesxml = doc.detachRootElement();
							Element agencyname = new Element("agencyname");
							agencyname.addContent(String.valueOf(rs.get("agencyname")));
							adrulesxml.addContent(agencyname.detach());
							agencyMap.put(msId.getKey(),adrulesxml);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		for (Entry<String, String> entry : MsidagencyMap.entrySet()) {
			try{
				Element elem = agencyMap.get(entry.getKey());
				if(elem !=null){
					elementMap.get(entry.getKey()).addContent(agencyMap.get(entry.getKey()));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return elementMap;
	}

	public Map<String, Element> getAgencyXML(Map<String, Element> elementMap, String agencyids){		
		Map<String,Element> agencyMap = new HashMap<String, Element>();
		List<Map<String, Object>> lst = ehcacheBatchDaoInterface.getdbData(agencyids, "agency");
		if(!lst.isEmpty()){
			for (Map<String, Object> rs : lst) {
				if (rs.get("adrulesxml") != null && !"".equals(rs.get("adrulesxml").toString().trim())) {
					try {
						SAXBuilder builder = new SAXBuilder();
						Document doc = builder.build(new StringReader(String.valueOf(rs.get("adrulesxml"))));
						Element adrulesxml = doc.detachRootElement();
						Element agencyname = new Element("agencyname");
						agencyname.addContent(String.valueOf(rs.get("agencyname")));
						adrulesxml.addContent(agencyname.detach());
						agencyMap.put(String.valueOf(rs.get("agencyid")),adrulesxml);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		for (Entry<String, Element> msId : elementMap.entrySet()) { 
			
			Element docElement = elementMap.get(msId.getKey());
			Document doc = new Document(docElement);
			XPathFactory xFactory = XPathFactory.instance();
			XPathExpression<Element> expression = xFactory.compile("group/Document", Filters.element());
			List<Element> pathElements = expression.evaluate(doc);
			for(Element aElement : pathElements){
				String agencyid=aElement.getChildText("agencyid");
				if(agencyid != null){
					Element agnElement = agencyMap.get(agencyid);
					aElement.addContent(agnElement.detach());
				}
			}
			elementMap.put(msId.getKey(), doc.detachRootElement());
		}
		return elementMap;
		
	}


	   public Map<String, Element> getMetaXML(Map<String, Element> elementMap){
			String msid = elementMap.keySet().toString();
			msid = msid.replace("[", "");
			msid = msid.replace("]", "");		
			List<Map<String, Object>> lst = ehcacheBatchDaoInterface.getdbData(msid, "metadata");
			if(!lst.isEmpty()){
				for (Map<String, Object> rs : lst) {
					String id = String.valueOf(rs.get("msid"));
					String desc = String.valueOf(rs.get("mtmdesc"));
					desc = desc.replace(" ", "_");
					Element mainNode =elementMap.get(id);
					Element metaNode = new Element("mData_"+desc);
					String minfo="";
					int metatype = Integer.parseInt(String.valueOf(rs.get("metatype")));
					int mtmdatatype = Integer.parseInt(String.valueOf(rs.get("mtmdatatype")));
					if (rs.get("omdntext") != null) {
						minfo=String.valueOf(rs.get("omdntext"));
					} else if (mtmdatatype == 0) {
						minfo=String.valueOf(rs.get("OMDChar"));
					} else if (mtmdatatype == 2) {
						minfo=String.valueOf(rs.get("OMDNumber"));
					} else if (mtmdatatype == 3) {
						minfo=String.valueOf(rs.get("OMDDateTime"));
					} else{
						minfo="";
					}
					metaNode.addContent(minfo);				
					mainNode.addContent(metaNode);
				}
			}
			return elementMap;
		}

	   public Map<String, Element> getSeoPath(Map<String, Element> elementMap){
		   String msid = elementMap.keySet().toString();
			msid = msid.replace("[", "(");
			msid = msid.replace("]", ")");
			msid = msid.replace(",", " OR ");
		   String SolrUrl = "http://solrsearch.indiatimes.com/TOISolrWebProject/travel/select?"
		   		+ "q=msid:"+msid+"&"
		   		+ "start=0&"
		   		+ "fq=hostid:259 AND status:2&"
		   		+ "rows=10&"
		   		+ "fl=seopath, msid&wt=xslt&tr=custom.xsl";
		   try {
			String extdata = ExternalHttpUtil.getExtDataForSolar(SolrUrl);
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(new StringReader(extdata));
			
			XPathFactory xFactory = XPathFactory.instance();
			XPathExpression<Element> expression = xFactory.compile("SolrResponse/Documents/Document", Filters.element());
			List<Element> pathElements = expression.evaluate(doc);
			Element mainNode = null;
			for(Element docElement : pathElements){
				mainNode = elementMap.get(docElement.getChildText("msid"));
				Element childNode = new Element("seopath");
				childNode.addContent(docElement.getChildText("seopath"));
				mainNode.addContent(childNode);
			}   
		   } catch (Exception e) {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   } 
		   return elementMap;
	   }
	   
	   public Map<String, Element> getMetaXMLPOI(Map<String, Element> elementMap){
			String msid = elementMap.keySet().toString();
			msid = msid.replace("[", "");
			msid = msid.replace("]", "");
			List<Map<String, Object>> lst = ehcacheBatchDaoInterface.getdbData(msid, "metadata");
			Map<String, String> metaMsid = new HashMap<String, String>();
			Map<String, Element> groupelementMap = new HashMap<String, Element>();
			Map<String, Element> imageMap = new HashMap<String, Element>();
			
			if(!lst.isEmpty()){
				for (Map<String, Object> rs : lst) {
					String minfo="";
					String id = String.valueOf(rs.get("msid"));
					int mtmdatatype = Integer.parseInt(String.valueOf(rs.get("mtmdatatype")));
					if (rs.get("omdntext") != null) {
						minfo=String.valueOf(rs.get("omdntext"));
					} else if (mtmdatatype == 0) {
						minfo=String.valueOf(rs.get("OMDChar"));
					} else if (mtmdatatype == 2) {
						minfo=String.valueOf(rs.get("OMDNumber"));
					} else if (mtmdatatype == 3) {
						minfo=String.valueOf(rs.get("OMDDateTime"));
					} else{
						minfo="";
					}
					int metatype = Integer.parseInt(String.valueOf(rs.get("metatype")));
					if(metatype==319){ // && minfo.equals("24/7, New Delhi")
						if(metaMsid.containsKey(minfo)){
							String value = metaMsid.get(minfo);
							value = value +"," +id;
							metaMsid.put(minfo, value);
						}else{
							metaMsid.put(minfo, String.valueOf(id));
							groupelementMap.put(minfo, new Element("group"));
						}
						
					}
				}
				HashSet<String> agencySet = new HashSet<String>();
				
				if(!metaMsid.isEmpty()){
					for (Entry<String, String> meta : metaMsid.entrySet()) {
						Element group = groupelementMap.get(meta.getKey());
						Element condition = new Element("condition");
						condition.addContent("mData_POIPrimaryName:" + meta.getKey());
						group.addContent(condition);
						// http://solrsearch.indiatimes.com/TOISolrWebProject/travel/select?defType=dismax&qf=mData_POIPrimaryName&start=0&fq=hostid:259%20AND%20contenttypeid:2%20AND%20contentsubtypeid:8%20AND%20status:2&sort=effectivedate%20asc%20,%20msid%20asc&rows=20&wt=xslt&tr=custom.xsl&q=%22Wok%20in%20the%20Clouds,%20Delhi%22
						try {
							//192.168.34.142,							192.168.41.117
						String encodedmeta = meta.getKey().replace("&", "&amp;");
						String SolrUrl = "http://solrsearch.indiatimes.com/TOISolrWebProject/travel/select?" + "q=\""
								+ encodedmeta + "\"&" + "start=0&fl=name,msid,agencyid,author*,source,stnameseo,seopath,text,content*,hasThumb&" //,mData_*
								+ "fq=hostid:259 AND contenttypeid:2 AND contentsubtypeid:8 AND multiPublishMaster:1 AND status:2&" + "rows=10&"
								+ "defType=dismax&qf=mData_POIPrimaryName&wt=xslt&tr=custom.xsl&sort=effectivedate asc, msid asc";
						//SolrUrl = SolrUrl.replace("solrsearch.indiatimes.com", "192.168.41.117");
							String extdata = ExternalHttpUtil.getExtDataForSolr(SolrUrl);
							SAXBuilder builder = new SAXBuilder();
							Document doc = builder.build(new StringReader(extdata));
							XPathFactory xFactory = XPathFactory.instance();
							
							XPathExpression<Element> expressionnumber = xFactory.compile("SolrResponse/Header",Filters.element());
							Element numFound = new Element("numFound");
							List<Element> num = expressionnumber.evaluate(doc);
							for (Element nm : num) {
								numFound.addContent(nm.getChildText("numFound"));
							}
							group.addContent(numFound);
							XPathExpression<Element> expression = xFactory.compile("SolrResponse/Documents/Document",Filters.element());
							List<Element> alldocument = expression.evaluate(doc);
							
							for (Element document : alldocument) {
								imageMap.put(document.getChildText("msid"), document);
								agencySet.add(document.getChildText("agencyid"));
								group.addContent(document.detach());
							}
						} catch (Exception e) {}
					}
				}
				
				Element group1 = new Element("group");
				Element condition1 = new Element("condition");
				condition1.addContent("mData_POIPrimaryName:");
				group1.addContent(condition1);
				int numcount = 0;
				
				for (Entry<String, Element> poiId : elementMap.entrySet()) {
					if(imageMap.get(poiId.getKey()) == null){
						numcount = numcount+1;
						imageMap.put(poiId.getKey(), poiId.getValue());
						agencySet.add(poiId.getValue().getChildText("agencyid"));
						group1.addContent(poiId.getValue());
					}
				}
				imageMap = getImageXML(imageMap);
				imageMap = getAgencyXML(imageMap);
				imageMap = getMetaXML(imageMap);
				Element numFound1 = new Element("numFound");
				numFound1.addContent(String.valueOf(numcount));
				group1.addContent(numFound1);
				groupelementMap.put("none", group1);
			}
			
			
			metaMsid = null;
			imageMap =null;
			return groupelementMap;
		}

	   public Map<String, List<Map<String,String>>> getDataJson(String msid){
		   Map<String, List<Map<String,String>>> allMap = new HashMap<String, List<Map<String,String>>>();
		   return getDataMapJson(msid, allMap);
	   }
	   
		public Map<String, List<Map<String,String>>> getDataMapJson(String msid,Map<String, List<Map<String,String>>> allMap){
			boolean firstVisit = true;
			List<Map<String, Object>> lst = ehcacheBatchDaoInterface.getdbData(msid,"guide");
			getMap(lst, allMap);
			System.out.println("getMap");
			
			if(!allMap.isEmpty()){
				allMap=getImage(allMap);
				System.out.println("getImage");
			}
			
			
			
			return allMap;
		}
		
		
		public void getMap(List<Map<String, Object>> lst, Map<String, List<Map<String,String>>> allMap){
			System.out.println(lst.size());
			if (lst != null && lst.size() > 0) {
				String msids = "";
				String refmsidstr="";
				for (Map<String, Object> rs : lst) {
					List<Map<String,String>> dataList = new ArrayList<Map<String,String>>();
					String mstype = String.valueOf(rs.get("mstype"));
					String mssubtype = String.valueOf(rs.get("mssubtype"));
					String id = String.valueOf(rs.get("msid"));
					String refmsid = String.valueOf(rs.get("multipublishmsid"));
					String msname = String.valueOf(rs.get("msname"));
					Map<String,String> map = new HashMap<String,String>();
					map.put("msid", id);
					map.put("mstype", mstype);
					map.put("mssubtype", mssubtype);
					map.put("title", String.valueOf(rs.get("msname")));
					map.put("parentId", String.valueOf(rs.get("mas_msid")));
					map.put("hostId", String.valueOf(rs.get("hostid")));
					map.put("hisActive", String.valueOf(rs.get("hisactive")));
					if(!"null".equals(refmsid)){
						map.put("refMsid", refmsid);
						refmsidstr = refmsidstr+","+refmsid;
					}
					dataList.add(map);
					
					if(!"null".equals(refmsid)){
						allMap.put(refmsid, dataList);
					}else{
						allMap.put(id, dataList);
					}
					//allMap.put(id, dataList);
					 if("43".equals(mstype) && "0".equals(mssubtype)){
						 msids = msids+id+",";
							
					}
					 logger.info(mstype+"---"+id +"---"+msname);
				if("2".equals(mstype) && "9".equals(mssubtype)){
					if(!"null".equals(refmsid)){
						msids = msids+refmsid+",";
						logger.info(mstype+"---"+refmsid +"---"+msname);
					}else{
						msids = msids+id+",";
						logger.info(mstype+"---"+id +"---"+msname);
					}
					
				}
				}
				if(!"".equals(refmsidstr)) {
					allMap.putAll(getRefMsidInfo(refmsidstr));
				}
				
				if(!"".equals(msids)) {
					msids = msids.substring(0, msids.lastIndexOf(","));
					System.out.println(msids);
					getMap(ehcacheBatchDaoInterface.getdbData(msids,"guide"), allMap);
				}
			}
		}
		

		public Map<String, List<Map<String,String>>> getImage(Map<String, List<Map<String,String>>> allMap){
			String msid = allMap.keySet().toString();
			msid = msid.replace("[", "");
			msid = msid.replace("]", "");
			List<Map<String, Object>> lst = ehcacheBatchDaoInterface.getdbData(msid, "artimage");
			if(!lst.isEmpty()){
				for (Map<String, Object> rs : lst) {
					String mas_msid = String.valueOf(rs.get("msid"));
					String id = String.valueOf(rs.get("slideid"));
					List<Map<String,String>> dataList = new ArrayList<Map<String,String>>();
					String mstype = "3";
					String mssubtype = "0";
					String msname = String.valueOf(rs.get("msname"));
					Map<String,String> map = new HashMap<String,String>();
					map.put("msid", id);
					map.put("mstype", mstype);
					map.put("mssubtype", mssubtype);
					map.put("title", String.valueOf(rs.get("hname")));
					map.put("parentId", mas_msid);
					map.put("hostId", "259");
					map.put("hisActive", String.valueOf(rs.get("hisactive")));
					
					
					dataList.add(map);
					
					allMap.put(id, dataList);
				}
			}
			return allMap;
			
		}
	   
		public void getRefMsidInfo(String msids,Map<String, List<Map<String,String>>> allMap){
			
			List<Map<String, Object>> lst = ehcacheBatchDaoInterface.getdbData(msids.substring(1), "refmsid");
			if(!lst.isEmpty()){
				for (Map<String, Object> rs : lst) {
					List<Map<String,String>> dataList = new ArrayList<Map<String,String>>();
					String mstype = String.valueOf(rs.get("mstype"));
					String mssubtype = String.valueOf(rs.get("mssubtype"));
					String id = String.valueOf(rs.get("msid"));
					String msname = String.valueOf(rs.get("msname"));
					Map<String,String> map = new HashMap<String,String>();
					map.put("msid", id);
					map.put("mstype", mstype);
					map.put("mssubtype", mssubtype);
					map.put("title", msname);
					map.put("parentId", String.valueOf(rs.get("mas_msid")));
					map.put("hostId", String.valueOf(rs.get("hostid")));
					map.put("hisActive", String.valueOf(rs.get("hisactive")));
					
					dataList.add(map);
					
					allMap.put(id, dataList);
				}
			}
			//return allMap;
			
		}
		
		public Map<String, List<Map<String,String>>> getRefMsidInfo(String msids){
			Map<String, List<Map<String,String>>> allMap = new HashMap<String, List<Map<String,String>>>();
			List<Map<String, Object>> lst = ehcacheBatchDaoInterface.getdbData(msids.substring(1), "refmsid");
			if(!lst.isEmpty()){
				for (Map<String, Object> rs : lst) {
					List<Map<String,String>> dataList = new ArrayList<Map<String,String>>();
					String mstype = String.valueOf(rs.get("mstype"));
					String mssubtype = String.valueOf(rs.get("mssubtype"));
					String id = String.valueOf(rs.get("msid"));
					String msname = String.valueOf(rs.get("msname"));
					Map<String,String> map = new HashMap<String,String>();
					map.put("msid", id);
					map.put("mstype", mstype);
					map.put("mssubtype", mssubtype);
					map.put("title", msname);
					map.put("parentId", String.valueOf(rs.get("mas_msid")));
					map.put("hostId", String.valueOf(rs.get("hostid")));
					map.put("hisActive", String.valueOf(rs.get("hisactive")));
					
					dataList.add(map);
					
					allMap.put(id, dataList);
				}
			}
			return allMap;
			
		}
	   

}
