package com.times.common.service.upload.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.Permission;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.times.common.service.FileContentServices;
import com.times.common.service.FileContentServicesImpl;
import com.times.common.service.upload.IUploadService;
import com.times.mailer.model.AmazonS3Credits;
import com.times.mailer.model.FileLocationInfo;

public class S3UploadService implements IUploadService {

	private static final Log log = LogFactory.getLog(S3UploadService.class);

	private static final String FROM_BATCH = "FromBatch";

	public boolean upload(FileLocationInfo location, String content) {

		return uploadFileAmazonS3(location.getAmazonS3(), content, null);
	}

	private boolean uploadFileAmazonS3(AmazonS3Credits credits, String str, Map headerDetails) {
		String fileName = "";
		try {
			AWSCredentials awsCredentials = new BasicAWSCredentials(credits.getAccessKey(), credits.getSecretKey());
			TransferManager tm = new TransferManager(awsCredentials);
			// data converted from String to byteArray
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Writer writer = new OutputStreamWriter(baos);
			writer.write(str);
			writer.flush();
			writer.close();

			byte[] zippedBytes;

			// Gzip compression from databyteArray using GZIPOutputStream
			try {
				ByteArrayOutputStream localbaos = new ByteArrayOutputStream();
				GZIPOutputStream out = new GZIPOutputStream(localbaos);
				out.write(baos.toByteArray(), 0, baos.toByteArray().length);
				// Complete the GZIP file
				out.finish();
				out.close();
				zippedBytes = localbaos.toByteArray();
			} catch (IOException e) {
			     log.error("AmazonClientException caught while compressing the content to S3", e);
				throw e;
			}

			InputStream inputStream = new ByteArrayInputStream(zippedBytes);
			int len = zippedBytes.length;
			ObjectMetadata meta = new ObjectMetadata();

			meta.setContentLength(len);
			meta.setContentType("text/html;charset=UTF-8");
			// adding gzip content coding in header
			meta.setContentEncoding("gzip");
			if (headerDetails != null && headerDetails.size() > 0) {
				Iterator<String> it = headerDetails.keySet().iterator();
				while (it.hasNext()) {
					String key = (String) it.next();
					if (!key.equalsIgnoreCase("Transfer-encoding")) {
						meta.setHeader(key, headerDetails.get(key));
					}
				}
				// below header for identify page is served from batch job.
				meta.setHeader(FROM_BATCH, "true");
			}
			
			AccessControlList acl = new AccessControlList();
			acl.grantPermission(GroupGrantee.AllUsers, Permission.Read);
			AmazonS3 s3client = tm.getAmazonS3Client();
			s3client.putObject(new PutObjectRequest(credits.getBucketName(), credits.getFileName() , inputStream, meta).withAccessControlList(acl));
			
			log.debug(credits.getFileName());
			return true;
		} catch (AmazonClientException e) {
			log.error("AmazonClientException caught while uploading the content to S3", e);
		} catch (IllegalArgumentException e) {
			log.error("Incorrect argument exception caught while uploading the content to S3", e);
		} catch (IOException e) {
			log.error("IO exception caught while uploading the content to S3", e);
		}
		
		return false;
	}

}
