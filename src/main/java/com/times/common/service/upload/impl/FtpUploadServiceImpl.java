package com.times.common.service.upload.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.http.conn.ConnectTimeoutException;

import com.times.common.service.upload.IUploadService;
import com.times.common.util.NetstorageUtil;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.model.FileLocationInfo;
import com.times.mailer.model.NetstorageInfo;

public class FtpUploadServiceImpl implements IUploadService {

	private static final Log log = LogFactory.getLog(FtpUploadServiceImpl.class);

	public boolean upload(FileLocationInfo location, String content) {
		// System.out.println(fileContent);
		// String server = "timescricket.upload.akamai.com";
		int port = 21;
		// String pass = "user4upl0ad";
		InputStream inputStream = null;
		FTPClient ftpClient = new FTPClient();

		NetstorageInfo storageInfo = location.getNetstorageLocation();

		try {
			ftpClient.setControlEncoding("UTF-8");
			ftpClient.setConnectTimeout(50000);
			// ftpClient.setSendBufferSize(4096);
			ftpClient.connect(storageInfo.getServerName(), port);

			if (!ftpClient.isConnected()) {
				ftpClient.connect(storageInfo.getServerName(), port);
			}

			// boolean conStatus = ftpClient.login(storageInfo.getUserid(),
			// storageInfo.getPassword());
			boolean conStatus = ftpClient.login(storageInfo.getUserid().trim(), storageInfo.getPassword());
			if (conStatus) {

				ftpClient.changeWorkingDirectory(storageInfo.getFolderLocation());
				ftpClient.enterLocalPassiveMode();
				// log.debug("buffer size: " +ftpClient.getBufferSize());
				ftpClient.setBufferSize(51200); // 1024
				ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

				inputStream = new ByteArrayInputStream(content.getBytes("UTF-8"));
				// log.debug(" file size : " + fileContent.length());
				boolean done = ftpClient.storeFile(storageInfo.getFileName(), inputStream);
				inputStream.close();
				if (done) {
					log.debug(" file size : " + content.length() + " uploaded.");
				}

				// InputStream inputStream = new FileInputStream(myFile);
				/*
				 * OutputStream outputStream =
				 * ftpClient.storeFileStream(storageInfo.getFileName()); byte[]
				 * bytesIn = new byte[4096]; int read = 0;
				 * 
				 * while((read = inputStream.read(bytesIn)) != -1) {
				 * outputStream.write(bytesIn, 0, read); }
				 * 
				 * if (inputStream != null) { inputStream.close(); }
				 */

				// to complete the transaction
				/*
				 * boolean completed = ftpClient.completePendingCommand(); if
				 * (completed) { log.debug("file is uploaded successfully."); }
				 */
			} else {
				log.error("connection not found for URL : " + storageInfo.getServerName()
						+ storageInfo.getFolderLocation());
				new CMSCallExceptions(
						"connection not found for URL : " + storageInfo.getServerName()
								+ storageInfo.getFolderLocation(),
						CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, new Exception("connection not found"));
			}
		} catch (ConnectTimeoutException e) {
			log.error("ConnectTimeoutException caught while connecting , url=" + storageInfo.getServerName()
					+ storageInfo.getFolderLocation() + " , msg=" + e.getMessage());
			new CMSCallExceptions(
					"ConnectTimeoutException exception caught while FTP upload, url " + storageInfo.getServerName()
							+ storageInfo.getFolderLocation() + storageInfo.getFileName() + " msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		} catch (ConnectException e) {
			log.error("ConnectException caught while connecting , url=" + " , msg=" + e.getMessage());
			new CMSCallExceptions(
					"ConnectException exception caught while FTP upload, url " + storageInfo.getServerName()
							+ storageInfo.getFolderLocation() + storageInfo.getFileName() + " msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		} catch (IOException e) {
			log.error("IOException caught while connecting , url=" + " , msg=" + e.getMessage());
			new CMSCallExceptions(
					"IOException exception caught while FTP upload, url " + storageInfo.getServerName()
							+ storageInfo.getFolderLocation() + storageInfo.getFileName() + " msg : " + e.getMessage(),
					CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		} finally {
			NetstorageUtil.disconnect(ftpClient);
		}
		return false;
	}

}
