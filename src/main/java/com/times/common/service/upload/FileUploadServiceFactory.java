package com.times.common.service.upload;

import com.times.common.service.upload.impl.FtpUploadServiceImpl;
import com.times.common.service.upload.impl.S3UploadService;
import com.times.mailer.model.FileLocationInfo.LocationType;

public class FileUploadServiceFactory {

	private static final IUploadService FTP_SERVICE = new FtpUploadServiceImpl();
	private static final IUploadService S3_SERVICE = new S3UploadService();

	public static IUploadService getUploadServiceByLocation(LocationType location) {
		switch (location) {
		case NET_STORAGE:
			return FTP_SERVICE;
		case AWS:
			return S3_SERVICE;
		default:
			return null;
		}
	}

}
