package com.times.common.service.upload;

import com.times.mailer.model.FileLocationInfo;

public interface IUploadService {

	boolean upload(FileLocationInfo location, String content);

}
