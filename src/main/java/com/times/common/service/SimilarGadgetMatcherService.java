package com.times.common.service;

import org.json.JSONObject;

import com.mongodb.DBObject;
import com.times.common.model.AmazonSearchResponse;

public interface SimilarGadgetMatcherService {
	
	public DBObject getSimilarProductsFromAmazon(String productName, String brandName,String category);
	
	public DBObject getExactMatchFromAmazonDump(String productName, String brandName,String category);
	
	public DBObject getSimilarProductsFromAmazonDumpRuleBased(String productName, String brandName,String category);
	
	public DBObject getAccumulatedSimilarProducts(String productName, String brandName,String category);
	
	
	
}
