package com.times.common.service;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;

public interface EhcacheBatchService {
	public List<Integer> getListFromPropertiesFile() throws FileNotFoundException;
	/*public Map<String, Map> getData(String msid);
	public String getData1(String msid);*/
	public String getJson(String msid, Map<String, List<Map<String,String>>> contentMap);
	public Map<String, Map<String, Element>> getDataXML(String msid);
	public Map<String, List<Map<String,String>>> getDataJson(String msid);
	public Element getXML(Map<String, Element> eleMap);
	public List<Element> getElementList(Map<String, Element> eleMap);
	public Element getXML(List<Element> lst, int pageno, int perpage);
	public Element getXMLPOI(Map<String, Element> eleMap);
	public String getFileContent(Document doc);
	public String getJsonContent(String str);
	public Map<String, Map<String, Element>> getDataMapXML(String msid,Map<String, Element> articleMap, Map<String, Element> poiMap, Map<String, Element> guideMap,Map<String, Element> slideshowMap );
	
}
