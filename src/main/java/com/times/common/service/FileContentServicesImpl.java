/**
 * 
 */
package com.times.common.service;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.springframework.web.util.UriUtils;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.Permission;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.times.common.util.NetstorageUtil;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;
import com.times.mailer.model.AmazonS3Credits;
import com.times.mailer.model.ImageInfo;
import com.times.mailer.model.NetstorageInfo;

/**
 * This service class is used to pull the content by using apache httpConnection and 
 * upload the pulled content into specified FTP conention. Source location and traget location is mentioned
 * in the Domain model object.
 * 
 * @author Ranjeet.Jha
 *
 */
public class FileContentServicesImpl implements FileContentServices {

	private static final Log log = LogFactory.getLog(FileContentServicesImpl.class);
		
	//private HttpClientParams clientParams = new HttpClientParams();
	
	private static final int httpTimeout = 10000; //10 sec
	private static final int httpConnectionTimeout = 3000;
	private static final String FROM_BATCH = "FromBatch";
	
	
	
	/**
	 * This method invoked by init-method hook by spring container.
	 */
	public void init() {
		/*clientParams.setParameter(HttpClientParams.MAX_REDIRECTS, "5");
		clientParams.setParameter(HttpClientParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(5, false));
		clientParams.setConnectionManagerTimeout(httpConnectionTimeout);		
		clientParams.setSoTimeout(httpTimeout);*/
	}
	
	
	@Override
	public boolean uploadContentByFTP(NetstorageInfo ftpDtls, String fileContent) throws Exception {
		//System.out.println(fileContent);
		//String server = "timescricket.upload.akamai.com";
		long startTime = System.currentTimeMillis();
        int port = 21;
        //String pass = "user4upl0ad";
        InputStream inputStream = null;
        FTPClient ftpClient = new FTPClient();
       
        try {
        	ftpClient.setControlEncoding("UTF-8");
        	ftpClient.setConnectTimeout(50000);
        	//ftpClient.setSendBufferSize(4096);
            ftpClient.connect(ftpDtls.getServerName(), port);
            
            if (!ftpClient.isConnected()) {
            	ftpClient.connect(ftpDtls.getServerName(), port);
            }
            
           //boolean conStatus = ftpClient.login(ftpDtls.getUserid(), ftpDtls.getPassword());
           boolean conStatus = ftpClient.login(ftpDtls.getUserid().trim(), ftpDtls.getPassword());
           if (conStatus) {
	           
	            ftpClient.changeWorkingDirectory(ftpDtls.getFolderLocation());
	            ftpClient.enterLocalPassiveMode();
	            //log.debug("buffer size: " +ftpClient.getBufferSize());
	            ftpClient.setBufferSize(51200); // 1024
	            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
	 
	            inputStream = new ByteArrayInputStream(fileContent.getBytes("UTF-8"));
	            //log.debug(" file size : " + fileContent.length());
	            boolean done = ftpClient.storeFile(ftpDtls.getFileName(), inputStream);
	            inputStream.close();
	            if (done) {
	            	log.debug(" file size : " + fileContent.length() + " uploaded");
	            }
	            log.debug("upload time in ms : " + (System.currentTimeMillis() - startTime) + " of fileContent " + 
	            		ftpDtls.getServerName() + ftpDtls.getFolderLocation() + ftpDtls.getFileName());
	           //InputStream inputStream = new FileInputStream(myFile);
	           /*OutputStream outputStream = ftpClient.storeFileStream(ftpDtls.getFileName());
	           byte[] bytesIn = new byte[4096];
	           int read = 0;

	           while((read = inputStream.read(bytesIn)) != -1) {
	               outputStream.write(bytesIn, 0, read);
	           }
	           
	           if (inputStream != null) {
	        	   inputStream.close();
	           }*/
	           
	            // to complete the transaction
	           /* boolean completed = ftpClient.completePendingCommand();
	            if (completed) {
	               log.debug("file is uploaded successfully.");
	            }*/
           } else {
        	   log.error("connection not found for URL : " + ftpDtls.getServerName() + " folder location : " + ftpDtls.getFolderLocation());
        	   new CMSCallExceptions("connection not found for URL : " + ftpDtls.getServerName() + ftpDtls.getFolderLocation(),	CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, new Exception("connection not found"));
           }
        } catch (ConnectTimeoutException e) {
			log.error("ConnectTimeoutException caught while connecting , url=" + ftpDtls.getServerName() + ftpDtls.getFolderLocation() +  " , msg="+ e.getMessage());
			new CMSCallExceptions("ConnectTimeoutException exception caught while FTP upload, url " + ftpDtls.getServerName() + ftpDtls.getFolderLocation() + ftpDtls.getFileName() + 
					" msg : " + e.getMessage(),	CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
		} catch (ConnectException e) {
			log.error("ConnectException caught while connecting , url=" + ftpDtls.getServerName()+ " , FileName : " + ftpDtls.getFileName() + " , msg="+ e.getMessage());
			new CMSCallExceptions("ConnectException exception caught while FTP upload, url " + ftpDtls.getServerName() + ftpDtls.getFolderLocation() + ftpDtls.getFileName() + 
					" msg : " + e.getMessage(),	CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
        } catch (IOException e) {
        	log.error("IOException caught while connecting , url=" + ftpDtls.getServerName()+ " , FileName : " + ftpDtls.getFileName() + " , msg="+ e.getMessage());
        	new CMSCallExceptions("IOException exception caught while FTP upload, url " + ftpDtls.getServerName() + ftpDtls.getFolderLocation() + ftpDtls.getFileName() + 
					" msg : " + e.getMessage(),	CMSExceptionConstants.NEWSLETTER_BATCH_EXCEPTION, e);
        } finally {
        	NetstorageUtil.disconnect(ftpClient);
        }
		return false;
	}
	
	@Override
	public String getContentByHTTP(String url) throws Exception {
		return getContentByHTTP(url, 5000, 5000);
	}
	
	
	@Override
	public String getContentByHTTP(String url, int coonectTimeout, int socketTimeout) throws Exception {
		long startTime = System.currentTimeMillis();

		String outData = null;
		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		InputStream inputStream = null;
		HttpClient hClient = null;
		try {
			hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
			RequestConfig config = RequestConfig.custom().setMaxRedirects(5)
					.setConnectTimeout(coonectTimeout).setSocketTimeout(socketTimeout).build();
			
			url = UriUtils.encodeQuery(url, "UTF-8");
			gMethod = new HttpGet(url);
			gMethod.setConfig(config);
			//gMethod.addHeader("Content-Type", "text/html; charset=UTF-8");
			hResponse = hClient.execute(gMethod);
			//hResponse.setCharacterEncoding("utf-8");
			int status = hResponse.getStatusLine().getStatusCode();
			//hResponse.addHeader(CoreProtocolPNames.HTTP_CONTENT_CHARSET, Consts.UTF_8);
			
			if (status == HttpStatus.SC_OK) {
				
				// this is not working need to figureout
				//hResponse.setHeader("Content-Type", "text/html; charset=UTF-8");
				//hResponse.getHeaders(name)("Content-Type");
				//String outData = new BasicResponseHandler().handleResponse(hResponse);
				HttpEntity entity = hResponse.getEntity();
				//Charset charset = Charset.forName("UTF-8");
				inputStream = entity.getContent();
				if (inputStream != null) {
					outData = getContentByInputStream(inputStream);
				}
				//String extData = EntityUtils.toString(entity, charset);
				/*File f = new File("d:/test.htm");
				FileOutputStream fio = new FileOutputStream(f);
				fio.write(extData.getBytes());*/
				//extData = new String(extData.getBytes(), charset);
				log.debug("pulling time in ms : " + (System.currentTimeMillis() - startTime) + " of URL : " + url);
				return outData;
			}
		} catch (IOException e) {
			log.error("IOException caught during reading of URL: " + url + " msg : "+ e.getMessage());
			log.debug("pulling time in ms : " + (System.currentTimeMillis() - startTime) + " of URL : " + url);
			new CMSCallExceptions(url + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
			//outData = getContentByHTTP(url);
		} catch (Exception e) {
			log.error("Exception caught during reading of URL: " + url + " msg : "+ e.getMessage());
			new CMSCallExceptions(url + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
			
		}
		return outData;
	}
	
	
	/* (non-Javadoc)
	 * @see com.times.common.service.FileContentServices#getContentAndHeaderByHTTP(java.lang.String)
	 * This API fetch content and headers for given url. Both Content and headers are returned in map with keys 'Content' and 'Header'.
	 */
	@Override
	public Map<Object, Object> getContentAndHeaderByHTTP(String url) throws Exception {
		long startTime = System.currentTimeMillis();

		
		Map<Object, Object> resultMap = new HashMap<Object, Object>();
		Map<Object, Object> headerMap = new HashMap<Object, Object>();
		String outData = null;
		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		InputStream inputStream = null;
		HttpClient hClient = null;
		try {
			HttpRequestRetryHandler requestRetryHandler=new HttpRequestRetryHandler(){
				@Override
				public boolean retryRequest(IOException exception,
						int executionCount, HttpContext context) {
					// TODO Auto-generated method stub
					if (executionCount < 1) {
						return true;
					} else {
						return false;
					}
				}
			  };
			  
			
			hClient = HttpClients.custom().setRetryHandler(requestRetryHandler).build();
			RequestConfig config = RequestConfig.custom().setMaxRedirects(5)
					.setConnectTimeout(5000).setSocketTimeout(15000).build();

			url = UriUtils.encodeQuery(url, "UTF-8");
			gMethod = new HttpGet(url);
			gMethod.setConfig(config);

			hResponse = hClient.execute(gMethod);
			int status = hResponse.getStatusLine().getStatusCode();

			if (status == HttpStatus.SC_OK) {


				HttpEntity entity = hResponse.getEntity();

				inputStream = entity.getContent();
				if (inputStream != null) {
					outData = getContentByInputStream(inputStream);
					resultMap.put("Content", outData);
				}
				Header h[] = hResponse.getAllHeaders();
				if (h != null && h.length > 0) {
					for (int i = 0; i < h.length; i++) {
						headerMap.put(h[i].getName(), h[i].getValue());
					}
					resultMap.put("Header", headerMap);
				}
				log.debug("pulling time in ms : " + (System.currentTimeMillis() - startTime) + " of URL : " + url);
				return resultMap;
			}
		} catch (IOException e) {			
			
			log.error("getContentAndHeaderByHTTP () : IOException caught during reading of URL: " + url + " msg : "+ e.getMessage());
			
			
			new CMSCallExceptions(url + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
			throw e;
			
		} catch (Exception e) {
			log.error("getContentAndHeaderByHTTP() : Exception caught during reading of URL: " + url + " msg : "+ e.getMessage());
			new CMSCallExceptions(url + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
			throw e;
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);

		}
		return resultMap;
	}

	/**
	 * This method is used to store the file in local specified location for testing puporse
	 * 
	 * @param netstorageInfo
	 * @param fileConent
	 * @return
	 */
	public boolean writeFileInLocalFileSystem(NetstorageInfo netstorageInfo, String fileConent) {
		String fileLocation = "D:/opt/LiveBlog";
		boolean status = false;
		try {
			 
			String fileName = fileLocation + netstorageInfo.getFolderLocation() + "/" + netstorageInfo.getFileName();
			
			File file = new File(fileName);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(fileConent);
			bw.close();
 
			log.debug("Done : " + fileName);
 
		} catch (IOException e) {
			log.error("Exception raised while writing in local file : " + e.getMessage());
			//e.printStackTrace();
		}
		return status;
	}

	 /**
	   * Opens a FTP connection 
	   * Writes the File to the FTP reading and writing to the Output Stream 
	   * using character Stream
	   * @param accessDetailsObj, fileContent
	   * @return
	   * @throws Exception
	   */
	  public void createFilesOnFTP(NetstorageInfo netstorageInfo,String fileContent)  throws Exception  {
		  BufferedReader reader = null;
		  FTPClient client = new FTPClient();
		  OutputStream os=null;
		  OutputStreamWriter out=null;
		 // String finalcontent="<meta content='text/html; charset=UTF-8' http-equiv='Content-Type'>"+fileContent;
	    try  {
	    	Date startTime1 = Calendar.getInstance().getTime();
			Long startTime=new Long(Calendar.getInstance().getTimeInMillis());
		 // client.setConnectTimeout(1000);
	      client.connect(netstorageInfo.getServerName());
	      client.login(netstorageInfo.getUserid(), netstorageInfo.getPassword());
	      client.changeWorkingDirectory(netstorageInfo.getFolderLocation());

	      //client.setSoTimeout(2000);
	      //client.setTcpNoDelay(true);
	      os= new BufferedOutputStream(client.storeFileStream(netstorageInfo.getFileName()));
	      out = new OutputStreamWriter(os);
	      //out = new OutputStreamWriter(os,"UTF-8");
	      out.write(fileContent);
	      out.flush();
	      out.close();
	      os.close();
	      client.completePendingCommand();
	      Date endTime1 = Calendar.getInstance().getTime();
			Long endTime = new Long(Calendar.getInstance().getTimeInMillis());
			float time = (endTime-startTime);
			//log.info(Thread.currentThread().getName()+ " started at "+startTime1+" ,ended at "+endTime1+" and took "+ time/1000 + " seconds to complete" );
			System.out.println(Thread.currentThread().getName()+ " started at "+startTime1+" ,ended at "+endTime1+" and took "+ time/1000 + " seconds to complete FTP process" );
	    }
	    catch (Exception e) {
			e.printStackTrace();
		}
	    finally {
			client.logout();
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ioe) {

					ioe.printStackTrace();
				}
			}
		}
	  }


	@Override
	public String getContentByHTTP(String url, String failoverUrl)	throws Exception {
		String htmlContent = null;
		try {
			htmlContent = getContentByHTTP(url);
			if (htmlContent == null) {
				htmlContent = getContentByHTTP(failoverUrl);
			}
		
		} catch (IOException e) {
			//outData = getContentByHTTP(url);
		} catch (Exception e) {
			log.error("Exception caught during reading of URL: " + url + " msg : "+ e.getMessage());
			new CMSCallExceptions(url + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
		} finally {
			
		}
		return htmlContent;

		
	}

	/*public static void main(String[] args) {
		String url = "http://card.cricket.timesofindia.indiatimes.com/nbthtmlscorecard_test.cms";
		
		String data = getExtData(url);
		//hResponse.setHeader("Content-Type", "text/html; charset=UTF-8");
		String st = new String(data.getBytes("ISO-8859-1"), "UTF-8");
		System.out.println(st);
		System.out.println(data);
	}*/
	
	/*private static String getExtData(String strURL) {
		HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
		RequestConfig config = RequestConfig.custom().setMaxRedirects(2)
				.setConnectTimeout(5000).setSocketTimeout(5000).build();

		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try {
			strURL = UriUtils.encodeQuery(strURL, "UTF-8");
			gMethod = new HttpGet(strURL);
			gMethod.setConfig(config);
			hResponse = hClient.execute(gMethod);
			int status = hResponse.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				//hResponse.setHeader("", "UTF-8");
				hResponse.setHeader("Content-Type", "text/html; charset=UTF-8");
				String extData = new BasicResponseHandler().handleResponse(hResponse);
				hResponse.setHeader("Content-Type", "text/html; charset=UTF-8");
				String st = new String(extData.getBytes("ISO-8859-1"), "UTF-8");
				System.out.println(st);
				
				System.out.println("------------------------------------------------------------" );
				System.out.println(extData);
				return st;
			}
		} catch (Exception e) {
			log.error("Exception caught during reading of URL: " + strURL + " msg : "+ e.getMessage());
			new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.EXT_LOG, e);
		} finally {
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	}*/


	@Override
	public boolean uploadFileAmazonS3(AmazonS3Credits credits,	String str , Map headerDetails) {
		String fileName = "";
		try {
			AWSCredentials awsCredentials = new BasicAWSCredentials(credits.getAccessKey(), credits.getSecretKey());	
		    TransferManager tm= new TransferManager(awsCredentials);
			byte[] byteArray = str.getBytes("UTF-8");
			int len = byteArray.length;
		    InputStream inputStream = new ByteArrayInputStream(byteArray);
			ObjectMetadata meta =  new ObjectMetadata();
			
			meta.setContentLength(len);
			meta.setContentType("text/html;charset=UTF-8");
			
			if (headerDetails != null && headerDetails.size() > 0) {
				Iterator it = headerDetails.keySet().iterator();
				while (it.hasNext()) {
					String key = (String) it.next();
					if (!key.equalsIgnoreCase("Transfer-encoding")) {
						meta.setHeader(key, headerDetails.get(key));
					}
				}
				// below header for identify page is served from batch job.
				meta.setHeader(FROM_BATCH, "true");
			}
			
			AccessControlList acl = new AccessControlList();
			acl.grantPermission(GroupGrantee.AllUsers, Permission.Read);
			AmazonS3 s3client = tm.getAmazonS3Client();
			s3client.putObject(new PutObjectRequest(credits.getBucketName(), credits.getFileName() , inputStream, meta).withAccessControlList(acl));
			/*Upload upload = tm.upload( new PutObjectRequest(credits.getBucketName(), credits.getFileName() , inputStream, meta).withAccessControlList(acl));
			try {
				upload.waitForCompletion();
			} catch (AmazonServiceException e) {
				//e.printStackTrace();
				log.error("Exception caught during uploading into Amazon : " + credits.getFileName() + " msg : "+ e.getMessage());
				new CMSCallExceptions("Exception caught during uploading into Amazon : " + credits.getFileName() + " msg : "+ e.getMessage() + e.getMessage() + " with paramsMap ",
						CMSExceptionConstants.EXT_LOG, e);
			} catch (AmazonClientException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
			log.debug(credits.getFileName());
		}  catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
		 
	
	
	/**
	 * @param url
	 * @param startTime
	 * @param httpMethod
	 * @return
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	/*private String getData(String url, long startTime, HttpMethod httpMethod)	throws IOException, UnsupportedEncodingException {
		String outData;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();  
		byte[] byteArray = new byte[1024];  
		int count = 0;  
		while ((count = httpMethod.getResponseBodyAsStream().read(byteArray, 0, byteArray.length)) > 0) {
			outputStream.write(byteArray, 0, count);
		}
		outData = new String(outputStream.toByteArray(), "UTF-8");
		log.debug("pulling time in ms : " + (System.currentTimeMillis() - startTime) + " of URL : " + url);
		return outData;
	}*/

	
	private String getContentByInputStream(InputStream is)	throws IOException, UnsupportedEncodingException {
		String outData;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();  
		byte[] byteArray = new byte[1024];  
		int count = 0;  
		while ((count = is.read(byteArray, 0, byteArray.length)) > 0) {
			outputStream.write(byteArray, 0, count);
		}
		outData = new String(outputStream.toByteArray(), "UTF-8");
		
		if (outputStream != null) {
			outputStream.close();
			outputStream = null;
		}
		return outData;
	}
	
	public String getExtData(String strURL) {
		HttpClient hClient = HttpClients.custom()
				.setRetryHandler(new DefaultHttpRequestRetryHandler(3, false))
				.build();
		RequestConfig config = RequestConfig.custom()
				.setConnectTimeout(httpTimeout)
				.setSocketTimeout(httpTimeout)
				.build();
		HttpRequestBase gMethod = null;
		HttpResponse httpResponse = null;
		try{
			gMethod = new HttpGet(strURL);
			gMethod.setConfig(config);
			httpResponse = hClient.execute(gMethod);
			int status = httpResponse.getStatusLine().getStatusCode();
			String extData = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
			if(status == HttpStatus.SC_OK){
				return extData;
			}else{
				log.error("Url: "+ strURL + ", Status: " + status + ", Data: " + extData);
				CMSCallExceptions tmperr = new CMSCallExceptions(strURL+
						"-status-"+status+" - BLANK - ResponseBody"+extData,
						CMSExceptionConstants.CMS_IO_Exception);
				tmperr = null;
			}
		} catch(Exception e){
			
			System.out.println("Exception while hiiting: "+ e);
//			logger.error(strURL + " - Exception Occured in get the external data", e);
//			new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
//					CMSExceptionConstants.EXT_LOG, e);			
		} finally{
			HttpClientUtils.closeQuietly(httpResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	}


	@Override
	public ImageInfo getOptimizedImage(ImageInfo imageInfo) throws Exception {

		ImageWriter imgWrtr = null;

		byte b[] = imageInfo.getOriginalData();
		ByteArrayInputStream bais = new ByteArrayInputStream(b);
		BufferedImage image = ImageIO.read(bais);

		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		if (image != null) {
			
			ImageInputStream iis = ImageIO.createImageInputStream(bais);

			Iterator<ImageReader> imageReaders = ImageIO.getImageReaders(iis);

			while (imageReaders.hasNext()) {
			    ImageReader reader = (ImageReader) imageReaders.next();
			    System.out.printf("formatName: %s%n", reader.getFormatName());
			}

			imgWrtr = ImageIO.getImageWritersByFormatName("jpg").next();
			
			ImageWriteParam param = imgWrtr.getDefaultWriteParam();
			
			if (param.canWriteCompressed()) { 
				// NOTE: Any method named [set|get]Compression.* throws UnsupportedOperationException if false
				// True in case of jpeg only
				param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
				param.setCompressionQuality(0.7f);
			}

			IIOImage iioImg = new IIOImage(image, null, null);
			
			ImageOutputStream ios = ImageIO.createImageOutputStream(bos);
			imgWrtr.setOutput(ios);

			imgWrtr.write(null, iioImg, param);

			bos.flush();
			imgWrtr.dispose();

			byte[] imageInByte = bos.toByteArray();

//			OutputStream out =null;
//			try {
//				out = new BufferedOutputStream(new FileOutputStream("E:/image/test_new4.jpg"));
//				out.write(imageInfo.getOptimizedData());
//			} finally {
//
//				if (out != null){
//					out.close();
//				}
//			}
//
//			OutputStream out1 =null;
//			try {
//				out1 = new BufferedOutputStream(new FileOutputStream("E:/image/test_orig4.jpg"));
//				out1.write(imageInfo.getOriginalData());
//			} finally {
//
//				if (out1 != null){
//					out1.close();
//				}
//			}

			bos.close();
			imageInfo.setOptimizedData(imageInByte);
		}

		return imageInfo;
	}
	
	public String getExtDataWithoutRetry(String strURL){

		HttpClient hClient = HttpClients.custom()
				.build();
		RequestConfig config = RequestConfig.custom()
				.setConnectTimeout(httpTimeout)
				.setSocketTimeout(httpTimeout+2000)
				.build();
		HttpRequestBase gMethod = null;
		HttpResponse httpResponse = null;
		try{
			gMethod = new HttpGet(strURL);
			gMethod.setConfig(config);
			httpResponse = hClient.execute(gMethod);
			int status = httpResponse.getStatusLine().getStatusCode();
			String extData = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
			if(status == HttpStatus.SC_OK){
				return extData;
			}else{
				log.error("\nURL: "+ strURL + ",\nSTATUS-CODE: " + status + ",\nDATA: " + extData);
			}
		} catch(Exception e){
			
			log.error("Exception while hitting:  "+ strURL, e);
//			logger.error(strURL + " - Exception Occured in get the external data", e);
//			new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ",
//					CMSExceptionConstants.EXT_LOG, e);			
		} finally{
			HttpClientUtils.closeQuietly(httpResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	
} 
		
	
}
