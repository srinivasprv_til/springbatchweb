/*package com.times.common.client;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.map.ObjectMapper;

import com.mongodb.DBObject;
import com.times.common.dao.MongoRestAPIService;
import com.times.common.dao.PerfectMarketingDaoImpl;
import com.times.common.model.PerfectMarketingObject;

public class PerfectMarketingEntryPoint {

	public static final String PM_RESULT_COLLECTION = "pmresult";
	public static final String PM_UNSUCESSFUL_COLLECTION = "pmunsuccessful";
	public static final String PM_FAILED_COLLECTION = "pmfailed";
	
	public static void main(String[] args) throws SQLException,
			ClassNotFoundException {
		
		String extUrl = "http://timesofindia-ws.indiatimes.perfectmarket.com/map.xml?url=timesofindia.com-#msid#";
		Date startDate = null;
		try {
			startDate = PerfectMarketingDaoImpl.getTopDate();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		LinkedHashMap listMap1 = (LinkedHashMap) PerfectMarketingDaoImpl.getTopMsid(startDate);
		LinkedHashMap listMap2 = new LinkedHashMap();
		try {
			listMap2 = (LinkedHashMap) PerfectMarketingDaoImpl.getAllPMObject(PM_UNSUCESSFUL_COLLECTION);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		LinkedHashMap listMap  = new LinkedHashMap();
		if(listMap2!=null){
			listMap.putAll(listMap2);
		}
		listMap.putAll(listMap1);
		
		Set<String> keys = listMap.keySet();
		
		for (String msid : keys) {
			Date date = (Date) listMap.get(msid);
			String url = extUrl.replaceAll("#msid#", msid);
			String pmURL = null;
			pmURL = MongoRestAPIService.getExtData(url);
	
			PerfectMarketingObject pmObj = new PerfectMarketingObject();
			
			if(pmURL!=null && !pmURL.equals("null")){
				pmURL = pmURL.substring(pmURL.indexOf("http://"), pmURL.indexOf("</uri>"));

				try {
					if(PerfectMarketingDaoImpl.getPMDBObject(msid,PM_UNSUCESSFUL_COLLECTION)==null){
						if(PerfectMarketingDaoImpl.getPMDBObject(msid,PM_RESULT_COLLECTION)==null){
							int serialNo = PerfectMarketingDaoImpl.getSerialNo(PM_RESULT_COLLECTION);
							pmObj.setSerialnumber(serialNo+1);
							pmObj.setMsid(Integer.valueOf(msid));
							pmObj.setRedirecturl(pmURL);
							pmObj.setDateadded(date);
							pmObj.setNoattempt(1);
							if(PerfectMarketingDaoImpl.addPMObject(pmObj,PM_RESULT_COLLECTION)){
								PerfectMarketingDaoImpl.updateSerialNo(PM_RESULT_COLLECTION,serialNo+1);
							}
						}
						else{
							pmObj = PerfectMarketingDaoImpl.getPMObject(msid,PM_RESULT_COLLECTION);
							PerfectMarketingObject pmUpdateObj = pmObj.clone();
							pmUpdateObj.setDateadded(date);
							pmUpdateObj.setRedirecturl(pmURL);
							PerfectMarketingDaoImpl.updatePMObject(pmObj, pmUpdateObj,PM_RESULT_COLLECTION);
						}
					}
					else{
						pmObj = PerfectMarketingDaoImpl.getPMObject(msid,PM_UNSUCESSFUL_COLLECTION);
						PerfectMarketingDaoImpl.deletePMObject(msid, PM_UNSUCESSFUL_COLLECTION);
						int serialNo = PerfectMarketingDaoImpl.getSerialNo(PM_UNSUCESSFUL_COLLECTION);
						if(serialNo>0){
							serialNo -= 1;
						}
						PerfectMarketingDaoImpl.updateSerialNo(PM_UNSUCESSFUL_COLLECTION,serialNo);
						
						PerfectMarketingObject pmUpdateObj = pmObj.clone();
						pmUpdateObj.setRedirecturl(pmURL);
						pmUpdateObj.setDateadded(date);
						pmUpdateObj.setNoattempt(pmObj.getNoattempt()+1);
						serialNo = PerfectMarketingDaoImpl.getSerialNo(PM_RESULT_COLLECTION);
						pmObj.setSerialnumber(serialNo+1);
						if(PerfectMarketingDaoImpl.addPMObject(pmObj,PM_RESULT_COLLECTION)){
							PerfectMarketingDaoImpl.updateSerialNo(PM_RESULT_COLLECTION,serialNo+1);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println(pmURL);
			}
			else{
				try {
					if(PerfectMarketingDaoImpl.getPMDBObject(msid,PM_RESULT_COLLECTION)==null){
						if(PerfectMarketingDaoImpl.getPMDBObject(msid,PM_FAILED_COLLECTION)==null){
							if(PerfectMarketingDaoImpl.getPMDBObject(msid,PM_UNSUCESSFUL_COLLECTION)==null){
								int serialNo = PerfectMarketingDaoImpl.getSerialNo(PM_UNSUCESSFUL_COLLECTION);
								pmObj.setSerialnumber(serialNo+1);
								pmObj.setMsid(Integer.valueOf(msid));
								pmObj.setRedirecturl(pmURL);
								pmObj.setDateadded(date);
								pmObj.setNoattempt(1);
								if(PerfectMarketingDaoImpl.addPMObject(pmObj,PM_UNSUCESSFUL_COLLECTION)){
									PerfectMarketingDaoImpl.updateSerialNo(PM_UNSUCESSFUL_COLLECTION,serialNo+1);
								}
							}
							else{
								pmObj = PerfectMarketingDaoImpl.getPMObject(msid,PM_UNSUCESSFUL_COLLECTION);
								if(pmObj.getNoattempt()>=2){
									int serialNo = PerfectMarketingDaoImpl.getSerialNo(PM_UNSUCESSFUL_COLLECTION);
									
									if(PerfectMarketingDaoImpl.deletePMObject(msid, PM_UNSUCESSFUL_COLLECTION)){
										PerfectMarketingDaoImpl.updateSerialNo(PM_UNSUCESSFUL_COLLECTION,serialNo-1);
										PerfectMarketingObject pmUpdateObj = pmObj.clone();
										pmUpdateObj.setNoattempt(pmObj.getNoattempt()+1);
										pmUpdateObj.setRedirecturl(pmURL);
										pmUpdateObj.setDateadded(date);
										if(PerfectMarketingDaoImpl.addPMObject(pmUpdateObj, PM_FAILED_COLLECTION)){
											PerfectMarketingDaoImpl.updateSerialNo(PM_FAILED_COLLECTION,PerfectMarketingDaoImpl.getSerialNo(PM_FAILED_COLLECTION)+1);
										}
									}
								}
								else{
									PerfectMarketingObject pmUpdateObj = pmObj.clone();
									pmUpdateObj.setNoattempt(pmObj.getNoattempt()+1);
									pmUpdateObj.setRedirecturl(pmURL);
									PerfectMarketingDaoImpl.updatePMObjectByMsid(msid, pmUpdateObj, PM_UNSUCESSFUL_COLLECTION);
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			//	System.out.println(pmURL);
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		Set<String> keys2 = listMap.keySet();

		for (String msid : keys2) {
			try {
				//PerfectMarketingObject pmObj = PerfectMarketingDAO.getPMObject(msid);
				DBObject pmDBObj = PerfectMarketingDaoImpl.getPMDBObject(msid,PM_RESULT_COLLECTION);
				ObjectMapper mapper = new ObjectMapper();
				System.out.println(pmDBObj.toString());
				//System.out.println(mapper.defaultPrettyPrintingWriter().writeValueAsString(pmObj));
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}
*/