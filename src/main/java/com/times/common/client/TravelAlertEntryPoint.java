/**
 * 
 */
package com.times.common.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;
import com.times.mailer.dao.TravelNewsletterDao;
import com.times.mailer.dao.TravelNewsletterDaoImpl;
import com.times.mailer.model.Mailer;

/** 
 * The main method of this class is used by cronTab.
 * 
 * @author ranjeet.jha
 *
 */
public class TravelAlertEntryPoint {

	private static final Log logger = LogFactory.getLog(TravelAlertEntryPoint.class);
	
	public static TravelNewsletterDao newsletterMasterDao;
	
	//private JobLauncher jobLauncher;
	public static ApplicationContext ctx = null;
	public static final String TRAVEL_ALERT_JOB_CONTEXT_NAME = "travel-alert-JobContext.xml";
	public static final String TRAVEL_ALERT_NEWSLETTER_JOB_ID = "travelAlertNewsletterJob";
	public static final String JOB_LAUNCHER_ID = "jobLauncher";
	public static final String NEWSLETTER_MASTER_DAO_ID = "newsletterMasterDao";
	
	/**
	 * Do not remove System.out.print on this method this is the out put of the shell script which fire the job.
	 * and do not put extra SOP in this method this would be problematic for the shell script.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if (ctx == null) {
			ctx = new ClassPathXmlApplicationContext(new String[]{TRAVEL_ALERT_JOB_CONTEXT_NAME});
		}
		TravelNewsletterDao travelNewsletterDao = (TravelNewsletterDaoImpl) getApplicationContext().getBean(NEWSLETTER_MASTER_DAO_ID);
		//dao.getNewsletterScheduled();
		/*newsletterMasterDao = travelNewsletterDao;
		List<Mailer> mailerList = newsletterMasterDao.getNewsletterScheduled();	
		if (mailerList != null) {
			for (Mailer entry : mailerList) {
				// This print is output for the shell script i.e. opt/script/newsletterscript.sh and fire the job based on the output of this SOP 	
				System.out.print(entry.getId() + ","+ getDailyOrWeeklyParam(entry) + ","+ getMorningOrEveningParam(entry) + "|");
			}
		}*/
		System.out.print("1031,1,1" + "|");
		
	}
	
	public void checkScheduledNewsLetter() {
		//System.out.println(new Date());
	}
	
	private static int getHour(){
		String[] hrMin = DateUtils.getHourAndMin(new Date()).split(":");
		String hrs = hrMin[0];
		String min = hrMin[1];
		int ihr = (int)Math.ceil(Double.parseDouble(hrs + "." + min));
		return ihr;
	}

	/**
	 * @param morningOrEvening
	 * @param entity
	 * @return
	 */
	private static long getMorningOrEveningParam(Mailer entity) {
		long morningOrEvening = 0;
		if (entity.getScheduleTime() == 1) {
			morningOrEvening = Constant.JOB_PARAM_NL_MORNING;
		} else if (entity.getScheduleTime() == 2) {
			morningOrEvening = Constant.JOB_PARAM_NL_EVENING;
		}
		return morningOrEvening;
	}

	/**
	 * @param dailyOrWeekly
	 * @param entity
	 * @return
	 */
	private static long getDailyOrWeeklyParam(Mailer entity) {
		long dailyOrWeekly = 0;
		if (entity.isDaily()) {
			dailyOrWeekly = Constant.JOB_PARAM_NL_DAILY;
		} else if (entity.isWeekly()) {
			dailyOrWeekly = Constant.JOB_PARAM_NL_WEEKLY;
		}
		return dailyOrWeekly;
	}
	
	/**
	 * @param newsletterMasterDao the newsletterMasterDao to set
	 */
	public  void setNewsletterMasterDao(TravelNewsletterDao newsletterMasterDao) {
		this.newsletterMasterDao = newsletterMasterDao;
	}

	public static ApplicationContext getApplicationContext() {
		if (ctx == null) {
			ctx = new ClassPathXmlApplicationContext(new String[]{TRAVEL_ALERT_JOB_CONTEXT_NAME});
		}
		return ctx;
	}
	
	public static void setApplicationContext(ApplicationContext applicationContext) {
		ctx = applicationContext;
	}
}
