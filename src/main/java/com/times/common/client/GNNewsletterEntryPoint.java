package com.times.common.client;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.times.common.util.Constant;
import com.times.common.util.DateUtils;
import com.times.mailer.batch.MailerReader;
import com.times.mailer.dao.MailerDao;
import com.times.mailer.dao.MailerDaoImpl;
import com.times.mailer.model.Mailer;

/**
 * This class is used as a Gadget Now Newsletter entry point and hooked by unix cronTab.
 * This class is called by the cron tab.
 * 
 * @author rajeev khatri
 *
 */
public class GNNewsletterEntryPoint {

	private static final Log logger = LogFactory.getLog(GNNewsletterEntryPoint.class);
	
	public static MailerDao newsletterMasterDao;
	
	private JobLauncher jobLauncher;
	public static ApplicationContext ctx = null;
	public static final String LAUNCH_CONTEXT_NAME = "launch-context.xml";
	public static final String NEWS_MAILER_JOB_ID = "NewsMailerJob";
	public static final String JOB_LAUNCHER_ID = "jobLauncher";
	
	
	private static final List<String> GADGETNOW__NL_LIST = new ArrayList<String>(){{
		 add("1025") ;
		}};
	
	/**
	 * Do not remove System.out.print on this method this is the out put of the shell script which fire the job.
	 * and do not put extra SOP in this method this would be problematic for the shell script.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if (ctx == null) {
			ctx = new ClassPathXmlApplicationContext(new String[]{LAUNCH_CONTEXT_NAME});
		}
		MailerDao dao = (MailerDaoImpl) getApplicationContext().getBean("newsletterMasterDao");
		//dao.getNewsletterScheduled();
		newsletterMasterDao = dao;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		//cal.add(Calendar.DATE, -4);
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		
		List<Mailer> mailerList = newsletterMasterDao.getNewsletterScheduled();	
		if (mailerList != null) {
			for (Mailer entry : mailerList) {
				
				if (GADGETNOW__NL_LIST.contains(entry.getId())) {
					// Movie Review run only on sat morning / first half
					if (entry.getId().equals("1003")) {
						int iHr = getHour();
						if (dayOfWeek == 7 && iHr < 12) { // 7 mean sat and entry.getScheduleTime() == 1 mean morning
							System.out.print(entry.getId() + ","+ getDailyOrWeeklyParam(entry) + ","+ getMorningOrEveningParam(entry) + "|");
							continue;
						} else {
							continue;
						}
					} 
					
					if (entry.getId().equals("1005")) {
						if (getDailyOrWeeklyParam(entry) == 2) {
							continue;
						}						
					}
					// This print is output for the shell script i.e. opt/script/newsletterscript.sh and fire the job based on the output of this SOP 	
					System.out.print(entry.getId() + ","+ getDailyOrWeeklyParam(entry) + ","+ getMorningOrEveningParam(entry) + "|");
				}
			}
				
		}
		
	}
	
	public void checkScheduledNewsLetter() {
		//System.out.println(new Date());
	}
	
	private static int getHour(){
		String[] hrMin = DateUtils.getHourAndMin(new Date()).split(":");
		String hrs = hrMin[0];
		String min = hrMin[1];
		int ihr = (int)Math.ceil(Double.parseDouble(hrs + "." + min));
		return ihr;
	}
	
	/**
	 * This method is used to check newsletter and if found scheduled then kick-off the job with specified parameter.
	 * TODO: this function is no more used and need to remove this 
	 */
	public void checkScheduledNewsLetter1() {
		long minDiff = 0;
		long dailyOrWeekly = 0;
		long morningOrEvening = 0;
		
		try {
			jobLauncher = (JobLauncher) getApplicationContext().getBean(GNNewsletterEntryPoint.JOB_LAUNCHER_ID);
			
			Date now = new Date();
			int hr = DateUtils.getHour(now);
			int mm = DateUtils.getMinute(now);
			hr = (int)Math.ceil(Double.parseDouble(hr + "." + mm));
			List<Mailer> mailerList = newsletterMasterDao.getNewsletterScheduled();	
			List<Mailer> mailerToBeExecuteList = new ArrayList<Mailer>();
			
			//TODO: for testing.
			
			if (mailerList != null) {
				for (Mailer entity : mailerList) {
					
					if (hr > 12) {
						minDiff = DateUtils.getMinuteDiff(now, entity.getMorningTime());
						
						
						minDiff = 6;
						if (minDiff < MailerReader.TIME_INTERVAL && minDiff > 0) {
							dailyOrWeekly = this.getDailyOrWeeklyParam( entity);
							morningOrEvening = this.getMorningOrEveningParam(entity);
							
							try {
								//TOINewsletterJobLauncher.getInstance().checkJobAndLaunchAsync(jobLauncher, entity.getId(), dailyOrWeekly, morningOrEvening);
								String[] args = {String.valueOf(entity.getId()), String.valueOf(dailyOrWeekly), String.valueOf(morningOrEvening)};
							
							} catch (Exception e) {
								e.printStackTrace();
								// TODO: handle exception
							}
						}
						
					} else{
						minDiff = DateUtils.getMinuteDiff(now, entity.getEveningTime());
						
						minDiff = 6; 
						if (minDiff < MailerReader.TIME_INTERVAL && minDiff > 0) {
							dailyOrWeekly = this.getDailyOrWeeklyParam( entity);
							morningOrEvening = this.getMorningOrEveningParam(entity);
							String[] args = {String.valueOf(entity.getId()), String.valueOf(dailyOrWeekly), String.valueOf(morningOrEvening)};
							
							Runtime.getRuntime().exec("java -cp \"target\\SpringBatchWeb-1.0.0-SNAPSHOT.jar;dependency\\*\" com.times.common.util.NewsletterJobLauncher", args);
						}
					}
				}
			}
			Thread.sleep(100000000);
			
		} catch (Exception e) {
			String msg = "Exception caught while checking scheduled newsletter " + DateUtils.getformatedDate(Constant.DATE_FORMAT_YYY_MM_DD_HH_MM_SSS, new Date()) + " , msg :"+ e.getLocalizedMessage();
			//new CMSCallExceptions(msg, CMSExceptionConstants.CMS_Exception, e);
			logger.error(msg);
		}
	}

	/**
	 * @param morningOrEvening
	 * @param entity
	 * @return
	 */
	private static long getMorningOrEveningParam(Mailer entity) {
		long morningOrEvening = 0;
		if (entity.getScheduleTime() == 1) {
			morningOrEvening = Constant.JOB_PARAM_NL_MORNING;
		} else if (entity.getScheduleTime() == 2) {
			morningOrEvening = Constant.JOB_PARAM_NL_EVENING;
		}
		return morningOrEvening;
	}

	/**
	 * @param dailyOrWeekly
	 * @param entity
	 * @return
	 */
	private static long getDailyOrWeeklyParam(Mailer entity) {
		long dailyOrWeekly = 0;
		if (entity.isDaily()) {
			dailyOrWeekly = Constant.JOB_PARAM_NL_DAILY;
		} else if (entity.isWeekly()) {
			dailyOrWeekly = Constant.JOB_PARAM_NL_WEEKLY;
		}
		return dailyOrWeekly;
	}
	
	/**
	 * @param newsletterMasterDao the newsletterMasterDao to set
	 */
	public  void setNewsletterMasterDao(MailerDao newsletterMasterDao) {
		this.newsletterMasterDao = newsletterMasterDao;
	}

	public static ApplicationContext getApplicationContext() {
		if (ctx == null) {
			ctx = new ClassPathXmlApplicationContext(new String[]{LAUNCH_CONTEXT_NAME});
		}
		return ctx;
	}
	
	public static void setApplicationContext(ApplicationContext applicationContext) {
		ctx = applicationContext;
	}
}
