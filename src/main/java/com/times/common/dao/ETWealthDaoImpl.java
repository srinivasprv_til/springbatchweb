package com.times.common.dao;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.times.mailer.batch.processor.ETWealthProcessor;

public class ETWealthDaoImpl implements ETWealthDaoInterface {

	private SimpleJdbcTemplate jdbcTemplate;
	
	private static final Logger log = LoggerFactory.getLogger(ETWealthDaoImpl.class);
	
	public ETWealthDaoImpl(DataSource ds) {
		this.jdbcTemplate = new SimpleJdbcTemplate(ds);
	}
	
	/**
	 * @author praveen.bokka
	 * 
	 * @param date1, date2 : two dates between which msids are required
	 * @return Map of msids and their publish dates
	 */
	public Map<String, Date> getMsids(Date date1, Date date2) {
		String sql = "select top 100 c.ART_DATE, c.MSID from Hierarchy as h(nolock) , Content as c(nolock) where h.msid= c.msid and h.mas_msid=7118560 and c.ART_DATE > '#DATE1' and c.ART_DATE < '#DATE2'";
		
		java.sql.Date sqlDate1 = new java.sql.Date(date1.getTime());
		java.sql.Date sqlDate2 = new java.sql.Date(date2.getTime());
		
		sql = sql.replace("#DATE1", sqlDate1.toString());
		sql = sql.replace("#DATE2", sqlDate2.toString());
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Map<String, Date> msidMap = new HashMap<String, Date>();
		
		Map<String, Integer> paramMap = new HashMap<String, Integer>();
		
		List<Map<String, Object>> idsList = this.jdbcTemplate.queryForList(sql,
				paramMap);
		
		if (idsList != null && idsList.size() > 0) {
			for (Map<String, Object> rs : idsList) {
				String msid =  String.valueOf(rs.get("msid"));
				Timestamp t = (Timestamp) rs.get("art_Date");
				String sDate = t.toString();
				try {
					Date date = dateFormat.parse(sDate);
					msidMap.put(msid, date);
				} catch (ParseException e) {
					log.error("SQL Exception occurred in getMsids with error = " + e.getMessage());
				}
			}
		}
		
		return msidMap;
	}
	
	

}
