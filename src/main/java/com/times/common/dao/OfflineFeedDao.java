package com.times.common.dao;

import java.util.List;

import com.mongodb.DBObject;
import com.times.common.model.UserActivity;
import com.times.common.model.UserComment;
import com.times.common.model.UserFeed;

public interface OfflineFeedDao {
	//public  DBObject getDBObjectByUserFeed(UserFeed userFeed) throws Exception;
	/*public  UserFeed getDBObjectByUserFeed(DBObject dbObject) throws Exception;
	public  UserActivity getDBObjectByUserActivity(DBObject activityDBObject) throws Exception;
	public  UserComment getDBObjectByUserComment(DBObject commentDBObject) throws Exception;
	*/
	public  List<UserFeed> getUserFeedList() throws Exception;
	public boolean updateStatus(UserFeed userFeed) throws Exception;
}
