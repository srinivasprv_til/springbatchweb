package com.times.common.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.mongodb.DBObject;
import com.times.common.model.PerfectMarketingObject;

public interface PerfectMarketingDAO {

	public Map getTopMsid(String lastMsid);
	public boolean addPMObject(PerfectMarketingObject pmObj, String collection);
	public PerfectMarketingObject getPMObject(String msid, String collection);
	public DBObject getPMDBObject(String msid, String collection,  String pmURL);
	public void updatePMObject(PerfectMarketingObject pmObj, PerfectMarketingObject pmUpdateObj, String collection);
	public void updatePMObjectByMsid(String msid, PerfectMarketingObject pmUpdateObj, String collection);
	public int getSerialNo(String collection);
	public void updateSerialNo(String collection, int serialNo);
	public void createSerialNo(String collection);
	public Date getTopDate();
	public String getLastMsid();
	public boolean deletePMObject(String msid, String collection);
	public Map getAllPMObject(String collection);
	public List<PerfectMarketingObject> getAllCollection(String collection);
	public int getTotalActiveStoriesInSection (String sectionMsid);
	public Map getLastProcessedMsid(String lastMsid , String sectionId);
	





	
}
