package com.times.common.dao;

import java.util.List;

import com.times.common.model.IBeatEntity;

public interface IBeatDAO {

	public static final String LAST_HOUR_UPDATES_API="http://ibeatserv.indiatimes.com/iBeat/topicData.html";

	/**
	 * Get IBeatEntities accessed in last one hour for a given hostName
	 * @see com.times.common.model.IBeatEntity
	 * @param hostName
	 * @return List of IbeatEntities accessed in last hour
	 */
	public List <IBeatEntity> getLastHourData(String hostName);

	//Implement if required
	//public List<IBeatEntity> getLastHourData(String hostId, String folderId) 




}
