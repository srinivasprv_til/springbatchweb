package com.times.common.dao;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Map.Entry;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import org.jdom2.Element;
import org.jdom2.Document;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPath;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.xml.sax.InputSource;

import com.times.common.util.EhcacheBatchUtil;
import com.times.common.util.ExternalHttpUtil;



public class CricketMYSQLDaoImpl implements CricketMYSQLDaoInterface {

	private SimpleJdbcTemplate jdbcTemplate;
	
	private static final Logger log = LoggerFactory.getLogger(CricketMYSQLDaoImpl.class);
	
	public CricketMYSQLDaoImpl(DataSource ds) {
		this.jdbcTemplate = new SimpleJdbcTemplate(ds);
	}
	
	/**
	 * @author Geetanjali.Tiwari
	 * 
	 * @param matchid
	 * @return 
	 */
	
	public int insertMatchID(String matchid, String status){
		String sql = "INSERT INTO cricket_match (matchid,startedAt,no_of_days,STATUS) VALUES ("+matchid+",NOW(),1,1)";
		return jdbcTemplate.update(sql, new HashMap());
	}
	public int updateMatchID(String matchid, String status){
		String sql = "update  cricket_match set status = 0 where status=1 and matchid="+matchid;
		return jdbcTemplate.update(sql, new HashMap());
	}
	public int selectMatchID(String matchid){
		String sql = "select matchid from cricket_match where matchid="+matchid+" limit 1";
		List lst = jdbcTemplate.queryForList(sql,  new HashMap());
		if(lst == null || lst.size() < 1){
			return  0;
		}else{
			Map<String,Integer> rval = (Map<String,Integer>) lst.iterator().next();
			return rval.get("matchid");
		}
		
	}
}