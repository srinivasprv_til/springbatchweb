package com.times.common.dao;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.springframework.web.util.UriUtils;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class MyttestMongoRestAPIService {
	
	private static final Log log = LogFactory.getLog(MyttestMongoRestAPIService.class);
	
	/*
	 * Please do not change this URL this is for test environment.
	 */
	public static String MONGO_REST_API_URL = "http://myttest.indiatimes.com/mra/";
	
	private static final String getURL = "get/#collection?query=#query";
	private static final String updateURL = "update/#collection?queryJson=#query&rowJson=#rowJson";
	private static final String insertURL = "insert/#collection?rowJson=#rowJson";
	private static final String deleteURL = "delete/#collection?query=#query";
	private static final String findandUpdateURL = "findAndUpdate/#collection?query=#query&rowJson=#rowJson";
	private static final String groupURL = "group/#collection?query=#query";
	
	/*static HttpClientParams clientParams = new HttpClientParams();
	
	static{
		clientParams.setParameter(HttpClientParams.MAX_REDIRECTS, "5");
		clientParams.setParameter(HttpClientParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));
		clientParams.setConnectionManagerTimeout(120000);		
		clientParams.setSoTimeout(120000);
	}*/

	public static DBObject get(String collection, DBObject query) {
		String extData = null;
		String strURL = null;
		List<DBObject> dbObj = null;
		strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_URL + strURL;
		extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			Object dbObjectRes = JSON.parse(extData);
			if (dbObjectRes instanceof List<?>) {
				List<DBObject> dbObjects = (List<DBObject>) dbObjectRes;
				if (((List<DBObject>) dbObjectRes).size() > 0) {
					return ((List<DBObject>) dbObjectRes).get(0);
				}
			} else if (dbObjectRes instanceof DBObject) {
				return (DBObject) dbObjectRes;
			}
		}
		return null;
	}
	
	public static List<DBObject> getList(String collection, DBObject query) {
		String extData = null;
		String strURL = null;
		List<DBObject> dbObj = null;
		strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_URL + strURL;
		extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			Object dbObjectRes = JSON.parse(extData);
			if (dbObjectRes instanceof List<?>) {
					return ((List<DBObject>) dbObjectRes);
			} 
		}
		return null;
	}
	
	public static List<DBObject> getList(String collection, DBObject query, Map<String, Object> options) {
		String extData = null;
		String strURL = null;
		List<DBObject> dbObj = null;
		strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_URL + strURL + optiosParameter(options);
		extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			Object dbObjectRes = JSON.parse(extData);
			if (dbObjectRes instanceof List<?>) {
					return ((List<DBObject>) dbObjectRes);
			} 
		}
		return null;
	}

	private static String optiosParameter(Map<String, Object> options){
		StringBuilder builder = new StringBuilder();
		for(Map.Entry<String, Object> entry : options.entrySet()){
			builder.append("&"+entry.getKey()+"="+entry.getValue());
		}
		return builder.toString();
	}

	public static boolean insert(String collection, DBObject rowJson) {
		String strURL = null;
		strURL = insertURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))
				.replaceAll("#collection", collection);
		strURL = MONGO_REST_API_URL + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}

	public static boolean update(String collection, DBObject queryJson,
			DBObject rowJson) {
		String strURL = null;
		strURL = updateURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))
				.replaceAll("#collection", collection).replaceAll("#query",
						Matcher.quoteReplacement(queryJson.toString()));
		strURL = MONGO_REST_API_URL + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}

	public static boolean delete(String collection, DBObject queryJson) {
		String strURL = null;
		strURL = deleteURL.replaceAll("#collection", collection).replaceAll(
				"#query", Matcher.quoteReplacement(queryJson.toString()));
		strURL = MONGO_REST_API_URL + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}

	public static boolean findAndUpdate(String collection, DBObject queryJson,
			DBObject rowJson) {
		String strURL = null;
		strURL = findandUpdateURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))
				.replaceAll("#collection", collection).replaceAll("#query",
						Matcher.quoteReplacement(queryJson.toString()));
		strURL = MONGO_REST_API_URL + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}

	private static String getExtData(String strURL) {
		/*HttpClient hClient = new HttpClient();	
		HttpMethod gMethod = null;
		try {
			strURL = URIUtil.encodeQuery(strURL);
			this replce is to hanlde the life & style section to get msid
			strURL = strURL.replace("%20&%20", "+%26+");
			gMethod = new GetMethod(strURL);
			gMethod.setParams(clientParams);
			int status = hClient.executeMethod(gMethod);
			if (status == 200) {
				String extData = new String(gMethod.getResponseBody(), "UTF-8");
				return extData;
			}*/
			
			HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
			RequestConfig config = RequestConfig.custom().setMaxRedirects(5)
					.setConnectTimeout(120000).setSocketTimeout(120000).build();

			HttpRequestBase gMethod = null;
			HttpResponse hResponse = null;
			try {
				strURL = strURL.replace("%20&%20", "+%26+");
				strURL = UriUtils.encodeQuery(strURL, "UTF-8");
				gMethod = new HttpGet(strURL);
				gMethod.setConfig(config);
				hResponse = hClient.execute(gMethod);
				int status = hResponse.getStatusLine().getStatusCode();
				if (status == HttpStatus.SC_OK) {
					String outData = new BasicResponseHandler().handleResponse(hResponse);
					return outData;
				}
		} catch (Exception e) {
			/*new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage()
					+ " with paramsMap ", CMSExceptionConstants.CMS_Exception, e);*/
			//CMSCallExceptions cmsCall = new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ", CMSExceptionConstants.CMS_Exception, e);
			log.error(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage()	+ " with paramsMap " + e.getMessage());
		} finally {
			if(gMethod != null){
				gMethod.releaseConnection();
				gMethod = null;
			}
		}
		return null;
	}
	
	public static DBObject group(String collection, DBObject query, Map<String, Object> options) {
		String strURL = null;
		strURL = groupURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_URL + strURL + optiosParameter(options);
		String extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			DBObject dbObjectRes = (DBObject) JSON.parse(extData);
			return dbObjectRes;
		}
		return null;
	}

	/*public static void main(String args[]) throws IOException {

		DBObject query = new BasicDBObject();
		query.put("seolocation", "http://localhost/home/education/entrance-exams");
		DBObject result = get("virtualurlmapping", query);

		DBObject insertDBObj = new BasicDBObject();
		insertDBObj.put("sname", "Ajay");
		insertDBObj.put("seolocation", "http://ajay.com");
		insert("virtualurlmapping", insertDBObj);

		DBObject updateDBObj = new BasicDBObject();
		updateDBObj.put("sname", "Sanjay");
		// updateDBObj.put("seolocation", "http://vijay.com");
		update("virtualurlmapping", insertDBObj, updateDBObj);

		DBObject deleteDBObj = new BasicDBObject();
		deleteDBObj.put("sname", "Ajay");
		// delete("virtualurlmapping",insertDBObj);

		DBObject findAndUpdateDBObj = new BasicDBObject();
		findAndUpdateDBObj.put("sname", "Ajay");
		// findAndUpdateDBObj.put("seolocation", "http://vijay.com");
		findAndUpdate("virtualurlmapping", insertDBObj, updateDBObj);

		System.out.println(result.toString());
	}*/

}
