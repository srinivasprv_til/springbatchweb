package com.times.common.dao;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.springframework.web.util.UriUtils;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.times.constants.CMSExceptionConstants;
import com.times.exception.CMSCallExceptions;

public class MongoRestAPIService {
	
	private static final Log log = LogFactory.getLog(MongoRestAPIService.class);
	
	/*
	 * Please do not change this URL this is for test environment.
	 */
	public static String MONGO_REST_API_URL = "http://mra.indiatimes.com/mra/";
	//public static String MONGO_REST_API_URL = "http://mratest.indiatimes.com/mra/";
	
	private static final String getURL = "get/#collection?query=#query";
	private static final String updateURL = "update/#collection?queryJson=#query&rowJson=#rowJson";
	private static final String insertURL = "insert/#collection?rowJson=#rowJson";
	private static final String deleteURL = "delete/#collection?query=#query";
	private static final String findandUpdateURL = "findAndUpdate/#collection?query=#query&rowJson=#rowJson";
	private static final String groupURL = "group/#collection?query=#query";
	private static final String distintURL= "distinct/#collection?query=#query&field=#field";
	
	// Url for insert data with POST request
	private static final String insertURL_post = "insert/#collection";
	private static final String updateURL_post = "update/#collection?queryJson=#query";
	
	private static final String countURL = "count/#collection?query=#query";
	
	private static final String addFileURL = "addFile/#collection";
	private static final String getFileURL = "getFile/#collection?query=#query";
	
	private static final String getAggregateUrl = "aggregate/#collection?query=#query";
	
	/*static HttpClientParams clientParams = new HttpClientParams();
	
	static{
		clientParams.setParameter(HttpClientParams.MAX_REDIRECTS, "5");
		clientParams.setParameter(HttpClientParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));
		clientParams.setConnectionManagerTimeout(120000);		
		clientParams.setSoTimeout(120000);
	}*/

	public static DBObject get(String collection, DBObject query) {
		String extData = null;
		String strURL = null;
		List<DBObject> dbObj = null;
		strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_URL + strURL;
		extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			Object dbObjectRes = JSON.parse(extData);
			if (dbObjectRes instanceof List<?>) {
				List<DBObject> dbObjects = (List<DBObject>) dbObjectRes;
				if (((List<DBObject>) dbObjectRes).size() > 0) {
					return ((List<DBObject>) dbObjectRes).get(0);
				}
			} else if (dbObjectRes instanceof DBObject) {
				return (DBObject) dbObjectRes;
			}
		}
		return null;
	}
	
	public static List<DBObject> getList(String collection, DBObject query) {
		String extData = null;
		String strURL = null;
		List<DBObject> dbObj = null;
		strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_URL + strURL;
		extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			Object dbObjectRes = JSON.parse(extData);
			if (dbObjectRes instanceof List<?>) {
					return ((List<DBObject>) dbObjectRes);
			} 
		}
		return null;
	}
	
	public static List<DBObject> getList(String collection, DBObject query, Map<String, Object> options) {
		String extData = null;
		String strURL = null;
		List<DBObject> dbObj = null;
		strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_URL + strURL + optiosParameter(options);
		extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			Object dbObjectRes = JSON.parse(extData);
			if (dbObjectRes instanceof List<?>) {
					return ((List<DBObject>) dbObjectRes);
			} 
		}
		return null;
	}

	private static String optiosParameter(Map<String, Object> options){
		StringBuilder builder = new StringBuilder();
		for(Map.Entry<String, Object> entry : options.entrySet()){
			builder.append("&"+entry.getKey()+"="+entry.getValue());
		}
		return builder.toString();
	}

	public static boolean insert(String collection, DBObject rowJson) {
		String strURL = null;
		strURL = insertURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))
				.replaceAll("#collection", collection);
		strURL = MONGO_REST_API_URL + strURL;
		
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}

	public static boolean update(String collection, DBObject queryJson,
			DBObject rowJson) {
		String strURL = null;
		strURL = updateURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))
				.replaceAll("#collection", collection).replaceAll("#query",
						Matcher.quoteReplacement(queryJson.toString()));
		strURL = MONGO_REST_API_URL + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}
	
	
	/**
	 * This API will update collection with using upser value.
	 * @param collection
	 * @param queryJson
	 * @param rowJson
	 * @param upsert
	 * @return
	 */
	public static boolean update(String collection, DBObject queryJson,
			DBObject rowJson, boolean upsert) {
		String strURL = null;
		strURL = updateURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))
				.replaceAll("#collection", collection).replaceAll("#query",
						Matcher.quoteReplacement(queryJson.toString()));
		strURL = strURL +  ", {upsert : "  + upsert + "}";
		strURL = MONGO_REST_API_URL + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}

	public static boolean delete(String collection, DBObject queryJson) {
		String strURL = null;
		strURL = deleteURL.replaceAll("#collection", collection).replaceAll(
				"#query", Matcher.quoteReplacement(queryJson.toString()));
		strURL = MONGO_REST_API_URL + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}

	public static boolean findAndUpdate(String collection, DBObject queryJson,
			DBObject rowJson) {
		String strURL = null;
		strURL = findandUpdateURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))
				.replaceAll("#collection", collection).replaceAll("#query",
						Matcher.quoteReplacement(queryJson.toString()));
		strURL = MONGO_REST_API_URL + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}
	
	public static List<DBObject> aggregateList(String collection, List<DBObject> query) {
		
		String extData = null;
		String strURL = null;
		List<DBObject> dbObj = null;
		
		String q = "";
		for(DBObject ob : query){
			
			q = q+ ","+ob.toString();
		}
		strURL = getAggregateUrl.replaceAll("#query", "["+Matcher.quoteReplacement(q.substring(1))+"]").replaceAll("#collection", collection);
		strURL = MONGO_REST_API_URL + strURL;
		extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			Object dbObjectRes = JSON.parse(extData);
			if (dbObjectRes instanceof List<?>) {
					return ((List<DBObject>) dbObjectRes);
			} 
		}
		return null;
	}
	
	public static Set<String> getDistinct (String collection,String field, DBObject query){
		
		String extData = null;
		String strURL = null;
		List<DBObject> dbObj = null;
		strURL = distintURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection).replaceAll("#field", field);
		strURL = MONGO_REST_API_URL + strURL;
		extData = getExtData(strURL);
		
		if(StringUtils.isNotBlank(extData)){
			extData=extData.substring(1,extData.length()-1);
			
			Set <String> distincts=new HashSet(Arrays.asList(extData.split(", ")));
			
			return distincts;
		}
		
		return null;
	}

	public static String getExtData(String strURL) {
		/*HttpClient hClient = new HttpClient();	
		HttpMethod gMethod = null;
		try {
			strURL = URIUtil.encodeQuery(strURL);
			this replce is to hanlde the life & style section to get msid
			strURL = strURL.replace("%20&%20", "+%26+");
			gMethod = new GetMethod(strURL);
			gMethod.setParams(clientParams);
			int status = hClient.executeMethod(gMethod);
			if (status == 200) {
				String extData = new String(gMethod.getResponseBody(), "UTF-8");
				return extData;
			}*/
		
		HttpRequestRetryHandler requestRetryHandler=new HttpRequestRetryHandler(){
			@Override
			public boolean retryRequest(IOException exception,
					int executionCount, HttpContext context) {
				// TODO Auto-generated method stub
				if (executionCount < 3) {
					return true;
				} else {
					return false;
				}
			}
		  };
		  
		
		HttpClient hClient = HttpClients.custom().setRetryHandler(requestRetryHandler).build();
		RequestConfig config = RequestConfig.custom().setMaxRedirects(5)
				.setConnectTimeout(40000).setSocketTimeout(40000).build();
		
		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try {
			strURL = strURL.replace("%20&%20", "+%26+");
			strURL = UriUtils.encodeQuery(strURL, "UTF-8");
			gMethod = new HttpGet(strURL);
			gMethod.setConfig(config);
			hResponse = hClient.execute(gMethod);
			int status = hResponse.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				String outData = new BasicResponseHandler().handleResponse(hResponse);
				return outData;
			} else {
				log.debug("Url : " + strURL + " & Status : " + status);
			}
		} catch (Exception e) {
			/*new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage()
					+ " with paramsMap ", CMSExceptionConstants.CMS_Exception, e);*/
			//CMSCallExceptions cmsCall = new CMSCallExceptions(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage() + " with paramsMap ", CMSExceptionConstants.CMS_Exception, e);
			log.error(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage()	+ " with paramsMap " + e.getMessage());
		} finally {
			/*if(gMethod != null){
				gMethod.releaseConnection();
				gMethod = null;
			}*/
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	}
	
	public static long getCount(String collection, DBObject query) {
		String extData = null;
		String strURL = null;
		
		strURL = countURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_URL + strURL;
		extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			Object dbObjectRes = JSON.parse(extData);
			return  Integer.parseInt(dbObjectRes.toString());
		}
		return 0;
	}
	
	public static DBObject group(String collection, DBObject query, Map<String, Object> options) {
		String strURL = null;
		strURL = groupURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_URL + strURL + optiosParameter(options);
		String extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			DBObject dbObjectRes = (DBObject) JSON.parse(extData);
			return dbObjectRes;
		}
		return null;
	}

	/*public static void main(String args[]) throws IOException {

		DBObject query = new BasicDBObject();
		query.put("seolocation", "http://localhost/home/education/entrance-exams");
		DBObject result = get("virtualurlmapping", query);

		DBObject insertDBObj = new BasicDBObject();
		insertDBObj.put("sname", "Ajay");
		insertDBObj.put("seolocation", "http://ajay.com");
		insert("virtualurlmapping", insertDBObj);

		DBObject updateDBObj = new BasicDBObject();
		updateDBObj.put("sname", "Sanjay");
		// updateDBObj.put("seolocation", "http://vijay.com");
		update("virtualurlmapping", insertDBObj, updateDBObj);

		DBObject deleteDBObj = new BasicDBObject();
		deleteDBObj.put("sname", "Ajay");
		// delete("virtualurlmapping",insertDBObj);

		DBObject findAndUpdateDBObj = new BasicDBObject();
		findAndUpdateDBObj.put("sname", "Ajay");
		// findAndUpdateDBObj.put("seolocation", "http://vijay.com");
		findAndUpdate("virtualurlmapping", insertDBObj, updateDBObj);

		System.out.println(result.toString());
	}*/
	
	// Inserting data in DB with POST request
	public static boolean insertWithPost(String collection, DBObject rowJson) {
		
		String strURL = null;
		
		// Add collection name in url
		strURL = insertURL_post.replaceAll("#collection", collection);
		
		// Get MRA url
		strURL = MONGO_REST_API_URL + strURL;
		
		// Post data
		if (postData(strURL,  rowJson) != null) {
			return true;
		}
		
		return false;
	}
	
	// Updating data in DB with POST request
	public static boolean updateWithPost(String collection, DBObject queryJson,	DBObject rowJson, boolean upsert) {
		String strURL = null;
        
        strURL = updateURL_post.replaceAll("#collection", collection).replaceAll("#query", Matcher.quoteReplacement(queryJson.toString()));
        
        strURL = strURL +  ", {upsert : "  + upsert + "}";
        
        strURL = MONGO_REST_API_URL + strURL;
        if (postData(strURL, rowJson) != null) {
               return true;
        }
       
        return false;
	}
	
	public static boolean updateWithPost(String collection, DBObject queryJson,	DBObject rowJson) {
		 String strURL = null;
        //strURL = insertURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))       .replaceAll("#collection", collection);
        strURL = updateURL_post.replaceAll("#collection", collection).replaceAll("#query", Matcher.quoteReplacement(queryJson.toString()));
        //strURL = strURL + ", {upsert :true}";
        strURL = MONGO_REST_API_URL + strURL;
        if (postData(strURL, rowJson) != null) {
               return true;
        }
       
        return false;
	}
	
	// POST data insert
	private static String postData(String strURL, DBObject content) {
		
		// Get http client
		HttpClient httpClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
		
		// Create request config parameters
		RequestConfig config = RequestConfig.custom().setMaxRedirects(2).setConnectTimeout(5000).setSocketTimeout(5000).build();
		
		// Post object
		HttpPost httpPost = null;
		
		// Response object
		HttpResponse httpResponse = null;
		
		try {
			// Encode Url
			strURL = UriUtils.encodeQuery(strURL, "UTF-8");
			
			// Create post request
			httpPost = new HttpPost(strURL);
			
			// Set configuration
			httpPost.setConfig(config);
			
			// List for query parameters to set in post entity
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			
			// Set content of dbObject as parameter 
			params.add(new BasicNameValuePair("rowJson", content.toString()));
			
			// Create entity with above parameters
			HttpEntity entity = new UrlEncodedFormEntity(params);
			
			// Set entity in post request
			httpPost.setEntity(entity);
			
			// Get response
			httpResponse = httpClient.execute(httpPost);
			
			log.info("Response for inserting with post in DB: "+ httpResponse!=null ? httpResponse.toString(): httpResponse);
			
			String extData = EntityUtils.toString(entity, "UTF-8");
			
			return extData;
			
		} catch (Exception e) {
			
			log.error(strURL+" - Exception Occured in get the external data Exception : "+ e.getMessage() + " with paramsMap ", e);
			
			new CMSCallExceptions(strURL+ " - Exception Occured in get the external data Exception : "+ e.getMessage() + " with paramsMap ",
									CMSExceptionConstants.CMS_Exception, e);
		} finally {
			
			HttpClientUtils.closeQuietly(httpClient);
			HttpClientUtils.closeQuietly(httpResponse);
		}
		return null;
	}
	
	
	public static String addFile(String collection, File file) {
		String strURL = null;
		strURL = addFileURL.replaceAll("#collection", collection);
		strURL = MONGO_REST_API_URL + strURL;
		String res = postFileCall(strURL, file);
		if (res != null) {
			return res;
		}

		return null;
	}
	
	
	public static byte[] getFile(String collection, DBObject query) {
		String strURL = null;
		strURL = getFileURL.replaceAll("#collection", collection).replaceAll("#query", Matcher.quoteReplacement(query.toString()));
		strURL = MONGO_REST_API_URL + strURL;
		ByteArrayOutputStream b = getFileCall(strURL);
		return b.toByteArray();
		/*FileOutputStream fos;
		try {
			 fos = new FileOutputStream (new File("myFile"));
			 ByteArrayOutputStream baos = new ByteArrayOutputStream();
  		     baos.writeTo(fos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */
		
	   
	}
	
	
	private static String postFileCall(String strURL, File content) {

		// Get http client
		HttpClient httpClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();

		// Create request config parameters
		//RequestConfig config = RequestConfig.custom().setMaxRedirects(2).setConnectTimeout(5000).setSocketTimeout(5000).build();

		// Post object
		HttpPost httpPost = null;

		// Response object
		HttpResponse httpResponse = null;

		try {

			// Create post request
			httpPost = new HttpPost(strURL);

			HttpEntity reqEntity = MultipartEntityBuilder.create()
					.addPart("file", new FileBody(content)).addTextBody("fileName",content.getName()).build();


			httpPost.setEntity(reqEntity);

			// Get response
			httpResponse = httpClient.execute(httpPost);

			log.info("Response for inserting with post in DB: "+ httpResponse!=null ? httpResponse.toString(): httpResponse);

			String data = IOUtils.toString(httpResponse.getEntity().getContent());

			return data;

		} catch (Exception e) {

			log.error(strURL+" - Exception Occured in get the external data Exception : "+ e.getMessage() + " with paramsMap ", e);

			new CMSCallExceptions(strURL+ " - Exception Occured in get the external data Exception : "+ e.getMessage() + " with paramsMap ",
					CMSExceptionConstants.CMS_Exception, e);
		} finally {

			HttpClientUtils.closeQuietly(httpClient);
			HttpClientUtils.closeQuietly(httpResponse);
		}
		return null;
	}
	
	
	public static ByteArrayOutputStream getFileCall(String strURL) {
	
		HttpRequestRetryHandler requestRetryHandler=new HttpRequestRetryHandler(){
			@Override
			public boolean retryRequest(IOException exception,
					int executionCount, HttpContext context) {
				// TODO Auto-generated method stub
				if (executionCount < 3) {
					return true;
				} else {
					return false;
				}
			}
		  };
		  
		
		HttpClient hClient = HttpClients.custom().setRetryHandler(requestRetryHandler).build();
		RequestConfig config = RequestConfig.custom().setMaxRedirects(5)
				.setConnectTimeout(40000).setSocketTimeout(40000).build();
		
		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try {
			strURL = strURL.replace("%20&%20", "+%26+");
			strURL = UriUtils.encodeQuery(strURL, "UTF-8");
			gMethod = new HttpGet(strURL);
			gMethod.setConfig(config);
			hResponse = hClient.execute(gMethod);
			int status = hResponse.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_OK) {
				HttpEntity entity = hResponse.getEntity();
			    ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
			    entity.writeTo(baos);
				return baos;
			} else {
				log.debug("Url : " + strURL + " & Status : " + status);
			}
		} catch (Exception e) {
		
			log.error(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage()	+ " with paramsMap " + e.getMessage());
		} finally {
		
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	}
	
}
