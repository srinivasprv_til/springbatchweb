package com.times.common.dao;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.times.common.model.IBeatEntity;
import com.times.common.service.FileContentServices;

public class IBeatDAOImpl implements IBeatDAO {

	private static final String DATA="DATA";
	private static final String VIEWS="Views";
	private static final String COMMENTS= "Comments";
	private static final String SOCIAL="Social";

	//Map of host name to ibeat key
	private Map<String,String> HOST_KEY_MAP;

	private FileContentServices fileContentServices;

	private static final Logger logger = LoggerFactory.getLogger(IBeatDAOImpl.class);


	@Override
	public List<IBeatEntity> getLastHourData(String hostName) {

		String extDataString=getIbeatLastHourData(hostName);

		List<IBeatEntity> ibeatEntityList=null;

		if (StringUtils.isNotBlank(extDataString)) {

			JSONObject ibeatDataJSON=null;

			try {
				ibeatDataJSON = new JSONObject(extDataString);
			} catch (JSONException e) {
				logger.error("Exception in converting extData to JSONObject  extData={}",String.valueOf(ibeatDataJSON),e);
			}

			if(ibeatDataJSON!=null && ibeatDataJSON.getJSONObject(DATA)!=null){

				ibeatEntityList=getIbeatListFromJSON(ibeatDataJSON.getJSONObject(DATA));
			}


		}

		return ibeatEntityList;
	}

	
	private List<IBeatEntity> getIbeatListFromJSON(JSONObject ibeatDataJSON){

		List<IBeatEntity> ibeatEntitesList=null;

		if(ibeatDataJSON!=null){

			ibeatEntitesList=new ArrayList<IBeatEntity>();

			Iterator <String> jsonItr = ibeatDataJSON.keys();

			while(jsonItr.hasNext()){

				String msidString=jsonItr.next();

				int msid=0;

				try{
					msid=Integer.parseInt(msidString);
				}catch(NumberFormatException nfe){
					logger.error("Invalid msid={}",String.valueOf(msidString));
				}

				JSONObject statsJSON=ibeatDataJSON.getJSONObject(msidString);

				IBeatEntity iBeatEntity = convertJSONToIbeatEntity(statsJSON,msid);

				if(iBeatEntity!=null){
					ibeatEntitesList.add(iBeatEntity);
				}
			}

		}

		return ibeatEntitesList;
	}

	/**
	 * Sample JSON: {"Views": 66844,"Comments": 0,"Social": 0} <br>
	 * @param statsJSON
	 * @param msid
	 * @return
	 */
	private IBeatEntity convertJSONToIbeatEntity(JSONObject statsJSON, int msid){

		IBeatEntity iBeatEntity=null;

		if(statsJSON!=null && msid>0){

			iBeatEntity= new IBeatEntity();

			iBeatEntity.setMsid(msid);

			if(statsJSON.get(VIEWS)!=null){
				iBeatEntity.setViews(statsJSON.getLong(VIEWS));
			}

			if(statsJSON.get(COMMENTS)!=null){
				iBeatEntity.setComments(statsJSON.getLong(COMMENTS));
			}

			if(statsJSON.get(SOCIAL)!=null){
				iBeatEntity.setSocial(statsJSON.getLong(SOCIAL));
			}

		}

		return iBeatEntity;

	}

	private String getIbeatLastHourData(String hostName){

		String extData=null;
		URIBuilder lastHourDataURL=null;
		try {
			lastHourDataURL = new URIBuilder(LAST_HOUR_UPDATES_API);
		} catch (URISyntaxException e) {

		}

		if (StringUtils.isNotBlank(hostName) && HOST_KEY_MAP.containsKey(hostName)) {
			lastHourDataURL.addParameter("host", hostName);
			lastHourDataURL.addParameter("k", HOST_KEY_MAP.get(hostName));

			String url=lastHourDataURL.toString();

			extData=fileContentServices.getExtData(url);
		}


		return extData;
	}

	
	public FileContentServices getFileContentServices() {
		return fileContentServices;
	}

	public void setFileContentServices(FileContentServices fileContentService) {
		this.fileContentServices = fileContentService;
	}


	public Map<String, String> getHOST_KEY_MAP() {
		return HOST_KEY_MAP;
	}


	public void setHOST_KEY_MAP(Map<String, String> hOST_KEY_MAP) {
		HOST_KEY_MAP = hOST_KEY_MAP;
	}




}
