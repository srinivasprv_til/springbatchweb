package com.times.common.dao;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Map.Entry;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import org.jdom2.Element;
import org.jdom2.Document;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPath;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.xml.sax.InputSource;

import com.times.common.util.EhcacheBatchUtil;
import com.times.common.util.ExternalHttpUtil;



public class EhcacheBatchDaoImpl implements EhcacheBatchDaoInterface {

	private SimpleJdbcTemplate jdbcTemplate;
	
	private static final Logger log = LoggerFactory.getLogger(EhcacheBatchDaoImpl.class);
	
	public EhcacheBatchDaoImpl(DataSource ds) {
		this.jdbcTemplate = new SimpleJdbcTemplate(ds);
	}
	
	/**
	 * @author Geetanjali.Tiwari
	 * 
	 * @param City Guide msid are required
	 * @return 
	 */
	
	public List<Map<String, Object>> getdbData(String msid, String type){
		String sql = "";
		if("guide".equals(type)){
			sql = "select h.mas_msid, h.hostid, m.msid, m.mstype, m.mssubtype, h.hisactive, msname, wb.refcontent_msid as multipublishmsid, ap.photodatalength from hierarchy h(nolock), masters m(nolock), wbcms_overridelink_lookup wb(nolock), ArticlePhotos ap " + 
					" where h.mas_msid in ("+msid+") and h.hostid=259 and h.hisactive=2 and h.msid=m.msid and m.msid *= wb.msid and m.msid *= ap.msid and wb.hostid=259  ";
		}else if("article".equals(type)){
			sql =  "select a.msid, art_title, artntext, c.agency_id, c.auid, art_date, AUEmail, AUNAME, AGENCY_NAME from articles a, content c, agency ag, author au where c.msid in  ("+msid+") and c.msid=a.msid and c.agency_id *=ag.agency_id and c.auid *= au.auid";
		}else if("refmsid".equals(type)){
			sql = "select h.mas_msid, h.hostid, m.msid, m.mstype, m.mssubtype, h.hisactive,  msname from hierarchy h(nolock), masters m(nolock) " + 
					" where h.msid in ("+msid+") and h.hostid=259 and h.hisactive=2 and h.msid=m.msid ";
		}else if("image".equals(type)){
			sql =  "select h.mas_msid as msid, ap.msid as slideid, h.hname, h.hisactive,  ag.AGENCY_id, ag.AGENCY_NAME from hierarchy h (nolock), ArticlePhotos ap (nolock), content c (nolock), agency ag (nolock)"
					+ " where h.mas_msid in ("+msid+") and h.msid=ap.msid and ap.msid=c.msid  and c.agency_id *=ag.agency_id";
		}else if("artimage".equals(type)){
			sql =  "select h.mas_msid as msid, h.hisactive,  ap.msid as slideid, h.hname from hierarchy h (nolock), ArticlePhotos ap (nolock) "
					+ " where h.mas_msid in ("+msid+") and h.msid=ap.msid ";
		}else if("metadata".equals(type)){
			sql = "select md.msid, md.metatype,mtmdesc, omdntext,OMDDateTime,OMDNumber,OMDChar, mtmdatatype from metadata md,metatpmaster mm"
					+ " where md.msid in ("+msid+") and md.metatype = mm.metatype";
		}else if("agency".equals(type)){
			sql = "select  agency_id as agencyid,AGENCY_NAME as agencyname ,ISNULL(adrulesxml, '') as adrulesxml "
					+  "from agency where agency_id in ("+msid+") ";
		}
		
		List<Map<String, Object>> lst = null;
		try{
			if(!"".equals(sql)){
				lst = this.jdbcTemplate.queryForList(sql);
			}else{
				lst = null;
			}
		}catch(Exception e){
			lst = null;
		}
		return lst;
	}	
}
