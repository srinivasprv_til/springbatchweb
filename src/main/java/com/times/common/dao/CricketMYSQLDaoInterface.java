package com.times.common.dao;

import java.util.List;
import java.util.Map;

import org.jdom2.Element;

public interface CricketMYSQLDaoInterface {
	
	public int insertMatchID(String matchid, String status);
	public int updateMatchID(String matchid, String status);
	public int selectMatchID(String matchid);

}
