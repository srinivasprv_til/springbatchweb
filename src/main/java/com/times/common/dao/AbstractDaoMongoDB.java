package com.times.common.dao;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.mongodb.MongoOptions;
import com.mongodb.ServerAddress;
/**
 * This is abstract DAO for MongoDB common stuff, which get when this abstract class extends.
 * 
 * @author Ranjeet.Jha
 *
 */
public abstract class AbstractDaoMongoDB {

	private final static Logger logger = Logger.getLogger(AbstractDaoMongoDB.class.getName());
	
	private int port;
	private String DB;
	private int connectionsPerHost;
	private int threadsAllowedToBlockForConnectionMultiplier;
	private int socketTimeout;
	
	private String dbMachine;
	
	/**
	 * Connects primarily to master.
	 */
	private Mongo masterMongo;
	
	/**
	 * can connect to slave. For readonly purpose.
	 */
	private Mongo slaveMongo;

	/**
	 * Initializing mongoDB related stuff.
	 */
	public void init() {
		//setMongoProperties();
	
		try {
			ArrayList<ServerAddress> addr = new ArrayList<ServerAddress>();
			for (String s : dbMachine.split(",")) {
				addr.add(new ServerAddress(s + ":" + port));
			}
			MongoOptions mongoOptions = new  MongoOptions();
			mongoOptions.autoConnectRetry = true;
			mongoOptions.connectionsPerHost = connectionsPerHost;
			mongoOptions.threadsAllowedToBlockForConnectionMultiplier = threadsAllowedToBlockForConnectionMultiplier;
			mongoOptions.socketTimeout = socketTimeout;
			
			/*
			 * if you run mongodb in auth mode then... 
			 *
			 */
			//boolean auth = db.authenticate("userid", "password".toCharArray());
			
			masterMongo = new Mongo(addr,mongoOptions);
			slaveMongo = new Mongo(addr, mongoOptions);
			slaveMongo.slaveOk();
		} catch (UnknownHostException e) {
			logger.log(null, e.getMessage());
		} catch (MongoException e) {
			logger.log(null, e.getMessage());
		}
	}

	/**
	 * killing daemon thread initialised by mongo.
	 */
	
	public void destroy() {
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			logger.log(null, e.getMessage());
		} finally {
			if (masterMongo != null) {
				masterMongo.close();
			}
			if (slaveMongo != null) {
				slaveMongo.close();
			}
		}

	}

	/**
	 * This method is used to release the resources related to MongoDB, that occupied during the activity.
	 * 
	 * @param jcmsDB
	 */
	public void releaseResources(DB jcmsDB) {
		if (jcmsDB != null) {
			try {
				jcmsDB.resetError();
				jcmsDB.requestDone();
			} catch (Exception e) {
				logger.log(null, e.getMessage());
			}
		}
	}
	
	/**
	 * @return
	 */
	public String getDbMachine() {
		return dbMachine;
	}


	/**
	 * @param dbMachine
	 */
	public void setDbMachine(String dbMachine) {
		this.dbMachine = dbMachine;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the dB
	 */
	public String getDB() {
		return DB;
	}

	/**
	 * @param dB the dB to set
	 */
	public void setDB(String dB) {
		DB = dB;
	}

	/**
	 * @return the connectionsPerHost
	 */
	public int getConnectionsPerHost() {
		return connectionsPerHost;
	}

	/**
	 * @param connectionsPerHost the connectionsPerHost to set
	 */
	public void setConnectionsPerHost(int connectionsPerHost) {
		this.connectionsPerHost = connectionsPerHost;
	}

	/**
	 * @return the threadsAllowedToBlockForConnectionMultiplier
	 */
	public int getThreadsAllowedToBlockForConnectionMultiplier() {
		return threadsAllowedToBlockForConnectionMultiplier;
	}

	/**
	 * @param threadsAllowedToBlockForConnectionMultiplier the threadsAllowedToBlockForConnectionMultiplier to set
	 */
	public void setThreadsAllowedToBlockForConnectionMultiplier(int threadsAllowedToBlockForConnectionMultiplier) {
		this.threadsAllowedToBlockForConnectionMultiplier = threadsAllowedToBlockForConnectionMultiplier;
	}

	/**
	 * @return the socketTimeout
	 */
	public int getSocketTimeout() {
		return socketTimeout;
	}

	/**
	 * @param socketTimeout the socketTimeout to set
	 */
	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	/**
	 * @return the masterMongo
	 */
	public Mongo getMasterMongo() {
		return masterMongo;
	}

	/**
	 * @param masterMongo the masterMongo to set
	 */
	public void setMasterMongo(Mongo masterMongo) {
		this.masterMongo = masterMongo;
	}

	/**
	 * @return the slaveMongo
	 */
	public Mongo getSlaveMongo() {
		return slaveMongo;
	}

	/**
	 * @param slaveMongo the slaveMongo to set
	 */
	public void setSlaveMongo(Mongo slaveMongo) {
		this.slaveMongo = slaveMongo;
	}
	
}
