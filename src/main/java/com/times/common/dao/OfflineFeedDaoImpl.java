package com.times.common.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.constant.UserFeedConstant;
import com.times.common.model.UserActivity;
import com.times.common.model.UserComment;
import com.times.common.model.UserFeed;
import com.times.common.util.OfflineFeedUtilities;
 
public class OfflineFeedDaoImpl implements OfflineFeedDao {
	public static final String OFFLINE_USER_FEED_COLLECTION = "offlineuserfeed";
	
	
	public List<UserFeed> getUserFeedList() throws Exception{
		DBObject userFeedDBObject = new BasicDBObject();
		userFeedDBObject.put(UserFeedConstant.STATUS, UserFeedConstant.STATUSVAL);	
		
		List<UserFeed> userFeedList = new ArrayList<UserFeed>();
		List<DBObject> dbObjectList = MongoRestAPIService.getList(OFFLINE_USER_FEED_COLLECTION,userFeedDBObject);
		
		for(DBObject dbObject : dbObjectList ){
			userFeedList.add(OfflineFeedUtilities.getDBObjectByUserFeed(dbObject));
		}
		return userFeedList;
	}
	
	public boolean updateStatus(UserFeed userFeed) throws Exception{
		boolean updstatus = false;
		updstatus = MongoRestAPIService.findAndUpdate(OFFLINE_USER_FEED_COLLECTION, getDBObjectByUserFeed(userFeed.getUserId(),userFeed.getId()), updateDBObjectByUserFeed());
		return updstatus;
	}

	private DBObject getDBObjectByUserFeed(String userid, String rowid) throws Exception {
		DBObject userFeedDBObject = new BasicDBObject();
		userFeedDBObject.put(UserFeedConstant.USERID,userid);
		userFeedDBObject.put(UserFeedConstant.ID,new BasicDBObject("$oid",rowid));
		return userFeedDBObject;
	}
	
	private DBObject updateDBObjectByUserFeed() throws Exception {
		BasicDBObject userFeedDBObject = new BasicDBObject();
		userFeedDBObject.append("$set", new BasicDBObject(UserFeedConstant.UPDATEDDATE, new Date()));
		userFeedDBObject.append("$set", new BasicDBObject(UserFeedConstant.STATUS, UserFeedConstant.STATUSVALUPD));
		return userFeedDBObject;
	}	
	
}

