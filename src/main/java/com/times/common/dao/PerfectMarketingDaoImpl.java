package com.times.common.dao;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.times.common.model.PerfectMarketingObject;
import com.times.common.wrapper.PerfectMarketingDataWrapper;

public class PerfectMarketingDaoImpl implements PerfectMarketingDAO {
	
	private static final Logger log = LoggerFactory.getLogger(PerfectMarketingDaoImpl.class);

	public static final String PM_RESULT_COLLECTION = "perfectm_result";
	public static final String PM_UNSUCESSFUL_COLLECTION = "perfectm_unsuccessful";
	public static final String PM_FAILED_COLLECTION = "perfectm_failed";

	private SimpleJdbcTemplate jdbcTemplate;

	public PerfectMarketingDaoImpl(DataSource ds) {
		this.jdbcTemplate = new SimpleJdbcTemplate(ds);
	}

	public Map getTopMsid(String lastMsid) {
		
		String sql = "";
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (lastMsid == null || lastMsid.equals("")) {
			sql = "select top 100 m.msid,c.art_Date from hierarchy h(nolock), masters m(nolock), content c(nolock) where m.msid=c.msid and h.msid =m.msid and mas_msid in (select msid from activednhierarchy h(nolock) where adnversionno = (select max(adnversionno)from activednhierarchy where hostid=153) and ((mstype in(1,25,33,37)) or (mstype=8 and mssubtype=0) ) and isactive=2) and hisactive=2 and m.mstype =2 and h.hostid=153 order by m.msid";
		} else {
//			sql = "select top 10 m.msid,c.art_Date from hierarchy h(nolock), masters m(nolock), content c(nolock) where m.msid=c.msid and h.msid =m.msid and mas_msid in (select msid from activednhierarchy h(nolock) where adnversionno = 12609 and isactive=2) and m.msid > #msid and m.mstype =2 and h.hostid=153 and c.art_date > '2010-01-01' order by m.msid";
			sql = "select top 100 m.msid,c.art_Date from hierarchy h(nolock), masters m(nolock), content c(nolock) where m.msid=c.msid and h.msid =m.msid and m.msid > #msid and m.mstype =2 and h.hostid in (19,153) order by m.msid";
			sql = sql.replace("#msid", lastMsid);
		}
		Map<String, Date> pmMap = new LinkedHashMap<String, Date>();

		Map<String, Integer> params = new HashMap<String, Integer>();

		List<Map<String, Object>> idsList = this.jdbcTemplate.queryForList(sql,
				params);

		if (idsList != null && idsList.size() > 0) {
			for (Map<String, Object> rs : idsList) {
				String msid =  String.valueOf(rs.get("msid"));
				
				try {
					Timestamp t = (Timestamp) rs.get("art_Date");
					String sDate = t.toString();
					Date date = dateFormat.parse(sDate);
					pmMap.put(msid, date);
				} catch (Exception e) {
					log.error("SQL Exception occurred in getTopMsid with error = " + e.getMessage());
				}
			}
		}
		return pmMap;
	}
	
	public Map getTopMsidOld(String lastMsid) {
		
		String sql = "";
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (lastMsid == null || lastMsid.equals("")) {
			sql = "select top 100 m.msid,c.art_Date from hierarchy h(nolock), masters m(nolock), content c(nolock) where m.msid=c.msid and h.msid =m.msid and mas_msid in (select msid from activednhierarchy h(nolock) where adnversionno = (select max(adnversionno)from activednhierarchy where hostid=153) and ((mstype in(1,25,33,37)) or (mstype=8 and mssubtype=0) ) and isactive=2) and hisactive=2 and m.mstype =2 and h.hostid=153 order by m.msid";
		} else {
			sql = "select top 20 m.msid,c.art_Date from hierarchy h(nolock), masters m(nolock), content c(nolock) where m.msid=c.msid and h.msid =m.msid and"
					+ " hisactive=2 "
					+ "and m.msid > #msid and m.mstype =2 and and m.mssubtype=0 and h.hostid=153 and c.art_date > '2001-01-01' and c.art_date < '2002-01-01' order by m.msid";
			sql = sql.replace("#msid", lastMsid);
		}
		Map<String, Date> pmMap = new LinkedHashMap<String, Date>();

		Map<String, Integer> params = new HashMap<String, Integer>();

		List<Map<String, Object>> idsList = this.jdbcTemplate.queryForList(sql,
				params);

		if (idsList != null && idsList.size() > 0) {
			for (Map<String, Object> rs : idsList) {
				String msid =  String.valueOf(rs.get("msid"));
				Timestamp t = (Timestamp) rs.get("art_Date");
				String sDate = t.toString();
				try {
					Date date = dateFormat.parse(sDate);
					pmMap.put(msid, date);
				} catch (ParseException e) {
					log.error("SQL Exception occurred in getTopMsid with error = " + e.getMessage());
				}
			}
		}
		return pmMap;
	}
	
	public Map getLastProcessedMsid(String lastMsid , String sectionId) {
		
		String sql = "";
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		
			sql = "select top 20 m.msid,c.art_Date from hierarchy h(nolock), masters m(nolock), content c(nolock) "
					+ "where m.msid=c.msid and h.msid =m.msid "
					+ "and mas_msid = " + sectionId
					+ "and hisactive=2 and m.msid > #msid and m.mstype =2 and h.hostid=153  order by m.msid";
			sql = sql.replace("#msid", lastMsid);
		
		Map<String, Date> pmMap = new LinkedHashMap<String, Date>();

		Map<String, Integer> params = new HashMap<String, Integer>();

		List<Map<String, Object>> idsList = this.jdbcTemplate.queryForList(sql,
				params);

		if (idsList != null && idsList.size() > 0) {
			for (Map<String, Object> rs : idsList) {
				String msid =  String.valueOf(rs.get("msid"));
				Timestamp t = (Timestamp) rs.get("art_Date");
				String sDate = t.toString();
				try {
					Date date = dateFormat.parse(sDate);
					pmMap.put(msid, date);
				} catch (ParseException e) {
					log.error("SQL Exception occurred in getTopMsid with error = " + e.getMessage());
				}
			}
		}
		return pmMap;
	}
	
	public int getTotalActiveStoriesInSection (String sectionMsid){
		String sql = "select count(*) from masters m, hierarchy h where h.mas_msid = " + sectionMsid +
				"and m.mstype=2 and m.mssubtype=0 and h.msid=m.msid and h.hisactive=2 and hostid=:hostid";
		Map<String,String> args = new HashMap<String, String>();
		args.put("hostid", "153");
		return this.jdbcTemplate.queryForInt(sql, args);
	}

	public boolean addPMObject(PerfectMarketingObject pmObj, String collection) {
		try {
		DBObject cmsDBObj = PerfectMarketingDataWrapper
				.getDBObjectByPerfectMarketingObject(pmObj);
		
			return MongoRestAPIService.insert(collection, cmsDBObj);
		} catch (MongoException e) {
			log.error("Exception occurred in addPMObject with error = " + e.getMessage());
			return false;
		} catch (Exception e) {
			log.error("Exception occurred in addPMObject with error = " + e.getMessage());
			return false;
		}

	}

	public PerfectMarketingObject getPMObject(String msid, String collection) {
		DBObject query = new BasicDBObject();
		query.put(PerfectMarketingObject.MSID, Integer.valueOf(msid));

		try {
			List<DBObject> pmResult = MongoRestAPIService.getList(collection,
					query);
			Iterator<DBObject> itr = pmResult.iterator();
			DBObject dbo = null;
			while (itr.hasNext()) {
				dbo = itr.next();
			}
			if (dbo != null) {
				return PerfectMarketingDataWrapper.getPMObject(dbo);
			} else {
				return null;
			}
		} catch (MongoException e) {
			log.error("Exception occurred in getPMObject with error = " + e.getMessage());  return null;
		} catch (Exception e) {
			log.error("Exception occurred in getPMObject with error = " + e.getMessage());  return null;
		}

	}

	public DBObject getPMDBObject(String msid, String collection, String pmUrl) {
		DBObject query = new BasicDBObject();
		query.put(PerfectMarketingObject.MSID, Integer.valueOf(msid));
		query.put(PerfectMarketingObject.REDIRECT_URL, pmUrl);

		try {
			List<DBObject> pmResult = MongoRestAPIService.getList(collection,
					query);
			Iterator<DBObject> itr = pmResult.iterator();
			DBObject dbo = null;
			while (itr.hasNext()) {
				dbo = itr.next();
			}
			if (dbo != null) {
				return dbo;
			} else {
				return null;
			}
		} catch (MongoException e) {
			log.error("Exception occurred in getPMDBObject with error = " + e.getMessage());  return null;
		} catch (Exception e) {
			log.error("Exception occurred in getPMDBObject with error = " + e.getMessage());  return null;
		}

	}

	public void updatePMObject(PerfectMarketingObject pmObj,
			PerfectMarketingObject pmUpdateObj, String collection) {
		try {
		DBObject cmsDBObj = PerfectMarketingDataWrapper
				.getDBObjectByPerfectMarketingObject(pmObj);
		DBObject cmsUpdateDBObj = PerfectMarketingDataWrapper
				.getDBObjectByPerfectMarketingObject(pmUpdateObj);
			MongoRestAPIService.findAndUpdate(collection, cmsDBObj,
					cmsUpdateDBObj);
		} catch (MongoException e) {
			log.error("Exception occurred in updatePMObject with error = " + e.getMessage());
		} catch (Exception e) {
			log.error("Exception occurred in updatePMObject with error = " + e.getMessage());
		}

	}

	public void updatePMObjectByMsid(String msid,
			PerfectMarketingObject pmUpdateObj, String collection) {
		try {
		DBObject query = new BasicDBObject();
		query.put(PerfectMarketingObject.MSID, Integer.valueOf(msid));
		DBObject cmsUpdateDBObj = PerfectMarketingDataWrapper
				.getDBObjectByPerfectMarketingObject(pmUpdateObj);
			MongoRestAPIService
					.findAndUpdate(collection, query, cmsUpdateDBObj);
		} catch (MongoException e) {
			log.error("Exception occurred in updatePMObjectByMsid with error = " + e.getMessage());
		} catch (Exception e) {
			log.error("Exception occurred in updatePMObjectByMsid with error = " + e.getMessage());
		}
	}

	public int getSerialNo(String collection) {
		DBObject query = new BasicDBObject();
		query.put("C_N", collection);

		try {
			List<DBObject> pmResult = MongoRestAPIService.getList("perfectm_serial_no",
					query);
			Iterator<DBObject> itr = pmResult.iterator();
			DBObject dbo = null;
			while (itr.hasNext()) {
				dbo = itr.next();
			}
			if (dbo != null) {
				return (Integer) dbo.get("S_N");
			} else {
				createSerialNo(collection);
				return 0;
			}
		} catch (MongoException e) {
			log.error("Exception occurred in getSerialNo with error = " + e.getMessage()); 
			return 0;
		} catch (Exception e) {
			log.error("Exception occurred in getSerialNo with error = " + e.getMessage());
			return 0;
		}

	}

	public void updateSerialNo(String collection, int serialNo) {
		DBObject query = new BasicDBObject();
		query.put("C_N", collection);

		DBObject updateQuery = new BasicDBObject();
		updateQuery.put("C_N", collection);
		updateQuery.put("S_N", serialNo);
		try {
			MongoRestAPIService.findAndUpdate("perfectm_serial_no", query, updateQuery);
		} catch (MongoException e) {
			log.error("Exception occurred in updateSerialNo with error = " + e.getMessage()); 
		} catch (Exception e) {
			log.error("Exception occurred in updateSerialNo with error = " + e.getMessage());
		}

	}

	public void createSerialNo(String collection) {
		DBObject query = new BasicDBObject();
		query.put("C_N", collection);
		query.put("S_N", 1);
		try {
			MongoRestAPIService.insert("perfectm_serial_no", query);
		} catch (MongoException e) {
			log.error("Exception occurred in createSerialNo with error = " + e.getMessage());
		} catch (Exception e) {
			log.error("Exception occurred in createSerialNo with error = " + e.getMessage());
		}

	}

	public Date getTopDate() {
		DBObject query = new BasicDBObject();
		BasicDBObject orderBy = new BasicDBObject();
		orderBy.put(PerfectMarketingObject.DATE_ADDED, -1);
		DBObject keys = new BasicDBObject();
		keys.put(PerfectMarketingObject.MSID, 1);
		keys.put(PerfectMarketingObject.DATE_ADDED, 1);
		Map<String, Object> options = new HashMap();
		options.put("key", keys);
		options.put("sort", orderBy);
		options.put("limit", 1);

		try {
			List<DBObject> pmResult = MongoRestAPIService.getList(
					PM_RESULT_COLLECTION, query, options);
			Iterator<DBObject> itr = pmResult.iterator();
			DBObject dbo = null;
			while (itr.hasNext()) {
				dbo = itr.next();
			}
			if (dbo != null) {
				return (Date) dbo.get(PerfectMarketingObject.DATE_ADDED);
			} else {
				return null;
			}
		} catch (MongoException e) {
			log.error("Exception occurred in getTopDate with error = " + e.getMessage());  return null;
		} catch (Exception e) {
			log.error("Exception occurred in getTopDate with error = " + e.getMessage());  return null;
		}

	}

	public String getLastMsid() {
		DBObject query = new BasicDBObject();
		BasicDBObject orderBy = new BasicDBObject();
		orderBy.put(PerfectMarketingObject.MSID, -1);
		DBObject keys = new BasicDBObject();
		keys.put(PerfectMarketingObject.MSID, 1);
		keys.put(PerfectMarketingObject.DATE_ADDED, 1);
		Map<String, Object> options = new HashMap();
		options.put("key", keys);
		options.put("sort", orderBy);
		options.put("limit", 1);

		try {
			List<DBObject> pmResult = MongoRestAPIService.getList(
					PM_RESULT_COLLECTION, query, options);
			Iterator<DBObject> itr = pmResult.iterator();
			DBObject dbo = null;
			while (itr.hasNext()) {
				dbo = itr.next();
			}
			if (dbo != null) {
				return (String.valueOf(dbo.get(PerfectMarketingObject.MSID)));
			} else {
				return null;
			}
		} catch (MongoException e) {
			log.error("Exception occurred in getTopDate with error = " + e.getMessage());  return null;
		} catch (Exception e) {
			log.error("Exception occurred in getTopDate with error = " + e.getMessage());  return null;
		}

	}

	public boolean deletePMObject(String msid, String collection) {

		DBObject query = new BasicDBObject();
		query.put(PerfectMarketingObject.MSID, Integer.valueOf(msid));

		try {
			if (collection.equals(PM_FAILED_COLLECTION)
					|| collection.equals(PM_UNSUCESSFUL_COLLECTION)) {
				List<PerfectMarketingObject> pmoCollection = getAllCollection(collection);
				Iterator<PerfectMarketingObject> itr = pmoCollection.iterator();
				int count = 0;
				while (itr.hasNext()) {
					PerfectMarketingObject pmo = itr.next();
					pmo.setSerialnumber(count++);
					updatePMObjectByMsid(String.valueOf(pmo.getMsid()), pmo,
							collection);
				}
			}
		} catch (Exception e) {
			log.error("Exception occurred in deletePMObject with error = " + e.getMessage()); 
		}

		return MongoRestAPIService.delete(collection, query);

	}

	public Map getAllPMObject(String collection) {
		DBObject query = new BasicDBObject();
		try {
			List<DBObject> pmResult = MongoRestAPIService.getList(collection,
					query);
			Iterator<DBObject> itr = pmResult.iterator();
			DBObject dbo = null;
			Map<String, Date> pmMap = new LinkedHashMap<String, Date>();

			while (itr.hasNext()) {
				dbo = itr.next();
				PerfectMarketingObject pmo = PerfectMarketingDataWrapper
						.getPMObject(dbo);
				pmMap.put(String.valueOf(pmo.getMsid()), pmo.getDateadded());
			}
			if (pmMap.size() > 0) {
				return pmMap;
			} else {
				return null;
			}
		} catch (MongoException e) {
			log.error("Exception occurred in getAllPMObject with error = " + e.getMessage());  return null;
		} catch (Exception e) {
			log.error("Exception occurred in getAllPMObject with error = " + e.getMessage());  return null;
		}

	}

	public List<PerfectMarketingObject> getAllCollection(String collection) {
		DBObject query = new BasicDBObject();
		try {
			List<DBObject> pmResult = MongoRestAPIService.getList(collection,
					query);
			Iterator<DBObject> itr = pmResult.iterator();
			DBObject dbo = null;
			List pmoList = new ArrayList();
			while (itr.hasNext()) {
				dbo = itr.next();
				PerfectMarketingObject pmo = PerfectMarketingDataWrapper
						.getPMObject(dbo);
				pmoList.add(pmo);
			}
			if (pmoList.size() > 0) {
				return pmoList;
			} else {
				return null;
			}
		} catch (MongoException e) {
			log.error("Exception occurred in getAllCollection with error = " + e.getMessage());  return null;
		} catch (Exception e) {
			log.error("Exception occurred in getAllCollection with error = " + e.getMessage());  return null;
		}

	}


}
