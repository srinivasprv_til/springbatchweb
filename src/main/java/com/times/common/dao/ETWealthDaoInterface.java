package com.times.common.dao;

import java.util.Date;
import java.util.Map;

public interface ETWealthDaoInterface {
	public Map getMsids(Date date1, Date date2);
}
