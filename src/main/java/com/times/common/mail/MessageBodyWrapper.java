package com.times.common.mail;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageBodyWrapper implements Serializable{
	
	private static final long serialVersionUID = -762865932853698980L;
	
	/**
	 * id of campaign
	 */
	@JsonProperty("cid")
	private Integer cid;
	
	@JsonProperty("message")
	private Message message = new Message();
	
	/**
	 * @return the message
	 */
	@JsonProperty("message")
	public Message getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	@JsonProperty("message")
	public void setMessage(Message message) {
		this.message = message;
	}

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	@Override
	public String toString() {
		return "MessageBodyWrapper [cid=" + cid + ", message=" + message + "]";
	}

}