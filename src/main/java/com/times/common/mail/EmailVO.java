
package com.times.common.mail;

import java.io.IOException;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

/**
 * This class is used as data transfer object for Email.
 * 
 * @author Ranjeet.jha
 *
 */
public class EmailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5771428425723825751L;
	private static final Logger logger = LoggerFactory.getLogger(EmailVO.class);
	
	@JsonProperty(value = "RECP")
	private String recipients;
	@JsonProperty(value="SENDER")
	private String sender;
	
	@JsonProperty(value="SUB")
	private String subject;
	
	@JsonProperty(value="BODY")
	private String body;
	private String contentType;
	private String mobile;
	private String smsmessage;
	
	@JsonProperty(value="MAIL_TP")
    private String mailType = "9";
    
	public String getMailType() {
		return mailType;
	}

	public void setMailType(String mailType) {
		this.mailType = mailType;
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	@JsonProperty(value="H_ID")
    private String hostId;
    
	
	/**
	 * @return the recipients
	 */
	public String getRecipients() {
		return recipients;
	}
	
	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}
	
	/**
	 * @return the sender
	 */
	public String getSender() {
		return sender;
	}
	
	/**
	 * @param sender the sender to set
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}
	
	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSmsmessage() {
		return smsmessage;
	}

	public void setSmsmessage(String smsmessage) {
		this.smsmessage = smsmessage;
	}
	
	public String toString(){
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
        String jsonString = "";
        try {
            jsonString = ow.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            logger.error("error while writing mail send bean to json");
        }
        catch(IOException io){
            logger.error("IO Exception  while writing mail send bean to json");
        }
        return jsonString;
    }
	
}
