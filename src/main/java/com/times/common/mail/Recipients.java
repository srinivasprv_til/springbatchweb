/**
 * 
 */
package com.times.common.mail;

import org.codehaus.jackson.annotate.JsonProperty;


public class Recipients {
	
	@JsonProperty("email")
	private String email;
	
	Recipients(String email) {
		this.email = email;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	
	@Override
	public String toString() {
		return "[email=" + email + "]";
	}

}