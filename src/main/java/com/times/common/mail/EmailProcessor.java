
package com.times.common.mail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class is delegating to send the email. to send the mail process the mail.
 *  
 * @author ranjeet.jha
 *
 */
public class EmailProcessor {
	
	private static Log logger = LogFactory.getLog(EmailProcessor.class);
	
	private MailSender mailSender;
	
	/**
	 * This method is used to send the mail by {@link EmailVO}.
	 * 
	 * @param message
	 * @throws Exception
	 */
	public void sendEMail(EmailVO mailDTO) throws Exception {
		try {
			if (mailDTO.getSender() != null) {
				mailSender.sendMessage(mailDTO.getRecipients(), mailDTO.getSubject(), mailDTO.getBody(), mailDTO.getSender());
			} else {
				mailSender.sendMessage(mailDTO.getRecipients(), mailDTO.getSubject(), mailDTO.getBody());
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled())
			logger.error("Exception caught during sent mail of process start " + e.getMessage());
			throw new Exception(e);
			
		} finally {
			mailDTO = null;
		}
	}
	
	/**
	 * This method is used to send the asynchronous mail by {@link EmailVO}.
	 * 
	 * @param message
	 * @throws Exception
	 */
	
	public void sendAsyncEMail(EmailVO mailDTO) throws Exception {
		try {
			mailSender.sendAsyncMessage(mailDTO.getRecipients(), mailDTO.getSubject(), mailDTO.getBody());
		} catch (Exception e) {
			if (logger.isErrorEnabled())
			logger.error("Exception caught during sent mail of process start " + e.getMessage());
			throw new Exception(e);
			
		} finally {
			mailDTO = null;
		}
	}
	
	
	/**
	 * This API will be used to send mailer with subject in non-english Language.
	 * @param mailDTO
	 * @throws Exception
	 */
	public void sendLanguageEMail(EmailVO mailDTO) throws Exception {
		try {
			mailSender.sendLanguageMessage(mailDTO.getRecipients(), mailDTO.getSubject(), mailDTO.getBody());
		} catch (Exception e) {
			if (logger.isErrorEnabled())
			logger.error("Exception caught during sent mail of process start " + e.getMessage());
			throw new Exception(e);
			
		} finally {
			mailDTO = null;
		}
	}
	
	
	/**
	 * This method is used to send the mail by sendPal API {@link EmailVO}.
	 * 
	 * @param message
	 * @throws Exception
	 */
	
	public void sendSendPalEMail(EmailVO mailDTO) throws Exception {
		try {
			mailSender.sendSendPalMessage(mailDTO.getRecipients(), mailDTO.getSubject(), mailDTO.getBody());
		} catch (Exception e) {
			if (logger.isErrorEnabled())
			logger.error("Exception caught during sent mail of process start " + e.getMessage());
			throw new Exception(e);
			
		} finally {
			mailDTO = null;
		}
	}
	
	/**
	 * This method is used to send the mail when Database sync process gets started.
	 * 
	 * @throws Exception
	 */
	public void  processStart(String additionalMessage) throws Exception {
		try {
			EmailVO mailDTO = MessageBuilder.getInstance().buildMessageForStartProcess(additionalMessage);
			mailSender.sendMessage(mailDTO.getRecipients(), mailDTO.getSubject(), mailDTO.getBody());
		} catch (Exception e) {
			if (logger.isErrorEnabled())
			logger.error("Exception caught during sent mail of process start " + e.getMessage());
			throw new Exception(e);
			
		} finally {
		}
	}
	
	/**
	 * This method is used to send the mail when Database sync process gets completed.
	 * 
	 * @param message
	 * @throws Exception
	 */
	public void  processCompleted(String message ) throws Exception {
		EmailVO emailDTO = null;
		try {
			emailDTO = MessageBuilder.getInstance().buildMessageForEndProcess(message);
			mailSender.sendMessage(emailDTO.getRecipients(), emailDTO.getSubject(), emailDTO.getBody());
		} catch (Exception e) {
			if (logger.isErrorEnabled())
			logger.error("Exception caught during sent mail of process start ");
			throw new Exception(e);
		} finally {
			emailDTO = null;
		}
	}

	/**
	 * @return the mailSender
	 */
	public MailSender getMailSender() {
		return mailSender;
	}

	/**
	 * @param mailSender the mailSender to set
	 */
	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	/**
	 * This method is used to ovrride the sender emailid.
	 * 
	 * @param senderEmailId
	 * @param replyToEmailId
	 */
	public void overrideSenderDetails(String senderEmailId, String replyToEmailId) {
		mailSender.overrideSenderDetails(senderEmailId, replyToEmailId);
	}
}
