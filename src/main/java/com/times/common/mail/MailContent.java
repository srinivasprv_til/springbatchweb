package com.times.common.mail;

import java.io.IOException;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;


/**
 * This pojo is used while call http://cmsgenericmailer.indiatimes.com/MailServiceWeb/messages/uploadMessage api with mail type 8.
 * @author Rajeev.Khatri
 *
 */
public class MailContent implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger(MailContent.class);


	private static final long serialVersionUID = 1L;

	@JsonProperty(value="MAIL_TP")
	private int mailType;
	@JsonProperty(value="H_ID")
	private int hostId;
	@JsonProperty("A_U_I")
	private String	commentid;
	@JsonProperty("F_ADD")
	private String	emailId;
	@JsonProperty("A_ID")
	private int loggedInUser;
	@JsonProperty("SYN")
	private String synopsys;
	@JsonProperty("TITLE")
	private String title;
	@JsonProperty("TYPE")
	private String type;
	@JsonProperty("A_D_N")
	private String userName;
	@JsonProperty("F_NAME")
	private String userFName;
	@JsonProperty("C_R_EROID")
	private String commentParentId;
	@JsonProperty("C_T")
	private String commentText;
	@JsonProperty("CITY")
	private String address;
	@JsonProperty("USER_TP")
	private String userType;
	@JsonProperty("POS")
	private String position;
	@JsonProperty("F_P_ADD")
	private String parentCommentUserEmail;
	@JsonProperty("P_C_NAME")
	private String parentCommentUserName;
	@JsonProperty("MSID")
	private String msid;	
	@JsonProperty("LINK")
	private String link;
	@JsonProperty("mailerformat")
	private String contentViewType = "Default";

	@JsonProperty("ALL_CU_ADD")
	private String allCommentUsersEmail;

	public String getParentCommentUserName() {
		return parentCommentUserName;
	}


	public void setParentCommentUserName(String parentCommentUserName) {
		this.parentCommentUserName = parentCommentUserName;
	}
	public String getParentCommentUserEmail() {
		return parentCommentUserEmail;
	}

	public void setParentCommentUserEmail(String parentCommentUserEmail) {
		this.parentCommentUserEmail = parentCommentUserEmail;
	}

	public String getAllCommentUsersEmail() {
		return allCommentUsersEmail;
	}

	public void setAllCommentUsersEmail(String allCommentUsersEmail) {
		this.allCommentUsersEmail = allCommentUsersEmail;
	}

	public String getCommentid() {
		return commentid;
	}


	public void setCommentid(String commentid) {
		this.commentid = commentid;
	}	
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public int getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(int loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	public String getSynopsys() {
		return synopsys;
	}


	public void setSynopsys(String synopsys) {
		this.synopsys = synopsys;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}




	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getUserFName() {
		return userFName;
	}


	public void setUserFName(String userFName) {
		this.userFName = userFName;
	}


	public String getCommentParentId() {
		return commentParentId;
	}


	public void setCommentParentId(String commentParentId) {
		this.commentParentId = commentParentId;
	}

	public int getMailType() {
		return mailType;
	}


	public void setMailType(int mailType) {
		this.mailType = mailType;
	}


	public int getHostId() {
		return hostId;
	}


	public void setHostId(int hostId) {
		this.hostId = hostId;
	}



	public String getCommentText() {
		return commentText;
	}


	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}



	public String getUserType() {
		return userType;
	}


	public void setUserType(String userType) {
		this.userType = userType;
	}


	public String getPosition() {
		return position;
	}


	public void setPosition(String position) {
		this.position = position;
	}



	public String getMsid() {
		return msid;
	}


	public void setMsid(String msid) {
		this.msid = msid;
	}



	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getLink() {
		return link;
	}


	public void setLink(String link) {
		this.link = link;
	}


	public String getContentViewType() {
		return contentViewType;
	}

	public void setContentViewType(String contentViewType) {
		this.contentViewType = contentViewType;
	}

	public String toString(){
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
		String jsonString = "";
		try {
			jsonString = ow.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			logger.error("error while writing mail send bean to json");
		}
		catch(IOException io){
			logger.error("IO Exception  while writing mail send bean to json");
		}
		return jsonString;
	}

}

