
package com.times.common.mail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class is responsible for preparing build emailDTO with required parameter 
 * i.e. message for the email.
 * 
 * @author Ranjeet Kumar Jha
 *
 */
public class MessageBuilder {

	
	private static Log logger = LogFactory.getLog(MessageBuilder.class);
	
	//private static final String EOL = System.getProperty("line.separator");	
	private static final String EOL = "<br />";	
	private static MessageBuilder instance;

	private String processStartEndEmailIds = "rajeev.khatri@timesinternet.in";
	private String processStrartSubject = "News Letter Email Process Started";
	private String processEndSubject = "News Letter Email Process Completed";
	
	/**
	 * private constructor to stop instantiation of this class from outside of this class.
	 */
	private MessageBuilder () {
		
	}
	
	/**
	 * This method is used to get the singleton instance of this class.
	 * 
	 * @return {@link MessageBuilder}
	 */
	public static MessageBuilder getInstance () {
		if (instance == null) {
			instance = new MessageBuilder();
		}
		return instance;
	}
	
	/**
	 * This method is used to build a message body for email 
	 * when database sync process started.
	 * 
	 * @return
	 * @throws Exception
	 */
	public EmailVO buildMessageForStartProcess(String msg) throws Exception {
		EmailVO emailDTO = new EmailVO();
		StringBuilder textMessage = new StringBuilder();
		
		try {
			textMessage.append("Hi,").append(EOL)
			  .append("This System generetor mail to inform you that news Letter Emailer process has been started.")
			  .append(EOL).append(EOL);
			  if (msg != null) {
				  textMessage.append(msg).append(EOL).append(EOL);
			  }
			  textMessage.append("Thanks and Regards")
			  .append(EOL)
	          .append("NewsLetter Emailer App");
			
			// set the text message in emailDTO 
			emailDTO.setBody(textMessage.toString());
			emailDTO.setSubject(processStrartSubject);
			emailDTO.setRecipients(processStartEndEmailIds);
			
		} catch (Exception e) {
			logger.error("Exception caught : " + e.getMessage());
			throw new Exception(e);
		}
		
		return emailDTO;
	}
	
	/**
	 * This method is used to build a message body for email 
	 * when database sync process completed.
	 * 
	 * @param message
	 * @return
	 * @throws Exception
	 */
	public EmailVO buildMessageForEndProcess(String message) throws Exception {
		EmailVO emailDTO = new EmailVO();
		StringBuilder textMessage = new StringBuilder();
		
		try {
			textMessage.append("Hi,").append(EOL)
			  .append("This System generetor mail to inform you that NewsLetter Mailing process has been completed.")
			  .append(EOL).append(EOL).append(EOL);
			 if (message != null) {
				  textMessage.append(message).append(EOL).append(EOL);
			  }			  
			  textMessage.append("Thanks and Regards")
			  .append(EOL)
	          .append("NewsLetter Emailer App");
			
			// set the text message in emailDTO 
			emailDTO.setBody(textMessage.toString());
			emailDTO.setSubject(processEndSubject);
			emailDTO.setRecipients(processStartEndEmailIds);
		} catch (Exception e) {
			logger.error("Exception caught : " + e.getMessage());
			throw new Exception(e);
		}
		return emailDTO;
	}
	
}
