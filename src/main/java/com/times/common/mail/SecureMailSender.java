/**
 * 
 */
package com.times.common.mail;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class is used to send the mail  by connecting-in into the SMTP userName and password of sender account. 
 * 
 * @author ranjeet.jha
 *
 */
public class SecureMailSender {
	private static Log logger = LogFactory.getLog(SecureMailSender.class);
	
	private static final String EOL = System.getProperty("line.separator");	
   
	//private static Session session ;
	private Session session ;
	private Transport transport;
	
    private static final String from = "ranjeet.kr@gmail.com";
    private static final String SMTP_HOST = "smtp.gmail.com";
    private static final String SMTP_PORT = "465"; // 587
    private String userName = "nlmailer.toi@gmail.com"; //
    private String password = "ranjeet@123";
    
	private static String mailBody = "Mail Body test mail.";
	public static String subject = "This is test mail from java api";
	public static String CONTENT_TYPE_HTML = "text/html";
    public static String CONTENT_TYPE_TEXT = "text/plain";
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
		//sendMail("Hi test subject", "test body ", "donotreply.ems@gmail.com", "ranjeet.jha@ebusinessware.com");
		System.out.println("starting......");
		SecureMailSender sender = new SecureMailSender();
		logger.debug("Open Resources ....");
		sender.openResources();
		int count = 2;
		for (int i = 0; i < count; i++) {
    		sender.sentMessage("donotreply.ems@gmail.com", "ranjeet.jha@indiatimes.co.in", subject, mailBody, CONTENT_TYPE_HTML);
    	}
    	
    	EmailVO mail = new EmailVO();
    	mail.setBody(mailBody);
    	mail.setContentType(CONTENT_TYPE_HTML);
    	mail.setSubject(subject); 
    	mail.setSender(from);
    	
    	for (int i = 0; i < count; i++) {
    		sender.sendMail(mail);
    	}
    	
    	
		sender.releasedResources();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/*public void sendMailByApacheMail(HtmlEmail email ) {
		HtmlEmail email = new HtmlEmail();
		email.setHostName(server.getHostName());
		email.setSmtpPort(server.getPort());
		email.setAuthenticator(new DefaultAuthenticator(server.getUsername(), server.getPassword()));
		email.setTLS(true);
		email.setFrom("test@example.com");
		email.addTo(to);
		email.setSubject(subject);
		email.setHtmlMsg(htmlMsg);
		email.send();
	}*/
	
    public void sentMessage(String from, String to, String subject, String body, String contentType) {
    	long startTime = System.currentTimeMillis();
    	openResources();
    	try {
        	Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
		
            InternetAddress[] address = {new InternetAddress(to)};
            message.setRecipients(Message.RecipientType.TO, address);

            message.setSubject(subject + "JavaMail API");
            message.setSentDate(new Date());
            //message.setReplyTo(addresses);

            //setHTMLContent(message);
            // Alternate form
            message.setContent(body, contentType);
            
            message.saveChanges();
            transport.sendMessage(message, address);
            System.out.println(" time taken : " + (System.currentTimeMillis() - startTime) + " ms");
        } catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	releasedResources();
    }
    
	private void openResources() {
		 try {
			 Properties props = getProperties();
			session = Session.getInstance(props);
			session = Session.getDefaultInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(userName, password);
						}
					});
			transport = session.getTransport("smtp");
			transport.connect(userName, password);
			//transport.connect(SMTP_HOST, userName, password);
			
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}
	
	private void releasedResources() {
		try {
			if (transport != null) {
				transport.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendMail(EmailVO emailDTO) throws Exception {
		sentMessage(emailDTO.getSender(), emailDTO.getRecipients(), emailDTO.getSubject(), emailDTO.getBody(), emailDTO.getContentType());
		/*final MimeMessage message = new MimeMessage(this.session);
		
		// 	set the message attribute
		message.setSender(new InternetAddress(emailDTO.getSender()));
		message.setSubject(emailDTO.getSubject());
		message.setContent(emailDTO.getBody(), "text/plain");
		message.setFrom(new InternetAddress(emailDTO.getSender(), "DONOT REPLYE"));
		
		Address[] addresses = InternetAddress.parse(emailDTO.getRecipients());
		if (emailDTO.getRecipients().indexOf(',') > 0)
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailDTO.getRecipients()));
		else
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(emailDTO.getRecipients()));

		this.transport.send(message);*/

	}
	
	
	
	/*public static void sendMail(String subject, String body, String sender, String recipients) throws Exception {

		//Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", "smtp.gmail.com");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.quitwait", "false");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication("donotreply.ems@gmail.com", "anushka#672");
					}
				});

		MimeMessage message = new MimeMessage(session);
		message.setSender(new InternetAddress(sender));
		message.setSubject(subject);
		message.setContent(body, "text/plain");
		if (recipients.indexOf(',') > 0)
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
		else
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients));

		Transport.send(message);

	}*/
			
	/**
	 * This method is used to set Mail SMTP properties key/value.
	 * 
	 * @return
	 */
	private Properties getProperties()  {
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", "smtp.gmail.com");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.quitwait", "false");
		return props;
	}
	
	public void sendMailInThread(final MimeMessage message) {
		try {
		new Thread(new Runnable() {
       	  public void run() {

       		  try{
                 Transport.send(message);
       		  }
       		  catch(MessagingException mex){
       			  mex.printStackTrace();
       		  }
         		  catch(Exception mex){
         			mex.printStackTrace();
        		  }
       	  }
       	}).start();
		
		} catch(Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * @return the session
	 */
	public Session getSession() {
		return session;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(Session session) {
		this.session = session;
	}

}
