/**
 * 
 */
package com.times.common.mail;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Message{

	/**
	 * html or plain or can be both
	 */
	@JsonProperty("html")
	private String html;
	
	@JsonProperty("plain")
	private String plain;
	
	/**
	 *  optional parameter
	 */
	@JsonProperty("subject")
	private String subject;
	
	/**
	 * to whom mail is to be sent
	 */
	@JsonProperty("to")
	private List<Recipients> to;
	
	
	/**
	 * optional parameter
	 */
	//@JsonProperty("cc")
	private List<Recipients> cc;
	
	
	/**
	 * optional parameter
	 */
	//@JsonProperty("bcc")
	private List<Recipients> bcc;
	
	/**
	 * optional parameter from
	 */
	private String fromemail;
	
	/**
	 * optional parameter reply to
	 */
	private String replytoemail;
	
	public Message() {}
	
	// constructor
	public Message(String html, String plain, List<Recipients> to){
		this.html = html;
		this.plain = plain;
		this.to = to;
	}
	
	/**
	 * @return the html
	 */
	public String getHtml() {
		return html;
	}

	/**
	 * @param html the html to set
	 */
	public void setHtml(String html) {
		this.html = html;
	}

	/**
	 * @return the plain
	 */
	public String getPlain() {
		return plain;
	}

	/**
	 * @param plain the plain to set
	 */
	public void setPlain(String plain) {
		this.plain = plain;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}	

	/**
	 * @return the to
	 */
	public List<Recipients> getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(List<Recipients> to) {
		this.to = to;
	}


	/**
	 * @return the cc
	 */
	public List<Recipients> getCc() {
		return cc;
	}

	/**
	 * @param cc the cc to set
	 */
	public void setCc(List<Recipients> cc) {
		this.cc = cc;
	}

	/**
	 * @return the bcc
	 */
	public List<Recipients> getBcc() {
		return bcc;
	}

	/**
	 * @param bcc the bcc to set
	 */
	public void setBcc(List<Recipients> bcc) {
		this.bcc = bcc;
	}
	
	public String getFromemail() {
		return fromemail;
	}

	public void setFromemail(String fromemail) {
		this.fromemail = fromemail;
	}

	public String getReplytoemail() {
		return replytoemail;
	}

	public void setReplytoemail(String replytoemail) {
		this.replytoemail = replytoemail;
	}

	@Override
	public String toString() {
		return "Message [html=" + html + ", plain=" + plain + ", subject="
				+ subject + ", to=" + to + ", cc=" + cc + ", bcc=" + bcc
				+ ", fromemail=" + fromemail + ", replytoemail=" + replytoemail
				+ "]";
	}

}