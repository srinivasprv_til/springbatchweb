package com.times.common.mail;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.apache.commons.lang3.StringEscapeUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

/**
 * This class is used to send the mail by connecting to the smtp.
 * 
 * @author Ranjeet.Jha
 * 
 *  TODO: Usually SMTP do require user/password so this'll work fine.
 *  But need to fix this to make this generic.
 */
public class MailSender {

	//public static final Log logger = LogFactory.getLog(MailSender.class);
	private static final Logger logger = LoggerFactory.getLogger(MailSender.class);
	
	public static final int NEW_CONNECTION_SLEEP_TIME = 100;

	public static final String HOST = "nmailer.indiatimes.com";
	public static final String FROM = "TimesofIndia <mailerservice@timesofindia.com>";
	public static final String REPLY_TO_ADDRESS = "TimesofIndia <timesofindia@bounce.indiatimes.com>";
	public static final String RETURN_PATH = "timesofindia@bounce.indiatimes.com";
	public static final String SENDPAL_API = "http://#host/api/#version/mail/send/#apikey";
	
	//thread pool for sending mail through sendpal
	private ExecutorService tPool4RestCall = null;
	
	/*public static final String REPLY_TO_ADDRESS = "TimesofIndia <timesofindia@bounce.indiatimes.com>"; //"Indiatimes.com <no-reply@indiatimes.com>";
	public static final String HOST = "10.157.211.113"; 
	public static final String FROM = "TOI Online Editor <toi.onlineeditor@indiatimes.com>";
	public static final String RETURN_PATH = "timesofindia@bounce.indiatimes.com";*/
	
	private String host = null;
	Transport bus = null;
	Session session = null;
	Properties props = null;

	//private String subject;
	private String fromEmailId;
	private String replyToEmailId;
	private String retrunPath;
	
	private String sendpalCampId;
	private String sendpalApiURL;
	private String sendpalKey;
	private String sendpalVersion;
	RestTemplate restTemplate = null;
	HttpHeaders headers = null;
	/**
	 * Constructor to call connectToServer.
	 */
	public MailSender() {
		// host = mailHost;
		//host = HOST;
		connectToServer();
		// logger.info("Host Set successfully:::" + host);
	}
	
	public MailSender(String host, String fromMailId, String returnPath, String replyBackEmailId) {
		this.host = host;
		this.fromEmailId = fromMailId;
		this.retrunPath = returnPath;
		this.replyToEmailId = replyBackEmailId;
		
		// connect to the server
		connectToServer();
	}
	
	public MailSender(String host, String fromMailId, String returnPath, String replyBackEmailId, String campaignId, String key, String version) {
		this.host = host;
		this.fromEmailId = fromMailId;
		this.retrunPath = returnPath;
		this.replyToEmailId = replyBackEmailId;
		this.sendpalCampId = campaignId;
		this.sendpalKey = key;
		this.sendpalVersion = version;
		this.sendpalApiURL = SENDPAL_API.replace("#host", this.host).replace("#version", this.sendpalVersion).replace("#apikey", sendpalKey);
		this.restTemplate = new RestTemplate();
		this.headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		
		ClientHttpRequestFactory httpFactory = restTemplate.getRequestFactory();
	    if(httpFactory!=null)
	    {
	        if(httpFactory instanceof SimpleClientHttpRequestFactory)
	        {
	            ((SimpleClientHttpRequestFactory)httpFactory).setConnectTimeout(10*1000);
	            ((SimpleClientHttpRequestFactory)httpFactory).setReadTimeout(30*1000);
	        }
	        
	    }

	}

	public void init() {
		//don't need to many threads here, because we just want to enforce timeout using Future and not parallelism.
		tPool4RestCall = Executors.newFixedThreadPool(1);
	}
	public void destroy() {
		//for clean exit.
		tPool4RestCall.shutdown();
	}
	
	/**
	 * This method is used to connect the smtp server.
	 */
	public void connectToServer() {
		try {
			props = new Properties();
			props.put("mail.smtp.host", host);
			session = Session.getInstance(props);
			bus = session.getTransport("smtp");
			bus.connect();
			if (logger.isInfoEnabled()) {
				logger.info("connected to mail server successfully:::::::::" + bus.isConnected());
			}
		} catch (Exception e) {
			logger.error("Exception while trying to connect to mail server:::", e);
		}
	}
	
	
	/**
	 * Connect to the SMTP server if server is disconnected, try 3 times to send mails and
	 * then drop this.
	 * 
	 * @param from
	 * @param to
	 * @param subject
	 * @param mailBody
	 * @return
	 */
	public synchronized boolean sendMessage(String to, String subject, String mailBody) {
		return sendMessage(to,subject,mailBody,fromEmailId);
	}
	
	
	
	/**
	 * Connect to the SMTP server if server is disconnected, try 3 times to send mails and
	 * then drop this.
	 * @param to
	 * @param subject
	 * @param mailBody
	 * @param sender
	 * @return
	 */
	public synchronized boolean sendMessage(String to, String subject, String mailBody, String sender) {

		long start = System.currentTimeMillis();
		boolean result = false;
		int reTry = 0;

		while (!result && reTry < 2) {
			try {
				if (!bus.isConnected() || reTry > 0) {
					bus.close();

					boolean fg = true;
					int reconnectCount = 1;
					while (fg) {
						try {
							// logger.debug("Again attempting to connect to mail server...........");
							bus = null;
							Thread.sleep(NEW_CONNECTION_SLEEP_TIME * reconnectCount);
							bus = session.getTransport("smtp");
							bus.connect();

							fg = false;
						} catch (MessagingException ex1) {
							logger.error("Remain Waiting for connection, slept " + NEW_CONNECTION_SLEEP_TIME * reconnectCount + " ms", ex1);
							reconnectCount++;
							cleanUp();
						} catch (Exception ex2) {
							logger.error("Some other exception occured, slept " + NEW_CONNECTION_SLEEP_TIME * reconnectCount + " ms", ex2);
							reconnectCount++;
							cleanUp();
						}
					}
					logger.debug("Time take in send mail connect ms " + (System.currentTimeMillis() - start));
				}

				// Instantiate a message
				Message msg = new MimeMessage(session);
				// Set message attributes
				msg.setFrom(new InternetAddress(sender));
				logger.debug("email : " + to);

				InternetAddress[] address = this.getInternetAddress(to);
				if (to.contains(",")) {
					address = this.getInternetAddress(to);
					msg.setRecipients(Message.RecipientType.TO, address);
				} else {
					address = new InternetAddress[1];
					address[0] = new InternetAddress(to);
					msg.setRecipient(Message.RecipientType.TO, address[0]);
				}


				msg.setSubject(subject);
				msg.setSentDate(new Date());
				msg.setHeader("Return-Path", retrunPath);			

				msg.setContent(mailBody , "text/html; charset=UTF-8");

				msg.saveChanges();
				bus.sendMessage(msg, address);
				result = true;
			} catch (MessagingException mex) {
				mex.printStackTrace();

				logger.error("Exception generated by mail server:::", mex);
				while (mex.getNextException() != null) {
					// Get next exception in chain
					Exception ex = mex.getNextException();
					logger.error("Exception generated by mail server:::", ex);
					if (!(ex instanceof MessagingException)) {
						break;
					} else {
						mex = (MessagingException) ex;
					}
				}

				reTry++;
				result = false;
				//cleanUp();
			} finally {
				logger.debug("Time taken to send mail ms " + (System.currentTimeMillis() - start));
			}
		}
		return result;
	}
	
	/**
	 * This API will be used to send mailer with subject in non-english Language.
	 * @param to
	 * @param subject
	 * @param mailBody
	 * @return
	 */
	public synchronized boolean sendLanguageMessage(String to, String subject, String mailBody) {

		long start = System.currentTimeMillis();
		boolean result = false;
		int reTry = 0;

		while (!result && reTry < 2) {
			try {
				if (!bus.isConnected() || reTry > 0) {
					bus.close();

					boolean fg = true;
					int reconnectCount = 1;
					while (fg) {
						try {
							// logger.debug("Again attempting to connect to mail server...........");
							bus = null;
							Thread.sleep(NEW_CONNECTION_SLEEP_TIME * reconnectCount);
							bus = session.getTransport("smtp");
							bus.connect();

							fg = false;
						} catch (MessagingException ex1) {
							logger.error("Remain Waiting for connection, slept " + NEW_CONNECTION_SLEEP_TIME * reconnectCount + " ms", ex1);
							reconnectCount++;
							cleanUp();
						} catch (Exception ex2) {
							logger.error("Some other exception occured, slept " + NEW_CONNECTION_SLEEP_TIME * reconnectCount + " ms", ex2);
							reconnectCount++;
							cleanUp();
						}
					}
					logger.debug("Time take in send mail connect ms " + (System.currentTimeMillis() - start));
				}

				// Instantiate a message
				Message msg = new MimeMessage(session);
				// Set message attributes
				msg.setFrom(new InternetAddress(fromEmailId));
				logger.debug("email : " + to);

				InternetAddress[] address = this.getInternetAddress(to);
				if (to.contains(",")) {
					address = this.getInternetAddress(to);
					msg.setRecipients(Message.RecipientType.TO, address);
				} else {
					address = new InternetAddress[1];
					address[0] = new InternetAddress(to);
					msg.setRecipient(Message.RecipientType.TO, address[0]);
				}

				try {
					msg.setSubject(MimeUtility.encodeText(subject, "utf-8", "B"));
				} catch (UnsupportedEncodingException e) {

					e.printStackTrace();
				}
				msg.setSentDate(new Date());
				msg.setHeader("Return-Path", retrunPath);			

				msg.setContent(mailBody , "text/html; charset=UTF-8");

				msg.saveChanges();
				bus.sendMessage(msg, address);
				result = true;
			} catch (MessagingException mex) {
				mex.printStackTrace();

				logger.error("Exception generated by mail server:::", mex);
				while (mex.getNextException() != null) {
					// Get next exception in chain
					Exception ex = mex.getNextException();
					logger.error("Exception generated by mail server:::", ex);
					if (!(ex instanceof MessagingException)) {
						break;
					} else {
						mex = (MessagingException) ex;
					}
				}

				reTry++;
				result = false;

			} finally {
				logger.debug("Time taken to send mail ms " + (System.currentTimeMillis() - start));
			}
		}
		return result;
	}

	/**
	 * @param to
	 * @return
	 * @throws AddressException
	 */
	private InternetAddress[] getInternetAddress(String to) throws AddressException {
		InternetAddress[] addresses = null;
		if (to.contains(",")) {
			String[] eArrays = to.split(",");
			if (eArrays != null && eArrays.length > 0)
				addresses = new InternetAddress[eArrays.length];
				for (int i = 0; i < eArrays.length; i++) {
					addresses[i] = new InternetAddress(eArrays[i]);
				}
		} else {
			addresses = new InternetAddress[1];
			addresses[0] = new InternetAddress(to);
		}
		return addresses;
	}

	/**
	 * @param from
	 * @param to
	 * @param subject
	 * @param str
	 * @param replyTo
	 * @return
	 */
	public synchronized boolean sendMessageWithReplyTo(String from, String to, String subject, String str,	String replyTo) {

		long start = System.currentTimeMillis();
		boolean result = false;
		int reTry = 0;
		while (!result && reTry < 2) {

			try {
				if (!bus.isConnected() || reTry > 0) {
					bus.close();

					// logger.debug("Disconnected from mail server.....................");
					boolean fg = true;
					int reconnectCount = 1;

					while (fg) {
						try {
							// logger.debug("Again attempting to connect to mail server...........");
							bus = null;
							Thread.sleep(NEW_CONNECTION_SLEEP_TIME * reconnectCount);
							bus = session.getTransport("smtp");
							bus.connect();
							fg = false;

						} catch (MessagingException ex1) {
							// logger.error("Remain Waiting for connection, slept "  + NEW_CONNECTION_SLEEP_TIME * reconnectCount + " ms", ex1);
							reconnectCount++;
							//cleanUp();

						} catch (Exception ex2) {
							logger.error("Some other exception occured, slept " + NEW_CONNECTION_SLEEP_TIME * reconnectCount + " ms", ex2);
							reconnectCount++;
							//cleanUp();
						}
					}
					// logger.debug("Time take in send mail connect ms " + (System.currentTimeMillis() - start));
				}

				// Instantiate a message
				Message msg = new MimeMessage(session);
				// Set message attributes
				msg.setFrom(new InternetAddress(from));
				InternetAddress[] address = { new InternetAddress(to) };
				msg.setRecipients(Message.RecipientType.TO, address);
				msg.setReplyTo(new InternetAddress[] { new InternetAddress(replyTo) });
				msg.setSubject(subject);
				msg.setSentDate(new Date());
				setHTMLContent(msg, str);
				msg.saveChanges();
				bus.sendMessage(msg, address);
				result = true;
			} catch (MessagingException mex) {
				// mex.printStackTrace();
				// logger.error("Exception generated by mail server:::", mex);
				while (mex.getNextException() != null) {
					// Get next exception in chain
					Exception ex = mex.getNextException();
					// logger.debug("Exception generated by mail server:::",  ex);
					if (!(ex instanceof MessagingException))
						break;
					else
						mex = (MessagingException) ex;
				}
				reTry++;
				result = false;
				cleanUp();
			} finally {
				// logger.debug("Time take in send mail send ms " +
				// (System.currentTimeMillis() - start));
			}
		}
		return result;
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		cleanUp();
	}

	/**
	 * 
	 * Close the connection and clean up the stuff.
	 */
	public void cleanUp() {
		try {
			if (bus != null) {
				bus.close();
			}
		} catch (Throwable e) {
			// logger.error("Error in finalize, SMTP connection not closed ", e);
		}
	}

	// Set a single part html content.
	// Sending data of any type is similar.
	public static void setHTMLContent(Message msg) throws MessagingException {
		/*
		 * String html = "<html><head><title>" +
		 * msg.getSubject() +
		 * "</title></head><body><h1>" +
		 * msg.getSubject() +
		 * "</h1><p>This is a test of sending an HTML e-mail" +
		 * " through Java.</body></html>";
		 */
		StringBuffer sbuf = new StringBuffer();

		try {

			InputStream in = (InputStream) new FileInputStream("/usr/toiclass/resultDynamic.html");
			BufferedReader buf = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			String line = null;
			while ((line = buf.readLine()) != null)
				sbuf.append(line);
		} catch (Exception e) {
			System.out.println(e);
		}
		// HTMLDataSource is an inner class
		msg.setDataHandler(new DataHandler(new HTMLDataSource(sbuf.toString())));
	}

	public static void setHTMLContent(Message msg, String str) throws MessagingException {
		// HTMLDataSource is an inner class
		msg.setDataHandler(new DataHandler(new HTMLDataSource(str)));
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the fromEmailId
	 */
	public String getFromEmailId() {
		if (fromEmailId == null || "".equals(fromEmailId)) {
			fromEmailId = FROM;
		}
		return fromEmailId;
	}

	/**
	 * @param fromEmailId the fromEmailId to set
	 */
	public void setFromEmailId(String fromEmailId) {
		this.fromEmailId = fromEmailId;
	}
	
	/*
	 * 
	 * Inner class to act as a JAF datasource to send HTML e-mail content
	 */
	static class HTMLDataSource implements DataSource {
		private String html;
		public HTMLDataSource(String htmlString) {
			html = htmlString;
		}

		// Return html string in an InputStream.
		// A new stream must be returned each time.
		public InputStream getInputStream() throws IOException {
			if (html == null)
				throw new IOException("Null HTML");
			return new ByteArrayInputStream(html.getBytes());
		}

		public OutputStream getOutputStream() throws IOException {
			throw new IOException("This DataHandler cannot write HTML");
		}

		public String getContentType() {
			return "text/html";
		}

		public String getName() {
			return "JAF text/html dataSource to send e-mail only";
		}

	}

	/**
	 * @return the replyToEmailId
	 */
	public String getReplyToEmailId() {
		return replyToEmailId;
	}

	/**
	 * @param replyToEmailId the replyToEmailId to set
	 */
	public void setReplyToEmailId(String replyToEmailId) {
		this.replyToEmailId = replyToEmailId;
	}

	/**
	 * @return the retrunPath
	 */
	public String getRetrunPath() {
		return retrunPath;
	}

	/**
	 * @param retrunPath the retrunPath to set
	 */
	public void setRetrunPath(String retrunPath) {
		this.retrunPath = retrunPath;
	}

	/**
	 * This method is used to ovrride the sender emailid.
	 * 
	 * @param senderEmailId
	 * @param replyToEmailId
	 */
	public void overrideSenderDetails(String senderEmailId, String replyToEmailId) {
		if (StringUtils.hasText(senderEmailId)) {
			this.fromEmailId = senderEmailId;
		}
		
		if (StringUtils.hasText(replyToEmailId)) {
			this.replyToEmailId = replyToEmailId;
		}
	}
	
	
	/**
	 * Connect to the SMTP server if server is disconnected, try 3 times to send mails and
	 * then drop this.Send mail asynchronous.
	 * 
	 * @param from
	 * @param to
	 * @param subject
	 * @param mailBody
	 * @return
	 */		
	
	public boolean sendAsyncMessage(String to, String subject, String mailBody) {

		long start = System.currentTimeMillis();
		boolean result = false;
		int reTry = 0;
		
		while (!result && reTry < 2) {
			try {
				if (!bus.isConnected() || reTry > 0) {
					bus.close();

					boolean fg = true;
					int reconnectCount = 1;
					while (fg) {
						try {
							// logger.debug("Again attempting to connect to mail server...........");
							bus = null;
							Thread.sleep(NEW_CONNECTION_SLEEP_TIME * reconnectCount);
							bus = session.getTransport("smtp");
							bus.connect();
							
							fg = false;
						} catch (MessagingException ex1) {
							 logger.error("Remain Waiting for connection, slept " + NEW_CONNECTION_SLEEP_TIME * reconnectCount + " ms", ex1);
							reconnectCount++;
							cleanUp();
						} catch (Exception ex2) {
							 logger.error("Some other exception occured, slept " + NEW_CONNECTION_SLEEP_TIME * reconnectCount + " ms", ex2);
							reconnectCount++;
							cleanUp();
						}
					}
					logger.debug("Time take in send mail connect ms " + (System.currentTimeMillis() - start));
				}

				// Instantiate a message
				Message msg = new MimeMessage(session);
				// Set message attributes
				msg.setFrom(new InternetAddress(fromEmailId));
				logger.debug("email : " + to);
				
				InternetAddress[] address = this.getInternetAddress(to);
				if (to.contains(",")) {
					address = this.getInternetAddress(to);
					msg.setRecipients(Message.RecipientType.TO, address);
				} else {
					address = new InternetAddress[1];
					address[0] = new InternetAddress(to);
					msg.setRecipient(Message.RecipientType.TO, address[0]);
				}
				
				//msg.setReplyTo(new InternetAddress[] { new InternetAddress(REPLY_TO_ADDRESS) });
				msg.setSubject(subject);
				msg.setSentDate(new Date());
				msg.setHeader("Return-Path", retrunPath);			
				//setHTMLContent(msg, mailBody);
				msg.setContent(mailBody , "text/html; charset=UTF-8");
				
				msg.saveChanges();
				bus.sendMessage(msg, address);
				result = true;
			} catch (MessagingException mex) {
				 mex.printStackTrace();
				
				 logger.error("Exception generated by mail server:::", mex);
				while (mex.getNextException() != null) {
					// Get next exception in chain
					Exception ex = mex.getNextException();
					 logger.error("Exception generated by mail server:::", ex);
					if (!(ex instanceof MessagingException)) {
						break;
					} else {
						mex = (MessagingException) ex;
					}
				}

				reTry++;
				result = false;
				//cleanUp();
			} finally {
				 logger.debug("Time taken to send mail ms " + (System.currentTimeMillis() - start));
			}
		}
		return result;
	}

	public String getSendpalCampId() {
		return sendpalCampId;
	}

	public void setSendpalCampId(String sendpalCampId) {
		this.sendpalCampId = sendpalCampId;
	}

	public String getSendpalKey() {
		return sendpalKey;
	}

	public void setSendpalKey(String sendpalKey) {
		this.sendpalKey = sendpalKey;
	}

		
	
	
	/**
	 * This API send mail to user using sendPal API.	 * 
	 * http://#host/api/#version/mail/send/#apikey
	 * @param to
	 * @param subject
	 * @param mailBody
	 */
	public synchronized void sendSendPalMessage(String to, String subject, String mailBody) {

		com.times.common.mail.Message message = new com.times.common.mail.Message();

		message.setHtml(StringEscapeUtils.escapeHtml4(mailBody));
		message.setSubject(StringEscapeUtils.escapeHtml4(subject));

		List<Recipients> toAddress = new ArrayList<Recipients>();
		String eArrays[] = to.split(",");
		if (eArrays != null && eArrays.length > 0) {
			for (int i = 0; i < eArrays.length; i++) {
				toAddress.add(new Recipients(eArrays[i]));
			}
		}

		message.setTo(toAddress);

		message.setReplytoemail(this.replyToEmailId);
		message.setFromemail(this.fromEmailId);

		MessageBodyWrapper messageContent = new MessageBodyWrapper();
		messageContent.setMessage(message);
		messageContent.setCid(Integer.parseInt(this.sendpalCampId));

		ObjectMapper mapper = new ObjectMapper();
		String body = null;
		try {
			body = mapper.writeValueAsString(messageContent);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);
		
		try {
//			ResponseEntity<String> result = restTemplate.exchange(this.sendpalApiURL, HttpMethod.POST, entity, String.class);
			
			//At times, RestTemplate doesn't timeout properly, hence using Future.get to ensure timeout after 1 minute.
			Future<ResponseEntity<String>> future = tPool4RestCall.submit(new SendpalMailSender(entity));
			ResponseEntity<String> result = future.get(1, TimeUnit.MINUTES);

			if (result != null) {

				String messageBody = result.getBody();
				if (messageBody != null) {
					JSONObject resultJSON = new JSONObject(messageBody.substring(1, messageBody.length()-1));
					int code = resultJSON.getInt("code"); 
					logger.debug("email : " + to + " and Result Code "  + code);
				}
			}
		} catch (Exception e) {
			logger.error("email : " + to  +",replyToEmailId " + replyToEmailId + ", fromEmailId " + fromEmailId + ", URL " + sendpalApiURL + "cid" 
					+ sendpalCampId + " and mailBody " + mailBody + ", subject "  + subject+ ", Exception "  + e.getMessage(), e );

		}

	}

	public String getSendpalApiURL() {
		return sendpalApiURL;
	}

	public void setSendpalApiURL(String sendpalApiURL) {
		this.sendpalApiURL = sendpalApiURL;
	}

	public String getSendpalVersion() {
		return sendpalVersion;
	}

	public void setSendpalVersion(String sendpalVersion) {
		this.sendpalVersion = sendpalVersion;
	}

	private class SendpalMailSender implements Callable<ResponseEntity<String>> {
		HttpEntity<String> entity = null;
		
		public SendpalMailSender(HttpEntity<String> entity) {
			super();
			this.entity = entity;
		}
		
		@Override
		public ResponseEntity<String> call() throws Exception {
			return restTemplate.exchange(sendpalApiURL, HttpMethod.POST, entity, String.class);
		}
	}
}
