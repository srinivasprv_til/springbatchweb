/**
 * 
 */
package com.times.common.wrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.util.Constant;
import com.times.common.util.DateUtils;
import com.times.mailer.model.MailUser;
import com.times.mailer.model.Mailer;
import com.times.mailer.model.NewsLetter;
import com.times.mailer.model.NewsLetterDailyTracker;
import com.times.mailer.model.Subscription;
import com.times.mailer.model.TravelAlertUser;
import com.times.mailer.model.TravelPreference;

/**
 * @author ranjeet.jha
 *
 */
public class NewsletterDBObjectWrapper {

	private static final Log logger = LogFactory.getLog(NewsletterDBObjectWrapper.class);
	
	/**
	 * This method is used to get the {@link Mailer} object by provided query.
	 * 
	 * @param objDB
	 * @param itype
	 * @return
	 */
	public static Mailer getMailerFromDBObject(DBObject objDB) {

		Mailer mailer = new Mailer();
		mailer.setId(String.valueOf(objDB.get(Mailer.ID)));
		mailer.setGroupId(String.valueOf(objDB.get(Mailer.GROUP_ID)));
		mailer.setCmsMappingId(String.valueOf(objDB.get(Mailer.MAPPING_ID)));
		mailer.setHtmlPath(String.valueOf(objDB.get(Mailer.EXTPATH)));
		
		String hrMinStr = DateUtils.getHourAndMin(new Date());
		String[] hrMin = null;
		if (hrMinStr != null) {
			hrMin =hrMinStr.split(":");
		}
		String hrs = hrMin[0];
		String min = hrMin[1];
		
		//Date masterDate = getStrTime(hrs, min);
		
		// iScheduletype referes either morning or evening 1 refer for morning befor 1 a
		int iScheduletype = (Integer.parseInt(hrs) < 12) ? 1 : 2;
		// schedule time for user of movie review is set for evening.iScheduletype is set to 1 as all user schedule time is set to 1.
		if (String.valueOf(objDB.get(Mailer.ID)).equals("1003")) {
			iScheduletype = 1;
		}
		//TODO: need for manual -ranjeet 
		//iScheduletype = 2; 
		mailer.setScheduleTime(iScheduletype);
		
		mailer.setTemplatepath(String.valueOf(objDB.get(Mailer.TEMPLATE)));
		mailer.setPersonalize(String.valueOf(objDB.get(Mailer.PERSONALIZE)));
		mailer.setMailSubject(String.valueOf(objDB.get(Mailer.SUBJECT)));
		mailer.setMailerFrom(String.valueOf(objDB.get(Mailer.FROMMAIL)));
		mailer.setName(String.valueOf(objDB.get(Mailer.NAME)));
		mailer.setMorningTime(String.valueOf(objDB.get(Mailer.SCHEDULED_MORNING_TIMING)));
		mailer.setEveningTime(String.valueOf(objDB.get(Mailer.SCHEDULED_EVENING_TIMING)));
		
		if ("1.0".equals(String.valueOf(objDB.get(Mailer.DAILY))) || "1".equals(String.valueOf(objDB.get(Mailer.DAILY)))) {
			mailer.setDaily(true);
		} else {
			mailer.setDaily(false);
		}
		if ("1.0".equals(String.valueOf(objDB.get(Mailer.WEEKLY))) || "1".equals(String.valueOf(objDB.get(Mailer.WEEKLY)))) {
			mailer.setWeekly(true);
		} else {
			mailer.setWeekly(false);
		}
		return mailer;
	}
	/**
	 * This method is used to convert the {@link DBObject} {@link NewsLetter}.
	 * 
	 * @param nlDBObject
	 * @return
	 * @throws Exception
	 */
	public static NewsLetter getNewsLetterFromBson(DBObject nlDBObject) {
		NewsLetter newsLetter = new NewsLetter();
		if (nlDBObject != null ) {
			newsLetter.setId(String.valueOf(nlDBObject.get(NewsLetter.ID)));
			newsLetter.setDisplayFormate(String.valueOf(nlDBObject.get(NewsLetter.DISPLAY_FORMATE)));
			newsLetter.setIpAddress(String.valueOf(nlDBObject.get(NewsLetter.IP_ADDRESS)));
			newsLetter.setStatus( ((Number)(nlDBObject.get(NewsLetter.STATUS))).intValue());
			newsLetter.setUserPref(String.valueOf(nlDBObject.get(NewsLetter.USER_PREF)));
			newsLetter.setSecName(String.valueOf(nlDBObject.get(NewsLetter.SECTION_NAME)));
			newsLetter.setDaily(((Boolean)nlDBObject.get(NewsLetter.DAILY)).booleanValue());
			newsLetter.setWeekly(((Boolean)nlDBObject.get(NewsLetter.WEEKLY)).booleanValue());
			
			newsLetter.setHashcode(String.valueOf(nlDBObject.get(NewsLetter.HASH_CODE)));
			
			//check added as wrong values in db
			if(nlDBObject.get(NewsLetter.SCHEDULETIME) == null) {
				newsLetter.setScheduleTime(1);	
			} else {
				newsLetter.setScheduleTime(((Number) nlDBObject.get(NewsLetter.SCHEDULETIME)).intValue());
			}
		}
		return newsLetter;
	}

	/**
	 * This method is used to convert {@link DBObject} object by provided data transfer object i.e. {@link NewsLetterDailyTracker}.
	 * 
	 * @param tracker
	 * @return
	 */
	public static  DBObject getDBObjectByNewsLetterTracker(NewsLetterDailyTracker nlailyTracker) {
		DBObject dbObject = null;
		if (nlailyTracker != null) {
			DBObject nlDBObject = new BasicDBObject();
			nlDBObject.put(NewsLetterDailyTracker.DATE, nlailyTracker.getDate());
			//nlDBObject.put(NewsLetterDailyTracker.DATE, MailerUtil.getFromatedDate(new Date(), MailerUtil.DATE_FORMATE_YYYMMDD));
			nlDBObject.put(NewsLetterDailyTracker.ADDED_AT, nlailyTracker.getAddedAt());
			nlDBObject.put(NewsLetterDailyTracker.NEWS_LETTER_ID, nlailyTracker.getNewsletterId());
			nlDBObject.put(NewsLetterDailyTracker.NEWS_LETTER_CODE, nlailyTracker.getNewsletterCode());
			nlDBObject.put(NewsLetterDailyTracker.SCHEDULED_TIME, nlailyTracker.getScheduledTime());
			nlDBObject.put(NewsLetterDailyTracker.SCHEDULED_FREQUENCY, nlailyTracker.getScheduleFrequence());
			nlDBObject.put(NewsLetterDailyTracker.NEWS_LETTER_READ_COUNTER, nlailyTracker.getReadCounter());
			nlDBObject.put(NewsLetterDailyTracker.NEWS_LETTER_SENT_COUNTER, nlailyTracker.getSentCounter());
			nlDBObject.put(NewsLetterDailyTracker.NEWS_LETTER_CLICK_COUNTER, nlailyTracker.getClickCounter());
			nlDBObject.put(NewsLetterDailyTracker.NEWS_LETTER_USERS_COUNT, nlailyTracker.getUsersCount());
			
			return nlDBObject;
		}
		
		return dbObject;
	}
	
	/**
	 * This method is used to get {@link NewsLetterDailyTracker} object by provided {@link DBObject}.
	 * 
	 * @param dbObject
	 * @return
	 */
	public static NewsLetterDailyTracker getNewsLetterTrackerByDBObject(DBObject dbObject) {
		NewsLetterDailyTracker newsLDBObject = new NewsLetterDailyTracker();
		if (dbObject != null) {
			newsLDBObject.setAddedAt((Date)dbObject.get(NewsLetterDailyTracker.ADDED_AT));
			newsLDBObject.setDate(String.valueOf(dbObject.get(NewsLetterDailyTracker.DATE)));
			newsLDBObject.setNewsletterId(((Number)dbObject.get(NewsLetterDailyTracker.NEWS_LETTER_ID)).intValue());
			newsLDBObject.setNewsletterCode(String.valueOf(dbObject.get(NewsLetterDailyTracker.NEWS_LETTER_CODE)));
			newsLDBObject.setScheduledTime(((Number)dbObject.get(NewsLetterDailyTracker.SCHEDULED_TIME)).intValue());
			newsLDBObject.setScheduleFrequence(((Number)dbObject.get(NewsLetterDailyTracker.SCHEDULED_FREQUENCY)).intValue());
			newsLDBObject.setReadCounter(((Number)dbObject.get(NewsLetterDailyTracker.NEWS_LETTER_READ_COUNTER)).longValue());
			newsLDBObject.setSentCounter(((Number)dbObject.get(NewsLetterDailyTracker.NEWS_LETTER_SENT_COUNTER)).longValue());
			newsLDBObject.setClickCounter(((Number)dbObject.get(NewsLetterDailyTracker.NEWS_LETTER_CLICK_COUNTER)).longValue());
		} else {
			logger.debug("Passed in BSON object for Subscription is null.");
			return null;
		}
		return newsLDBObject;
	}
	
	

	/**
	* This is the NewsLetterDailyTracker domain model EntityMapper which convert
	* from domain model object i.e. <code>NewsLetterDailyTracker</code> to {@link DBObject).
	*
	* @param nlailyTracker
	* @return
	* @throws exception
	*/
	/*public static DBObject getDBObjectByNewsLetterDailyTracker(NewsLetterDailyTracker nlailyTracker)  {
		DBObject nlDBObject = new BasicDBObject();
		nlDBObject.put(NewsLetterDailyTracker.ADDED_DATE, nlailyTracker.getAddedAt());
		nlDBObject.put(NewsLetterDailyTracker.NEWS_LETTER_ID, nlailyTracker.getNewsletterId());
		nlDBObject.put(NewsLetterDailyTracker.NEWS_LETTER_CODE, nlailyTracker.getNewsletterCode());
		nlDBObject.put(NewsLetterDailyTracker.SCHEDULED_TIME, nlailyTracker.getScheduledTime());
		nlDBObject.put(NewsLetterDailyTracker.SCHEDULED_FREQUENCY, nlailyTracker.getScheduleFrequence());
		nlDBObject.put(NewsLetterDailyTracker.NEWS_LETTER_COUNTER, nlailyTracker.getReadCounter());
		nlDBObject.put(NewsLetterDailyTracker.NEWS_LETTER_SENT_COUNTER, nlailyTracker.getSentCounter());
		nlDBObject.put(NewsLetterDailyTracker.NEWS_LETTER_CLICK_COUNTER, nlailyTracker.getSentCounter());
		return nlDBObject;
	}*/
	
	/**
	 * @param newsLDBObject
	 * @return
	 * @throws Exception
	 */
	/*public static NewsLetterDailyTracker getNewsLetterDailyTrackerFromBson(DBObject newsLDBObject) throws Exception {
		NewsLetterDailyTracker subs = new NewsLetterDailyTracker();
		if (newsLDBObject != null) {
			subs.setAddedAt((Date)newsLDBObject.get(NewsLetterDailyTracker.ADDED_DATE));
			subs.setNewsletterId(((Number)newsLDBObject.get(NewsLetterDailyTracker.NEWS_LETTER_ID)).intValue());
			subs.setNewsletterCode(String.valueOf(newsLDBObject.get(NewsLetterDailyTracker.NEWS_LETTER_CODE)));
			subs.setScheduledTime(((Number)newsLDBObject.get(NewsLetterDailyTracker.SCHEDULED_TIME)).intValue());
			subs.setScheduleFrequence(((Number)newsLDBObject.get(NewsLetterDailyTracker.SCHEDULED_FREQUENCY)).intValue());
			subs.setReadCounter(((Number)newsLDBObject.get(NewsLetterDailyTracker.NEWS_LETTER_COUNTER)).longValue());
			subs.setSentCounter(((Number)newsLDBObject.get(NewsLetterDailyTracker.NEWS_LETTER_SENT_COUNTER)).longValue());
			subs.setClickCounter(((Number)newsLDBObject.get(NewsLetterDailyTracker.NEWS_LETTER_CLICK_COUNTER)).longValue());
		} else {
			logger.debug("Passed in BSON object for Subscription is null.");
			return null;
		}
		return subs;
	}*/


	/**
	 * @param dbObjectList
	 * @return
	 */
	public static List<TravelAlertUser> getTravelAlertUserList(List<DBObject> dbObjectList, Mailer myMailer)  {
		
		List<TravelAlertUser> list = null;
		try {
			if (dbObjectList != null && dbObjectList.size() > 0) {
				list = new ArrayList<TravelAlertUser>();
				for (DBObject dbObject : dbObjectList) {
					list.add(getTravelAlertUserFromBson(dbObject, myMailer));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return list;
	}

	/**
	 * @param dbObject
	 * @return
	 */
	public static TravelAlertUser getTravelAlertUserFromBson(DBObject dbObject , Mailer myMailer)  {
		
		TravelAlertUser travelAlertUser = null;
		try {
			if (dbObject != null) {
				
				travelAlertUser = new TravelAlertUser();
	
				travelAlertUser.setEmailId((String)dbObject.get(Subscription.SSO_USER_ID));
				travelAlertUser.setTemplatePath(myMailer.getTemplatepath());
				
				List<DBObject> nlListMap = (List<DBObject>) dbObject.get(Subscription.NEWS_LETTER_MAP);
				if (nlListMap != null && nlListMap.size() > 0) {
					for(DBObject db : nlListMap){
						//int schTime = ((Number)db.get(NewsLetter.SCHEDULETIME)).intValue();
						travelAlertUser.addNewsletter(getNewsLetterFromBson(db));
						//break;
					}
				}
				
				List<DBObject> tpListMap = (List<DBObject>) dbObject.get(Subscription.NEWS_LETTER_PREF);
				if (tpListMap != null && tpListMap.size() > 0) {
					for(DBObject db : tpListMap){
						if (((Boolean)db.get(Constant.VERIFIED_PREFERENCE)).booleanValue())  {
							travelAlertUser.addTravelSubscrptionPreference(getTravelPreferenceFromBson(db));
						}
					}
				}
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return travelAlertUser;
	}
	
	/**
	 * This method return a {@link DBObject} which will save in the db.
	 * 
	 * @param alertUser
	 * @return
	 */
	public static DBObject getDBObjectByTravelAlertUser(TravelAlertUser alertUser, Map<String, Set<String>> map) {
		DBObject dbObject = new BasicDBObject();
		
		if (alertUser != null) {
			dbObject.put(TravelAlertUser.EMAIL_ID, alertUser.getEmailId());
			dbObject.put(TravelAlertUser.NLID, alertUser.getNlid());
			dbObject.put(TravelAlertUser.USER_PREFERENCES_MAP, map);
			dbObject.put(TravelAlertUser.MSID, getMsidList(alertUser.getMsid()));
			dbObject.put(TravelAlertUser.EMAIL_ID, alertUser.getEmailId());
			dbObject.put("C_AT", new Date());
		}
		return dbObject;
	}
	
	
	
	/**
	 * @param db
	 * @return TravelPreference object
	 */
	public static TravelPreference getTravelPreferenceFromBson(DBObject db) {
		TravelPreference travelPreference = new TravelPreference();
		if (db != null && (Boolean)db.get(TravelPreference.VERIFIED_PREF)) {
			travelPreference.setId(String.valueOf(db.get(TravelPreference.ID)));
			travelPreference.setVerified((Boolean)db.get(TravelPreference.VERIFIED_PREF));
			travelPreference.setPreferenceValue(String.valueOf(db.get(TravelPreference.PREF)));
			travelPreference.setHashcode(String.valueOf(db.get(TravelPreference.HASH_CODE)));
			
		}
		return travelPreference;
	}
	
	/**
	 * @param msidSet
	 * @return List<Integer>
	 */
	private static List<Integer> getMsidList(Set<Integer> msidSet) {
		
		List<Integer> list = new ArrayList<Integer>();
		if (msidSet != null) {
			
			Iterator<Integer> it = msidSet.iterator();
			while (it.hasNext()) {
				list.add(it.next());
			}
		}
		
		return list;
	}
	
	/**
	 * This method return a {@link DBObject} which will save in the db.
	 * 
	 * @param alertUser
	 * @return
	 */
	public static DBObject getDBObjectByAlertUser(MailUser alertUser, Map<String, Set<String>> map, Map<Integer, String> msidTimestampMap) {
		DBObject dbObject = new BasicDBObject();
		
		if (alertUser != null) {
			dbObject.put(Constant.EMAIL_ID, alertUser.getEmailId());			
			dbObject.put(Constant.NLID_MSID_COLLECTION, map);			
			dbObject.put(Constant.MSID, msidTimestampMap);
			
			dbObject.put("C_AT", new Date());
		}
		return dbObject;
	}
}
