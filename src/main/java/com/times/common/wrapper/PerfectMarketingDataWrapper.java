package com.times.common.wrapper;

import java.util.Date;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.common.model.PerfectMarketingObject;

public class PerfectMarketingDataWrapper {

	
	public static PerfectMarketingObject getPMObject(DBObject pmDBObject) throws Exception {
		PerfectMarketingObject pmObj = new PerfectMarketingObject();
		if (pmDBObject != null) {

			pmObj.setSerialnumber((Integer)pmDBObject.get(PerfectMarketingObject.SERIAL_NUMBER));
			pmObj.setMsid((Integer)pmDBObject.get(PerfectMarketingObject.MSID));
			pmObj.setRedirecturl(String.valueOf(pmDBObject.get(PerfectMarketingObject.REDIRECT_URL)));
			pmObj.setDateadded((Date)pmDBObject.get(PerfectMarketingObject.DATE_ADDED));
			pmObj.setNoattempt((Integer)pmDBObject.get(PerfectMarketingObject.NO_ATTEMPT));

		} else {
			return null;
		}
		return pmObj;
	}
	
	public static DBObject getDBObjectByPerfectMarketingObject(PerfectMarketingObject pmObj) throws Exception {
		DBObject mrDBObject = new BasicDBObject();
		mrDBObject.put(PerfectMarketingObject.SERIAL_NUMBER, pmObj.getSerialnumber());
		//mrDBObject.put("$inc", new BasicDBObject(PerfectMarketingObject.SERIAL_NUMBER, 1));
		mrDBObject.put(PerfectMarketingObject.MSID, pmObj.getMsid());
		mrDBObject.put(PerfectMarketingObject.REDIRECT_URL, pmObj.getRedirecturl());
		mrDBObject.put(PerfectMarketingObject.DATE_ADDED, pmObj.getDateadded());
		mrDBObject.put(PerfectMarketingObject.NO_ATTEMPT, pmObj.getNoattempt());
		if(pmObj.getExtractedfrom()!= null) {
			mrDBObject.put(PerfectMarketingObject.EXT_FROM, pmObj.getExtractedfrom());
		}

		return mrDBObject;
	}
}
