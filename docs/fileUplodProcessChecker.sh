#define fileToLog
logFile=processCheckerLog

# This function is used to check the passed arg and run
# There are few 5 or 10 sec  basis job which is being called in background
# and this shell script check those running process and if running does nothing and if not running then invoked.
#this is applicable when server get restarted. this function takes 2 argument first shell script name and second extention i.e .sh
function checkAndStart()
{
PROCESS_NUM=`ps -ef | grep sh | grep $1 | wc -l`
now="$(date +'%d/%m/%Y %T')"
if [ $PROCESS_NUM -eq 1 ]
  then
        #echo "inside if"
        processid=`ps -ef | grep sh | grep $1 | awk ' { print $2 } '`
        #kill -9 $processid
        echo "process id $processid ($1 ) already running at $now..." >> $logFile
  else
        #echo "inside else"
        echo "NOT RUNNING $1 shell script at $now" >> $logFile
        #echo $1$2
        sh $1$2 &
fi
}


#echo "call function with first paramete shell script name and second parameter with extention."
checkAndStart "breakingNewsUploadJob" ".sh"
checkAndStart "liveblogUploadJob" ".sh"
echo "checked completed $(date +'%d/%m/%Y %T')" >> $logFile
